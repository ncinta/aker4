<script type="text/javascript">
var idChofer = 0;
var id = 0;

function modalLicenciaNew(id, nombre) {
 
        window.idChofer = id;
        $('#nombreChofer').text('');
        $('#nombreChofer').text(nombre);

        $("#licencia_expedicion").datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
          $("#licencia_vencimiento").datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
      
        $('#modalLicenciaNew').modal('show');
}

function modalLicenciaDelete(id, numero) {
        window.id = id;
        $('#nombreLicencia').text('');
        $('#nombreLicencia').text(numero);
        $('#modalLicenciaDelete').modal('show');
}

function modalLicenciaEdit(id, numero,categoria,expedicion,vencimiento,aviso) {
     
        window.id = id;
        $('#nombreLicencia').text('');
        $('#nombreLicencia').text(numero);
        $('#licencia_edit_numero').val(numero);
        $('#licencia_edit_categoria').val(categoria);
        $('#licencia_edit_aviso').val(aviso);
        $("#licencia_edit_expedicion").datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
          $("#licencia_edit_vencimiento").datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
   
        $('#licencia_edit_expedicion').val(expedicion);
        
        $('#licencia_edit_vencimiento').val(vencimiento);
        $('#modalLicenciaEdit').modal('show');
}

$('#btnLicenciaNew').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_chofer_newlicencia') }}",
                data: {
                'id': window.idChofer,
                'form': $("#formLicencia").serialize(),
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                        $("#choferes").text("Espere......");
                },
        });
        req.done(function (respuesta) {
        $("#choferes").text("");
                $("#choferes").html(respuesta.html);
                $('#modalLicenciaNew').modal('toggle'); //cierro el form
                document.getElementById("formLicencia").reset();
           
                newNotify('Exito!','success','La licencia se creó correctamente');
        });
});

$('#btnLicenciaEdit').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_chofer_editlicencia') }}",
                data: {
                'id': window.id,
                        'form': $("#formEditLicencia").serialize(),
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                        $("#choferes").text("Espere....."); 
                },
        });
        req.done(function (respuesta) {
                $("#choferes").text("");
                $("#choferes").html(respuesta.html);
                $('#modalLicenciaEdit').modal('toggle'); //cierro el form
                document.getElementById("formEditLicencia").reset();
             
                newNotify('Exito!','success','La licencia se modificó correctamente');
        });
});

$('#btnLicenciaDelete').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_chofer_deletelicencia') }}",
                data: {
                'id': window.id,
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                        $("#choferes").text("Espere....."); 
                },
        });
        req.done(function (respuesta) {
        $("#choferes").text("");
                $("#choferes").html(respuesta.html);
                $('#modalLicenciaDelete').modal('toggle'); //cierro el form
                newNotify('Exito!','success','La licencia se eliminó correctamente');
        });
});


</script>