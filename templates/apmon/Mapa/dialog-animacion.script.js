var animate;
var velocidad = 1000;
var zoomInteligente;
var nivelZoom;
var step;

//con esto abro el dialog-form
function verAnimacionPanel(max) {
    $("#dialog-animacion").dialog("open");
    $("#slider").slider("option", "max", max);

}

$(function () {
    //esto es para setear los datos del dialog.
    $("#dialog-animacion").dialog({
        position: [10, 10],
        autoOpen: false,
        width: 350,
        height: 250,
        modal: false,
        open: function () {
            step = 0;
            nivelZoom = 16;
            zoomInteligente = false;
            velocidad = 1000;
        },
        close: function () {
        },
    });

    //inicializo los divs vacios.
    $("#animate-panel-status").html("");
    $("#animate-panel-data").html("");

    //botones para cuando hay stop
    $('#btn-animatePlay').removeAttr('disabled');
    $('#btn-animatePause').attr('disabled', true);
    $('#btn-animateStop').attr('disabled', true);

    $("#slider").slider({
        change: function (event, ui) {
            step = $("#slider").slider("option", "value");
            moveToOneStep(step);
        }
    });

});

function animatePlay() {
    //botones para cuando hay play
    $('#btn-animatePlay').attr('disabled', true);
    $('#btn-animatePause').removeAttr('disabled');
    $('#btn-animateStop').removeAttr('disabled');

    $("#animate-panel-status").html("Iniciando...");
    moveToStep(step);
}

function animatePause() {
    //botones para cuando hay play
    $('#btn-animatePlay').removeAttr('disabled');
    $('#btn-animateStop').removeAttr('disabled');

    clearTimeout(animate);
    $("#animate-panel-status").html("Pausado");
}

function animateStop() {
    clearTimeout(animate);
    step = 0;
    $("#animate-panel-status").html("Detenido.");

    //botones para cuando hay stop
    $('#btn-animatePlay').removeAttr('disabled');
    $('#btn-animateStop').attr('disabled', true);
}

function setVelocidad(ms) {
    velocidad = ms;
}

function setZoomInteligente() {
    zoomInteligente = $("#zoomInteligente").is(":checked");
    if (zoomInteligente === true) {
        nivelZoom = 16;
    }
}

var zoomActual;
var zoomUlt;
function moveToStep(c) {
    if (puntos.length > c) {
        pto = puntos[c];

        if (zoomInteligente === true) {
            if (pto[3] < 50) {
                zoomActual = 18;
            } else if (pto[3] < 100) {
                zoomActual = 15;
            } else if (pto[3] < 200) {
                zoomActual = 13;
            }
        } else {
            zoomActual = 14;
        }
        if (zoomUlt !== zoomActual) {
            clearTimeout(animate);
            setNivelZoom(zoomActual);
        }
        zoomUlt = zoomActual;

        moverPunteroMapa(pto[0], pto[1], pto[2], pto[3]);  //voy al pto en el mapa. lat/lon/dir/vel

        moveToGrid(c, pto[4]);
        $("#slider").slider("option", "value", step);
        //incremento el step
        step = step + 1;
        //espero y vuelvo a llamar a la funcion.
        animate = setTimeout(function () {
            moveToStep(step);
        }, velocidad);
    }else{
       animateStop();
    }
}

function moveToGrid(c, oid) {
    if (c === 0) {
        //$('#grilla'+grillaPage).animate({scrollTop: 0}, 0);
    }
    x = c + 1;
    $("#animate-panel-data").html($("#servicio_panel_" + oid).html());
    $("#animate-panel-status").html("<em>Reproduciendo " + x + " de " + puntos.length + "</em>");
    porc = x * 100 / puntos.length;
    $("#progressbar").progressbar({value: porc.toFixed(0)});
}


function moveToOneStep(c) {
    if (puntos.length > c) {
        pto = puntos[c];
        if (zoomInteligente === true) {
            if (pto[3] < 50) {
                zoomActual = 18;
            } else if (pto[3] < 100) {
                zoomActual = 15;
            } else if (pto[3] < 200) {
                zoomActual = 13;
            }
        } else {
            zoomActual = 14;
        }
        if (zoomUlt !== zoomActual) {
            setNivelZoom(zoomActual);
        }
        zoomUlt = zoomActual;

        moverPunteroMapa(pto[0], pto[1], pto[2], pto[3]);  //voy al pto en el mapa. lat/lon/dir/vel

        moveToGrid(c, pto[4]);
    }
}