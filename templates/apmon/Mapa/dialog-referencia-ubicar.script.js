//con esto abro el dialog-form
function buscarReferencia() {
    $.get( "{{ url('main_referencia_search_ajax') }}",
            { 'query' : null }, 
        function(respuesta) {            
            $("#dialog-form-referencia-table").html(respuesta['referencias']); 
            $('#dialog-form-referencia-buscar').dialog("open");
        }, 'json');
}

$(function() {
    //esto es para setear los datos del dialog.
    $("#dialog-form-referencia-buscar").dialog({
        position: [$(window).width() - 280, 140],
        autoOpen: false,
        width: 250,
        height: 400,
        modal: false,
        open: function() {
            //aca se hace la inicializacion de las variables del form
        },
        close: function() {
            //aca se hacen las cosas cuando se cierra.
        }
    });
});

//aca es cuando hago el post y debo crear la nueva referencia.-
function dialogformReferenciaSearch() {
   //envio de datos.
    $.get( "{{ url('main_referencia_search_ajax') }}",
            { 'query' : $("#input-search").val() }, 
        function(respuesta) {            
            $("#dialog-form-referencia-table").html(respuesta['referencias']); 
        }, 'json');
}

function centerReferencia(id, clase, lat, lon) {
    var request = $.ajax({
          url: "{{ path('main_referencia_marker') }}", 
          data: {'id' : id},
          dataType: "json",
          timeout: 10000, //in milliseconds
    });
        request.done(function(respuesta) {
           {{ mapa.fcStopDraw() }}();
           {{ mapa.fcAddAllIcons() }}(respuesta['icon']);       //agrego los iconos
           {{ mapa.fcAddAllPolygons() }}(respuesta['polygon']); //agrego los poligonos
           {{ mapa.fcAddAllCircles() }}(respuesta['circle']);   //agrego circulos
           {{ mapa.fcAddAllMarkers() }}(respuesta['referencia']);   //agrego las referencias.
           {{ mapa.fcSetClusterMinSize() }}(4); 
           {{ mapa.fcIniDraw() }}();
        });
    
    
    {{ mapa.fcAddMarker() }}({id_sc: 'r'+id, visible: true});
    if (clase === 1) {
        {{ mapa.fcAddCircle() }}({id_sc: 'c'+id, visible: true});                        
    } else {
        {{ mapa.fcAddPolygon() }}({id_sc: 'p'+id, visible: true});                        
    }
    gotoCenter(lat, lon, true);
}

