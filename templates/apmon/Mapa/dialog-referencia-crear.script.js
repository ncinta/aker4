var ultimo = null;    //es el puntero al ultimo elemento credo

//con esto abro el dialog-form
function crearReferencia() {
    $('#dialog-form-crear-ref').dialog( "moveToTop" );
    $('#dialog-form-crear-ref').dialog( "open" );        
}    

$(function() {
    //esto es para setear los datos del dialog.
    $( "#dialog-form-crear-ref" ).dialog({ 
        position: [10, 10], 
        autoOpen: false, 
        width: 350, 
        height: 390, 
        modal: false, 
        open: function () {
            //aca se hace la inicializacion de las variables del form
            $('#formcr_nombre').val('');
            $('#formcr_tipoDibujo').val('');
            $('#formcr_tipoDibujo').prop('disabled', false);
            $('#formcr_categoria').val('');
            $('#formcr_tipoReferencia').val('');
            
            document.getElementById('cr_zona-icono').src= '{{ asset("images/ball.png") }}';
            document.getElementById('formcr_pathIcono').value= '{{ asset("images/ball.png") }}';
        },
        close: function () {
            $('#dialog-icons').dialog("close");
            $('#cr_colorSelector').close;
        },
    });
    
    //esto es para seleccionar el color
    $("#cr_colorSelector").spectrum({
        color: color,
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        palette: palette_default,
        localStorageKey: "spectrum.homepage", // Any Spectrum with the same string will share selection
        hideAfterPaletteSelect:true,
        chooseText: "Cambiar",
        cancelText: "Cancelar",
        
//        change: function(color) {
//            rr_changeColor(color.toHexString());
//        },
    });
    
    $("#cr_colorSelector").on("dragstop.spectrum", function(e, color) {
        cr_changeColor(color.toHexString());
    });
    
    $("#cr_colorSelector").on("change", function(e, color) {
        cr_changeColor(color.toHexString());
    }); 

    $( "#cr_transparentSelector" ).slider({ 
        max: 10,
        min: 1,
        value : 5,
        change: function(event, ui) {            
            cr_changeTransparent(ui.value / 10)
        },
    });
    
    
});    
    $("#formcr_nombre").keyup(function() {
    $("#formcr_identificador").val($("#formcr_nombre").val()).change();
})

// Se llama a esta funcion cuando se selecciona uno de los tipos de dibujo.
function cr_clickTipoDibujo() {
    val = document.getElementById('formcr_tipoDibujo').value;
    if (val != null && val != '') {
        $('#formcr_tipoDibujo').prop('disabled', true);   //deshabilito el sector
        $('#formcr_clase').prop('value', document.getElementById('formcr_tipoDibujo').value);   //seteo el valor
        cr_newDraw();   //inicia el dibujado de lo que seleccionamos.  
    }
}

// Nuevo dibujo y instanciacion de variables.
function cr_newDraw(){
    cr_closeDraw();
    
    ultimo = 'ultimo';
    var c = {{ mapa.getMapName() }}.getCenter();
    
    tDraw = document.getElementById('formcr_tipoDibujo').value;
    if (tDraw == 1) {  //agrego un nuevo radio
        {{ mapa.fcAddCircle() }}({id_sc:ultimo, editable: true, center: c, radius: 50, visible: true});
    } else {
        var paths = new Array();
        paths.push(new google.maps.LatLng(c.lat()-0.001, c.lng()-0.001));
        paths.push(new google.maps.LatLng(c.lat()-0.001, c.lng()+0.001));
        paths.push(new google.maps.LatLng(c.lat()+0.001, c.lng()+0.001));
        paths.push(new google.maps.LatLng(c.lat()+0.001, c.lng()-0.001));
        {{ mapa.fcAddPolygon() }}({id_sc:ultimo, editable: true, path: paths, visible: true });
    }
    cr_changeColor();
    cr_changeTransparent();
}        
    
function cr_showDraw(ver)  {
    tDraw = document.getElementById('formcr_tipoDibujo').value;
    if (tDraw == 1) {  //agrego un nuevo radio
        {{ mapa.fcAddCircle() }}({id_sc:ultimo, visible: ver });
    } else {
        {{ mapa.fcAddPolygon() }}({id_sc:ultimo, visible: ver }); 
    }
}

function cr_changeColor(color) {    
    if (color == null) {
        //color = $('#cr_colorSelector div').css('backgroundColor');
        color = $("#cr_colorSelector").spectrum("get").toHexString();
    }    
    $('#formcr_color').val(color); //aca grabo el valor en el form
    tDraw = document.getElementById('formcr_tipoDibujo').value;
    if (tDraw == 1) {  //agrego un nuevo radio
        {{ mapa.fcAddCircle() }}({id_sc:ultimo, strokeColor: color, strokeWeight: 5, fillColor: color});
    } else {
        {{ mapa.fcAddPolygon() }}({id_sc:ultimo, strokeColor: color, strokeWeight: 5, fillColor: color});

    }
}    

function cr_changeTransparent(transparent) {
    if (ultimo != null) {
        if (transparent == null) {
            transparent = $( "#cr_transparentSelector" ).slider("value") / 10;
            strokeTransparent = transparent + 0.2;
        }
        $('#formcr_transparencia').val(transparent); //aca grabo el valor en el form
        tDraw = document.getElementById('formcr_tipoDibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
            {{ mapa.fcAddCircle() }}({id_sc:ultimo, strokeOpacity: strokeTransparent, fillOpacity: transparent });
        } else {
            {{ mapa.fcAddPolygon() }}({id_sc:ultimo, strokeOpacity: strokeTransparent, fillOpacity: transparent });

        }
    }
}    
// Cerrar el dibujo
function cr_closeDraw(){
    if(ultimo != null){        
        tDraw = document.getElementById('formcr_tipoDibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
            //cierro la edición del circulo.
            {{ mapa.fcAddCircle() }}({id_sc:ultimo, editable: false });
                            
            var circulo = {{ mapa.varCircles() }}.get(ultimo);
            //grabo en los inputs el radio y el centro.
            document.getElementById('formcr_centro').value = circulo.getCenter();
            document.getElementById('formcr_radio').value = circulo.getRadius();
        } else {
            // cierro el poligono.
            {{ mapa.fcAddPolygon() }}({id_sc:ultimo, editable: false, visible: false });
                
            //obtengo los puntos para grabarlo en el input del poligono.
            var puntos = {{ mapa.fcPolygonPath() }}(ultimo);
            var res = puntos.join();
            document.getElementById('formcr_poligono').value = res;  
            
            var puntos_obj = {{ mapa.fcPolygonObject() }}(ultimo);
            document.getElementById('formcr_area').value = google.maps.geometry.spherical.computeArea(puntos_obj);
        }
        cr_showDraw(false);
    }
}

//aca es cuando hago el post y debo crear la nueva referencia.-
function dialogformReferenciaPost() {
    cr_closeDraw();   //es para cerrar el dibujo si esta abierto
    var request = $.ajax({
      type: 'POST',
      url: "{{ path('main_referencia_create_ajax') }}", //en el ReferenciaController.
      data: { 'referencia': $("#formcr_referencia").serialize() },   //serializo el form
      dataType: "json",
      //async: true,
      beforeSend: function(){                  
        },
    });
    request.done(function(respuesta) {    //true || verReferencias
        if (respuesta['result'] != true) {
            alert(respuesta['result'])
        } else {
            tDraw = document.getElementById('formcr_tipoDibujo').value;
            if (tDraw == 1) {  //agrego un nuevo radio        
                {{ mapa.fcAddCircle() }}({id_sc:ultimo, visible: false});                        
            } else {
                {{ mapa.fcAddPolygon() }}({id_sc:ultimo, visible: false});                        
            }
            newRef = respuesta['referencia']
            {{ mapa.fcAddIcon() }}(newRef['icono'], true);
            {{ mapa.fcAddAllMarkers() }}(newRef['referencia']);
            {{ mapa.fcAddAllCircles() }}(newRef['circle']); 
            {{ mapa.fcAddAllPolygons() }}(newRef['polygon']); 
            ultimo = null;                
            $("#dialog-form-crear-ref").dialog( "close" );
            //verTodasReferencias();
        }
    });            
}
function dialogformReferenciaCancel() {
    cr_closeDraw();         //cierro el dibujo
    ultimo = null;          //anulo el puntero de lo que tengo creado
    $( "#dialog-form-crear-ref" ).dialog( "close" );  //cierro el form    
}

