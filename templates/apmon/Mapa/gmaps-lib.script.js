function gotoCenter(lat, lon, zoom) {
     {{ mapa.getMapName() }}.panTo(new google.maps.LatLng (lat, lon));
     if (zoom == true) {
         {{ mapa.getMapName() }}.setZoom(16); 
     }
}

function setNivelZoom(nro) {
    {{ mapa.getMapName() }}.setZoom(nro); 
}

function showInfoReferencia(e, marcador){
    {{ mapa.fcStopDraw() }}();
    url = trim('{{ url("main_referencia_info_ajax") }}');
    var request = $.ajax({
        url: url.replace(/^\s+/g,'').replace(/\s+$/g,''),
        data: {'id_sc': marcador.id_sc},
        beforeSend: function() {
            {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: '<em><b>Obteniendo detalles, espere...</b></em>'});
            {{ mapa.varMarkers() }}.get(marcador.id_sc).openInfo();
        }
    });
    request.done(function(info) {
        {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: info});
        {{ mapa.varMarkers() }}.get(marcador.id_sc).openInfo();
    });
    {{ mapa.fcIniDraw() }}();   
}


function showInfoServicio(e, marcador) {
   /* $('#panel-equipos').animate({ scrollTop: $('#'+marcador.id_sc).offset().top  }, 'slow'); */
    
    marks = {{ mapa.varMarkers() }};
    var request = $.ajax({
        url: "{{ url('main_mapa_servicio_infoAJAX') }}",
        data: {'idServ': marcador.id_sc},
        beforeSend: function() {
            {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: '<em><b>Obteniendo detalles, espere...</b></em>'});
            marks.get(marcador.id_sc).openInfo();
        }
    });
    request.done(function(info) {
        {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: info});
        marks.get(marcador.id_sc).openInfo();
    });
}

function showInfoBackend(e, marcador,oid) {
    marks = {{ mapa.varMarkers() }};
    var request = $.ajax({
        url: "{{ url('main_mapa_backend_infoAJAX') }}",
        data: {'oid': oid},
        beforeSend: function() {
            {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: '<em><b>Obteniendo detalles, espere...</b></em>'});
            marks.get(marcador.id_sc).openInfo();
        }
    });
    request.done(function(info) {
        {{ mapa.fcAddMarker() }}({id_sc: marcador.id_sc, informacion: info});
        marks.get(marcador.id_sc).openInfo();
    });
}
