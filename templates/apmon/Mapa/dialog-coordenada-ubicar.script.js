//con esto abro el dialog-form
function buscarCoordenada() {
    $('#dialog-form-coordenada-buscar').dialog("open");
    
}

$(function() {
    //esto es para setear los datos del dialog.
    var btnBuscar = $("#buscarCoord");
    $("#dialog-form-coordenada-buscar").dialog({
        position: [$(window).width() - 280, 140],
        autoOpen: false,
        width: 350,
        height: 200,
        modal: false,
        open: function() {
            $("#buscarCoord").prop('disabled', true);
            $("#input-search-coord-long").val("");
            $("#input-search-coord-lat").val("");
            //aca se hace la inicializacion de las variables del form
        },
        close: function() {
            //aca se hacen las cosas cuando se cierra.
        }
    });
});
$(function() {
    var lat = $("#input-search-coord-lat");
    var long = $("#input-search-coord-long");
    var btnBuscar = $("#buscarCoord");
    lat.add(long).on('input', function() {
        // Verifica si ambos campos de texto están vacíos
        if (lat.val().trim() === '' || long.val().trim() === '') {
          // Si al menos uno de los campos está vacío, deshabilita el botón
          btnBuscar.prop('disabled', true);
        } else {
          // Si ambos campos tienen contenido, habilita el botón
          btnBuscar.prop('disabled', false);
        }
      });
});

function dialogCoordenadaBuscar() {
    var lat = $("#input-search-coord-lat").val();
    var long = $("#input-search-coord-long").val();
    gotoCenter(lat, long, true);
}
function dialogCoordenadaCancel() {
   
    $( "#dialog-form-coordenada-buscar" ).dialog( "close" );  //cierro el form    
}