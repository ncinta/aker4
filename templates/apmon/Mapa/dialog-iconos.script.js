var parentIcono = null;

function cambiarIcono(parent) {
    parentIcono = parent;
    $( "#dialog-icons" ).dialog( "open" );    
}   
 
$(function() {
    $( "#dialog-icons" ).dialog(
        { position: [30, 30], 
          autoOpen: false, 
          height: 400, 
          width: 550, 
          modal: false, 
          open: function () {
            $.get( '{{ url("main_referencia_obtenergrillaiconos_ajax") }}' , 
                function(respuesta) {
                    //se ejecuta cuando se hace un click en el icono
                    $('#dialog-iconos-grilla').html(respuesta); 
                    $( ".icono-dialog" ).click(function(event) {
                       path = jQuery(event.target).attr('src');
                       if (parentIcono !== null) {
                           if(parentIcono === 1) {
                               document.getElementById('cr_zona-icono').src= path;
                               document.getElementById('formcr_pathIcono').value= path;
                           } else {
                               document.getElementById('rr_zona-icono').src= path;
                               document.getElementById('formrr_pathIcono').value= path;
                           }
                       }
                       $( "#dialog-icons" ).dialog( "close" );       
                    });    
                }, 'html');
            },
        }
    );

});

