<script type="text/javascript">
var idGrupo = 0;

function modalGrupoEdit(id, nombre) {
        window.id = id;
        $('#edit_grupoNombre').val(nombre);
        $('#modalGrupoEdit').modal('show');
}

function modalGrupoDelete(id, nombre) {
        window.id = id;
        $('#nombreGrupo').text('');
        $('#nombreGrupo').text(nombre);
        $('#modalGrupoDelete').modal('show');
}


$('#btnGrupoNew').click(function () {
    if($('#grupo_nombre').val() !== ''){
         var req = $.ajax({
            type: 'POST',
                    url: "{{ path('apmon_notificacionchofer_newajax') }}",
                    data: {
                        'form': $("#formGrupo").serialize(),
                    },
                    dataType: "json",
                    //async: true,
                    beforeSend: function () {
                    },
            });
            req.done(function (respuesta) {
                    $("#grupos").text("");
                    $("#grupos").html(respuesta.html);
                    $('#modalGrupoNew').modal('toggle'); //cierro el form
                    document.getElementById("formGrupo").reset();
              
                    newNotify('Exito!','success','El grupo de notificación se creó correctamente');
            });
        }else{
                
                    newNotify('Error!','error','Elcampo nombre no puede estar vacío');

    }
});

$('#btnGrupoEdit').click(function () {
    if($('#edit_grupoNombre').val() !== ''){
         var req = $.ajax({
            type: 'POST',
                    url: "{{ path('apmon_notificacionchofer_editajax') }}",
                    data: {
                        'id': window.id,
                        'form': $("#formEditGrupo").serialize(),
                    },
                    dataType: "json",
                    //async: true,
                    beforeSend: function () {
                    },
            });
            req.done(function (respuesta) {
                    $("#grupos").text("");
                    $("#grupos").html(respuesta.html);
                    $('#modalGrupoEdit').modal('toggle'); //cierro el form
                    document.getElementById("formEditGrupo").reset();
                  
                    newNotify('Exito!','success','El grupo de notificación se creó correctamente');
            });
        }else{
                   
                    newNotify('Error!','error','El campo nombre no puede estar vacío');

    }
});

$('#btnGrupoDelete').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_notificacionchofer_deleteajax') }}",
                data: {
                'id': window.id,
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
                $("#grupos").text("");
                $("#grupos").html(respuesta.html);
                $('#modalGrupoDelete').modal('toggle'); //cierro el form
                newNotify('Exito!','success','El grupo  se eliminó correctamente');
        });
});

</script>