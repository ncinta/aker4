<script type="text/javascript">
var idChof = 0;
var idContact = 0;

$('#btnChoferAdd').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_notificacionchofer_addchofer') }}",
                data: {
                'id': '{{grupo.id}}',
                        'form': $("#formChoferes").serialize(),
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
        $("#choferes").text("");
                $("#choferes").html(respuesta.html);
                $("#formChofer").text("");
                $("#formChofer").html(respuesta.form);
                $('#modalAddChofer').modal('toggle'); //cierro el form
                newNotify('Exito!','success','El chofer se agregó correctamente');
        });
});

$('#btnContactoAdd').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_notificacionchofer_addcontacto') }}",
                data: {
                'id': '{{grupo.id}}',
                        'form': $("#formContactos").serialize(),
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
        $("#contactos").text("");
                $("#contactos").html(respuesta.html);
                $("#formContacto").text("");
                $("#formContacto").html(respuesta.form);
                $('#modalAddContacto').modal('toggle'); //cierro el form
                newNotify('Exito!','success','El contacto se agregó correctamente');
        });
});

function modalChoferDelete(id, nombre) {
        window.idChof = id;
        $('#nombreChofer').text('');
        $('#nombreChofer').text(nombre);
        $('#modalChoferDelete').modal('show');
}

function modalContactoDelete(id, nombre) {
        window.idContact = id;
        $('#nombreContacto').text('');
        $('#nombreContacto').text(nombre);
        $('#modalContactoDelete').modal('show');
}

    $('#btnChoferQuitar').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_notificacionchofer_deletechofer') }}",
                data: {
                'id': window.idChof,
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
        $("#choferes").text("");
                $("#choferes").html(respuesta.html);
                $("#formChofer").text("");
                $("#formChofer").html(respuesta.form);
                $('#modalChoferDelete').modal('toggle'); //cierro el form
                newNotify('Exito!','success','El chofer se quitó correctamente');
        });
});

$('#btnContactoQuitar').click(function () {
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('apmon_notificacionchofer_deletecontacto') }}",
                data: {
                'id': window.idContact,
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
        $("#contactos").text("");
                $("#contactos").html(respuesta.html);
                $("#formContacto").text("");
                $("#formContacto").html(respuesta.form);
                $('#modalContactoDelete').modal('toggle'); //cierro el form
                newNotify('Exito!','success','El contacto se quitó correctamente');
        });
});

        //cuando se checkea todos.
        function checkAllChoferes() {
        $("input:checkbox").attr('checked', document.getElementById('check_all_choferes').checked);
        clickCheckChofer();
        }

        //funcion que hace visible los botones cuando se checkea un check.
        function clickCheckChofer() {
        var count = 0;
        for (var i = 0; i < document.formChoferes.elements.length; i++) {
                if (document.formChoferes.elements[i].checked) {
                count++;
                }
                }
        }
       
        function checkAllContactos() {
        $("input:checkbox").attr('checked', document.getElementById('check_all_contactos').checked);
        clickCheckContacto();
        }

        //funcion que hace visible los botones cuando se checkea un check.
        function clickCheckContacto() {
        var count = 0;
        for (var i = 0; i < document.formContactos.elements.length; i++) {
                if (document.formContactos.elements[i].checked) {
                count++;
                }
                }
        }


</script>