$(function () {
    //disableFlotas();
    $("#consumocombustible_desde").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });
    $("#consumocombustible_hasta").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });

    $('#consumocombustible_servicio').select2();
});

$("#consultar").on('click', function () {
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('fuel_info_rendimientocargas_generar',{id: organizacion.id}) }}",
        data: {
            'formRendimiento': $("#formRendimiento").serialize(), // formulario con los datos

        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.status == 'ok') {
            $('#modalLoad').modal('hide');
            $("#content").html("");
            $("#content").append(respuesta.html);



        }

    });
});

