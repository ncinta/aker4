google.charts.load('current', { packages: ['corechart', 'bar'] });// todos los paquetes de graficos

$(document).on('ready', function () {

  $('#consumocombustible_servicio option[value="-1"]').remove();
  $('#consumocombustible_servicio').select2();

  $('#consumocombustible_desde').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es',
    widgetPositioning: {
      horizontal: 'right',
      vertical: 'bottom'
    }
  });

  $('#consumocombustible_hasta').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es',
    widgetPositioning: {
      horizontal: 'right',
      vertical: 'bottom'
    }
  });

})



$('#consultar').on('click', function () {
  var request = $.ajax({
    type: 'POST',
    url: "{{ path('fuel_info_rendimiento_generar',{id: organizacion.id}) }}",
    data: {
      formRendimiento: $('#formRendimiento').serialize() // formulario con los datos
    },
    dataType: 'json',
    // async: true,
    beforeSend: function () {
      $('#modalLoad').modal({ backdrop: 'static', keyboard: false });
      $('#graficos-tab').tab('show');
    }
  })
  request.done(function (respuesta) {
    $('#modalLoad').modal('hide');
    if (respuesta.status == 'ok') {
      $('#tabla').html('');
      if (respuesta.con_datos) {
        $('#tabla').append(respuesta.html);
        getDataTable('tableRendimiento');
        google.charts.setOnLoadCallback(function () {
          drawChartBarrasLitros(respuesta.grafico);
        })
      }else{
        $('#tabla').append('<div class="alert alert-xs alert-warning"><em>No se registran consumos.</em></div>');
        $('#grafico_barras_litros').append('<div class="alert alert-xs alert-warning"><em>No se registran consumos.</em></div>');       

      }
    }
  })
});
function drawChartBarrasLitros(data) {
  var i = 1;
  var arr = new Array();
  arr[0] = ['Servicio', 'Litros Movimiento', 'Litros Ralenti', 'Litros x Km (Teórico)'];

  Object.entries(data).forEach(([key, value]) => {
    arr[i] = [value.nombre, value.odolitro_movimiento, value.odolitro_ralenti, value.rendimiento_teorico];
    console.log(arr[i]);
    i++
  });

  var data = google.visualization.arrayToDataTable(arr);

  var options = {
    seriesType: 'bars',
    series: { 2: { type: 'line' } },
    width: '100%',  // Ancho del gráfico
    height: '100%',  // Altura del gráfico
    isStacked: true,
    vAxis: {
      title: 'Litros'
    }
  };

  var chart = new google.visualization.ComboChart(document.getElementById('grafico_barras_litros'));
  chart.draw(data, options);

}






