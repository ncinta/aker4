function buscarCargas(vid) {
    cargaId = vid;    
    //traer los datos del back de la carga    
    route = Routing.generate('fuel_check_buscarcarga', {id: vid});

    var req = $.ajax({
        type: 'GET',
        url: route,        
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (r) {
      //  alert(r);
        $("#tablaCarga").html(r);
        $("#modalBuscarCarga").modal('show');
    });
    //cargar el form con los datos    
    
}

function setCarga(combustibleId) {
    route = Routing.generate('fuel_check_setcarga', {carga: cargaId, combustible: combustibleId});
    var req = $.ajax({
        type: 'POST',
        url: route,
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (r) {
        $("#modalBuscarCarga").modal('hide');
        $('#tablaCargas').DataTable().ajax.reload();
    });
}