function uploadCargas(vid) {   //paso el id de la organizacion para que sean asociadas las cargas
    var f = $(this);
    var formData = new FormData(document.getElementById("formUpload"));

    var req = $.ajax({
        type: 'POST',
        url: Routing.generate('fuel_check_upload', { id: vid }),
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
    req.done(function (r) {       
        $("#modalUploadCargas").modal('hide');
        $('#tablaCargas').DataTable().ajax.reload();
    });

}
