function addCarga(vid, vservicioId) {
    cargaId = vid;
    servicioId = vservicioId;
    //alert(id);
    //traer los datos del back de la carga    
    route = Routing.generate('fuel_check_dataimport', {id: vid});

    var req = $.ajax({
        type: 'GET',
        url: route,
        data: {
            'formAdd': $("#formAdd").serialize(), //form de productos
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (r) {
        //alert(r);
        $("#addfuel_fecha").val(r.fecha);
        $("#addfuel_odometro").val(r.fecha);
        $("#addfuel_descripcion").val(r.descripcion);
        $("#addfuel_litros_carga").val(r.litrosCarga);
        $("#addfuel_monto_total").val(r.montoTotal);
        $("#addfuel_guia_despacho").val(r.guiaDespacho);
        
        $("#addfuel_puntoCarga").val(r.puntoCarga);
        $('#addfuel_puntoCarga').select2({width: '100%'}).trigger('change');

        $("#addfuel_chofer").val(r.chofer);
        $('#addfuel_chofer').select2({width: '100%'}).trigger('change');

        $("#addfuel_tipoCombustible").val(r.tipoCombustible);
        $('#addfuel_tipoCombustible').select2({width: '100%'}).trigger('change');
    });
    //cargar el form con los datos
    
    $("#modalAddCarga").modal('show');
}

function grabarCarga() {
    route = Routing.generate('fuel_check_agregarcarga', {carga: cargaId, servicio: servicioId});
    var req = $.ajax({
        type: 'POST',
        url: route,
        data: {'data': $("#formAdd").serialize()},
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#modalAddCarga").modal('hide');
            $(`#btnAddCarga_${cargaId}`).html('Grabando...'); 
            $(`#btnAddCarga_${cargaId}`).attr("disabled", true);
        },
    });
    req.done(function (r) {
        $('#tablaCargas').DataTable().ajax.reload();
    });
}