function ignorarCheck(id) {
    route = Routing.generate('fuel_check_changestatus', {carga: id});
    var req = $.ajax({
        type: 'POST',
        url: route,
        data: {'status': 8},
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (r) {        
        $('#tablaCargas').DataTable().ajax.reload();
    });
}

function showBtnDelete() {
    var count = 0;
    for(var i=0; i<document.formCheck.elements.length; i++) {
        if (document.formCheck.elements[i].checked) {
            count++;
        }
    }
    if(count > 0){
        $("#btnDeleteCarga").show();
    }else{
        $("#btnDeleteCarga").hide();
    }
}

function checkAll() {
    $("input:checkbox").prop('checked', document.getElementById('check_all').checked);
     showBtnDelete();
 }

 function deleteCargas(){
    route = Routing.generate('fuel_check_deletecargas');

    var req = $.ajax({
        type: 'POST',
        url: route,
        data: {
            'formCheck': $("#formCheck").serialize(), //form de productos
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (r) {
        if(r.status == 'ok'){
            $('#modalDelete').modal('toggle');
            $("#btnDeleteCarga").hide();
            newNotify('Exito !!!', 'success', 'Cargas Eliminadas');
        }
        $('#tablaCargas').DataTable().ajax.reload();
    
    });
 }
 

