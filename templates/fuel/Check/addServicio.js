
function addServicio(vcargaId) {
    cargaId = vcargaId;
    //alert(id);
    //solo debo mostrar el modal porque los datos del servicio ya los tengo.    
    $("#modalServicio").modal('show');
}

function grabarServicio() {
    //alert('aca');
    servicioId = document.getElementById("selectServicio").value;

    route = Routing.generate('fuel_check_asociarservicio', { id: cargaId });    
    var req = $.ajax({
        type: 'POST',
        url: route,
        data: {'servicioId': servicioId},
        dataType: "json",
    });
    req.done(function (r) {
        $("#modalServicio").modal('hide');
        $('#tablaCargas').DataTable().ajax.reload();
    });
}