function getDataMap() {
    reqDataMap = $.ajax({
        type: 'POST',
        url: "{{ path('servicio_historico_datamap') }}",
        data: {
            'arrIds': arrIds
        },
        dataType: "json",
        async: true,
        beforeSend: function () {

        }
        ,
    }
    );
    reqDataMap.done(function (respuesta) {
        jQuery.each(respuesta.arrData, function (i, val) {
            $("#sentido_" + i).html("<img width='16px' height='16px' src='" + val.icono + "' title='" + val.ultVelocidad + "' />" + " " + val.ultVelocidad + " km/h");
            if (val.leyenda == null) {
                $("#direccion_" + i).html(val.direccion);
            } else {
                if (val.iconoRef != null) {
                    $("#direccion_" + i).html(val.direccion + "<br>" +
                        "<img width='24px' height='24px' src='" + val.iconoRef + "' />" +
                        " <b>" + val.leyenda + "</b>");
                } else {
                    $("#direccion_" + i).html(val.direccion + "<br>" + " <b>" + val.leyenda + "</b>");
                }
            }

        });
    });
}
