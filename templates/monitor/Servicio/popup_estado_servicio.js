globalIdItinerario = 0;
globalIdServicio = 0;
$("#btnEstadoServicioModificar").click(function () {
    var request = $.ajax({
        type: "POST",
        url: "{{ path('itinerario_servicio_editestado') }}", //aca cambiar por funcion en servicio
        data: {
            estado: $("#estado_servicio").val(),
            idServicio: globalIdServicio,
            idItinerario: globalIdItinerario
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });

        },
    });
    request.done(function (respuesta) {
        window.location.reload(); // es la vista show. necesito refrescar pagina porque son muchos los cambios de datos que hay

        $("#modalLoad").modal('hide');
        $("#modalEstadoServicio").modal('toggle');

    });
});


function changeEstadoServicio(idServicio, idItinerario) {
    globalIdItinerario = idItinerario;
    globalIdServicio = idServicio;
    $("#modalEstadoServicio").modal({ backdrop: 'static', keyboard: false });
}


