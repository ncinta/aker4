var arrIds = [];
$(document).ready(function () {
    $('#historico_servicio').select2();
    $('#historico_itinerario').select2();
    arrIds = "{{ arrIds }}";

    var tabla = $('table').dataTable({
        language: {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
    getDataMap();
    setInterval(reload, 60000);

});

function reload() {
    var req = $.ajax({
        type: 'GET',
        url: "{{ path('servicio_historico') }}",
        data: {
            'formHistServicio': $("#formHistServicio").serialize(), //form de productos
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
    });
    req.done(function (respuesta) {
        $("#sin_reportar").html("");
        $("#sin_reportar").append(respuesta.html_sin_reportar);

        $("#retrasados").html("");
        $("#retrasados").append(respuesta.html_retrasados);

        $("#reportando").html("");
        $("#reportando").append(respuesta.html_reportando);

        $("#count_sin_reportar").html("");
        $("#count_sin_reportar").text(respuesta.count_sin_reportar);

        $("#count_retrasado").html("");
        $("#count_retrasado").text(respuesta.count_retrasados);

        $("#count_reportando").html("");
        $("#count_reportando").text(respuesta.count_reportando);

        tabla = $('table').dataTable();
        $('#modalLoad').modal('hide');
        arrIds = respuesta.arrIds;
        if (reqDataMap !== null) {
            reqDataMap.abort();
        }
        getDataMap();

    });
}

$("#historico_tipoServicio").on('change', function () {
    if ($("#historico_tipoServicio").val() == "-1") {
        $('#historico_estado').prop('disabled', 'disabled');
        $('#historico_servicio').prop('disabled', 'disabled');
        $('#historico_transporte').prop('disabled', 'disabled');
        $('#historico_satelital').prop('disabled', 'disabled');
        $('#historico_empresa').prop('disabled', 'disabled');
        $('#historico_itinerario').prop('disabled', 'disabled');
    } else {
        $('#historico_estado').prop('disabled', false);
        $('#historico_servicio').prop('disabled', false);
        $('#historico_transporte').prop('disabled', false);
        $('#historico_satelital').prop('disabled', false);
        $('#historico_empresa').prop('disabled', false);
        $('#historico_itinerario').prop('disabled', false);

    }

});

$('#consultar').click(function () {
    reload();
});
$('#actualizar').click(function () {
    reload();
});