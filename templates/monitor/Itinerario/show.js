var idItRef = 0;


$("#btnModalServicios").click(function () {
	$('input[type="checkbox"]').prop("checked", false);
});
$("#btnModalReferencias").click(function () {
	$('input[type="checkbox"]').prop("checked", false);
});
$("#btnModalUsuarios").click(function () {
	$('input[type="checkbox"]').prop("checked", false);
});



$("#btnServicioAdd").click(function () {
	servicios = formatServicios();  
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_addservicio') }}",
		data: {
			idItinerario: "{{ itinerario.id }}",
			formServicio: servicios
		},
		dataType: "json",
		// async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		$("#tableServicios").text("");
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		$("#tableServicios").html(respuesta.html);
		getDataTable('tablaServicio');
		$("#modalServicio").modal("toggle");
		$("#servicio_servicio").val("").trigger("change");
		refreshTimeline();
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","Servicios agregados correctamente");
	
		window.location.reload(); 
	});
});


var globalIdServicio = 0;

function deleteServicio(id) {	
	$("#modalDeleteServicio").modal("show");
	globalIdServicio = id;
}

$("#btnEliminarServicio").click(function () {
	// alert('aca');
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_delservicio') }}",
		data: {
			idServ: globalIdServicio,
			id: "{{ itinerario.id }}",
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		$("#tableServicios").text("");
		$("#tableServicios").html(respuesta.html);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		getDataTable('tablaServicio');
		$("#modalDeleteServicio").modal("toggle");
		refreshTimeline();
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","El servicio se eliminó correctamente");
	;
	});
	globalIdServicio= 0;
});


$("#btnServicioEdit").click(function () {
	// alert('aca');
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_editservicio') }}",
		data: {
			idServ: globalIdServicio,
			id: "{{ itinerario.id }}",
			formServicio: $("#formServicioEdit").serialize(), // form de formServicioEdit
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	
	});
	request.done(function (respuesta) {
		$("#tableServicios").text("");
		$("#tableServicios").html(respuesta.html);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		getDataTable('tablaServicio');
		$("#modalServicioEdit").modal("toggle");
		refreshTimeline();
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","El servicio se modificó correctamente");
	
	});
	globalIdServicio= 0;
});


$("#btnReferenciaAdd").click(function () {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_addreferencia') }}",
		data: {
			id: "{{ itinerario.id }}",
			formReferencia: $("#formReferencia").serialize(), // form de productos
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		$("#modalReferencia").modal("toggle"); // cierro el form
		refreshTimeline();
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","Puntos agregados correctamente");
	
	});
});

$("#btnHorarioAdd").click(function () {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_setclock') }}",
		data: {
			id: "{{ itinerario.id }}",
			idItRef: idItRef,
			formHorario: $("#formHorario").serialize(), // form de productos
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		refreshTimeline();
		$('#modalLoad').modal('hide');
		$("#modalHorario").modal("toggle"); // cierro el form
	});
});

function referenciaClock(idRef) {
	$("#horario_arribo").val("");
	$("#horario_partida").val("");
	idItRef = idRef;
	var req = $.ajax({
		type: "GET",
		url: "{{ path('itinerario_getclock') }}",
		data: {
			idItRef: idItRef,
			id: "{{ itinerario.id }}",
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#horario_arribo").val(respuesta["ingreso"]);
		$("#horario_partida").val(respuesta["egreso"]);
		refreshTimeline();

	});
	$("#modalHorario").modal("toggle");
}

function referenciaDel(idRef) {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_delreferencia') }}",
		data: {
			idRef: idRef,
			id: "{{ itinerario.id }}",
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		refreshTimeline();
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","La referencia se eliminó correctamente");
		
	});
}
function referenciaUp(idRef) {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_upreferencia') }}",
		data: {
			idRef: idRef,
			id: "{{ itinerario.id }}",
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		refreshTimeline();
		$('#modalLoad').modal('hide');
	});
}

function referenciaDown(idRef) {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_downreferencia') }}",
		data: {
			idRef: idRef,
			id: "{{ itinerario.id }}",
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		refreshTimeline();
		$('#modalLoad').modal('hide');
	});
}
function changeStopType(idRef) {
	var stopType = $('#stopType_' + idRef).val();
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_changestoptype') }}",
		data: {
			idRef: idRef,
			id: "{{ itinerario.id }}",
			stopType: stopType
		},
		dataType: "json",
	});
	req.done(function (respuesta) {
		$("#tableReferencias").text("");
		$("#tableReferencias").html(respuesta.html);
		$("#tableReferencias_main").text("");
		$("#tableReferencias_main").html(respuesta.html_main);
		refreshTimeline();
		newNotify('Exito!',"success","El tipo de parada se modificó correctamente");
		$('#modalLoad').modal('hide');
	});
}

$("#btnUsuarioAdd").click(function () {
	var req = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_addusuario') }}",
		data: {
			id: "{{ itinerario.id }}",
			formUsuario: $("#formUsuario").serialize(), // form de usuarios
		},
		dataType: "json",
		// async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		$('#modalLoad').modal('hide');
		$("#tableUsuarios").text("");
		$("#tableUsuarios").html(respuesta.html);
		$("#modalUsuario").modal("toggle"); // cierro el form
		$("#usuario_usuario").val("").trigger("change");
		getDataTable('tablaUsuario');
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		document.getElementById("formUsuario").reset();
		getDataTable('tablaBitacora');
		newNotify('Exito!',"success","Usuarios agregados correctamente");
		
	});
});

var globalIdUsuario = 0;

function quitarUsuario(id) {
	$("#modalDeleteUsuario").modal("show");
	globalIdUsuario= id;
}

$("#btnQuitarUsuario").click(function () {
	// alert('aca');
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_deleteusuario') }}",
		data: {
			id: globalIdUsuario,
			idIt: '{{itinerario.id}}'
		},
		dataType: "json",
		// async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		$("#tableUsuarios").text("");
		$("#tableUsuarios").html(respuesta.html);
		getDataTable('tablaUsuario');
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		$('#modalLoad').modal('hide');
		$("#modalDeleteUsuario").modal("toggle");
		newNotify('Exito!',"success","Usuarios eliminados correctamente");
	});
	globalIdUsuario = 0;
});

$("#btnContactoAdd").click(function () {
	// alert('aca');
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_addcontacto') }}",
		data: {
			idIt: "{{ itinerario.id }}",
			formContacto: $("#formContacto").serialize(), // formulario con los datos
		},
		dataType: "json",
		// async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		$("#tableContactos").text("");
		$("#tableContactos").html(respuesta.html);
		getDataTable('tablaContacto');
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		document.getElementById("formContacto").reset();
		$("#modalContacto").modal("toggle");
		$("#contactos_contacto").val("").trigger("change");
		$('#modalLoad').modal('hide');
		newNotify('Exito!',"success","Contactos agregados correctamente");
	});
});

var globalIdContacto = 0;

function quitarContacto(id) {
	$("#modalDeleteContacto").modal("show");
	globalIdContacto= id;
}

$("#btnQuitarContacto").click(function () {
	var request = $.ajax({
		type: "POST",
		url: "{{ path('itinerario_deletecontacto') }}",
		data: {
			id: globalIdContacto,
			idIt: '{{itinerario.id}}'
		},
		dataType: "json",
		// async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		$("#tableContactos").text("");
		$("#tableContactos").html(respuesta.html);
		getDataTable('tablaContacto');
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		$('#modalLoad').modal('hide');
		$("#modalDeleteContacto").modal("toggle");
		newNotify('Exito!',"success","El contacto se quitó correctamente");
	
	});
	globalIdContacto = 0;
});

$('#btnCustodioAdd').click(function () {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_addservicio') }}",
		data: {
			idItinerario: "{{ itinerario.id }}",
			form: $("#formCustodio").serialize(), // formulario con los datos
		},
		dataType: "json"
	});
	req.done(function (respuesta) {
		$("#tableServicios").text("");
		$("#tableServicios").html(respuesta.html);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		document.getElementById("formCustodio").reset();
		refreshTimeline();
		$('#modalLoad').modal('hide');
		$('#modalCustodio').modal('toggle'); // cierro el form
		newNotify('Exito!',"success","Custodios agregados  correctamente");
	
		window.location.reload(); 
	});
});

function accionEvento(servicio_id, evento_id, itinerario_id, accion) {
	var request = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_asignar_eventos') }}",
		data: {
			'servicio': servicio_id,
			'evento': evento_id,
			'itinerario': itinerario_id,
			'accion': accion
		},
		dataType: "json",
		success: function (respuesta) {
			$("#div" + evento_id + "_" + servicio_id).html(respuesta.html);
			newNotify('Exito!',"success","Eventos agregados correctamente");
		}
	});
}

//agrego un nuevo comentario
$("#btnNewComentario").click(function () {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_addcomentario') }}",
		data: {
			'comentario': $("#comentario_respuesta").val(), //form de productos
			'id': '{{itinerario.id}}' //form de productos
		},
		dataType: "json",
		//async: true,
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		$("#comentario_respuesta").summernote("reset");
		$("#listaComentarios").text("");
		$("#listaComentarios").append(respuesta.strPopup);
		$("#tableBitacora").text("");
		$("#tableBitacora").html(respuesta.htmlBitacora);
		getDataTable('tablaBitacora');
		refreshTimeline();
		$('#modalLoad').modal('hide');
		$('#modalNewComentario').modal('hide'); //oculto el dialog.
		newNotify('Exito!',"success","Comentario agregado correctamente");
		
		//alert("Comentario ingresado con exito!");
	});

});

$('#btnEventoAdd').click(function () {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_addevento') }}",
		data: {
			idItinerario: "{{ itinerario.id }}",
			formEvento: $("#formEvento").serialize(), // formulario con los datos
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		if (respuesta.html !== 'error') {
			$("#tablaEventos").text("");
			$("#tablaEventos").html(respuesta.html);
			window.location.reload(); // no me queda otra que recargar porque sino tengo que ahcer codigo para actualizar los eventos del form y no vale la pena
		}

	});
});

$('#btnDetencion').click(function () {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_editdetencion') }}",
		data: {
			idItinerario: "{{ itinerario.id }}",
			formDetencion: $("#formDetencion").serialize(), // formulario con los datos
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		if (respuesta.data !== 'error') {
			$("#eventos_detencion_tiempo").val(respuesta.data);
			$("#label_eventos_detencion_tiempo").html(respuesta.data_str);
			newNotify('Exito!',"success","El valor se modificó correctamente");
			
			//actualizar valor de textbox y cambiar valor en label

		} else {
			newNotify('Error!',"error","Error al modificar el valor");		
		}
		$('#modalLoad').modal('hide');

	});
});


function deleteEvento(id) {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_deleteevento') }}",
		data: {
			idItinerario: "{{ itinerario.id }}",
			id: id
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		if (respuesta.data !== 'error') {
			newNotify('Exito!',"success","El evento se eliminó correctamente");
			
			window.location.reload();
		} else {
			newNotify('Error!',"error","Error al eliminar el evento");
			
		}
		$('#modalLoad').modal('hide');

	});
}

function chageEstadoEvento(id, estado) {
	var r = false;
	
	if(estado == 1){
		 r = confirm('Esta seguro de desactivar el evento?');
		
	}else {
		 r = confirm('Esta seguro de activar el evento? ');

	}
	if (r == true) {
		var req = $.ajax({
			type: 'POST',
			url: "{{ path('itinerario_evento_estado') }}",
			data: {
				idItinerario: "{{ itinerario.id }}",
				id: id
			},
			dataType: "json",
			beforeSend: function () {
				$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
			},
		});
		req.done(function (respuesta) {
			if (respuesta.data !== 'error') {
				newNotify('Exito!',"success","El evento se modificó correctamente");	
				if(respuesta.activo){
					$('#span_' + id).removeClass('label-default').addClass('label-success');
				}else{
					$('#span_' + id).removeClass('label-success').addClass('label-default');
				}
				//window.location.reload();
			} else {
				newNotify('Error!',"error","Error al eliminar el evento");
				
			}
			$('#modalLoad').modal('hide');

		});
	}
}

function resendEvento(id) {
	var req = $.ajax({
		type: 'POST',
		url: "{{ path('itinerario_resendevento') }}",
		data: {
			idItinerario: id,
		},
		dataType: "json",
		beforeSend: function () {
			$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	req.done(function (respuesta) {
		if (respuesta.data !== 'error') {
			newNotify('Exito!',"success","Los eventos se han reconfigurado correctamente");
			window.location.reload();
		} else {
			newNotify('Error!',"error","Error al reconfigurar los eventos");
			
		}
		$('#modalLoad').modal('hide');

	});
}


$("#btnChangeEstado").click(function () {
	$("#modalEstado").modal({ backdrop: 'static', keyboard: false });
	refreshTimeline();
});

id = 0;



	$("#servicio_servicio").on('select2:select', function (e) { //cargar poi seleccionado  en tabla
		var nombre = e.params.data.text;
		var indice = nombre.indexOf("-");
		var id = nombre.substring(0, indice - 1);

		var request = $.ajax({
			type: 'POST',
			url: "{{ path('itinerario_getchoferes') }}",
			data: {
			'id': id // id servicio      
			},
			dataType: "json",
			// async: true,
			beforeSend: function () {
			
			}
		});
		request.done(function (respuesta) {
			crearTablaServicio(id, nombre, indice);
			cargarSelectChofer(respuesta.id,respuesta.choferes);
			
		});

	});
	$("#servicio_servicio").on('select2:unselect', function (e) { //borrar poi seleccionado  en tabla
		var nombre = e.params.data.text;
		var indice = nombre.indexOf("-");
		var id = nombre.substring(0, indice - 1);

		$('#' + id).remove();

	});

function formatServicios() {
    var servicios = [];
    $('#tabla_servicio tr').each(function () {
        id = $(this).attr('id');
        if (id !== undefined) {
            var servicio = {
                id: this.id, 
                nro_contenedor: $('#nro_contenedor_' + id).val(), 
                bill: $('#bill_' + id).val(),
                patente_acoplado: $('#patente_acoplado_' + id).val(),
				chofer: $('#chofer_'+id).val()
            };
            servicios.push(servicio);
        }

    });

    return JSON.stringify(servicios);
}



