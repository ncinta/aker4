
$(document).ready(function () {
    datatableItinerarios();

});

function procesar(codigo, empresa, servicio, inicio, fin, referencias, nota) {
    return new Promise((resolve, reject) => {
        var req = $.ajax({
            type: 'POST',
            url: "{{ path('itinerario_import_procesar') }}",
            data: {
                'codigo': codigo,
                'empresa': empresa,
                'servicio': servicio,
                'fecha_inicio': inicio,
                'fecha_fin': fin,
                'referencias': referencias,
                'nota': nota
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
            dataType: "json",
            //async: true,
            beforeSend: function () {
            },
        });
        req.done(function (respuesta) {
            if (respuesta.status) {
                $('#td_' + respuesta.codigo).html('<div class="btn-success badge">OK</div>');
            } else {
                $('#td_' + respuesta.codigo).html('<div class="btn-default badge">Error</div>');
            }

        });
    });
};



function datatableItinerarios() {
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return column === 3 || column === 4 || column === 5 ?
                        data.replace(/\s/g, '') :
                        data.replace(/(&nbsp;|<([^>]+)>)/ig, "");
                }
            },
            columns: [1, 2, 3, 4, 5, 6, 7, 8]
        }
    };
    oTable = $('#tableItinerarios').DataTable({
        dom: 'Bfrtip',
        paging: false,
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'excel'
            }),
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            }
        ],
        language: {
            processing: "Busqueda en curso...",
            search: "Buscar&nbsp;:",
            lengthMenu: "_MENU_ Elementos",            
            info: "de  _START_ a  _END_ de  _TOTAL_ elementos",
            zeroRecords: "No hay registros",
            emptyTable: "Tabla vacía",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Próxima",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": Orden Ascendente",
                sortDescending: ": Orden Descendente"
            }
        }
    });


}

