globalId = 0;
function reorderPopup() {
    orden = 0;
    $('#tabla_referencia tr').each(function () {
        $('#orden_' + $(this).attr('id')).text(orden); // table row ID )
        orden = orden + 1;
        var rowCount = $('#tabla_referencia tr').length - 1;
        if (rowCount === 0) {
            orden = 0;
        }
    });

}

function setClockPopup() {
    $('#tabla_referencia tr').each(function (index_tr, tr) {
        id = $(this).attr("id");
        $(tr).children('td').each((index_td, td) => {
            if (index_tr === 1) {
                $("#arribo_" + id).text($("#itinerario_fechaInicio").val());
            } else {
                $("#arribo_" + id).text('Arribo');
            }
        });

    });
    idLast = $('#tabla_referencia tr:last').attr('id');;
    $("#arribo_" + idLast).text($("#itinerario_fechaFin").val());
}
$("#itinerario_template").on('click', function () {
    if ($('#itinerario_template').is(":checked")) {
        $('#div_template_nombre').show();

    } else {
        $('#div_template_nombre').hide();
    }
});

$(function () {
    $('#servicio_servicio').select2({ width: "100%" });
    $('#referencia_referencia').select2({ width: "100%" });
    $('#contactos_contacto').select2({ width: "100%" });
    $('#usuario_usuario').select2({ width: "100%" });
    $('#custodio_custodio').select2({ width: "100%" });
    $('#itinerario_nota').summernote({ toolbar: false, height: 270 });
    $('#div_template_nombre').hide();


    $("#referencia_referencia").on('select2:select', function (e) { //cargar poi seleccionado  en tabla
        var id = e.params.data.id;
        var nombre = e.params.data.text;
        var rowCount = $('#tabla_referencia tr').length - 1;
        crearTablaReferencia(id,nombre,rowCount);
        setClockPopup();

    });

    $("#referencia_referencia").on('select2:unselect', function (e) { //borrar poi seleccionado  en tabla
        var id = e.params.data.id;
        $('#' + id).remove();
        reorderPopup();
        setClockPopup();
    });

    $("#servicio_servicio").on('select2:select', function (e) { //cargar poi seleccionado  en tabla
        var nombre = e.params.data.text;
        var indice = nombre.indexOf("-");
        var id = nombre.substring(0, indice - 1);

    var request = $.ajax({
        type: 'POST',
        url: "{{ path('itinerario_getchoferes') }}",
        data: {
           'id': id // id servicio      
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
        
        }
    });
    request.done(function (respuesta) {
        crearTablaServicio(id, nombre, indice);
        cargarSelectChofer(respuesta.id,respuesta.choferes);
        
    });


    });


    $("#servicio_servicio").on('select2:unselect', function (e) { //borrar poi seleccionado  en tabla
        var nombre = e.params.data.text;
        var indice = nombre.indexOf("-");
        var id = nombre.substring(0, indice - 1);

        $('#' + id).remove();

    });


    $('#form').smartWizard({
        theme: 'arrows', // theme for the wizard, related css need to include for other than default theme
        justified: false, // Nav menu justification. true/false
        autoAdjustHeight: false, // Automatically adjust content height
        backButtonSupport: true, // Enable the back button support
        enableUrlHash: true, // Enable selection of the step based on url hash
        lang: { // Language variables for button
            next: 'Siguiente',
            previous: 'Volver'
        }
    });


    $("#itinerario_fechaInicio").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });
    $("#itinerario_fechaFin").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });

    $("#horario_arribo").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });
    $("#horario_partida").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });



});

function setFecha(id) {
    $("#modalHorario").modal();
    globalId= id;

}


$('#btnHorarioAdd').click(function () {
    $("#arribo_" + globalId).text($("#horario_arribo").val());
    $("#salida_" + globalId).text($("#horario_partida").val());
    $("#modalHorario").modal('hide');
});


function formatPois() {
    var pois = [];
    $('#tabla_referencia tr').each(function () {
        id = $(this).attr('id');
        if (id !== undefined) {
            var poi = {
                id: this.id, orden: $('#orden_' + id).text(), nombre: $('#nombre_' + id).text(),
                arribo: $('#arribo_' + id).text(), salida: $('#salida_' + id).text(), stopType: $('#stopType_' + id).val()
            };
            pois.push(poi);
        
        }

    });

    return JSON.stringify(pois);
}
function formatServicios() {
    var servicios = [];
    $('#tabla_servicio tr').each(function () {
        id = $(this).attr('id');
        if (id !== undefined) {
            var servicio = {
                id: this.id,
                nro_contenedor: $('#nro_contenedor_' + id).val(),
                bill: $('#bill_' + id).val(),
                patente_acoplado: $('#patente_acoplado_' + id).val(),
                chofer: $('#chofer_' + id).val()
            };
            servicios.push(servicio);
        }

    });

    return JSON.stringify(servicios);
}

$('#btnNewItinerario').click(function () {
    pois = formatPois();
    servicios = formatServicios();
    modoIngreso = 0;
    {% if data is defined and template is defined %}
        modoIngreso = 2 // es template
    {% endif%}

    var request = $.ajax({
        type: 'POST',
        url: "{{ path('itinerario_create',{idOrg: organizacion.id}) }}",
        data: {
            'formItinerario': $("#formItinerario").serialize(), // formulario con los datos
            'formCustodio': $("#formCustodio").serialize(), // formulario con los datos
            'selectServ': $("#formServicio").serialize(), // formulario con los datos
            'formContacto': $("#formContacto").serialize(), // formulario con los datos
            'formEvento': $("#formEvento").serialize(), // formulario con los datos
            'formUsuario': $("#formUsuario").serialize(), // formulario con los datos
            'formReferencia': pois,
            'formServicio': servicios,
            'modoIngreso' : modoIngreso,
            'create_template': $('#itinerario_template').is(":checked"),
            'template_nombre': $('#itinerario_template_nombre').val()
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
        //    $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.status == 'ok') {
            $('#modalLoad').modal('hide');
            window.location.href = Routing.generate('itinerario_show', { 'id': respuesta.id });

        }

    });
});

$("#form").on("leaveStep", function (e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
    var inicio = $('#itinerario_fechaInicio').data("DateTimePicker").date();
    var fin = $('#itinerario_fechaFin').data("DateTimePicker").date();


    if ($("#itinerario_codigo").val().length == 0 || inicio == null || fin == null) {
        newNotify('Error !!!', 'error', 'Todos los campos son obligatorios');
        return false;
    }

    if (nextStepIndex == 5 || currentStepIndex == 5) {
        $("#resumen_codigo").text("");
        $("#resumen_inicio").text("");
        $("#resumen_fin").text("");
        $("#resumen_cliente").text("");
        $("#resumen_logistica").text("");
        $("#resumen_logistica").text("");
        $("#resumen_servicios").text("");
        $("#resumen_custodios").text("");
        $("#resumen_puntos").text("");
        $("#resumen_contactos").text("");
        $("#resumen_usuarios").text("");

        $("#resumen_codigo").append(" " + $("#itinerario_codigo").val());
        $("#resumen_inicio").append(" " + $("#itinerario_fechaInicio").val());
        $("#resumen_fin").append(" " + $("#itinerario_fechaFin").val());
        $("#resumen_cliente").append(" " + $("#itinerario_empresa option:selected").text());
        $("#resumen_logistica").append(" " + $("#itinerario_logistica option:selected").text());

        var servicios = $('#servicio_servicio').select2("data");
        for (var i = 0; i <= servicios.length - 1; i++) {
            $("#resumen_servicios").append(servicios[i].text + ", ");
        }

        var custodios = $('#custodio_custodio').select2("data");
        for (var i = 0; i <= custodios.length - 1; i++) {
            $("#resumen_custodios").append(custodios[i].text + ", ");
        }

        var referencias = $('#referencia_referencia').select2("data");
        for (var i = 0; i <= referencias.length - 1; i++) {
            $("#resumen_puntos").append(referencias[i].text + ", ");
        }
        var contactos = $('#contactos_contacto').select2("data");
        for (var i = 0; i <= contactos.length - 1; i++) {
            $("#resumen_contactos").append(contactos[i].text + ", ");
        }
        var usuarios = $('#usuario_usuario').select2("data");
        for (var i = 0; i <= usuarios.length - 1; i++) {
            $("#resumen_usuarios").append(usuarios[i].text + ", ");
        }
    }
    if (nextStepIndex == 1 && currentStepIndex == 0) {
        if($('#itinerario_fechaInicio').val() !== '' && $('#itinerario_fechaFin').val() !== ''){
            setClockPopup();
        }
    }

});
