let codigo_externo = "";
let patente = "";
let key = "";
$(document).ready(function () {
    $("#btnFin").hide();
    clearInputs();
    $("#referencia_referencia").select2(
        { width: '100%' }
    );
    $("#servicio_servicio").select2(
        { width: '100%' }
    );
    {% for key, dato in data %}
    document.getElementById("{{key}}").checked = true;
    {% endfor %}


});

$('#referencia_referencia').on('change', function(e) {
    if($('#referencia_referencia').val() != ""){

        var req = $.ajax({
            type: "POST",
        url: "{{ path('itinerario_import_check_provincia') }}",
        data: {
            formReferencia: $("#formReferencia").serialize(),
            
        },
        dataType: "json",
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
    });
    req.done(function (respuesta) {
        if(!respuesta.check.provincia){ // si no tiene provincia le damos la opcion de update
            $("#labFaltanDatos").show();
            $("#provinciaReferencia").show();
        }else{
            $("#labFaltanDatos").hide();
            $("#provinciaReferencia").hide();

        }
        if(!respuesta.check.ciudad){// si no tiene ciudad le damos la opcion de update
            $("#labFaltanDatos").show();
            $("#ciudadReferencia").show();
        }else{
            $("#labFaltanDatos").hide();
            $("#ciudadReferencia").hide();

        }
        $("#modalLoad").modal('hide');
    });
    }
    
});
    function accion(codigo) {
        if ($('#' + codigo).is(':checked')) {
            $(this).prop('checked', true);
        } else {
            $(this).prop('checked', false);
        }



    }

    function updateCodExtReferencia(codigo, nombre, lat, long, direccion, city, state){
        clearInputs(); 
        codigo_externo = codigo;
        $("#referencia_nombre").val(nombre);
        $("#referencia_codigoExterno").val(codigo);
        $("#referencia_latitud").val(lat);
        $("#referencia_longitud").val(long);
        $("#referencia_direccion").val(direccion);
        $("#referencia_radio").val(100);
        $("#referencia_ref_ciudad").val(city);
        $("#referencia_ref_provincia").val(state).trigger("change");;
        $("#referencia_provincia").val(state).trigger("change");;
        $("#referencia_ciudad").val(city);
        $("#modalReferencia").modal("show");
    }

    function clearInputs(){
      
        $("#ciudadReferencia").hide();
        $("#provinciaReferencia").hide();
        $("#labFaltanDatos").hide();
        $("#referencia_ciudad").val("");
        $("#referencia_ref_ciudad").val("");
        $("#referencia_provincia").val("");
        $("#referencia_ref_provincia").val("");
        $("#referencia_referencia").val("").trigger("change");;
        $("#servicio_servicio").val("").trigger("change");;
    
    }
    $("#btnReferenciaAdd").click(function () {
        var req = $.ajax({
            type: "POST",
            url: "{{ path('itinerario_import_update_referencia') }}",
            data: {
                formReferencia: $("#formReferencia").serialize(),
                codigo_externo: codigo_externo,
            },
            dataType: "json",
            beforeSend: function () {
                $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
            },
        });
        req.done(function (respuesta) {
            newNotify('Exito!',"success","LA referencia se asignó correctamente");
                $(".accion_"+codigo_externo).text(''); //ocultamos boton
                $(".accion_"+codigo_externo).html('<i title="la referencia existe" class="glyphicon glyphicon-ok"></i>'); //ocultamos boton

              
            
            $("#modalLoad").modal("hide");
            $("#modalReferencia").modal("hide");
            clearInputs();
        });
    }); 

        
    $("#btnReferenciaNew").click(function () {
        var req = $.ajax({
            type: "POST",
            url: "{{ path('itinerario_import_create_referencia') }}",
            data: {
                formreferencia: $("#formReferenciaNew").serialize(),
            },
            dataType: "json",
            beforeSend: function () {
                $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
            },
        });
        req.done(function (respuesta) {                 
            $("#modalLoad").modal("hide");
            if(respuesta.status){
                $(".accion_"+codigo_externo).text(''); //ocultamos boton
                $(".accion_"+codigo_externo).html('<i title="la referencia existe" class="glyphicon glyphicon-ok"></i>'); //ocultamos boton
                $("#modalReferencia").modal("hide");
                $("#modalNewReferencia").modal("hide");
                newNotify('Exito!',"success","La referencia se creó correctamente");
               
            }else{
                newNotify('Error!',"error","La referencia con ese código ya existe");
           
            }   
            clearInputs(); 
        });
    });
    
    function updatePatente(patente_servicio){
        patente = patente_servicio;
        $("#servicio_nombre").val(patente);
        $("#servicio_patente").val(patente);
        $("#modalServicio").modal("show");
    }


    $("#btnServicioAdd").click(function () {
        var req = $.ajax({
            type: "POST",
            url: "{{ path('itinerario_import_update_servicio') }}",
            data: {
                formServicio: $("#formServicio").serialize(),
                patente: patente,
            },
            dataType: "json",
            beforeSend: function () {
                $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
            },
        });
        req.done(function (respuesta) {    
            if(respuesta.status){
                newNotify('Exito!',"success","El servicio se asignó correctamente");
                     
                $(".accion_servicio_"+patente).text(''); //ocultamos boton
                $(".accion_servicio_"+patente).html('<i title="la patente existe" class="glyphicon glyphicon-ok"></i>'); //ocultamos boton
            } else{
                newNotify('Error!',"error","Error al asignar la patente al servicio");
                     
            }     
                
            
            $("#modalLoad").modal("hide");
            $("#modalServicio").modal("hide");
            clearInputs();
        });
    });

        $("#btnFin").click(function () {
         
        var req = $.ajax({
            type: "POST",
            url: "{{ path('itinerario_import_chr_terminar') }}",
            dataType: "json",
            beforeSend: function () {
                $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
            },
        });
        req.done(function (respuesta) {          
            route = Routing.generate('itinerario_list', { idOrg: respuesta.id });   
            window.location.replace(route);
            
        });
    });
    
    $("#btnServicioNew").click(function () {
        var req = $.ajax({
            type: "POST",
            url: "{{ path('itinerario_import_create_servicio') }}",
            data: {
                formServicio: $("#formServicioNew").serialize(),
            },
            dataType: "json",
            beforeSend: function () {
                $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
            },
        });
        req.done(function (respuesta) {                 
            $("#modalLoad").modal("hide");
            if(respuesta.status){

                $(".accion_servicio_"+patente).text(''); //ocultamos boton
                $(".accion_servicio_"+patente).html('<i title="la patente existe" class="glyphicon glyphicon-ok"></i>'); //ocultamos boton
                $("#modalServicio").modal("hide");
                $("#modalNewServicio").modal("hide");
                newNotify('Exito!',"success","El servicio se creó correctamente");
           
            }else{
                newNotify('Error!',"error","El equipo con esa patente ya existe");                
            }   
            
        });
    });
    
	$('#btnImport').click(function () {
        $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
    
        {% for key,dato in data|sort %} //que los recorra de manera ordenada
        if($('#'+'{{key}}').is(':checked')) { // recorro todos los itinerarios y verifico que este check, si es asi lo mando al back
            procesar('{{key}}');
        }          
        {% endfor %}
        $("#modalLoad").modal('hide');
        $("#btnImport").hide();
        $("#btnFin").show();

    });
    
    function procesar(codigo) {
        return new Promise((resolve, reject) => {
            var req = $.ajax({
                type: 'POST',
                url: "{{ path('itinerario_import_chr_procesar') }}",
                data: {
                    'codigo': codigo,
                },
                success: function (data) {
                    resolve(data)
                },
                error: function (error) {
                    reject(error)
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                    $('#td_' + codigo).html('<div class="btn-default badge">Espere....</div>');
                },
            });
            req.done(function (respuesta) {
                if (respuesta.status) {
                    setEvento(respuesta.id);    //agrega los eventos
                }else{
                    $('#td_' + respuesta.codigo).html('<div class="btn-danger badge">Error</div>');
                }
                
            });
        });
    };
        
    
    function setEvento(id) {
        return new Promise((resolve, reject) => {
            var req = $.ajax({
                type: 'POST',
                url: "{{ path('itinerario_import_cargareventos') }}",
                data: {
                    'id': id,
                },
                success: function (data) {
                    resolve(data)
                },
                error: function (error) {
                    reject(error)
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
            });
            req.done(function (respuesta) {
                if (respuesta.status) {
                    $('#td_' + respuesta.codigo).html('<div class="btn-success badge">OK</div>');
                } else {
                    $('#td_' + respuesta.codigo).html('<div class="btn-danger badge">Error</div>');
                }
                
            });
        });
    };
    
    