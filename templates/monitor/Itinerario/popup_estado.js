var itinerarios = [];
var idItinerario = 0;
var fechaNow = "{{ 'now'|date('d/m/Y H:i', app.user.timeZone) }}";
var fechaInicio = null;
var fechaFin = null;

$(document).ready(function () {    
  
    $('#nota').summernote({
        height: 100,
        toolbar: false,
    });
    $("#estado").val(0);

    $("#historico_fecha_desde").datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $("#historico_fecha_hasta").datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $("#fecha").datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: 'es',
        widgetPositioning: {
            horizontal: 'right',
            vertical: 'bottom'
        }
    });
   

});

function showBtnChangeEstado(nombre) {
    var count = 0;
    cargarDivPopup(nombre);
    for (var i = 0; i < document.formCheck.elements.length; i++) {
        if (document.formCheck.elements[i].checked) {
            count++;
        }
    }
    if (count > 0) {
        $("#btnChangeEstado").show();
    } else {
        $("#btnChangeEstado").hide();
    }
}

function cargarDivPopup(nombre, estado) {
    var index = window.itinerarios.indexOf(nombre);
    if (index !== -1) {
        window.itinerarios.splice(index, 1);
    }
    else {
        window.itinerarios = [];
        window.itinerarios.push(nombre);
    }
    $("#estado_itinerario_nombre").text(window.itinerarios.toString());
    if (estado == 0) {
        $("#estado_old").text("Estado Actual : Nuevo");
    } else if (estado == 2) {
        $("#estado_old").text("Estado Actual : Auditoría");
    } else if (estado == 1) {
        $("#estado_old").text("Estado Actual : En Curso");
    } else if (estado == 9) {
        $("#estado_old").text("Estado Actual : Cerrado");
    } else {
        //por si hay otro estado
    }
}

function changeEstado(nombre, id, estado, fechaInicio, fechaFin) {
    cargarDivPopup(nombre, estado);
    document.id = id;
   
    document.fechaInicio = fechaInicio;
    document.fechaFin = fechaFin;
    $("#formCheck_" + id).prop('checked', true);
    $("#modalEstado").modal({ backdrop: 'static', keyboard: false });
    setupVistasFechas($("#estado").val());
}

$("#btnChangeEstado").click(function () {
    $("#modalEstado").modal({ backdrop: 'static', keyboard: false });
    document.getElementById("formEstado").reset();
});

$("#btnEstadoModificar").click(function () {
    var form_check = null;
    if ($('#formCheck').length > 0) { //para la lista de itinerarios mando los checks
        form_check = $("#formCheck").serialize()
        //alert( $("#formCheck").serialize());
    } else { //para el mapa solo mando el id
        form_check = document.id;
    }
  
    var request = $.ajax({
        type: "POST",
        url: "{{ path('itinerario_editestado') }}",
        data: {
            estado: $("#estado").val(),
            fecha: $("#fecha").val(),
            nota: $("#nota").summernote('code'),
            vista: $("#vista").val(),
            formCheck: form_check, // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });

        },
    });
    request.done(function (respuesta) {
        if ($("#vista").val() === 'list') {
            $('#tablaItinerarios').text("");
            $('#tablaItinerarios').append(respuesta.html);
            dataT();

            document.getElementById("formCheck").reset();
            window.itinerarios = [];
            
        } else if ($("#vista").val() === 'mapa') {
            $('#labelEstado_' + respuesta.idItinerario).text("");
            $('#labelEstado_' + respuesta.idItinerario).append(respuesta.html);
        } else {
            window.location.reload(); // es la vista show. necesito refrescar pagina porque son muchos los cambios de datos que hay
        }
       
        $('#nota').summernote('reset');
        
        $("#modalLoad").modal('hide');
        $("#modalEstado").modal('toggle');
        document.fechaInicio = null;
        document.fechaFin= null;

    });
});

$("#btnCancelar").click(function () {
    document.getElementById("formCheck").reset();
    $("#btnChangeEstado").hide();
    window.itinerarios = [];
});

$("#estado").change(function () {
    setupVistasFechas(this.value);
    var div = document.getElementById('implantado-div');
    if (this.value === '3') {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
});

function setupVistasFechas(estado) {
   
    if (document.fechaInicio == null || document.fechaInicio == '') {
        document.fechaInicio = fechaNow;
    } else {
        $("#fecha").val(document.fechaInicio);
    }
    if (estado == 1) { //en curso
        //$("#labelFecha").html('<label class="label label-info h6">Fecha en que se pondrá en curso el itinerario</label>');
        $("#labelFecha").html('Seleccione la fecha en la que se <b>inicia</b> el itinerario');
        $("#divFecha").show();

    } else if (estado == 2) { //auditoria
        $("#divFecha").hide();

    } else if (estado == 9) { //cerrado
        if (document.fechaFin == null || document.fechaFin == '') {
            document.fechaFin = fechaNow;
        } else {
            $("#fecha").val(document.fechaFin);
        }
        //$("#labelFecha").html('<label class="label label-default h6">Fecha en la que terminará el itinerario</label>');
        $("#labelFecha").html('Seleccione la fecha en la que se <b>cierra</b> el itinerario');
        $("#divFecha").show();
    } else {
        $("#divFecha").hide(); //eliminado

    }
}

function dataT() {
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return column === 3 || column === 4 || column === 5 ?
                        data.replace(/\s/g, '') :
                        data.replace(/(&nbsp;|<([^>]+)>)/ig, "");
                }
            },
            columns: [1, 2, 3, 4, 5, 6, 7, 8]
        }
    };
    oTable = $('#tableItinerarios').DataTable({
        dom: 'Bfrtip',
        paging: false,
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'excel'
            }),
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            }
        ],
        language: {
            processing: "Busqueda en curso...",
            search: "Buscar&nbsp;:",
            lengthMenu: "_MENU_ Elementos",
            info: "de  _START_ a  _END_ de  _TOTAL_ elementos",
            zeroRecords: "No hay registros",
            emptyTable: "Tabla vacía",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "Próxima",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": Orden Ascendente",
                sortDescending: ": Orden Descendente"
            }
        }
    });
}