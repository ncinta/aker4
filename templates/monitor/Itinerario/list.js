
$(document).ready(function () {
    dataT();
    document.getElementById("formCheck").reset();
    $("#historico_fecha_desde").datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $("#historico_fecha_hasta").datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $("#btnChangeEstado").hide();
    $('#historico_estado').select2();
    $('#historico_servicio').select2();
    $('#historico_tipoServicio').select2();
    $('#historico_transporte').select2();
    $('#historico_satelital').select2();
    $('#historico_empresa').select2();
});
$('#consular').click(function () {
    var req = $.ajax({
        type: 'GET',
        url: "{{ path('itinerario_historico') }}",
        data: {
            'formHistIt': $("#formHistItinerario").serialize(), //form de productos
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
    });
    req.done(function (respuesta) {
        $("#tablaItinerarios").text("");
        $("#tablaItinerarios").html(respuesta.html);
        $("#filtro").html(respuesta.filtro);
        dataT();
        $('#modalLoad').modal('hide');



    });
});


function dataT() {
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return column === 3 || column === 4 || column === 5 ?
                        data.replace(/\s/g, '') :
                        data.replace(/(&nbsp;|<([^>]+)>)/ig, "");
                }
            },
            columns: [1, 2, 3, 4, 5, 6, 7, 8]
        }
    };
    oTable = $('#tableItinerarios').DataTable({
        dom: 'Bfrtip',
        paging: false,
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'excel'
            }),
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            }
        ],
        language: {
            processing: "Busqueda en curso...",
            search: "Buscar&nbsp;:",
            lengthMenu: "_MENU_ Elementos",
            info: "de  _START_ a  _END_ de  _TOTAL_ elementos",
            zeroRecords: "No hay registros",
            emptyTable: "Tabla vacía",
            paginate: {
                first: "Primera",
                previous: "Anterior",
                next: "prosima",
                last: "Ultima"
            },
            aria: {
                sortAscending: ": Orden Ascendente",
                sortDescending: ": Orden Descendente"
            }
        }
    });


}

//cuando se checkea todos.
function checkAll() {
    if ($('#check_all').is(":checked")) {
        $(":checkbox").prop("checked", "checked");
        showBtnChangeEstado('')
    }else{
        $(":checkbox").prop("checked", false);
        showBtnChangeEstado('')
    }
}

