$(document).ready(function () {
    setInterval(function () {
        refreshTimeline();
    }, 50000000); // refresca timeline cada 10 segundos
})

function refreshTimeline() {

    var request = $.ajax({
        type: "GET",
        url: "{{ path('itinerario_timeline') }}",
        data: {
            idItinerario: "{{ itinerario.id }}",
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
           // $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
    });
    request.done(function (respuesta) {
        $("#timeline").text("");
        $("#timeline").html(respuesta.html);
    });
    //$('#modalLoad').modal('hide');
}