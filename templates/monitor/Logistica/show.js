
$(document).ready(function () {
    $("#forContacto").removeClass("disable-div");  //está en el layout
    getDataTable('tablaUsuario'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaServicio'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaContacto'); //funcion que se encuentra en el layout_Aker
    getDataTable('tableItinerarios'); //funcion que se encuentra en el layout_Aker
    $('#servicio_servicio').select2({ 'width': '100%', placeholder: "Escriba el nombre de sus servicios..." });
    $("#historico_fecha_desde").datetimepicker({ format: 'DD/MM/YYYY', locale: 'es' });
    $('#usuario_timeZone option[value="{{logistica.organizacion.timeZone}}"]').attr("selected", true);
    setInterval(getDataMap, 30000);


});


$('#btnCancelar').click(function () { // alert('aca');
    document.getElementById("formContacto").reset();
    $('#btnContactoAdd').text('Agregar');
});


$('#btnContactoAdd').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('logistica_contacto') }}",
        data: {
            'idLogistica': '{{ logistica.id }}',
            'form': $("#formContacto").serialize(), // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.existe === false) {
            $("#tableContactos").text("");
            $("#tableContactos").html(respuesta.html);
            $("#contacto_tipoNotificaciones").val(null).trigger("change");
            getDataTable('tablaContacto');

            $('#modalLoad').modal('hide');
            $('#modalContacto').modal('toggle');

            document.getElementById("formContacto").reset();

            newNotify('Exito!',"success","Contactos agregados correctamente");
            if (respuesta.notifApp) {
                if (respuesta.usuario === null) {
                    $("#forContacto").addClass("disable-div");  //está en el layoutf
                    $('#usuario_nombre').val(respuesta.nombre);
                    $('#usuario_email').val(respuesta.email);
                    $('#usuario_telefono').val(respuesta.celular);
                    $('#para_contactos_app').removeAttr('hidden');
                    $("#modalNewUsuario").modal("show");
                } else {
                    newNotify('Exito!',"warning","El usuario ya existe, se asocia contacto a usuario");
                }
            }else{
                document.getElementById("formContacto").reset();
            }
        } else {
            $('#modalLoad').modal('hide');
            newNotify('Error!',"error","El contacto con ese numero telefónico ya existe");
        }
        $('#modalLoad').modal('hide');
    });


});

$('#btnServicioAdd').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('logistica_addservicio') }}",
        data: {
            'idLogistica': '{{ logistica.id }}',
            'form': $("#formServicio").serialize(), // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {

        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        getDataTable('tablaServicio');
        $('#modalServicio').modal('toggle');
        $("#servicio_servicio").val('').trigger('change');
        $('#modalLoad').modal('hide');
        newNotify('Exito!',"success","Servicios agregados correctamente");
     
    });

});


$('#btnServicioNew').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('logistica_newservicio') }}",
        data: {
            'idLogistica': '{{ logistica.id }}',
            'form': $("#formServicioNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {

        $('#modalServicio').modal('toggle');
        $("#modalNewServicio").modal("toggle");
        $("#servicio_servicio").val('').trigger('change')
        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        document.getElementById("formServicioNew").reset();
        getDataTable('tablaServicio');
        $('#modalLoad').modal('hide');

    });

});


var globalIdServicio = 0;

function deleteServicio(id) {
    $('#modalDeleteServicio').modal('show');
    globalIdServicio = id;

}
$('#btnEliminarServicio').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('logistica_deleteservicio') }}",
        data: {
            'id': globalIdServicio,
            'idLogistica': '{{ logistica.id }}'
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {

        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        getDataTable('tablaServicio');
        $('#modalDeleteServicio').modal('toggle');
        $('#modalLoad').modal('hide');
        newNotify('Exito!',"success","Servicios eliminados correctamente");
        
    });
    globalIdServicio = 0;
});

$('#btnUsuarioNew').click(function () {
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('logistica_newusuario') }}",
        data: {
            'idLogistica': '{{ logistica.id }}',
            'form': $("#formUsuarioNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.html) {
            $("#modalNewUsuario").modal("toggle");
            $("#tableUsuarios").text("");
            $("#tableUsuarios").html(respuesta.html);
            newNotify('Exito!',"success","El usuario se creó correctamente");
            

        } else {
            newNotify('Error!',"error",respuesta.error);
        }
        getDataTable('tablaUsuario');
        $('#modalLoad').modal('hide');
        $('#para_contactos_app').hide();

        document.getElementById("formUsuarioNew").reset();

    });

});

$(".btnCancelar").click(function () {
    document.getElementById("formUsuarioNew").reset();
    document.getElementById("formContacto").reset();
});