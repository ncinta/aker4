function crearReferenciaStep1(lat, lon) {
    alert('No se puede utilizar esta función por ahora. Gracias.');
    //if ({{ mapa.getMapName() }}.getZoom() > 15) {
    //    if (lat != null && lon != null && lat != 0 && lon != 0 ) {        
    //        {{ mapa.getMapName() }}.setCenter(new google.maps.LatLng(lat,lon));
    //      gotoCenter(lat, lon, true);
    //    }
    //    crearReferencia();
    //} else {
    //    alert('Esta muy lejos del suelo en el mapa. Acerquese un poco para poder crear mejor una referencia');
    //}    
}

idEvHist = 0;
function showModalComentar(show, id) {        
    document.idEvHist = id;
    if (show == true) {        
        console.log(document.getElementById("titulo"+id).innerText);

        $("#commentTitle").html(document.getElementById("titulo"+id).innerText);

        //debo obtener el evento desde el back
        var request = $.ajax({
            type: 'GET',
            url: "{{ path('app_eventotemporal_obtener') }}",
            data: { 'id': id },
            dataType: "json",
            beforeSend: function () {
                $("#commentData").html('<em>Cargado datos. Espere..... </em>');
            },
        });
        request.done(function (respuesta) {
            $("#commentData").html(respuesta['evento']);
            //$("#commentProto").html('evento');    
        });
        
        document.getElementById("formComentario").reset();       
        $('#modalComentar').modal('show'); //muestro el dialog.
    } else {        
        $('#modalComentar').modal('hide'); //oculto el dialog.
    }    
}

function showModalFiltrar() {    
        $('#modalFiltrar').modal('show'); //muestro el dialog.
}

$("#btnComentarioAdd").click(function () {       
    if(document.req_servicios != null){
        document.req_servicios.abort();//abortamos si hay peticiones ya en curso
       }
       if(document.req_eventos != null){
        document.req_eventos.abort();//abortamos si hay peticiones ya en curso
       }
	var request = $.ajax({
		type: "POST",
		url: "{{ path('app_eventotemporal_comentar') }}",
		data: {
			'formComentario': $("#formComentario").serialize(), //form de comentario
            'id': document.idEvHist
		},
		dataType: "json",		
		beforeSend: function () {
			//$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		document.getElementById("formComentario").reset();
        showEventos();
		$("#modalComentar").modal("toggle");				
        newNotify('Exito!',"success","El comentario se registró correctamente");
        //$("#modalLoad").modal('hide');
	});   
});