$(document).ready(function () {
    $("#forContacto").removeClass("disable-div");  //está en el layout
    getDataTable('tablaUsuario'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaServicio'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaContacto'); //funcion que se encuentra en el layout_Aker
    $('#servicio_servicio').select2({ 'width': '100%',  placeholder: "Escriba el nombre de sus servicios..." });
    $("#historico_fecha_desde").datetimepicker({ format: 'DD/MM/YYYY', locale: 'es' });
    $('#usuario_timeZone option[value="{{transporte.organizacion.timeZone}}"]').attr("selected", true);
    setInterval(getDataMap, 30000);


});



$('#btnCancelar').click(function () { // alert('aca');
    document.getElementById("formContacto").reset();
    document.getElementById("formServicio").reset();
    $('#btnContactoAdd').text('Agregar');
});

$('#btnContactoAdd').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_contacto') }}",
        data: {
            'idTransporte': '{{ transporte.id }}',
            'form': $("#formContacto").serialize(), // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
         }
    });
    request.done(function (respuesta) {
        if (respuesta.existe === false) {
            $("#tableContactos").text("");
            $("#tableContactos").html(respuesta.html);
            $("#contacto_tipoNotificaciones").val(null).trigger("change");
            getDataTable('tablaContacto');

            $('#modalLoad').modal('hide');
            $('#modalContacto').modal('toggle');

            document.getElementById("formContacto").reset();

            newNotify('Exito!',"success","Contactos agregados correctamente");
            if (respuesta.notifApp) {
                if (respuesta.usuario === null) {
                    $("#forContacto").addClass("disable-div");  //está en el layoutf
                    $('#usuario_nombre').val(respuesta.nombre);
                    $('#usuario_email').val(respuesta.email);
                    $('#usuario_telefono').val(respuesta.celular);
                    $('#para_contactos_app').removeAttr('hidden');
                    $("#modalNewUsuario").modal("show");
                } else {
                    newNotify('Exito!',"warning","El usuario ya existe, se asocia contacto a usuario");
                }
            }else{
                document.getElementById("formContacto").reset();
            }
        } else {
            newNotify('Error!',"error","El contacto con ese numero telefónico ya existe");
        }
    });
  
});

$('#btnServicioAdd').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_addservicio') }}",
        data: {
            'idTransporte': '{{ transporte.id }}',
            'form': $("#formServicio").serialize(), // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
         }
    });
    request.done(function (respuesta) {

        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        getDataTable('tablaServicio');
        $('#modalServicio').modal('toggle');
        $("#servicio_servicio").val('').trigger('change')
        $('#modalLoad').modal('hide');
        newNotify('Exito!',"success","Servicios agregados correctamente");
       
    });

});


$('#btnServicioNew').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_newservicio') }}",
        data: {
            'idTransporte': '{{ transporte.id }}',
            'form': $("#formServicioNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
         }
    });
    request.done(function (respuesta) {

        $('#modalServicio').modal('toggle');
        $("#modalNewServicio").modal("toggle");
        $("#servicio_servicio").val('').trigger('change')
        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        document.getElementById("formServicioNew").reset();
        getDataTable('tablaServicio');
        getDataTable('tablaServicio');

    });

});

$('#btnChoferNew').click(function () { // alert('aca');
    if($("#chofer_documento").val() ===''){
        newNotify('Error!',"error"," <p>El campo <em>documento personal</em></p>"+ 'no puede estar vacío');
    }else{
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_newchofer') }}",
        data: {
            'idTransporte': '{{ transporte.id }}',
            'form': $("#formChoferNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
         }
    });
    request.done(function (respuesta) {

        $('#modalChoferNew').modal('toggle');
        $("#tableChoferes").text("");
        $("#tableChoferes").html(respuesta.html);
        document.getElementById("formChoferNew").reset();
        getDataTable('tableChofer');
       

    });
}
});


$('#btnUsuarioNew').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_newusuario') }}",
        data: {
            'idTransporte': '{{ transporte.id }}',
            'form': $("#formUsuarioNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () { 
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.html) {
            $("#modalNewUsuario").modal("toggle");
            $("#tableUsuarios").text("");
            $("#tableUsuarios").html(respuesta.html);
            newNotify('Exito!',"success","El usuario se creó correctamente");
          

        } else {
            newNotify('Error!',"error",respuesta.error);
            
        }
        getDataTable('tablaUsuario');
        $('#modalLoad').modal('hide');
        $('#para_contactos_app').hide();

        document.getElementById("formUsuarioNew").reset();

    });

});

var globalIdServicio = 0;

function deleteServicio(id) {
    $('#modalDeleteServicio').modal('show');
    globalIdServicio = id;

}
$('#btnEliminarServicio').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('transporte_deleteservicio') }}",
        data: {
            'id': globalIdServicio,
            'idTransporte': '{{ transporte.id }}'
        },
        dataType: "json",
        // async: true,
        beforeSend: function () { 
            $("#tableServicios").text("Espere....");
        }
    });
    request.done(function (respuesta) {

        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        getDataTable('tablaServicio');
        $('#modalDeleteServicio').modal('toggle');
        $('#modalLoad').modal('hide');
        newNotify('Exito!',"success","El servicio se eliminó correctamente");
        
    });
    globalIdServicio = 0;
});


