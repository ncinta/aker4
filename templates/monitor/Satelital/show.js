$(document).ready(function () {
    getDataTable('tablaServicio');
});

var globalIdServicio = 0;

function deleteServicio(id) {
    $('#modalDeleteServicio').modal('show');
    globalIdServicio = id;

}
$('#btnEliminarServicio').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('satelital_deleteservicio') }}",
        data: {
            'id': globalIdServicio,
            'idSatelital': '{{ satelital.id }}'
        },
        dataType: "json",
        // async: true,
        beforeSend: function () { 
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {

        $("#tableServicios").text("");
        $("#tableServicios").html(respuesta.html);
        getDataTable('tablaServicio');
        $('#modalLoad').modal('hide');
        $('#modalDeleteServicio').modal('toggle');
    });
    globalIdServicio = 0;
});