var idContacto = 0;
var idUsuario = 0;

$(document).ready(function () {
    $("#forContacto").removeClass("disable-div");  //está en el layout
    getDataTable('tableItinerarios'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaUsuario'); //funcion que se encuentra en el layout_Aker
    getDataTable('tablaContacto'); //funcion que se encuentra en el layout_Aker
 
    $("#historico_fecha_desde").datetimepicker({ format: 'DD/MM/YYYY', locale: 'es' });
    $('#usuario_timeZone option[value="{{empresa.organizacion.timeZone}}"]').attr("selected", true);


});


$('#btnCancelar').click(function () { // alert('aca');
    document.getElementById("formContacto").reset();
    $('#btnContactoAdd').text('Agregar');
});

$('#btnContactoAdd').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('empresa_contacto') }}",
        data: {
            'idEmpresa': '{{ empresa.id }}',
            'form': $("#formContacto").serialize(), // formulario con los datos
        },
        dataType: "json",
        // async: true,
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
         }
    });
    request.done(function (respuesta) {
        if(respuesta.existe === false){
            $("#tableContactos").text("");
            $("#tableContactos").html(respuesta.html);
            $("#contacto_tipoNotificaciones").val(null).trigger("change");

            document.getElementById("formContacto").reset();
            $('#modalLoad').modal('hide');
            $('#modalContacto').modal('hide');
            newNotify('Exito!',"success","Los contactos se agregaron correctamente");
         
            if(respuesta.notifApp){
                if(respuesta.usuario === null){
                    $("#forContacto").addClass("disable-div");  //está en el layout
                    $('#usuario_nombre').val(respuesta.nombre);
                    $('#usuario_email').val(respuesta.email);
                    $('#usuario_telefono').val(respuesta.celular);

                $('#para_contactos_app').removeAttr('hidden');
                $("#modalNewUsuario").modal("show");
                }else{
                    newNotify('Exito!',"warning","El usuario ya existe, se asocia contacto a usuario");
                
                    document.getElementById("formContacto").reset();
                }
            }else{
                document.getElementById("formContacto").reset();
            }
        }else{
            newNotify('Error!',"error","El contacto con ese numero telefónico ya existe");
          
        }
    });
    $('#btnContactoAdd').text('Agregar');
});

$('#btnUsuarioNew').click(function () { // alert('aca');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('empresa_newusuario') }}",
        data: {
            'idEmpresa': '{{ empresa.id }}',
            'form': $("#formUsuarioNew").serialize(), // formulario con los datos
        },
        dataType: 'json',
        // async: true,
        beforeSend: function () { 
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    request.done(function (respuesta) {
        if (respuesta.html) {
            $("#modalNewUsuario").modal("toggle");
            $("#tableUsuarios").text("");
            $("#tableUsuarios").html(respuesta.html);
            newNotify('Exito!',"success","El usuario se creó correctamente");
            getDataTable('tablaUsuario');
        } else {
            newNotify('Error!',"error",respuesta.error);
        }
        document.getElementById("formContacto").reset();
        $('#modalLoad').modal('hide');
        $('#para_contactos_app').hide();

        document.getElementById("formUsuarioNew").reset();

    });

});

$(".btnCancelar").click(function () {
    document.getElementById("formUsuarioNew").reset();
    document.getElementById("formContacto").reset();
});