
$(document).on('ready', function () {
    getDataTable('tablaIformes');
});

$('#informe_tipo').val("0").trigger("change");

int10 = setInterval('actualizarEstados()', 15000);
int1 = setInterval('mostrarContador()', 1000);
var contador = 15;
var request;

function actualizarEstados() {
    {% for informe in inbox %}
        {% if informe.estado < 3 %}
        checkEstado('{{informe.uid}}');
        {% endif %}
    {% endfor %}
    contador = 15;
    mostrarContador();
}


function mostrarContador() {
    contador = contador - 1;
    $("#contador").html('Los datos se actualizarán en: ' + contador + ' segundos.');
    if (contador == 0 || contador == 60) {
        $("#contador").html('Actualizando....');
        contador = 15;
        actualizarEstados();
    }
}
$(".a_informe_show").on("click",function(){
    $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
});

// Cerrar el modal cuando se navega hacia atrás o la página se carga desde el historial
$(window).on("pageshow", function(event) {
    if (event.originalEvent.persisted || window.performance && window.performance.navigation.type === 2) {
        $("#modalLoad").modal('hide');
    }
});

function checkEstado(uid) { //la url se pasa como se pasa en el formulari ode prudencia vial   
    $.ajax({
        url: "{{ path('informe_checkestado') }}",
        method: 'POST',
        data: { 'uid': uid }, //todos los formarios deben tener id=form
        dataType: "json",
        timeout: 5000, // Tiempo en milisegundos (5000 ms = 5 segundos)
        beforeSend: function () {
        },
        success: function (response) {
            if (response.htmlSpan) {
                $('#spanStatus_' + uid).text("");
                $('#spanStatus_' + uid).html(response.htmlSpan);
                $('#asuntoHref_' + uid).text("");
                $('#asuntoHref_' + uid).html(response.htmlHref);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus)
        }


    });
}


