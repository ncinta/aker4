const scrollTop = document.querySelector('.table-scroll-top');
const scrollContainer = document.querySelector('.table-container');

scrollTop.addEventListener('scroll', function () {
    scrollContainer.scrollLeft = scrollTop.scrollLeft;
});

scrollContainer.addEventListener('scroll', function () {
    scrollTop.scrollLeft = scrollContainer.scrollLeft;
});

function detalle(uid, id) {
    $.ajax({
        url: "{{path('informe_getdetalle')}}",
        method: 'POST',
        data: { 'uid': uid, 'servicio': id, 'tipo': 2 }, //todos los formarios deben tener id=form
        dataType: "json",
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
        success: function (data) {
            {% include "informe/Templates/detalle.js" %}         
                
        },
        error: function (jqXHR, textStatus, errorThrown) {
            newNotify('Error!', 'error', 'No hay datos del informe');
        }


    });
}
