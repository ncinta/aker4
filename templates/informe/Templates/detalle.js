$('#modalDetalle').modal('show');
$('#content_detalle').text("");
$('#content_detalle').html(data.html);
      
    var marcadores = {{ mapa.varMarkers() }}.items;
    Object.keys(marcadores).forEach(function(key) {
     if(key != 'exceso_a' && key != 'exceso_b'){
            {{ mapa.fcAddMarker() }}({id_sc: key, visible: false }); 
        }
    });


if (data['response']['referenciasHidden']) {   
    //oculto todas als referencias que pueden estar dando vuelta por ahí
    {{ mapa.fcStopDraw() }}();
    {{ mapa.fcAddAllIcons() }}(data['response']['referenciasHidden']['icon']);       //agrego los iconos
    {{ mapa.fcAddAllPolygons() }}(data['response']['referenciasHidden']['polygon']); //agrego los poligonos
    {{ mapa.fcAddAllCircles() }}(data['response']['referenciasHidden']['circle']);   //agrego circulos
    {{ mapa.fcAddAllMarkers() }}(data['response']['referenciasHidden']['referencia']);   //agrego las referencias.
    {{ mapa.fcIniDraw() }}();
}
if (data['response']['referenciasShow']) {   
    {{ mapa.fcStopDraw() }}();
    {{ mapa.fcAddAllIcons() }}(data['response']['referenciasShow']['icon']);       //agrego los iconos
    {{ mapa.fcAddAllPolygons() }}(data['response']['referenciasShow']['polygon']); //agrego los poligonos
    {{ mapa.fcAddAllCircles() }}(data['response']['referenciasShow']['circle']);   //agrego circulos
    {{ mapa.fcAddAllMarkers() }}(data['response']['referenciasShow']['referencia']);   //agrego las referencias.
    {{ mapa.fcSetClusterMinSize() }}(4); 
    {{ mapa.fcIniDraw() }}();
}
if (data['response']['excesos']) {
    var latCenter = 0;
    var longCenter = 0;          
    {{ mapa.fcStopDraw() }}();
    data['response']['excesos'].forEach(function(item) {
        {{ mapa.fcAddIcon() }}({    
            id_sc: 'icon_'+item.fecha.date,
            icon_url: '/images/ballred.png',
            icon_size_width: 24,
            icon_size_height: 24,
            icon_origin_x: 0,
            icon_origin_y: 0,
            icon_anchor_x: 12,
            icon_anchor_y: 12 }, false
        );
        {{ mapa.fcAddMarker() }}({
            id_sc: 'exceso_'+item.fecha.date,
            latitud: item.latitud,
            longitud: item.longitud,
            icono: 'icon_'+item.fecha.date,
            label_texto: item.velocidad +'km/h<br> (Máx.'+ item.velMaxPermitida+'km/h)',
            label_clase : 'servicio',
            label_borde : 2,
        });
        
        latCenter = item.latitud;
        longCenter = item.longitud;  
    });
    {{ mapa.fcIniDraw() }}();
    gotoCenter(latCenter, longCenter, true);
}
$('#modalLoad').modal('hide');
