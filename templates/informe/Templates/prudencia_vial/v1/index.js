function detalle(uid, id) {
    $.ajax({
        url: "{{path('informe_getdetalle')}}",
        method: 'POST',
        data: { 'uid': uid, 'servicio': id, 'tipo': 1 }, //todos los formarios deben tener id=form
        dataType: "json",
        beforeSend: function () {
            $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        },
        success: function (data) {
            {% include "informe/Templates/detalle.js" %}         
                
        },
        error: function (jqXHR, textStatus, errorThrown) {
            newNotify('Error!', 'error', 'No hay datos del informe');
        }


    });
}
