
function drawChart(informe) {

    //armo los datos segun necesito la estructura general de chart (no es lo mismo que viene)
    var datosDeGraficos = new Array();
    informe.forEach(function (objeto, index) {
        //transformo la data que viene en lo que necesito
        var i = 1;
        var arr = new Array();
        arr[0] = ['fecha', objeto.variable];
        objeto.data.forEach(function (obj) {
            arr[i] = [new Date(obj.fecha), obj.valor]
            i++;
        });

        var options = {
            title: objeto.nombre,
            curveType: 'function',
            legend: { position: 'rigth' },
            hAxis: { title: 'Fecha', titleTextStyle: { color: '#333' }, slantedTextAngle: 90 },
            vAxis: { title: objeto.vAxis },
            colors: [objeto.color],
            explorer: {
                actions: ['dragToZoom', 'rightClickToReset'],
                axis: 'horizontal',
                keepInBounds: true,
                maxZoomIn: 10.0
            },
            crosshair: {
                trigger: 'both',
                orientation: 'vertical'
            },
        };
        datosDeGraficos.push({ data: arr, options: options });
    });

    // Contenedor para los gráficos
    var chartsContainer = document.getElementById('pantalla');

    // Arreglo para almacenar todos los gráficos
    var charts = [];

    datosDeGraficos.forEach(function (grafico, index) {
        var chartDiv = document.createElement('div');
        chartDiv.id = 'chart_div_' + index;
        chartDiv.style.width = '100%';
        chartDiv.style.height = '300px';
        chartsContainer.appendChild(chartDiv);

        var data = google.visualization.arrayToDataTable(grafico.data);
        var options = grafico.options;

        var chart = new google.visualization.AreaChart(chartDiv);

        // Agregar el gráfico al arreglo
        charts.push(chart);

        google.visualization.events.addListener(chart, 'ready', function () {
            console.log('ready');            
        });

        google.visualization.events.addListener(chart, 'click', function () {
            console.log('click');
        });

        google.visualization.events.addListener(chart, 'zoom', function () {
            console.log('zoom');
        });

        google.visualization.events.addListener(chart, 'onmouseover', function () {
            console.log('onmouseover');
        });

        chart.draw(data, options);


        // Agregar evento de zoom a cada gráfico
        /**google.visualization.events.addListener(chart, 'ready', function () {
            console.log('ready');
            var state = chart.getChart().getChartState();
            console.log(state);
            charts.forEach(function (otherChart, otherIndex) {
                if (index !== otherIndex) {
                    otherChart.getChart().setOption('explorer', {
                        actions: state.actions,
                        axis: state.axis,
                        keepInBounds: state.keepInBounds,
                        maxZoomIn: state.maxZoomIn
                    });
                }
            });
        });*/


        // Agregar evento de selección a cada gráfico
        google.visualization.events.addListener(chart, 'select', function () {
            console.log('select');
            var selection = chart.getSelection();
            charts.forEach(function (otherChart, otherIndex) {
                if (index !== otherIndex) {
                    otherChart.setSelection(selection);
                }
            });
        });



    });



}

