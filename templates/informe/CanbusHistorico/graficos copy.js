
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {       
        //imprimo en el script los valores que vienen en la data         
        var x = 0;
        {% for graph in dt %}
            var i = 1;
            var {{ graph.variable }} = new Array();
            {{ graph.variable }}[0] = ['fecha', '{{ graph.variable }}'];
            {% for key,value in graph.data %}            
                {{ graph.variable }}[i] = [ new Date({{ value["fecha"]["Y"] }}, {{ value["fecha"]["m"] }}, {{ value["fecha"]["d"] }}, {{ value["fecha"]["H"] }}, {{ value["fecha"]["i"] }}, {{ value["fecha"]["s"] }} ), {{ graph.variable }} ];
                i++;
            {% endfor %}    

            var data[x] = google.visualization.arrayToDataTable({{ graph.variable }});

            var options[x] = {
                title: {{ graph.nombre }},
                curveType: 'function',
                legend: {position: 'rigth'},
                hAxis: {title: '{{ graph.variable }}',  titleTextStyle: {color: '#333'}, slantedTextAngle: 90 },
                vAxis: {minValue: 0, maxValue: 100},
                colors: ['#aa66cc'], // Cambiar los colores aquí
                explorer: {
                    actions: ['dragToZoom', 'rightClickToReset'],
                    axis: 'horizontal', // Puedes cambiar a 'vertical' o 'both' según tus necesidades
                    keepInBounds: true,
                    maxZoomIn: 10.0
                }
            };
            x++;
        {% endfor %}

        var x = 0;
        {% for graph in dt %}
            var chart = new google.visualization.AreaChart(document.getElementById('myArea1'));
            chart.draw(data[x], options[x]);
        {% endfor %}
            
    }
