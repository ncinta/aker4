$(document).ready(function () {            
    $('#liVerAgrupadas').css('display', 'none');
});

//variable globla de ver referencia
var verReferencias = '0';

// Hace visible todas las referencias ocultas
function verTodasReferencias(){
    if (this.document.getElementById("checkboxReferencias").checked) {
        verReferencias = '1';
        $('#liVerAgrupadas').css('display', 'block');
    } else {
        verReferencias = '0';
        $('#liVerAgrupadas').css('display', 'none');
    }
    var request = $.ajax({
          url: "{{ path('sauron_referencias_viewAJAX') }}", 
          data: {'visible' : verReferencias},
          dataType: "json"
    });
    request.done(function(respuesta) {
       {{ mapa.fcAddAllMarkers() }}(respuesta['0']['icon']);               
       {{ mapa.fcAddAllPolygons() }}(respuesta['0']['polygon']); 
       {{ mapa.fcAddAllCircles() }}(respuesta['0']['circle']); 
       verAgrupadasReferencias();
    });
}
        
//Activa o desactiva los clusters de las referencias.        
function verAgrupadasReferencias() {
    if (this.document.getElementById("checkboxAgrupadasReferencias").checked) {
        {{ mapa.fcSetClusterMinSize() }}(4);
    } else {
        {{ mapa.fcSetClusterMinSize() }}(200000);
    }
}     