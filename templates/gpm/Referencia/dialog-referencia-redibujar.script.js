var referencia_id = null;    //es el puntero al elemento q estoy redibujando.
var color = '#0000ff';    //color por defecto.

var palette_default = [
    ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
    ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
    ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
    ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
    ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
    ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
    ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
    ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
];

$(function(){    
    //esto es para setear los datos del dialog.
    
    $("#rr_colorSelector").on("dragstop.spectrum", function(e, color) {
        rr_changeColor(color.toHexString());
    });
    
    $("#rr_colorSelector").on("change", function(e, color) {
        rr_changeColor(color.toHexString());
    }); 


    $( "#rr_transparentSelector" ).slider({ 
        max: 10,
        min: 1,
        value : 5,
        change: function(event, ui) {  
            rr_changeTransparent(ui.value / 10)
        },
    });

});

function redibujarReferencia(id, lat, lon) {
    referencia_id = id;   //actualiza la variable global.
    $('#formrr_referencia_id').val(id); //aca grabo el valor en el form

    //abro el cuadro de dialogo.
     $('#dialog-form-redibujar-ref').modal('show');
    {{ mapa.varMarkers() }}.get('r'+id).closeInfo();
    mark = {{ mapa.varMarkers() }}.get('r' + id);
    $('#rr_nombre-referencia').text( mark.label_texto );

    //icono
    icono = {{ mapa.varIcons() }}.get( 'i' + id );
    if (icono[0] != null) {
        document.getElementById('rr_zona-icono').src= icono[0]['url'];
        document.getElementById('formrr_pathIcono').value= icono[0]['url'];
    } else {
        document.getElementById('rr_zona-icono').src= '{{ asset('images/ball.png') }}';
        document.getElementById('formrr_pathIcono').value= '{{ asset('images/ball.png') }}';
    }

    //determinacion de la clase de dibujo
    circulo = {{ mapa.varCircles() }}.get('c' + id);
    if (circulo != null) {
        $('#rr_dibujo').text('Circulo');
        //es un circulo
        var color = circulo.get('fillColor')
        var transparencia = circulo.get('fillOpacity');
        
        {{ mapa.fcAddCircle() }}({id_sc: 'c'+ id, editable: true});        
    } else {
        $('#rr_dibujo').text('Poligono');
        
        //es un poligono
        polygon = {{ mapa.varPolygons() }}.get('p' + id);
        
        var color = polygon.get('fillColor');
        var transparencia = polygon.get('fillOpacity');
        
        {{ mapa.fcAddPolygon() }}({id_sc: 'p'+ id, editable: true});
    }
  
   //esto es para seleccionar el color
    $("#rr_colorSelector").spectrum({
        color: color,
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        palette: palette_default,
        localStorageKey: "spectrum.homepage", // Any Spectrum with the same string will share selection
        hideAfterPaletteSelect:true,
        chooseText: "Cambiar",
        cancelText: "Cancelar",
        
//        change: function(color) {
//            rr_changeColor(color.toHexString());
//        },
    });
    $('#formrr_color').val(color); //aca grabo el valor en el form
    //
    //seteo de la transparencia.
    $( "#rr_transparentSelector" ).slider({ value : transparencia *10  });
    
    {{ mapa.getMapName() }}.setCenter(new google.maps.LatLng (lat, lon));
    {{ mapa.getMapName() }}.setZoom(15);
    
} 

function rr_changeColor(color) {    
    if (color == null) {
        color = colorToHex($('#rr_colorSelector div').css('backgroundColor'));
    }
    $('#formrr_color').val(color); //aca grabo el valor en el form

    if (circulo != null) {  
        {{ mapa.fcAddCircle() }}({id_sc: 'c'+referencia_id, strokeColor: color, strokeWeight: 5, fillColor: color});
    } else {
        {{ mapa.fcAddPolygon() }}({id_sc: 'p'+referencia_id, strokeColor: color, strokeWeight: 5, fillColor: color});

    }
}    

function rr_changeTransparent(transparent) {
   
    if (referencia_id != null) {
        if (transparent == null) {
            transparent = $( "#rr_transparentSelector" ).slider("value") / 10;
        }
      
        strokeTransparent = transparent + 0.2;
        $('#formrr_transparencia').val(transparent); //aca grabo el valor en el form
        if (circulo != null) { 
            {{ mapa.fcAddCircle() }}({id_sc: 'c' + referencia_id, strokeOpacity: strokeTransparent, fillOpacity: transparent });
        } else {
            {{ mapa.fcAddPolygon() }}({id_sc: 'p' + referencia_id, strokeOpacity: strokeTransparent, fillOpacity: transparent });

        }

    }
}

// Cerrar el dibujo
function rr_closeDraw(){
    if(referencia_id != null){        
        if (circulo != null) { 
            //grabo en los inputs el radio y el centro.
            document.getElementById('formrr_centro').value = circulo.getCenter();
            document.getElementById('formrr_radio').value = circulo.getRadius();
        } else {
            //obtengo los puntos para grabarlo en el input del poligono.
            var puntos = {{ mapa.fcPolygonPath() }}('p'+referencia_id);
            var res = puntos.join();
            document.getElementById('formrr_poligono').value = res;                
            
            var puntos_obj = {{ mapa.fcPolygonObject() }}('p'+referencia_id);            
            document.getElementById('formrr_area').value = google.maps.geometry.spherical.computeArea(puntos_obj);
            //alert(z + ' mts2 - '+ (z/10000)+' hect.');
            
        }
        cr_showDraw(false);
    }
}

//aca es cuando hago el post y debo crear la nueva referencia.-
function dialogformRedibujoPost() {
    rr_closeDraw();   //es para cerrar el dibujo si esta abierto
    var request = $.ajax({
      type: 'POST',
      url: "{{ path('gpm_referencia_update_ajax') }}", //en el ReferenciaController.
      data: { 'referencia': $("#formrr_referencia").serialize() },   //serializo el form
      dataType: "json",
      //async: true,
      beforeSend: function(){                  
        },
    });
    request.done(function(respuesta) {    //true || verReferencias
        rr_closeDraw();
        {{ mapa.fcAddPolygon() }}({id_sc: 'p'+ respuesta.id, editable: false, visible: true});                        
        newRef = respuesta['referencia']
        {{ mapa.fcAddIcon() }}(newRef['icono'], true);
        {{ mapa.fcAddAllMarkers() }}(newRef['referencia']);
        {{ mapa.fcAddAllCircles() }}(newRef['circle']); 
        ultimo = null;                
        $("#dialog-form-redibujar-ref").modal('toggle'); //cierro el form
        //verTodasReferencias();
    });    
}

function dialogformRedibujoCancel() {
        cerrarMapa();
}

$("#closeMapa").click(function(){
    cerrarMapa();
});

function cerrarMapa(){
    id = document.getElementById("formrr_referencia_id").value;
    {{ mapa.fcAddPolygon() }}({id_sc: 'p'+ id, editable: false, visible: true});   
    rr_closeDraw();         //cierro el dibujo
    refererencia_id = null;          //anulo el puntero de lo que tengo creado
    $( "#dialog-form-redibujar-ref" ).modal('toggle');;  //cierro el form 
}