var ultimo = null; //es el puntero al ultimo elemento credo

        $(document).ready(function () {
$("#actividad_referencia").select2({
width: '100%', // need to override the changed default
        ajax: {
        url: "{{ path('gpm_referencia_get') }}",
                data: function (params) {
                var query = {
                id: "{{ organizacion.id }}",
                        search: params.term,
                        type: 'public'
                };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                },
                dataType: 'json'
        },
        language: {
        noResults: function () {
        return '<a href="#modalReferenciaNew" data-toggle="modal" >Agregar Referencia</a>'
        }
        },
        escapeMarkup: function (markup) {
        return markup;
        }
});
        $("#actividad_edit_referencia").select2({
width: '100%', // need to override the changed default
        ajax: {
        url: "{{ path('gpm_referencia_get') }}",
                data: function (params) {
                var query = {
                id: "{{ organizacion.id }}",
                        search: params.term,
                        type: 'public'
                };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                },
                dataType: 'json'
        },
        language: {
        noResults: function () {
        return '<a href="#modalReferenciaNew" data-toggle="modal" >Agregar Referencia</a>'
        }
        },
        escapeMarkup: function (markup) {
        return markup;
        }
});
        $("#actividad_copiar_referencia").select2({
width: '100%', // need to override the changed default
        ajax: {
        url: "{{ path('gpm_referencia_get') }}",
                data: function (params) {
                var query = {
                id: "{{ organizacion.id }}",
                        search: params.term,
                        type: 'public'
                };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                },
                dataType: 'json'
        },
        language: {
        noResults: function () {
        return '<a href="#modalReferenciaNew" data-toggle="modal" >Agregar Referencia</a>'
        }
        },
        escapeMarkup: function (markup) {
        return markup;
        }
});
        });
        $("#referencia_color").on("change", function() {
cr_changeColor($(this).val());
});
        $("#referencia_transparencia").on("change", function() {
$("#referencia_transparencia").val($(this).val());
        cr_changeTransparent($(this).val() / 10);
});
        // Se llama a esta funcion cuando se selecciona uno de los tipos de dibujo.
        $("#referencia_dibujo").on("change", function() {
val = document.getElementById('referencia_dibujo').value;
        if (val != null && val != '') {
window.ultimo = null;
        $('#referencia_dibujo').prop('disabled', true); //deshabilito el sector
        $('#referencia_clase').prop('value', document.getElementById('referencia_dibujo').value); //seteo el valor
        cr_newDraw(); //inicia el dibujado de lo que seleccionamos.  
}
});
// Nuevo dibujo y instanciacion de variables.
        function cr_newDraw(){
        cr_closeDraw();
                window.ultimo = 'ultimo';
                var c = {{ mapa.getMapName() }}.getCenter();
                tDraw = document.getElementById('referencia_dibujo').value;
                if (tDraw == 1) {  //agrego un nuevo radio
        {{ mapa.fcAddCircle() }}({id_sc:window.ultimo, editable: true, center: c, radius: 50, visible: true});
        } else {
        var paths = new Array();
                paths.push(new google.maps.LatLng(c.lat() - 0.001, c.lng() - 0.001));
                paths.push(new google.maps.LatLng(c.lat() - 0.001, c.lng() + 0.001));
                paths.push(new google.maps.LatLng(c.lat() + 0.001, c.lng() + 0.001));
                paths.push(new google.maps.LatLng(c.lat() + 0.001, c.lng() - 0.001));
        {{ mapa.fcAddPolygon() }}({id_sc:window.ultimo, editable: true, path: paths, visible: true });
        }
        cr_changeColor();
                cr_changeTransparent();
        }

function cr_showDraw(ver)  {
tDraw = document.getElementById('referencia_dibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
{{ mapa.fcAddCircle() }}({id_sc:window.ultimo, visible: ver });
} else {
{{ mapa.fcAddPolygon() }}({id_sc:window.ultimo, visible: ver });
}
}

function cr_changeColor(color) {
$('#referencia_color').val(color); //aca grabo el valor en el form
        tDraw = document.getElementById('referencia_dibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
{{ mapa.fcAddCircle() }}({id_sc:window.ultimo, strokeColor: color, strokeWeight: 5, fillColor: color});
} else {
{{ mapa.fcAddPolygon() }}({id_sc:window.ultimo, strokeColor: color, strokeWeight: 5, fillColor: color});
}
}

function cr_changeTransparent(transparent) {
if (window.ultimo != null) {
strokeTransparent = transparent + 0.2;
        tDraw = document.getElementById('referencia_dibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
{{ mapa.fcAddCircle() }}({id_sc:window.ultimo, strokeOpacity: strokeTransparent, fillOpacity: transparent });
} else {
{{ mapa.fcAddPolygon() }}({id_sc:window.ultimo, strokeOpacity: strokeTransparent, fillOpacity: transparent });
}
}
}
// Cerrar el dibujo
function cr_closeDraw(){
if (window.ultimo != null){
tDraw = document.getElementById('referencia_dibujo').value;
        if (tDraw == 1) {  //agrego un nuevo radio
//cierro la edición del circulo.
{{ mapa.fcAddCircle() }}({id_sc:window.ultimo, editable: false });
        var circulo = {{ mapa.varCircles() }}.get(window.ultimo);
        //grabo en los inputs el radio y el centro.
        document.getElementById('referencia_centro').value = circulo.getCenter();
        document.getElementById('referencia_radio').value = circulo.getRadius();
} else {
// cierro el poligono.
{{ mapa.fcAddPolygon() }}({id_sc:window.ultimo, editable: false, visible: false });
        //obtengo los puntos para grabarlo en el input del poligono.
        var puntos = {{ mapa.fcPolygonPath() }}(window.ultimo);
        var res = puntos.join();
        document.getElementById('referencia_poligono').value = res;
        var puntos_obj = {{ mapa.fcPolygonObject() }}(window.ultimo);
        document.getElementById('referencia_area').value = google.maps.geometry.spherical.computeArea(puntos_obj);
}
cr_showDraw(false);
}
}

$('#btnReferenciaAdd').click(function () {
window.ultimo = 'ultimo';
        cr_closeDraw();
        var req = $.ajax({
        type: 'POST',
                url: "{{ path('gpm_referencia_new') }}",
                data: {
                'id': "{{ organizacion.id }}",
                        'formReferencia': $("#formReferenciaNew").serialize(), //form de productos
                },
                dataType: "json",
                //async: true,
                beforeSend: function () {
                },
        });
        req.done(function (respuesta) {
        document.getElementById("formReferenciaNew").reset();
                $("#actividad_referencia").append(new Option(respuesta.nombre, respuesta.id));
                $('#referencia_dibujo option:eq(0)')
                $('#referencia_dibujo').prop('disabled', false); //deshabilito el sector
                $('#referencia_unica').val(0);
                $('#referencia_actividad').val(0);
                $("#referencias").html(respuesta.html);
                $('#modalReferenciaNew').modal('toggle'); //cierro el form
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
//                hiddenReferencias();
                newNotify('Exito!',"success","La referencia se creó correctamente");
             
        
});
});
        $('#btnReferenciaGeneral').click(function () {
            $('#tituloreferencia').text("Crear referencia general para todas las actividades");
                verReferencias('0');
        });
      
        $('#btnReferenciaCancelar').click(function () {
             $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
        });
        function crearReferenciaUnica(idActividad, nombreActividad) {
                verReferencias(idActividad);
                $('#tituloreferencia').text("Crear Referencia Única para " + nombreActividad);
                $('#referencia_unica').val(1);
                $('#referencia_actividad').val(idActividad);
                $('#modalReferenciaNew').modal('show');
        }
        
   
        function verReferencias(idActividad){
                      var request = $.ajax({
                        url: "{{ path('gpm_referencias_viewAJAX') }}", 
                        data: {'actividad' : idActividad},
                        dataType: "json",
                        timeout: 10000, //in milliseconds
                  });
                  request.done(function(respuesta) {
                     {{ mapa.fcStopDraw() }}();
                        borrarDelMapa(respuesta);
                        agregarAlMapa(respuesta);
                     {{ mapa.fcSetClusterMinSize() }}(4); 
                     {{ mapa.fcIniDraw() }}();
                 });
        }

function removeReferencia(id, unica) {
    if (unica === null || unica === 1){
    $('#nombreReferencia').text("La referencia se eliminará de forma permanente. ¿Está seguro?");
    } else{
    $('#nombreReferencia').text("¿Está seguro de quitar la referencia de la actividad ?");
    }
    $('#referencia_quitar_unica').val(unica);
            $('#referencia_quitar_id').val(id);
            $('#modalReferenciaQuitar').modal('show');
}

function agregarAlMapa(respuesta){
              {{ mapa.fcAddAllIcons() }}(respuesta['visibles']['icon']);       //agrego los iconos
                        {{ mapa.fcAddAllPolygons() }}(respuesta['visibles']['polygon']); //agrego los poligonos
                        {{ mapa.fcAddAllCircles() }}(respuesta['visibles']['circle']);   //agrego circulos
                        {{ mapa.fcAddAllMarkers() }}(respuesta['visibles']['referencia']);   //agrego las referencias.
                    
}

function borrarDelMapa(respuesta){
      {{ mapa.fcAddAllIcons() }}(respuesta['invisibles']['icon']);       //borro todos  los iconos
      {{ mapa.fcAddAllPolygons() }}(respuesta['invisibles']['polygon']); //borro todos  los poligonos
      {{ mapa.fcAddAllCircles() }}(respuesta['invisibles']['circle']);   //borro todos  circulos
      {{ mapa.fcAddAllMarkers() }}(respuesta['invisibles']['referencia']);   //borro todos  las referencias.
                    
}

$('#btnReferenciaQuitar').click(function () {

var req = $.ajax({
type: 'POST',
        url: "{{ path('gpm_referencia_quitar') }}",
        data: {
        'id': "{{ organizacion.id }}",
                'formReferencia': $("#formReferenciaQuitar").serialize(), //form de productos
                'actividad':'{{ actividad is defined ? actividad.id : null }}'
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
});
        req.done(function (respuesta) {
        document.getElementById("formReferenciaQuitar").reset();
                $('#referencia_unica').val(0);
                $('#referencia_actividad').val(0);
                $("#referencias").html(respuesta.html);
                 {{ mapa.fcStopDraw() }}();
                        borrarDelMapa(respuesta);
                      
                     {{ mapa.fcSetClusterMinSize() }}(4); 
                     {{ mapa.fcIniDraw() }}();
                $('#modalReferenciaQuitar').modal('toggle'); //cierro el form
        });
});


