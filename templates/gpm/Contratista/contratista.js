var idContratista = 0;
var idPersona = 0;
$("#proyecto_contratista").select2({
    width: '100%', // need to override the changed default
    ajax: {
        url: "{{ path('gpm_contratista_get') }}",
        data: function (params) {
            var query = {
                id: "{{ organizacion.id }}",
                search: params.term,
                type: 'public'
            };
            // Query parameters will be ?search=[term]&type=public
            return query;
        },
        dataType: 'json'
    },
    language: {
        noResults: function () {
            return '<a href="#modalContratistaAgregar" data-toggle="modal" >Agregar Contratista</a>'
        }
    },
    escapeMarkup: function (markup) {
        return markup;
    }
});


$('#btnContratistaAgregar').click(function () {
    var nombre = document.getElementById('contratista_nombre').value;
    if (nombre.length !== 0) {
        var req = $.ajax({
            type: 'POST',
            url: "{{ path('gpm_contratista_new') }}",
            data: {
                'id': "{{organizacion.id}}",
                'formContratista': $("#formContratistaNew").serialize(), //form de productos
            },
            dataType: "json",
            //async: true,
            beforeSend: function () {
            },
        });
        req.done(function (respuesta) {
            $("#proyecto_contratista").append(new Option(respuesta.nombre, respuesta.id));
            $("#contratistas").html(respuesta.html);
            $('#modalContratistaAgregar').modal('toggle'); //cierro el form

            document.getElementById("formContratistaNew").reset();
            newNotify('Exito!',"success","El contratista se creó correctamente");
           
        });
    } else {
        newNotify('Error!',"error","Debe contener un nombre");        
    }
});

function modalDelete(id, nombre) {
    window.idContratista = id;
    $('#contratista_borrar_nombre').text('');
    $('#contratista_borrar_nombre').text(nombre);
    $('#modalContratistaEliminar').modal('show');
}

function modalModificar(id, nombre) {
    window.idContratista = id;
    $('#contratista_edit_nombre').val(nombre);
    $('#modalContratistaModificar').modal('show');
}

$('#btnContratistaRemover').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_contratista_remover') }}",
        data: {
            'id': window.idContratista,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#contratistas").html(respuesta.html);
        $('#modalContratistaEliminar').modal('toggle'); //cierro el form
    });
});

$('#btnContratistaModificar').click(function () {
    var nombre = document.getElementById('contratista_edit_nombre').value;
    if (nombre.length !== 0) {
        var req = $.ajax({
            type: 'POST',
            url: "{{ path('gpm_contratista_edit') }}",
            data: {
                'id': window.idContratista,
                'formContratistaEdit': $("#formContratistaEdit").serialize(), //form de productos
            },
            dataType: "json",
            //async: true,
            beforeSend: function () {
            },
        });
        req.done(function (respuesta) {
            $("#contratistas").html(respuesta.html);
            $('#modalContratistaModificar').modal('toggle'); //cierro el form

            document.getElementById("formContratistaEdit").reset();
            newNotify('Exito!',"success","El contratista se modificó correctamente");
            
        });
    } else {
        newNotify('Error!',"error","Debe contener un nombre");      
    }
});
$('#btnPersonalAgregar').click(function () {
    {% if contratista is defined %}
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_contratista_newpersona') }}",
        data: {
            'formPersona': $("#formPersonaNew").serialize(), //form de productos
            'idCont': "{{ contratista.id }}"
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        document.getElementById("formPersonaNew").reset();
        $("#personal").text("");
        $("#personal").html(respuesta.html);
        $('#modalPersonaAgregar').modal('toggle'); //cierro el form
        newNotify('Exito!',"success","La persona se creó correctamente");
        
    });
    {% endif %}
});

function modalDeletePersona(idPers, nombre) {
    window.idPersona = idPers;
    $('#actividad_persona_nombre').text('');
    $('#actividad_persona_nombre').text(nombre);
    $('#modalPersonaEliminar').modal('show');
}

$('#btnPersonaRemover').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_contratista_removerpersona') }}",
        data: {
            'id': window.idPersona,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#personal").text("");
        $("#personal").html(respuesta.html);
        $('#modalPersonaEliminar').modal('toggle'); //cierro el form
        newNotify('Exito!',"success","La persona se eliminó correctamente");
         
    });
});

function modalModificarPersona(id, nombre,telefono,cuit,documento) {
    window.idPersona = id;
    $('#persona_nombre_edit').val(nombre);
    $('#persona_telefono_edit').val(telefono);
    $('#persona_cuit_edit').val(cuit);
    $('#persona_documento_edit').val(documento);
    $('#modalPersonaModificar').modal('show');
}
$('#btnPersonaModificar').click(function () {
    var nombre = document.getElementById('persona_nombre_edit').value;
    if (nombre.length !== 0) {
        var req = $.ajax({
            type: 'POST',
            url: "{{ path('gpm_contratista_personaedit') }}",
            data: {
                'id': window.idPersona,
                'formPersonaEdit': $("#formPersonaEdit").serialize(), //form de productos
            },
            dataType: "json",
            //async: true,
            beforeSend: function () {
            },
        });
        req.done(function (respuesta) {
            $("#personal").html(respuesta.html);
            $('#modalPersonaModificar').modal('toggle'); //cierro el form

            document.getElementById("formPersonaEdit").reset();
            newNotify('Exito!',"success","La persona se modificó correctamente");            
        });
    } else {
        newNotify('Error!',"error","Debe contener un nombre");
    }
});

