
$("#proyecto_cliente").select2({
    width: '100%', // need to override the changed default
    ajax: {
        url: "{{ path('gpm_cliente_get') }}",
        data: function (params) {
            var query = {
                id: "{{ organizacion.id }}",
                search: params.term,
                type: 'public'
            };
            // Query parameters will be ?search=[term]&type=public
            return query;
        },
        dataType: 'json'
    },
    language: {
        noResults: function () {
            return '<a href="#modalClienteAgregar" data-toggle="modal" >Agregar Cliente</a>'
        }
    },
    escapeMarkup: function (markup) {
        return markup;
    }
});


$('#btnClienteAgregar').click(function () {
    var nombre = document.getElementById('cliente_nombre').value;
    if (nombre.length !== 0) {
        var req = $.ajax({
            type: 'POST',
            url: "{{ path('gpm_cliente_new') }}",
            data: {
                'id': "{{organizacion.id}}",
                'formCliente': $("#formClienteNew").serialize(), //form de productos
            },
            dataType: "json",
            //async: true,
            beforeSend: function () {
            },
        });
        req.done(function (respuesta) {
            $("#proyecto_cliente").append(new Option(respuesta.nombre, respuesta.id));
            $('#modalClienteAgregar').modal('toggle'); //cierro el form
        });
    } else {
        newNotify('Error!',"error","Debe contener un nombre");
    }
});