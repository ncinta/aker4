var idProyecto = 0;

 $(document).ready(function () {

            $("#proyecto_responsable").select2({
                width: '100%', // need to override the changed default
                ajax: {
                    url: "{{ path('gpm_responsable_getresponsables') }}",
                    data: function (params) {
                        var query = {
                            id: "{{ proyecto.organizacion.id }}",
                            search: params.term,
                            type: 'public'
                        };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    dataType: 'json'
                },
                language: {
                    noResults: function () {
                        return '<a href="#modalResponsableAgregar" data-toggle="modal" >Agregar Responsable</a>'
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


            $('#proyecto_descripcion').summernote({
                height: 200,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
      
                    ]
                });

                $('#btnResponsableAgregar').click(function () {
                  var nombre = document.getElementById('reponsable_nombre').value;
                   if(nombre.length !== 0){
                    var req = $.ajax({
                        type: 'POST',
                        url: "{{ path('gpm_responsable_newresponsable') }}",
                        data: {
                            'id': "{{proyecto.organizacion.id}}",
                            'formResponsable': $("#formResponsableNew").serialize(), //form de productos
                        },
                        dataType: "json",
                        //async: true,
                        beforeSend: function () {
                        },
                    });
                    req.done(function (respuesta) {
                        $("#proyecto_responsable").append(new Option(respuesta.nombre, respuesta.id));
                        $('#modalResponsableAgregar').modal('toggle'); //cierro el form
                         document.getElementById("formResponsableNew").reset();
                         newNotify('Exito!',"success","El responsable se creó correctamente");
                                               
                    });
                     }else{
                        newNotify('Error!',"error","Debe contener un nombre");
            }
                });
            $("#proyecto_cliente").select2({
                width: '100%', // need to override the changed default
                ajax: {
                    url: "{{ path('gpm_cliente_get') }}",
                    data: function (params) {
                        var query = {
                            id: "{{ proyecto.organizacion.id }}",
                            search: params.term,
                            type: 'public'
                        };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    dataType: 'json'
                },
                language: {
                    noResults: function () {
                        return '<a href="#modalClienteAgregar" data-toggle="modal" >Agregar Cliente</a>'
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


            $('#btnClienteAgregar').click(function () {
                var req = $.ajax({
                    type: 'POST',
                    url: "{{ path('gpm_cliente_new') }}",
                    data: {
                        'id': "{{ proyecto.organizacion.id}}",
                        'formCliente': $("#formClienteNew").serialize(), //form de productos
                    },
                    dataType: "json",
                    //async: true,
                    beforeSend: function () {
                    },
                });
                req.done(function (respuesta) {
                    $("#proyecto_cliente").append(new Option(respuesta.nombre, respuesta.id));
                    $('#modalClienteAgregar').modal('toggle'); //cierro el form
                      document.getElementById("formClienteNew").reset();
                      newNotify('Exito!',"success","El cliente se creó correctamente");
                        
                });
            });


            $("#proyecto_contratista").select2({
                width: '100%', // need to override the changed default
                ajax: {
                    url: "{{ path('gpm_contratista_get') }}",
                    data: function (params) {
                        var query = {
                            id: "{{ proyecto.organizacion.id }}",
                            search: params.term,
                            type: 'public'
                        };
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    dataType: 'json'
                },
                language: {
                    noResults: function () {
                        return '<a href="#modalContratistaAgregar" data-toggle="modal" >Agregar Contratista</a>'
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });


            $('#btnContratistaAgregar').click(function () {
                 var nombre = document.getElementById('contratista_nombre').value;
                   if(nombre.length !== 0){
                        var req = $.ajax({
                            type: 'POST',
                            url: "{{ path('gpm_contratista_new') }}",
                            data: {
                                'id': "{{proyecto.organizacion.id}}",
                                'formContratista': $("#formContratistaNew").serialize(), //form de productos
                            },
                            dataType: "json",
                            //async: true,
                            beforeSend: function () {
                            },
                        });
                        req.done(function (respuesta) {
                            $("#proyecto_contratista").append(new Option(respuesta.nombre, respuesta.id));
                            $('#modalContratistaAgregar').modal('toggle'); //cierro el form

                         document.getElementById("formContratistaNew").reset();
                         newNotify('Exito!',"success","El contratista se creó correctamente");
                               
                        });
                   }else{
                    newNotify('Error!',"error","Debe contener un nombre");
                    
                   }
                
            });
    });
            $(function () {
        {% if proyecto is defined %}
                $("#proyecto_fecha_inicio").datetimepicker({
                    format: 'DD/MM/YYYY',
                }).data("DateTimePicker").date('{{ proyecto.fechaInicio|date("d/m/Y")}}');
                $("#proyecto_fecha_fin").datetimepicker({
                    format: 'DD/MM/YYYY',
                }).data("DateTimePicker").date('{{ proyecto.fechaFin|date("d/m/Y")}}');
        {% endif %}


            });
            
