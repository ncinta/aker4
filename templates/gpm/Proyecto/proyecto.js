function modalProyectoDelete(id, nombre) {
    window.idProyecto = id;
    $("#proyecto_borrar_nombre").text("");
    $("#proyecto_borrar_nombre").text(nombre);
    $('#modalProyectoEliminar').modal('show');
}

$('#btnProyectoDelete').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_proyecto_delete') }}",
        data: {
            'id': window.idProyecto,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#totalProyecto").text(respuesta.totalProyectos);
        $(".tablaProyectos").text("");
        $(".tablaProyectos").html(respuesta.html);
        $('#modalProyectoEliminar').modal('toggle'); //cierro el form
        closeNav();
        getDatatable();
    });
});


function setSidebarDataActividades(id) {
    openNav();
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_proyecto_getactividades') }}",
        data: {
            'id': id,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#content-sidebar").text("Cargando.....");
        },
    });
    req.done(function (respuesta) {
        $("#content-sidebar").text("");
        $("#content-sidebar").html(respuesta.htmlSidebar);
    });
}
function setSidebarDataProyecto(id) {
    openNav();
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_proyecto_getproyecto') }}",
        data: {
            'id': id,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#content-sidebar").text("Cargando.....");
        },
    });
    req.done(function (respuesta) {
        $("#titulo-sidebar").text("");
        $("#titulo-sidebar").html(respuesta.tituloSidebar);
        $("#content-sidebar").text("");
        $("#content-sidebar").html(respuesta.htmlSidebar);
    });
}





