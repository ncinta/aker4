
        $(document).ready(function () {
                {% if actividad.id != null%}

            // se carga el formulario desde aca sino symfony lo maneja de manera automatica y me tira errores al querer parsear a objeto el form
                        $('#actividad_nombre').val('{{actividad.nombre}}');
                        $('#actividad_fechaInicio').val('{{actividad.fechaInicio|date('Y-m-d')~'T'~actividad.fechaInicio|date('H:i')}}');
                        $('#actividad_fechaFin').val('{{actividad.fechaFin|date('Y-m-d')~'T'~actividad.fechaFin|date('H:i')}}');
                        $('#actividad_codigo').val('{{actividad.codigo}}');
                        $('#actividad_descripcion').summernote('code', '{{actividad.descripcion}}');
                        $('#actividad_contratista').val('{{actividad.contratista? actividad.contratista.id : 0}}');
                        $('#actividad_contratista').trigger('change');
                        $('#actividad_responsable').val('{{actividad.responsable? actividad.responsable.id : 0}}');
                        $('#actividad_responsable').trigger('change');
                        $('#actividad_nivelTension').val('{{actividad.opciones and actividad.opciones["NIVEL_TENSION"] is defined  ? actividad.opciones["NIVEL_TENSION"]["key"]: 0}}');
                        $('#actividad_nivelTension').trigger('change');
                        $('#actividad_corteEnergia').prop('checked','{{actividad.opciones and actividad.opciones["CORTE_ENERGIA"] is defined ? true : false}}');
                        {% if actividad.anchoTrabajo != null %}
                        $('#actividad_anchoTrabajo').val('{{actividad.anchoTrabajo}}');
                        {% endif %}
                        //cargo los selects de vehiculo y referencia porque no los puedo cargar en el type
                        var req1 = $.ajax({
                            type: 'POST',
                            url: "{{ path('gpm_actividad_getservyref') }}",
                            data: {
                                'id':{{act is defined?act.id:actividad.id}},
                            },
                            dataType: "json",
                            //async: true,
                            beforeSend: function () {
                            },
                        });
                        req1.done(function (respuesta) {
                            $('#actividad_actgpmServicios').val(respuesta.servicios);
                            $('#actividad_actgpmServicios').trigger('change');
                            $('#actividad_actgpmRefgpm').val(respuesta.referencias);
                            $('#actividad_actgpmRefgpm').trigger('change');
                        });
                      
                        var req1 = $.ajax({
                            type: 'POST',
                            url: "{{ path('gpm_actividad_getpersonal') }}",
                            data: {
                                'id':{{act is defined?act.id:actividad.id}},
                            },
                            dataType: "json",
                            //async: true,
                            beforeSend: function () {
                            },
                        });
                        req1.done(function (respuesta) {
                            $('#actividad_personal').val(respuesta.personal);
                            $('#actividad_personal').trigger('change');
                            
                        });
            {% endif%}


                        $("#actividad_responsable").select2({
                            width: '100%', // need to override the changed default
                            ajax: {
                                url: "{{ path('gpm_responsable_getresponsables') }}",
                                data: function (params) {
                                    var query = {
                                        id: "{{ actividad.proyecto.organizacion.id }}",
                                        search: params.term,
                                        type: 'public'
                                    };
                                    // Query parameters will be ?search=[term]&type=public
                                    return query;
                                },
                                dataType: 'json'
                            },
                            language: {
                                noResults: function () {
                                    return '<a href="#modalResponsableAgregar" data-toggle="modal" >Agregar Responsable</a>'
                                }
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            }
                        });
                        $("#actividad_personal").select2({
                            width: '100%', // need to override the changed default
                             ajax: {
                                url: "{{ path('gpm_persona_get') }}",
                                data: function (params) {
                                    var query = {
                                        id: $("select#actividad_contratista option:checked").val(),
                                        search: params.term,
                                        type: 'public'
                                    };
                                    // Query parameters will be ?search=[term]&type=public
                                    return query;
                                },
                                dataType: 'json'
                            },
                            language: {
                                noResults: function () {
                                    return 'Sin resultado, Agregar en el sector de contratistas.'
                                }
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            }

                        });

                        $("#actividad_contratista").select2({
                            width: '100%', // need to override the changed default
                            ajax: {
                                url: "{{ path('gpm_contratista_get') }}",
                                data: function (params) {
                                    var query = {
                                        id: "{{ actividad.proyecto.organizacion.id }}",
                                        search: params.term,
                                        type: 'public'
                                    };
                                    // Query parameters will be ?search=[term]&type=public
                                    return query;
                                },
                                dataType: 'json'
                            },
                            language: {
                                noResults: function () {
                                    return '<a href="#modalContratistaAgregar" data-toggle="modal" >Agregar Contratista</a>'
                                }
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            }
                        });

                        $("#actividad_actgpmServicios").select2({
                            width: '100%', // need to override the changed default
                        });
                        $("#actividad_motivos").select2({
                            width: '100%', // need to override the changed default
                        });


                        $("#actividad_actgpmRefgpm").select2({
                            width: '100%', // need to override the changed default
                             ajax: {
                                url: "{{ path('gpm_referencia_get') }}",
                                        data: function (params) {
                                        var query = {
                                        id: "{{ organizacion.id }}",
                                                search: params.term,
                                                type: 'public'
                                        };
                                                // Query parameters will be ?search=[term]&type=public
                                                return query;
                                        },
                                        dataType: 'json'
                                },
                             "language": {
                                "noResults": function(){
                                         $('#tituloreferencia').text("Crear referencia general para todas las actividades");
                                           verReferencias('0');
                                    return "<a id='btnReferencia' href='#modalReferenciaNew' data-toggle='modal'>+ Referencia General</a>";
                                }
                            },
                             escapeMarkup: function (markup) {
                                 return markup;
                             }
                        });
                        
                    $('#actividad_contratista').change(function(e){
                         $('#actividad_personal').val(null);
                         $('#actividad_personal').trigger('change');
                    });
                    
            });

      
     
     $('#btnResponsableAgregar').click(function () {
         var nombre = document.getElementById('reponsable_nombre').value;
                   if(nombre.length !== 0){
                    var req = $.ajax({
                        type: 'POST',
                        url: "{{ path('gpm_responsable_newresponsable') }}",
                        data: {
                            'id': "{{ actividad.proyecto.organizacion.id}}",
                            'formResponsable': $("#formResponsableNew").serialize(), //form de productos
                        },
                        dataType: "json",
                        //async: true,
                        beforeSend: function () {
                        },
                    });
                    req.done(function (respuesta) {
                        $("#actividad_responsable").append(new Option(respuesta.nombre, respuesta.id));
                        $('#modalResponsableAgregar').modal('toggle'); //cierro el form
                        document.getElementById("formResponsableNew").reset();
                        newNotify('Exito!',"success","El responsable se creó correctamente");
                        
                    });
            }else{
                newNotify('Error!',"error","Debe contener un nombre");
              
            }
        });