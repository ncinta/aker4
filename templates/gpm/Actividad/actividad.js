
var idActividad = 0;
var idPersonal = 0;
var idServicio = 0;
var idIngreso = 0;
$(function () {
    {% if actividad is defined %}
    var min = new Date('{{actividad.proyecto.fechaInicio|date("Y/m/d H:i:s")}}');
        var diff =  new Date('{{actividad.proyecto.fechaFin|date("Y/m/d H:i:s")}}').getDate() - min.getDate();
      
        $("#actividad_fecha").datetimepicker({
            format: 'DD/MM/YYYY',
            defaultDate: min,
            minDate:min,
            maxDate: (new Date(min)).setDate(min.getDate() + diff)
        }).data("DateTimePicker").date('{{ actividad.fechaInicio|date("d/m/Y")}}');
        $("#actividad_inicio").datetimepicker({
            format: 'HH:mm',
        }).data("DateTimePicker").date('{{ actividad.fechaInicio|date("H:i")}}');
        $("#actividad_fin").datetimepicker({
            format: 'HH:mm',
        }).data("DateTimePicker").date('{{ actividad.fechaFin|date("H:i")}}');
    {% endif %}


});

function setSidebarDataActividad(id) { //abre el panel derecho con los datos de la actividad
    openNav();
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_getactividad') }}",
        data: {
            'id': id,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
            $("#content-sidebar").text("Cargando.....");
        },
    });
    req.done(function (respuesta) {
        $("#titulo-sidebar").text("");
        $("#titulo-sidebar").html(respuesta.htmlTituloSidebar);
        $("#content-sidebar").text("");
        $("#content-sidebar").html(respuesta.htmlSidebar);
    });
}

function modalDelete(id, nombre) {
    window.idActividad = id;
    $('#actividad_borrar_nombre').text('');
    $('#actividad_borrar_nombre').text(nombre);
    $('#modalActividadEliminar').modal('show');
}

function modalFinalizar(id, nombre) {
    window.idActividad = id;
    $('#actividad_finalizar_nombre').text('');
    $('#actividad_finalizar_nombre').text(nombre);
    $('#modalActividadFinalizar').modal('show');
}

function modalCerrar(id, nombre) {
    window.idActividad = id;
    $('#actividad_finalizar_nombre').text('');
    $('#actividad_finalizar_nombre').text(nombre);
    $('#modalActividadCerrar').modal('show');
}

$('#btnActividadRemover').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_remover') }}",
        data: {
            'id': window.idActividad,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#titulo-sidebar").text("");
        $("#titulo-sidebar").html(respuesta.htmlTituloSidebar);
        $("#content-sidebar").text("");
        $("#content-sidebar").html(respuesta.htmlSidebar);
        $("#tablaActividades_" + respuesta.idProyecto).text("");
        $("#tablaActividades_" + respuesta.idProyecto).html(respuesta.html);
        $('#modalActividadEliminar').modal('toggle'); //cierro el form
    });
});


function modalDeletePersonal(idAct, idPers, nombre) {
    window.idActividad = idAct;
    window.idPersonal = idPers;
    $('#actividad_persona_nombre').text('');
    $('#actividad_persona_nombre').text(nombre);
    $('#modalPersonaQuitar').modal('show');
}

$('#btnPersonaQuitar').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_quitarpersona') }}",
        data: {
            'id': window.idActividad,
            'idPersonal': window.idPersonal,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#personal").text("");
        $("#personal").html(respuesta.html);
        $('#modalPersonaQuitar').modal('toggle'); //cierro el form
    });
});



function modalDeleteServicio(idAct, idServ, nombre) {
    window.idActividad = idAct;
    window.idServicio = idServ;
    $('#actividad_servicio_nombre').text('');
    $('#actividad_servicio_nombre').text(nombre);
    $('#modalServicioEliminar').modal('show');
}

$('#btnActividadFinalizar').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_finalizar') }}",
        data: {
            'id': window.idActividad,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $('#modalActividadFinalizar').modal('toggle'); //cierro el form
        location.reload(); // refresco porque hay que ir a buscar el backend ya que la finalizo
    });
});

$('#btnActividadCerrar').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_cerrar') }}",
        data: {
            'id': window.idActividad,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $('#modalActividadCerrar').modal('toggle'); //cierro el form
        location.reload(); // refresco porque hay que ir a buscar el backend ya que la cerró
    });
});



$('#btnServicioRemover').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_removerservicio') }}",
        data: {
            'id': window.idActividad,
            'idServicio': window.idServicio,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#content-sidebar").text("");
        $("#content-sidebar").html(respuesta.htmlSidebar);
        $('#modalServicioEliminar').modal('toggle'); //cierro el form
    });
});

function modalMapaServicio(id, nombre) {
    $("#nombreVehiculo").text(nombre);

    $('#modalMapaServicio').modal('show');
}

function modalIngresoManual(id, nombre) {
    window.idActividad = id;
    $('#actividad_ingreso_manual_nombre').text('');
    $('#actividad_ingreso_manual_nombre').text(nombre);
    $('#modalIngresoManual').modal('show');
}

$('#btnActividadIngreso').click(function () {
var req = $.ajax({
type: 'POST',
        url: "{{ path('gpm_actividad_ingresomanual') }}",
        data: {
        'id': "{{ actividad is defined ? actividad.id:0}}",
                'form': $("#formIngresoManual").serialize(), //form de productos
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
});
        req.done(function (respuesta) {
                $("#ingreso_manual").text("");
                $("#ingreso_manual").html(respuesta.html);
                $('#modalIngresoManual').modal('toggle'); //cierro el form
                document.getElementById("formIngresoManual").reset();
                newNotify('Exito!',"success","El reporte se creó correctamente");
        });
});

function modalDeleteIngreso(id, nombre) {
    window.idIngreso = id;
    $('#ingreso_borrar_nombre').text('');
    $('#ingreso_borrar_nombre').text(nombre);
    $('#modalIngresoEliminar').modal('show');
}

$('#btnIngresoRemover').click(function () {
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('gpm_actividad_removeringreso') }}",
        data: {
            'id': window.idIngreso,
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        $("#ingreso_manual").text("");
        $("#ingreso_manual").html(respuesta.html);
        $('#modalIngresoEliminar').modal('toggle'); //cierro el form
        newNotify('Exito!',"success","El reporte se eliminó correctamente");
        
    });
});




