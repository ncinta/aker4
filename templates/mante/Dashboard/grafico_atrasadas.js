            function setGraficoAtrasadas(atrasadas) {
                var nuevas = new Array();
                var enCurso = new Array();
                var cerradas = new Array();
                var retrabajo = new Array();
                var normal = new Array();
                var labels = new Array();
                var backC = new Array();
                var backEc = new Array();
                var backN = new Array();
                var borderC = new Array();
                var borderEc = new Array();
                var borderN = new Array();
                var backNueva = 'rgba(54, 162, 235, 0.2)';
                var backCerrada = 'rgba(184, 237, 237, 0.2)';
                var backEnCurso = 'rgba(255, 206, 86, 0.2)';
                var borderNueva = 'rgba(54, 162, 235, 1)';
                var backRe = 'rgba(255, 99, 132, 0.2)';
                var backNor = 'rgba(235, 224, 255, 0.2)';
                var borderCerrada = 'rgba(130, 221, 221, 1)';
                var borderEnCurso = 'rgba(255, 206, 86,1)';
                var borderRe = 'rgba(255, 99, 132, 1)';
                var borderNor = 'rgba(190, 161, 244, 1)';
                var n = e = c = r = l = i = 0;
                var ctx = document.getElementById('byAtrasadas').getContext('2d');
                var ctr = document.getElementById('byRetrabajo').getContext('2d');
                for (var j in atrasadas) {
                    for (var k in atrasadas[j]) {
                        if (j == 'nueva') {
                            nuevas[n] = atrasadas[j][k];
                            labels[l] = k;
                            l++;
                            n++;
                        }
                        if (j == 'encurso') {
                            enCurso[e] = atrasadas[j][k];
                            backEc.push(backEnCurso);
                            borderEc.push(borderEnCurso);
                            e++;
                        }
                        if (j == 'cerrada') {
                            cerradas[c] = atrasadas[j][k];
                            backC.push(backCerrada);
                            borderC.push(borderCerrada);
                            c++;
                        }
                        if (j == 'retrabajo') {
                            //alert(atrasadas[j][k]);
                            retrabajo[r] = atrasadas[j][k];
                            r++;
                        }
                        if (j == 'normal') {
                            normal[i] = atrasadas[j][k];
                            backN.push(backNueva);
                            borderN.push(borderNueva);
                            i++;
                        }



                    }
                }
                var cursoChart = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: labels,
                        datasets: [{
                                label: 'Cerradas',
                                data: cerradas,
                                backgroundColor: backC,
                                borderColor: borderC,
                                borderWidth: 1

                            },
                            {
                                label: 'En Curso',
                                data: enCurso,
                                backgroundColor: backEc,
                                borderColor: borderEc,
                                borderWidth: 1

                            },
                            {
                                label: 'Nuevas',
                                data: nuevas,
                                backgroundColor: backN,
                                borderColor: borderN,
                                borderWidth: 1

                            }
                        ]
                    },
                });
                var estadoChart = new Chart(ctr, {
                    type: 'horizontalBar',
                    data: {
                        labels: labels,
                        datasets: [{
                                label: 'Retrabajo',
                                data: retrabajo,
                                backgroundColor: backRe,
                                borderColor: borderRe,
                                borderWidth: 1

                            },
                            {
                                label: 'Normal',
                                data: normal,
                                backgroundColor: backNor,
                                borderColor: borderNor,
                                borderWidth: 1

                            }
                        ]
                    },
                });

            }
   