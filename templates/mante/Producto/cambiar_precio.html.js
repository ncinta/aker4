
        var idProductoStock;
        function cambiarPrecio(idStock) {
            idProductoStock = idStock;            
            $('#modalPrecio').modal('show');   //cierro el form            
        }
        
        $(document).ready(function () {
            $('#btnCambiarPrecio').click(function () {
                var req = $.ajax({
                    type: 'POST',
                    url: "{{ path('producto_cambiarprecio') }}",
                    data: {
                        'id': idProductoStock,
                        'formPrecio': $("#formPrecio").serialize(), //form de productos
                    },
                    dataType: "json",
                    beforeSend: function () {
                    },
                });
                req.done(function (respuesta) {                
                    $("#tablaDepositos").text("");
                    $("#tablaDepositos").html(respuesta.html);
                    $('#modalPrecio').modal('toggle');   //cierro el form
                });
            });
        });        
