function procesarRespuestaEvento(respuesta) {    
    //hay eventos nuevos que deben ser atendidos
    if (respuesta['eventos'] !== null) {        
        eventos = respuesta['eventos'];
        temporales = eventos['temporales'];
      
       // console.log("cant_total: " + eventos['cant_total'] + " cant_nuevos: " + eventos['cant_nuevos'] );
        if (eventos['cant_total'] > 0) {             //si hay eventos se entra
            document.getElementById('playerevento').pause();   //detiene siempre el sonido
            document.getElementById('player').pause();   //detiene siempre el sonido
           // alert(eventos['cant_nuevos']);
            if (eventos['cant_nuevos'] > 0) {
                if (eventos['sonido'] === true) {   //reproduce sonido si esta configurado.
                    if (eventos['cant_panicos_nuevos'] !== 0) {
                        document.getElementById('player').play();
                    } else {
                        if (eventos['cant_eventos_nuevos'] !== 0) {
                            document.getElementById('playerevento').play();
                        }
                    }
                }
                PNotify.removeAll();
                if (eventos['cant_nuevos'] > 0) {
                    //avisa de los nuevos.
                    newNotify('Eventos Nuevos !!!','warning',"<p><em>Hay " + eventos['cant_nuevos'] + " eventos pendientes de ser vistos. </em></p>");                
                } 

                //hay ultimo evento a ser mostrado
                if (eventos['body'] != null) {  
                    updateCount(eventos['cant_total']);
                    if ($('#modalEvento').is(':hidden')) {   //la pantalla esta oculta
                        //muestro el ultimo evento acontecido.  
                       // alert(eventos['body']);
                        updateDialog(eventos['body']);                                
                        showModal(true, eventos);   //muesto la pantalla.
                    } else {
                            updateDialog(eventos['body']);
                    }                            
                } else {
                    showModal(false, null);   //muesto la pantalla.
                }
            }
            if (eventos['button_evento'] != null) {  //que todos los eventos los ponga juntos sea panico u otros
                $("#zonenotifevento").text("");
                $("#zonenotifevento").append(eventos['button_evento']);

                 $(".btn-notification-aker").text("");
                 $(".btn-notification-aker").append(eventos['button_evento_aker']);
               
            }
          
        }
    }    
}

function consultarEventos() {
    $.get("{{ url('app_evento_query') }}",
        null,
        function(respuesta) {            
            procesarRespuestaEvento(respuesta);
        }
    , 'json');

    return false;
}

function consultarEventosAker() {
 //   $.get("{{ url('app_evento_query') }}",
 //       null,
 //       function(respuesta) {            
 //           procesarRespuestaEvento(respuesta);
 //       }
 //   , 'json');

    return false;
}

function setupVisto(id) {  
    console.log("setup visto -> "+id);
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('main_evento_setupvisto') }}",
        data: {'id': id},
        dataType: "json"
    });    
    request.done(function(respuesta) {
        updateDialog(respuesta['eventos']['body']);      
        updateCount(respuesta['eventos']['cant_total']);
        if(respuesta['eventos']['cant_total'] == 0){
            showModal(false, null); 
        }
        $("#zonenotifevento").text("");
        $("#zonenotifevento").append(respuesta['eventos']['button_evento']);
    });
    return true;
}

function showModal(show, eventos) {
    if (show) { 
        $('#modalEvento').modal('show'); //muestro el dialog.
    } else {        
        $('#modalEvento').modal('hide'); //oculto el dialog.
    }    
}

function updateDialog(data) {
    $("#alerta_evento_data").text("");
    $("#alerta_evento_data").append(data);                                
}



function updateCount(count) {
    $("#alerta_evento_count").text("");
    $("#alerta_evento_count").append("Hay " + count + " nuevos eventos sin atender.");
}

idEvHist = 0;
function showModalRegistrar(show, id) {
    document.idEvHist = id;
    if (show == 1) {        
        $('#modalRegistrar').modal('show'); //muestro el dialog.
    } else {        
        $('#modalRegistrar').modal('hide'); //oculto el dialog.
    }    
}

function registrar(){
    var req = $.ajax({
        type: 'POST',
        url: "{{ path('main_panico_registrarajax') }}",
        data: {
            'formRegistrar': $("#formRegistrar").serialize(), //form de productos
            'id': document.idEvHist
        },
        dataType: "json",
        //async: true,
        beforeSend: function () {
        },
    });
    req.done(function (respuesta) {
        showModalRegistrar(2, 0);
        updateDialog(respuesta['eventos']['body']);      
        updateCount(respuesta['eventos']['cant_total']);
        if(respuesta['eventos']['cant_total'] == 0){
            showModal(false, null); 
        }
        $("#zonenotifevento").text("");
        $("#zonenotifevento").append(respuesta['eventos']['button_evento']);
  });
}


