function consultarMantenimientos() {    
    $.get('{{ url("tareamant_queryAJAX") }}',
        null,
        function(respuesta) {            
            if (respuesta != null) {
                if (respuesta['cantidad'] > 0) {
                    if (respuesta['nuevas'] > 0) {
                        newNotify('Mantenimiento !!!','warning','Hay nuevas tareas de mantenimiento para realizarse');                          
                    }
                }
                $("#zonenotifmant").text("");
                $("#zonenotifmant").append(respuesta['html']);    
            }
            setTimeout(function() { consultarMantenimientos(); } , 60000 *5 );  //llamo a la consulta 5min.
        }, 
        'json'
    );
};

function renderBtnMantenimientos() {
    var html = '{{ app.session.get("btnmantpend")|raw }}';
    if (html === "") {
    //console.log('html');
        consultarMantenimientos();
    } else {
        $("#zonenotifmant").text("");
        $("#zonenotifmant").html(html);   
    }
}

function consultarMantenimientosAker() {    
    $.get('{{ url("tareamant_queryAJAX") }}',
        null,
        function(respuesta) {
            if (respuesta != null) {
                if (respuesta['cantidad'] > 0) {
                    if (respuesta['nuevas'] > 0) {
                        newNotify('Mantenimiento !!!','warning','Hay nuevas tareas de mantenimiento para realizarse');                                         
                    }
                }
                $(".btn-notification-mante").text("");
                $(".btn-notification-mante").append(respuesta['html']);    
            }
            setTimeout(function() { consultarMantenimientos(); } , 60000 *5 );  //llamo a la consulta 5min.
        }, 
        'json'
    );
};

