var contador = 15;
var request;

function refresh() {
    contador = 15;
    request = $.ajax({
        type: 'GET',
        url: "{{ path('servicio_refresh') }}",
        data: {
            'id': servicioId
        },
        dataType: "json"
    });
    request.done(function (respuesta) {
        $("#datos").text("");
        $('#datos').append(respuesta['html']);
        $("#domicilio").text("");
        $('#domicilio').append(respuesta['data']['domicilio']);
        $("#cercania").text("");
        $('#cercania').append(respuesta['data']['cercania']);
        {{ mapa.fcStopDraw() }}();
        {{ mapa.fcAddAllMarkers() }}
        (respuesta['data']);
        gotoCenter(respuesta['data']['latitud'], respuesta['data']['longitud'], false);
        {{ mapa.fcIniDraw() }}();
    });
}

function mostrarContador() {
    contador = contador - 1;
    $("#contador").html('Los datos se actualizarán en: ' + contador + ' segundos.');
    if (contador == 0 || contador == 15) {
        $("#contador").html('Actualizando....');
    }
}

int60 = setInterval('refresh()', 15000);
int1 = setInterval('mostrarContador()', 1000);