
function consultarChofer() {
    
    var req = $.ajax({
        type: 'GET',
        url: "{{ path('chofer_historico') }}",
        data: {
            'formHistChofer': $("#formHistChofer").serialize(), // form de productos
            'servicio': servicioId, // form de productos
        },
        dataType: "json",
        beforeSend: function () { // $("#modalLoad").modal({ backdrop: 'static', keyboard: false });
        }
    });
    req.done(function (respuesta) {
        $("#tableHistorico").text("");
        $("#tableHistorico").html(respuesta.html);
        $('#modalLoad').modal('hide');
    });
}

