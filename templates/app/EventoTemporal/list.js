var contador = 15; //que cargue un par de segundos despues que el ajax del popup
var reloadOnPlay = true;
var constContador = 15

$(document).ready(function () {
    enabledEventos = false;    //deshabilito la consulta a nivel global de popup
    int1 = setInterval('mostrarContador()', 1000);
    int2 = setInterval(reload, constContador * 1000);
    reloadOnPlay = true;
});

function mostrarContador() {
    window.contador = window.contador - 1;
    $("#btnActualizar").text("");
    $("#btnActualizar").append("<span class='fa fa-refresh'></span>&nbsp;");
    $("#btnActualizar").append(window.contador.toString().padStart(2, '0'));
    if (window.contador == 0 || window.contador == constContador) {
        window.contador = constContador;
        $("#btnActualizar").attr("disabled", true);
        $("#btnActualizar").text("");
        $("#btnActualizar").append("<span class='fa fa-cloud-upload'></span>&nbsp;00");
    }
}

function reload() {
    window.contador = constContador;

    var req = $.ajax({
        type: 'GET',
        url: "{{ path('app_eventotemporal_reload') }}",
        //data: {'evento': null},
        dataType: "json",
    });

    req.done(function (respuesta) {
        $("#onlineTemporal").text("");
        $("#onlineTemporal").append(respuesta.tabla);
        $("#btnActualizar").removeAttr("disabled");   //habilito el boton
        if (respuesta.playsound) {
            document.getElementById('player').play();
        }
        //alert(respuesta.body);

    });
}

function pauseReload() {
    if (reloadOnPlay) {   //esta iniciado el clock lo pauso       
        reloadOnPlay = false;
        clearInterval(int1);
        clearInterval(int2);
        $("#btnPauseReload").text("");
        $("#btnPauseReload").append("<span class='fa fa-play'></span>&nbsp; Iniciar");
        $('#btnActualizar').addClass('btn-warning');
        $('#btnActualizar').removeClass('btn-success');
    } else {             //esta en pausa el clock lo inicio.       
        reloadOnPlay = true;
        int1 = setInterval('mostrarContador()', 1000);
        int2 = setInterval(reload, constContador * 1000);
        contador = constContador;
        mostrarContador();
        $("#btnPauseReload").text("");
        $("#btnPauseReload").append("<span class='fa fa-pause'></span>&nbsp; Pausar");
        $('#btnActualizar').removeClass('btn-warning');
        $('#btnActualizar').addClass('btn-success');
    }
}

function playSound() {
    document.getElementById('player').play();
}

function batch(accion) {
    $('#btnVisto').css('display', 'none');
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('app_eventotemporal_batch') }}",
        data: {
            'accion': accion,
            'checks': getValues()
        },
        beforeSend: function () {
            // setting a timeout
            message('Espere, cargando...');
        },
        dataType: "json"
    });
    request.done(function (ids) {
        message('Actualizando...');
        $("#onlineTemporal").text("");
        reload();
    });

}

function message(text) {
    $('#espere').text(text);
}

var checkedAll = false;

// toma los valores de los checks y genera un string con todos los ids de los grupos.
function getValues() {
    var allvalues = '';
    for (var i = 0; i < document.formCheck.elements.length; i++) {
        if (document.formCheck.elements[i].type === "checkbox") {
            if (document.formCheck.elements[i].checked) {
                if (allvalues !== '') {
                    allvalues = allvalues + ' ' + document.formCheck.elements[i].value;
                } else {
                    allvalues = document.formCheck.elements[i].value;
                }
            }
        }
    }
    return allvalues;
}
// cuando se checkea todos.
function checkAll() {
    checkedAll = !checkedAll;
    if (checkedAll) {
        $('#hrefselectAll').text('Desmarcar todos');
    } else {
        $('#hrefselectAll').text('Marcar todos');
    }
    $("input:checkbox").prop('checked', checkedAll);
    clickCheck();
}

function clickCheck() {
    var count = 0;
    for (var i = 0; i < document.formCheck.elements.length; i++) {
        if (document.formCheck.elements[i].checked) {
            count++;
        }
    }
    if (count == 0) {
        $('#btnVisto').css('display', 'none');
    } else {
        $('#btnVisto').css('display', 'block');
    }
}

$(function () {
    $('#historial_servicio').select2();
});

function grabarVisto(id, consultar) {
    console.log("grabar visto -> " + id);
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('app_eventotemporal_grabarvisto') }}",
        data: { 'id': id },
        dataType: "json"
    });
    request.done(function (respuesta) {
        reload();  //vuelvo a consultar los eventos.
    });
    return true;
}

idEvHist = 0;
function showModalComentar(show, id) {    
    document.idEvHist = id;
    if (show == 1) {        
        console.log(document.getElementById("titulo"+id).innerText);

        $("#commentTitle").html(document.getElementById("titulo"+id).innerText);

        //debo obtener el evento desde el back
        var request = $.ajax({
            type: 'GET',
            url: "{{ path('app_eventotemporal_obtener') }}",
            data: { 'id': id },
            dataType: "json"
        });
        request.done(function (respuesta) {
            $("#commentData").html(respuesta['evento']);
            //$("#commentProto").html('evento');    
        });
        
        document.getElementById("formComentario").reset();
        pauseReload();
        $('#modalComentar').modal('show'); //muestro el dialog.
    } else {        
        $('#modalComentar').modal('hide'); //oculto el dialog.
    }    
}

$("#btnComentarioAdd").click(function () {    
    reload();
    pauseReload(); //largo el timer
	var request = $.ajax({
		type: "POST",
		url: "{{ path('app_eventotemporal_comentar') }}",
		data: {
			'formComentario': $("#formComentario").serialize(), //form de comentario
            'id': document.idEvHist
		},
		dataType: "json",		
		beforeSend: function () {
			//$("#modalLoad").modal({ backdrop: 'static', keyboard: false });
		},
	});
	request.done(function (respuesta) {
		document.getElementById("formComentario").reset();
		$("#modalComentar").modal("toggle");				
		newNotify('Exito!','success','se registró su comentario correctamente');
	});
});