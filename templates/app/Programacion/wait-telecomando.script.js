var count = 0;    //es global
var timeEspera = 0;  //es global

function finish(msj){
    $( "#progressbar" ).progressbar({ value: 100 });
    count = 0;
    clearInterval(timeEspera);   //detengo el timer
    $('#bitacora').append('<h3>'+ msj + '</h3>');
}

function launchEspera(id, idCom) {
    var vid = id;
    var vidCom = idCom;
    count = 0;
    timeEspera = setInterval(
            function() {
                count = count +5;
                var request = $.ajax({
                    type: 'GET',
                    url: "{{ path('main_programacion_cortar_wait') }}",
                    data: {'id': vid, 'idCom': vidCom},
                    dataType: "html",
                    beforeSend: function() {
                        $('#bitacora').append(' .');
                        $("#progressbar").progressbar({ value: count/2 });
                    },
                });
                request.done(function(html) {
                    count = count +5;
                    $( "#progressbar" ).progressbar({ value: count/2 });
                    if (count === 200) {                            
                        finish('El comando se ejecutará cuando el equipo vuelva a reportar. Gracias..');
                        $("#btn-status-cerrar").prop('disabled', '');
                    }
                    if (html != '*') {
                        finish(html);
                        $("#btn-status-cerrar").prop('disabled', '');
                    }
                });
            }
    , 3000);
}