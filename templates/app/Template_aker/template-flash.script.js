function newNotify(title, type, text) {
    $('.ui-pnotify').remove(); //elimino el cartel previo si es que hay
    new PNotify({
        styling: 'bootstrap3',
        addclass: "stack-topleft",
        type: type,
        title: title,
        text: text,
        icon: 'picon iconic-icon-check-alt white',
        opacity: 0.95,
        history: false,
        sticker: false
    });
}
;