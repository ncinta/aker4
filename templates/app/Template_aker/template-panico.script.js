var idEv = 0;   //mantiene el id del ultimo evento que se esta mostrando en pantalla.

function procesarRespuestaEvento(respuesta) {
    //hay eventos nuevos que deben ser atendidos
    if (respuesta['eventos'] !== null) {        
        eventos = respuesta['eventos'];
       // console.log("cant_total: " + eventos['cant_total'] + " cant_nuevos: " + eventos['cant_nuevos'] );
        if (eventos['cant_total'] > 0) {             //si hay eventos se entra
            document.getElementById('playerevento').pause();   //detiene siempre el sonido
            document.getElementById('player').pause();   //detiene siempre el sonido
            if (eventos['cant_nuevos'] > 0) {
                if (eventos['sonido'] === true) {   //reproduce sonido si esta configurado.
                    if (eventos['cant_panicos_nuevos'] !== 0) {
                        document.getElementById('player').play();
                    } else {
                        if (eventos['cant_eventos_nuevos'] !== 0) {
                            document.getElementById('playerevento').play();
                        }
                    }
                }
                PNotify.removeAll();
                if (eventos['titulos_eventos'] != null) {
                    //avisa de los nuevos.
                    newNotify('Eventos Nuevos !!!',"warning","<p><em>Hay " + eventos['cant_eventos_nuevos'] + " eventos pendientes de ser vistos. </em></p>"+ eventos['titulos_eventos']);
                    
                } 
                
                if (eventos['titulos_panicos'] != null) {
                    //avisa de los nuevos.
                    newNotify('Pánicos Nuevos !!!',"warning","<p><em>Hay " + eventos['cant_panicos_nuevos'] + " panicos pendientes de ser vistos. </em></p>"+ eventos['titulos_panicos']);

                } 

                //hay ultimo evento a ser mostrado
                if (eventos['ultimo'] != null) {  
                    updateCount(eventos['cant_nuevos']-1);
                    if ($('#modalEvento').is(':hidden')) {   //la pantalla esta oculta
                        //muestro el ultimo evento acontecido.  
                        updateDialog(eventos['ultimo']);                                
                        showModal(true, eventos);   //muesto la pantalla.
                    } else {
                        console.log("idEv:" + idEv +" evento.id:" + eventos['ultimo']['id']);
                        if (idEv !== eventos['ultimo']['id']) {
                            updateDialog(eventos['ultimo']);
                        }
                    }                            
                } else {
                    showModal(false, null);   //muesto la pantalla.
                }
            }
            if (eventos['button_evento'] != null) {    //dibuja el boton en el topbar
                $("#zonenotifevento").text("");
                $("#zonenotifevento").append(eventos['button_evento']);
            }
            if (eventos['button_panico'] != null) {
                $("#zonenotifpanico").text("");
                $("#zonenotifpanico").append(eventos['button_panico']);
            }
        }
    }    
}

var segundos = 60;

function consultarEventos() {
    $.get("{{ url('app_evento_query') }}",
        null,
        function(respuesta) {            
            procesarRespuestaEvento(respuesta);
        }
    , 'json');

    return false;
};

function setupVisto(id, consultar) {
    console.log("setup visto -> "+id);    
    var request = $.ajax({
        type: 'POST',
        url: "{{ path('main_evento_setupvisto') }}",
        data: {'id': id},
        dataType: "json"
    });    
    request.done(function(respuesta) {
        if (consultar) {
            consultarEventos();  //vuelvo a consultar los eventos.
            //console.log("despues de consultarEventos")
        }
    });
    return true;
}

function showModal(show, eventos) {
    if (show) {        
        $('#modalEvento').modal('show'); //muestro el dialog.
    } else {        
        $('#modalEvento').modal('hide'); //oculto el dialog.
    }    
}

function updateDialog(data) {
    idEv = data['id'];   //actualizo el ID con el evento que estoy mostrando
    console.log("idEv seteado a -> " + idEv)
    $("#alerta_evento_title").text("");
    $("#alerta_evento_title").append(data['title']);

    $("#alerta_evento_data").text("");
    $("#alerta_evento_data").append(data['body']);                                
}

function updateCount(count) {
    $("#alerta_evento_count").text("");
    
    if (count > 0) {
        if (count > 1) {
            $("#alerta_evento_count").append("Hay " + count + " eventos mas sin atender.");
        } else {
            $("#alerta_evento_count").append("Hay " + count + " evento mas sin atender.");
        }
        $("#buttonSiguienteEvento").show();
    } else {
        $("#buttonSiguienteEvento").hide();
    }    
}    

$(document).ready(function() {
    
    $("#buttonVistaEvento").click(function() {
        setupVisto(idEv, false);   //solo seteo visto
        showModal(false, null);
        console.log("click en VistaEvento")
    });
    
    $("#buttonSiguienteEvento").click(function() {
        setupVisto(idEv, true);   //solo seteo visto
        console.log("click en SiguienteEvento")        
    });
});