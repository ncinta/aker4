function AdjustFullSize() {
     var screenHeight = 450;
     var screenWidth = 610;

     if (window.innerHeight) {
        screenHeight = window.innerHeight;
        screenWidth = window.innerWidth;
     } else if (document.body) {
        screenHeight = document.body.clientHeight;
        screenWidth = document.body.clientWidth;
     }

     mainDivHeight = screenHeight - 125 - 45; //125 header +45 footer
     mainDivWidth = screenWidth - 0;

     document.getElementById('main').style.height = mainDivHeight + 'px';
     //document.getElementById('main').style.width = mainDivWidth + 'px';

     if(typeof document.getElementById("panel") !== "undefined"){ 
        $('#panel').height(mainDivHeight - 20);
        if(typeof document.getElementById("panel-body") !== "undefined"){ 
            if (document.getElementById("page-header") !== null) { 
                $('#panel-body').height(mainDivHeight - 80);
            } else {
                $('#panel-body').height(mainDivHeight - 50);
            }
        }
     }    
}