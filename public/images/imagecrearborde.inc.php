<?php
function imagecrearborde ($src, $br, $bg, $bb) {
    $is = imagecreatefrompng ($src);
    $im = imagecreatetruecolor (24, 24);
    imagealphablending ($im, false);
    imagefilledrectangle ($im, 0, 0, 24, 24, imagecolorallocatealpha ($im, 0, 0, 0, 0));
    for ($y=1; $y<23; $y++) {
	for ($x=1; $x<23; $x++) {
	    if (imagecolorat ($is, $x, $y) == 0x00FFFFFF) {
		if ((imagecolorat ($is, $x-1, $y-2) &
		     imagecolorat ($is, $x  , $y-2) &
		     imagecolorat ($is, $x+1, $y-2) &
		     imagecolorat ($is, $x-2, $y-1) &
		     imagecolorat ($is, $x-1, $y-1) &
		     imagecolorat ($is, $x  , $y-1) &
		     imagecolorat ($is, $x+1, $y-1) &
		     imagecolorat ($is, $x+2, $y-1) &
		     imagecolorat ($is, $x-2, $y  ) &
		     imagecolorat ($is, $x-1, $y  ) &
		     imagecolorat ($is, $x+1, $y  ) &
		     imagecolorat ($is, $x+2, $y  ) &
		     imagecolorat ($is, $x-2, $y+1) &
		     imagecolorat ($is, $x-1, $y+1) &
		     imagecolorat ($is, $x  , $y+1) &
		     imagecolorat ($is, $x+1, $y+1) &
		     imagecolorat ($is, $x+2, $y+1) &
		     imagecolorat ($is, $x-1, $y+2) &
		     imagecolorat ($is, $x  , $y+2) &
		     imagecolorat ($is, $x+1, $y+2)) != 0x00FFFFFF)
		{
		    imagesetpixel ($im, $x, $y, imagecolorallocatealpha ($im, 0xFF, 0xFF, 0xFF, 0x0));
		}
	    }
	}
    }
    imagedestroy ($is);
    imagefilter ($im, IMG_FILTER_SMOOTH, 2);
    for ($y=0; $y<24; $y++) {
	for ($x=0; $x<24; $x++) {
	    $rgba = imagecolorat ($im, $x, $y);
	    $r = $br;
	    $g = $bg;
	    $b = $bb;
	    $a = 0x7F - (($rgba & 0xFF) >> 1);
	    imagesetpixel ($im, $x, $y, imagecolorallocatealpha ($im, $r, $g, $b, $a));
	}
    }
    return $im;
}
?>