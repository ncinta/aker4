<?php
/* Parametros que recibe el script, via variables GET:

   texto=       El texto a generar
 */

// Para las transparencias me vino bien esto: http://www.bl0g.co.uk/070327/Creating_Transparent_PNG_Images_in_GD/
// Considerar usar ImageMagik para no tener que usar utf8_decode
// No deberia autodetectar yo el encoding!!! se supone que estan todos en UTF8... que paso?
// (usuario microw, referencia "Plaza Boli'var")
$utf8 = true;
for ($i=1; $i<strlen($_GET["texto"])-1; $i++) {
    $m1 = ord(substr($_GET["texto"],$i-1,1)) >= 0x80;
    $m2 = ord(substr($_GET["texto"],$i,1)) >= 0x80;
    $m3 = ord(substr($_GET["texto"],$i+1,1)) >= 0x80;
    if ($m2 && !$m1 && !$m3) {
	$utf8 = false;
    }
}
if ($utf8) {
    $_GET["texto"] = utf8_decode ($_GET["texto"]);
}

$width = 200;
$height = 50;
$im = imagecreatetruecolor ($width, $height);
imagealphablending ($im, false);              // v--- 0 = opaco, 127 = transparente
$clear = imagecolorallocatealpha ($im, 0, 0, 0, 127);
imagefilledrectangle ($im, 0, 0, $width, $height, $clear);
imagealphablending ($im, true);
// Pintar cosas
// Mostrar tambien el texto
$texto = explode (" // ", $_GET["texto"]);
for ($i=0; $i<count($texto); $i++) {
    $borde = imagecolorallocatealpha ($im, 255, 128, 0, 0);
    $font = 3;
    $w = strlen($texto[$i]) * imagefontwidth ($font);
    for ($y=-2; $y<=2; $y++) {
        for ($x=-2; $x<=2; $x++) {
            if ($x | $y) {
                imagestring ($im, $font, ($width-$w)*0.5+$x, 2+$y+$i*15, $texto[$i], $borde);
            }
        }
    }
    imagestring ($im, $font, ($width-$w)*0.5, 2+$i*15, $texto[$i], imagecolorallocatealpha ($im, 255, 255, 255, 0));
}
// Listo
imagealphablending ($im, false);
imagesavealpha ($im, true);
header ("Content-Type: image/png");
imagepng ($im);
imagedestroy ($im);
?>