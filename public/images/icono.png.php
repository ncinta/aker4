<?php

/* Parametros que recibe el script, via variables GET:

  angulo=      El angulo, de 0 a 359
  color=       El nivel de color, de 0 a 100
 */

// Para las transparencias me vino bien esto: http://www.bl0g.co.uk/070327/Creating_Transparent_PNG_Images_in_GD/

if (isset($_GET["s"])) {
    $width = $_GET["s"];
    $height = $_GET["s"];
} else {
    $width = 24;
    $height = 24;
}
$im = imagecreatetruecolor($width, $height);
imagealphablending($im, false);              // v--- 0 = opaco, 127 = transparente
$clear = imagecolorallocatealpha($im, 0, 0, 0, 127);
imagefilledrectangle($im, 0, 0, $width, $height, $clear);
imagealphablending($im, true);
// Pintar cosas
// Cargar la flecha segun el angulo
$i = $_GET["angulo"] / 10.0;
settype($i, "integer");
if ($_GET["angulo"] == -1) {
    $arrow_src = "img/donut.png";
} else {
    $arrow_src = "img/arrow$i.png";
}
$arrow = imagecreatefrompng($arrow_src);
imagealphablending($arrow, false);
// Tintar la flecha, segun el valor de color
//  0..50  -> blanco..amarillo
// 50..100 -> amarillo..rojo
$color = $_GET["color"];
settype($color, "integer");
if ($color > 0 && $color <= 40) {
    $rgb = '#eefd9e';
} elseif ($color > 40 && $color <= 60) {
    $rgb = '#DFD907';
} elseif ($color > 60 && $color <= 80) {
    $rgb = '#DF7F07';
} elseif ($color > 80 && $color <= 100) {
    $rgb = '#E56000';
} elseif ($color > 100) {
    $rgb = '#E50D00';
} else {
    $rgb = '#FFFFFF';
}

for ($y = 0; $y < 24; $y++) {
    for ($x = 0; $x < 24; $x++) {
        $rgba = imagecolorat($arrow, $x, $y);  //devuelve el color de ese punto
        $r = ($rgba >> 16) & 0xFF;  //obtiene el red
        $g = ($rgba >> 8) & 0xFF;   //obtiene el green
        $b = $rgba & 0xFF;          //obtiene el blue
        $a = $rgba >> 24;           //obtiene la transparencia

        if ($a == 0 && $r == 255) {
            $r = hexdec(substr($rgb, 1, 2));
            $g = hexdec(substr($rgb, 3, 2));
            $b = hexdec(substr($rgb, 5, 2));
        } else {
            $r = 0x00;
            $g = 0x00;
            $b = 0x00;
        }
        $rgba = imagecolorallocatealpha($arrow, $r, $g, $b, $a);   //genera un nuevo color
        imagesetpixel($arrow, $x, $y, $rgba);  //asigna el color creado antes al pixel
    }
}


imagecopy($im, $arrow, ($width - 24) / 2, ($height - 24) / 2, 0, 0, 24, 24);   //copia parte de la imagen
// Superponer un borde si se requiere
if (isset($_GET["borde"])) {
    $r = ('0x' . substr($_GET["borde"], 0, 2)) * 1;
    $g = ('0x' . substr($_GET["borde"], 2, 2)) * 1;
    $b = ('0x' . substr($_GET["borde"], 4, 2)) * 1;
    require_once ('imagecrearborde.inc.php');
    $su = imagecrearborde($arrow_src, $r, $g, $b);
    imagecopy($im, $su, 0, 0, 0, 0, 24, 24);
}
// Poner el icono de la bateria, si corresponde
if (isset($_GET["bateriacritica"])) {
    $imbc = imagecreatefrompng("img/bateriacritica.png");
    $bcsx = imagesx($imbc);
    $bcsy = imagesy($imbc);
    imagecopy($im, $imbc, $width - $bcsx, $height - $bcsy, 0, 0, $bcsx, $bcsy);
    imagedestroy($imbc);
}
// Poner la estrellita, si corresponde
/*
  if (isset($_GET["estrellita"])) {
  //$imbc = imagecreatefrompng ("star-1.png");
  $imbc = imagecreatefrompng ("estrellita3.png");
  $bcsx = imagesx ($imbc);
  $bcsy = imagesy ($imbc);
  //imagecopy ($im, $imbc, $width-$bcsx, 0, 0, 0, $bcsx, $bcsy);
  imagecopy ($im, $imbc, 0, $height-$bcsy, 0, 0, $bcsx, $bcsy);
  imagedestroy ($imbc);
  }
 */
// Listo
imagealphablending($im, false);
imagesavealpha($im, true);
header("Content-Type: image/png");
imagepng($im);
imagedestroy($im);
?>