function getHoraLocal () {
    var ahora = new Date();
    return ahora.getHours() * 3600 + ahora.getMinutes() * 60 + ahora.getSeconds();
}
var referencia_servidor = 62622;
var referencia_local = getHoraLocal();
function trunc (x) {
    return x - x % 1;
}
function mostrarHora () {
    var oHora = document.getElementById("hora");
    var hora_actual = new Date();
    hora_actual = hora_actual.getHours() * 3600 + hora_actual.getMinutes() * 60 + hora_actual.getSeconds();
    //hora_actual = hora_actual - referencia_local + referencia_servidor;
    while (hora_actual < 0) {
        hora_actual += 86400;
    }
    hora_actual %= 86400;
    var texto = "";
    var hora = trunc(hora_actual / 3600);
    var minuto = trunc(hora_actual / 60) % 60;
    var segundo = hora_actual % 60;
    if (hora < 10) texto += "0";
    texto += hora + ":";
    if (minuto < 10) texto += "0";
    texto += minuto + ":";
    if (segundo < 10) texto += "0";
    texto += segundo;
    oHora.innerHTML = texto;
}
