
(function($){var pu_log=function(msg){try{console.log(msg);}catch(err){}};$.PeriodicalUpdater=function(url,options,callback,autoStopCallback){var settings=jQuery.extend(true,{url:url,cache:false,method:'GET',data:'',minTimeout:1000,maxTimeout:8000,multiplier:2,maxCalls:0,autoStop:0,verbose:0},options);var timer=null;var timerInterval=settings.minTimeout;var maxCalls=settings.maxCalls;var autoStop=settings.autoStop;var calls=0;var noChange=0;var originalMaxCalls=maxCalls;var reset_timer=function(interval){if(timer!==null){clearTimeout(timer);}
timerInterval=interval;timer=setTimeout(getdata,timerInterval);};var boostPeriod=function(){if(settings.multiplier>=1){before=timerInterval;timerInterval=timerInterval*settings.multiplier;if(timerInterval>settings.maxTimeout){timerInterval=settings.maxTimeout;}
after=timerInterval;reset_timer(timerInterval);}
after=timerInterval;pu_log('adjusting timer from '+before+' to '+after+'.',1);reset_timer(timerInterval);};var ajaxSettings=jQuery.extend(true,{},settings);if(settings.type&&!ajaxSettings.dataType){ajaxSettings.dataType=settings.type;}
if(settings.sendData){ajaxSettings.data=settings.sendData;}
ajaxSettings.type=settings.method;ajaxSettings.ifModified=true;var handle={restart:function(newInterval){maxCalls=originalMaxCalls;calls=0;noChange=0;reset_timer(newInterval||timerInterval);return;},stop:function(){maxCalls=-1;return;}};function getdata(){var toSend=jQuery.extend(true,{},ajaxSettings);if(typeof(options.data)=='function'){toSend.data=options.data();if(toSend.data){if(typeof(toSend.data)=="number"){toSend.data=toSend.data.toString();}}}
if(maxCalls===0){$.ajax(toSend);}else if(maxCalls>0&&calls<maxCalls){$.ajax(toSend);calls++;}}
var remoteData=null;var prevData=null;ajaxSettings.success=function(data){remoteData=data;};ajaxSettings.complete=function(xhr,success){if(maxCalls===-1){return;}
if(success=="success"||success=="notmodified"){var rawData=$.trim(xhr.responseText);if(rawData=='STOP_AJAX_CALLS'){handle.stop();return;}
if(prevData==rawData){if(autoStop>0){noChange++;if(noChange==autoStop){handle.stop();if(autoStopCallback){autoStopCallback(noChange);}
return;}}
boostPeriod();}else{noChange=0;reset_timer(settings.minTimeout);prevData=rawData;if(remoteData===null){remoteData=rawData;}
if((ajaxSettings.dataType==='json')&&(typeof(remoteData)==='string')&&(success=="success")){remoteData=JSON.parse(remoteData);}
if(settings.success){settings.success(remoteData,success,xhr,handle);}
if(callback){callback(remoteData,success,xhr,handle);}}}
if(settings.complete){settings.complete(xhr,success);}
remoteData=null;};ajaxSettings.error=function(xhr,textStatus){pu_log("Error message: "+textStatus+" (In 'error')",2);if(textStatus!="notmodified"){prevData=null;reset_timer(settings.minTimeout);}
if(settings.error){settings.error(xhr,textStatus);}};$(function(){if(settings.runatonce){getdata();}else{reset_timer(timerInterval);}});return handle;};})(jQuery);