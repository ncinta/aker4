<?php

namespace GMaps\Util;

class EncodeHelper
{
    // Sign a URL with a given crypto key. Note that this URL must be properly URL-encoded
    public static function signUrl($url_to_sign, $privateKey){
        // parse the url
        $url = parse_url($url_to_sign);

        $urlPartToSign = $url['path'] . "?" . $url['query'];

        // Decode the private key into its binary format
        $decodedKey = EncodeHelper::decodeBase64UrlSafe($privateKey);

        // Create a signature using the private key and the URL-encoded string using HMAC SHA1. This signature will be binary.
        $signature = hash_hmac("sha1",$urlPartToSign, $decodedKey,  true);

        $encodedSignature = EncodeHelper::encodeBase64UrlSafe($signature);

        return $url_to_sign."&signature=".$encodedSignature;
    }
    
    // Encode a string to URL-safe base64
    public static function encodeBase64UrlSafe($value){
      return str_replace(array('+', '/'), array('-', '_'), base64_encode($value));
    }

    // Decode a string from URL-safe base64
    public static function decodeBase64UrlSafe($value){
      return base64_decode(str_replace(array('-', '_'), array('+', '/'), $value));
    }
}
// http://gmaps-samples.googlecode.com/svn/trunk/urlsigning/UrlSigner.php-source
