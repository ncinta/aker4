<?php

namespace GMaps\Util;

use GMaps\Util\EncodeHelper;

class GMHelper
{
    protected static $client_id = 'gme-securityconsultant';
    protected static $cripto_key = '1_VRrkRiPWnoHXipBQ1zvxiaEuA=';
                                  //1/VRrkRiPWnoHXipBQ1zvxiaEuA=
    protected static $key = 'AIzaSyDibOaHHnzyJjWF2RELbKW_ib0QWcaIlcQ';
    protected static $region = 'ar';
    protected static $cargado_gmaps_api = false; 
    protected static $cargado_helpers_map = false; 
    
    public static function setKey($key){
        GMHelper::$key = $key;
    }
    
    public static function setClientId($client_id){
        GMHelper::$client_id = $client_id;
    }
    
    public static function setCriptoKey($cripto_key){
        GMHelper::$cripto_key = $cripto_key;
    }
    
    public static function setRegion($region){
        GMHelper::$region = $region;
    }
    
    public static function isCargadaAPI(){
        return GMHelper::$cargado_gmaps_api;
    }
    
    public static function setCargadaAPI($cargada){
        GMHelper::$cargado_gmaps_api = $cargada;
    }
    
    public static function getAPI(){
        $tipo = 0; //nada
      //  $url = 'http://maps.googleapis.com/maps/api/js?sensor=false&language=es&v=3&libraries=geometry';
        $url = 'https://maps.googleapis.com/maps/api/js?language=es&v=quarterly&libraries=geometry';


        $api = '';
        if(GMHelper::$client_id){
            $api = '&client='.GMHelper::$client_id;
            $tipo = 2; //enterprise
        } elseif(GMHelper::$key){
            $api = '&key='.GMHelper::$key;
            $tipo = 1; //normal
        }
        
        //die('////<pre>'.nl2br(var_export($url.$api, true)).'</pre>////');
        return array($url.$api, $tipo);
    }
    
    public static function getGeocoderAPI(){
        $tipo = 0; //nada
        $url = 'http://maps.google.com/maps/geo?output=xml&hl=es';
        
        if(GMHelper::$client_id){
           // $url = 'http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&language=es';
            $url = 'https://maps.googleapis.com/maps/api/geocode/xml?language=es';
            if(GMHelper::$region){
                $url .= '&region='.GMHelper::$region;
            }
            $url .= '&client='.GMHelper::$client_id;
            $tipo = 2; //enterprise
        } elseif(GMHelper::$key){
            $url = 'http://maps.google.com/maps/geo?output=xml&hl=es&key='.GMHelper::$key;
            $tipo = 1; //normal
        }
        
        return array($url, $tipo);
    }
    
    public static function encodeUrl($url, $tipo){
        if($tipo >= 2){
            return EncodeHelper::signUrl($url, GMHelper::$cripto_key);
        } else {
            return $url;
        }
    }
    
    public static function isCargadoHelpersMap(){
        return GMHelper::$cargado_helpers_map;
    }
    
    public static function setCargadoHelpersMap($cargada){
        GMHelper::$cargado_helpers_map = $cargada;
    }

    public static function newPolyShapeParam($coords){
        $cs = "";

        if(is_array($coords)){
            $pri = true;
            foreach($coords as $c){
                if(!$pri){
                    $cs .= ", ";
                }
                if(is_array($c)){
                    $cs .= "{$c[0]}, {$c[1]}";
                } else {
                    $cs .= "{$c}";
                }
                $pri = false;
            }
        } else {
            $cs = $coords;
        }
        
        return "{$cs}";
    }

    public static function newPolyShape($coords){
        return GMHelper::newMarkerShape('poly', GMHelper::newPolyShapeParam($coords));
    }
    
    public static function newRectShapeParam($x1, $y1, $x2, $y2){
        return "{$x1}, {$y1}, {$x2}, {$y2}";
    }
    
    public static function newRectShape($x1, $y1, $x2, $y2){
        return GMHelper::newMarkerShape('rect', GMHelper::newRectShapeParam($x1, $y1, $x2, $y2));
    }
    
    public static function newCircleShapeParam($x, $y, $r){
        return "{$x}, {$y}, {$r}";
    }
    
    public static function newCircleShape($x, $y, $r){
        return GMHelper::newMarkerShape('circle', GMHelper::newCircleShapeParam($x, $y, $r));
    }
    
    public static function newMarkerShape($type, $coords){
        return "{type: '{$type}', coord: [{$coords}]}";
    }
    
    public static function newPointPath($coords){
        $cs = "";

        if(is_array($coords)){
            $pri = true;
            foreach($coords as $c){
                if(!$pri){
                    $cs .= ", ";
                }
                if(is_array($c)){
                    $cs .= GMHelper::newLatLon($c[0], $c[1]);
                    $pri = false;
                } 
            }
        } else {
            $cs = $coords;
        }
        
        return "{$cs}";
    }
    
    public static function newSize($width, $height){
        return "new google.maps.Size({$width}, {$height})";
    }
    
    public static function newPoint($x, $y){
        return "new google.maps.Point({$x}, {$y})";
    }
    
    public static function newLatLon($lat, $long, $no_wrap=true){
        return "new google.maps.LatLng({$lat}, {$long}, ".($no_wrap ? 'true' : 'false').")";
    }
    
    public static function newBounds($pto_so, $pto_ne){
        return "new google.maps.LatLngBounds({$pto_so}, {$pto_ne})";
    }
    
    public static function newBoundsLL($lat_so, $long_so, $lat_ne, $long_ne){
        return GMHelper::newBounds(GMHelper::newLatLon($lat_so, $long_so), GMHelper::newLatLon($lat_ne, $long_ne));
    }
    
    public static function newMarkerImage($url, $size=null, $origin=null, $anchor=null, $scaledSize=null){
        $r =  "new google.maps.MarkerImage('{$url}'";
        
        if($size){
            $r .=  ", {$size}";
            if($origin){
                $r .=  ", {$origin}";
                if($anchor){
                    $r .=  ", {$anchor}";
                    if($scaledSize){
                        $r .=  ", {$scaledSize}";
                    }
                }
            }
        }
        
        $r .=  ")";
                
        return $r;
    }
}
