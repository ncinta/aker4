<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;

class Circle {
    protected $name;
    protected $clickable = false;
    protected $editable = false;
    protected $strokeColor = '#000000';
    protected $strokeOpacity = 1.0;
    protected $strokeWeight = 1;
    protected $visible = true;
    protected $zIndex = null;
    protected $fillColor = '#FFAA00';
    protected $fillOpacity = 0.7;
    protected $latitude = null;
    protected $longitude = null;
    protected $radius = 50;

    public function __construct($name, $latitude, $longitude, $radius = 50){
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->radius = $radius;
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setClickable($clickable){
        $this->clickable = $clickable;
    }
    
    public function getClickable(){
        return $this->clickable;
    }
    
    public function setEditable($editable){
        $this->editable = $editable;
    }
    
    public function getEditable(){
        return $this->editable;
    }
    
    public function setStrokeColor($strokeColor){
        $this->strokeColor = $strokeColor;
    }
    
    public function getStrokeColor(){
        return $this->strokeColor;
    }
    
    public function setStrokeOpacity($strokeOpacity){
        if(is_numeric($strokeOpacity) && $strokeOpacity >= -0.00000001 && $strokeOpacity <= 1.00000001){
            $this->strokeOpacity = $strokeOpacity;
        } else {
            $this->strokeOpacity = 1.0;
        }
    }
    
    public function getStrokeOpacity(){
        return $this->strokeOpacity;
    }
    
    public function setStrokeWeight($strokeWeight){
        if(is_numeric($strokeWeight) && $strokeWeight >= 0){
            $this->strokeWeight = $strokeWeight;
        } else {
            $this->strokeWeight = 1;
        }
    }
    
    public function getStrokeWeight(){
        return $this->strokeWeight;
    }
    
    public function setFillColor($fillColor) {
        $this->fillColor = $fillColor;
    }
    
    public function getFillColor() {
        return $this->fillColor;
    }
    
    public function setFillOpacity($fillOpacity) {
        if(is_numeric($fillOpacity) && $fillOpacity >= -0.00000001 && $fillOpacity <= 1.00000001){
            $this->fillOpacity = $fillOpacity;
        } else {
            $this->fillOpacity = 0.5;
        }
    }
    
    public function getFillOpacity() {
        return $this->fillOpacity;
    }
    
    public function setVisible($visible){
        $this->visible = $visible;
    }
    
    public function getVisible(){
        return $this->visible;
    }
    
    public function setZIndex($zIndex){
        $this->zIndex = $zIndex;
    }
    
    public function getZIndex(){
        return $this->zIndex;
    }
    
    public function setLatitude($latitude){
        $this->latitude = $latitude;
    }
    
    public function getLatitude(){
        return $this->latitude;
    }
    
    public function setLongitude($longitude){
        $this->longitude = $longitude;
    }
    
    public function getLongitude(){
        return $this->longitude;
    }
    
    public function setRadius($radius){
        $this->radius = $radius;
    }
    
    public function getRadius(){
        return $this->radius;
    }
    
    //-----------------------------------------------------
    
    public function renderAll($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "var {$this->name} = new google.maps.Circle(");
        $res .= $this->render($d, $t+1, $map);
        $res .= $this->f($d, $t, ");");
        
        return $res;
    }
    
    public function render($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "{");
        
        $res .= $this->f($d, $t+1, "id_sc: '{$this->name}',");
        $res .= $this->f($d, $t+1, "center: ".GMHelper::newLatLon($this->latitude, $this->longitude).",");
        $res .= $this->f($d, $t+1, "radius: {$this->radius},");
        if($map){
            $res .= $this->f($d, $t+1, "map: {$map},");
        }
        $res .= $this->f($d, $t+1, "visible: ".($this->visible?'true':'false').",");
        $res .= $this->f($d, $t+1, "clickable: ".($this->clickable?'true':'false').",");
        $res .= $this->f($d, $t+1, "editable: ".($this->editable?'true':'false')."");
        if($this->strokeOpacity){
            $res .= $this->f($d, $t+1, ", strokeOpacity: {$this->strokeOpacity}");
        }
        if($this->strokeWeight){
            $res .= $this->f($d, $t+1, ", strokeWeight: {$this->strokeWeight}");
        }
        if($this->strokeColor){
            $res .= $this->f($d, $t+1, ", strokeColor: '{$this->strokeColor}'");
        }
        if($this->fillColor){
            $res .= $this->f($d, $t+1, ", fillColor: '{$this->fillColor}'");
        }
        if($this->fillOpacity){
            $res .= $this->f($d, $t+1, ", fillOpacity: {$this->fillOpacity}");
        }
        if($this->zIndex){
            $res .= $this->f($d, $t+1, ", zIndex: '{$this->zIndex}'");
        }
        
        $res .= $this->f($d, $t, "}");
        return $res;
    }
    
    private function f($debug, $tabs, $contenido){
        return HtmlHelper::format($debug, $tabs, $contenido);
    }
}