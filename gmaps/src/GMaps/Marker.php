<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;
use GMaps\Icon;

class Marker {
    // Tipos de animacion
    const ANIMATION_BOUNCE = "google.maps.Animation.BOUNCE";
    const ANIMATION_DROP = "google.maps.Animation.DROP";
    
    protected $name;
    protected $icon;
    protected $latitude;
    protected $longitude;
    protected $title;
    protected $animation = null;
    protected $clickable = true;
    protected $draggable = false;
    protected $optimized = true;
    protected $visible = true;
    protected $visible_leyenda = true;
    protected $visible_marcador = true;
    protected $clusterable = false;
    protected $raiseOnDrag = false;
    protected $flat = false;
    protected $cursor = null;
    protected $zIndex = null;
    protected $labelTexto = null;
    protected $labelClase = null;
    protected $labelBorde = 2;
    protected $informacion = null;
    //listener
    protected $listener_click_me = false;
    protected $listener_dblclick_me = false;
    protected $listener_drag_me = false;
    protected $listener_dragend_me = false;
    protected $listener_dragstart_me = false;

    public function __construct($name, $latitude, $longitude, $title = '', $icon = null){
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->title = $title;
        $this->icon = $icon;
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setIcon($icon){
        $this->icon = $icon;
    }
    
    public function getIcon(){
        return $this->icon;
    }
    
    public function setLatitude($latitude){
        $this->latitude = $latitude;
    }
    
    public function getLatitude(){
        return $this->latitude;
    }
    
    public function setLongitude($longitude){
        $this->longitude = $longitude;
    }
    
    public function getLongitude(){
        return $this->longitude;
    }
    
    public function setTitle($title){
        $this->title = $title;
    }
    
    public function getTitle(){
        return $this->title;
    }
    
    public function setAnimation($animation){
        $this->animation = $animation;
    }
    
    public function getAnimation(){
        return $this->animation;
    }
    public function setClickable($clickable){
        $this->clickable = $clickable;
    }
    
    public function getClickable(){
        return $this->clickable;
    }
    
    public function setDraggable($draggable){
        $this->draggable = $draggable;
    }
    
    public function getDraggable(){
        return $this->draggable;
    }
    
    public function setOptimized($optimized){
        $this->optimized = $optimized;
    }
    
    public function getOptimized(){
        return $this->optimized;
    }
    
    public function setVisible($visible){
        $this->visible = $visible;
    }
    
    public function getVisible(){
        return $this->visible;
    }
    
    public function setVisibleLeyenda($visible_leyenda){
        $this->visible_leyenda = $visible_leyenda;
    }
    
    public function getVisibleLeyenda(){
        return $this->visible_leyenda;
    }
    
    public function setVisibleMarcador($visible_marcador){
        $this->visible_marcador = $visible_marcador;
    }
    
    public function getVisibleMarcador(){
        return $this->visible_marcador;
    }
    
    public function setClusterable($clusterable){
        $this->clusterable = $clusterable;
    }
    
    public function getClusterable(){
        return $this->clusterable;
    }
    
    public function setRaiseOnDrag($raiseOnDrag){
        $this->raiseOnDrag = $raiseOnDrag;
    }
    
    public function getRaiseOnDrag(){
        return $this->raiseOnDrag;
    }
    
    public function setFlat($flat){
        $this->flat = $flat;
    }
    
    public function getFlat(){
        return $this->flat;
    }
    
    public function setCursor($cursor){
        $this->cursor = $cursor;
    }
    
    public function getCursor(){
        return $this->cursor;
    }
    
    public function setZIndex($zIndex){
        $this->zIndex = $zIndex;
    }
    
    public function getZIndex(){
        return $this->zIndex;
    }
    
    public function setLabelTexto($labelTexto){
        $this->labelTexto = $labelTexto;
    }
    
    public function getLabelTexto(){
        return $this->labelTexto;
    }
    
    public function setLabelClase($labelClase){
        $this->labelClase = $labelClase;
    }
    
    public function getLabelClase(){
        return $this->labelClase;
    }
    
    public function setLabelBorde($labelBorde){
        $this->labelBorde = $labelBorde;
    }
    
    public function getLabelBorde(){
        return $this->labelBorde;
    }

    public function setLabel($texto, $clase=null, $borde=null){
        $this->labelTexto = $texto;
        if($clase){
            $this->setLabelClase($clase);
        }
        if($borde){
            $this->setLabelBorde($borde);
        }
    }
    
    public function setInformacion($informacion){
        $this->informacion = $informacion;
    }
    
    public function getInformacion(){
        return $this->informacion;
    }
    
    //-----------------------------------------------------
    //EVENT LISTENERS
    public function tieneListeners(){
        return $this->listener_click_me or 
                $this->listener_dblclick_me or 
                $this->listener_drag_me or 
                $this->listener_dragend_me or 
                $this->listener_dragstart_me;
    }
    public function setClickListener($listener){
        $this->listener_click_me = $listener;
    }
    public function setDblclickListener($listener){
        $this->listener_dblclick_me = $listener;
    }
    public function setDragListener($listener){
        $this->listener_drag_me = $listener;
    }
    public function setDragendListener($listener){
        $this->listener_dragend_me = $listener;
    }
    public function setDragstartListener($listener){
        $this->listener_dragstart_me = $listener;
    }
    
    //-----------------------------------------------------
    
    public function renderAll($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "var {$this->name} = new Marcador(");
        $res .= $this->render($d, $t+1, $map);
        $res .= $this->f($d, $t, ");");
        $res .= $this->renderListeners($d, $t, $this->name);
        
        return $res;
    }
    
    public function render($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "{");
        
        $res .= $this->f($d, $t+1, "id_sc: '{$this->name}',");
        $res .= $this->f($d, $t+1, "position: ".GMHelper::newLatLon($this->latitude, $this->longitude).",");
        if($map){
            $res .= $this->f($d, $t+1, "map: {$map},");
        }
        if($this->title){
            $res .= $this->f($d, $t+1, "title: '{$this->title}'");
        }
        if($this->icon){
            $res .= $this->icon->renderInMarker($d, $t+1);
        }
        $res .= $this->f($d, $t+1, ", visible: ".($this->visible?'true':'false')."");
        $res .= $this->f($d, $t+1, ", visible_leyenda: ".($this->visible_leyenda?'true':'false')."");
        $res .= $this->f($d, $t+1, ", visible_marcador: ".($this->visible_marcador?'true':'false')."");
        $res .= $this->f($d, $t+1, ", clusterable: ".($this->clusterable?'true':'false')."");
        $res .= $this->f($d, $t+1, ", draggable: ".($this->draggable?'true':'false')."");
        $res .= $this->f($d, $t+1, ", clickable: ".($this->clickable?'true':'false')."");
        $res .= $this->f($d, $t+1, ", flat: ".($this->flat?'true':'false')."");
        $res .= $this->f($d, $t+1, ", crossOnDrag: ".($this->raiseOnDrag?'true':'false')."");
        $res .= $this->f($d, $t+1, ", optimized: ".($this->optimized?'true':'false')."");
        if($this->animation){
            $res .= $this->f($d, $t+1, ", animation: {$this->animation}");
        }
        if($this->cursor){
            $res .= $this->f($d, $t+1, ", cursor: '{$this->cursor}'");
        }
        if($this->zIndex){
            $res .= $this->f($d, $t+1, ", zIndex: {$this->title}");
        }
        if($this->labelTexto){
            $res .= $this->f($d, $t+1, ", label_texto: '{$this->labelTexto}'");
            if($this->labelBorde){
                $res .= $this->f($d, $t+1, ", label_borde: {$this->labelBorde}");
            }
            if($this->labelClase){
                $res .= $this->f($d, $t+1, ", label_clase: '{$this->labelClase}'");
            }
        }
        if($this->informacion){
            $res .= $this->f($d, $t+1, ", informacion: '{$this->informacion}'");
        }
        
        $res .= $this->f($d, $t, "}");
        
        return $res;
    }
    
    public function renderListeners($d, $t, $var){  //d:debug, t:tabs
        $res = '';
    
        //eventos
        if($this->listener_click_me){
            $res .= $this->f($d, $t, "google.maps.event.addListener({$var}, 'click', function(e){ {$this->listener_click_me}(e, {$var}) });");
        }
        if($this->listener_dblclick_me){
            $res .= $this->f($d, $t, "google.maps.event.addListener({$var}, 'dblclick', function(e){ {$this->listener_dblclick_me}(e, {$var}) });");
        }
        if($this->listener_drag_me){
            $res .= $this->f($d, $t, "google.maps.event.addListener({$var}, 'drag', function(e){ {$this->listener_drag_me}(e, {$var}) });");
        }
        if($this->listener_dragstart_me){
            $res .= $this->f($d, $t, "google.maps.event.addListener({$var}, 'dragstart', function(e){ {$this->listener_dragstart_me}(e, {$var}) });");
        }
        if($this->listener_dragend_me){
            $res .= $this->f($d, $t, "google.maps.event.addListener({$var}, 'dragend', function(e){ {$this->listener_dragend_me}(e, {$var}) });");
        }
        
        return $res;
    }
    
    private function f($debug, $tabs, $contenido){
        return HtmlHelper::format($debug, $tabs, $contenido);
    }
}