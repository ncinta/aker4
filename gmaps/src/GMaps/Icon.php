<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;

class Icon {
    protected $name;
    protected $img_name;
    protected $img_url;
    protected $img_size = null;
    protected $img_origin = null;
    protected $img_anchor = null;
    protected $img_scaled_size = null;
    
    protected $img_size_width = null;
    protected $img_size_height = null;
    protected $img_origin_x = null;
    protected $img_origin_y = null;
    protected $img_anchor_x = null;
    protected $img_anchor_y = null;
    protected $img_scaled_size_width = null;
    protected $img_scaled_size_height = null;
    
    protected $sdw_name;
    protected $sdw_url = null;
    protected $sdw_size = null;
    protected $sdw_origin = null;
    protected $sdw_anchor = null;
    protected $sdw_scaled_size = null;
    
    protected $sdw_size_width = null;
    protected $sdw_size_height = null;
    protected $sdw_origin_x = null;
    protected $sdw_origin_y = null;
    protected $sdw_anchor_x = null;
    protected $sdw_anchor_y = null;
    protected $sdw_scaled_size_width = null;
    protected $sdw_scaled_size_height = null;
    
    protected $shape_name;
    protected $shape = null;
    
    protected $shape_type = null;
    protected $shape_coords = null;

    public function __construct($name, $img_url){
        $this->setName($name);
        $this->img_url = $img_url;
    }
    
    public function setName($name){
        $this->name = $name;
        $this->img_name = 'icon_'.$name.'_image';
        $this->sdw_name = 'icon_'.$name.'_shadow';
        $this->shape_name = 'icon_'.$name.'_shape';
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getImageName(){
        return $this->img_name;
    }
    
    public function setImageUrl($img_url){
        $this->img_url = $img_url;
    }
    
    public function getImageUrl(){
        return $this->img_url;
    }
    
    public function setImageSize($width, $height){
        $this->img_size = GMHelper::newSize($width, $height);
        $this->img_size_width = $width;
        $this->img_size_height = $height;
    }
    
    public function getImageSize(){
        return $this->img_size;
    }
    
    public function setImageOrigin($x, $y){
        $this->img_origin = GMHelper::newPoint($x, $y);
        $this->img_origin_x = $x;
        $this->img_origin_y = $y;
    }
    
    public function getImageOrigin(){
        return $this->img_origin;
    }
    
    public function setImageAnchor($x, $y){
        $this->img_anchor = GMHelper::newPoint($x, $y);
        $this->img_anchor_x = $x;
        $this->img_anchor_y = $y;
        if(!$this->img_origin){
            $this->setImageOrigin(0, 0);
        }
    }
    
    public function getImageAnchor(){
        return $this->img_anchor;
    }
    
    public function setImageScaledSize($width, $height){
        $this->img_scaled_size = GMHelper::newSize($width, $height);
        $this->img_scaled_size_width = $width;
        $this->img_scaled_size_height = $height;
    }
    
    public function getImageScaledSize(){
        return $this->img_scaled_size;
    }
    
    public function getShadowName(){
        return $this->sdw_name;
    }
    
    public function setShadowUrl($sdw_url){
        $this->sdw_url = $sdw_url;
    }
    
    public function getShadowUrl(){
        return $this->sdw_url;
    }
    
    public function setShadowSize($width, $height){
        $this->sdw_size = GMHelper::newSize($width, $height);
        $this->sdw_size_width = $width;
        $this->sdw_size_height = $height;
    }
    
    public function getShadowSize(){
        return $this->sdw_size;
    }
    
    public function setShadowOrigin($x, $y){
        $this->sdw_origin = GMHelper::newPoint($x, $y);
        $this->sdw_origin_x = $x;
        $this->sdw_origin_y = $y;
    }
    
    public function getShadowOrigin(){
        return $this->sdw_origin;
    }
    
    public function setShadowAnchor($x, $y){
        $this->sdw_anchor = GMHelper::newPoint($x, $y);
        $this->sdw_anchor_x = $x;
        $this->sdw_anchor_y = $y;
        if(!$this->sdw_origin){
            $this->setShadowOrigin(0, 0);
        }
    }
    
    public function getShadowAnchor(){
        return $this->sdw_anchor;
    }
    
    public function setShadowScaledSize($width, $height){
        $this->sdw_scaled_size = GMHelper::newSize($width, $height);
        $this->sdw_scaled_size_width = $width;
        $this->sdw_scaled_size_height = $height;
    }
    
    public function getShadowScaledSize(){
        return $this->sdw_scaled_size;
    }
    
    public function getShapeName(){
        return $this->shape_name;
    }
    
    public function setShape($type, $coords){
        $this->shape = GMHelper::newMarkerShape($type, $coords);
        $this->shape_type = $type;
        $this->shape_coords = $coords;
    }
    
    public function setPolyShape($coords){
        $this->setShape('poly', GMHelper::newPolyShapeParam($coords));
    }
    
    public function setRectShape($x1, $y1, $x2, $y2){
        $this->setShape('rect', GMHelper::newRectShapeParam($x1, $y1, $x2, $y2));
    }
    
    public function setCircleShape($x, $y, $r){
        $this->setShape('circle', GMHelper::newCircleShapeParam($x, $y, $r));
    }
    
    public function getShape(){
        return $this->shape;
    }
    //-----------------------------------------------------
    
    public function render($d, $t){  //d:debug, t:tabs
        $res = '';
    
        //image
        $res .= $this->f($d, $t, "var {$this->img_name} = new google.maps.MarkerImage(");
        $res .= $this->f($d, $t+1, "'{$this->img_url}'");
        if($this->img_size){
            $res .= $this->f($d, $t+1, ", {$this->img_size}");
            if($this->img_origin){
                $res .= $this->f($d, $t+1, ", {$this->img_origin}");
                if($this->img_anchor){
                    $res .= $this->f($d, $t+1, ", {$this->img_anchor}");
                    if($this->img_scaled_size){
                        $res .= $this->f($d, $t+1, ", {$this->img_scaled_size}");
                    }
                }
            }
        }
        $res .= $this->f($d, $t, ");");
        
        //shadow
        if($this->sdw_url){
            $res .= $this->f($d, $t, "var {$this->sdw_name} = new google.maps.MarkerImage(");
            $res .= $this->f($d, $t+1, "'{$this->sdw_url}'");
            if($this->sdw_size){
                $res .= $this->f($d, $t+1, ", {$this->sdw_size}");
                if($this->sdw_origin){
                    $res .= $this->f($d, $t+1, ", {$this->sdw_origin}");
                    if($this->sdw_anchor){
                        $res .= $this->f($d, $t+1, ", {$this->sdw_anchor}");
                        if($this->sdw_scaled_size){
                            $res .= $this->f($d, $t+1, ", {$this->sdw_scaled_size}");
                        }
                    }
                }
            }
            $res .= $this->f($d, $t, ");");
        }
        
        //shape
        if($this->shape){
            $res .= $this->f($d, $t, "var {$this->shape_name} = {$this->shape};");
        }
        
        return $res;
    }
    
    public function renderInArray(){ 
        $res = "[{$this->img_name}, ";
        if($this->sdw_url){
            $res .= "{$this->sdw_name}, ";
        } else {
            $res .= "null, ";
        }
        if($this->shape){
            $res .= "{$this->shape_name}";
        } else {
            $res .= "null";
        }
        $res .= "]";
        
        return $res;
    }
    
    public function renderInMarker($d, $t){  //d:debug, t:tabs
        $res = '';
        $res .= $this->f($d, $t, ", icono: '{$this->name}'");
        return $res;
    }
    
    public function renderInProperties($d, $t){  //d:debug, t:tabs
        $res = '';
        $res .= $this->f($d, $t, "{");
        $res .= $this->f($d, $t+1, "id_sc: '{$this->name}' ");
    
        //image
        if($this->img_name){
            $res .= $this->f($d, $t+1, ", icon_url: '{$this->img_url}' ");
            if($this->img_size){
                $res .= $this->f($d, $t+1, ", icon_size_width: {$this->img_size_width} ");
                $res .= $this->f($d, $t+1, ", icon_size_height: {$this->img_size_height} ");
                if($this->img_origin){
                    $res .= $this->f($d, $t+1, ", icon_origin_x: {$this->img_origin_x} ");
                    $res .= $this->f($d, $t+1, ", icon_origin_y: {$this->img_origin_y} ");
                    if($this->img_anchor){
                        $res .= $this->f($d, $t+1, ", icon_anchor_x: {$this->img_anchor_x} ");
                        $res .= $this->f($d, $t+1, ", icon_anchor_y: {$this->img_anchor_y} ");
                        if($this->img_scaled_size){
                            $res .= $this->f($d, $t+1, ", icon_scaled_size_width: {$this->img_scaled_size_width} ");
                            $res .= $this->f($d, $t+1, ", icon_scaled_size_height: {$this->img_scaled_size_height} ");
                        }
                    }
                }
            }
        }
        
        //shadow
        if($this->sdw_url){
            $res .= $this->f($d, $t+1, ", shadow_url: '{$this->sdw_url}' ");
            if($this->sdw_size){
                $res .= $this->f($d, $t+1, ", shadow_size_width: {$this->sdw_size_width} ");
                $res .= $this->f($d, $t+1, ", shadow_size_height: {$this->sdw_size_height} ");
                if($this->sdw_origin){
                    $res .= $this->f($d, $t+1, ", shadow_origin_x: {$this->sdw_origin_x} ");
                    $res .= $this->f($d, $t+1, ", shadow_origin_y: {$this->sdw_origin_y} ");
                    if($this->sdw_anchor){
                        $res .= $this->f($d, $t+1, ", shadow_anchor_x: {$this->sdw_anchor_x} ");
                        $res .= $this->f($d, $t+1, ", shadow_anchor_y: {$this->sdw_anchor_y} ");
                        if($this->sdw_scaled_size){
                            $res .= $this->f($d, $t+1, ", shadow_scaled_size_width: {$this->sdw_scaled_size_width} ");
                            $res .= $this->f($d, $t+1, ", shadow_scaled_size_height: {$this->sdw_scaled_size_height} ");
                        }
                    }
                }
            }
        }
        
        //shape
        if($this->shape){
            $res .= $this->f($d, $t+1, ", shape_type: '{$this->shape_type}' ");
            $res .= $this->f($d, $t+1, ", shape_coords: [{$this->shape_coords}] ");
        }
        
        $res .= $this->f($d, $t, "}");
        
        /*
        icon_url
        icon_size_width 
        icon_size_height
        icon_origin_x 
        icon_origin_y 
        icon_anchor_x 
        icon_anchor_y 
        icon_scaled_size_width 
        icon_scaled_size_height
        
        shadow_url
        shadow_size_width
        shadow_size_height
        shadow_origin_x
        shadow_origin_y
        shadow_anchor_x
        shadow_anchor_y
        shadow_scaled_size_width
        shadow_scaled_size_height
        
        shape_type
        shape_coords
        */
        return $res;
    }
    
    private function f($debug, $tabs, $contenido){
        return HtmlHelper::format($debug, $tabs, $contenido);
    }
}