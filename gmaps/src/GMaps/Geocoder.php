<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;
use VMap3;
use Geocoder\Provider\MapQuest\MapQuest;
use Geocoder\Provider\Nominatim\Nominatim;
use Geocoder\Common\StatefulGeocoder;
use Geocoder\Common\Query\ReverseQuery;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class Geocoder
{

    //acceso a testeo local
    const ROOT_URL_OSM = 'https://nominatim.akercontrol.com'; //en local va sin puerto sino colocar puerto 9233
    const ROOT_URL_VMAP3 = '172.31.0.56:9232';

    //http://119.8.73.42:8080/reverse
    //acceso a produccion
    //const ROOT_URL_OSM = 'http://190.151.141.49:9233';
    // const ROOT_URL_VMAP3 = '127.0.0.1:9232';

    protected $goOsm = true;
    protected $goVMap = true;

    protected $debug = false;
    //variable y metodos
    protected $geocoder_name;
    protected $fc_geocode;
    protected $fc_geocode_inv;
    //encriptacion
    protected $encriptar_url = false;

    public function __construct()
    {
        $this->setName('gd');
    }

    public function setName($name)
    {
        $this->geocoder_name = "geocoder_{$name}";
        $this->fc_geocode = "{$this->geocoder_name}_geocodificar";
        $this->fc_geocode_inv = "{$this->fc_geocode}_inverso";
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    public function setKey($key)
    {
        GMHelper::setKey($key);
    }

    public function fcGeocodificar()
    {
        return $this->fc_geocode;
    }

    public function fcInversa()
    {
        return $this->fc_geocode_inv;
    }

    //-----------------------------------------------------

    public function renderJs()
    {

        //-- JS a ejecutar
        $body = '';
        $body .= $this->f(0, '');
        $body .= $this->f(1, "var {$this->geocoder_name};");

        //funcion de inicializacion
        $body .= $this->f(1, "function initialize_geocoder_{$this->geocoder_name}() {");
        $body .= $this->f(2, "{$this->geocoder_name} = new google.maps.Geocoder();");
        $body .= $this->f(1, "};");

        //llamada a fc de inicializacion
        $body .= $this->f(1, "google.maps.event.addDomListener(window, 'load', initialize_geocoder_{$this->geocoder_name});");

        //funcion geocodificacion
        $body .= $this->f(1, "function {$this->fc_geocode}(text, callback, callback_error) {");
        $body .= $this->f(2, "{$this->geocoder_name}.geocode({ 'address': text}, function(results, status) {");
        $body .= $this->f(3, "if (status == google.maps.GeocoderStatus.OK) {");
        $body .= $this->f(4, "var p_gc_res = results[0].geometry.location;");
        $body .= $this->f(4, "if(callback != undefined){");
        $body .= $this->f(5, "callback(p_gc_res.lat(), p_gc_res.lng(), results[0].formatted_address);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "if(callback_error != undefined){");
        $body .= $this->f(5, "callback_error(status);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "});");
        $body .= $this->f(1, "};");

        //funcion geocodificacion inversa
        $body .= $this->f(1, "function {$this->fc_geocode_inv}(latitud, longitud, callback, callback_error) {");
        $body .= $this->f(2, "var lat = parseFloat(latitud);");
        $body .= $this->f(2, "var lng = parseFloat(longitud);");
        $body .= $this->f(2, "var latlng = new google.maps.LatLng(lat, lng);");
        $body .= $this->f(2, "{$this->geocoder_name}.geocode({ 'latLng': latlng }, function(results, status) {");
        $body .= $this->f(3, "if (status == google.maps.GeocoderStatus.OK) {");
        $body .= $this->f(4, "var p_gc_res = results[0].geometry.location;");
        $body .= $this->f(4, "if(callback != undefined){");
        $body .= $this->f(5, "callback(results[0].formatted_address);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "if(callback_error != undefined){");
        $body .= $this->f(5, "callback_error(status);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "});");
        $body .= $this->f(1, "};");

        $api = GMHelper::getAPI();
        if ($api[1] >= 2) {
            $this->encriptar_url = true;
        }

        //-- se arma JS para enviar
        $res = '';
        $res .= $this->f(0, '');
        if (!GMHelper::isCargadaAPI()) {
            $res .= $this->f(0, HtmlHelper::getSectionRefJS($this->debug, $api[0]));
            GMHelper::setCargadaAPI(true);
        }

        $res .= $this->f(0, HtmlHelper::getSectionJS($this->debug, $body));

        return $res;
    }

    private function f($tabs, $contenido)
    {
        return HtmlHelper::format($this->debug, $tabs, $contenido);
    }

    public function geocodificar($address)
    {
        return $this->buscarIndirectoGoogle('address', urlencode($address));
    }

    //ests es la funcion por la que entra siempre a buscar en los motores
    public function inverso($latitud, $longitud, $provider = 'default', $is_validOsm = null)
    {
        //dd($this->goOsm);
        $result['exito'] = false;
        if ($latitud == 0 || $longitud == 0) {
            return $result;
        }

        if ($provider) { //si no viene el motor. no busca
            if (($latitud < -90 || $latitud > 90) || ($longitud < -360 || $longitud > 360) || ($longitud === 0 || $longitud === 0)) {
                return null;
            }

            if ($provider == 'security') {
                if ($this->goVMap) {
                    $result = $this->buscarVMap3($latitud, $longitud);
                }
            } else {
                if (!$is_validOsm) {
                    $this->goOsm = false;
                }
                $this->goOsm = true;

                if (isset($result['exito']) && $result['exito'] == false && $this->goOsm) {
                    $result = $this->buscarInOsmProviders($latitud, $longitud);
                }
            }

            //die('////<pre>'.nl2br(var_export($result, true)).'</pre>////');
            if ($result === null) {
                $result['exito'] = true;
                $result['direccion'] = 'n/a';
            }

            if (isset($result['provided_by'])) {
                $result['direccion'] = $result['direccion'] . ' [' . substr($result['provided_by'], 0, 1) . ']';
            }
        } else {
            $result['exito'] = true;
            $result['direccion'] = 'Error en el motor de mapa asignado';
        }

        return $result;
    }

    /** Siempre busca en OSM, hace las validaciones iniciales y parsea el resultado pero siempre 
     * sobre OSM
     */
    public function inversoOSM($latitud, $longitud)
    {
        $result['exito'] = false;
        if ($latitud == 0 || $longitud == 0) {
            return $result;
        }

        if (($latitud < -90 || $latitud > 90) || ($longitud < -360 || $longitud > 360) || ($longitud === 0 || $longitud === 0)) {
            return null;
        }


        if (isset($result['exito']) && $result['exito'] == false && $this->goOsm) {
            $result = $this->buscarInOsmProviders($latitud, $longitud);
        }

        if ($result === null) {
            $result['exito'] = true;
            $result['direccion'] = 'n/a';
        }

        if (isset($result['provided_by'])) {
            $result['direccion'] = $result['direccion'] . ' [' . substr($result['provided_by'], 0, 1) . ']';
        }

        return $result;
    }
    /**
     * @param latitud
     * @param longitud
     * 
     * Reverse geocoding con provider Nominatim Open Street Maps servido por MapQuest Api. 
     * Si no funciona busca en MapQuest Geocoding
     */
    private function buscarInOsmProviders($latitud, $longitud)
    {

        //Provider Nominatim servido por mapquest.
        $provider = new Nominatim(self::ROOT_URL_OSM, MapQuest::MPQ_API_KEY);

        //$provider = new Nominatim(Nominatim::ROOT_URL, MapQuest::MPQ_API_KEY);
        $geocoder = new StatefulGeocoder($provider, 'es');
        $intentos = 3;
        $result = null;
        do {
            $result = $geocoder->reverseQuery(ReverseQuery::fromCoordinates($latitud, $longitud));
            if (isset($result['exito']) && $result['exito']) {
                $result['direccion'] = $result['direccion'];
                return $result;
            }
            $intentos--;
            usleep(100000); //medio segundo 500 miliseconds
        } while ($intentos > 0);

        //sino funciono nominatim salgo
        return $result;

        //si no funciono Nominatim voy a Mapquest
        //return $this->buscarInMapQuestProvider($latitud, $longitud);
    }

    /**
     * Provider MapQuest Api Geocoding
     * Reverse Geocoding de un punto desde Open MapQuest o si la calidad no es optima ver:
     * https://developer.mapquest.com/documentation/geocoding-api/quality-codes/ retorna null
     */
    private function buscarInMapQuestProvider($latitud, $longitud)
    {

        $geocualityMapquest = array("P1", "L1", "I1", "B3");
        $provider = new MapQuest(MapQuest::MPQ_API_KEY);
        $geocoder = new StatefulGeocoder($provider, 'es');
        $intentos = 3;

        do {
            $result = $geocoder->reverseQuery(ReverseQuery::fromCoordinates($latitud, $longitud));
            if (isset($result['exito']) && $result['exito']) {

                if (in_array(substr($result['geocode_quality_code'], 0, 2), $geocualityMapquest)) {
                    return $result;
                }
                break;
            }
            $intentos--;
            usleep(200000); //microsegundos
        } while ($intentos > 0);

        return null;
    }

    private function buscarVMap3($latitud, $longitud)
    {
        $res = array('exito' => false, 'direccion' => '-', 'longitud' => $latitud, 'latitud' => $longitud, 'url' => '');
        //consulta motor vmap3
        $vmap = new VMap3\VMap3(self::ROOT_URL_VMAP3, 'prod');
        $result = $vmap->vmap3_search_address(array('latitude' => $latitud, 'longitude' => $longitud));
        $res = array('exito' => false);
        if (is_array($result)) {
            $esquina = $result[0];
            $div = $vmap->vmap3_division_list_to_text($esquina["street_division"]);

            // die('////<pre>'.nl2br(var_export($div, true)).'</pre>////');
            if ($div) {
                $div = '(' . $div . ')';
            }
            if ($esquina['street'] && $esquina['number'] != null) {
                $texto = "$esquina[street], al $esquina[number] $div";
            } elseif ($esquina['street'] && $esquina['intersection'] && $esquina['street'] != $esquina['intersection']) {
                $texto = "$esquina[street] y $esquina[intersection] $div";
            } elseif ($esquina['street']) {
                $texto = "sobre $esquina[street] $div";
            } else {
                $texto = '---';
            }
            if ($texto != '---') {
                $res['provided_by'] = 'vmap3';
                $res['direccion'] = $texto;
                $res['exito'] = true;
            }
        }
        return $res;
    }

    private function buscarIndirectoGoogle($nombre_parametro, $consulta)
    {
        $api = GMHelper::getGeocoderAPI();
        $request_url = GMHelper::encodeUrl($api[0] . "&" . $nombre_parametro . '=' . $consulta, $api[1]);

        $res = array('consulta' => $consulta, 'exito' => false, 'url' => $request_url, 'direccion' => '-', 'longitud' => '-', 'latitud' => '-');
        //consulta google.
        $api = GMHelper::getGeocoderAPI();
        $request_url = GMHelper::encodeUrl($api[0] . "&" . $nombre_parametro . '=' . $consulta, $api[1]);

        $geocode_pending = true;

        $intento = 3;
        while ($geocode_pending && $intento > 0) {
            try {
                try {
                    $content = file_get_contents($request_url);
                } catch (\Exception $e) {
                    return null;
                }
                if ($content) {
                    $xml = simplexml_load_string($content); //, LIBXML_NOWARNING);
                    //die('////<pre>'.nl2br(var_export($xml, true)).'</pre>////');
                    //$xml = simplexml_load_file($request_url) ;//, LIBXML_NOWARNING);

                    if ($api[1] < 2) {
                        //Version anterior
                        $status = $xml->Response->Status->code;
                        if (strcmp($status, "200") == 0) {
                            // Successful geocode
                            $geocode_pending = false;
                            $place = $xml->Response->Placemark;
                            $res['direccion'] = $place->address;

                            $coordinates = $place->Point->coordinates;
                            $coordinatesSplit = preg_split("/\,/", $coordinates);
                            // Format: Longitude, Latitude, Altitude
                            $res['latitud'] = $coordinatesSplit[1];
                            $res['longitud'] = $coordinatesSplit[0];
                            $res['exito'] = true;
                        } else if (strcmp($status, "620") == 0) {
                            // sent geocodes too fast
                            usleep(20000);
                        } else {
                            // failure to geocode
                            $geocode_pending = false;
                        }
                    } else {
                        //Version nueva
                        $status = $xml->status;
                        if (strcmp($status, "OK") == 0) {
                            // Successful geocode
                            $geocode_pending = false;
                            if (isset($xml->result) and isset($xml->result[0])) {
                                $place = $xml->result[0];
                                $res['direccion'] = $place->formatted_address;

                                $coordinates = $place->geometry->location;
                                $res['latitud'] = $coordinates->lat;
                                $res['longitud'] = $coordinates->lng;
                                $res['exito'] = true;
                            }
                        } else if (strcmp($status, "OVER_QUERY_LIMIT") == 0) {
                            // sent geocodes too fast
                            usleep(20000);
                        } else {
                            // failure to geocode o zero result
                            $geocode_pending = false;
                        }
                    }
                } else {
                    $xml = null;
                }
            } catch (Exception $exc) {
                $xml = null;
            }

            $intento--;
        }
        $res['provided_by'] = 'google';
        return $res;
    }

    function url_exists($url = NULL)
    {

        if (empty($url)) {
            return false;
        }

        // get_headers() realiza una petición GET por defecto,
        // cambiar el método predeterminadao a HEAD
        // Ver http://php.net/manual/es/function.get-headers.php
        stream_context_set_default(
            array(
                'http' => array(
                    'method' => 'GET'
                )
            )
        );
        $headers = @get_headers($url);
        sscanf($headers[0], 'HTTP/%*d.%*d %d', $httpcode);
        // Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
        $accepted_response = array(200, 301, 302);
        if (in_array($httpcode, $accepted_response)) {
            return true;
        } else {
            return false;
        }
    }
}
