<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;

class ClusterIcon {
    protected $text = null;
    protected $title = null;
    protected $anchor_x = null;
    protected $anchor_y = null;
    protected $anchor_icon_x = null;
    protected $anchor_icon_y = null;
    protected $background_position = null;
    protected $font_family = null;
    protected $font_style = null;
    protected $font_weight = null;
    protected $height = null;
    protected $text_color = null;
    protected $text_decoration = null;
    protected $text_size = null;
    protected $img_url = null;
    protected $width = null;

    public function __construct($img_url){
        $this->img_url = $img_url;
    }
    
    public function setText($text){
        $this->text = $text;
    }
    public function getText(){
        return $this->text;
    }
    
    public function setTitle($title){
        $this->title = $title;
    }
    public function getTitle(){
        return $this->title;
    }
    
    public function setAnchor($anchor_y, $anchor_x){
        $this->anchor_y = $anchor_y;
        $this->anchor_x = $anchor_x;
    }
    public function getAnchor(){
        return array($this->anchor_y, $this->anchor_x);
    }
    
    public function setAnchorIcon($anchor_icon_y, $anchor_icon_x){
        $this->anchor_icon_y = $anchor_icon_y;
        $this->anchor_icon_x = $anchor_icon_x;
    }
    public function getAnchorIcon(){
        return array($this->anchor_icon_y, $this->anchor_icon_x);
    }
    
    public function setBackgroundPosition($background_position){
        $this->background_position = $background_position;
    }
    public function getBackgroundPosition(){
        return $this->background_position;
    }
    
    public function setFontFamily($font_family){
        $this->font_family = $font_family;
    }
    public function getFontFamily(){
        return $this->font_family;
    }
    
    public function setFontStyle($font_style){
        $this->font_style = $font_style;
    }
    public function getFontStyle(){
        return $this->font_style;
    }
    
    public function setFontWeight($font_weight){
        $this->font_weight = $font_weight;
    }
    public function getFontWeight(){
        return $this->font_weight;
    }
    
    public function setHeight($height){
        $this->height = $height;
    }
    public function getHeight(){
        return $this->height;
    }
    
    public function setTextColor($text_color){
        $this->text_color = $text_color;
    }
    public function getTextColor(){
        return $this->text_color;
    }
    
    public function setTextDecoration($text_decoration){
        $this->text_decoration = $text_decoration;
    }
    public function getTextDecoration(){
        return $this->text_decoration;
    }
    
    public function setTextSize($text_size){
        $this->text_size = $text_size;
    }
    public function getTextSize(){
        return $this->text_size;
    }
    
    public function setImgUrl($img_url){
        $this->img_url = $img_url;
    }
    public function getImgUrl(){
        return $this->img_url;
    }
    
    public function setWidth($width){
        $this->width = $width;
    }
    public function getWidth(){
        return $this->width;
    }
    
    //-----------------------------------------------------
    
    public function render($d, $t){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "{");

        $anterior = false;
        if($this->img_url){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."url: '{$this->img_url}'");
            $anterior = true;
        }
        if($this->width){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."width: {$this->width}");
            $anterior = true;
        }
        if($this->text_size){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."textSize: {$this->text_size}");
            $anterior = true;
        }
        if($this->text_decoration){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."textDecoration: {$this->text_decoration}");
            $anterior = true;
        }
        if($this->text_color){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."textColor: '{$this->text_color}'");
            $anterior = true;
        }
        if($this->height){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."height: {$this->height}");
            $anterior = true;
        }
        if($this->font_weight){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."fontWeight: {$this->font_weight}");
            $anterior = true;
        }
        if($this->font_style){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."fontStyle: {$this->font_style}");
            $anterior = true;
        }
        if($this->font_family){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."fontFamily: {$this->font_family}");
            $anterior = true;
        }
        if($this->background_position){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."backgroundPosition: '{$this->background_position}'");
            $anterior = true;
        }
        if($this->anchor_icon_y && $this->anchor_icon_x){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."anchorIcon: [{$this->anchor_icon_y}, {$this->anchor_icon_x}]");
            $anterior = true;
        }
        if($this->anchor_y && $this->anchor_x){
            $res .= $this->f($d, $t+1, ($anterior?', ':'')."anchor: [{$this->anchor_y}, {$this->anchor_x}]");
            $anterior = true;
        }
        
        $res .= $this->f($d, $t, "}");

        return $res;
    }
    
    private function f($debug, $tabs, $contenido){
        return HtmlHelper::format($debug, $tabs, $contenido);
    }
}