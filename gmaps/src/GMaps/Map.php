<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;
use GMaps\Util\JQHelper;
use GMaps\Icon;
use GMaps\Marker;
use GMaps\Polyline;
use GMaps\Polygon;
use GMaps\Circle;

class Map {

    // Tipos de mapa
    const TYPE_HYBRID = "google.maps.MapTypeId.HYBRID";
    const TYPE_ROADMAP = "google.maps.MapTypeId.ROADMAP";
    const TYPE_SATELLITE = "google.maps.MapTypeId.SATELLITE";
    const TYPE_TERRAIN = "google.maps.MapTypeId.TERRAIN";
    // Posiciones de los controles
    const POS_BOTTOM_CENTER = "google.maps.ControlPosition.BOTTOM_CENTER";
    const POS_BOTTOM_LEFT = "google.maps.ControlPosition.BOTTOM_LEFT";
    const POS_BOTTOM_RIGHT = "google.maps.ControlPosition.BOTTOM_RIGHT";
    const POS_LEFT_BOTTOM = "google.maps.ControlPosition.LEFT_BOTTOM";
    const POS_LEFT_CENTER = "google.maps.ControlPosition.LEFT_CENTER";
    const POS_LEFT_TOP = "google.maps.ControlPosition.LEFT_TOP";
    const POS_RIGHT_BOTTOM = "google.maps.ControlPosition.RIGHT_BOTTOM";
    const POS_RIGHT_CENTER = "google.maps.ControlPosition.RIGHT_CENTER";
    const POS_RIGHT_TOP = "google.maps.ControlPosition.RIGHT_TOP";
    const POS_TOP_CENTER = "google.maps.ControlPosition.TOP_CENTER";
    const POS_TOP_LEFT = "google.maps.ControlPosition.TOP_LEFT";
    const POS_TOP_RIGHT = "google.maps.ControlPosition.TOP_RIGHT";
    // Tipos de control de tipo de mapa
    const STYLE_CONTROL_MAP_DEFAULT = "google.maps.MapTypeControlStyle.DEFAULT";
    const STYLE_CONTROL_MAP_DROPDOWN_MENU = "google.maps.MapTypeControlStyle.DROPDOWN_MENU";
    const STYLE_CONTROL_MAP_HORIZONTAL_BAR = "google.maps.MapTypeControlStyle.HORIZONTAL_BAR";
    // Tipos de control de zoom
    const STYLE_CONTROL_ZOOM_DEFAULT = "google.maps.ZoomControlStyle.DEFAULT";
    const STYLE_CONTROL_ZOOM_LARGE = "google.maps.ZoomControlStyle.LARGE";
    const STYLE_CONTROL_ZOOM_SMALL = "google.maps.ZoomControlStyle.SMALL";
    // Tipos de control de escala
    const STYLE_CONTROL_SCALE_DEFAULT = "google.maps.ScaleControlStyle.DEFAULT";
    // Tipos angulo de incidencia
    const TILT_0 = 0;
    const TILT_45 = 45;

    protected static $num_map_cargado = 0;
    protected $debug = false;
    protected $div_name = 'canvas';
    protected $include_jquery = false;
    protected $ini_lati;
    protected $ini_long;
    protected $ini_zoom;
    protected $ini_width = '100%';
    protected $ini_height = '100%';
    protected $map_type;
    protected $control_maptype = true;
    protected $control_maptype_maps = array(Map::TYPE_HYBRID, Map::TYPE_ROADMAP, Map::TYPE_SATELLITE, Map::TYPE_TERRAIN);
    protected $control_maptype_pos = Map::POS_TOP_RIGHT;
    protected $control_maptype_style = Map::STYLE_CONTROL_MAP_DEFAULT;
    protected $control_pan = true;
    protected $control_pan_pos = Map::POS_TOP_LEFT;
    protected $control_zoom = true;
    protected $control_zoom_pos = Map::POS_TOP_LEFT;
    protected $control_zoom_style = Map::STYLE_CONTROL_ZOOM_DEFAULT;
    protected $control_scale = false;
    protected $control_scale_pos = Map::POS_BOTTOM_LEFT;
    protected $control_scale_style = Map::STYLE_CONTROL_SCALE_DEFAULT;
    protected $control_streetview = true;
    protected $control_streetview_pos = Map::POS_RIGHT_CENTER;
    protected $control_overview = false;
    protected $control_overview_opened = false;
    protected $control_rotate = false;
    protected $control_rotate_pos = Map::POS_TOP_LEFT;
    protected $tilt = Map::TILT_0;
    protected $disable_default_ui = true;
    protected $scrollwheel = true;
    protected $keyboard_shortcuts = true;
    protected $draggable = true;
    protected $disable_doubleclick_zoom = false;
    protected $no_clear_div = false;
    protected $min_zoom = false;
    protected $max_zoom = false;
    protected $background_color = false;
    protected $dragging_cursor = false;
    protected $draggable_cursor = false;
    protected $heading = false;
    protected $styles = array();
    protected $bound_lati_1 = false;
    protected $bound_long_1 = false;
    protected $bound_lati_2 = false;
    protected $bound_long_2 = false;
    protected $label_max_longitud = 100;
    protected $label_wrap_longitud = 60;
    protected $cluster_styles = null;
    protected $cluster_minsize = 20000;
    protected $cluster_grid = 100;
    //listener
    protected $listener_bounds_changed = false;
    protected $listener_center_changed = false;
    protected $listener_click_me = false;
    protected $listener_dblclick_me = false;
    protected $listener_drag = false;
    protected $listener_dragend = false;
    protected $listener_dragstart = false;
    protected $listener_heading_changed = false;
    protected $listener_idle = false;
    protected $listener_maptypeid_changed = false;
    protected $listener_mousemove_me = false;
    protected $listener_mouseout_me = false;
    protected $listener_mouseover_me = false;
    protected $listener_projection_changed = false;
    protected $listener_resize = false;
    protected $listener_rightclick_me = false;
    protected $listener_tilesloaded = false;
    protected $listener_tilt_changed = false;
    protected $listener_zoom_changed = false;
    //variable y metodos
    protected $map_name;
    protected $var_markers;
    protected $var_markers_icon;
    protected $var_icons;
    protected $var_cluster;
    protected $var_stop_draw;
    protected $fc_ini_draw;
    protected $fc_stop_draw;
    protected $fc_get_cluster;
    protected $fc_get_clusterable_markers;
    protected $fc_get_clusterable_markers_size;
    protected $fc_get_cluster_size;
    protected $fc_ini_cluster;
    protected $fc_set_cluster_grid;
    protected $fc_set_cluster_minsize;
    protected $fc_add_marker;
    protected $fc_add_all_markers;
    protected $fc_change_all_marker;
    protected $fc_center_marker;
    protected $fc_add_polyline;
    protected $fc_add_polygon;
    protected $fc_add_circle;
    protected $fc_add_all_polylines;
    protected $fc_add_all_polygons;
    protected $fc_add_all_circles;
    protected $fc_redimension;
    protected $fc_get_icon;
    protected $fc_add_icon;
    protected $fc_add_all_icons;
    protected $fc_update_icons;
    //encriptacion
    protected $encriptar_url = false;

    public function __construct($ini_lati = -32.889534, $ini_long = -68.844795, $ini_zoom = 10, $map_type = Map::TYPE_ROADMAP) {
        $this->ini_lati = $ini_lati;
        $this->ini_long = $ini_long;
        $this->ini_zoom = $ini_zoom;
        $this->map_type = $map_type;

        Map::$num_map_cargado += 1;
        $this->setDivName('canvas_' . Map::$num_map_cargado);
    }

    public function setDebug($debug) {
        $this->debug = $debug;
    }

    public function setKey($key) {
        GMHelper::setKey($key);
    }

    public function setDivName($div_name) {
        $this->div_name = $div_name;
        $this->map_name = "map_{$this->div_name}";
        $this->var_markers = "{$this->map_name}_markers";
        $this->var_markers_icon = "{$this->var_markers}_and_icon";
        $this->fc_add_marker = "{$this->map_name}_marker_add";
        $this->fc_add_all_markers = "{$this->fc_add_marker}_all";
        $this->fc_change_all_marker = "{$this->map_name}_change_add";
        $this->var_icons = "{$this->map_name}_icons";
        $this->var_stop_draw = "{$this->map_name}_stop_draw";
        $this->fc_ini_draw = "{$this->var_stop_draw}_fc_ini";
        $this->fc_stop_draw = "{$this->var_stop_draw}_fc_stop";
        $this->var_cluster = "{$this->map_name}_cluster";
        $this->fc_get_cluster = "get_{$this->var_cluster}";
        $this->fc_get_clusterable_markers = "get_{$this->map_name}_clusterable";
        $this->fc_get_clusterable_markers_size = "{$this->fc_get_clusterable_markers}_size";
        $this->fc_get_cluster_size = "get_{$this->var_cluster}_size";
        $this->fc_ini_cluster = "ini_{$this->var_cluster}";
        $this->fc_set_cluster_grid = "set_{$this->var_cluster}_grid";
        $this->fc_set_cluster_minsize = "set_{$this->var_cluster}_minsize";
        $this->var_polylines = "{$this->map_name}_polylines";
        $this->var_polygons = "{$this->map_name}_polygons";
        $this->var_circles = "{$this->map_name}_circles";
        $this->fc_center_marker = "{$this->map_name}_center_marker";
        $this->fc_add_polyline = "{$this->map_name}_polyline_add";
        $this->fc_add_polygon = "{$this->map_name}_polygon_add";
        $this->fc_add_circle = "{$this->map_name}_circle_add";
        $this->fc_add_all_polylines = "{$this->fc_add_polyline}_all";
        $this->fc_add_all_polygons = "{$this->fc_add_polygon}_all";
        $this->fc_add_all_circles = "{$this->fc_add_circle}_all";
        $this->fc_get_polygon_path = "{$this->var_polygons}_path";
        $this->fc_get_polygon_object = "{$this->var_polygons}_object";
        $this->fc_redimension = "{$this->map_name}_resize";
        $this->fc_get_icon = "{$this->map_name}_icon_get";
        $this->fc_add_icon = "{$this->map_name}_icon_add";
        $this->fc_add_all_icons = "{$this->fc_add_icon}_all";
        $this->fc_update_icons = "{$this->map_name}_icons_update_all";
    }

    public function getDivName() {
        return $this->div_name;
    }

    public function getMapName() {
        return $this->map_name;
    }

    public function varMarkers() {
        return $this->var_markers;
    }

    public function varMarkersIcon() {
        return $this->var_markers_icon;
    }

    public function varPolygons() {
        return $this->var_polygons;
    }

    public function varCircles() {
        return $this->var_circles;
    }

    public function fcPolygonPath() {
        return $this->fc_get_polygon_path;
    }

    public function fcPolygonObject() {
        return $this->fc_get_polygon_object;
    }

    public function varIcons() {
        return $this->var_icons;
    }

    public function varStopDraw() {
        return $this->var_stop_draw;
    }

    public function fcIniDraw() {
        return $this->fc_ini_draw;
    }

    public function fcStopDraw() {
        return $this->fc_stop_draw;
    }

    public function varCluster() {
        return $this->var_cluster;
    }

    public function fcGetCluster() {
        return $this->fc_get_cluster;
    }

    public function fcGetClusterableMarkers() {
        return $this->fc_get_clusterable_markers;
    }

    public function fcGetClusterableMarkersSize() {
        return $this->fc_get_clusterable_markers_size;
    }

    public function fcGetClusterSize() {
        return $this->fc_get_cluster_size;
    }

    public function fcSetClusterGrid() {
        return $this->fc_set_cluster_grid;
    }

    public function fcSetClusterMinSize() {
        return $this->fc_set_cluster_minsize;
    }

    public function fcIniCluster() {
        return $this->fc_ini_cluster;
    }

    public function varPolylines() {
        return $this->var_polylines;
    }

    public function fcAddMarker() {
        return $this->fc_add_marker;
    }

    public function fcChangeMarkers() {
        return $this->fc_change_all_marker;
    }

    public function fcAddAllMarkers() {
        return $this->fc_add_all_markers;
    }

    public function fcAddPolyline() {
        return $this->fc_add_polyline;
    }

    public function fcAddPolygon() {
        return $this->fc_add_polygon;
    }

    public function fcAddCircle() {
        return $this->fc_add_circle;
    }

    public function fcAddAllPolylines() {
        return $this->fc_add_all_polylines;
    }

    public function fcAddAllPolygons() {
        return $this->fc_add_all_polygons;
    }

    public function fcAddAllCircles() {
        return $this->fc_add_all_circles;
    }

    public function fcRedimensionar() {
        return $this->fc_redimension;
    }

    public function fcGetIcon() {
        return $this->fc_get_icon;
    }

    public function fcAddIcon() {
        return $this->fc_add_icon;
    }

    public function fcAddAllIcons() {
        return $this->fc_add_all_icons;
    }

    public function fcUpdateAllIcons() {
        return $this->fc_update_icons;
    }

    public function fcCenterMarker() {
        return $this->fc_center_marker;
    }

    public function setIncludeJQuery($include_jquery) {
        $this->include_jquery = $include_jquery;
    }

    public function getIncludeJQuery() {
        return $this->include_jquery;
    }

    public function setIniLatitud($ini_lati) {
        $this->ini_lati = $ini_lati;
    }

    public function setIniLongitud($ini_long) {
        $this->ini_long = $ini_long;
    }

    public function setIniZoom($ini_zoom) {
        $this->ini_zoom = $ini_zoom;
    }

    public function setWidth($ini_width) {
        $this->ini_width = $ini_width;
    }

    public function setHeight($ini_height) {
        $this->ini_height = $ini_height;
    }

    public function setSize($ini_width, $ini_height) {
        $this->ini_width = $ini_width;
        $this->ini_height = $ini_height;
    }

    public function getMapType() {
        return $this->map_type;
    }

    public function setMapType($map_type) {
        $this->map_type = $map_type;
    }

    public function getMapTypeControl() {
        return $this->control_maptype;
    }

    public function setMapTypeControl($control_maptype) {
        $this->control_maptype = $control_maptype;
    }

    public function getMapTypeStyle() {
        return $this->control_maptype_style;
    }

    public function setMapTypeStyle($control_maptype_style) {
        $this->control_maptype_style = $control_maptype_style;
    }

    public function getLabelMaxLongitud() {
        return $this->label_max_longitud;
    }

    public function setLabelMaxLongitud($label_max_longitud) {
        $this->label_max_longitud = $label_max_longitud;
    }

    public function getLabelWrapLongitud() {
        return $this->label_wrap_longitud;
    }

    public function setLabelWrapLongitud($label_wrap_longitud) {
        $this->label_wrap_longitud = $label_wrap_longitud;
    }

    public function getMapTypePos() {
        return $this->control_maptype_pos;
    }

    public function setMapTypePos($control_maptype_pos) {
        $this->control_maptype_pos = $control_maptype_pos;
    }

    public function getMapTypes() {
        return $this->control_maptype_maps;
    }

    public function setMapTypes($control_maptype_maps) {
        if ($control_maptype_maps and is_array($control_maptype_maps)) {
            $this->control_maptype_maps = array_unique($control_maptype_maps);
        } else {
            $this->control_maptype_maps = array();
        }
    }

    public function clearMapTypes() {
        $this->control_maptype_maps = array();
    }

    public function removeMapType($map_type) {
        if (in_array($map_type, $this->control_maptype_maps)) {
            $this->control_maptype_maps = array_diff($this->control_maptype_maps, array($map_type));
        }
    }

    public function addMapType($map_type) {
        if (!in_array($map_type, $this->control_maptype_maps)) {
            $this->control_maptype_maps[] = $map_type;
        }
    }

    public function getPanControl() {
        return $this->control_pan;
    }

    public function setPanControl($control_pan) {
        $this->control_pan = $control_pan;
    }

    public function getPanPos() {
        return $this->control_pan_pos;
    }

    public function setPanPos($control_pan_pos) {
        $this->control_pan_pos = $control_pan_pos;
    }

    public function getZoomControl() {
        return $this->control_zoom;
    }

    public function setZoomControl($control_zoom) {
        $this->control_zoom = $control_zoom;
    }

    public function getZoomPos() {
        return $this->control_zoom_pos;
    }

    public function setZoomPos($control_zoom_pos) {
        $this->control_zoom_pos = $control_zoom_pos;
    }

    public function getZoomStyle() {
        return $this->control_zoom_style;
    }

    public function setZoomStyle($control_zoom_style) {
        $this->control_zoom_style = $control_zoom_style;
    }

    public function getScaleControl() {
        return $this->control_scale;
    }

    public function setScaleControl($control_scale) {
        $this->control_scale = $control_scale;
    }

    public function getScalePos() {
        return $this->control_scale_pos;
    }

    public function setScalePos($control_scale_pos) {
        $this->control_scale_pos = $control_scale_pos;
    }

    public function getScaleStyle() {
        return $this->control_scale_style;
    }

    public function setScaleStyle($control_scale_style) {
        $this->control_scale_style = $control_scale_style;
    }

    public function getStreetviewControl() {
        return $this->control_streetview;
    }

    public function setStreetviewControl($control_streetview) {
        $this->control_streetview = $control_streetview;
    }

    public function getStreetviewPos() {
        return $this->control_streetview_pos;
    }

    public function setStreetviewPos($control_streetview_pos) {
        $this->control_streetview_pos = $control_streetview_pos;
    }

    public function getOverviewControl() {
        return $this->control_overview;
    }

    public function setOverviewControl($control_overview) {
        $this->control_overview = $control_overview;
    }

    public function getOverviewOpened() {
        return $this->control_overview_opened;
    }

    public function setOverviewOpened($control_overview_opened) {
        $this->control_overview_opened = $control_overview_opened;
    }

    public function getRotateControl() {
        return $this->control_rotate;
    }

    public function setRotateControl($control_rotate) {
        $this->control_rotate = $control_rotate;
    }

    public function getRotatePos() {
        return $this->control_rotate_pos;
    }

    public function setRotatePos($control_rotate_pos) {
        $this->control_rotate_pos = $control_rotate_pos;
    }

    public function getTilt() {
        return $this->tilt;
    }

    public function setTilt($tilt) {
        $this->tilt = $tilt;
    }

    public function isDisableDefaultUI() {
        return $this->disable_default_ui;
    }

    public function disableDefaultUI($disable_default_ui) {
        $this->disable_default_ui = $disable_default_ui;
    }

    public function isScrollwheel() {
        return $this->scrollwheel;
    }

    public function setScrollwheel($scrollwheel) {
        $this->scrollwheel = $scrollwheel;
    }

    public function isKeyboardShortcuts() {
        return $this->keyboard_shortcuts;
    }

    public function setKeyboardShortcuts($keyboard_shortcuts) {
        $this->keyboard_shortcuts = $keyboard_shortcuts;
    }

    public function isDraggable() {
        return $this->draggable;
    }

    public function setDraggable($draggable) {
        $this->draggable = $draggable;
    }

    public function isDisableDoubleClickZoom() {
        return $this->disable_doubleclick_zoom;
    }

    public function disableDoubleClickZoom($disable_doubleclick_zoom) {
        $this->disable_doubleclick_zoom = $disable_doubleclick_zoom;
    }

    public function isNoClearDiv() {
        return $this->no_clear_div;
    }

    public function noClearDiv($no_clear_div) {
        $this->no_clear_div = $no_clear_div;
    }

    public function getMinZoom() {
        return ($this->min_zoom ? $this->min_zoom : null);
    }

    public function setMinZoom($min_zoom) {
        $this->min_zoom = $min_zoom;
    }

    public function getMaxZoom() {
        return ($this->max_zoom ? $this->max_zoom : null);
    }

    public function setMaxZoom($max_zoom) {
        $this->max_zoom = $max_zoom;
    }

    public function getBackgroundColor() {
        return ($this->background_color ? $this->background_color : null);
    }

    public function setBackgroundColor($background_color) {
        $this->background_color = $background_color;
    }

    public function getDraggingCursor() {
        return ($this->dragging_cursor ? $this->dragging_cursor : null);
    }

    public function setDraggingCursor($dragging_cursor) {
        $this->dragging_cursor = $dragging_cursor;
    }

    public function getDraggableCursor() {
        return ($this->draggable_cursor ? $this->draggable_cursor : null);
    }

    public function setDraggableCursor($draggable_cursor) {
        $this->draggable_cursor = $draggable_cursor;
    }

    public function getHeading() {
        return ($this->heading ? $this->heading : null);
    }

    public function setHeading($heading) {
        $this->heading = $heading;
    }

    public function getStyles() {
        return $this->styles;
    }

    public function setStyles($styles) {
        if ($styles and is_array($styles)) {
            $this->styles = array_unique($styles);
        } else {
            $this->styles = array();
        }
    }

    public function clearStyles() {
        $this->styles = array();
    }

    public function removeStyle($style) {
        if (in_array($style, $this->styles)) {
            $this->styles = array_diff($this->styles, array($style));
        }
    }

    public function addStyle($style) {
        if (!in_array($style, $this->styles)) {
            $this->styles[] = $style;
        }
    }

    public function fitBounds($lati_1, $long_1, $lati_2, $long_2) {
        if ($lati_1 < $lati_2) {
            $this->bound_lati_1 = $lati_1;
            $this->bound_lati_2 = $lati_2;
        } else {
            $this->bound_lati_1 = $lati_2;
            $this->bound_lati_2 = $lati_1;
        }

        if ($long_1 < $long_2) {
            $this->bound_long_1 = $long_1;
            $this->bound_long_2 = $long_2;
        } else {
            $this->bound_long_1 = $long_2;
            $this->bound_long_2 = $long_1;
        }
    }

    //-----------------------------------------------------
    //EVENT LISTENERS

    public function setBoundsChangedListener($listener) {
        $this->listener_bounds_changed = $listener;
    }

    public function setCenterChangedListener($listener) {
        $this->listener_center_changed = $listener;
    }

    public function setClickListener($listener) {
        $this->listener_click_me = $listener;
    }

    public function setDblclickListener($listener) {
        $this->listener_dblclick_me = $listener;
    }

    public function setDragListener($listener) {
        $this->listener_drag = $listener;
    }

    public function setDragendListener($listener) {
        $this->listener_dragend = $listener;
    }

    public function setDragstartListener($listener) {
        $this->listener_dragstart = $listener;
    }

    public function setHeadingChangedListener($listener) {
        $this->listener_heading_changed = $listener;
    }

    public function setIdleListener($listener) {
        $this->listener_idle = $listener;
    }

    public function setMaptypeChangedListener($listener) {
        $this->listener_maptypeid_changed = $listener;
    }

    public function setMouseMoveListener($listener) {
        $this->listener_mousemove_me = $listener;
    }

    public function setMouseOutListener($listener) {
        $this->listener_mouseout_me = $listener;
    }

    public function setMouseOverListener($listener) {
        $this->listener_mouseover_me = $listener;
    }

    public function setProjectionChangedListener($listener) {
        $this->listener_projection_changed = $listener;
    }

    public function setResizeListener($listener) {
        $this->listener_resize = $listener;
    }

    public function setRightClickListener($listener) {
        $this->listener_rightclick_me = $listener;
    }

    public function setTilesLoadedListener($listener) {
        $this->listener_tilesloaded = $listener;
    }

    public function setTiltChangedListener($listener) {
        $this->listener_tilt_changed = $listener;
    }

    public function setZoomChangedListener($listener) {
        $this->listener_zoom_changed = $listener;
    }

    public function setClusterStyles($cluster_styles) {
        $this->cluster_styles = $cluster_styles;
    }

    public function setClusterMinSize($cluster_minsize) {
        $this->cluster_minsize = $cluster_minsize;
    }

    public function setClusterGrid($cluster_grid) {
        $this->cluster_grid = $cluster_grid;
    }

    //-----------------------------------------------------
    // MARCADORES
    protected $markers = array();

    public function getMarkers() {
        return $this->markers;
    }

    public function setMarkers($markers) {
        if ($markers and is_array($markers)) {
            $this->markers = array_unique($markers);
        } else {
            $this->markers = array();
        }
    }

    public function clearMarkers() {
        $this->markers = array();
    }

    public function existsMarker($name) {
        return array_key_exists($name, $this->markers);
    }

    public function removeMarker($name) {
        if ($this->existsMarker($name)) {
            unset($this->markers[$name]);
        }
    }

    public function addMarkerPerName($name, $marker) {
        $this->markers[$name] = $marker;
    }

    public function addMarker($marker) {
        if ($marker) {
            $this->addMarkerPerName($marker->getName(), $marker);
        }
    }

    public function getMarker($name) {
        if ($this->existsMarker($name)) {
            return $this->markers[$name];
        } else {
            return null;
        }
    }

    //-----------------------------------------------------
    // ICONOS

    protected $icons = array();

    public function getIcons() {
        return $this->icons;
    }

    public function setIcons($icons) {
        if ($icons and is_array($icons)) {
            $this->icons = array_unique($icons);
        } else {
            $this->icons = array();
        }
    }

    public function clearIcons() {
        $this->icons = array();
    }

    public function existsIcon($name) {
        return array_key_exists($name, $this->icons);
    }

    public function removeIcon($name) {
        if ($this->existsIcon($name)) {
            unset($this->icons[$name]);
        }
    }

    public function addIconPerName($name, $icon) {
        $this->icons[$name] = $icon;
    }

    public function addIcon($icon) {
        if ($icon) {
            $this->addIconPerName($icon->getName(), $icon);
        }
    }

    public function getIcon($name) {
        if ($this->existsIcon($name)) {
            return $this->icons[$name];
        } else {
            return null;
        }
    }

    //-----------------------------------------------------
    // POLILINEAS

    protected $polylines = array();

    public function getPolylines() {
        return $this->polylines;
    }

    public function setPolylines($polylines) {
        if ($polylines and is_array($polylines)) {
            $this->polylines = array_unique($polylines);
        } else {
            $this->polylines = array();
        }
    }

    public function clearPolylines() {
        $this->polylines = array();
    }

    public function existsPolyline($name) {
        return array_key_exists($name, $this->polylines);
    }

    public function removePolyline($name) {
        if ($this->existsPolyline($name)) {
            unset($this->polylines[$name]);
        }
    }

    public function addPolylinePerName($name, $polyline) {
        $this->polylines[$name] = $polyline;
    }

    public function addPolyline($polyline) {
        if ($polyline) {
            $this->addPolylinePerName($polyline->getName(), $polyline);
        }
    }

    public function getPolyline($name) {
        if ($this->existsPolyline($name)) {
            return $this->polylines[$name];
        } else {
            return null;
        }
    }

    //-----------------------------------------------------
    // POLIGONOS

    protected $polygons = array();

    public function getPolygons() {
        return $this->polygons;
    }

    public function setPolygons($polygons) {
        if ($polygons and is_array($polygons)) {
            $this->polygons = array_unique($polygons);
        } else {
            $this->polygons = array();
        }
    }

    public function addPolygonPerName($name, $polygone) {
        $this->polygons[$name] = $polygone;
    }

    public function addPolygon($polygon) {
        if ($polygon) {
            $this->addPolygonPerName($polygon->getName(), $polygon);
        }
    }

    //-----------------------------------------------------
    // CIRCULOS

    protected $circles = array();

    public function getCircles() {
        return $this->circles;
    }

    public function setCircles($circles) {
        if ($circles and is_array($circles)) {
            $this->circles = array_unique($circles);
        } else {
            $this->circles = array();
        }
    }

    public function addCirclePerName($name, $circle) {
        $this->circles[$name] = $circle;
    }

    public function addCircle($circle) {
        if ($circle) {
            $this->addCirclePerName($circle->getName(), $circle);
        }
    }

    //-----------------------------------------------------

    public function renderHtml() {
        $body = '';

        //se arma HTML para enviar
        $res = '';
        $res .= HtmlHelper::getDiv($this->debug, array(
                    'id' => $this->div_name,
                    'style' => "width:{$this->ini_width}; height:{$this->ini_height}"
                        ), $body);

        return $res;
    }

    public function renderJs() {

        //tipos de mapa
        $mapas = "[";
        foreach ($this->control_maptype_maps as $m) {
            $mapas .= "{$m}, ";
        }
        $mapas .= "]";
        //estilos
        $estilos = "[";
        foreach ($this->styles as $s) {
            $estilos .= "{$s}, ";
        }
        $estilos .= "]";
        //controles
        $control_maptype = $this->control_maptype ? "true" : "false";
        $control_pan = $this->control_pan ? "true" : "false";
        $control_zoom = $this->control_zoom ? "true" : "false";
        $control_scale = $this->control_scale ? "true" : "false";
        $control_streetview = $this->control_streetview ? "true" : "false";
        $control_overview = $this->control_overview ? "true" : "false";
        $control_overview_opened = $this->control_overview_opened ? "true" : "false";
        $control_rotate = $this->control_rotate ? "true" : "false";
        $scrollwheel = $this->scrollwheel ? "true" : "false";
        //comportamiento iu 
        $disable_default_ui = $this->disable_default_ui ? "true" : "false";
        $keyboard_shortcuts = $this->keyboard_shortcuts ? "true" : "false";
        $draggable = $this->draggable ? "true" : "false";
        $disable_doubleclick_zoom = $this->disable_doubleclick_zoom ? "true" : "false";
        $no_clear_div = $this->no_clear_div ? "true" : "false";

        //-- JS a ejecutar
        $body = '';
        $body .= $this->f(0, '');
        $body .= $this->f(1, "var {$this->map_name};");
        $body .= $this->f(1, "google.maps.visualRefresh = true;");

        //funcion de inicializacion
        $body .= $this->f(1, "function initialize_{$this->div_name}() {");
        $body .= $this->f(2, "var opt_{$this->div_name} = {");

        //opciones por defecto
        $body .= $this->f(3, "center: new google.maps.LatLng({$this->ini_lati}, {$this->ini_long}),");
        $body .= $this->f(3, "zoom: {$this->ini_zoom},");
        $body .= $this->f(3, "mapTypeId: {$this->map_type},");

        $body .= $this->f(3, "disableDefaultUI: {$disable_default_ui},");

        $body .= $this->f(3, "mapTypeControl: {$control_maptype},");
        $body .= $this->f(3, "mapTypeControlOptions: {");
        $body .= $this->f(4, "mapTypeIds: {$mapas},");
        $body .= $this->f(4, "style: {$this->control_maptype_style},");
        $body .= $this->f(4, "position: {$this->control_maptype_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "panControl: {$control_pan},");
        $body .= $this->f(3, "panControlOptions: {");
        $body .= $this->f(4, "position: {$this->control_pan_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "zoomControl: {$control_zoom},");
        $body .= $this->f(3, "zoomControlOptions: {");
        $body .= $this->f(4, "style: {$this->control_zoom_style},");
        $body .= $this->f(4, "position: {$this->control_zoom_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "scaleControl: {$control_scale},");
        $body .= $this->f(3, "scaleControlOptions: {");
        $body .= $this->f(4, "style: {$this->control_scale_style},");
        $body .= $this->f(4, "position: {$this->control_scale_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "streetViewControl: {$control_streetview},");
        $body .= $this->f(3, "streetViewControlOptions: {");
        $body .= $this->f(4, "position: {$this->control_streetview_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "overviewMapControl: {$control_overview},");
        $body .= $this->f(3, "overviewMapControlOptions: {");
        $body .= $this->f(4, "opened: {$control_overview_opened}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "rotateControl: {$control_rotate},");
        $body .= $this->f(3, "rotateControlOptions: {");
        $body .= $this->f(4, "position: {$this->control_rotate_pos}");
        $body .= $this->f(3, "},");

        $body .= $this->f(3, "tilt: {$this->tilt},");

        $body .= $this->f(3, "scrollwheel: {$scrollwheel},");
        $body .= $this->f(3, "keyboardShortcuts: {$keyboard_shortcuts},");
        $body .= $this->f(3, "draggable: {$draggable},");
        $body .= $this->f(3, "disableDoubleClickZoom: {$disable_doubleclick_zoom},");
        $body .= $this->f(3, "noClear: {$no_clear_div},");

        if ($this->min_zoom)
            $body .= $this->f(3, "minZoom: {$this->min_zoom},");
        if ($this->max_zoom)
            $body .= $this->f(3, "maxZoom: {$this->max_zoom},");
        if ($this->background_color)
            $body .= $this->f(3, "backgroundColor: '{$this->background_color}',");
        if ($this->dragging_cursor)
            $body .= $this->f(3, "draggingCursor: '{$this->dragging_cursor}',");
        if ($this->draggable_cursor)
            $body .= $this->f(3, "draggableCursor: '{$this->draggable_cursor}',");
        if ($this->heading)
            $body .= $this->f(3, "heading: {$this->heading},");

        $body .= $this->f(3, "styles: {$estilos}");

        $body .= $this->f(2, "};");

        //contruccion del mapa
        $body .= $this->f(2, "{$this->map_name} = new google.maps.Map(document.getElementById('{$this->div_name}'), opt_{$this->div_name});");

        //eventos
        if ($this->listener_click_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'click', {$this->listener_click_me});");
        }
        if ($this->listener_dblclick_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'dblclick', {$this->listener_dblclick_me});");
        }
        if ($this->listener_mousemove_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'mousemove', {$this->listener_mousemove_me});");
        }
        if ($this->listener_mouseout_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'mouseout', {$this->listener_mouseout_me});");
        }
        if ($this->listener_mouseover_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'mouseover', {$this->listener_mouseover_me});");
        }
        if ($this->listener_rightclick_me) {
            $body .= $this->f(2, "google.maps.event.addListener({$this->map_name}, 'rightclick', {$this->listener_rightclick_me});");
        }
        if ($this->listener_bounds_changed) {
            $body .= $this->f(2, "{$this->map_name}.bounds_changed = {$this->listener_bounds_changed};");
        }
        if ($this->listener_center_changed) {
            $body .= $this->f(2, "{$this->map_name}.center_changed = {$this->listener_center_changed};");
        }
        if ($this->listener_drag) {
            $body .= $this->f(2, "{$this->map_name}.drag = {$this->listener_drag};");
        }
        if ($this->listener_dragend) {
            $body .= $this->f(2, "{$this->map_name}.dragend = {$this->listener_dragend};");
        }
        if ($this->listener_dragstart) {
            $body .= $this->f(2, "{$this->map_name}.dragstart = {$this->listener_dragstart};");
        }
        if ($this->listener_heading_changed) {
            $body .= $this->f(2, "{$this->map_name}.heading_changed = {$this->listener_heading_changed};");
        }
        if ($this->listener_idle) {
            $body .= $this->f(2, "{$this->map_name}.idle = {$this->listener_idle};");
        }
        if ($this->listener_maptypeid_changed) {
            $body .= $this->f(2, "{$this->map_name}.maptypeid_changed = {$this->listener_maptypeid_changed};");
        }
        if ($this->listener_projection_changed) {
            $body .= $this->f(2, "{$this->map_name}.projection = {$this->listener_projection_changed};");
        }
        if ($this->listener_resize) {
            $body .= $this->f(2, "{$this->map_name}.resize = {$this->listener_resize};");
        }
        if ($this->listener_tilesloaded) {
            $body .= $this->f(2, "{$this->map_name}.tilesloaded = {$this->listener_tilesloaded};");
        }
        if ($this->listener_tilt_changed) {
            $body .= $this->f(2, "{$this->map_name}.tilt = {$this->listener_tilt_changed};");
        }
        if ($this->listener_zoom_changed) {
            $body .= $this->f(2, "{$this->map_name}.zoom_changed = {$this->listener_zoom_changed};");
        }

        //llamada a metodos post-construccion
        if ($this->bound_lati_1 and $this->bound_long_1 and $this->bound_lati_2 and $this->bound_long_2) {
            $bounds = GMHelper::newBoundsLL($this->bound_lati_1, $this->bound_long_1, $this->bound_lati_2, $this->bound_long_2);

            $body .= $this->f(2, "{$this->map_name}.fitBounds({$bounds});");
        }

        $body .= $this->f(2, "{$this->fc_stop_draw}();");

        //iconos iniciales
        if ($this->icons and count($this->icons)) {
            foreach ($this->icons as $icon) {
                if ($icon instanceof Icon) {
                    $body .= $this->f(2, "{$this->fc_add_icon}(");
                    $body .= $icon->renderInProperties($this->debug, 3);
                    $body .= $this->f(2, ", false);");
                }
            }
            $body .= $this->f(2, "{$this->fc_update_icons}();");
        }

        //marcadores iniciales
        if ($this->markers and count($this->markers)) {
            foreach ($this->markers as $marker) {
                if ($marker instanceof Marker) {
                    $body .= $this->f(2, "{$this->fc_add_marker}(");
                    $body .= $marker->render($this->debug, 3, $this->map_name);
                    $body .= $this->f(2, ");");

                    if ($marker->tieneListeners()) {
                        $var_marc_ltn = "marc_list_{$marker->getName()}";
                        $body .= $this->f(2, "if({$this->var_markers}.exists('{$marker->getName()}')){");
                        $body .= $this->f(3, "var {$var_marc_ltn} = {$this->var_markers}.get('{$marker->getName()}').getMarker();");
                        $body .= $marker->renderListeners($this->debug, 3, $var_marc_ltn);
                        $body .= $this->f(2, "}");
                    }
                }
            }
        }

        //polilineas iniciales
        if ($this->polylines and count($this->polylines)) {
            foreach ($this->polylines as $polyline) {
                if ($polyline instanceof Polyline) {
                    $body .= $this->f(2, "{$this->fc_add_polyline}(");
                    $body .= $polyline->render($this->debug, 3, $this->map_name);
                    $body .= $this->f(2, ");");
                }
            }
        }

        //poligonos iniciales
        if ($this->polygons and count($this->polygons)) {
            foreach ($this->polygons as $polygon) {
                if ($polygon instanceof Polygon) {
                    $body .= $this->f(2, "{$this->fc_add_polygon}(");
                    $body .= $polygon->render($this->debug, 3, $this->map_name);
                    $body .= $this->f(2, ");");
                }
            }
        }

        //circulos iniciales
        if ($this->circles and count($this->circles)) {
            foreach ($this->circles as $circle) {
                if ($circle instanceof Circle) {
                    $body .= $this->f(2, "{$this->fc_add_circle}(");
                    $body .= $circle->render($this->debug, 3, $this->map_name);
                    $body .= $this->f(2, ");");
                }
            }
        }

        //iniciar cluster
        $body .= $this->f(2, "{$this->var_stop_draw} = false;");
        $body .= $this->f(2, "{$this->fc_ini_cluster}();");

        $body .= $this->f(1, "};");

        //llamada a fc de inicializacion
        $body .= $this->f(1, "google.maps.event.addDomListener(window, 'load', initialize_{$this->div_name});");
        //cluster
        $body .= $this->f(1, "var {$this->var_stop_draw} = false;");
        $body .= $this->f(1, "var {$this->var_cluster} = null;");
        $body .= $this->f(1, "var {$this->var_cluster}_grid = {$this->cluster_grid};");
        $body .= $this->f(1, "var {$this->var_cluster}_minsize = {$this->cluster_minsize};");
        //funcion ini / stop draw
        $body .= $this->f(1, "function {$this->fc_ini_draw}() {");
        $body .= $this->f(2, "{$this->var_stop_draw} = false;");
        $body .= $this->f(2, "if({$this->var_cluster}==null){");
        $body .= $this->f(3, "{$this->fc_ini_cluster}();");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "{$this->var_cluster}.repaint();");
        $body .= $this->f(1, "}");
        $body .= $this->f(1, "function {$this->fc_stop_draw}() {");
        $body .= $this->f(2, "{$this->var_stop_draw} = true;");
        $body .= $this->f(1, "}");
        //funcion cluster
        $body .= $this->f(1, "function {$this->fc_get_cluster}() {");
        $body .= $this->f(2, "if({$this->var_cluster}==null){");
        $body .= $this->f(3, "{$this->fc_ini_cluster}();");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return {$this->var_cluster};");
        $body .= $this->f(1, "}");
        $body .= $this->f(1, "function {$this->fc_set_cluster_grid}(grid) {");
        $body .= $this->f(2, "{$this->var_cluster}_grid = grid;");
        $body .= $this->f(2, "if({$this->var_cluster}==null){");
        $body .= $this->f(3, "{$this->fc_ini_cluster}();");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "{$this->var_cluster}.setGridSize({$this->var_cluster}_grid);");
        $body .= $this->f(3, "if(!{$this->var_stop_draw}) {$this->var_cluster}.repaint();");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "}");
        $body .= $this->f(1, "function {$this->fc_set_cluster_minsize}(minsize) {");
        $body .= $this->f(2, "{$this->var_cluster}_minsize = minsize;");
        $body .= $this->f(2, "if({$this->var_cluster}==null){");
        $body .= $this->f(3, "{$this->fc_ini_cluster}();");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "{$this->var_cluster}.setMinimumClusterSize({$this->var_cluster}_minsize);");
        $body .= $this->f(3, "if(!{$this->var_stop_draw}) {$this->var_cluster}.repaint();");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "}");
        // get clustereable markers
        $body .= $this->f(1, "function {$this->fc_get_clusterable_markers}() {");
        $body .= $this->f(2, "var clusterArray = [];");
        //$body .= $this->f(0, "console.debug('------------------------------ fc_get_clusterable_markers');");
        $body .= $this->f(2, "var markersArray = {$this->var_markers}.values();");
        $body .= $this->f(2, "for(var i in markersArray){");
        $body .= $this->f(3, "var m = markersArray[i];");
        $body .= $this->f(3, "if(m.marker.getVisible() && m.clusterable){");
        $body .= $this->f(4, "clusterArray.push(m);");
        $body .= $this->f(3, "}");
        //$body .= $this->f(0, "console.debug('- MARKER '+m.id_sc+ ' - ['+m.marker.getVisible()+ ']['+clusterArray.length+']');");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return clusterArray");
        $body .= $this->f(1, "}");
        // get clustereable markers size
        $body .= $this->f(1, "function {$this->fc_get_clusterable_markers_size}() {");
        $body .= $this->f(2, "return {$this->fc_get_clusterable_markers}().length");
        $body .= $this->f(1, "}");
        $body .= $this->f(1, "function {$this->fc_ini_cluster}() {");
        $body .= $this->f(2, "if({$this->var_cluster} != null){");
        $body .= $this->f(3, "{$this->var_cluster}.clearMarkers();");
        $body .= $this->f(3, "{$this->var_cluster}.addMarkers({$this->fc_get_clusterable_markers}(), !{$this->var_stop_draw} );");
        $body .= $this->f(2, "}else{");
        $body .= $this->f(3, "var mo = {");
        $body .= $this->f(4, "gridSize: {$this->var_cluster}_grid");
        $body .= $this->f(4, ", minimumClusterSize: {$this->var_cluster}_minsize");
        $body .= $this->f(4, ", averageCenter: true");
        $body .= $this->f(4, ", maxZoom: null");
        $body .= $this->f(4, ", zoomOnClick: true");
        if ($this->cluster_styles) {
            $body .= $this->f(4, ", styles: [");
            $anterior = false;
            foreach ($this->cluster_styles as $cl_icon) {
                if ($anterior) {
                    $body .= $this->f(5, ", ");
                }
                $anterior = true;
                $body .= $cl_icon->render($this->debug, 5);
            }
            $body .= $this->f(4, "]");
        }
        $body .= $this->f(3, "};");
        $body .= $this->f(3, "{$this->var_cluster}=new MarkerClusterer({$this->map_name}, {$this->fc_get_clusterable_markers}(), mo);");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(!{$this->var_stop_draw}) {$this->var_cluster}.repaint();");
        $body .= $this->f(1, "}");
        //funcion cluster size
        $body .= $this->f(1, "function {$this->fc_get_cluster_size}() {");
        $body .= $this->f(2, "if({$this->var_cluster} != null){");
        $body .= $this->f(2, "return {$this->var_cluster}.markers_.length;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return 0;");
        $body .= $this->f(1, "}");
        //iconos
        $body .= $this->f(1, "var {$this->var_icons} = new HashTable();");
        $body .= $this->f(1, "var {$this->var_markers_icon} = new HashTable();");
        //funcion obtener icono
        $body .= $this->f(1, "function {$this->fc_get_icon}(parameters) {");
        $body .= $this->f(2, "var icon = undefined;");
        $body .= $this->f(2, "var shadow = undefined;");
        $body .= $this->f(2, "var shape = undefined;");
        $body .= $this->f(2, "if(parameters != undefined){");
        $body .= $this->f(3, "if(parameters.id_sc != undefined && {$this->var_icons}.exists(parameters.id_sc)){");
        $body .= $this->f(4, "var tmp_icon = {$this->var_icons}.get(parameters.id_sc);");
        $body .= $this->f(4, "icon = tmp_icon[0];");
        $body .= $this->f(4, "shadow = tmp_icon[1];");
        $body .= $this->f(4, "shape = tmp_icon[2];");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.icon_url != undefined){");
        $body .= $this->f(4, "if(parameters.icon_size_width != undefined && parameters.icon_size_height != undefined){");
        $body .= $this->f(5, "if(parameters.icon_origin_x != undefined && parameters.icon_origin_y != undefined){");
        $body .= $this->f(6, "if(parameters.icon_anchor_x != undefined && parameters.icon_anchor_y != undefined){");
        $body .= $this->f(7, "if(parameters.icon_scaled_size_width != undefined && parameters.icon_scaled_size_height != undefined){");
        $body .= $this->f(8, "icon = new google.maps.MarkerImage(");
        $body .= $this->f(9, "parameters.icon_url, ");
        $body .= $this->f(9, "new google.maps.Size(parameters.icon_size_width, parameters.icon_size_height), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.icon_origin_x, parameters.icon_origin_y), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.icon_anchor_x, parameters.icon_anchor_y), ");
        $body .= $this->f(9, "new google.maps.Size(parameters.icon_scaled_size_width, parameters.icon_scaled_size_height)");
        $body .= $this->f(8, ");");
        $body .= $this->f(7, "} else {");
        $body .= $this->f(8, "icon = new google.maps.MarkerImage(");
        $body .= $this->f(9, "parameters.icon_url, ");
        $body .= $this->f(9, "new google.maps.Size(parameters.icon_size_width, parameters.icon_size_height), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.icon_origin_x, parameters.icon_origin_y), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.icon_anchor_x, parameters.icon_anchor_y)");
        $body .= $this->f(8, ");");
        $body .= $this->f(7, "}");
        $body .= $this->f(6, "} else {");
        $body .= $this->f(7, "icon = new google.maps.MarkerImage(");
        $body .= $this->f(8, "parameters.icon_url, ");
        $body .= $this->f(8, "new google.maps.Size(parameters.icon_size_width, parameters.icon_size_height), ");
        $body .= $this->f(8, "new google.maps.Point(parameters.icon_origin_x, parameters.icon_origin_y)");
        $body .= $this->f(7, ");");
        $body .= $this->f(6, "}");
        $body .= $this->f(5, "} else {");
        $body .= $this->f(6, "icon = new google.maps.MarkerImage(");
        $body .= $this->f(7, "parameters.icon_url, ");
        $body .= $this->f(7, "new google.maps.Size(parameters.icon_size_width, parameters.icon_size_height)");
        $body .= $this->f(6, ");");
        $body .= $this->f(5, "}");
        $body .= $this->f(4, "} else {");
        $body .= $this->f(5, "icon = new google.maps.MarkerImage(parameters.icon_url);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.shadow_url != undefined){");
        $body .= $this->f(4, "if(parameters.shadow_size_width != undefined && parameters.shadow_size_height != undefined){");
        $body .= $this->f(5, "if(parameters.shadow_origin_x != undefined && parameters.shadow_origin_y != undefined){");
        $body .= $this->f(6, "if(parameters.shadow_anchor_x != undefined && parameters.shadow_anchor_y != undefined){");
        $body .= $this->f(7, "if(parameters.shadow_scaled_size_width != undefined && parameters.shadow_scaled_size_height != undefined){");
        $body .= $this->f(8, "shadow = new google.maps.MarkerImage(");
        $body .= $this->f(9, "parameters.shadow_url, ");
        $body .= $this->f(9, "new google.maps.Size(parameters.shadow_size_width, parameters.shadow_size_height), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.shadow_origin_x, parameters.shadow_origin_y), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.shadow_anchor_x, parameters.shadow_anchor_y), ");
        $body .= $this->f(9, "new google.maps.Size(parameters.shadow_scaled_size_width, parameters.shadow_scaled_size_height)");
        $body .= $this->f(8, ");");
        $body .= $this->f(7, "} else {");
        $body .= $this->f(8, "shadow = new google.maps.MarkerImage(");
        $body .= $this->f(9, "parameters.shadow_url, ");
        $body .= $this->f(9, "new google.maps.Size(parameters.shadow_size_width, parameters.shadow_size_height), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.shadow_origin_x, parameters.shadow_origin_y), ");
        $body .= $this->f(9, "new google.maps.Point(parameters.shadow_anchor_x, parameters.shadow_anchor_y)");
        $body .= $this->f(8, ");");
        $body .= $this->f(7, "}");
        $body .= $this->f(6, "} else {");
        $body .= $this->f(7, "shadow = new google.maps.MarkerImage(");
        $body .= $this->f(8, "parameters.shadow_url, ");
        $body .= $this->f(8, "new google.maps.Size(parameters.shadow_size_width, parameters.shadow_size_height), ");
        $body .= $this->f(8, "new google.maps.Point(parameters.shadow_origin_x, parameters.shadow_origin_y)");
        $body .= $this->f(7, ");");
        $body .= $this->f(6, "}");
        $body .= $this->f(5, "} else {");
        $body .= $this->f(6, "shadow = new google.maps.MarkerImage(");
        $body .= $this->f(7, "parameters.shadow_url, ");
        $body .= $this->f(7, "new google.maps.Size(parameters.shadow_size_width, parameters.shadow_size_height)");
        $body .= $this->f(6, ");");
        $body .= $this->f(5, "}");
        $body .= $this->f(4, "} else {");
        $body .= $this->f(5, "shadow = new google.maps.MarkerImage(parameters.shadow_url);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.shape_type != undefined && parameters.shape_coords != undefined){");
        $body .= $this->f(4, "shape = {type: parameters.shape_type, coord: parameters.shape_coords};");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return [icon, shadow, shape];");
        $body .= $this->f(1, "};");
        //funcion agregar icono
        $body .= $this->f(1, "function {$this->fc_add_icon}(parameters, update) {");
        $body .= $this->f(2, "if(parameters != undefined && parameters.id_sc != undefined){");
        $body .= $this->f(3, "var res = {$this->fc_get_icon}(parameters);");
        $body .= $this->f(3, "{$this->var_icons}.put(parameters.id_sc, res);");
        $body .= $this->f(3, "if(update != undefined && update){");
        $body .= $this->f(4, "{$this->fc_update_icons}();");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion agregar iconos
        $body .= $this->f(1, "function {$this->fc_add_all_icons}(all) {");
        $body .= $this->f(2, "if(all != undefined){");
        $body .= $this->f(3, "if(jQuery.isArray(all)){");
        $body .= $this->f(4, "{$this->fc_stop_draw}();");
        $body .= $this->f(4, "for(var p in all){");
        $body .= $this->f(5, "{$this->fc_add_icon}(all[p], false);");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "{$this->fc_ini_draw}();");
        $body .= $this->f(4, "{$this->fc_update_icons}();");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "{$this->fc_add_icon}(all, true);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion actualizar iconos/marcadores
        $body .= $this->f(1, "function {$this->fc_update_icons}() {");
        $body .= $this->f(2, "if({$this->var_markers_icon} != undefined){");
        $body .= $this->f(3, "{$this->fc_stop_draw}();");
        $body .= $this->f(3, "var mks = [];");
        $body .= $this->f(3, "var keys = {$this->var_markers_icon}.keys();");
        $body .= $this->f(3, "for(var k in keys){");
        $body .= $this->f(4, "mks.push({id_sc: keys[k], icono: {$this->var_markers_icon}.get(keys[k])});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "{$this->fc_ini_draw}();");
        $body .= $this->f(3, "{$this->fc_add_all_markers}(mks);");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");

        //marcador con label
        $body .= $this->f(1, "function Marcador(options){");
        $body .= $this->f(2, "this.setMap_original = this.setMap;");
        $body .= $this->f(2, "this.setValues(options);");
        $body .= $this->f(2, "this.marker = new google.maps.Marker(options);");
        $body .= $this->f(2, "this.bindTo('position', this.marker, 'position');");
        $body .= $this->f(2, "this.updateInformacion();");
        $body .= $this->f(2, "var me = this;");
        $body .= $this->f(2, "google.maps.event.addListener(this.marker, 'click', function(e){ openInformacion(e, me) });");
        $body .= $this->f(2, "var div = this.div_ = document.createElement('div');");
        $body .= $this->f(2, "div.style.cssText = 'position: absolute; display: none; text-align: center; ';");
        $body .= $this->f(2, "div.setAttribute('class','label_marcador');");
        $body .= $this->f(2, "this.setMap = function(mapa) {");
        $body .= $this->f(3, "if(this.marker != undefined){");
        $body .= $this->f(4, "this.marker.setMap(mapa);");
        $body .= $this->f(3, "};");
        $body .= $this->f(3, "this.setMap_original(mapa);");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        //marcador con label - prototipo
        $body .= $this->f(1, "Marcador.prototype = new google.maps.OverlayView;");
        //marcador con label - onAdd
        $body .= $this->f(1, "Marcador.prototype.onAdd = function() {");
        $body .= $this->f(2, "var pane = this.getPanes().overlayImage;");
        $body .= $this->f(2, "pane.appendChild(this.div_);");
        $body .= $this->f(2, "var me = this;");
        $body .= $this->f(2, "this.listeners_ = [");
        $body .= $this->f(3, "google.maps.event.addListener(this, 'position_changed', function() { if(!{$this->var_stop_draw}) me.draw(); }),");
        $body .= $this->f(3, "google.maps.event.addListener(this.marker, 'position_changed', function() { me.position = me.marker.position; if(!{$this->var_stop_draw}) me.draw(); }),");
        $body .= $this->f(3, "google.maps.event.addListener(this, 'text_changed', function() { if(!{$this->var_stop_draw}) me.draw(); }),");
        $body .= $this->f(3, "google.maps.event.addListener(this, 'zindex_changed', function() { if(!{$this->var_stop_draw}) me.draw(); })");
        $body .= $this->f(2, "];");
        $body .= $this->f(1, "};");
        //marcador con label - onRemove
        $body .= $this->f(1, "Marcador.prototype.onRemove = function() {");
        $body .= $this->f(2, "if (this.div_.parentNode != null) {");
        $body .= $this->f(2, "this.div_.parentNode.removeChild(this.div_);");
        $body .= $this->f(3, "for (var i = 0, I = this.listeners_.length; i < I; ++i) {");
        $body .= $this->f(4, "google.maps.event.removeListener(this.listeners_[i]);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - setInformacion
        $body .= $this->f(1, "Marcador.prototype.setInformacion = function(informacion) {");
        $body .= $this->f(2, "this.informacion = informacion;");
        $body .= $this->f(2, "this.updateInformacion();");
        $body .= $this->f(1, "};");
        //marcador con label - openInfo
        $body .= $this->f(1, "Marcador.prototype.openInfo = function() {");
        $body .= $this->f(2, "this.updateInformacion();");
        $body .= $this->f(2, "if(this.real_infowindow != undefined){");
        $body .= $this->f(3, "this.real_infowindow.setPosition(this.marker.getPosition());");
        $body .= $this->f(3, "this.real_infowindow.setZIndex(10000);");
        $body .= $this->f(3, "this.real_infowindow.open({$this->map_name}, this.marker);");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - closeInfo
        $body .= $this->f(1, "Marcador.prototype.closeInfo = function() {");
        $body .= $this->f(2, "if(this.real_infowindow != undefined){");
        $body .= $this->f(3, "this.real_infowindow.close();");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - updateInformacion
        $body .= $this->f(1, "Marcador.prototype.updateInformacion = function() {");
        $body .= $this->f(2, "if(this.informacion == undefined || this.informacion == ''){");
        $body .= $this->f(3, "this.closeInfo();");
        $body .= $this->f(3, "this.real_infowindow = undefined;");
        $body .= $this->f(3, "this.ultimo_infowindows = undefined;  ");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "if(this.real_infowindow == undefined){");
        $body .= $this->f(4, "this.real_infowindow = new google.maps.InfoWindow();");
        $body .= $this->f(4, "this.ultimo_infowindows = undefined; ");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(this.ultimo_infowindows != this.informacion){");
        $body .= $this->f(4, "this.real_infowindow.setContent(this.informacion);");
        $body .= $this->f(4, "this.ultimo_infowindows = this.informacion; ");
        $body .= $this->f(4, "var mp = this.real_infowindow.getMap();");
        $body .= $this->f(4, "if(mp){");
        $body .= $this->f(5, "this.real_infowindow.close();");
        $body .= $this->f(5, "this.real_infowindow.open(mp, this.marker);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - openInformacion
        $body .= $this->f(1, "function openInformacion(e, marcador) {");
        $body .= $this->f(2, "if(e != undefined && marcador != undefined){");
        $body .= $this->f(3, "marcador.openInfo();");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - draw
        $body .= $this->f(1, "Marcador.prototype.draw = function() {");
        $body .= $this->f(2, "if(!{$this->var_stop_draw}){");
        $body .= $this->f(2, "if(this.div_ != undefined){");
        $body .= $this->f(2, "var div = this.div_;");
        $body .= $this->f(2, "var crear = false;");
        $body .= $this->f(2, "var existe = false;");
        $body .= $this->f(2, "var projection = this.getProjection();");
        $body .= $this->f(2, "if(projection != undefined){");
        $body .= $this->f(3, "var position = projection.fromLatLngToDivPixel(this.get('position'));");
        $body .= $this->f(3, "div.style.left = position.x + 'px';");
        $body .= $this->f(3, "div.style.top = position.y + 'px';");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "div.style.display = 'block';");
        $body .= $this->f(2, "div.style.zIndex = 605; ");
        $body .= $this->f(2, "var div_interno = undefined;");
        $body .= $this->f(2, "if(this.div_interno_ != undefined){");
        $body .= $this->f(2, "div_interno = this.div_interno_;");
        $body .= $this->f(3, "existe = true;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var espacio = undefined;");
        $body .= $this->f(2, "if(this.espacio_ != undefined){");
        $body .= $this->f(3, "espacio = this.espacio_;");
        $body .= $this->f(3, "existe = true;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var span = undefined;");
        $body .= $this->f(2, "if(this.span_ != undefined){");
        $body .= $this->f(3, "span = this.span_;");
        $body .= $this->f(3, "existe = true;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var spans = undefined;");
        $body .= $this->f(2, "if(this.spans_ != undefined){");
        $body .= $this->f(3, "spans = this.spans_;");
        $body .= $this->f(3, "existe = true;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var label_borde = 2;");
        $body .= $this->f(2, "if(this.label_borde != undefined){");
        $body .= $this->f(3, "label_borde = this.label_borde;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(this.ultimo_borde == undefined || this.ultimo_borde != label_borde){");
        $body .= $this->f(3, "this.ultimo_borde = label_borde;");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(!existe || crear){");
        $body .= $this->f(3, "crear = true;");
        $body .= $this->f(3, "var div_interno = document.createElement('div');");
        $body .= $this->f(3, "div_interno.style.cssText = 'position: relative; left: -50%; top: -10px; color: black; white-space: nowrap; font-family: Arial; font-weight: normal; font-size: 14px; line-height:13px; text-align: center;';");
        $body .= $this->f(3, "div_interno.setAttribute('class','label_interno');");
        $body .= $this->f(3, "var espacio = document.createElement('span');");
        $body .= $this->f(3, "espacio.style.cssText = 'visibility: hidden;';");
        $body .= $this->f(3, "div_interno.appendChild(espacio);");
        $body .= $this->f(3, "var max_cant = 1 + label_borde*2;");
        $body .= $this->f(3, "spans = new Array();");
        $body .= $this->f(3, "for (var i = 0; i < max_cant; i++) {");
        $body .= $this->f(4, "for (var j = 0; j < max_cant; j++) {");
        $body .= $this->f(5, "var spani = document.createElement('span');");
        $body .= $this->f(5, "spani.setAttribute('class','label_borde');");
        $body .= $this->f(5, "spani.style.cssText = 'position: absolute; left:'+(i-label_borde)+'px; top:'+(j-label_borde)+'px; z-index:100; color:white;';");
        $body .= $this->f(5, "spans[i+j*max_cant] = spani;");
        $body .= $this->f(5, "div_interno.appendChild(spani);");
        $body .= $this->f(4, "};");
        $body .= $this->f(3, "};");
        $body .= $this->f(3, "var span = document.createElement('span');");
        $body .= $this->f(3, "span.setAttribute('class','label_texto');");
        $body .= $this->f(3, "span.style.cssText = 'position: absolute; left:0; top:0; z-index:150; color:black;';");
        $body .= $this->f(3, "div_interno.appendChild(span);");
        $body .= $this->f(3, "this.ultimo_texto = undefined;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var label_texto = '';");
        $body .= $this->f(2, "if(this.label_texto != undefined){");
        $body .= $this->f(3, "label_texto = this.label_texto.toString();");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(this.ultimo_texto == undefined || this.ultimo_texto != label_texto){");
        $body .= $this->f(3, "var texto_a_colocar = label_texto;");
        $body .= $this->f(3, "if(texto_a_colocar.length > " . $this->label_max_longitud . "){");
        $body .= $this->f(4, "texto_a_colocar = texto_a_colocar.substring(0," . $this->label_max_longitud . ")+'\u2026';");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(texto_a_colocar.length > 1){");
        $body .= $this->f(4, "texto_a_colocar = texto_a_colocar.match(RegExp('.{1," . $this->label_wrap_longitud . "}(\\\\s|$)|\\\\S+?(\\\\s|$)', 'g')).join('<br/>');");
        $body .= $this->f(4, "var matches = texto_a_colocar.match(/<br\/>/g);");
        $body .= $this->f(4, "var saltos = (matches ? matches.length : 0);");
        $body .= $this->f(4, "if(saltos > 0){");
        $body .= $this->f(5, "div_interno.setAttribute('class','label_interno label_interno_salto');");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "for(var i=0; i < spans.length; i++){");
        $body .= $this->f(4, "spans[i].innerHTML = texto_a_colocar;");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "span.innerHTML = texto_a_colocar;");
        $body .= $this->f(3, "espacio.innerHTML = texto_a_colocar;");
        $body .= $this->f(3, "this.ultimo_texto = label_texto;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "var label_clase = 'estilo_defecto';");
        $body .= $this->f(2, "if(this.label_clase != undefined && this.label_clase != ''){");
        $body .= $this->f(3, "label_clase = this.label_clase.toString();");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(this.ultimo_clase == undefined || this.ultimo_clase != label_clase){");
        $body .= $this->f(3, "div.setAttribute('class','label_marcador '+label_clase);");
        $body .= $this->f(3, "this.ultimo_clase = label_clase;");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "this.div_interno_ = div_interno;");
        $body .= $this->f(2, "this.espacio_ = espacio;");
        $body .= $this->f(2, "this.span_ = span;");
        $body .= $this->f(2, "this.spans_ = spans;");
        $body .= $this->f(2, "if(existe && crear){");
        $body .= $this->f(3, "while (div.firstChild) {");
        $body .= $this->f(4, "div.removeChild(div.firstChild);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "div.appendChild(div_interno);");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //marcador con label - sets de markers
        $body .= $this->f(1, "Marcador.prototype.setOptions = function(options){");
        $body .= $this->f(2, "var vl = null;");
        $body .= $this->f(2, "var vm = null;");
        $body .= $this->f(2, "var val = null;");
        $body .= $this->f(2, "var vam = null;");
        $body .= $this->f(2, "var v1 = null;");
        $body .= $this->f(2, "var v2 = null;");
        $body .= $this->f(2, "var v3 = null;");
        $body .= $this->f(2, "if(options != undefined){");
        $body .= $this->f(3, "if (options.visible != undefined) {");
        $body .= $this->f(4, "v1 = options.visible;");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if (options.visible_leyenda != undefined) {");
        $body .= $this->f(4, "v2 = options.visible_leyenda;");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if (options.visible_marcador != undefined) {");
        $body .= $this->f(4, "v3 = options.visible_marcador;");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(v2 != null){");
        $body .= $this->f(3, "vl = v2;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "vl = v1; ");
        $body .= $this->f(2, "} ");
        $body .= $this->f(2, "if(v3 != null){");
        $body .= $this->f(3, "vm = v3;");
        $body .= $this->f(2, "} else {");
        $body .= $this->f(3, "vm = v1; ");
        $body .= $this->f(2, "} ");
        $body .= $this->f(2, "var new_options = [];");
        $body .= $this->f(2, "for(var i in options){");
        $body .= $this->f(3, "if(i!='visible' || i!='visible_marcador' || i!='visible_leyenda'){");
        $body .= $this->f(4, "new_options[i] = options[i];");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "vam = this.marker.getVisible();");
        $body .= $this->f(3, "this.marker.setOptions(new_options);");
        $body .= $this->f(2, "};");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "val = this.div_.style.visibility!='hidden';");
        $body .= $this->f(2, "};");
        $body .= $this->f(2, "this.setValues(new_options);");
        $body .= $this->f(2, "google.maps.OverlayView.prototype.setOptions.call(new_options);");
        $body .= $this->f(2, "this.updateInformacion();");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "if(vl != null && vl != val){");
        $body .= $this->f(4, "if(vl){");
        $body .= $this->f(5, "this.div_.style.visibility='';");
        $body .= $this->f(4, "} else {");
        $body .= $this->f(5, "this.div_.style.visibility='hidden';");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "};");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "if(vm != null && vm != vam){");
        $body .= $this->f(4, "this.marker.setVisible(vm);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.setPosition = function(position) {");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "this.marker.setPosition(position);");
        $body .= $this->f(2, "};");
        $body .= $this->f(2, "this.position = position;");
        $body .= $this->f(2, "if(this.div_ != undefined && this.div_.style != undefined){");
        $body .= $this->f(3, "if(!{$this->var_stop_draw}) this.draw();");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.setIcon = function(image) {");
        $body .= $this->f(2, "if(this.marker != undefined && this.marker.getIcon() != image){");
        $body .= $this->f(3, "this.marker.setIcon(image);");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.setShadow = function(image) {");
        $body .= $this->f(2, "if(this.marker != undefined && this.marker.getShadow() != image){");
        $body .= $this->f(3, "this.marker.setShadow(image);");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.setShape = function(image) {");
        $body .= $this->f(2, "if(this.marker != undefined && this.marker.getShape() != image){");
        $body .= $this->f(3, "this.marker.setShape(image);");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.getPosition = function() {");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "return this.marker.getPosition();");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "return null;");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.getDraggable = function() {");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "return this.marker.getDraggable();");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "return false;");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.setTitle = function(titulo) {");
        $body .= $this->f(2, "if(this.marker != undefined){");
        $body .= $this->f(3, "this.marker.setTitle(titulo);");
        $body .= $this->f(2, "};");
        $body .= $this->f(1, "};");
        //marcador con label - get marcador de google
        $body .= $this->f(1, "Marcador.prototype.getMarker = function() {");
        $body .= $this->f(2, "return this.marker;");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.marcador = function() {");
        $body .= $this->f(2, "return this.marker;");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "Marcador.prototype.divLeyenda = function() {");
        $body .= $this->f(2, "return this.div_;");
        $body .= $this->f(1, "};");

        //marcadores 
        $body .= $this->f(1, "var {$this->var_markers} = new HashTable();");
        //funcion agregar marcador
        $body .= $this->f(1, "function {$this->fc_add_marker}(parameters) {");
        $body .= $this->f(2, "var nuevo = false;");
        $body .= $this->f(2, "if(parameters != undefined && parameters.id_sc != undefined){");
        $body .= $this->f(3, "var marker = undefined;");
        $body .= $this->f(3, "if(!{$this->var_markers}.exists(parameters.id_sc)){");
        $body .= $this->f(4, "marker = new Marcador({");
        $body .= $this->f(5, "position: new google.maps.LatLng(0, 0, true), title: '', visible: true, draggable: false, clickable: true, flat: false, crossOnDrag: true, optimized: true, map: {$this->map_name} ");
        $body .= $this->f(4, "});");
        $body .= $this->f(4, "nuevo = true;");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "marker = {$this->var_markers}.get(parameters.id_sc);");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "parameters.nuevo = nuevo;");
        $body .= $this->f(3, "marker.setOptions(parameters);");
        $body .= $this->f(3, "if(parameters.latitud != undefined && parameters.longitud != undefined){");
        $body .= $this->f(4, "marker.setPosition(new google.maps.LatLng(parameters.latitud, parameters.longitud, true));");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.texto != undefined ){");
        $body .= $this->f(4, "marker.setTitle(parameters.texto);");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "var icono_name = undefined;");
        $body .= $this->f(3, "if(parameters.icono != undefined){");
        $body .= $this->f(4, "icono_name = parameters.icono;");
        $body .= $this->f(4, "if({$this->var_icons}.exists(parameters.icono)){");
        $body .= $this->f(5, "var ic = {$this->var_icons}.get(parameters.icono);");
        $body .= $this->f(5, "marker.setIcon(ic[0]);");
        $body .= $this->f(5, "marker.setShadow(ic[1]);");
        $body .= $this->f(5, "marker.setShape(ic[2]);");
        $body .= $this->f(4, "} else {");
        $body .= $this->f(5, "marker.setIcon(undefined);");
        $body .= $this->f(5, "marker.setShadow(undefined);");
        $body .= $this->f(5, "marker.setShape(undefined);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else if(parameters.new_icono != undefined){");
        $body .= $this->f(4, "var new_icono = {$this->fc_get_icon}(parameters.new_icono);");
        $body .= $this->f(5, "marker.setIcon(new_icono[0]);");
        $body .= $this->f(5, "marker.setShadow(new_icono[1]);");
        $body .= $this->f(5, "marker.setShape(new_icono[2]);");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "marker.setMap({$this->map_name});");
        $body .= $this->f(3, "if(!{$this->var_stop_draw}) marker.draw();");
        $body .= $this->f(3, "{$this->var_markers}.put(parameters.id_sc, marker);");
        $body .= $this->f(3, "{$this->var_markers_icon}.put(parameters.id_sc, icono_name);");
        $body .= $this->f(2, "}");
        //---- inicio la colocacion del listener_click_me -----
//        $body .= $this->f(3, "if(parameters.listener_click_me != undefined ){");
//        $body .= $this->f(4, "var marc_list = {$this->map_name}_markers.get(parameters.id_sc).getMarker();");
//        $body .= $this->f(4, "console.log(marc_list.listener_click_me);");
//        $body .= $this->f(4, "var fc = marc_list.listener_click_me;");
//        $body .= $this->f(4, "google.maps.event.addListener(marc_list, 'click', function(e){ marc_list.listener_click_me(e, marc_list) });");
//        $body .= $this->f(3, "}");
        //---- fin la colocacion del listener_click_me -----
        $body .= $this->f(2, "if(!{$this->var_stop_draw}) {");
        $body .= $this->f(3, "{$this->fc_ini_cluster}();");
        $body .= $this->f(2, "}");
        //$body .= $this->f(2, "return existe;");
        $body .= $this->f(1, "};");
        //funcion agregar marcadores
        $body .= $this->f(1, "function {$this->fc_add_all_markers}(all) {");
        $body .= $this->f(2, "if(all != undefined){");
        $body .= $this->f(3, "if(jQuery.isArray(all)){");
        $body .= $this->f(4, "{$this->fc_stop_draw}();");
        $body .= $this->f(4, "var puntos_iniciales = {$this->fc_get_cluster_size}();");
        $body .= $this->f(4, "for(var p in all){");
        $body .= $this->f(5, "{$this->fc_add_marker}(all[p]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "var puntos_finales = {$this->fc_get_clusterable_markers_size}();");
        $body .= $this->f(4, "{$this->fc_ini_draw}();");
        $body .= $this->f(4, "if(puntos_iniciales != puntos_finales) {");
        $body .= $this->f(5, "{$this->fc_ini_cluster}();");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "{$this->fc_add_marker}(all);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion cambiar todos los marcadores
        $body .= $this->f(1, "function {$this->fc_change_all_marker}(patern, options) {");
        $body .= $this->f(2, "{$this->fc_stop_draw}();");
        $body .= $this->f(2, "{$this->var_markers}.each(function(id, mk){");
        $body .= $this->f(3, "if(id.indexOf(patern) != -1){");
        $body .= $this->f(4, "var p = new Array();");
        $body .= $this->f(4, "p['id_sc'] = id;");
        $body .= $this->f(4, "for(var i in options){");
        $body .= $this->f(5, "p[i] = options[i];");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "{$this->fc_add_marker}(p);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "});");
        $body .= $this->f(2, "{$this->fc_ini_draw}();");
        $body .= $this->f(1, "};");
        //funcion centrar marcador
        $body .= $this->f(1, "function {$this->fc_center_marker}(id_marker) {");
        $body .= $this->f(2, "if(id_marker != undefined && {$this->var_markers}.exists(id_marker)){");
        $body .= $this->f(3, "{$this->map_name}.setCenter({$this->var_markers}.get(id_marker).getPosition());");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");

        //polilineas 
        $body .= $this->f(1, "var {$this->var_polylines} = new HashTable();");
        //funcion agregar polilinea
        $body .= $this->f(1, "function {$this->fc_add_polyline}(parameters) {");
        $body .= $this->f(2, "if(parameters != undefined && parameters.id_sc != undefined){");
        $body .= $this->f(3, "var polyline = undefined;");
        $body .= $this->f(3, "if(!{$this->var_polylines}.exists(parameters.id_sc)){");
        $body .= $this->f(4, "polyline = new google.maps.Polyline({");
        $body .= $this->f(5, "path: [], visible: true, clickable: false, editable: false, geodesic: false, strokeColor: '#000000', strokeWeight: 1, strokeOpacity: 1.0");
        $body .= $this->f(4, "});");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "polyline = {$this->var_polylines}.get(parameters.id_sc);");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "polyline.setOptions(parameters);");
        $body .= $this->f(3, "if(parameters.color != undefined ){");
        $body .= $this->f(4, "polyline.setOptions({strokeColor: parameters.color});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.ancho != undefined && jQuery.isNumeric(parameters.ancho) && parameters.ancho >= 0){");
        $body .= $this->f(4, "polyline.setOptions({strokeWeight: parameters.ancho});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.puntos != undefined && jQuery.isArray(parameters.puntos)){");
        $body .= $this->f(4, "var puntos = [];");
        $body .= $this->f(4, "for(var p in parameters.puntos){");
        $body .= $this->f(5, "puntos.push(new google.maps.LatLng(parameters.puntos[p][0], parameters.puntos[p][1]));");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "polyline.setOptions({path: puntos});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "polyline.setMap({$this->map_name});");
        $body .= $this->f(3, "{$this->var_polylines}.put(parameters.id_sc, polyline);");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion agregar polilineas
        $body .= $this->f(1, "function {$this->fc_add_all_polylines}(all) {");
        $body .= $this->f(2, "if(all != undefined){");
        $body .= $this->f(3, "if(jQuery.isArray(all)){");
        $body .= $this->f(4, "for(var p in all){");
        $body .= $this->f(5, "{$this->fc_add_polyline}(all[p]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "{$this->fc_add_polyline}(all);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");

        //poligonos   
        $body .= $this->f(1, "var {$this->var_polygons} = new HashTable(); ");
        //funcion agregar poligono
        $body .= $this->f(1, "function {$this->fc_add_polygon}(parameters){");
        $body .= $this->f(2, "if(parameters != undefined && parameters.id_sc != undefined){");
        $body .= $this->f(3, "var polygon = undefined;");
        $body .= $this->f(3, "if(!{$this->var_polygons}.exists(parameters.id_sc)) {");
        $body .= $this->f(4, "polygon = new google.maps.Polygon({");
        $body .= $this->f(5, "path: [], visible: true, clickable: false,editable: false, geodesic: false, strokeColor: '#000000', strokeWeight: 1, strokeOpacity: 1.0, fillColor: '#000000', fillOpacity: 0");
        $body .= $this->f(4, "});");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "polygon = {$this->var_polygons}.get(parameters.id_sc); ");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "polygon.setOptions(parameters);");
        $body .= $this->f(3, "if(parameters.color != undefined){");
        $body .= $this->f(4, "polygon.setOptions({strokeColor: parameters.color});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.ancho != undefined && jQuery.isNumeric(parameters.ancho) && parameters.ancho >= 0){");
        $body .= $this->f(4, "polygon.setOptions({strokeWeight: parameters.ancho});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.puntos != undefined && jQuery.isArray(parameters.puntos)){");
        $body .= $this->f(4, "var puntos = [];");
        $body .= $this->f(4, "for(var p in parameters.puntos){");
        $body .= $this->f(5, "puntos.push(new google.maps.LatLng(parameters.puntos[p][0], parameters.puntos[p][1]));");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "polygon.setOptions({path: puntos});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "polygon.setMap({$this->map_name});");
        $body .= $this->f(3, "{$this->var_polygons}.put(parameters.id_sc, polygon);");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion agregar poligonos
        $body .= $this->f(1, "function {$this->fc_add_all_polygons}(all) {");
        $body .= $this->f(2, "if(all != undefined){");
        $body .= $this->f(3, "if(jQuery.isArray(all)){");
        $body .= $this->f(4, "for(var p in all){");
        $body .= $this->f(5, "{$this->fc_add_polygon}(all[p]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "{$this->fc_add_polygon}(all);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion get path poligono
        $body .= $this->f(1, "function {$this->fc_get_polygon_path}(pol) {");
        $body .= $this->f(2, "var res = new Array();");
        $body .= $this->f(2, "if(pol != undefined){");
        $body .= $this->f(3, "if({$this->var_polygons}.exists(pol)){");
        $body .= $this->f(4, "var polygon = {$this->var_polygons}.get(pol).getPath().getArray();");
        $body .= $this->f(4, "var pr = null;");
        $body .= $this->f(4, "for(var n = 0; n < polygon.length; n++){");
        $body .= $this->f(5, "var p = polygon[n];");
        $body .= $this->f(5, "if(pr == null) pr=p;");
        $body .= $this->f(5, "res.push([p.lat(), p.lng()]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(4, "if(pr != null){");
        $body .= $this->f(5, "res.push([pr.lat(), pr.lng()]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return res;");
        $body .= $this->f(1, "};");
        $body .= $this->f(1, "function {$this->fc_get_polygon_object}(pol) {");
        $body .= $this->f(2, "var res = null;");
        $body .= $this->f(2, "if(pol != undefined){");
        $body .= $this->f(3, "if({$this->var_polygons}.exists(pol)){");
        $body .= $this->f(4, "var polygon = {$this->var_polygons}.get(pol).getPath();");
        $body .= $this->f(5, "if(polygon != null) res = polygon;");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(2, "return res;");
        $body .= $this->f(1, "};");

        //circulos   
        $body .= $this->f(1, "var {$this->var_circles} = new HashTable(); ");
        //funcion agregar circulo
        $body .= $this->f(1, "function {$this->fc_add_circle}(parameters){");
        $body .= $this->f(2, "if(parameters != undefined && parameters.id_sc != undefined){");
        $body .= $this->f(3, "var circle = undefined;");
        $body .= $this->f(3, "if(!{$this->var_circles}.exists(parameters.id_sc)) {");
        $body .= $this->f(4, "circle = new google.maps.Circle({");
        $body .= $this->f(5, "visible: true, clickable: false, editable: false, strokeColor: '#000000', strokeWeight: 1, strokeOpacity: 1.0, fillColor: '#000000', fillOpacity: 0, radius: 10");
        $body .= $this->f(4, "});");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "circle = {$this->var_circles}.get(parameters.id_sc); ");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "circle.setOptions(parameters);");
        $body .= $this->f(3, "if(parameters.radio != undefined){");
        $body .= $this->f(4, "circle.setOptions({radius: parameters.radio});");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "if(parameters.latitud != undefined && parameters.longitud != undefined){");
        $body .= $this->f(4, "circle.setCenter(new google.maps.LatLng(parameters.latitud, parameters.longitud, true));");
        $body .= $this->f(3, "}");
        $body .= $this->f(3, "circle.setMap({$this->map_name});");
        $body .= $this->f(3, "{$this->var_circles}.put(parameters.id_sc, circle);");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");
        //funcion agregar circulos
        $body .= $this->f(1, "function {$this->fc_add_all_circles}(all) {");
        $body .= $this->f(2, "if(all != undefined){");
        $body .= $this->f(3, "if(jQuery.isArray(all)){");
        $body .= $this->f(4, "for(var p in all){");
        $body .= $this->f(5, "{$this->fc_add_circle}(all[p]);");
        $body .= $this->f(4, "}");
        $body .= $this->f(3, "} else {");
        $body .= $this->f(4, "{$this->fc_add_circle}(all);");
        $body .= $this->f(3, "}");
        $body .= $this->f(2, "}");
        $body .= $this->f(1, "};");

        //funcion redimensionado
        $body .= $this->f(1, "function {$this->fc_redimension}() {");
        $body .= $this->f(2, "google.maps.event.trigger({$this->map_name}, 'resize');");
        $body .= $this->f(1, "};");

        $api = GMHelper::getAPI();
        if ($api[1] >= 2) {
            $this->encriptar_url = true;
        }

        //-- se arma JS para enviar
        $res = '';
        $res .= $this->f(0, '');
        if (!GMHelper::isCargadaAPI()) {
            $res .= $this->f(0, HtmlHelper::getSectionRefJS($this->debug, $api[0]));
            GMHelper::setCargadaAPI(true);
        }
        if ($this->include_jquery) {
            $res .= $this->f(0, JQHelper::getSectionJQuery($this->debug));
        }
        if (!GMHelper::isCargadoHelpersMap()) {
            $res .= $this->f(0, JQHelper::getSectionJQPeriodicalUpdater($this->debug));
            $res .= $this->f(0, HtmlHelper::addHashMap($this->debug));
            $res .= $this->f(0, HtmlHelper::addMarkerCluster($this->debug, $this->var_stop_draw));
            GMHelper::setCargadoHelpersMap(true);
        }

        $res .= $this->f(0, HtmlHelper::getSectionJS($this->debug, $body));

        return $res;
    }

    private function f($tabs, $contenido) {
        return HtmlHelper::format($this->debug, $tabs, $contenido);
    }

    public function recargaJsonManual($url, $callback = null) {
        $res = $this->f(2, "");
        $res .= $this->f(2, "jQuery.get( '{$url}', {}, function(data) {");
        $res .= $this->f(3, "{$this->fc_add_all_markers}(data);");
        if ($callback) {
            $res .= $this->f(3, "{$callback}(data);");
        }
        $res .= $this->f(2, "}, 'json');");

        return $res;
    }

    public function recargaJsonAutomatica($url, $time, $callback = null) {
        $res = $this->f(2, "");
        $res .= $this->f(2, "jQuery(document).ready(function(){");
        $res .= $this->f(3, "jQuery.PeriodicalUpdater(");
        $res .= $this->f(4, "'{$url}',");
        $res .= $this->f(4, "{'minTimeout':{$time}, 'maxTimeout':{$time}, 'type':'json'},");
        $res .= $this->f(4, "function(data){");
        $res .= $this->f(5, "{$this->fc_add_all_markers}(data);");
        if ($callback) {
            $res .= $this->f(5, "{$callback}(data);");
        }
        $res .= $this->f(4, "}");
        $res .= $this->f(3, ");");
        $res .= $this->f(2, "});");

        return $res;
    }

    public function recargaJsonPolilineasManual($url, $callback = null) {
        $res = $this->f(2, "");
        $res .= $this->f(2, "jQuery.get( '{$url}', {}, function(data) {");
        $res .= $this->f(3, "{$this->fc_add_all_polylines}(data);");
        if ($callback) {
            $res .= $this->f(3, "{$callback}(data);");
        }
        $res .= $this->f(2, "}, 'json');");

        return $res;
    }

    public function recargaJsonPolilineasAutomatica($url, $time, $callback = null) {
       //  die('////<pre>' . nl2br(var_export('$idsRef', true)) . '</pre>////');
        $res = '';
        $res .= $this->f(2, "jQuery(document).ready(function(){");
        $res .= $this->f(3, "jQuery.PeriodicalUpdater(");
        $res .= $this->f(4, "'{$url}',");
        $res .= $this->f(4, "{'minTimeout':{$time}, 'maxTimeout':{$time}, 'type':'json'},");
        $res .= $this->f(4, "function(data){");
        $res .= $this->f(5, "{$this->fc_add_all_polylines}(data);");
        if ($callback) {
            $res .= $this->f(5, "{$callback}(data);");
        }
        $res .= $this->f(4, "}");
        $res .= $this->f(3, ");");
        $res .= $this->f(2, "});");

        return $res;
    }

}
