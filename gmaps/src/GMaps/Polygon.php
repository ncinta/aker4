<?php

namespace GMaps;

use GMaps\Util\HtmlHelper;
use GMaps\Util\GMHelper;

class Polygon {
    protected $name;
    protected $clickable = false;
    protected $editable = false;
    protected $geodesic = false;
    protected $strokeColor = '#000000';
    protected $strokeOpacity = 1.0;
    protected $strokeWeight = 1;
    protected $visible = true;
    protected $zIndex = null;
    protected $path = array();
    protected $fillColor = '#FFAA00';
    protected $fillOpacity = 0.7;

    public function __construct($name, $path = array()){
        $this->name = $name;
        $this->setPath($path);
    }
    
    public function setName($name){
        $this->name= $name;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setClickable($clickable){
        $this->clickable =  $clickable;
    }
    
    public function getClickable(){
        return $this->clickable;
    }
    
    public function setEditable($editable){
        $this->editable = $editable;
    }
    
    public function getEditable(){
        return $this->editable;
    }
    
    public function setStrokeColor($strokeColor){
        $this->strokeColor = $strokeColor;
    }
    
    public function getStrokeColor(){
        return $this->strokeColor;
    }
    
    public function setStrokeOpacity($strokeOpacity){
        if(is_numeric($strokeOpacity) && $strokeOpacity >= -0.00000001  && $strokeOpacity <= 1.00000001){
            $this->strokeOpacity = $strokeOpacity;
        } else {
            $this->strokeOpacity = 1.0;
        }
    }
    
    public function getStrokeOpacity(){
        return $this->strokeOpacity;
    }
    
    public function setStrokeWeight($strokeWeight){
        if(is_numeric($strokeWeight) && $strokeWeight >= 0){
            $this->strokeWeight = $strokeWeight;
        } else { 
            $this->strokeWeight = 1;
        }
    }
    
    public function getStrokeWeight(){
        return $this->strokeWeight;
    }
    
    public function setGeodesic($geodesic){
        $this->geodesic = $geodesic;
    }
    
    public function getGeodesic(){
        return $this->geodesic;
    }
    
    public function setFillColor($fillColor) {
        $this->fillColor = $fillColor;
    }
    
    public function getFillColor() {
        return $this->fillColor;
    }
    
    public function setFillOpacity($fillOpacity) {
        if(is_numeric($fillOpacity) && $fillOpacity >= -0.00000001 && $fillOpacity <= 1.00000001){
            $this->fillOpacity = $fillOpacity;
        } else { 
            $this->fillOpacity = 0.5;
        }
    }
    
    public function getFillOpacity() {
        return $this->fillOpacity;
    }
    
    public function setVisible($visible){
        $this->visible = $visible;
    }
    
    public function getVisible(){
        return $this->visible;
    }
    
    public function setZIndex($zIndex){
        $this->zIndex = $zIndex;
    }
    
    public function getZIndex(){
        return $this->zIndex;
    }
    
    public function setPath($path){
        if(is_array($path)){
            foreach($path as $point){
                $this->addPoint($point);
            }
        } else { 
            $this->path = array();
        }
    }
    
    public function getPath(){
        return $this->path;
    }
    
    public function addPoint($point){
        if(is_array($point) && count($point) > 1){
            $this->path[] = array($point[0], $point[1]);
        }
    }
    

    public function addPointLatLong($lat, $long){
        $this->addPoint(array($lat, $long));
    }
    
    //-----------------------------------------------------
    
    public function renderAll($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "var {$this->name} = new google.maps.Polygon(");
        $res .= $this->render($d, $t+1, $map);
        $res .= $this->f($d, $t, ");");
        
        return $res;
    }
    
    public function render($d, $t, $map){  //d:debug, t:tabs
        $res = '';
    
        $res .= $this->f($d, $t, "{");
        
        $res .= $this->f($d, $t+1, "id_sc: '{$this->name}',");
        $res .= $this->f($d, $t+1, "path: [".GMHelper::newPointPath($this->path)."],");
        if($map){
            $res .= $this->f($d, $t+1, "map: {$map},");
        }
        $res .= $this->f($d, $t+1, "editable: ".($this->editable?'true':'false').",");
        $res .= $this->f($d, $t+1, "geodesic: ".($this->geodesic?'true':'false').",");
        $res .= $this->f($d, $t+1, "visible: ".($this->visible?'true':'false').",");
        $res .= $this->f($d, $t+1, "clickable: ".($this->clickable?'true':'false')."");
        if($this->fillColor){
            $res .= $this->f($d, $t+1, ", fillColor: '{$this->fillColor}'");
        } 
        if($this->fillOpacity){
            $res .= $this->f($d, $t+1, ", fillOpacity: {$this->fillOpacity}");
        }
        if($this->strokeOpacity){
            $res .= $this->f($d, $t+1, ", strokeOpacity: {$this->strokeOpacity}");
        }
        if($this->strokeWeight){
            $res .= $this->f($d, $t+1, ", strokeWeight: {$this->strokeWeight}");
        }
        if($this->strokeColor){
            $res .= $this->f($d, $t+1, ", strokeColor: '{$this->strokeColor}'");
        }
        if($this->zIndex){ 
            $res .= $this->f($d, $t+1, ", zIndex: '{$this->zIndex}'");
        }
        
        $res .= $this->f($d, $t, "}");
        return $res;
    } 
      
    private function f($debug, $tabs, $contenido){
        return HtmlHelper::format($debug, $tabs, $contenido);
    }
}