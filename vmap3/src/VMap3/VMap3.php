<?php

namespace VMap3;

class VMap3 {

    private $mode = 'prod';
    private $motor_mapas = '190.151.141.49:9232';

    public function __construct($motor_mapas, $mode = null) {
        if ($mode != null) {
            $this->mode = $mode;
        }
        $this->motor_mapas = $motor_mapas;
        if (in_array(@$_SERVER['REMOTE_ADDR'], array(
                    '127.0.0.1',
                    '::1',
                ))) {
            //estoy en produccion
            //$this->motor_mapas = '192.168.28.80:9232';
            //$this->motor_mapas = '172.31.255.1:9232';
        }
    }

    private function vmap3_execute_request($request, $params) {
        $amp = false;
        foreach ($params as $k => $v) {
            if (!$amp) {
                $request .= "?";
                $amp = true;
            } else {
                $request .= "&";
            }
            $request .= $k . "=" . urlencode($v);
        }
        $response = file_get_contents("http://" . $this->motor_mapas . "/" . $request);
        return $response;
    }

    private function vmap3_parse_division_list($text) {
        if (!$text)
            return array();
        $text = explode("^", $text);
        $list = array();
        for ($i = 0; $i < count($text); $i += 2) {
            $list[] = array("id" => $text[$i], "name" => $text[$i + 1]);
        }
        return $list;
    }

    private function vmap3_km_check($number) {
        $number = trim($number);
        if (strtolower(substr($number, 0, 2)) == "km") {
            return trim(substr($number, 2)) * 1000.0;
        } else {
            return $number;
        }
    }

    private function vmap3_km_set($street, $number) {
        if (substr($street, 0, 5) == "Ruta ") {
            if ($number != '0') {
                return sprintf("km %.1f", $number / 1000.0);
            } else {
                return null;
            }
        } else {
            return $number;
        }
    }

    public function vmap3_search_address($address) {
        if (isset($address["number"])) {
            $address["number"] = $this->vmap3_km_check($address["number"]);
        }
        $result = $this->vmap3_execute_request("search_address", $address);
        if ($result == "*" && isset($address["latitude"]) && isset($address["longitude"]) && ($address["latitude"] || $address["longitude"])) {
//aca no encontre nada.
            return array(
                0 => array(
                    "street" => "",
                    "street_division" => array(),
                    "number" => 0,
                    "intersection" => "",
                    "intersection_division" => array(),
                    "latitude" => $address["latitude"],
                    "longitude" => $address["longitude"]
                )
            );
        } else if ($result == "*") {
            return false;
        }
        $result = explode("\n", $result);
        for ($i = 0; $i < count($result); $i++) {
            $result[$i] = explode("\t", $result[$i]);
            $result[$i] = array(
                "street" => $result[$i][0],
                "street_division" => $this->vmap3_parse_division_list($result[$i][1]),
                "number" => $this->vmap3_km_set($result[$i][0], $result[$i][2]),
                "intersection" => $result[$i][3],
                "intersection_division" => $this->vmap3_parse_division_list($result[$i][4]),
                "latitude" => $result[$i][5],
                "longitude" => $result[$i][6]
            );
        }
//die('////<pre>'.nl2br(var_export($result, true)).'</pre>////');
        return $result;
    }

    public function vmap3_list_divisions($parent_id = -1) {
        $result = $this->vmap3_execute_request("list_divisions", array("parent_id" => $parent_id));
        if ($result == "*")
            return false;
        $result = explode("\n", $result);
        for ($i = 0; $i < count($result); $i++) {
            $result[$i] = explode("\t", $result[$i]);
            $result[$i] = array(
                "id" => $result[$i][0],
                "parent_id" => $result[$i][1],
                "name" => $result[$i][2]
            );
        }
        return $result;
    }

    public function vmap3_division_list_to_text($list) {
        if ($list && count($list)) {
            $text = "";
            $lastname = "";
//for ($i = 0; $i < count($list) && $list[$i]["id"] % 1000000; $i++) {
            for ($i = 0; $i < count($list) - 1; $i++) {
                if ($list[$i]["name"] != $lastname) {
                    if ($i)
                        $text .= ", ";

                    //$text .= $list[$i]["name"];
                    //$text .= preg_replace('([^A-Za-z0-9_ ]áéíóúñÑ)', ' ', $list[$i]["name"]);
                    $text .= $this->limpiar($list[$i]["name"]);
                    $lastname = $list[$i]["name"];
                }
            }
//die('////<pre>'.nl2br(var_export($text, true)).'</pre>////');
            return $text;
        } else {
            return "";
        }
    }

    private function limpiar($string) {
        $string = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $string);
        $string = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $string);
        $string = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $string);
        $string = str_replace(array('í', 'ì', 'î', 'ï'), "i", $string);
        $string = str_replace(array('é', 'è', 'ê', 'ë'), "e", $string);
        $string = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $string);
        $string = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $string);
        $string = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $string);
        $string = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $string);
        $string = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $string);
        $string = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $string);
        $string = str_replace("ç", "c", $string);
        $string = str_replace("Ç", "C", $string);
        $string = str_replace("ñ", "n", $string);
        $string = str_replace("Ñ", "N", $string);
        $string = str_replace("Ý", "Y", $string);
        $string = str_replace("ý", "y", $string);
        return $string;
    }

}

?>