<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class PlanType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre', TextType::class, array(
                'required' => true,
                'label' => 'Nombre'
            ))
            ->add('intervaloDias', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Días'
            ))
            ->add('intervaloKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Kilómetros'
            ))
            ->add('intervaloHoras', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Horas'
            ))
            ->add('avisoDias', TextType::class, array(
                'required' => false,
                'label' => 'Días de anticipo'
            ))
            ->add('avisoKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Kilómetros de anticipo'
            ))
            ->add('avisoHoras', TextType::class, array(
                'required' => false,
                'label' => 'Horas de anticipo'
            ))
            ->add('porHoras', CheckboxType::class, array(
                'required' => false,
                'label' => 'Por hora'
            ))
            ->add('porDias', CheckboxType::class, array(
                'required' => false,
                'label' => 'Por Días'
            ))
            ->add('porKilometros', CheckboxType::class, array(
                'required' => false,
                'label' => 'Por Kilometros'
            ))
            //        ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Mantenimiento',
        ));
    }

    public function getBlockPrefix()
    {
        return 'mant';
    }
}
