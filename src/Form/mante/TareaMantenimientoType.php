<?php

namespace App\Form\mante;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TareaMantenimientoType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $mantenimientos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->mantenimientos = $options['mantenimientos'];
        if (count($this->servicios) > 1) {
            //armo el list de grupos/servicios
            $choiceS['Todos los equipos'] = 0;
            foreach ($this->grupos as $grupo) {
                $choiceS['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
                // $choiceS['g' . $grupo->getId()] = 'Grp: ' . $grupo->getNombre();
            }
        }
        foreach ($this->servicios as $servicio) {
            $choiceS[$servicio->getNombre()] = $servicio->getId();
        }

        //armo el list del mantenimiento
        $choiceM = array();
        $choiceM['Elija el Mantenimiento'] = 0;
        foreach ($this->mantenimientos as $mantenimiento) {
            $choiceM[$mantenimiento->getNombre()] = $mantenimiento->getId();
        }
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choiceS,
                'required' => true,
               
            ))
            ->add('mantenimiento', ChoiceType::class, array(
                'choices' => $choiceM,
                'required' => true,
                'attr' => array(
                    'onchange' => 'changeMantenimiento();'
                )
            ))
            ->add('intervaloDias', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Días',
            ))
            ->add('intervaloKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Kilómetros'
            ))
            ->add('intervaloHoras', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Horas'
            ))
            ->add('avisoDias', TextType::class, array(
                'required' => false,
                'label' => 'Días de anticipo'
            ))
            ->add('avisoKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Kilómetros de anticipo'
            ))
            ->add('avisoHoras', TextType::class, array(
                'required' => false,
                'label' => 'Horas de anticipo'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'servicios' => null,
            'grupos' => null,
            'mantenimientos' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'mant';
    }
}
