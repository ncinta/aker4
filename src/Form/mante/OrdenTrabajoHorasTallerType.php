<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class OrdenTrabajoHorasTallerType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('horaTaller', ChoiceType::class, array(
                'choices' => $this->getHorasTaller(),
                'multiple' => false,
                'required' => true,
                'label' => 'Descripción',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('cantidad', NumberType::class, array(
                'label' => 'Cantidad',
                'required' => false
            ))
            ->add('actualizar', CheckboxType::class, array(
                'label' => 'Actualizar precio',
                'required' => false
            ))
            ->add('unitario', NumberType::class, array(
                'required' => false,
                'label' => 'Precio Unitario'
            ));
    }

    private function getHorasTaller()
    {
        $results = $this->em->getRepository('App:HoraTaller')->byOrganizacion($this->organizacion);
        $choice = array();
        foreach ($results as $hora) {
            $choice[$hora->getNombre()] = $hora->getId();
        }
        return $choice;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'hora';
    }
}
