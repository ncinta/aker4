<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CentroCostoServicioRetirarType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->centro_id = $options['centro_id'];
        $this->em = $options['em'];

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $this->getServicios(),
                'multiple' => true,
                'required' => true,
                'label' => 'Servicios a retirar',
                'attr' => array('class' => 'col-md-12')
                
            ))
            ->add('motivo', TextareaType::class, array(
                'required' => true,
                'label' => 'Motivo'
            ))
            ->add('fin_prestacion', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha finalizacion'
                ),
                'required' => true,
                'label' => 'Fecha finalización',
            ))
            ;
           
    }

    private function getServicios()
    {
        $results = $this->em->getRepository('App:Servicio')->findBy(array('centrocosto'=> $this->centro_id ));
        $choice  = array();
        foreach ($results as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        //ksort($choice);
        return $choice;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'em' => null,
            'centro_id' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
