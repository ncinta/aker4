<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductoType extends AbstractType
{

    private $organizacion;
    private $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];
        $builder
            ->add('nombre', TextType::class, array(
                'required' => true,
                'label' => 'Nombre'
            ))
            ->add('marca', TextType::class, array(
                'required' => false,
                'label' => 'Marca'
            ))
            ->add('modelo', TextType::class, array(
                'required' => false,
                'label' => 'Modelo'
            ))
            ->add('codigo', TextType::class, array(
                'required' => false,
                'label' => 'Código Interno'
            ))
            ->add('codigoBarra', TextType::class, array(
                'required' => false,
                'label' => 'Código de Barra'
            ))
            ->add('tasaIva', NumberType::class, array(
                'label' => 'IVA',
                'required' => false,
            ))
            ->add('rubro', ChoiceType::class, array(
                'choices' => $this->getRubros(),
                'multiple' => false,
                'required' => false,
                'label' => 'Rubro',
                'attr' => array('class' => 'js-example-basic-single')
            ));
    }

    private function getRubros()
    {
        $results = $this->em->getRepository('App:Rubro')
            ->findByOrg($this->organizacion, array('d.nombre' => 'ASC'));
        $rubro = array();
        foreach ($results as $dep) {
            $rubro[$dep->getNombre()] = $dep->getId();
        }
        return $rubro;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Producto',
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'producto';
    }
}
