<?php

/*
 */

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TareaMantenimientoListType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $centrocostos;
    protected $moduloCentroCosto;
    protected $estado;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->moduloCentroCosto = $options['moduloCentroCosto'];
        $this->centrocostos = $options['centrocostos'];
        $this->estado = $options['estado'];
        $choiceS = array();
        if (count($this->servicios) > 1) {
            //armo el list de grupos/servicios
            $choiceS[''] = -1;
            foreach ($this->grupos as $grupo) {
                $choiceS['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
                // $choiceS['g' . $grupo->getId()] = 'Grp: ' . $grupo->getNombre();
            }
        }
        foreach ($this->servicios as $servicio) {
            $choiceS[$servicio->getNombre()] = $servicio->getId();
        }

        if ($this->moduloCentroCosto) {
            //armo el list del mantenimiento
            $choiceCC = array();
            $choiceCC[''] = -1;
            foreach ($this->centrocostos as $cc) {
                $choiceCC[$cc->getNombre()] = $cc->getId();
            }
        }
        $choiceEstado = array(
            'Todos' => 0,
            'Vencidos' => 3,
            'Por Vencer' => 2
        );

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choiceS,
                'required' => true,
                
            ));
        if ($this->moduloCentroCosto) {
            $builder->add('centrocosto', ChoiceType::class, array(
                'choices' => $choiceCC,
                'required' => true,
          
            ));
        }
        $builder->add('estado', ChoiceType::class, array(
            'choices' => $choiceEstado,
            'required' => true,
            'data' => $this->estado,
           
        ));;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'servicios' => null,
            'estado' => null,
            'grupos' => null,
            'moduloCentroCosto' => null,
            'centrocostos' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'mant';
    }
}
