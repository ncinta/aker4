<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OrdenTrabajoTareaType extends AbstractType
{

    protected $organizacion = null;
    protected $em = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('fecha', TextType::class, array(
                'required' => false,
                'label' => 'Fecha de Inicio',
            ))
            ->add('descripcion', TextareaType::class, array(
                'required' => false,
                'label' => 'Descripción'
            ))
            ->add('mecanico', EntityType::class, array(
                'label' => 'Mecanicos',
                'class' => 'App:Mecanico',
                'required' => false,
                'multiple' => true,
                'query_builder' => $this->em->getRepository('App:Mecanico')->getQueryByOrganizacion($this->organizacion),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\TareaOt',
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'tarea';
    }
}
