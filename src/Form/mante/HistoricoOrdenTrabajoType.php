<?php

namespace App\Form\mante;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of HistoricoOrdeTrabajoType
 *
 * @author nicolas
 */
class HistoricoOrdenTrabajoType extends AbstractType
{

    protected $servicios = null;
    protected $grupos = null;
    protected $sectores = null;
    protected $depositos = null;
    protected $centros = null;
    protected $talleres = null;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choice_deposito = array();
        $choice_talleres = array();
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->sectores = $options['sectores'];
        $this->depositos = $options['depositos'];
        $this->centros = $options['centrocosto'];
        $this->talleres = $options['talleres'];


        if (count($this->servicios) > 1) {
            //armo el list de grupos/servicios
            $choice_servicio[0] = ('Todos los equipos');
            foreach ($this->grupos as $grupo) {
                $choice_servicio['g' . $grupo->getId()] = 'Grp: ' . $grupo->getNombre();
            }
        }
        $choice_servicio[0] = ('Todos');
        foreach ($this->servicios as $servicio) {
            $choice_servicio[$servicio->getId()] = $servicio->getNombre();
        }

        $choice_centro[0] = ('Todos');
        foreach ($this->centros as $centro) {
            $choice_centro[$centro->getId()] = $centro->getNombre();
        }
        // die('////<pre>' . nl2br(var_export($options['centrocosto'], true)) . '</pre>////');

        $choice_sector = array();
        $choice_sector['Todos'] = 0;
        foreach ($this->talleres as $taller) {
            foreach ($taller->getSectores() as $sector) {
                $choice_sector[$taller->getNombre()][$sector->getNombre()] = $sector->getId();
                // $choice_sector[$taller->getNombre()][$sector->getId()] = $sector->getNombre();
            }
        }
        // die('////<pre>' . nl2br(var_export($choice_sector, true)) . '</pre>////');

        if (count($this->depositos) > 0) {
            $choice_deposito['Todos'] = 0;
            foreach ($this->depositos as $deposito) {
                $choice_deposito[$deposito->getNombre()] = $deposito->getId();
            }
        }
        if (count($this->talleres) > 0) {
            $choice_talleres['Todos'] = 0;
            foreach ($this->talleres as $taller) {
                $choice_talleres[$taller->getNombre()] = $taller->getId();
            }
        }

        $ch_tipo = array(-1 => 'Todos', 0 => 'Correctivo', 1 => 'Preventivo');
        $builder
            ->add('fecha_desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('fecha_hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('tipo', ChoiceType::class, array(
                'choices' => array_flip($ch_tipo),
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => array_flip($choice_servicio),
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('taller', ChoiceType::class, array(
                'choices' => $choice_talleres,
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('sector', ChoiceType::class, array(
                'choices' => $choice_sector,
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('deposito', ChoiceType::class, array(
                'choices' => $choice_deposito,
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('centrocosto', ChoiceType::class, array(
                'choices' => array_flip($choice_centro),
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'servicios' => null,
            'grupos' => null,
            'talleres' => null,
            'depositos' => null,
            'centrocosto' => null,
            'sectores' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historico';
    }
}
