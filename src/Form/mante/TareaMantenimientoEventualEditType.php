<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TareaMantenimientoEventualEditType extends AbstractType
{

    private $talleres;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->talleres = $options['talleres'];
        $choice = array();
        foreach ($this->talleres as $taller) {
            $choice[$taller->getId()] = $taller->getNombre();
        }
        $builder
            ->add('observacion', TextareaType::class, array(
                'required' => true,
                'label' => 'Intervención realizada',
            ))
            ->add('costo', NumberType::class, array(
                'required' => false,
                'label' => 'Costo ($)'
            ))
            ->add('fecha', TextType::class, array(
                'attr' => array('class' => 'calendario'),
                'required' => true,
                'label' => 'Fecha'
            ))
            ->add('taller', EntityType::class, array(
                'class' => 'App:Taller',
                'choices' => $this->talleres,
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ServicioMantenimientoHistorico',
            'talleres' => null,
        ));
    }

    public function getName()
    {
        return 'mant';
    }
}
