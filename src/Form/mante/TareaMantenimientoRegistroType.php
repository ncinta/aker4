<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Form\app\ActividadHistoricoType;

class TareaMantenimientoRegistroType extends AbstractType
{

    protected $odometro;
    protected $horometro;
    protected $porHoras;
    protected $porDias;
    protected $porKilometros;
    protected $talleres;
    protected $actividades;
    protected $depositos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->odometro = $options['odometro'];
        $this->horometro = $options['horometro'];
        $this->porHoras = $options['porHoras'];
        $this->porDias = $options['porDias'];
        $this->porKilometros = $options['porKilometros'];
        $this->depositos = $options['depositos'];
        $this->sectores = $options['sectores'];
        $this->centroscosto = $options['centroscosto'];

        $builder
            ->add('observacion', TextareaType::class, array(
                'required' => true,
                'label' => 'Descripción',
            ))
            ->add('costo', NumberType::class, array(
                'required' => false,
                'label' => 'Costo ($)'
            ))
            ->add('fecha', TextType::class, array(
                'required' => true,
                'label' => 'Fecha Actual',
            ));
        if ($this->porKilometros) {
            //  die('////<pre>' . nl2br(var_export('asdasd', true)) . '</pre>////');
            $builder->add('dato_realizacion', NumberType::class, array(
                'required' => false,
                'label' => 'Realizado a los (kms)',
                'attr' => array(
                    'value' => $this->odometro,
                    'onchange' => 'calcular_proximo()'
                )
            ));
            $builder->add('dato_proximo', NumberType::class, array(
                'required' => false,
                'label' => 'Próximo (kms)',
            ));
        } elseif ($this->porHoras) {
            $builder->add('dato_realizacion', NumberType::class, array(
                'required' => false,
                'label' => 'Realizado a las (hs)',
                'attr' => array(
                    'value' => $this->horometro,
                    'onchange' => 'calcular_proximo()'
                )
            ));
            $builder->add('dato_proximo', NumberType::class, array(
                'required' => false,
                'label' => 'Próximas (hs)',
            ));
        } elseif ($this->porDias) {
            $builder->add('dato_realizacion_fecha', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario'
                ),
                'required' => true,
                'label' => 'Realizado el ',
                //'attr' => array(
                //    'onchange' => 'calcular_proxima_fecha()')
            ));
            $builder->add('dato_proxima_fecha', TextType::class, array(
                'disabled' => true,
                'attr' => array(
                    'class' => 'calendario'
                ),
                'required' => false,
                'label' => 'Próxima fecha el ',
            ));
        }
        if ($options['talleres']) {
            $choice = array();
            foreach ($options['talleres'] as $taller) {
                $choice[$taller->getNombre()] = $taller->getId();
            }
            $builder->add(
                'taller',
                ChoiceType::class,
                array(
                    'choices' => $choice,
                    'required' => true,
                )
            );
        }
        // die('////<pre>' . nl2br(var_export(count($options['depositos']), true)) . '</pre>////');
        if ($options['depositos']) {
            $choice = array();
            foreach ($options['depositos'] as $deposito) {
                $choice[$deposito->getNombre()] = $deposito->getId();
            }
            $builder->add(
                'deposito',
                ChoiceType::class,
                array(
                    'choices' => $choice,
                    'required' => true,
                )
            );
        }
        if ($options['sectores']) {
            $choice = array();
            foreach ($options['sectores'] as $sector) {
                $choice[$sector->getNombre()] = $sector->getId();
            }
            $builder->add(
                'sector',
                ChoiceType::class,
                array(
                    'choices' => $choice,
                    'required' => true,
                )
            );
        }
        if ($options['centroscosto']) {
            $choice = array();
            foreach ($options['centroscosto'] as $centro) {
                $choice[$centro->getNombre()] = $centro->getId();
            }
            $builder->add(
                'centrocosto',
                ChoiceType::class,
                array(
                    'choices' => $choice,
                    'required' => false,
                )
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'odometro' => null,
            'horometro' => null,
            'porHoras' => null,
            'porDias' => null,
            'porKilometros' => null,
            'talleres' => null,
            'depositos' => null,
            'sectores' => null,
            'centroscosto' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'mant';
    }
}
