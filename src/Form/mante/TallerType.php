<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * Description of TallerType
 *
 * @author nicolas
 */
class TallerType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Teléfono',
                'required' => false,
            ))
            ->add('direccion', TextType::class, array(
                'label' => 'Dirección',
                'required' => false,
            ))
            ->add('rubro', TextType::class, array(
                'label' => 'Rubro',
                'required' => false,
            ))
            ->add('horario', TextType::class, array(
                'label' => 'Horario de atención',
                'required' => false,
            ))
            ->add('email', TextType::class, array(
                'label' => 'Email',
                'required' => false,
            ))
            ->add('nota', TextareaType::class, array(
                'label' => 'Nota',
                'required' => false,
            ))
            ->add('propio', CheckboxType::class, array(
                'label' => 'Es propio?',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Taller',
        ));
    }

    public function getBlockPrefix()
    {
        return 'taller';
    }
}
