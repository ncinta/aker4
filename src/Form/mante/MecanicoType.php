<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MecanicoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choice = array();
        foreach ($builder->getData()->getArrayTipoMecanico() as $key => $value) {
            $choice[$value] = $key;
        }
        //  die('////<pre>' . nl2br(var_export($choice, true)) . '</pre>////');
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('documento', TextType::class, array(
                'label' => 'Documento',
                'required' => false,
            ))
            ->add('seguro', TextType::class, array(
                'label' => 'Seguro',
                'required' => false,
            ))
            ->add('telefonoParticular', TextType::class, array(
                'label' => 'Tel. Particular',
                'required' => false,
            ))
            ->add('telefonoContacto', TextType::class, array(
                'label' => 'Tel. Contacto',
                'required' => false,
            ))
            ->add('tipo', ChoiceType::class, array(
                'label' => 'Tipo de Mecánico',
                'choices' => $choice,
                'required' => true,
                'multiple' => false
            ))
            ->add('costoHora', NumberType::class, array(
                'label' => 'Costo Hora',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Mecanico',
        ));
    }

    public function getBlockPrefix()
    {
        return 'mecanico';
    }
}
