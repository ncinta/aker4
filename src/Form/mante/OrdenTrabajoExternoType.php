<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrdenTrabajoExternoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('tallerExterno', ChoiceType::class, array(
                'choices' => $this->getTallerExterno(),
                'multiple' => false,
                'required' => true,
                'label' => 'Taller',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('producto', ChoiceType::class, array(
                'choices' => $this->getProducto(),
                'multiple' => false,
                'required' => true,
                'label' => 'Producto',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('cantidad', NumberType::class, array(
                'label' => 'Cantidad',
                'required' => false
            ))
            ->add('descripcion', TextType::class, array(
                'label' => 'Descripción',
                'required' => false
            ))
            ->add('costoTotal', NumberType::class, array(
                'required' => false,
                'label' => 'Costo Total'
            ));
    }

    private function getTallerExterno()
    {
        $results = $this->em->getRepository('App:TallerExterno')->findByOrg($this->organizacion);
        $choice = array();
        foreach ($results as $taller) {
            $choice[$taller->getNombre()] = $taller->getId();
        }
        return $choice;
    }

    private function getProducto()
    {
        $results = $this->em->getRepository('App:Producto')->byOrganizacion($this->organizacion);
        $choice = array();
        $choice[""] = 0;
        foreach ($results as $producto) {
            $choice[$producto->getNombre()] = $producto->getId();
        }
        return $choice;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'externo';
    }
}
