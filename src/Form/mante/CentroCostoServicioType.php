<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CentroCostoServicioType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->centro_id = $options['centro_id'];
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $this->getServicios(),
                'multiple' => true,
                'required' => true,
                'label' => 'Servicio',
                'attr' => array('class' => 'col-md-12')
            ))
            ->add('inicio_prestacion', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicio'
                ),
                'required' => false,
                'label' => 'Desde',
            ))
            ->add('horasUso', NumberType::class, array(
                'required' => false,
                'label' => 'Horas de uso c/24',
            ));
    }

    private function getServicios()
    {
        $results = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($this->organizacion);
        $choice = $choice2 = array();
        foreach ($results as $servicio) {
            if ($servicio->getCentroCosto() != null) {
                if ($this->centro_id != $servicio->getCentroCosto()->getId()) {
                    $choice[$servicio->getCentroCosto()->getNombre()][$servicio->getNombre()] = $servicio->getId();
                }
            } else {
                $choice2[$servicio->getNombre()] = $servicio->getId();
            }
            //            if ($servicio->getCentroCosto() != null) {
            //                $choice[$servicio->getId()] = sprintf('%s (en %s)', $servicio->getNombre(), $servicio->getCentroCosto()->getNombre());
            //            } else {
            //                $choice[$servicio->getId()] = $servicio->getNombre();
            //            }
        }
        $choice['Sin Asignar'] = $choice2;
        //ksort($choice);
        return $choice;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
            'centro_id' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
