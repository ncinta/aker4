<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Producto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Description of VehiculoFichaTecnicaType
 *
 * @author nicolas
 */
class VehiculoFichaTecnicaType extends AbstractType
{

    protected $em;
    protected $organizacion;

    //    function __construct($options) {
    //        $this->organizacion = $options['organizacion'];
    //        $this->em = $options['em'];
    //    }
    //    

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // die('////<pre>' . nl2br(var_export($options['organizacion']->getId(), true)) . '</pre>////');
        $this->em = $options['em'];
        $this->organizacion = $options['organizacion'];
        $builder
            ->add('producto', ChoiceType::class, array(
                'choices' => $this->getProductos(),
                'multiple' => false,
                'label' => false,
                'required' => true
            ))
            ->add('actividad', ChoiceType::class, array(
                'choices' => $this->getActividades(),
                'multiple' => false,
                'label' => 'Actividad donde se usa',
                'required' => true
            ))
            ->add('cantidad', TextType::class, array('required' => true, 'label' => false,))
            ->add('frecuenciaCambio', TextType::class, array('required' => true, 'label' => false,))
            ->add('alternativo', TextType::class, array('required' => true, 'label' => false,));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\VehiculoFichaTecnica',
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'vehiculofichatecnica';
    }

    private function getProductos()
    {
        $arr = array();
        $productos = $this->em->getRepository('App:Producto')->byOrganizacion($this->organizacion);
        foreach ($productos as $producto) {
            $arr[$producto->getNombre()] = $producto->getId();
        }
        return $arr;
    }

    private function getActividades()
    {
        $arr = array();
        $actividades = $this->em->getRepository('App:Actividad')->byOrganizacion($this->organizacion);
        foreach ($actividades as $act) {
            $str = $act->getMantenimiento()->getNombre() . ' | ' . $act->getNombre();
            $arr[$str] = $act->getId();
        }
        return $arr;
    }
}
