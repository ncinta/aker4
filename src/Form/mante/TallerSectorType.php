<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TallerSectorType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array('required' => true))
            ->add('telefono', TextType::class, array('required' => false))
            ->add('rubro', TextType::class, array('required' => false))
            ->add('horario', TextType::class, array('required' => false))
            ->add('descripcion', TextType::class, array('required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\TallerSector',

        ));
    }

    public function getBlockPrefix()
    {
        return 'sector';
    }
}
