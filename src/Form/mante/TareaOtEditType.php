<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TareaOtEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('fecha', TextType::class, array(
                'required' => false,
                'label' => 'Fecha de Inicio',
            ))
            ->add('descripcion', TextareaType::class, array(
                'required' => false,
                'label' => 'Descripción'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            // 'data_class' => 'App\Entity\TareaOt',
        ));
    }

    public function getBlockPrefix()
    {
        return 'tarea_edit';
    }
}
