<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Entity\Prestacion as Prestacion;

class CentroCostoServicioEditType extends AbstractType
{

    protected $prestacion = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->prestacion = $options['prestacion'];
        $this->em = $options['em'];

        $builder
            ->add('inicio_prestacion', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicio'
                ),
                'required' => false,
                'label' => 'Desde',
                'data' => $this->prestacion->getInicioPrestacion()
            ))
            ->add('horasUso', NumberType::class, array(
                'required' => false,
                'label' => 'Horas de uso c/24',
                'data' => $this->prestacion->getHorasUso()
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'prestacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'prestacion';
    }
}
