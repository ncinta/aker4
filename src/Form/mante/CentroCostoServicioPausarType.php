<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CentroCostoServicioPausarType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('motivo', TextareaType::class, array(
                'required' => true,
                'label' => 'Motivo'
            ))
            ->add('inicio_pausa', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Hora de inicio de fuera de servicio'
                ),
                'required' => true,
                'label' => 'Inicio de parada',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
