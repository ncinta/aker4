<?php


namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


/**
 * Description of InformeMecanicoType
 *
 * @author nicolas
 */
class InformeMecanicoType extends AbstractType
{
    protected $mecanicos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->mecanicos = $options['mecanicos'];
        $choice['Todos'] = 0;

        foreach ($this->mecanicos as $mecanico) {
            $choice[$mecanico->getNombre()] = $mecanico->getId();
        }

        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    //                        'Excel' => '5',
                    //'6' => 'CSV',
                ),
                'required' => true,
            ))
            ->add('mecanico', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mecanicos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informe';
    }
}
