<?php

/*
 */

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TareaMantenimientoEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('intervaloDias', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Días',
            ))
            ->add('intervaloKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Kilómetros'
            ))
            ->add('intervaloHoras', TextType::class, array(
                'required' => false,
                'label' => 'Intervalo Horas'
            ))
            ->add('avisoDias', TextType::class, array(
                'required' => false,
                'label' => 'Días de anticipo'
            ))
            ->add('avisoKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Kilómetros de anticipo'
            ))
            ->add('avisoHoras', TextType::class, array(
                'required' => false,
                'label' => 'Horas de anticipo'
            ))
            ->add('proximoDias', DateType::class, array(
                'required' => false,
                //'disabled' => true,
                'label' => 'Próxima fecha',
                'widget' => 'single_text',
            ))
            ->add('proximoKilometros', TextType::class, array(
                'required' => false,
                'label' => 'Próximo Odómetro'
            ))
            ->add('proximoHoras', TextType::class, array(
                'required' => false,
                'label' => 'Próximo Horómetro'
            ))
            //        ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ServicioMantenimiento',
        ));
    }

    public function getName()
    {
        return 'mant';
    }
}
