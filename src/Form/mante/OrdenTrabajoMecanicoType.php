<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class OrdenTrabajoMecanicoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('mecanico', ChoiceType::class, array(
                'choices' => $this->getMecanicos(),
                'multiple' => false,
                'required' => true,
                'label' => 'Mecánico',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('cantidad', NumberType::class, array(
                'label' => 'Cantidad Horas',
                'required' => false
            ))
     
        ;
    }

    private function getMecanicos()
    {
        $results = $this->em->getRepository('App:Mecanico')->findByOrganizacion($this->organizacion);
        $choice = array();
        foreach ($results as $mecanico) {
            $choice[$mecanico->getNombre()] = $mecanico->getId();
        }
        //die('////<pre>' . nl2br(var_export($choice, true)) . '</pre>////');
        return $choice;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'mecanico';
    }
}
