<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class OrdenTrabajoType extends AbstractType
{

    private $organizacion;
    private $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        if (isset($options['servicios'])) {
            $servicios = array();
            foreach ($options['servicios'] as $servicio) {
                $servicios[$servicio->getNombre()] = $servicio->getId();
            }
            $builder->add('servicio', ChoiceType::class, array(
                'choices' => $servicios,
                'multiple' => false,
                'required' => true,
                'label' => 'Servicio',
                'attr' => array('class' => 'js-example-basic-single')
            ));
        }
        $builder
            ->add('centroCosto', ChoiceType::class, array(
                'choices' => $this->getCentroCosto(),
                'multiple' => false,
                'required' => false,
                'label' => 'Centro de Costo',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('deposito', ChoiceType::class, array(
                'choices' => $this->getDepositos(),
                'multiple' => false,
                'required' => true,
                'label' => 'Depósito',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('taller', ChoiceType::class, array(
                'choices' => $this->getTalleres(),
                'multiple' => false,
                'required' => true,
                'label' => 'Taller',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('tallerSector', ChoiceType::class, array(
                'choices' => $this->getSectorTalleres(),
                'multiple' => false,
                'required' => true,
                'label' => 'Sector del Taller',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('descripcion', TextType::class, array(
                'label' => 'Descripcion',
                'required' => true,
                'attr' => array(
                    'maxlength' => 255,
                    //'onKeyPress' => "cuenta()",
                    'onKeyUp' => "cuenta()",
                ),
            ))
            ->add('nota', TextareaType::class, array(
                'label' => 'Nota',
                'required' => false
            ))
            ->add('fecha', DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
            ))
            ->add('fechaIngreso', DateTimeType::class, array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
            ))
            ->add('fechaEgreso', DateTimeType::class, array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
            ))
            ->add('odometro', IntegerType::class, array(
                'label' => 'Odómetro',
                'required' => false,
            ))
            ->add('horometro', IntegerType::class, array(
                'label' => 'Horómetro',
                'required' => false,
            ))
            ->add('tipo', ChoiceType::class, array(
                'label' => 'Tipo',
                'required' => true,
                'choices' => array_flip(array(0 => 'Correctivo', 1 => 'Preventivo', 2 => 'Retrabajo'))
            ))
            ->add('numeroExterno', TextType::class, array(
                'label' => 'Nro. Externo',
                'required' => false,
            ))
            ->add('numeroInterno', TextType::class, array(
                'label' => 'Nro. Interno',
                'required' => false,
            ));
    }

    private function getCentroCosto()
    {
        $results = $this->em->getRepository('App:CentroCosto')->findByOrg($this->organizacion);
        $centro = array();
        foreach ($results as $centrocosto) {
            $centro[$centrocosto->getNombre()] = $centrocosto->getId();
        }
        // die('////<pre>'.nl2br(var_export($centro, true)).'</pre>////');
        return $centro;
    }

    private function getSectorTalleres()
    {
        $results = $this->em->getRepository('App:TallerSector')->findByOrg($this->organizacion);
        $sectoresTaller = array();
        foreach ($results as $sector) {
            if ($sector->getTaller()) {
                $sectoresTaller[$sector->getTaller()->getNombre()][$sector->getNombre()] = $sector->getId();
            }
        }
        //die('////<pre>' . nl2br(var_export($sectoresTaller, true)) . '</pre>////');
        return $sectoresTaller;
    }

    private function getTalleres()
    {
        $results = $this->em->getRepository('App:Taller')->findByOrg($this->organizacion);
        $talleres = array();
        foreach ($results as $taller) {
            $talleres[$taller->getNombre()] = $taller->getId();
        }
        return $talleres;
    }

    private function getDepositos()
    {
        $results = $this->em->getRepository('App:Deposito')
            ->byOrganizacion($this->organizacion, array('d.nombre' => 'ASC'));
        $depos = array();
        foreach ($results as $dep) {
            $depos[$dep->getNombre()] = $dep->getId();
        }
        return $depos;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\OrdenTrabajo',
            'servicios' => null,
            'talleres' => null,
            'sectoresTaller' => null,
            'depositos' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'orden';
    }
}
