<?php

namespace App\Form\mante;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Form\Type\ChoiceType;


/**
 * Description of StockType
 *
 * @author nicolas
 */
class StockType extends AbstractType
{
    protected $organizacion = null;
    protected $producto = null;
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->producto = $options['producto'];

        $builder
            ->add('ult_compra', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Fecha de Compra'
                ),
                'required' => true,
                'label' => 'Fecha de Compra'
            ))
            ->add('stock', TextType::class, array(
                'label' => 'Cantidad a ingresar',
                'required' => true
            ))
            ->add('ult_proveedor', TextType::class, array(
                'label' => 'Proveedor',
                'required' => false
            ))
            ->add('costo', TextType::class, array(
                'label' => 'Precio unitario',
                'required' => false
            ))
            ->add('tasaIva', NumberType::class, array(
                'label' => 'IVA',
                'required' => false,
            ))
            ->add('deposito', EntityType::class, array(
                'required' => true,
                'label' => 'Deposito',
                'class' => 'App:Deposito',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->where('d.organizacion = :organizacion')
                        ->addOrderBy('d.nombre', 'ASC')
                        ->setParameter('organizacion', $this->organizacion);
                }
            ));
        if ($this->producto != null) {
            $builder->add('producto', EntityType::class, array(
                'required' => true,
                'label' => 'Producto',
                'class' => 'App:Producto',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.id = :producto')
                        ->addOrderBy('p.nombre', 'ASC')
                        ->setParameter('producto', $this->producto->getId());
                }
            ));
        } else {
            $builder->add('producto', EntityType::class, array(
                'required' => true,
                'label' => 'Producto',
                'class' => 'App:Producto',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->join('p.organizacion', 'o')
                        ->where('o.id = :organizacion')
                        ->addOrderBy('p.nombre', 'ASC')
                        ->setParameter('organizacion', $this->organizacion);
                }
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Stock',
            'organizacion' => null,
            'producto' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'stock';
    }
}
