<?php

namespace App\Form\monitor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransporteType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('domicilio')
            ->add('localidad')
            ->add('contacto')
            ->add('telefono')
            ->add('email')
            ->add('nota');
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Transporte',
        ));
    }

    public function getBlockPrefix()
    {
        return 'transporte';
    }
}
