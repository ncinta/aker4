<?php


namespace App\Form\monitor\import;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of ReferenciaType
 *
 * @author yesica
 */
class ReferenciaType extends AbstractType
{

    protected $organizacion;
    protected $em;
    protected $provincias;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];
        $this->provincias = $options['provincias'];
        $choiceProv = array();
        foreach ($this->provincias as $provincia) {
            $choiceProv[$provincia->getNombre()] = $provincia->getCode();
        }

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre de referencia',
                'required' => true,
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripcion',
                'required' => false,
            ))
            ->add('pathIcono', HiddenType::class, array(
                'required' => false,
            ))
            //->add('poligono', 'hidden')
            ->add('direccion', TextType::class, array(
                'label' => 'Direccion',
                'required' => false,
            ))
            ->add('latitud', NumberType::class, array(
                'label' => 'Latitud',
                'required' => false,
            ))
            ->add('longitud', NumberType::class, array(
                'label' => 'Longitud',
                'required' => false,
            ))
            ->add('radio', IntegerType::class, array(
                'label' => 'Longitud de radio (mts.)',
                'required' => false,
            ))
            ->add('codigoExterno', TextType::class, array(
                'label' => 'Identificador',
                'required' => false,
            ))
            ->add('ref_provincia', ChoiceType::class, array(
                'choices' => $choiceProv,
                'required' => false,
                'label' => 'Provincia',
                'multiple' => false
            ))
            ->add('ref_ciudad', TextType::class, array(
                'label' => 'Ciudad',
                'required' => false,
            ));

     
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Referencia',
            'organizacion' => null,
            'provincias' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
