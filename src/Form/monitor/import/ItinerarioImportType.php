<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\monitor\import;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class ItinerarioImportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('archivo', FileType::class, array(
                'required' => true,
                'label' => 'Archivo Excel',
            ))
            ->add('tipo', ChoiceType::class, array(
                'required' => true,
                'label' => 'Tipo de Archivo',
                'choices'=>[
                    'SADI' => 1,
                    'CH ROBINSON' => 2,
                    'P44' => 3,
                    'TEKNAL' => 4
                ]
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getBlockPrefix()
    {
        return 'import';
    }
}
