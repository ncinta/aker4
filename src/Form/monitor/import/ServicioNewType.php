<?php

namespace App\Form\monitor\import;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Transporte;
use App\Entity\Satelital;

class ServicioNewType extends AbstractType
{
  //formulario para el import de los intinerarios
    protected $em;
    protected $organizacion;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];
        $this->organizacion = $options['organizacion'];

        $builder
            ->add('nombre')
            ->add('color', TextType::class, array(
                'label' => 'Color',
                'required' => false,
            ))
            ->add('transporte', EntityType::class, array(
                'class' => Transporte::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->join('t.organizacion', 'o')
                        ->where('o.id = ' . $this->organizacion->getId())
                        ->orderBy('o.nombre', 'ASC');
                }
            ))
            ->add('satelital', EntityType::class, array(
                'class' => Satelital::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->join('s.organizacion', 'o')
                        ->where('o.id = ' . $this->organizacion->getId())
                        ->orderBy('s.nombre', 'ASC');
                }
            ));

        $builder->add('patente', TextType::class, array(
            'required' => false,
            'label' => 'Patente',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'em' => null,
            'organizacion' => null,

        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
