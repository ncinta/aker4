<?php

namespace App\Form\monitor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Empresa;
use App\Entity\Logistica;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ItinerarioType extends AbstractType
{

    protected $organizacion;
    protected $em;
    protected $userlogin;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->empresas = $options['empresas'];
        $this->userlogin = $options['userlogin'];
        $this->em = $options['em'];
        $builder
            ->add('codigo', TextType::class, array(
                'required' => true,
                'label' => 'Código'
            ))
            ->add('fechaInicio', TextType::class, array(
                'required' => true,
                'label' => 'Inicio'
            ))
            ->add('fechaFin', TextType::class, array(
                'required' => true,
                'label' => 'Fin'
            ))
            ->add('nota', TextareaType::class, array(
                'required' => false,
                'label' => 'Nota'
            ))
            ->add('empresa', EntityType::class, array(
                'label' => 'Cliente',
                'class' => Empresa::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    if (!is_null($this->userlogin->getEmpresa())) {   //es un usuario de un cliente
                        return $er->createQueryBuilder('e')
                            ->where('e.id = ?1')
                            ->setParameter('1', $this->userlogin->getEmpresa()->getId());
                    } else {
                        return $er->createQueryBuilder('e')
                            ->join('e.organizacion', 'o')
                            ->where('o.id = ?1')
                            ->orderBy('e.nombre', 'ASC')
                            ->setParameter('1', $this->userlogin->getOrganizacion()->getId());
                    }
                    return $query->getQuery()->getResult();
                },
                
            ))
            ->add('nota', TextareaType::class, array(
                'required' => false,
                'label' => 'Nota'
            ))

            ->add('logistica', EntityType::class, array(
                'label' => 'Logistica',
                'required' => false,
                'class' => Logistica::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.organizacion', 'o')
                        ->where('o.id = ' . $this->organizacion->getId())
                        ->orderBy('o.nombre', 'ASC');
                },
            ));
    }

    private function getEmpresas()
    {
        $results = $this->em->getRepository('App:Empresa')->getQueryEmpresas($this->userlogin);
        $empresas = array();
        foreach ($results as $empresa) {
            //$empresas[$empresa->getNombre()] = $empresa->getId();
            $empresas[$empresa->getId()] = $empresa->getNombre();
        }
        return $empresas;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Itinerario',
            'organizacion' => null,
            'empresas' => null,
            'userlogin' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'itinerario';
    }
}
