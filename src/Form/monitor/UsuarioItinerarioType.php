<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\monitor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UsuarioItinerarioType extends AbstractType
{

    protected $usuarios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->usuarios = $options['usuarios'];
        $builder
            ->add('usuario', ChoiceType::class, array(
                'choices' => $this->usuarios,
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                'choice_value' => function ($value) {
                    return $value;
                },
                'choice_label' => function ($value) {
                    return $value;
                },
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'usuarios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'usuario';
    }
}
