<?php

namespace App\Form\monitor;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of HistoricoItinerarioType
 *
 * @author nicolas
 */
class SeguimientoType extends AbstractType
{

    protected $referencias = null;
    protected $empresas = null;
    protected $transportes = null;
    protected $satelitales = null;
    protected $itinerarios = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->empresas = $options['empresas'];
        $this->transportes = $options['transportes'];
        $this->satelitales = $options['satelitales'];
        $this->itinerarios = $options['itinerarios'];


        $choice_transportes = array();
        $choice_empresas = array();
        $choice_satelitales = array();
        $choice_itinerarios = array();


        if (count($this->transportes) > 1) {
            foreach ($this->transportes as $transporte) {
                $choice_transportes[$transporte->getNombre()] = $transporte->getId();
            }
        }

        if (count($this->empresas) > 1) {
            foreach ($this->empresas as $empresa) {
                $choice_empresas[$empresa->getNombre()] = $empresa->getId();
            }
        }

        if (count($this->satelitales) > 1) {
            $choice_satelitales['Todos'] = 0;
            foreach ($this->satelitales as $satelital) {
                $choice_satelitales[$satelital->getNombre()] = $satelital->getId();
            }
        }
        //$choice_itinerarios["---"] = -1;
     //   $choice_itinerarios["Todos"] = 0;
        if (count($this->itinerarios) > 1) {
            foreach ($this->itinerarios as $itinerario) {
                $choice_itinerarios[$itinerario->getCodigo()] = $itinerario->getId();
            }
        }

        $builder
            //                ->add('fecha_desde', TextType::class, array(
            //                    'required' => true,
            //                    'label' => 'Desde',
            //                    'attr' => array(
            //                        'style' => 'width: 105px;')
            //                ))
            //                ->add('fecha_hasta', TextType::class, array(
            //                    'required' => true,
            //                    'label' => 'Hasta',
            //                    'attr' => array(
            //                        'style' => 'width: 105px;')
            //                ))
            ->add('transporte', ChoiceType::class, array(
                'choices' => $choice_transportes,
                'multiple' => true,
            ))
            ->add('satelital', ChoiceType::class, array(
                'choices' => $choice_satelitales,
                'multiple' => true,
            ))
            ->add('empresa', ChoiceType::class, array(
                'choices' => $choice_empresas,
                'multiple' => true,
            ))
            ->add('itinerario', ChoiceType::class, array(
                'choices' => $choice_itinerarios,
                'label' => 'Itinerario',
                'multiple' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'empresas' => null,
            'transportes' => null,
            'satelitales' => null,
            'itinerarios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historico';
    }
}
