<?php

namespace App\Form\monitor;

use App\Entity\Itinerario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of HistoricoItinerarioType
 *
 * @author nicolas
 */
class HistoricoItinerarioType extends AbstractType
{

    protected $servicios = null;
    protected $referencias = null;
    protected $tipoServico = null;
    protected $empresas = null;
    protected $transportes = null;
    protected $satelitales = null;
    protected $itinerario = null;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
      
        $this->servicios = $options['servicios'];
        $this->tipoServicio = $options['tipoServicio'];
        $this->empresas = $options['empresas'];
        $this->transportes = $options['transportes'];
        $this->satelitales = $options['satelitales'];
        $this->itinerario = $options['itinerario'];
        $choice_servicio = array();
        $choice_tipoServicio = array();
        $choice_transportes = array();
        $choice_empresas = array();
        $choice_satelitales = array();

        if (count($this->servicios) > 0) {
            $choice_servicio['Todos'] = 0;
            foreach ($this->servicios as $servicio) {
                $choice_servicio[$servicio->getNombre()] = $servicio->getId();
            }
        }

        if (count($this->tipoServicio) > 0) {
            $choice_tipoServicio['Todos'] = 0;
            foreach ($this->tipoServicio as $tipo) {
                $choice_tipoServicio[$tipo->getNombre()] = $tipo->getId();
            }
        }
        //die('////<pre>'.nl2br(var_export($choice_tipoServicio, true)).'</pre>////');

        if (count($this->transportes) > 0) {
            $choice_transportes['Todos'] = 0;
            foreach ($this->transportes as $transporte) {
                $choice_transportes[$transporte->getNombre()] = $transporte->getId();
            }
        }

        if (count($this->empresas) > 0) {
            $choice_empresas['Todos'] = 0;
            foreach ($this->empresas as $empresa) {
                $choice_empresas[$empresa->getNombre()] = $empresa->getId();
            }
        }

        if (count($this->satelitales) > 0) {
            $choice_satelitales['Todos'] = 0;
            foreach ($this->satelitales as $satelital) {
                $choice_satelitales[$satelital->getNombre()] = $satelital->getId();
            }
        }

        $builder
            ->add('fecha_desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde',
               
            ))
            ->add('fecha_hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta',
                
            ))
            ->add('estado', ChoiceType::class, array(
                'choices' => $this->itinerario->getArrayStrEstado(),
                'required' => true,
                'multiple' => false,
              
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice_servicio,
                'multiple' => false,
               
            ))
            ->add('transporte', ChoiceType::class, array(
                'choices' => $choice_transportes,
                'multiple' => false,
               
            ))
            ->add('satelital', ChoiceType::class, array(
                'choices' => $choice_satelitales,
                'multiple' => false,
               
            ))
            ->add('empresa', ChoiceType::class, array(
                'choices' => $choice_empresas,
                'multiple' => false,
               
            ))
            ->add('tipoServicio', ChoiceType::class, array(
                'choices' => $choice_tipoServicio,
                'multiple' => false,
               
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'servicios' => null,
            'empresas' => null,
            'transportes' => null,
            'satelitales' => null,
            'tipoServicio' => null,
            'itinerario' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historico';
    }
}
