<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\monitor;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class EventoItinerarioType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       
        $builder->add('panico', CheckboxType::class, array(
            'label' => 'Pánico',
            'required' => false,
        ))
        ->add('pois', CheckboxType::class, array(
            'label' => 'Entrada/Salida de Puntos de interés',
            'required' => false,
        ))
        ->add('detencion', CheckboxType::class, array(
            'label' => 'Detención',
            'required' => false,
        ))    
        ->add('detencion_tiempo', IntegerType::class, array(
            'label' => 'Tiempo de Detención',
            'required' => false,
            'data'=> 20,
            'attr' => [
                'min' => 1
              ]
        ))
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getBlockPrefix()
    {
        return 'eventos';
    }
}
