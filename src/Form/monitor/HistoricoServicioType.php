<?php

namespace App\Form\monitor;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of HistoricoItinerarioType
 *
 * @author nicolas
 */
class HistoricoServicioType extends AbstractType
{

    protected $referencias = null;
    protected $tipoServicio = null;
    protected $empresas = null;
    protected $transportes = null;
    protected $satelitales = null;
    protected $itinerarios = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $this->tipoServicio = $options['tipoServicio'];
        $this->empresas = $options['empresas'];
        $this->transportes = $options['transportes'];
        $this->satelitales = $options['satelitales'];
        $this->itinerarios = $options['itinerario'];
        $this->servicio = $options['servicio'];

        $choice_tipoServicio = array();
        $choice_transportes = array();
        $choice_empresas = array();
        $choice_satelitales = array();
        $choice_servicio = array();
        $choice_itinerarios = array();


        if (count($this->servicio) > 0) {
            foreach ($this->servicio as $servicio) {
                $choice_servicio[$servicio->getNombre()] = $servicio->getId();
            }
        }
        // die('////<pre>' . nl2br(var_export($choice_servicio, true)) . '</pre>////');

        if (count($this->tipoServicio) > 0) {
            $choice_tipoServicio['Todos'] = -2;
            $choice_tipoServicio['Sin Reporte'] = -1;
            foreach ($this->tipoServicio as $tipo) {
                $choice_tipoServicio[$tipo->getNombre()] = $tipo->getId();
            }
        }

        if (count($this->transportes) > 0) {
            $choice_transportes['Todos'] = 0;
            foreach ($this->transportes as $transporte) {
                $choice_transportes[$transporte->getNombre()] = $transporte->getId();
            }
        }

        if (count($this->empresas) > 0) {
            $choice_empresas['Todos'] = 0;
            foreach ($this->empresas as $empresa) {
                $choice_empresas[$empresa->getNombre()] = $empresa->getId();
            }
        }

        if (count($this->satelitales) > 0) {
            $choice_satelitales['Todos'] = 0;
            foreach ($this->satelitales as $satelital) {
                $choice_satelitales[$satelital->getNombre()] = $satelital->getId();
            }
        }

        if (count($this->itinerarios) > 0) {
            // $choice_itinerarios['Todos'] = 0;
            foreach ($this->itinerarios as $itinerario) {
                $choice_itinerarios[$itinerario->getCodigo()] = $itinerario->getId();
            }
        }

        $builder

            ->add('estado', ChoiceType::class, array(
                'choices' => array(
                    'Abiertos' => 0,
                    'Cerrados' => 1,
                    'Todos' => 2,
                ),
                'required' => true,
                'multiple' => false,
               
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice_servicio,
                'multiple' => true,
                
            ))
            ->add('transporte', ChoiceType::class, array(
                'choices' => $choice_transportes,
                'multiple' => false,
               
            ))
            ->add('satelital', ChoiceType::class, array(
                'choices' => $choice_satelitales,
                'multiple' => false,
              
            ))
            ->add('empresa', ChoiceType::class, array(
                'choices' => $choice_empresas,
                'label' => 'Cliente',
                'multiple' => false,
               
            ))
            ->add('tipoServicio', ChoiceType::class, array(
                'choices' => $choice_tipoServicio,
                'multiple' => false,
               
            ))
            ->add('itinerario', ChoiceType::class, array(
                'choices' => $choice_itinerarios,
                'multiple' => true,
               
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'empresas' => null,
            'transportes' => null,
            'satelitales' => null,
            'tipoServicio' => null,
            'itinerario' => null,
            'servicio' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historico';
    }
}
