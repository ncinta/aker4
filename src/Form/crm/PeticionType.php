<?php

namespace App\Form\crm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PeticionType extends AbstractType
{

    protected $usuarios;
    protected $servicios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->usuarios = $options['usuarios'];
        $this->servicios = $options['servicios'];
        for ($i = 0; $i <= 100; $i = $i + 10) {
            $porcentaje[$i] = $i . ' %';
        }

        $builder
            ->add('asunto', TextType::class, array(
                'label' => 'Asunto',
                'required' => true
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripción',
                'required' => false
            ))
            ->add('fecha_inicio', DateType::class, array(
                'label' => 'Fecha Inicio',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false
            ))

            ->add('asignado_a', EntityType::class, array(
                'label' => 'Asignado a',
                'class' => 'App\Entity\Usuario',
                'choice_label' => 'nombre',
                'choices' => $this->usuarios,
            ))
            ->add('servicio', EntityType::class, array(
                'label' => 'Servicios',
                'class' => 'App\Entity\Servicio',
                'choice_label' => 'nombre',
                'choices' => $this->servicios,
                'multiple' => false,
                'expanded' => false,
                'required' => true
            ))
            ->add('categoria', EntityType::class, array(
                'label' => 'Categoría',
                'class' => 'App\Entity\CategoriaCrm',
                'choice_label' => 'nombre'
            ))
            ->add('prioridad', ChoiceType::class, array(
                'label' => 'Prioridad',
                'choices' => array('Baja' => 0, 'Normal' => 1, 'Alta' => 2, 'Urgente' => 3),
                'preferred_choices' => array(1),
            ))
            ->add('tipo', ChoiceType::class, array(
                'label' => 'Tipo',
                'choices' => array('Normal' => 0, 'Retrabajo' => 1),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Peticion',
            'usuarios' => null,
            'servicios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'peticion';
    }
}
