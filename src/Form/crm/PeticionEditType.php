<?php

namespace App\Form\crm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Repository\EstadoRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PeticionEditType extends AbstractType
{

    protected $usuarios;
    protected $cambiarAsignado;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->usuarios = $options['usuarios'];
        $this->cambiarAsignado = $options['cambiarAsignado'];
     
        $builder
            ->add('nuevaOT', SubmitType::class)
            ->add('nuevaOR', SubmitType::class)
            ->add('descartar', SubmitType::class)
            ->add('cambiar', SubmitType::class)
            ->add('fecha_inicio', DateType::class, array(
                'label' => 'Fecha Inicio',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false
            ))
            ->add('estado', EntityType::class, array(
                'label' => 'Estado',
                'class' => 'App:Estado',
                'query_builder' => function (EstadoRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.isCerrado = false')
                        ->orderBy('e.posicion', 'ASC');
                },
                'choice_label' => 'nombre'
            ))
            ->add('categoria', EntityType::class, array(
                'label' => 'Categoría',
                'class' => 'App\Entity\CategoriaCrm',
                'choice_label' => 'nombre'
            ))
            ->add('prioridad', ChoiceType::class, array(
                'label' => 'Prioridad',
                'choices' => array('Baja' => 0, 'Normal' => 1, 'Alta' => 2, 'Urgente' => 3),
                'preferred_choices' => array(1),
            ))
            ->add('nota', TextareaType::class, array(
                'label' => 'Nota',
                'required' => false,
                'mapped' => false
            ))
            ->add('tipo', ChoiceType::class, array(
                'label' => 'Tipo',
                'choices' => array('Normal' => 0, 'Retrabajo' => 1),
            ));
        if ($this->cambiarAsignado == true) {
            $builder
                ->add('asignado_a', EntityType::class, array(
                    'class' => 'App\Entity\Usuario',
                    'choice_label' => 'nombre',
                    'choices' => $this->usuarios,
                ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Peticion',
            'usuarios' => null,
            'cambiarAsignado' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'form_peticion';
    }
}
