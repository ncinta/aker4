<?php

namespace App\Form\crm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PeticionFilter extends AbstractType
{

    protected $servicios;
    protected $usuarios;
    protected $usr;
    protected $estado;
    protected $categoria;
    protected $centrocosto;
    protected $grupos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->usr = $options['userlogin'];
        $this->usuarios = $options['usuarios'];
        $this->estado = $options['estado'];
        $this->categoria = $options['categoria'];
        $this->centrocosto = $options['centrocosto'];

        $ch_categoria = array();
        //categoria
        foreach ($this->categoria as $cat) {
            $ch_categoria[$cat->getNombre()] = $cat->getId();
        }
        
        $this->grupos = $options['grupos'];
        $this->servicios = $options['servicios'];

        $choice['Todos los servicios'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        //usuarios
        $ch_usuarios['Todos'] = 0;
        foreach ($this->usuarios as $usr) {
            $ch_usuarios[$usr->getNombre()] = $usr->getId();
        }
        //die('////<pre>' . nl2br(var_export($ch_usuarios, true)) . '</pre>////');
        //estado
        $ch_estado['Nuevo'] = 1;
        foreach ($this->estado as $est) {
            $ch_estado[$est->getNombre()] = $est->getId();
        }
        //usuarios
        $ch_centrocosto['Todos'] = 0;
        foreach ($this->centrocosto as $centro) {
            $ch_centrocosto[$centro->getNombre()] = $centro->getId();
        }
        $builder
            ->add('desde', DateType::class, array(
                'label' => 'Desde',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy ',
                'required' => false,
            ))
            ->add('hasta', DateType::class, array(
                'label' => 'Hasta',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy ',
                'required' => false,
            ))
            ->add('fechatodos', CheckboxType::class, array(
                'label' => 'No tener en cuenta fecha',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'required' => false,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
                'label' => 'Servicio',
            ))
            ->add('asignado_a', ChoiceType::class, array(
                'choices' => $ch_usuarios,
                'preferred_choices' => array($this->usr->getId()),
                'required' => false,
                'label' => 'Asignado a',
            ))
            ->add('estado', ChoiceType::class, array(
                // 'choice_label' => 'nombre',
                'choices' => $ch_estado,
                'preferred_choices' => array(-1),
                'required' => true,
                'label' => 'Estado',
            ))
            ->add('categoria', ChoiceType::class, array(
                // 'choice_label' => 'nombre',
                'choices' => $ch_categoria,
                'required' => false,
                'empty_data' => null,
                'label' => 'Categoría',
            ))
            ->add('prioridad', ChoiceType::class, array(
                'choices' => array('' => -1, 'Baja' => 0, 'Normal' => 1, 'Alta' => 2, 'Urgente' => 3),
                'label' => 'Prioridad',
            ))
            ->add('tipo', ChoiceType::class, array(
                'choices' => array('' => -1, 'Normal' => 0, 'Retrabajo' => 1),
                'label' => 'Tipo',
            ))
            ->add('centrocosto', ChoiceType::class, array(
                'choices' => $ch_centrocosto,
                'multiple' => false,
                'label' => 'Centro Costo',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\FiltroPeticion',
            'grupos' => null,
            'userlogin' => null,
            'usuarios' => null,
            'servicios' => null,
            'estado' => null,
            'categoria' => null,
            'centrocosto' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'filtro';
    }
}
