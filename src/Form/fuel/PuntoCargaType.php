<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PuntoCargaType extends AbstractType
{

    protected $em = null;
    protected $organizacion = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];
        $this->organizacion = $options['organizacion'];

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('cuit', TextType::class, array(
                'label' => 'Cuit',
                'required' => false,
            ))
            ->add('codigo', TextType::class, array(
                'label' => 'Código',
                'required' => false,
            ))
            ->add('servicio', EntityType::class, array(
                'label' => 'Servicio',
                'class' => 'App:Servicio',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Servicio')
                    ->getQueryAllByOrganizacion($this->organizacion)
            ))
            ->add('referencia', EntityType::class, array(
                'label' => 'Referencia',
                'class' => 'App:Referencia',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Referencia')
                    ->getQueryReferenciasDisponibles($this->organizacion)
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\PuntoCarga',
            'em' => null,
            'organizacion' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'puntocarga';
    }
}
