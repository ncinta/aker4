<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CombustibleInformeType extends AbstractType
{

    protected $servicios;
    protected $grupos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $choice[' '] = -1;
        $choice['Todal la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }

        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                  //  'Gráfico' => '3',
                ),
                'required' => true,
                'label' => 'Destino'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,

            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial.'
                ),
                'required' => true,
                'label' => 'Desde',
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final.'
                ),
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'consumocombustible';
    }
}
