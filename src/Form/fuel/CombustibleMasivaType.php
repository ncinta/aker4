<?php

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CombustibleMasivaType extends AbstractType
{

    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $estaciones = array();
        $puntoscarga = array();
        $servicios = array();
        $this->em = $options['em'];


        foreach ($options['estaciones'] as $estacion) {
            $estaciones[$estacion->getNombre()] = $estacion->getId();
        }
        if ($options['puntoscarga']) {
            foreach ($options['puntoscarga'] as $puntocarga) {
                $puntoscarga[$puntocarga->getNombre()] = $puntocarga->getId();
            }
        }

        foreach ($options['servicios'] as $servicio) {

            $servicios[$servicio->getNombre()] = $servicio->getId();
        }

        // die('////<pre>'.nl2br(var_export(count($esta), true)).'</pre>////');

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $servicios,
                'multiple' => false,
                'required' => true,
                'label' => 'Servicio',

            ))
            ->add('fecha', TextType::class, array(
                'required' => true,
            ))
            ->add('litros_carga', NumberType::class, array(
                'required' => true,
            ))
            ->add('tipoCarga', ChoiceType::class, array(
                'choices' => array('Parcial' => 1, 'Completo' => 2),
                'multiple' => false,
                'required' => true,
                'label' => 'Parcial/Completo',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('monto_total', NumberType::class, array(
                'required' => false,
                'attr' => array('min' => '0', 'step' => '0.01'),
            ))
            ->add('guia_despacho', TextType::class, array(
                'label' => 'Guia Despacho',
                'required' => false,
            ))
            ->add('descripcion', TextType::class, array(
                'label' => 'Descripción',
                'required' => false,
            ))
            ->add('odometro', NumberType::class, array(
                'label' => 'Odómetro',
                'required' => false,
            ))
            ->add('horometro', NumberType::class, array(
                'label' => 'Horometro',
                'required' => false,
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $estaciones,
                'multiple' => false,
                'required' => false,
                'label' => 'Estación',
            ))
            ->add('puntocarga', ChoiceType::class, array(
                'choices' => $puntoscarga,
                'multiple' => false,
                'required' => false,
                'label' => 'Punto de Carga',
            ))
            ->add('chofer', EntityType::class, array(
                'label' => 'Chofer',
                'class' => 'App:Chofer',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Chofer')->getQueryByOrganizacion($options['organizacion']),
            ))
            ->add('tipoCombustible', EntityType::class, array(
                'label' => 'Combustible',
                'class' => 'App:TipoCombustible',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:TipoCombustible')->getQuery(),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'estaciones' => null,
            'puntoscarga' => null,
            'servicios' => null,
            'organizacion' => null,
            'em' => null
        ));
    }

    public function getBlockPrefix()
    {

        return 'secur_combustibletype';
    }
}
