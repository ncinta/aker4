<?php

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AddCombustibleType extends AbstractType
{

    private $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];
        $estaciones = array();
        if ($options['estaciones']) {
            foreach ($options['estaciones'] as $estacion) {
                $estaciones[$estacion->getNombre()] = $estacion->getId();
            }
        }
        $builder
            ->add('fecha', TextType::class, array(
                'required' => true
            ))
            ->add('litros_carga', TextType::class)
            ->add('monto_total', TextType::class)
            ->add('guia_despacho')
            ->add('odometroTablero', IntegerType::class, array(
                'label' => 'Odómetro',
                'required' => false,
            ))
            ->add('horometroTablero', IntegerType::class, array(
                'label' => 'Horómetro',
                'required' => false,
            ))
            ->add('puntoCarga', ChoiceType::class, array(
                'choices' => $estaciones,
                'multiple' => false,
                'required' => false,
                'label' => 'Estacion',
            ))
            ->add('cargaCompleta', ChoiceType::class, array(
                'choices' => array('Parcial' => 1, 'Completo' => 2),
                'multiple' => false,
                'required' => true,
                'label' => 'Parcial/Completo',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('descripcion', TextType::class, array(
                'label' => 'Descripción',
                'required' => false,
            ))
            ->add('chofer', EntityType::class, array(
                'label' => 'Chofer',
                'class' => 'App:Chofer',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Chofer')->getQueryByOrganizacion($options['organizacion']),
            ))
            ->add('tipoCombustible', EntityType::class, array(
                'label' => 'Combustible',
                'class' => 'App:TipoCombustible',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:TipoCombustible')->getQuery(),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CargaCombustible',
            'estaciones' => null,
            'organizacion' => null,
            'em' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'addfuel';
    }
}
