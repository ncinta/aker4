<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CombustibleGraficoType extends AbstractType
{

    protected $servicios;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        //        $choice[] = 'Todal la flota';
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        $builder
            ->add('tipo', ChoiceType::class, array(
                'choices' => array(
                    'Porcentaje de Tanque' => '1',
                    'Litros en Tanque' => '2',
                    'Litros/Distancia' => '3',
                    'Litros/Tiempo' => '4',
                ),
                'required' => true,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                ),
                'required' => true,
                'label' => 'Desde',
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array('class' => 'calendario'),
                'required' => true, 'label' => 'Hasta'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'graficoconsumo';
    }
}
