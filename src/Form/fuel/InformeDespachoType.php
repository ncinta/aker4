<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\fuel;

use App\Entity\PuntoCarga;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class InformeDespachoType extends AbstractType
{

    protected $estaciones;

    private function getEstaciones()
    {
        $choice['Todas las estaciones'] = 0;
        foreach ($this->estaciones as $estacion) {
            if ($estacion instanceof PuntoCarga) {
                //le colocamos un punto adelante para saber que es un punto de carga
                $choice[$estacion->getNombre()] = $estacion->getId();
            } else {
                $choice['Referencias'][$estacion->getId()] = $estacion->getNombre();
            }
        }
        //dd($choice);
        return $choice;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->estaciones = $options['estaciones'];

        $builder
            ->add('estacion', ChoiceType::class, array(
                'label' => 'Punto de despacho',
                'choices' => $this->getEstaciones(),
                'required' => true,
                //'multiple' => true,
                'mapped' => false,
                //'choice_value' => function ($value) {return $value;},
                //'choice_label' => function ($value) {return $value;},
            ))
            ->add('tipoCombustible', EntityType::class, array(
                'label' => 'Combustible',
                'class' => 'App:TipoCombustible',
                'required' => false,
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                )
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Desde'
                ),
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Hasta'
                ),
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'estaciones' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informedespacho';
    }
}
