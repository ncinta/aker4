<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PuntoCargaCombType extends AbstractType
{

    protected $combustibles = null;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->combustibles = $options['combustibles'];
        $choice_combustible = array();
        if (count($this->combustibles) > 0) {
            foreach ($this->combustibles as $combustible) {
                $choice_combustible[$combustible->getNombre()] = $combustible->getId();
            }
        }
        $builder
            ->add('id', HiddenType::class)
            ->add('precio', NumberType::class, array(
                'label' => 'Precio',
                'required' => true,
                'attr' => array(
                    'style' => 'width: 200px;'
                )
            ))
            ->add('tipocombustible', ChoiceType::class, array(
                'choices' => $choice_combustible,
                //             'choices_as_values' => true,
                'label' => 'Combustible',
                'multiple' => false,
                'attr' => array(
                    'style' => 'width: 200px;'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'combustibles' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'puntocarga_comb';
    }
}
