<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\fuel;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class InformeCargasType extends AbstractType
{

    protected $estaciones;
    protected $servicios;
    protected $grupos;

    private function getEstaciones()
    {
        $choice['Todas las estaciones'] = 0;
        foreach ($this->estaciones as $estacion) {
            $choice[$estacion->getNombre()] = $estacion->getId();
        }
        return $choice;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->estaciones = $options['estaciones'];
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }


        $builder
            ->add('estacion', ChoiceType::class, array(
                'label' => 'Estación de Servicio',
                'choices' => $this->getEstaciones(),
                'required' => true,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                )
            ))->add('ingreso', ChoiceType::class, array(
                'choices' => array(
                    'Todos' => '-1',
                    'Manual' => '0',
                    'Web Service' => '1',
                    'Aplicación' => '2',
                    'Excel' => '3',
                    'Checkeado' => '9',
                )
            ))
            ->add('mostrar_detallado', CheckboxType::class, array(
                'label' => 'Detalle de cargas',
                'required' => false,
                'attr' => array(
                    'checked' => 'checked',
                    'title' => 'Muestra los detalles de carga por carga, sino los totaliza.',
                )
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'estaciones' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informecarga';
    }
}
