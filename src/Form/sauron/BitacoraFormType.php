<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BitacoraFormType extends AbstractType
{

    protected $eventos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->eventos = $options['eventos'];
        $ch_evento = array();
        foreach ($this->eventos as $key => $value) {
            $ch_evento[$value['desc']] = $key;
        }
        //die('////<pre>'.nl2br(var_export($ch_evento, true)).'</pre>////');
        $ch_evento['Todos'] = 0;
        $builder
            ->add('fecha_desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('fecha_hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('evento', ChoiceType::class, array(
                'choices' => $ch_evento,
                'multiple' => false,
                'attr' => array(
                    'title' => 'Eventos a visualizar',
                    'style' => 'width: 119px;'
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'eventos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'bitacoraform';
    }
}
