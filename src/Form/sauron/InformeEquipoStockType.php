<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InformeEquipoStockType
 *
 * @author nicolas
 */
class InformeEquipoStockType extends AbstractType
{

    protected $organizaciones;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizaciones = $options['organizaciones'];
        $choice['Todas las organizaciones'] = 0;
        foreach ($this->organizaciones as $org) {
            if ($org->getTipoOrganizacion() == 1) {
                $choice[$org->getNombre() . ($org->getOrganizacionPadre() ? ' (' . $org->getOrganizacionPadre()->getNombre() . ')' : '')] = $org->getId();
            }
        }

        $builder
            ->add('organizacion', ChoiceType::class, array(
                'label' => 'Distribuidor',
                'choices' => $choice,
                'required' => true,
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla'  => '1',
                ),
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'organizaciones' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informesinreportes';
    }
}
