<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CombustibleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', TextType::class)
            ->add('litros_carga', NumberType::class, array(
                'scale' => 2
            ))
            ->add('guia_despacho')
            ->add('odometro', IntegerType::class, array(
                'required' => false,
            ))
            ->add('monto_total', NumberType::class, array(
                'scale' => 2
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CargaCombustible',
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_combustibletype';
    }
}
