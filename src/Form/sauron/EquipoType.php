<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EquipoType extends AbstractType
{

    protected $orgs;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->orgs = $options['organizaciones'];
        $choice = array();
        foreach ($this->orgs as $org) {
            $choice[$org->getId()] = $org->getNombre();
        }
        //die('////<pre>'.nl2br(var_export($organizaciones, true)).'</pre>////');

        $builder
            ->add('mdmid', TextType::class, array(
                'required' => true,
                'label' => 'Modem ID '
            ))
            ->add('imei', TextType::class, array(
                'required' => false,
                'label' => 'Número Imei'
            ))
            ->add('numeroSerie', TextType::class, array(
                'required' => false,
                'label' => 'Número de Serie'
            ))
            ->add('modelo', EntityType::class, array(
                'required' => true,
                'label' => 'Modelo',
                'class' => 'App:Modelo',
            ))
            ->add('proveedor', EntityType::class, array(
                'required' => false,
                'label' => 'Proveedor',
                'class' => 'App:Proveedor',
            ))
            ->add('propietario', EntityType::class, array(
                'class' => 'App:Organizacion',
                'choice_label' => 'nombre',
                'label' => 'Propietario',
                'choices' => $this->orgs
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Equipo',
            'organizaciones' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'equipo';
    }
}
