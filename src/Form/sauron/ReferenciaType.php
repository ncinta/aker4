<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Description of ReferenciaType
 *
 * @author yesica
 */
class ReferenciaType extends AbstractType
{

    protected $propietario;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->propietario = $options['propietario'];
        $this->em = $options['em'];
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre de referencia',
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripcion',
                'required' => false,
            ))
            ->add('pathIcono', HiddenType::class)
            ->add('direccion', TextType::class, array(
                'label' => 'Direccion',
                'required' => false,
            ))
            ->add('latitud', TextType::class, array(
                'label' => 'Latitud',
                'required' => true,
            ))
            ->add('longitud', TextType::class, array(
                'label' => 'Longitud',
                'required' => true,
            ))
            ->add('radio', IntegerType::class, array(
                'label' => 'Longitud de radio',
                'required' => false,
            ))
            ->add('velocidadMaxima', IntegerType::class, array(
                'label' => 'Velocidad Max en referencia',
                'required' => false,
            ))
            //                ->add('cortePorRalenti', IntegerType::class, array(
            //                    'label' => 'Tiempo antes corte por ralenti',
            //                    'required' => false,
            //                ))
            ->add('entrada', CheckboxType::class, array(
                'label' => 'Entrada a referencia',
                'required' => false,
                'attr' => array('checked' => 'checked'),
            ))
            ->add('salida', CheckboxType::class, array(
                'label' => 'Salida a referencia',
                'required' => false,
                'attr' => array('checked' => 'checked'),
            ))
            ->add('anguloDeteccion', TextType::class, array(
                'label' => 'En grados',
                'required' => false,
            ))
            ->add('categoria', EntityType::class, array(
                'label' => 'Categoria',
                'class' => 'App:Categoria',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Categoria')->getQueryCategoriasDisponibles($this->propietario),
            ))
            ->add('tipoReferencia', EntityType::class, array(
                'label' => 'Tipo de referencia',
                'class' => 'App:TipoReferencia',
                'required' => true,
            ))
            ->add('visibilidad', CheckboxType::class, array(
                'label' => 'Mostrar en mapa',
                'required' => false,
                'attr' => array('checked' => true)
            ))
            ->add('kml', FileType::class, array(
                'required' => false,
                'label' => 'Importar archivo KML'
            ))
            ->add('codigoExterno', TextType::class, array(
                'label' => 'Identificador',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Referencia',
            'propietario' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
