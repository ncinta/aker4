<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServicioShellBajaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha_baja', 'text', array(
                'required' => true,
                'label' => 'Fecha de Baja',
            ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
