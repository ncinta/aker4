<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EquipoRetiroType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('motivo', TextareaType::class, array(
                'required' => true,
                'label' => 'Motivo'
            ));
    }

    public function getBlockPrefix()
    {
        return 'retiro';
    }
}
