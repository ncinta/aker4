<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortalType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referencia')
            ->add('tipoPortal')
            ->add('desde')
            ->add('hasta')
            ->add('dias')
            ->add('precio');
    }

    public function getBlockPrefix()
    {
        return 'precioportal';
    }
}
