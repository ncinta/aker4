<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServicioShellAltaType extends AbstractType
{

    protected $patente;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->patente = $options['patente'];
        $builder
            ->add('fecha_alta', 'text', array(
                'required' => true,
                'label' => 'Fecha de Alta',
            ))
            ->add('dato_fiscal', 'text', array(
                'required' => true,
                'label' => 'RUT',
            ))
            ->add('patente', 'text', array(
                'required' => true,
                'label' => 'Patente',
                'attr' => array(
                    'value' => $this->patente,
                )
            ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
