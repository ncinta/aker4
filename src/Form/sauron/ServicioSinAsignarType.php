<?php

/**
 * Description of ServicioSinAsignarType
 *
 * @author yesica
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ServicioSinAsignarType extends AbstractType
{

    protected $qbServicios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->qbServicios = $options['queryResult'];
        $choice = array();
        if ($this->qbServicios != null) {
            foreach ($this->qbServicios as $servicio) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
            //sdie('////<pre>'.nl2br(var_export($choice, true)).'</pre>////');
            $builder->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'queryResult' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
