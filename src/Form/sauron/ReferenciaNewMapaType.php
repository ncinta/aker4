<?php

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferenciaNewMapaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organizacion_id')
            ->add('nombre', 'text', array(
                'label' => 'Nombre',
            ))
            ->add('pathIcono', 'hidden')
            ->add('direccion', 'text', array(
                'label' => 'Direccion',
                'required' => false,
            ))
            ->add('latitud', 'text', array(
                'label' => 'Latitud',
                'required' => false,
            ))
            ->add('longitud', 'text', array(
                'label' => 'Longitud',
                'required' => false,
            ))
            ->add('radio', 'integer', array(
                'label' => 'Longitud de radio',
                'required' => false,
            ))
            ->add('velocidadMaxima', 'integer', array(
                'label' => 'Velocidad Max.',
                'required' => false,
            ))
            ->add('categoria', 'entity', array(
                'label' => 'Categoria',
                'class' => 'App:Categoria',
                'required' => false,
            ))
            ->add('tipoReferencia', 'entity', array(
                'label' => 'Tipo',
                'class' => 'App:TipoReferencia',
                'required' => false,
            ))
            ->add('codigoExterno', 'text', array(
                'label' => 'Identificador',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Referencia',
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
