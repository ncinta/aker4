<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class InputsType extends AbstractType
{

    protected $modelos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->modelos = $options['modelos'];
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre'
            ))
            ->add('icono', HiddenType::class)
            ->add('abbr', TextType::class, array(
                'label' => 'Abreviatura',
            ))
            ->add('modeloSensor', EntityType::class, array(
                'class' => 'App:ModeloSensor',
                'choice_label' => 'nombre',
                'label' => 'Modelo de Input',
                'choices' => $this->modelos
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ServicioInputs',
            'modelos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'inputs';
    }
}
