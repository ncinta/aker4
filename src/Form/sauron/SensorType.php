<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\sauron;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class SensorType extends AbstractType
{

    protected $modelos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->modelos = $options['modelos'];
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre del Sensor'
            ))
            ->add('abbr', TextType::class, array(
                'label' => 'Abreviatura',
                //                    'max_length' => 5,
            ))
            ->add('serie', TextType::class, array(
                'label' => 'Nro. de Serie',
                'required' => false,
            ))
            ->add('modeloSensor', EntityType::class, array(
                'class' => 'App:ModeloSensor',
                'choice_label' => 'nombre',
                'label' => 'Modelo de Sensor',
                'choices' => $this->modelos
            ))
            ->add('medida', ChoiceType::class, array(
                'label' => 'Unidad de Medida',
                'required' => false,
                'choices' => array(
                    '°C' => '°C', '°F' => '°F', 'hPa' => 'hPa', '%' => '%'
                )
            ))
            ->add('valorMinimo', NumberType::class, array('label' => 'Valor Mínimo', 'required' => false))
            ->add('valorMaximo', NumberType::class, array('label' => 'Valor Máximo', 'required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Sensor',
            'modelos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'sensor';
    }
}
