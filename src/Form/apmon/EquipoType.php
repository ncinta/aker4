<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EquipoType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('mdmid',  TextType::class, array(
                'required' => true,
                'label' => 'Modem ID '
            ))
            ->add('imei',  TextType::class, array(
                'required' => false,
                'label' => 'Número Imei'
            ))
            ->add('numeroSerie', TextType::class, array(
                'required' => false,
                'label' => 'Número de Serie'
            ))
            ->add('modelo', EntityType::class, array(
                'required' => true,
                'label' => 'Modelo',
                'class' => 'App:Modelo',
            ))
            ->add('proveedor', EntityType::class, array(
                'required' => false,
                'label' => 'Proveedor',
                'class' => 'App:Proveedor',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Equipo',
        ));
    }

    public function getBlockPrefix()
    {
        return 'equipo';
    }
}
