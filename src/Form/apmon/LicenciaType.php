<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LicenciaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', TextType::class, array(
                'label' => 'Nro. Licencia',
                'required' => false,
                
            ))  
            ->add('categoria', TextType::class, array(
                'label' => 'Categoría',
                'required' => false,
                
            ))  
            ->add('expedicion', TextType::class, array(
                'label' => 'Fecha Expedición',
                'required' => false,
                
            ))
            ->add('vencimiento', TextType::class, array(
                'label' => 'Fecha Vencimiento',
                'required' => false,
                
            ))  
             ->add('aviso', IntegerType::class, array(
                'label' => 'Aviso',
                'required' => false,
                
            ))
            ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Licencia',
        ));
    }

    public function getBlockPrefix()
    {
        return 'licencia';
    }
}
