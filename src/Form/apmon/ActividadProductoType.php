<?php

namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * Description of ActividadType
 *
 * @author nicolas
 */
class ActividadProductoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cantidad', NumberType::class, array(
                'label' => 'Cantidad',
                'required' => true,
            ))
            ->add('producto', EntityType::class, array(
                'label' => 'Producto',
                'class' => 'App:Producto',
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ActividadProducto',
        ));
    }

    public function getBlockPrefix()
    {
        return 'actividad';
    }
}
