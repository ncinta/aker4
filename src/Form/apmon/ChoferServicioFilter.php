<?php

namespace App\Form\apmon;

use App\Entity\ChoferServicioHistorico;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * Description of ChoferServicioFilter
 *
 * @author nicolas
 */
class ChoferServicioFilter extends AbstractType
{

    protected $organizacion;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];
        $builder
            ->add('desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $this->getServicios(),
                'multiple' => false,
                'required' => false,
                'label' => 'Servicio',


            ))
            ->add('chofer', ChoiceType::class, array(
                'choices' => $this->getChoferes(),
                'multiple' => false,
                'required' => false,
                'label' => 'Chofer',


            ))
            ->add('estado', ChoiceType::class, array(
                'choices' => $this->getEstados(),
                'multiple' => false,
                'required' => false,
                'label' => 'Estado',
            ));
    }

    private function getChoferes()
    {
        $choice = array();
        foreach ($this->organizacion->getChoferes() as $chofer) {
            $choice[$chofer->getNombre()] = $chofer->getId();
        }
        return $choice;
    }
    private function getServicios()
    {
        $servicios = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($this->organizacion);
        $choice = array();
        foreach ($servicios as $servicio) {
            if ($servicio->getVehiculo()) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        //ksort($choice);
        return $choice;
    }

    private function getEstados()
    {
        $choferServicio = new ChoferServicioHistorico();
        return $choferServicio->getArrayStrEstado();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historico';
    }
}
