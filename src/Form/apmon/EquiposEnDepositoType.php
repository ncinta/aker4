<?php

/**
 * Description of ServicioSinAsignarType
 *
 * @author yesica
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class EquiposEnDepositoType extends AbstractType
{

    protected $qbEquipos;
    protected $servicio;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->qbEquipos = $options['queryResult'];
        $this->servicio = $options['servicio'];

        foreach ($this->qbEquipos as $equipo) {
            $choice[$equipo->getMdmid() . ' (' . $equipo->getNumeroSerie() . ' - ' . $equipo->getModelo() . ')'] = $equipo->getId();
        }

        $builder->add('equipo', ChoiceType::class, array(
            'choices' => $choice,
            'multiple' => false,
        ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'queryResult' => null,
            'servicio' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
