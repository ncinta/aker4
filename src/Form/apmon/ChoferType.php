<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChoferType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('cuit', TextType::class, array(
                'label' => 'Identificador Fiscal',
                'required' => false,
            ))
            ->add('documento', TextType::class, array(
                'label' => 'Documento Personal',
                'required' => true,
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Teléfono',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Chofer',
        ));
    }

    public function getBlockPrefix()
    {
        return 'chofer';
    }
}
