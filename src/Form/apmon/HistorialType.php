<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class HistorialType extends AbstractType
{

    protected $servicios;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $choice = array();
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        $referencias['Todos los grupos'] = 0;
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ))
            ->add('desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('referencias', ChoiceType::class, array(
                'choices' => $referencias,
                'multiple' => false,
                'attr' => array(
                    'title' => 'Grupo de referencias a considerar'
                ),
            ))
            ->add('solo_referencias', ChoiceType::class, array(
                'choices' => array(
                    'Todas las tramas' => '0',
                    'Solo tramas con referencias' => '1'
                ),
                'attr' => array(
                    'title' => 'Opcion de referencias'
                )
            ))
            ->add('tramas', ChoiceType::class, array(
                'required' => false,
                'expanded' => true,
                'multiple' => false,
                'choices' => array(
                    'Sólo posiciones válidas' => '0',
                    'Tódos los reportes' => '1'
                ),
                'attr' => array(
                    'title' => 'Tipo de reportes a incluir'
                ),
            ))
            ->add('mostrar_bateria', CheckboxType::class, array(
                'label' => 'Nivel de bateria',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra el nivel de batería en cada reporte'
                )
            ))
            ->add('mostrar_sensores', CheckboxType::class, array(
                'label' => 'Sensores',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra el estado de los sensores en cada reporte'
                )
            ))
            ->add('mostrar_tiempos', CheckboxType::class, array(
                'label' => 'Tiempos y distancias',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra el tiempo y la distancia empleados desde la transmisión anterior.'
                )
            ))
            ->add('mostrar_fecha_recepcion', CheckboxType::class, array(
                'label' => 'Fecha de recepción',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra la fecha y hora de recepción de la comunicacion en nuestros servidores.'
                )
            ))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'label' => 'Direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en cada transmisión.',
                    'onclick' => 'if (document.getElementById("historial_mostrar_direcciones").checked) {
                                   alert("Si elige mostrar las direcciones puede hacer que la obtención de datos sea lenta.");
                                };',
                )
            ))
            ->add('mostrar_sinposicion', CheckboxType::class, array(
                'label' => 'Sin posición',
                'required' => false,
                'attr' => array(
                    'title' => 'Se incluyen las que no tiene posición válida.'
                )
            ))
            ->add('mostrar_referencias', CheckboxType::class, array(
                'label' => 'Referencias',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las referencias por las que pasa.'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historial';
    }
}
