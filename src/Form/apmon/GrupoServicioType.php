<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Description of GrupoReferenciaType
 *
 * @author yesica
 */
class GrupoServicioType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre de Grupo'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\GrupoServicio',
        ));
    }

    public function getBlockPrefix()
    {
        return 'grupo_servicio';
    }
}
