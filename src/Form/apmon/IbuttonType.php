<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class IbuttonType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dallas', TextType::class, array(
                'label' => 'Dallas',
                'required' => true,
            ))

            ->add('tipo', ChoiceType::class, array(
                'label' => 'Tipo',
                'choices' => ['Seleccione un tipo'=> 0,'Ibutton' => 1, 'Otro' => 2],
                'required' => true,
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Ibutton',
        ));
    }

    public function getBlockPrefix()
    {
        return 'ibutton';
    }
}
