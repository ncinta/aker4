<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ChoferTomarServicioType extends AbstractType
{

    protected $servicios;
    protected $estados;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $this->estados = $options['estados'];

        $choice = array();
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
                'multiple' => false,
            ))
            ->add('estado', ChoiceType::class, array(
                'choices' => $this->estados,
                'required' => true,
                'multiple' => false,
            ))

            ->add('fecha', TextType::class, array(
                'required' => true, 'label' => 'Fecha'
            ));
          
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'estados' => null

        ));
    }

    public function getBlockPrefix()
    {
        return 'chofer_servicio';
    }
}
