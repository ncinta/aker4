<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PrestacionType extends AbstractType
{

    protected $organizacion;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('inicio_prestacion', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicio'
                ),
                'required' => false,
                'label' => 'Desde',
            ))
            ->add('fin_prestacion', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha finalizacion'
                ),
                'required' => false,
                'label' => 'Hasta',
            ))
            ->add('objetivo', EntityType::class, array(
                'label' => 'Objetivo',
                'class' => 'App:Objetivo',
                'required' => false,
            ))
            ->add('transporte', EntityType::class, array(
                'label' => 'Transporte',
                'class' => 'App:Transporte',
                'required' => false,
            ))
            ->add('contratista', EntityType::class, array(
                'label' => 'Contratista',
                'class' => 'App:Contratista',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Contratista')->getQueryByOrganizacion($this->organizacion),
            ))
            ->add('servicios', EntityType::class, array(
                'label' => 'Servicios',
                'class' => 'App:Servicio',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Servicio')->getQueryByOrganizacion($this->organizacion),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Prestacion',
            'organizacion' => null,
            'em' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'prestacion';
    }
}
