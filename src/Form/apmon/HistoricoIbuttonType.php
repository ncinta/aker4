<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HistoricoIbuttonType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choice['Enviar a Depósito'] = '1';
        $choice['Reportar Extravío'] = '3';

        $builder
            ->add('fecha', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha'
                ),
                'required' => true, 'label' => 'Fecha'
            ))
            ->add('accion', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('motivo', TextareaType::class, array(
                'label' => 'Nota',
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    public function getBlockPrefix()
    {
        return 'historico_ibutton';
    }
}
