<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UsuarioNewType extends AbstractType
{

    protected $paises;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->paises = $options['paises'];

        $builder
            ->add('nombre', TextType::class, array(
                'required' => true,
                'label' => 'Apellido y Nombre'
            ))
            ->add('username', TextType::class, array(
                'required' => true,
                'label' => 'Nombre Usuario'
            ))
            ->add('email', EmailType::class, array(
                'required' => true,
                'label' => 'Email'
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class, 'options' => array('required' => true), 'first_options' => array('label' => 'Clave'),
                'second_options' => array('label' => 'Repita Clave'),
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Habilitado',
                'required' => false
            ))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => $this->paises
            ))
            ->add('vista_mapa', ChoiceType::class, array(
                'label' => 'Vista de Mapas',
                'expanded' => false,
                'choices' => array(
                    'Vista de Mapa y Satelite' => '1',
                    'Vista de Mapa' => '2',
                    'Vista de Satelite' => '3',
                )
            ))
            ->add('redirigir_login', ChoiceType::class, array(
                'label' => 'Redirigir en login',
                'expanded' => false,
                'empty_data' => 0,
                'choices' => array(
                    'No redirigir' => '0',
                    'A Ver Flota' => '1',
                )
            ))
            ->add('change_password', CheckboxType::class, array(
                'label' => 'Cambiar Contraseña',
                'required' => false
            ))
            ->add('showReferencias', CheckboxType::class, array(
                'label' => 'Mostrar Referencias en Mapas',
                'required' => false
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array(
                    'Sin Limite' => '0',
                    '7 días' => '7',
                    '10 días' => '10',
                    '15 días' => '15',
                    '30 días' => '30',
                    '45 días' => '45',
                    '1 meses' => '60',
                    '6 meses' => '180',
                )
            ))
            ->add('telefono', TextType::class, array(
                'label' => 'Número Celular',
                'required' => false,
            )) ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Usuario',
            'paises' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'usuario';
    }
}
