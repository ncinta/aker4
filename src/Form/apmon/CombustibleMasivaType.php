<?php


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CombustibleMasivaType extends AbstractType
{

    protected $estaciones;
    protected $servicios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $estaciones = array();
        foreach ($options['estaciones'] as $estacion) {
            $estaciones[$estacion->getId()] = $estacion->getNombre();
        }
        $servicios = array();
        foreach ($options['servicios'] as $servicio) {
            $servicios[$servicio->getId()] = $servicio->getNombre();
        }
        // die('////<pre>'.nl2br(var_export(count($esta), true)).'</pre>////');
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $servicios,
                'multiple' => false,
                'required' => true,
                'label' => 'Servicio',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('fecha', TextType::class, array(
                'required' => true,
            ))
            ->add('litros_carga', NumberType::class, array(
                'required' => true,
            ))
            ->add('monto_total', NumberType::class, array(
                'required' => false,
            ))
            ->add('guia_despacho', TextType::class, array(
                'label' => 'Guia Despacho',
                'required' => false,
            ))
            ->add('odometro', NumberType::class, array(
                'label' => 'Odómetro Tablero',
                'required' => false,
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $estaciones,
                'multiple' => false,
                'required' => false,
                'label' => 'Estación',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\CargaCombustible',
            'estaciones' => null,
            'servicios' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_combustibletype';
    }
}
