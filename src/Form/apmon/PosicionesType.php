<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class PosicionesType extends AbstractType
{

    protected $servicios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->servicios = $options['servicios'];

        $choice = array();

        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ))
            ->add('tiempo', ChoiceType::class, array(
                'choices' => array(
                    '5 mins.' => '5',
                    '10 mins.' => '10',
                    '15 mins.' => '15',
                    '20 mins.' => '20',
                    '25 mins.' => '25',
                    '30 mins.' => '30',
                    '35 mins.' => '35',
                    '40 mins.' => '40',
                    '45 mins.' => '45',
                    '50 mins.' => '50',
                    '55 mins.' => '55',
                    '60 mins.' => '60',
                ),
                'multiple' => false,
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'posiciones';
    }
}
