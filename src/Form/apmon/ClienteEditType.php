<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClienteEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('direccion')
            ->add('telefono_oficina', TextType::class, array('label' => 'Teléfono'))
            ->add('email', TextType::class, array('label' => 'E-Mail'))
            ->add('web_site')
            ->add('dato_fiscal', TextType::class, array(
                'label' => 'Identificación Fiscal',
                'required' => false
            ))
            ->add('proveedor_mapas', ChoiceType::class, array(
                'label' => 'Vista de Mapas',
                'expanded' => false,
                'choices' => array(
                    'Security' => 'security',
                    'GoogleMaps' => 'google'
                )
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array(
                    'Sin Limite' => '0',
                    '10 días' => '10',
                    '30 días' => '30',
                    '6 meses' => '180',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
        ));
    }

    public function getBlockPrefix()
    {
        return 'distribuidor';
    }
}
