<?php


/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Form\apmon;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ChoferIbuttonType extends AbstractType
{

    protected $ibuttons;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->ibuttons = $options['ibuttons'];
        $choice = array();
        foreach ($this->ibuttons as $ibutton) {
            $choice[$ibutton->getDallas()] = $ibutton->getId();
        }

        $builder
            ->add('ibutton', ChoiceType::class, array(
                'choices' => $choice,

                'required' => true,
            ))
            ->add('fecha', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha'
                ),
                'required' => true, 'label' => 'Fecha'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'ibuttons' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'chofer_ibutton';
    }
}
