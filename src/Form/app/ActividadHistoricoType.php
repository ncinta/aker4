<?php


namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ActividadHistoricoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        foreach ($options['productos'] as $actProd) {
            $builder
                ->add($actProd->getActividad()->getId(), CheckboxType::class, array(
                    'required' => false,
                    'label' => $actProd->getActividad()->getNombre(),
                    //'value' => $actividad->getId()
                ))
                ->add($actProd->getProducto()->getId(), TextType::class, array(
                    'required' => true,
                    'label' => $actProd->getProducto()->getNombre(),
                    //   'value' => $producto->getCantidad()
                ));
        }

        //die('////<pre>' . nl2br(var_export($actividad->getId(), true)) . '</pre>////');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'productos' => null,
            // 'data_class' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'actividades';
    }
}
