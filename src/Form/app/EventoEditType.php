<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EventoEditType extends AbstractType
{

    private $tnotificweb;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->tnotificweb = $options['tnotificweb'];
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombre', 'required' => true))
            ->add('activo', CheckboxType::class, array('label' => 'Activo', 'required' => true))
            ->add('lunes', CheckboxType::class, array('label' => 'Lunes', 'required' => false))
            ->add('martes', CheckboxType::class, array('label' => 'Martes', 'required' => false))
            ->add('miercoles', CheckboxType::class, array('label' => 'Miercoles', 'required' => false))
            ->add('jueves', CheckboxType::class, array('label' => 'Jueves', 'required' => false))
            ->add('viernes', CheckboxType::class, array('label' => 'Viernes', 'required' => false))
            ->add('sabado', CheckboxType::class, array('label' => 'Sábado', 'required' => false))
            ->add('domingo', CheckboxType::class, array('label' => 'Domingo', 'required' => false))
            ->add('sonido', CheckboxType::class, array('label' => 'Reproducir Sonido', 'required' => false))
            ->add('registrar', CheckboxType::class, array('label' => 'Pedir Respuesta', 'required' => false))
            ->add('horaInicio', TextType::class, array('label' => 'Desde las'))
            ->add('horaFin', TextType::class, array('label' => 'hasta las'))
            ->add('protocolo', TextareaType::class, array('label' => 'Protocolo', 'required' => false))
            ->add('notificacionWeb', ChoiceType::class, array(
                'label' => 'Tipo de Alerta Web',
                'required' => true,
                'choices' => $this->tnotificweb
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Evento',
            'tnotificweb' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'evento';
    }
}
