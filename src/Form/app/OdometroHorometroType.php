<?php


namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

/**
 * Description of TallerType
 *
 * @author nicolas
 */
class OdometroHorometroType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('odometro', NumberType::class, array(
                'label' => 'Odómetro',
                'required' => true,
            ))
            ->add('horometro_hora', IntegerType::class, array(
                'required' => true,
            ))
            ->add('horometro_minuto', IntegerType::class, array(
                'required' => true,
            ))
            ->add('fecha', DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'valores';
    }
}
