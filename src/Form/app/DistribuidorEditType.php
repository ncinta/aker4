<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\Form\app\DistribuidorConfigType as DistribuidorConfigType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DistribuidorEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('direccion')
            ->add('telefono_oficina', TextType::class, array('label' => 'Teléfono'))
            ->add('email', 'email', array('label' => 'E-Mail'))
            ->add('web_site')
            ->add('proveedor_mapas', ChoiceType::class, array(
                'label' => '',
                'expanded' => false,
                'choices' => array(
                    'security' => 'Security',
                    'google' => 'GoogleMaps'
                )
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array(
                    '0' => 'Sin Limite',
                    '10' => '10 días',
                    '30' => '30 días',
                    '180' => '6 meses',
                )
            ))
            ->add('configuracionDistribuidor', new DistribuidorConfigType());
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
        ));
    }

    public function getName()
    {
        return 'distribuidor';
    }
}
