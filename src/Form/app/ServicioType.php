<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ServicioType extends AbstractType
{

    protected $servicios;
    protected $select;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $this->select = $options['select']; //opcion para poder elegir 1 o varias. si viene 1 solamente 1
        $choice = array();
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => false,
                'multiple' => $this->select != 1 ? true : false,
                'attr' => array(
                    'class' => 'servicio_select'
                )

            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'select' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
