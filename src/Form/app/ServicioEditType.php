<?php

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Form\app\VehiculoType;
use App\Entity\Transporte;
use App\Entity\Satelital;

class ServicioEditType extends AbstractType
{

    protected $strTipoObjetos;
    protected $organizacion;
    protected $tipoObjeto;
    protected $inputs;
    protected $accesorios;
    protected $required_patente;
    protected $required_portal;
    protected $arrayStrMedidorCombustible;
    protected $monitor;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizacion = $options['organizacion'];
        $this->tipoObjeto = $options['tipoObjeto'];
        $this->inputs = $options['inputs'];
        $this->monitor = $options['monitor'];
        $this->accesorios = $options['accesorios'];
        $this->required_patente = isset($options['required_patente']) ? $options['required_patente'] : false;
        $this->required_portal = isset($options['required_portal']) ? $options['required_portal'] : false;
        $this->arrayStrMedidorCombustible = $options['arrayStrMedidorCombustible'];
        //   die('////<pre>'.nl2br(var_export($this->arrayStrMedidorCombustible, true)).'</pre>////');
        foreach ($options['arrayStrTipoObjetos'] as $key => $value) {
            $this->strTipoObjetos[$value] = $key;
        }
        $builder
            ->add('nombre')
            ->add('color', TextType::class, array(
                'label' => 'Color',
                'required' => false,
            ))
            ->add('fecha_alta', TextType::class, array(
                'required' => true,
                'label' => 'Fecha de Alta',
            ))
            ->add('dato_fiscal', TextType::class, array(
                'required' => false,
                'label' => 'Id. Fiscal',
            ))
            ->add('tipoObjeto', ChoiceType::class, array(
                'label' => 'Tipo',
                'choices' => $this->strTipoObjetos
            ))
            ->add('tipoServicio', EntityType::class, array(
                'label' => 'Clase',
                'class' => 'App:TipoServicio',
                'multiple' => false,
                'expanded' => false
            ));
        //        die('////<pre>'.nl2br(var_export($this->accesorios, true)).'</pre>////');
        if (!is_null($this->accesorios)) {
            if (array_key_exists('cortemotor', $this->accesorios) && $this->accesorios['cortemotor'] == true) {
                $builder->add('cortemotor', CheckboxType::class, array(
                    'label' => 'Corte de Motor',
                    'required' => false,
                ));
            }
            if (array_key_exists('botonpanico', $this->accesorios) && $this->accesorios['botonpanico'] == true) {
                $builder->add('botonpanico', CheckboxType::class, array(
                    'label' => 'Botón de Pánico',
                    'required' => false,
                ));
            }
            if (array_key_exists('combustible', $this->accesorios) && $this->accesorios['combustible'] == true) {
                $builder->add('medidorcombustible', CheckboxType::class, array(
                    'label' => 'Medidor Combustible',
                    'required' => false,
                ));
            }
            if (array_key_exists('cortecerrojo', $this->accesorios) && $this->accesorios['cortecerrojo'] == true) {
                $builder->add('cortecerrojo', CheckboxType::class, array(
                    'label' => 'Apert/Cierre Cerrojo',
                    'required' => false,
                ));
            }
            if (array_key_exists('inputs', $this->accesorios) && $this->accesorios['inputs'] == true && count($this->inputs) >= 1) {
                $builder->add('entradasdigitales', CheckboxType::class, array(
                    'label' => 'Entradas Digitales',
                    'required' => false,
                ));
            }
            if (array_key_exists('canbus', $this->accesorios) && $this->accesorios['canbus'] == true) {
                $builder->add('canbus', CheckboxType::class, array(
                    'label' => 'CanBus',
                    'required' => false,
                ));
            }

            if (array_key_exists('horometro', $this->accesorios) && $this->accesorios['horometro'] == true) {
                $builder->add('horometro', CheckboxType::class, array(
                    'label' => 'Horómetro',
                    'required' => false,
                ));
            }

            $builder->add('tipoMedidorCombustible', ChoiceType::class, array(
                'choices' => $this->arrayStrMedidorCombustible
            ));
        }
        if ($this->tipoObjeto == 1) {
            $builder->add('vehiculo', VehiculoType::class, array(
                'required_patente' => $this->required_patente,
                'required_portal' => $this->required_portal,
                'new' => false,
                'organizacion' => $this->organizacion,
            ));
        }
        if ($this->monitor) {
            $builder->add('transporte', EntityType::class, array(
                'class' => Transporte::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->join('t.organizacion', 'o')
                        ->where('o.id = ' . $this->organizacion->getId())
                        ->orderBy('o.nombre', 'ASC');
                }
            ))
                ->add('satelital', EntityType::class, array(
                    'class' => Satelital::class,
                    'required' => false,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->join('s.organizacion', 'o')
                            ->where('o.id = ' . $this->organizacion->getId())
                            ->orderBy('s.nombre', 'ASC');
                    }
                ));
            $builder->add('iscustodio', CheckboxType::class, array(
                'label' => 'Custodio',
                'required' => false,
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Servicio',
            'organizacion' => null,
            'tipoObjeto' => null,
            'inputs' => null,
            'accesorios' => null,
            'arrayStrTipoObjetos' => null,
            'required_patente' => null,
            'required_portal' => null,
            'arrayStrMedidorCombustible' => null,
            'monitor' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
