<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ContactoType extends AbstractType
{

    protected $config_sms;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->config_sms = $options['config_sms'];
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre y Apellido'
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'required' => false,
            ))
            
            ->add('celular', TextType::class, array(
                'label' => 'Número Celular',
                'required' => true,
            )) 
            ->add('cargo', TextType::class, array(
                'label' => 'Cargo/Area',
                'required' => false,
            ))
            ->add('cargo', TextType::class, array(
                'label' => 'Cargo/Area',
                'required' => false,
            ))

            ->add('tipoNotificaciones', EntityType::class, array(
                'required' => false,
                'label' => 'Notificar por',
                'class' => 'App:TipoNotificacion',
                'multiple'=> true
            ))
        
            ->add('noticeme', TextType::class, array(
                'label' => 'ID NoticeMe',
                'required' => false,
            ));
        }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Contacto',
            'config_sms' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'contacto';
    }
}
