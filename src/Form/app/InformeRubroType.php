<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class InformeRubroType extends AbstractType
{

    private $organizacion;
    private $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];
        $this->organizacion = $options['organizacion'];

        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Gráfico' => '2',
                    'Excel' => '5',
                ),
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'required' => false,
                'label' => 'Hasta'
            ))
            ->add('rubro', ChoiceType::class, array(
                'choices' => $this->getRubro(),
                'multiple' => false,
                'required' => false,
                'label' => 'Rubro',
                'attr' => array('class' => 'js-example-basic-single')
            ))
            ->add('centroCosto', ChoiceType::class, array(
                'choices' => $this->getCentroCosto(),
                'multiple' => false,
                'required' => false,
                'label' => 'Centro de Costo',
                'attr' => array('class' => 'js-example-basic-single')
            ));
    }

    private function getRubro()
    {
        $results = $this->em->getRepository('App:Rubro')->findByOrg($this->organizacion);
        $rubros['Todos'] = '0';
        foreach ($results as $r) {
            $rubros[$r->getNombre()] = $r->getId();
        }
        return $rubros;
    }
    private function getCentroCosto()
    {
        $results = $this->em->getRepository('App:CentroCosto')->findByOrg($this->organizacion);
        $centro = array();
        foreach ($results as $centrocosto) {
            $centro[$centrocosto->getNombre()] = $centrocosto->getId();
        }
        return $centro;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informe';
    }
}
