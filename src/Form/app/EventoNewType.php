<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EventoNewType extends AbstractType
{

    private $tnotificweb;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->tnotificweb = $options['tnotificweb'];
        //die('////<pre>'.nl2br(var_export($options, true)).'</pre>////');
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombre', 'required' => true))
            ->add('lunes')
            ->add('martes')
            ->add('miercoles')
            ->add('jueves')
            ->add('viernes')
            ->add('sabado')
            ->add('domingo')
            ->add('horaInicio', TextType::class)
            ->add('horaFin', TextType::class)
            ->add('sonido', CheckboxType::class, array('label' => 'Reproducir Sonido', 'required' => false))
            ->add('registrar', CheckboxType::class, array('label' => 'Pedir Respuesta', 'required' => false))
            ->add('protocolo', TextareaType::class, array('label' => 'Protocolo', 'required' => false))
            ->add('tipoEvento', ChoiceType::class, array(
                'label' => 'Tipo de Evento',
                'required' => false,
                'choices' => $options['teventos']
            ))
            ->add('notificacionWeb', ChoiceType::class, array(
                'label' => 'Tipo de Alerta Web',
                'required' => true,
                //'default' => 0,
                'choices' => $this->tnotificweb
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Evento',
            'teventos' => null,
            'tnotificweb' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'evento';
    }
}
