<?php


namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of ReferenciaType
 *
 * @author yesica
 */
class ReferenciaType extends AbstractType
{

    protected $organizacion;
    protected $propietario;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->propietario = $options['propietario'];
        $this->em = $options['em'];

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre de referencia',
                'required' => true,
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripcion',
                'required' => false,
            ))
            ->add('pathIcono', HiddenType::class, array(
                'required' => false,
            ))
            //->add('poligono', 'hidden')
            ->add('direccion', TextType::class, array(
                'label' => 'Direccion',
                'required' => false,
            ))
            ->add('latitud', NumberType::class, array(
                'label' => 'Latitud',
                'required' => false,
            ))
            ->add('longitud', NumberType::class, array(
                'label' => 'Longitud',
                'required' => false,
            ))
            ->add('radio', IntegerType::class, array(
                'label' => 'Longitud de radio',
                'required' => false,
            ))
            ->add('velocidadMaxima', IntegerType::class, array(
                'label' => 'Velocidad Max en referencia',
                'required' => false,
            ))
            ->add('entrada', CheckboxType::class, array(
                'label' => false,
                'required' => false,
            ))
            ->add('salida', CheckboxType::class, array(
                'label' => false,
                'required' => false,
            ))
            ->add('anguloDeteccion', TextType::class, array(
                'label' => false,
                'required' => false,
            ))
            ->add('tipoReferencia', EntityType::class, array(
                'label' => 'Tipo de referencia',
                'class' => 'App:TipoReferencia',
                'required' => true,
            ))
            ->add('visibilidad', CheckboxType::class, array(
                'label' => 'Mostrar en mapa',
                'required' => false,
                'attr' => array('checked' => true)
            ))
            ->add('kml', FileType::class, array(
                'required' => false,
                'label' => 'Importar archivo KML'
            ))
            ->add('codigoExterno', TextType::class, array(
                'label' => 'Identificador',
                'required' => false,
            ))
            ->add('provincia', EntityType::class, array(
                'label' => 'Provincia',
                'class' => 'App:Provincia',
                'required' => false,
            ))
            ->add('ciudad', TextType::class, array(
                'label' => 'Ciudad',
                'required' => false,
            ));
        if ($this->organizacion == $this->propietario) {
            $builder->add('categoria', EntityType::class, array(
                'label' => 'Categoria',
                'class' => 'App:Categoria',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Categoria')->getQueryCategoriasDisponibles($this->organizacion),
            ));
        }
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Referencia',
            'organizacion' => null,
            'propietario' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
