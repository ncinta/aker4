<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CustodioType extends AbstractType
{

    protected $servicios;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicios = $options['servicios'];
        $builder
            ->add('custodio', ChoiceType::class, array(
                'choices' => $this->servicios,
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                'attr' => array(
                    'class' => 'servicio_select'
                ),
                'choice_value' => function ($value) {
                    return $value;
                },
                'choice_label' => function ($value) {
                    return $value;
                },
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,

        ));
    }

    public function getBlockPrefix()
    {
        return 'custodio';
    }
}
