<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReferenciaSelectType extends AbstractType
{

    protected $provincias;
    protected $referencias;
    protected $select;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->referencias = $options['referencias'];
        $this->provincias = $options['provincias'];
        $this->select = $options['select']; //opcion para poder elegir 1 o varias. si viene 1 solamente 1
        $choice = array();
        $choiceProv = array();
        foreach ($this->referencias as $referencia) {
            $choice[$referencia->getNombre()] = $referencia->getId();
        }
        foreach ($this->provincias as $provincia) {
            $choiceProv[$provincia->getNombre()] = $provincia->getCode();
        }

        $builder
            ->add('referencia', ChoiceType::class, array(
                'choices' => $choice,
                'required' => false,
                'label' => 'Punto de interés',
                'multiple' => $this->select != 1 ? true : false
            ))
            ->add('provincia', ChoiceType::class, array(
                'choices' => $choiceProv,
                'required' => false,
                'label' => 'Provincia',
                'multiple' => false
            ))
            ->add('ciudad', TextType::class, array(
                'label' => 'Ciudad',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'referencias' => null,
            'select' => null,
            'provincias' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
