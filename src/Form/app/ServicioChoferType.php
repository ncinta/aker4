<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ServicioChoferType extends AbstractType
{

    protected $choferes;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->choferes = $options['choferes'];
        $choice = array();
        foreach ($this->choferes as $chofer) {
            $choice[$chofer->getNombre()] = $chofer->getId();
        }

        $builder
            ->add('chofer', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choferes' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio_chofer';
    }
}
