<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EventoHistorialType extends AbstractType
{

    protected $evento_select;
    protected $servicio_select;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->evento_select = $options['evento_select'];
        $this->servicio_select = $options['servicio_select'];

        //die('////<pre>'.nl2br(var_export($this->evento_select, true)).'</pre>////');

        $builder
            ->add('fecha_desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('fecha_hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ));
        if ($options['servicios']) {
            $ch_servicio = array();
            $ch_servicio['Todos'] = 0;
            foreach ($options['servicios'] as $servicio) {
                $ch_servicio[$servicio->getNombre()] = $servicio->getId();
            }
            $builder->add('servicio', ChoiceType::class, array(
                'choices' => $ch_servicio,
                'multiple' => false,
                'preferred_choices' => array($this->servicio_select),
                'attr' => array(
                    'title' => 'Servicio a visualizar',
                    'style' => 'width: 119px;'
                ),
            ));
        }
        if ($options['eventos']) {
            $ch_evento = array();
            $ch_evento['Todos'] = 0;
            foreach ($options['eventos'] as $evento) {
                $ch_evento[$evento->getNombre()] = $evento->getId();
            }
            $builder->add('evento', ChoiceType::class, array(
                'choices' => $ch_evento,
                'multiple' => false,
                'preferred_choices' => array($this->evento_select),
                'attr' => array(
                    'title' => 'Eventos a visualizar',
                    'style' => 'width: 119px;'
                ),
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'eventos' => null,
            'servicios' => null,
            'evento_select' => null,
            'servicio_select' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'historial';
    }
}
