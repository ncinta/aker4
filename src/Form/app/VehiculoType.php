<?php

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\app\VehiculoDatoExtraType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Model\app\VehiculoModeloManager;

class VehiculoType extends AbstractType
{

    // protected $em;
    protected $organizacionId;
    protected $requiredTanque;
    protected $organizacion;
    protected $vehiculoModeloManager;

    public function __construct(VehiculoModeloManager $vManager)
    {
        $this->vehiculoModeloManager = $vManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->required_patente = $options['required_patente'];
        $this->required_portal = $options['required_portal'];
        $this->new = $options['new'];
        $this->organizacion = $options['organizacion'];

        $this->organizacionId = $options['organizacion']->getId();
        $this->requiredTanque = $options['requiredTanque'];
        //die('////<pre>' . nl2br(var_export($options['organizacion']->getId(), true)) . '</pre>////');

        $builder
            ->add('patente', TextType::class, array(
                'required' => $this->required_patente,
            ))
            ->add('modelo')
            ->add('color')
            ->add('marca')
            ->add('rendimiento', TextType::class, array(
                'required' => false,
                'label' => 'Rendimiento lts/100km'
            ))
            ->add('rendimientoHora', TextType::class, array(
                'required' => false,
                'label' => 'Rendimiento lts/hora'
            ))
            ->add('litrosTanque', IntegerType::class, array(
                'required' => $this->requiredTanque ? true : false,
                'label' => 'Capacidad en Tanque (lts)'
            ))
            ->add('anioFabricacion', IntegerType::class, array(
                'required' => false,
                'label' => 'Año'
            ))
            ->add('numeroMotor', TextType::class, array(
                'required' => false,
                'label' => 'Número de Motor'
            ))
            ->add('numeroChasis', TextType::class, array(
                'required' => false,
                'label' => 'Número de Chasis'
            ))
            ->add('cedulaVerde', TextType::class, array(
                'required' => false,
                'label' => 'Número de Cedula Verde'
            ))
            ->add('carModelo', EntityType::class, array(
                'choices' => $this->getCarModelo(),
                'required' => false,
                'multiple' => false,
                //'required' => true,
                'class' => \App\Entity\VehiculoModelo::class,
                'label' => 'Ficha Técnica',
            ));
        if ($this->required_portal) {
            $builder->add('tipoVehiculo', EntityType::class, array(
                'required' => false,
                'label' => 'Tarifa de Portico/Peaje',
                'class' => 'App:TipoVehiculo',
            ));
        }
        if ($this->new) {
            $builder->add('datosExtra', CollectionType::class, array(
                'entry_type' => VehiculoDatoExtraType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'prototype' => true,
                'required' => false
            ));
        }
    }

    private function getCarModelo()
    {
        $results = $this->vehiculoModeloManager->findByOrganizacion($this->organizacion);
        return $results;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Vehiculo',
            'required_patente' => null,
            'required_portal' => null,
            'organizacion' => null,
            'new' => null,
            'em' => null,
            'requiredTanque' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'appbundle_vehiculotype';
    }
}
