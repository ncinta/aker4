<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Form\user\UsuarioMasterType as UsuarioMasterType;
use App\Form\dist\DistribuidorConfigType as DistribuidorConfigType;

class DistribuidorNewType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('direccion')
            ->add('telefono_oficina', 'text', array('label' => 'Teléfono'))
            ->add('email', 'email', array('label' => 'E-Mail'))
            ->add('web_site')
            ->add('proveedor_mapas', 'choice', array(
                'label' => '',
                'expanded' => false,
                'choices' => array(
                    'security' => 'Security',
                    'google' => 'GoogleMaps'
                )
            ))
            ->add('limite_historial', 'choice', array(
                'choices' => array(
                    '0' => 'Sin Limite',
                    '10' => '10 días',
                    '30' => '30 días',
                    '180' => '6 meses',
                )
            ))
            ->add('usuario_master', new UsuarioMasterType($this->opt))
            ->add('configuracionDistribuidor', new DistribuidorConfigType());
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
        ));
    }

    public function getName()
    {
        return 'distribuidor';
    }
}
