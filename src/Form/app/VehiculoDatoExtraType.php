<?php

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class VehiculoDatoExtraType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoDatoExtra', EntityType::class, array(
                'label' => 'Tipo de Dato',
                'required' => true,
                'class' => 'App\Entity\TipoDatoExtra',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('td')
                        ->orderBy('td.nombre', 'ASC');
                },
            ))
            ->add('dato', TextType::class, array(
                'label' => 'Descripción',
                'required' => true
            ))
            ->add('nota', TextareaType::class, array(
                'label' => 'Nota',
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\VehiculoExtra',
        ));
    }

    public function getBlockPrefix()
    {
        return 'vehiculoextra';
    }
}
