<?php


namespace App\Form\app;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of HistoricoItinerarioType
 *
 * @author nicolas
 */
class FlotaType extends AbstractType
{

    protected $tipoServico = null;
    protected $claseServicio = null;
    protected $empresas = null;
    protected $transportes = null;
    protected $satelitales = null;
    protected $moduloMonitor = false;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->claseServicio = $options['claseServicio'];
        $this->empresas = $options['empresas'];
        $this->transportes = $options['transportes'];
        $this->satelitales = $options['satelitales'];
        $this->moduloMonitor = $options['moduloMonitor'];

        $choice_claseServicio = array();
        $choice_transportes = array();
        $choice_empresas = array();
        $choice_satelitales = array();

        $choice_tipoServicio = $options['arrayStrTipoObjetos'];
        $choice_estado = $options['arrayStrEstado'];

        if (count($this->claseServicio) > 0) {
            $choice_claseServicio['Todos'] = 0;
            foreach ($this->claseServicio as $tipo) {
                $choice_claseServicio[$tipo->getNombre()] = $tipo->getId();
            }
        }
        //die('////<pre>'.nl2br(var_export($choice_tipoServicio, true)).'</pre>////');

        if (count($this->transportes) > 0) {
            $choice_transportes['Todos'] = 0;
            foreach ($this->transportes as $transporte) {
                $choice_transportes[$transporte->getNombre()] = $transporte->getId();
            }
        }

        if (count($this->empresas) > 0) {
            $choice_empresas['Todos'] = 0;
            foreach ($this->empresas as $empresa) {
                $choice_empresas[$empresa->getNombre()] = $empresa->getId();
            }
        }

        if (count($this->satelitales) > 0) {
            $choice_satelitales['Todos'] = 0;
            foreach ($this->satelitales as $satelital) {
                $choice_satelitales[$satelital->getNombre()] = $satelital->getId();
            }
        }

        $builder
            ->add('tipoServicio', ChoiceType::class, array(
                'choices' => array_flip($choice_tipoServicio),
                'multiple' => false,
                'required' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('claseServicio', ChoiceType::class, array(
                'choices' => $choice_claseServicio,
                'multiple' => false,
                'required' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ))
            ->add('estado', ChoiceType::class, array(
                'choices' => $choice_estado,
                'multiple' => false,
                'required' => false,
                'attr' => array(
                    'style' => 'width: 120px;'
                )
            ));
        if ($this->moduloMonitor) {
            $builder
                ->add('transporte', ChoiceType::class, array(
                    'choices' => $choice_transportes,
                    'multiple' => false,
                    'attr' => array(
                        'style' => 'width: 120px;'
                    )
                ))
                ->add('satelital', ChoiceType::class, array(
                    'choices' => $choice_satelitales,
                    'multiple' => false,
                    'attr' => array(
                        'style' => 'width: 120px;'
                    )
                ))
                ->add('empresa', ChoiceType::class, array(
                    'choices' => $choice_empresas,
                    'multiple' => false,
                    'attr' => array(
                        'style' => 'width: 120px;'
                    )
                ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'empresas' => null,
            'transportes' => null,
            'satelitales' => null,
            'claseServicio' => null,
            'moduloMonitor' => null,
            'arrayStrTipoObjetos' => null,
            'arrayStrEstado' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'flota';
    }
}
