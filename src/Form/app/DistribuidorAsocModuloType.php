<?php

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DistribuidorAsocModuloType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('modulos', EntityType::class, array(
                'class' => 'App:Modulo',
                'multiple' => true,
                'expanded' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
        ));
    }

    public function getName()
    {
        return 'distribuidorasociarmodulo';
    }
}
