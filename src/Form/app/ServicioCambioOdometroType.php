<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\app;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ServicioCambioOdometroType extends AbstractType
{

    protected $servicio;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->servicio = $options['servicio'];
        $builder
            ->add('odometro', IntegerType::class, array(
                'label' => 'Odómetro de Tablero',
                'required' => true,
            ))
            ->add('fecha', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario'
                ),
                'required' => true,
                'label' => 'Fecha lectura tablero'
            ))
            ->add('horas', IntegerType::class, array(
                'label' => 'Horas',
                'required' => true,
            ))
            ->add('minutos', IntegerType::class, array(
                'label' => 'Minutos',
                'required' => true,
            ));
        //        if ($this->servicio->getEquipo()->getModelo()->getTipoProgramacion() != 1) {
        //1 es carfinder, por lo que para otros equipos se debe poner la fecha en la 
        //que se hizo el cambio del odometro para que calcule los kms recorridos.
        //        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicio' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'servicio';
    }
}
