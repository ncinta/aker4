<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MetricaType
 *
 * @author nicolas
 */
class MetricaType extends AbstractType
{

    protected $organizaciones;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizaciones = $options['organizaciones'];
        $choice['Todas las organizaciones'] = 0;

        foreach ($this->organizaciones as $org) {          
                $choice[$org->getNombre() . ($org->getOrganizacionPadre() ? ' (' . $org->getOrganizacionPadre()->getNombre() . ')' : '')] = $org->getId();
            
        }

        $builder
        ->add('desde', TextType::class, array(
            'attr' => array(
                'class' => 'calendario',
                'placeholder' => 'Fecha inicial'
            ),
            'required' => true, 'label' => 'Desde'
        ))
        ->add('hasta',  TextType::class, array(
            'attr' => array(
                'class' => 'calendario',
                'placeholder' => 'Fecha final'
            ),
            'required' => true,
            'label' => 'Hasta'
        ))
            ->add('organizacion', ChoiceType::class, array(
                'label' => 'Distribuidor',
                'choices' => $choice,
                'required' => true,
            ));
          
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'organizaciones' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'metrica';
    }
}
