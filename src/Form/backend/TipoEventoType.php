<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Form\backend\VariableType;
use App\Form\backend\VariableEventoType as VariableEvento;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TipoEventoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['variables']) {
        }
        $builder
            ->add('nombre')
            ->add('codename');

        if ($options['variables']) {
            foreach ($options['variables'] as $value) {
                $var[$value->getNombre()] = $value->getId();
            }
            $builder
                ->add('variables', CollectionType::class, array(
                    'entry_type' => ChoiceType::class,
                    //'entry_type' => VariableEvento::class,
                    'entry_options' => [
                        'label' => false,
                        'choices' => $var,
                    ],
                    'allow_add' => true,
                    'prototype' => true,
                    'required' => false
                ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\TipoEvento',
            'variables' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_Backend_variableeventotype';
    }
}
