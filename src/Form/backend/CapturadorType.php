<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CapturadorType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombre del Capturador', 'required' => true))
            ->add('ip', TextType::class, array(
                'label' => 'IP',
                'required' => true
            ))
            ->add('puerto_trama', NumberType::class, array(
                'label' => 'Puerto de Tramas',
                'required' => true
            ))
            ->add('programador', TextType::class, array(
                'label' => 'Programador asociado',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Capturador',
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_Backend_capturadortype';
    }
}
