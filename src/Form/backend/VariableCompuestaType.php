<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class VariableCompuestaType extends AbstractType
{

    private $variables;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->variables = $options['variables'];
        $vars = array();
        foreach ($this->variables as $value) {
            $vars[$value->getId()] = $value->getNombre() . ' (' . $value->getCodename() . ')';
        }
        //die('////<pre>' . nl2br(var_export($vars, true)) . '</pre>////');
        $builder
            ->add('variables', 'choice', array(
                'label' => 'Variables Asociadas',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'choices' => $vars
            ));
    }

    public function getName()
    {
        return 'variable';
    }
}
