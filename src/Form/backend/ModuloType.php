<?php

/**
 * Este formulario se usa para la creacion y edicion de modulos de una aplicacion
 * @author CGB <cbrandolin@securityconsultant.com.ar>
 */

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ModuloType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('codename')
            ->add('alcance', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '0' => 'Para todos',
                    '1' => 'Para el distribuidor',
                    '2' => 'Para el cliente'
                )),
                'label' => 'Alcance',
                'required' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Modulo',
        ));
    }

    public function getBlockPrefix()
    {
        return 'modulonewtype';
    }
}
