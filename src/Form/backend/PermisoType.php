<?php

/**
 * Este formulario se usa como form embebido para el formulario ModuloNewType para la
 * asignación de permisos.
 * 
 * @author CGB <cbrandolin@securityconsultant.com.ar>
 */

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PermisoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codename', TextType::class, array('label' => 'CodeName', 'required' => true))
            ->add('restringido', TextType::class, array('label' => 'Restringido a', 'required' => false))
            ->add('descripcion', TextType::class, array('label' => 'Descripción', 'required' => true))
            ->add('usable', CheckboxType::class, array('label' => 'Usable por usuario', 'required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Permiso',
        ));
    }

    public function getBlockPrefix()
    {
        return 'permisonewtype';
    }
}
