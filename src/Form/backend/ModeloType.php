<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ModeloType extends AbstractType
{

    private $tprog;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->tprog = $options['tprog'];

        $builder
            ->add('nombre')
            ->add('fabricante')
            ->add('cantidadGeocercas', IntegerType::class, array('label' => 'Máximo de Geocercas ', 'required' => false))
            ->add('tipoProgramacion', ChoiceType::class, array(
                'label' => 'Tipo Programación',
                'choices' => array_flip($this->tprog)
            ))
            ->add('cortemotor', CheckboxType::class, array(
                'label' => 'Corte de Motor',
                'required' => false,
            ))
            ->add('cortecerrojo', CheckboxType::class, array(
                'label' => 'Corte de Cerrojo',
                'required' => false,
            ))
            ->add('botonpanico', CheckboxType::class, array(
                'label' => 'Botón de Pánico',
                'required' => false,
            ))
            ->add('medidorcombustible', CheckboxType::class, array(
                'label' => 'Medidor Combustible',
                'required' => false,
            ))
            ->add('entradasdigitales', CheckboxType::class, array(
                'label' => 'Entradas Digitales',
                'required' => false,
            ))
            ->add('canbus', CheckboxType::class, array(
                'label' => 'CanBus',
                'required' => false,
            ))
            ->add('horometro', CheckboxType::class, array(
                'label' => 'Horómetro',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Modelo',
            'tprog' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_Backend_modelotype';
    }
}
