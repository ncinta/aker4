<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VariableType extends AbstractType
{

    private $tipos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->tipos = $options['tipos'];
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombresss', 'required' => true))
            ->add('usable', CheckboxType::class, array('label' => 'Usable', 'required' => false))
            ->add('codename', TextType::class, array('label' => 'Codename', 'required' => true))
            ->add('cotaMinima', TextType::class, array('label' => 'Cota Mínima', 'required' => false))
            ->add('cotaMaxima', TextType::class, array('label' => 'Cota Maxima', 'required' => false))
            ->add('valorDefault', TextType::class, array('label' => 'Valor Default', 'required' => false))
            ->add('unidadMedida', TextType::class, array('label' => 'Unidad Medida', 'required' => false))
            ->add('role', TextType::class, array('label' => 'Role (ROLE_VAR_)', 'required' => true))
            ->add('tipoVariable', ChoiceType::class, array(
                'label' => 'Tipo Variable',
                'choices' => array_flip($this->tipos)
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Variable',
            'tipos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'variabletype';
    }
}
