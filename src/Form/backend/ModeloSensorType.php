<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ModeloSensorType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombre', 'required' => true))
            ->add('seriado', CheckboxType::class, array('label' => 'Sensor Seriado', 'required' => false))
            ->add('campoTrama', TextType::class, array('label' => 'Campo en Trama', 'required' => false))
            ->add('tipoSensor', ChoiceType::class, array(
                'label' => 'Tipo Sensor',
                'required' => false,
                'choices' => array_flip(array(1 => 'Analogico', 2 => 'Digital'))
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ModeloSensor',
        ));
    }

    public function getBlockPrefix()
    {
        return 'modelosensortype';
    }
}
