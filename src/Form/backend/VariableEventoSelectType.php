<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VariableEventoSelectType extends AbstractType
{

    protected $variables;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->variables = $options['variables'];
        $builder
        ->add('variable', ChoiceType::class, array(
                'choices' => $this->variables,
                'required' => false,
                'multiple'=> true,
         
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'variables' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'variables_evento';
    }
}
