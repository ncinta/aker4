<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TipoVehiculoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo');
    }

    public function getName()
    {
        return 'secur_Backend_tipovehiculotype';
    }
}
