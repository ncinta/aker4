<?php

namespace App\Form\backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class MotivoTxType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, array('label' => 'Nombre', 'required' => true))
            ->add('code', TextType::class, array('label' => 'Codígo de Motivo ', 'required' => true));
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\MotivoTx',
        ));
    }

    public function getBlockPrefix()
    {
        return 'secur_Backend_motivotxtype';
    }
}
