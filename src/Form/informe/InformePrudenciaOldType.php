<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class InformePrudenciaOldType extends AbstractType
{

    protected $servicios;
    protected $grupos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];

        $choice = array();
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    //'3' => 'Mapa',
                ),
                'required' => true,
            ))
            ->add('max_ruta', TextType::class, array(
                'required' => true,
                'label' => 'Máxima en Ruta',
                'data' => 110
            ))
            ->add('max_avenida', TextType::class, array(
                'required' => true,
                'label' => 'Máxima en Avenida',
                'data' => 60
            ))
            ->add('mostrar_totales', CheckboxType::class, array(
                'label' => 'Mostrar totales',
                'required' => false,
                'attr' => array(
                    'checked' => true,
                )
            ))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'label' => 'Direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en donde se produjeron las detenciones.',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeprudencia';
    }
}