<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Description of InformeConsolidadoType
 *
 * @author nicolas
 */
class InformeConsolidadoType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $metricaManager;

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            $choice[$servicio->getNombre()] = $servicio->getId();
        }
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('tiempo_minimo', NumberType::class, array(
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'Tiempo mínimo (min.)'
                ),
                'required' => true,
                'label' => 'Tiempo mínimo'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    'Mapa' => '6',
                ),
                'required' => true,
            ))
            //                ->add('mostrar_totales', 'checkbox', array(
            //                    'label' => 'Mostrar totales',
            //                    'required' => false,
            //                    'attr' => array(
            //                        'checked' => true,
            //            )))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'label' => 'Direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en donde se produjeron las detenciones.',
                    'onclick' => 'if (document.getElementById("informedetenciones_mostrar_direcciones").checked) {
                                   alert("Si elige mostrar las direcciones puede hacer que la obtención de datos sea lenta.");
                                };',
                )
            ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-consolidado');
    
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeconsolidado';
    }
}
