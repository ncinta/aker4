<?php

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class InformeResponsabilidadV2Type extends AbstractType
{

    protected $choferes;
    protected $referencias;
    protected $grpreferencias;
    protected $metricaManager;


    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);

        $this->choferes = $options['choferes'];
        $choiceChofer = ['Todos los choferes' => '0' ];
        
        foreach ($this->choferes as $chofer) {
            $choiceChofer[$chofer->getNombre()] = $chofer->getId();
        }

        $this->grpreferencias = $options['grupos_referencias'] != null ? $options['grupos_referencias'] : $options['gruposRef'];
        $this->referencias = $options['referencias'];


        $grpRef['No considerar Referencias'] = -1;
        $grpRef['En todas las referencias.'] = '0';
        foreach ($this->grpreferencias as $grp) {
            $grpRef['Grupo: ' . $grp->getNombre()] = 'g' . $grp->getId();
        }

        $grpRef['-----------------'] = '-';
        foreach ($this->referencias as $ref) {
            $grpRef[$ref->getNombre()] = $ref->getId();
        }

        $builder
            ->add('chofer', ChoiceType::class, array(
                'choices' => $choiceChofer,
                'required' => true,
                'multiple' => true,  // Permite selección múltiple
            ))
            ->add('asunto', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control informe_asunto',
                    'placeholder' => 'Breve descripción del informe',
                    'maxlength' => 100 // Limitar la entrada a 100 caracteres
                ),
                'required' => true,
                'label' => 'Asunto'
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'required' => true,
                'label' => 'Grupo de Referencia/s',
                'multiple' => true,  // Permite selección múltiple
                //  'expanded' => true   // Opciones mostradas como checkboxes
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    //'3' => 'Mapa',
                ),
                'required' => true,
            ))
            ->add('max_referencia', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Máx. en Referencia',
                'data' => 60
            ))
            ->add('max_resto', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Máx. fuera Ref.',
                'data' => 110
            ))
            ->add('maxima_por_mapa', CheckboxType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label' => 'Habilitar máxima por mapa',
                'required' => false,
                'attr' => array(
                    'checked' => false,

                )
            ))
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event, 'informe-responsabilidad');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choferes' => null,
            'grupos_referencias' => null, //este es para el informe viejo, sacar en algún momento
            'gruposRef' => null, //informe nuevo
            'referencias' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeresponsabilidadv2';
    }
}
