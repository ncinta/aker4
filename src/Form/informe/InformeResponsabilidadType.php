<?php

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class InformeResponsabilidadType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $referencias;
    protected $grpreferencias;
    protected $metricaManager;


    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);

        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->grpreferencias = $options['grupos_referencias'] != null ? $options['grupos_referencias'] : $options['gruposRef'];
        $this->referencias = $options['referencias'];

        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }
        $grpRef['No considerar Referencias'] = -1;
        $grpRef['En todas las referencias.'] = '0';
        foreach ($this->grpreferencias as $grp) {
            $grpRef['Grupo: ' . $grp->getNombre()] = 'g' . $grp->getId();
        }

        $grpRef['-----------------'] = '-';
        foreach ($this->referencias as $ref) {
            $grpRef[$ref->getNombre()] = $ref->getId();
        }

        $builder
            ->add('asunto', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control informe_asunto',
                    'placeholder' => 'Breve descripción del informe',
                    'maxlength' => 100 // Limitar la entrada a 100 caracteres
                ),
                'required' => true,
                'label' => 'Asunto'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
                'multiple' => true,  // Permite selección múltiple
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'required' => true,
                'label' => 'Grupo de Referencia/s',
                'multiple' => true,  // Permite selección múltiple
              //  'expanded' => true   // Opciones mostradas como checkboxes
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    //'3' => 'Mapa',
                ),
                'required' => true,
            ))
            ->add('max_referencia', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Máx. en Referencia',
                'data' => 60
            ))
            ->add('max_resto', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Máx. fuera Ref.',
                'data' => 110
            ))
            ->add('maxima_por_mapa', CheckboxType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label' => 'Habilitar máxima por mapa',
                'required' => false,
                'attr' => array(
                    'checked' => false,

                )
            ))
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event, 'informe-responsabilidad');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'grupos_referencias' => null, //este es para el informe viejo, sacar en algún momento
            'gruposRef' => null, //informe nuevo
            'referencias' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeresponsabilidad';
    }
}
