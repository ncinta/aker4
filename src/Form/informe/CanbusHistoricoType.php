<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CanbusHistoricoType extends AbstractType
{

    protected $servicios;
    protected $metricaManager;

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->servicios = $options['servicios'];
        $choice = array();
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }
        //dd($options['variablesCan']);
               
        $builder
            ->add('variables', ChoiceType::class, array(
                'choices' => $options['variablesCan'],
                'multiple' => true,
                'expanded' => true, // Si quieres que se muestre como checkboxes
                'required' => false,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta',  TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Gráficos' => '2',
                    //'Mapa' => '3',
                    //'Excel' => '5',
                ),
                'required' => true,
            ));
    }


    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-canbus-historico');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'variablesCan' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'informecanbushistorico';
    }
}
