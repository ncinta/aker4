<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformeExcesosVelocidadChoferType extends AbstractType
{

    protected $servicios;
    protected $grupoReferencias;
    protected $grupos;
    protected $choferes;
    private $metricaManager;
   

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->servicios = $options['servicios'];
        $this->grupoReferencias = $options['grupos_referencias'];
        $this->grupos = $options['grupos'];
        $this->choferes = $options['choferes'];

        $choice['Toda la flota'] = 0;
        $choiceChofer['Todos los Choferes'] = 0;

        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        foreach ($this->choferes as $chofer) {
            $choiceChofer[$chofer->getNombre()] = $chofer->getId();
        }

        $grpRef['No considerar referencias.'] = '0';
        $grpRef['En todas las referencias.'] = '-1';
        foreach ($this->grupoReferencias as $grp) {
            $grpRef[$grp->getId()] = 'Sólo en ' . $grp->getNombre();
        }
        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                ),
                'required' => true,
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'required' => true,
                'label' => 'Grupo de Referencias'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('chofer', ChoiceType::class, array(
                'choices' => $choiceChofer,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('velocidad_maxima', NumberType::class, array(
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'Velocidad máxima'
                ),
                'required' => true,
                'label' => 'Velocidad maxima'
            ))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'label' => 'Mostrar direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en posición donde se produjo el exceso de velocidad.',
                    'onclick' => 'if (document.getElementById("informeexcesosvelocidad_mostrar_direcciones").checked) {
                                   alert("Si elige mostrar las direcciones puede hacer que la obtención de datos sea lenta.");
                                };',
                )
            ))
            ->add('solo_referencias', CheckboxType::class, array(
                'label' => 'Sólo en referencias',
                'required' => false,
                'attr' => array(
                    'title' => 'Considera los excesos solo en los grupos de referencias seleccionadas.',
                    'onclick' => 'checkSolo();',
                )
            ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-excesos-chofer');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos_referencias' => null,
            'grupos' => null,
            'choferes' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeexcesosvelocidad';
    }
}
