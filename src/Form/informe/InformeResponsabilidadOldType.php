<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class InformeResponsabilidadOldType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $referencias;
    protected $grpreferencias;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->grpreferencias = $options['grupos_referencias'];
        $this->referencias = $options['referencias'];

        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }
        $grpRef['En todas las referencias.'] = '-1';
        foreach ($this->grpreferencias as $grp) {
            $grpRef['Grupo: ' . $grp->getNombre()] = '*' . $grp->getId();
        }
        $grpRef['-----------------'] = '-';
        foreach ($this->referencias as $ref) {
            $grpRef[$ref->getNombre()] = $ref->getId();
        }
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'required' => true,
                'label' => 'Referencia/s'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    //'3' => 'Mapa',
                ),
                'required' => true,
            ))
            ->add('max_referencia', IntegerType::class, array(
                'required' => true,
                'label' => 'Máx. en Referencia',
                'data' => 60
            ))
            ->add('max_resto', IntegerType::class, array(
                'required' => true,
                'label' => 'Máx. fuera Ref.',
                'data' => 110
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'grupos_referencias' => null,
            'referencias' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeresponsabilidad';
    }
}