<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Entity\Metrica;
use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformeDistanciasType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $calcular_distancias;
    private $metricaManager;
   

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);

        $this->servicios = $options['servicios'];
        $this->grupos = $options['grupos'];
        $this->calcular_distancias = $options['calcular_distancias'];
        $choice['Todos los equipos'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }
        //die('////<pre>' . nl2br(var_export($choice, true)) . '</pre>////');
        $builder
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,

            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Gráfico pantalla' => '2',
                    'Excel' => '5',
                ),
                'required' => true,

            ))
            ->add('mostrar_desglosado', CheckboxType::class, array(
                'label' => 'Desglosar por día',
                'required' => false,
                'attr' => array(
                    'title' => 'En caso de pedir por varios dias, lo muestra día por día en forma separada.',
                )
            ))
            ->add('mostrar_totales', CheckboxType::class, array(
                'label' => 'Mostrar totales',
                'required' => false,
                'attr' => array(
                    'checked' => true,
                )
            ));
        if ($this->calcular_distancias) {
            $builder->add('calcular_distancias', CheckboxType::class, array(
                'label' => 'Cálculo por puntos',
                'required' => false,
                //                'attr' => array(
                //                    'checked' => false,
            ));
        }
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-distancias');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'calcular_distancias' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'informedistancias';
    }
}
