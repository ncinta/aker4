<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformePrudenciaType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $gruposRef;
    protected $referencias;
    private $metricaManager;

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);

        $this->servicios = $options['servicios'];
        $this->referencias = $options['referencias'];
        $this->grupos = $options['grupos'];
        $this->gruposRef = $options['gruposRef'];

        $choice = array();
        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        $choiceRef = array();
        $choiceRef['No considerar Referencias'] = -1;
        $choiceRef['Todas las Referencias'] = 0;
        foreach ($this->gruposRef as $grupo) {
            $choiceRef['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->referencias as $referencia) {
            $choiceRef[$referencia->getNombre()] = $referencia->getId();
        }

        $builder
            ->add('servicio', ChoiceType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'choices' => $choice,
                'required' => true,
                'multiple' => true,
            ))
            ->add('referencia', ChoiceType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'choices' => $choiceRef,
                'required' => true,
                'multiple' => true,
            ))
            ->add('asunto', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control informe_asunto',
                    'placeholder' => 'Breve descripción del informe',
                    'maxlength' => 100 // Limitar la entrada a 100 caracteres
                ),
                'required' => true,
                'label' => 'Asunto'
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Hasta'
            ))

            ->add('max_ruta', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Máxima en Ruta',
                'data' => 110
            ))
            ->add('max_avenida', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required' => true,
                'label' => 'Máxima en Avenida',
                'data' => 60
            ))
            ->add('mostrar_totales', CheckboxType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label' => 'Mostrar totales',
                'required' => false,
                'attr' => array(
                    'checked' => true,
                )
            ))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label' => 'Direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en donde se produjeron las detenciones.',
                )
            ))
            ->add('maxima_por_mapa', CheckboxType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label' => 'Habilitar máxima por mapa',
                'required' => false,
                'attr' => array(
                    'checked' => false,
                    
                )
            ))
            ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event, 'informe-prudencia');
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'referencias' => null,
            'gruposRef' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeprudencia';
    }
}
