<?php

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Description of IndiceMantenimientoType
 *
 * @author nicolas
 */
class IndiceMantenimientoType extends AbstractType
{

    protected $organizacion;
    protected $em;
    protected $metricaManager;

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];
        $builder
            ->add('desde',  TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $this->getServicios(),
                'multiple' => false,
                'required' => true,
                'label' => 'Grupo de Servicios / Servicio',


            ))
            ->add('centro_costo', ChoiceType::class, array(
                'choices' => $this->getCentrosCosto(),
                'multiple' => true,
                'required' => true,
                'label' => 'Centro de Costo',


            ));
    }

    private function getCentrosCosto()
    {
        $cc = $this->em->getRepository('App:CentroCosto')->findBy(array('organizacion' => $this->organizacion->getId()), array('nombre' => 'asc'));
        $choice = array();
        $choice['Todos los centros de costo'] = 0;
        foreach ($cc as $centroCosto) {
            $choice['Centro de Costo'][$centroCosto->getNombre()] = 'c' . $centroCosto->getId();
        }
        return $choice;
    }
    private function getServicios()
    {
        $gs = $this->em->getRepository('App:GrupoServicio')->findBy(array('organizacion' => $this->organizacion->getId()), array('nombre' => 'asc'));
        $servicios = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($this->organizacion);
        $choice = array();
        $choice['Toda la flota'] = 0;

        foreach ($gs as $grupoServicio) {
            $choice['Grupo de Servicio'][$grupoServicio->getNombre()] = 'g' . $grupoServicio->getId();
        }
        foreach ($servicios as $servicio) {
            $choice['Servicio'][$servicio->getNombre()] = $servicio->getId();
        }

        //ksort($choice);
        return $choice;
    }

    
    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-indice-mantenimiento');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
        ));
    }



    public function getBlockPrefix()
    {
        return 'indice';
    }
}
