<?php

/*
Informe nuevo, cuando se deje de usar el viejo renombrar este
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformeExcesosVelocidadType extends AbstractType
{

    protected $servicios;
    protected $grupos;
    protected $gruposRef;
    protected $referencias;
    private $metricaManager;
   

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->servicios = $options['servicios'];
        $this->referencias = $options['referencias'];
        $this->grupos = $options['grupos'];
        $this->gruposRef = $options['gruposRef'];

        $choice = array();
        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        $choiceRef = array();
        $choiceRef['No considerar Referencias'] = -1;
        $choiceRef['Todas las Referencias'] = 0;
        foreach ($this->gruposRef as $grupo) {
            $choiceRef['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->referencias as $referencia) {
            $choiceRef[$referencia->getNombre()] = $referencia->getId();
        }

        $builder
          
            ->add('referencia', ChoiceType::class, array(
                'choices' => $choiceRef,
                'required' => true,
                'multiple' => true,
                'label' => 'Grupo de Referencias'
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
                'multiple' => true,
            ))
            ->add('asunto', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control informe_asunto',
                    'placeholder' => 'Breve descripción del informe',
                    'maxlength' => 100 // Limitar la entrada a 100 caracteres
                ),
                'required' => true,
                'label' => 'Asunto'
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('velocidad_maxima', NumberType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Velocidad máxima'
                ),
                'required' => true,
                'label' => 'Velocidad maxima'
            ))
            ->add('mostrar_direcciones', CheckboxType::class, array(
                'label' => 'Mostrar direcciones',
                'required' => false,
                'attr' => array(
                    'title' => 'Muestra las direcciones en posición donde se produjo el exceso de velocidad.',
                    'onclick' => 'if (document.getElementById("informeexcesosvelocidad_mostrar_direcciones").checked) {
                                   alert("Si elige mostrar las direcciones puede hacer que la obtención de datos sea lenta.");
                                };',
                )
                ))
                ->add('maxima_por_mapa', CheckboxType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label' => 'Habilitar máxima por mapa',
                    'required' => false,
                    'attr' => array(
                        'checked' => false,
                        
                    )
                ))
                ;
            // ->add('solo_referencias', CheckboxType::class, array(
            //     'label' => 'Sólo en referencias',
            //     'required' => false,
            //     'attr' => array(
            //         'title' => 'Considera los excesos solo en los grupos de referencias seleccionadas.',
            //         'onclick' => 'checkSolo();',
            //     )
            // ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-excesos');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'grupos' => null,
            'referencias' => null,
            'gruposRef' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeexcesosvelocidad';
    }
}
