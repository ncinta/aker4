<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformeEstadiaType extends AbstractType
{

    protected $servicios;
    protected $referencias;
    protected $grpreferencias;
    protected $grupos;
    private $metricaManager;
   

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->servicios = $options['servicios'];
        $this->referencias = $options['referencias'];
        $this->grpreferencias = $options['grupos_referencias'];
        $this->grupos = $options['grupos'];

        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        //$grpRef['0'] = 'No considerar referencias.';
        $grpRef['En todas las referencias.'] = '-1';
        foreach ($this->grpreferencias as $grp) {
            $grpRef['Grupo: ' . $grp->getNombre()] = '*' . $grp->getId();
        }
        $grpRef['-----------------'] = '-';
        foreach ($this->referencias as $ref) {
            $grpRef[$ref->getNombre()] = $ref->getId();
        }

        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                    //'6' => 'CSV',
                ),
                'required' => true,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'required' => true,
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'required' => true,
                'label' => 'Referencia/s'
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('tiempo_minimo', NumberType::class, array(
                'attr' => array(
                    'class' => 'span2',
                    'placeholder' => 'Tiempo en la Referencia (min.)'
                ),
                'required' => true,
                'label' => 'Tiempo mínimo en la Referencia'
            ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-estadia');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'referencias' => null,
            'grupos_referencias' => null,
            'grupos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeestadia';
    }
}
