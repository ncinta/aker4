<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class InformeManejoType extends AbstractType
{

    protected $choferes;
    protected $servicios;
    protected $grupos;
    private $metricaManager;
   

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
        
        $this->choferes = $options['choferes'];
        $this->grupos = $options['grupos'];
        $this->servicios = $options['servicios'];

        $choiceServ['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choiceServ['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choiceServ[$servicio->getNombre()] = $servicio->getId();
            }
        }
        $choice['Todos los choferes'] = 0;

        foreach ($this->choferes as $chofer) {
            $choice[$chofer->getNombre()] = $chofer->getId();
        }
        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    'Excel' => '5',
                ),
                'required' => true,
            ))
            ->add('chofer', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choiceServ,
                'multiple' => false,
            ))
            ->add('desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-manejo');
    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choferes' => null,
            'grupos' => null,
            'servicios' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'manejo';
    }
}
