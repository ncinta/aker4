<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\informe;

use App\Model\app\MetricaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AgroReferenciaType extends AbstractType
{

    protected $servicios;
    protected $referencias;
    protected $grpreferencias;
    protected $grupos;
    protected $metricaManager;

    public function __construct(MetricaManager $metricaManager)
    {
        $this->metricaManager = $metricaManager;
       
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);

        $this->servicios = $options['servicios'];
        $this->referencias = $options['referencias'];
        $this->grpreferencias = $options['grupos_referencias'];
        $this->grupos = $options['grupos'];

        $choice['Toda la flota'] = 0;
        foreach ($this->grupos as $grupo) {
            $choice['Grp: ' . $grupo->getNombre()] = 'g' . $grupo->getId();
        }
        foreach ($this->servicios as $servicio) {
            if ($servicio->getEquipo() != null) {
                $choice[$servicio->getNombre()] = $servicio->getId();
            }
        }

        //$grpRef['0'] = 'No considerar referencias.';
        $grpRef['En todas las referencias.'] = '-1';
        foreach ($this->grpreferencias as $grp) {
            $grpRef['Grupo: ' . $grp->getNombre()] = '*' . $grp->getId();
        }
        $grpRef['-----------------'] = '-';
        foreach ($this->referencias as $ref) {
            $grpRef[$ref->getNombre()] = $ref->getId();
        }

        $builder
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    //'5' => 'Excel',
                    //'6' => 'CSV',
                ),
                'attr' => array('class' => 'span3'),
                'required' => true,
            ))
            ->add('servicio', ChoiceType::class, array(
                'choices' => $choice,
                'attr' => array('class' => 'span3'),
                'required' => true,
            ))
            ->add('referencia', ChoiceType::class, array(
                'choices' => $grpRef,
                'attr' => array('class' => 'span3'),
                'required' => true,
                'label' => 'Referencia/s'
            ))
            ->add('melga', IntegerType::class, array(
                'label' => 'Tolerancia',
                'required' => true,
            ))
            ->add('ancho', NumberType::class, array(
                'label' => 'Ancho trabajo (mts)',
                'required' => false,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ));
    }

    public function onPostSubmit(FormEvent $event)
    {
        $metrica = $this->metricaManager->setDataInforme($event,'informe-agro-referencia');
    
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'servicios' => null,
            'referencias' => null,
            'grupos_referencias' => null,
            'grupos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informeref';
    }
}
