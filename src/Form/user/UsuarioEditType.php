<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\user;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioEditType extends AbstractType
{

    protected $paises;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->paises = $options['paises'];
        $organizacion = $options['organizacion'];
        $builder
            ->add('nombre', TextType::class, array(
                'required' => true,
                'label' => 'Apellido y Nombre'
            ))
            ->add('username', TextType::class, array(
                'required' => true,
                'label' => 'Nombre Usuario'
            ))
            ->add('email', EmailType::class, array(
                'required' => true,
                'label' => 'Email'
            ))
            ->add('telefono', TextType::class, array(
                'required' => false,
                'label' => 'Teléfono'
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Habilitado',
                'required' => false
            ))
            ->add('change_password', CheckboxType::class, array(
                'label' => 'Cambiar Contraseña',
                'required' => false
            ))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => $this->paises
            ))
            ->add('vista_mapa', ChoiceType::class, array(
                'label' => 'Vista de Mapas',
                'expanded' => false,
                'choices' => array_flip(array(
                    '1' => 'Vista de Mapa y Satelite',
                    '2' => 'Vista de Mapa',
                    '3' => 'Vista de Satelite',
                ))
            ))
            ->add('showReferencias', CheckboxType::class, array(
                'label' => 'Mostrar Referencias en Mapas',
                'required' => false
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array_flip(
                    array(
                        '0' => 'Sin Limite',
                        '7' => '7 días',
                        '30' => '30 días',
                        '180' => '6 meses',
                    )
                )
            ));
        if ($organizacion->getTipoOrganizacion() == 2) {
            $builder->add(
                'redirigir_login',
                ChoiceType::class,
                array(
                    'label' => 'Redirigir en login',
                    'expanded' => false,
                    'empty_data' => 0,
                    'choices' => array(
                        'No redirigir' => '0',
                        'A Ver Flota' =>  '1',
                    )
                )
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Usuario',
            'paises' => null,
            'organizacion' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'usuario';
    }
}
