<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\user;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UsuarioMasterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //die('////<pre>' . nl2br(var_export($paises, true)) . '</pre>////');
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre y Apellido',
                'required' => true
            ))
            ->add('username', TextType::class, array(
                'label' => 'Nombre de Login'
            ))
            ->add('email', EmailType::class, array('required' => true))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Los campos de contraseña deben coincidir.',
                'first_name' => 'first',
                'second_name' => 'second',
                'options' => array(
                    'label' => 'Clave',
                    'required' => true
                )
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Habilitado'
            ))
            //                ->add('timeZone', ChoiceType::class, array(
            //                    'label' => 'Zona Horaria',
            //                    'required' => true,
            //                    'choices' => $options['paises'],
            //                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Usuario',
            'calypso.pais' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'usuario_master';
    }
}
