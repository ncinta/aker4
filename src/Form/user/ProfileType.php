<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\user;

use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{

    protected $paises;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->paises = $options['paises'];
        $usuario = $options['usuario'];
        $builder
            ->add('nombre', TextType::class, array(
                'required' => true,
                'label' => 'Apellido y Nombre'
            ))
            ->add('username', TextType::class, array(
                'required' => true,
                'label' => 'Nombre Usuario',
                'disabled' => true
            ))
            ->add('email', EmailType::class, array(
                'required' => true,
                'label' => 'Email'
            ))
            // ->add('plainPassword', RepeatedType::class, array('type' => 'password', 'options' => array('required' => true)))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => array_flip($this->paises),
            ))
            ->add(
                'vista_mapa',
                ChoiceType::class,
                array(
                    'label' => 'Vista de Mapas',
                    'expanded' => false,
                    'choices' => array(
                        'Vista de Mapa y Satelite' => 1,
                        'Vista de Mapa' => 2,
                        'Vista de Satelite' => 3,
                    )
                )
            )
            ->add('showReferencias', CheckboxType::class, array(
                'label' => 'Mostrar Referencias en Mapas',
                'required' => false
            ));
        if ($usuario->getOrganizacion()->getTipoOrganizacion() == 2) {
            $builder->add(
                'redirigir_login',
                ChoiceType::class,
                array(
                    'label' => 'Redirigir en login',
                    'expanded' => false,
                    'empty_data' => 0,
                    'choices' => array(
                        'No redirigir' => '0',
                        'A Ver Flota' =>  '1',
                    )
                )
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Usuario',
            'paises' => null,
            'usuario' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'profile';
    }
}
