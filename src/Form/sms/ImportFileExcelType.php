<?php

namespace App\Form\sms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportFileExcelType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('archivo', \Symfony\Component\Form\Extension\Core\Type\FileType::class, array(
                'required' => true,
                'label' => 'Archivo Excel',
            ));
    }

    public function getName()
    {
        return 'fileimportexcel';
    }
}
