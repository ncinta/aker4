<?php

namespace App\Form\sms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SmsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('destino', 'text', array(
                'required' => true,
                'max_length' => 10
            ))
            ->add('message', 'textarea', array(
                'required' => true,
                'max_length' => 160
            ));
    }

    public function getName()
    {
        return 'secur_smsbundle_smstype';
    }
}
