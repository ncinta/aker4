<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class InformeSinReportesType extends AbstractType
{

    protected $organizaciones;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->organizaciones = $options['organizaciones'];
        $choice['Todas las organizaciones'] = 0;
        foreach ($this->organizaciones as $org) {
            if ($org->getTipoOrganizacion() == 1) {
                $choice[$org->getNombre() . ($org->getOrganizacionPadre() ? ' (' . $org->getOrganizacionPadre()->getNombre() . ')' : '')] = $org->getId();
            }
        }
        for ($index = 1; $index < 60; $index++) {
            $dias[] = $index;
        }
        $builder
            ->add('organizacion', ChoiceType::class, array(
                'label' => 'Distribuidor',
                'choices' => $choice,
                'required' => true,
            ))
            ->add('dias', ChoiceType::class, array(
                'label' => 'Días sin reportar',
                'choices' => $dias,
                'required' => true,
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                ),
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'organizaciones' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informesinreportes';
    }
}
