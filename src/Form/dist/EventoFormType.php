<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\dist;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoFormType extends AbstractType
{

    protected $eventos;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->eventos = $options['eventos'];
        $ch_evento = array();
        $ch_evento['Todos'] = 0;
        foreach ($this->eventos as  $value) {
            $ch_evento[$value->getNombre()] = $value->getId();
        }
        //die('////<pre>'.nl2br(var_export($ch_evento, true)).'</pre>////');
        $builder
            ->add('fecha_desde', TextType::class, array(
                'required' => true,
                'label' => 'Desde',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('fecha_hasta', TextType::class, array(
                'required' => true,
                'label' => 'Hasta',
                'attr' => array(
                    'style' => 'width: 105px;'
                )
            ))
            ->add('evento', ChoiceType::class, array(
                'choices' => $ch_evento,
                'multiple' => false,
                'attr' => array(
                    'title' => 'Eventos a visualizar',
                    'style' => 'width: 119px;'
                ),
            ))
            ->add('atender', CheckboxType::class, array(
                'label' => 'Eventos Atendidos',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'eventos' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'bitacoraform';
    }
}
