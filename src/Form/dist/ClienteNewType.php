<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use App\Form\user\UsuarioMasterType as UsuarioMasterType;
use App\Form\dist\ClienteExtraType as ClienteExtraType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ClienteNewType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $pais = $options['calypso.pais'];
        //die('////<pre>' . nl2br(var_export($pais->getArrayPaises(), true)) . '</pre>////');
        $builder
            ->add('nombre', TextType::class, array('required' => true))
            ->add('direccion', TextType::class, array('required' => false))
            ->add('latitud', TextType::class, array('required' => false))
            ->add('longitud', TextType::class, array('required' => false))
            ->add('telefono_oficina', TextType::class, array('label' => 'Teléfono'))
            ->add('email', EmailType::class, array('label' => 'E-Mail'))
            ->add('dato_fiscal', TextType::class, array(
                'required' => false,
                'label' => 'Identificación Fiscal'
            ))
            ->add('web_site')
            ->add('proveedor_mapas', ChoiceType::class, array(
                'label' => '',
                'expanded' => false,
                'choices' => array(
                    'Security' => 'security',
                    'OpenStreetMaps' => 'osm'
                )
            ))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => array_flip($pais),
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '0' => 'Sin Limite',
                    '10' => '10 días',
                    '30' => '30 días',
                    '180' => '6 meses',
                ))
            ))

            ->add('usuario_master', UsuarioMasterType::class)
            //->add('usuario_master', new UsuarioMasterType(array('calypso.pais' => $paises)))
            ->add('configuracionDistribuidor', ClienteExtraType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
            'paises' => null,
        ));
        $resolver->setRequired('calypso.pais');
    }

    public function getBlockPrefix()
    {
        return 'cliente';
    }
}
