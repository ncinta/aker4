<?php

/*
 * This file is part of the SecurUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class DistribuidorConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pathLogo', HiddenType::class)
    
            ->add('logo', FileType::class, array(
                'required' => false,
                'label' => 'Archivo de Logo'
            ))
            ->add('copyright', TextType::class, array(
                'label' => 'Texto Copyright',
                'required' => false
            ))
            ->add('css', TextType::class, array(
                'label' => 'Archivo CSS',
                'required' => false
            ))
            ->add('dominio', TextType::class, array(
                'label' => 'Dominio Web',
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ConfiguracionDistribuidor',
        ));
    }

    public function getBlockPrefix()
    {
        return 'configuracionDistribuidor';
    }
}
