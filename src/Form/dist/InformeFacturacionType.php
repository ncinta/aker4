<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class InformeFacturacionType extends AbstractType
{

    protected $distribuidores;
    protected $clientes;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->root = $options['root'];
        $this->distribuidores = $options['distribuidores'];
        $this->clientes = $options['clientes'];

        if (count($this->distribuidores) == 0) {
            $choice[$this->root->getNombre()] = $this->root->getId();
        } else {
            $choice['Todos de ' . $this->root->getNombre()] = '*' . $this->root->getId();
            foreach ($this->distribuidores as $org) {
                $choice[$org->getNombre()] = $org->getId();
            }
        }
        $clientes['Todos de ' . $this->root->getNombre()] = '*' . $this->root->getId();
        foreach ($this->clientes as $org) {
            $clientes[$org->getNombre()] = $org->getId();
        }

        $builder
            ->add('distribuidor', ChoiceType::class, array(
                'label' => 'Distribuidor',
                'choices' => $choice,
                'required' => (count($this->distribuidores) == 0)
            ))
            ->add('cliente', ChoiceType::class, array(
                'label' => 'Cliente',
                'choices' => $clientes,
                'required' => false,
            ))
            ->add('desde', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha inicial'
                ),
                'required' => true, 'label' => 'Desde'
            ))
            ->add('hasta', TextType::class, array(
                'attr' => array(
                    'class' => 'span2 calendario',
                    'placeholder' => 'Fecha final'
                ),
                'required' => true,
                'label' => 'Hasta'
            ))
            ->add('destino', ChoiceType::class, array(
                'choices' => array(
                    'Pantalla' => '1',
                    //                        '5' => 'Excel',
                ),
                'required' => true,
            ))
            ->add('root', HiddenType::class, array(
                'attr' => array(
                    'value' => $this->root,
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'root' => null,
            'clientes' => null,
            'distribuidores' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'informefacturacion';
    }
}
