<?php

/*
 * This file is part of the UserBundle package.
 *
 * Este form permite crear un nuevo Distribuidor, pero se usa exclusivamente cuando
 * se esta logueado como distribuidor por lo que se listan las distribuiciones
 * hijas que dependen de la logueada.
 * Esto asegura que no se pueda agregar nada en una distribuidora de otra rama.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ChipType extends AbstractType
{

    protected $strEstado;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->strEstado = $options['arrayStrEstados'];
        $builder
            ->add('imei', TextType::class, array(
                'label' => 'SIM ID'
            ))
            ->add('numeroTelefono', TextType::class, array(
                'label' => 'Número Celular'
            ))
            ->add('prestadora', EntityType::class, array(
                'label' => 'Prestadora',
                'class' => 'App:Prestadora',
            ))
            ->add('estado', ChoiceType::class, array(
                'label' => 'Estado',
                'choices' => $this->strEstado
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Chip',
            'arrayStrEstados' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'chip';
    }
}
