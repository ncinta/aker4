<?php

/*
 * This file is part of the UserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Form\dist\DistribuidorConfigType as DistribuidorConfigType;
use App\Form\dist\ClienteExtraType as ClienteExtraType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClienteEditType extends AbstractType
{

    protected $paises;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $pais = $options['calypso.pais'];
        $builder
            ->add('nombre', TextType::class, array('required' => true))
            ->add('direccion', TextType::class, array('required' => false))
            ->add('latitud', TextType::class, array('required' => false))
            ->add('longitud', TextType::class, array('required' => false))
            ->add('telefono_oficina', TextType::class, array('label' => 'Teléfono'))
            ->add('email', TextType::class, array('label' => 'E-Mail'))
            ->add('web_site')
            ->add('dato_fiscal', TextType::class, array(
                'required' => false,
                'label' => 'Identificación Fiscal'
            ))
            ->add('web_site')
            ->add('proveedor_mapas', ChoiceType::class, array(
                'label' => '',
                'expanded' => false,
                'choices' => array(
                    'Security' => 'security',
                    'OpenStreetMaps' => 'osm'
                )
            ))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => array_flip($pais),
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '0' => 'Sin Limite',
                    '10' => '10 días',
                    '30' => '30 días',
                    '180' => '6 meses',
                ))
            ))
            ->add('configuracionDistribuidor', ClienteExtraType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
            'paises' => null,
        ));
        $resolver->setRequired('calypso.pais');
    }

    public function getBlockPrefix()
    {
        return 'cliente';
    }
}
