<?php

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Form\dist\DistribuidorConfigType;


class DistribuidorEditType extends AbstractType
{

    protected $paises;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->paises = $options['paises'];
        $builder
            ->add('nombre', TextType::class, array('required' => true))
            ->add('direccion', TextType::class, array('required' => false))
            ->add('telefono_oficina', TextType::class, array('label' => 'Teléfono'))
            ->add('email', EmailType::class, array('label' => 'E-Mail'))
            ->add('web_site')
            ->add('dato_fiscal', TextType::class, array(
                'required' => false,
                'label' => 'Identificación Fiscal'
            ))
            ->add('latitud', TextType::class, array('required' => false))
            ->add('longitud', TextType::class, array('required' => false))
            ->add('proveedor_mapas', ChoiceType::class, array(
                'label' => '',
                'expanded' => false,
                'choices' => array(
                    'Security' => 'security',
                    'GoogleMaps' => 'google'
                )
            ))
            ->add('limite_historial', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '0' => 'Sin Limite',
                    '10' => '10 días',
                    '30' => '30 días',
                    '180' => '6 meses',
                ))
            ))
            ->add('timeZone', ChoiceType::class, array(
                'label' => 'Zona horaria',
                'required' => true,
                'choices' => $this->paises
            ))
            ->add('configuracionDistribuidor', DistribuidorConfigType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Organizacion',
            'paises' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'distribuidor';
    }
}
