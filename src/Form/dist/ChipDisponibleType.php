<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChipDisponibleType
 *
 * @author nicolas
 */

namespace App\Form\dist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ChipDisponibleType extends AbstractType
{

    protected $qbChips;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->qbChips = $options['queryResult'];
        $choice = array();
        if ($this->qbChips != null) {
            foreach ($this->qbChips as $chip) {
                $choice[$chip->getImei()] = $chip->getId();
            }

            $builder->add('chip', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ));
        } else {
            $builder->add('chip', ChoiceType::class, array(
                'choices' => $choice,
                'multiple' => false,
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'queryResult' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'chip';
    }
}
