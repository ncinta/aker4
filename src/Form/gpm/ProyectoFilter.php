<?php

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProyectoFilter extends AbstractType
{

    protected $responsable;
    protected $estado;
    protected $contratista;
    protected $cliente;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->responsable = $options['responsable'];
        $this->estado = $options['estado'];
        $this->contratista = $options['contratista'];
        $this->cliente = $options['cliente'];

        $ch_responsable['Todos'] = 0;
        foreach ($this->responsable as $responsable) {
            $ch_responsable[$responsable->getPersona()->getNombre()] = $responsable->getId();
        }

        $ch_contratista['Todos'] = 0;
        foreach ($this->contratista as $contratista) {
            $ch_contratista[$contratista->getNombre()] = $contratista->getId();
        }
        $ch_cliente['Todos'] = 0;
        foreach ($this->cliente as $cliente) {
            $ch_cliente[$cliente->getNombre()] = $cliente->getId();
        }
        //usuarios
        $ch_estado = $this->estado;

        $builder
            ->add('desde', TextType::class, array(
                'required' => false,
                'label' => 'Desde',
            ))
            ->add('hasta', TextType::class, array(
                'required' => false,
                'label' => 'Hasta',
            ))
            ->add('fechatodos', CheckboxType::class, array(
                'label' => 'No tener en cuenta fecha',
                'invalid_message' => 'Debe ingresar una fecha válida (dd/mm/aaaa)',
                'required' => false,
            ))
            ->add('responsable', ChoiceType::class, array(
                'choices' => $ch_responsable,
                'multiple' => false,
                'label' => 'Responsable',
            ))
            ->add('contratista', ChoiceType::class, array(
                'choices' => $ch_contratista,
                'multiple' => false,
                'label' => 'Contratista',
            ))
            ->add('cliente', ChoiceType::class, array(
                'choices' => $ch_cliente,
                'multiple' => false,
                'label' => 'Cliente',
            ))
            ->add('estado', ChoiceType::class, array(
                // 'choice_label' => 'nombre',
                'choices' => $ch_estado,
                'preferred_choices' => array(-1),
                'required' => true,
                'label' => 'Estado',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\FiltroProyecto',
            'responsable' => null,
            'estado' => null,
            'contratista' => null,
            'cliente' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'filtro';
    }
}
