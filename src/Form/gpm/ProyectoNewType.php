<?php

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Description of ProyectoNewType
 *
 * @author nicolas
 */
class ProyectoNewType extends AbstractType
{

    protected $organizacion;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder

            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('codigo', TextType::class, array(
                'label' => 'Codigo',
                'required' => false,
            ))
            ->add('color', ColorType::class, array(
                'label' => 'Color',
                'required' => false,
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripción',
                'required' => false,
            ))
            ->add('fecha_inicio', TextType::class, array(
                'required' => false,
                'label' => 'Fecha de Inicio',
            ))
            ->add('fecha_fin', TextType::class, array(
                'required' => false,
                'label' => 'Fecha de Finalización',
            ))
            ->add('contratista', EntityType::class, array(
                'label' => 'Contratista',
                'class' => 'App:Contratista',
                'multiple' => false,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Contratista')->getQueryByOrganizacion($this->organizacion),
            ))
            ->add('cliente', EntityType::class, array(
                'label' => 'Cliente',
                'class' => 'App:Cliente',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Cliente')->getQueryByOrganizacion($this->organizacion),
            ))
            ->add('responsable', EntityType::class, array(
                'label' => 'Responsable de Proyecto',
                'class' => 'App:ResponsableGpm',
                'query_builder' => $this->em->getRepository('App:ResponsableGpm')->getQueryByOrganizacion($this->organizacion),
                'choice_label' => 'persona.nombre',
                'multiple' => false,
                'required' => false,
            ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => \App\Entity\Proyecto::class,
            'organizacion' => null,
            'em' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'proyecto';
    }
}
