<?php

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Description of MotivoActividad
 *
 * @author nicolas
 */
class MotivoActividadType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('descripcion', TextType::class, array(
                'label' => 'Descripción',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\MotivoActividadGpm',
        ));
    }

    public function getBlockPrefix()
    {
        return 'motivo_actividad';
    }
}
