<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of IngresoManualType
 *
 * @author nicolas
 */
class IngresoManualType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', TextType::class, array(
                'label' => 'Fecha',
                'required' => true,
            ))
            ->add('hectarea', TextType::class, array(
                'label' => 'Cantidad de hectareas',
                'required' => false,
            ))
            ->add('distancia', TextType::class, array(
                'label' => 'Cantidad de kilometros',
                'required' => false,
            ))
            ->add('tiempo', TextType::class, array(
                'label' => 'Tiempo en cumplir la actividad (Horas:minutos)',
                'required' => false,
            ))
            ->add('chofer', TextType::class, array(
                'label' => 'Chofer',
                'required' => false,
            ))
            ->add('operario', TextType::class, array(
                'label' => 'Operario, persona que ingresa estos datos',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'ingreso_manual';
    }
}
