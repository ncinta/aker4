<?php

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\ActividadGpm;

/**
 * Description of ActividadNewType
 *
 * @author nicolas
 */
class ActividadType extends AbstractType
{

    protected $organizacion;
    protected $configuracion;
    protected $contratista;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];
        $this->configuracion = $options['configuracion'];
        $this->contratista = $options['contratista'];

        $nivelTension = array(
            'Baja Tensión' => 1,
            'Media Tensión' => 2,
            'Alta Tensión' => 3,
        );

        //  die('////<pre>' . nl2br(var_export($this->configuracion, true)) . '</pre>////');
        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('codigo', TextType::class, array(
                'label' => 'Codigo',
                'required' => false,
            ))
            ->add('descripcion', TextareaType::class, array(
                'label' => 'Descripción',
                'required' => false,
            ))
            ->add('contratista', EntityType::class, array(
                'label' => 'Contratista',
                'class' => 'App:Contratista',
                'multiple' => false,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Contratista')->getQueryByOrganizacion($this->organizacion),
            ))
            ->add('fecha', TextType::class, array(
                'required' => false,
                'label' => 'Fecha',
            ))
            ->add('inicio', TextType::class, array(
                'required' => false,
                'label' => 'Hora de Inicio',
            ))
            ->add('fin', TextType::class, array(
                'required' => false,
                'label' => 'Hora de Fin',
            ))
            ->add('responsable', EntityType::class, array(
                'label' => 'Responsable de Actividad',
                'class' => 'App:ResponsableGpm',
                'query_builder' => $this->em->getRepository('App:ResponsableGpm')->getQueryByOrganizacion($this->organizacion),
                'choice_label' => 'persona.nombre',
                'required' => false,
            ))
            ->add('actgpmServicios', EntityType::class, array(
                'label' => 'Vehiculo',
                'class' => 'App:Servicio',
                'multiple' => true,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Servicio')->getQueryByOrganizacion($this->organizacion),
            ))
            ->add('actgpmRefgpm', EntityType::class, array(
                'label' => 'Lugar (referencia general)',
                'class' => 'App:ReferenciaGpm',
                'multiple' => true,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:ReferenciaGpm')->getQueryByOrganizacion($this->organizacion)
            ))
            ->add('motivos', EntityType::class, array(
                'label' => 'Motivo',
                'class' => 'App:MotivoActividadGpm',
                'choice_label' => 'nombre',
                'multiple' => false,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:MotivoActividadGpm')->getQueryByOrganizacion($this->organizacion),
            ));


        if ($this->configuracion['NIVEL_TENSION']['value']) { //para edest
            $builder->add('nivelTension', ChoiceType::class, array(
                'label' => 'Nivel de Tensión',
                'required' => false,
                'multiple' => false,
                'choices' => $nivelTension
            ));
        }
        if ($this->configuracion['CORTE_ENERGIA']['value']) { //para edest
            $builder->add('corteEnergia', CheckboxType::class, array(
                'label' => 'Corte de energía',
                'required' => false,
            ));
        }

        if ($this->configuracion['HECTAREA']['value']) { //para edest
            $builder->add('anchoTrabajo', TextType::class, array(
                'label' => 'Ancho de trabajo',
                'required' => false,
            ));
        }
        if ($this->configuracion['PERSONAL']['value']) { //para edest
            $builder->add('personal', EntityType::class, array(
                'label' => 'Personal',
                'class' => 'App:Persona',
                'multiple' => true,
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Persona')->getQueryByContratista($this->contratista)
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'organizacion' => null,
            'em' => null,
            'configuracion' => null,
            'contratista' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'actividad';
    }
}
