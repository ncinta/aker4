<?php

namespace App\Form\gpm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of ReferenciaType
 *
 * @author nicolas
 */
class ReferenciaGpmType extends AbstractType
{

    protected $organizacion;
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->organizacion = $options['organizacion'];
        $this->em = $options['em'];

        $builder
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre de referencia',
                'required' => true,
            ))
            ->add('pathIcono', HiddenType::class, array(
                'required' => false,
            ))
            ->add('dibujo', ChoiceType::class, array(
                'choices' => [
                    '' => 0,
                    'Poligonal' => 2,
                ],
                'required' => true,
            ))
            ->add('transparencia', RangeType::class, array(
                'attr' => array(
                    'min' => 1,
                    'max' => 10
                )
            ))
            ->add('tipoReferencia', EntityType::class, array(
                'label' => 'Tipo de referencia',
                'class' => 'App:TipoReferencia',
                'required' => true,
            ))
            ->add('color', ColorType::class, array(
                'label' => 'Color',
                'required' => false,
            ))
            ->add('centro', HiddenType::class, array(
                'required' => false,
            ))
            ->add('area', HiddenType::class, array(
                'required' => false,
            ))
            ->add('poligono', HiddenType::class, array(
                'required' => false,
            ))
            ->add('radio', HiddenType::class, array(
                'required' => false,
            ))
            ->add('unica', HiddenType::class, array(
                'required' => false,
            ))
            ->add('actividad', HiddenType::class, array( //va a llevar el id de la actividad si es para referencia unica
                'required' => false,
            ));
        if ($this->organizacion != null) {
            $builder->add('categoria', EntityType::class, array(
                'label' => 'Categoria',
                'class' => 'App:Categoria',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Categoria')->getQueryCategoriasDisponibles($this->organizacion),
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\ReferenciaGpm',
            'organizacion' => null,
            'em' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'referencia';
    }
}
