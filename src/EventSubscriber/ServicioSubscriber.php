<?php

namespace App\EventSubscriber;

use App\Entity\BitacoraChofer;
use App\Entity\BitacoraServicio;
use App\Entity\Chofer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\ServicioEvent;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;

class ServicioSubscriber implements EventSubscriberInterface
{
    protected $em;
    protected $userLoginManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userLoginManager)
    {
        $this->em = $em;
        $this->userLoginManager = $userLoginManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            ServicioEvent::EVENTO_DISABLED => 'onEventoDisabled',
            ServicioEvent::EVENTO_ENABLED => 'onEventoEnabled',
            // Otros eventos...
        ];
    }

    public function onEventoDisabled(ServicioEvent $event)
    {
        $evento = $event->getEvento();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($evento, false);

        $bitacora = new BitacoraServicio();     
        $bitacora->setTipoEvento($bitacora::EVENTO_EVENTO_DISABLED);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }

    public function onEventoEnabled(ServicioEvent $event)
    {
        $evento = $event->getEvento();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($evento, true);

        $bitacora = new BitacoraServicio();
        $bitacora->setTipoEvento($bitacora::EVENTO_EVENTO_ENABLED);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }


    private function buildDataArray($evento, $activado)
    {
        $data = [
            'evento' => $evento->getNombre(),
            'activado' => $activado ? 'ACtivado' : 'Desactivado',
        ];
        return $data;
    }

    private function save($bitacora, $servicio, $data)
    {
        $bitacora->setData(json_encode($data));
        $bitacora->setServicio($servicio);
        $bitacora->setEjecutor($this->userLoginManager->getUser());
        $bitacora->setOrganizacion($servicio->getOrganizacion());

        $this->em->persist($bitacora);
        $this->em->flush();

        return $bitacora;
    }
}
