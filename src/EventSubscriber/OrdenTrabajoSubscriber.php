<?php

namespace App\EventSubscriber;

use App\Entity\BitacoraChofer;
use App\Entity\BitacoraServicio;
use App\Entity\Chofer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\OrdenTrabajoEvent;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;

class OrdenTrabajoSubscriber implements EventSubscriberInterface
{
    protected $em;
    protected $userLoginManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userLoginManager)
    {
        $this->em = $em;
        $this->userLoginManager = $userLoginManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrdenTrabajoEvent::OT_NEW => 'onOrdenTrabajoNew',
            OrdenTrabajoEvent::OT_EDIT => 'onOrdenTrabajoEdit',
            OrdenTrabajoEvent::OT_CLOSE => 'onOrdenTrabajoClose',
            OrdenTrabajoEvent::OT_DELETE => 'onOrdenTrabajoDelete',
        ];
    }

    public function onOrdenTrabajoNew(OrdenTrabajoEvent $event)
    {
        $ot = $event->getOrdenTrabajo();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($ot);

        $bitacora = new BitacoraServicio();     
        $bitacora->setTipoEvento($bitacora::EVENTO_ORDENTRABAJO_NEW);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }

    public function onOrdenTrabajoDelete(OrdenTrabajoEvent $event)
    {
        $ot = $event->getOrdenTrabajo();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($ot);

        $bitacora = new BitacoraServicio();     
        $bitacora->setTipoEvento($bitacora::EVENTO_ORDENTRABAJO_DELETE);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }

    public function onOrdenTrabajoClose(OrdenTrabajoEvent $event)
    {
        $ot = $event->getOrdenTrabajo();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($ot);

        $bitacora = new BitacoraServicio();     
        $bitacora->setTipoEvento($bitacora::EVENTO_ORDENTRABAJO_CLOSE);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }



    public function onOrdenTrabajoEdit(OrdenTrabajoEvent $event)
    {
        $ot = $event->getOrdenTrabajo();
        $servicio = $event->getServicio();

        $data = $this->buildDataArray($ot);

        $bitacora = new BitacoraServicio();     
        $bitacora->setTipoEvento($bitacora::EVENTO_ORDENTRABAJO_EDIT);

        $bitacora = $this->save($bitacora, $servicio, $data);
    }



    private function buildDataArray($ot)
    {
        $data = [
            'ordenTrabajo' => $ot->getFormatIdentificador(),
            'ejecutor' => $ot->getEjecutor()->getNombre(),
            'fecha' => $ot->getFecha() ? $ot->getFecha()->format('d/m/Y'): '---',
            'descripcion' => $ot->getDescripcion(),
            'estado'=> $ot->getStrEstado(),
            'fechaCierre' => $ot->getFechaCierre() ? $ot->getFechaCierre()->format('d/m/Y'):'---',
            'fechaCierreSistema' => $ot->getCloseAt() ? $ot->getCloseAt()->format('d/m/Y') : '---',
        ];
        return $data;
    }

    private function save($bitacora, $servicio, $data)
    {
        $bitacora->setData(json_encode($data));
        $bitacora->setServicio($servicio);
        $bitacora->setEjecutor($this->userLoginManager->getUser());
        $bitacora->setOrganizacion($servicio->getOrganizacion());

        $this->em->persist($bitacora);
        $this->em->flush();

        return $bitacora;
    }
}
