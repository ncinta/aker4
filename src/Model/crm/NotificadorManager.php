<?php

namespace App\Model\crm;

/**
 * Description of PanicoControlManager
 *
 * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
 */

use Doctrine\ORM\EntityManager;
use App\Entity\Evento;
use App\Entity\Central;
use App\Entity\Notificacion;
use App\Model\app\Base\NotificadorBaseManager;

class NotificadorManager extends NotificadorBaseManager
{

    private $userlogin;

    public function setUserlogin($userlogin)
    {
        $this->userlogin = $userlogin;
    }

    public function despacharSMS($destinos)
    {
        parent::despacharSMS($destinos);
        $this->registrar($destinos);
    }

    public function despacharPUSH($personas, $mensaje, $titulo, $evento = null)
    {
        parent::despacharPUSH($personas, $mensaje, $titulo, $evento);
        $this->registrar($personas);
    }

    public function despacharMail($destinos, $asunto, $cuerpo)
    {
        $pers = null;
        foreach ($destinos as $arr) {
            if (isset($arr['persona']) && $arr['persona']->getEmail() != null && $arr['persona']->getNotificarPeticion() === true) {
                $pers[] = $arr['persona']->getEmail();
            }
        }
        if ($pers) {
            parent::despacharMail($pers, $asunto, $cuerpo);
        }
    }

    private function registrar($data)
    {
        // die('////<pre>' . nl2br(var_export('$data', true)) . '</pre>////');
        $this->em->getConnection()->beginTransaction();
        foreach ($data as $d) {
            $reg = new Notificacion();
            if ($d['persona']) {
                $reg->setPersona($d['persona']);
            }
            $reg->setAbonado($d['abonado']);
            $reg->setMensaje($d['mensaje']);
            $reg->setEjecutor($this->userlogin->getUser());
            $reg->setTipo($d['tipo']);
            $reg->setDestino($d['destino']);
            $this->em->persist($reg);
            $this->em->flush();
        }
        $this->em->getConnection()->commit();
        return true;
    }

    //$data[] = array(
    //    'tipo' => 1, //es sms.
    //    'destino' => $persona->getTelefonoCelular(),
    //    'persona' => $persona,
    //    'mensaje' => $mensaje,
    //    'ejecutor' => null,
    //    'evento' => null,
    //);
}
