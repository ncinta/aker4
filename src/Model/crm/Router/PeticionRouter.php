<?php

namespace App\Model\crm\Router;

use App\Model\app\Router\BasicRouter;

class PeticionRouter extends BasicRouter
{

    public function btnShow($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_PETICION_VER') && $servicio->getTipoObjeto() == 1) {
            return $this->armarArray('Ver PR', 'fa fa-clipboard', $this->router->generate('crm_peticion_showpeticiones', array('idServicio' => $servicio->getId())), 'Visualiza los pedidos de reparacion para el servicio');
        }
    }

    public function btnNew($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_PETICION_AGREGAR')) {
            return $this->armarArray('Nuevo PR', 'fa fa-clipboard', $this->router->generate('crm_peticion_new', array('idServicio' => $servicio->getId())), 'Agrega una nueva petición para el servicio en curso');
        }
    }

    public function btnEdit($peticion)
    {
        if ($this->userlogin->isGranted('ROLE_PETICION_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('crm_peticion_edit', array('id' => $peticion->getId())), 'Permite cambiar datos de la peticón actual.');
        }
    }

    public function btnDelete($peticion)
    {
        if ($this->userlogin->isGranted('ROLE_PETICION_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
