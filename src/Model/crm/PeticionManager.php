<?php

namespace App\Model\crm;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion;
use App\Entity\Servicio;
use App\Entity\Peticion;
use App\Entity\Comentario;
use App\Entity\FiltroPeticion;
use App\Model\app\InformeUtilsManager;
use App\Model\app\UserLoginManager;

class PeticionManager
{

    protected $em;
    protected $userlogin;
    protected $informeUtilsManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, InformeUtilsManager $informeUtilsManager)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->informeUtilsManager = $informeUtilsManager;
    }

    public function create($servicio, $usuario)
    {
        $estado = $this->em->getRepository('App:Estado')->getDefault();

        $peticion = new Peticion;
        $peticion->setServicio($servicio);
        $peticion->setAutor($usuario);
        $peticion->setAsignadoA($usuario);
        $peticion->setOrganizacion($usuario->getOrganizacion());
        $peticion->setIsPrivado(false);
        $peticion->setEstado($estado);
        $peticion->setPrioridad(1);
        $peticion->setResultado(0);
        $peticion->setTipo(0);
        return $peticion;
    }

    public function createMany($organizacion, $usuario)
    {
        $estado = $this->em->getRepository('App:Estado')->getDefault();

        $peticion = new Peticion;
        $peticion->setAutor($usuario);
        $peticion->setOrganizacion($organizacion());
        $peticion->setIsPrivado(false);
        $peticion->setEstado($estado);
        $peticion->setPrioridad(1);
        $peticion->setTipo(0);
        return $peticion;
    }

    public function findAllByIds($ids)
    {
        return $this->em->getRepository('App:Peticion')->findAllByIds($ids);
    }

    public function find($id)
    {
        return $this->em->getRepository('App:Peticion')->findOneBy(array('id' => $id));
    }

    public function findAutorAll($autor, $organizacion = null)
    {
        if ($organizacion) {
            return $this->em->getRepository('App:Peticion')->findOrganizacionAll($autor, $organizacion);
        }
        return $this->em->getRepository('App:Peticion')->findAutorAll($autor);
    }

    public function findAbiertas($servicio)
    {
        return $this->em->getRepository('App:Peticion')->findByServicio($servicio, -1);
    }

    public function findByServicio($servicio)
    {
        return $this->em->getRepository('App:Peticion')->findByServicio($servicio);
    }

    public function save(Peticion $peticion)
    {
        $this->em->persist($peticion);
        $this->em->flush();
        return ($peticion);
    }

    public function addNota(Peticion $peticion, $nota, $usuario)
    {
        $comentario = new Comentario();
        $comentario->setPeticion($peticion);
        $comentario->setUsuario($usuario);
        $comentario->setTexto($nota);
        $comentario->setCambio(json_encode($peticion->getChanges()));
        $comentario->setResultado($peticion->getResultado());

        $peticion->addComentario($comentario);
        $this->em->persist($comentario);
    }

    public function setEnCurso(Peticion $peticion)
    {
        $estado = $this->em->getRepository('App:Estado')->find(2);   //hardcode de peticion en curso.
        $peticion->setEstado($estado);
        if (is_null($peticion->getFechaInicio())) {
            $peticion->setFechaInicio(new \DateTime(), new \DateTimeZone($this->userlogin->getTimezone()));
        }
        $this->save($peticion);
        return $peticion;
    }

    public function setCerrada(Peticion $peticion, $fin, $motivo = null)
    {
        $estado = $this->em->getRepository('App:Estado')->getCerrado();
        $peticion->setEstado($estado);
        $peticion->setTerminado(100);
        $peticion->setFechaFin($fin);
        $peticion->setCloseAt(new \DateTime());
        $peticion->setMotivoCierre($motivo);
        $this->save($peticion);
        return $peticion;
    }

    public function setExito(Peticion $peticion)
    {
        //$this->setCerrada($peticion);
        $peticion->setResultado(1);
    }

    public function setFracaso(Peticion $peticion)
    {
        $peticion->setResultado(-1);
    }

    public function nuevaPeticion($newPet, $nota, $usuario, $estado, $setPadre, $servicio = null)
    {
        //    $newPet = $this->create($peticion->getServicio(), $peticion->getAutor());
        //    $newPet->setFechaInicio(new \DateTime());
        $newPet->setDescripcion(sprintf('%s<p>%s @ %s  <em>%s</em>', $newPet->getDescripcion(), $usuario, $newPet->getUpdated_at()->format('d-m-Y H:i'), $nota));
        $newPet->setCategoria($newPet->getCategoria());
        $newPet->setEstado($estado);
        $newPet->setServicio($servicio);
        if ($setPadre) {
            $newPet->setPadre($newPet);
        }
        $this->save($newPet);
        return $newPet;
    }

    public function cerrarPeticion(Peticion $peticion, $estado)
    {
        $peticion->setFechaFin(new \DateTime());
        $peticion->setEstado($estado);
        $this->save($peticion);
        return $peticion;
    }

    public function obtener($filtro)
    {
        $arrServicios = $this->informeUtilsManager->obtenerServicios($filtro->getServicio());
        $servicios = array();
        foreach($arrServicios['servicios'] as $servicio){
            $servicios[] = $servicio->getId();

        }
   //  die('////<pre>' . nl2br(var_export($servicios, true)) . '</pre>////');            
        return $this->em->getRepository('App:Peticion')->filter($filtro, $servicios);
    }

    public function getDataStatusUsuarios($organizacion, $fecha)
    {
        $usuarios = null;
        $usrTmp = $organizacion->getUsuarios();
        foreach ($usrTmp as $usuario) {
            $add = false;
            if (count($usuario->getPeticionesAutor()) > 0) {
                $totalAutor = count($usuario->getPeticionesAutor());
            }
            if (count($usuario->getPeticionesAsignadas()) > 0) {
                $totalAsignadas = count($usuario->getPeticionesAsignadas());
            }

            //esto es para obtener la estructura de peticiones.
            $filtro = new FiltroPeticion();
            $filtro->setAsignadoA($usuario);
            $filtro->setEstado(-1);  //abierta
            $filtro->setFecha($fecha);
            $filtro->setFechaPuntual(false);
            $pets = $this->obtener($filtro);
            $peticiones = array(
                'Hoy' => array('color' => 'success', 'cantidad' => 0, 'cerradas' => 0, 'abiertas' => '0'),
                'Atrasadas' => array('color' => 'warning', 'cantidad' => 0, 'cerradas' => 0, 'abiertas' => '0')
            );
            foreach ($pets as $p) {
                if ($p->getFechaInicio()->format('Ymd') == $fecha->format('Ymd')) {
                    $peticiones['Hoy']['cantidad'] = $peticiones['Hoy']['cantidad'] + 1;
                    $add = true;
                } else {
                    $peticiones['Atrasadas']['cantidad'] = $peticiones['Atrasadas']['cantidad'] + 1;
                    $add = true;
                }
            }
            unset($filtro);

            if ($add) {
                $usuarios[] = array(
                    'totalAutor' => isset($totalAutor) ? $totalAutor : 0,
                    'totalAsignadas' => isset($totalAsignadas) ? $totalAsignadas : 0,
                    'peticiones' => $peticiones,
                    'usuario' => $usuario,
                );
            }
        }
      
        return $usuarios;
    }

    public function findAllByOrg(Organizacion $organizacion, $desde = null, $hasta = null)
    {
        return $this->em->getRepository('App:Peticion')->findByOrganizacion($organizacion, $desde, $hasta);
    }

    public function findAllEstadoByOrg(Organizacion $organizacion, $desde = null, $hasta = null)
    {
        $nuevas = $this->em->getRepository('App:Peticion')->findNueva($organizacion, $desde, $hasta);
        $encurso = $this->em->getRepository('App:Peticion')->findEnCurso($organizacion, $desde, $hasta);
        $cerradas = $this->em->getRepository('App:Peticion')->findCerrada($organizacion, $desde, $hasta);

        return array_merge($nuevas, $encurso, $cerradas);
    }

    public function delete(Peticion $peticion)
    {
        $this->em->remove($peticion);
        $this->em->flush();
        return true;
    }

    // cuenta las peticiones por estado
    public function getCountPeticionPorEstado($servicio, $estado, $pendiente, $fecha)
    {        
        $query = $this->em->createQueryBuilder()
            ->select('count(p.id)')
            ->from('App:Peticion', 'p')
            ->join('p.estado', 'e')
            ->where('p.servicio = :servicio')
            ->andWhere('e.posicion = :estado')
            ->setParameter('servicio', $servicio)
            ->setParameter('estado', $estado);
        if (!$pendiente) {
            $query->andWhere('p.fecha_inicio <= :fecha');
            $query->setParameter('fecha', $fecha);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    // traigo las que se crearon ese dia
    public function getNuevasPorServicio($servicio)
    {
        $ayer = date("Y-m-d H:m:s", strtotime('-1 days'));
        $hoy = date("Y-m-d H:m:s");
        $query = $this->em->createQueryBuilder()
            ->select('count(p.id)')
            ->from('App:Peticion', 'p')
            ->where('p.servicio = :servicio')
            ->andWhere('p.created_at >= :ayer')
            ->andWhere('p.created_at <= :hoy')
            ->setParameter('ayer', $ayer)
            ->setParameter('hoy', $hoy)
            ->setParameter('servicio', $servicio);
        return $query->getQuery()->getSingleScalarResult();
    }

    // traigo las que se cerraron ese dia
    public function getRealizadasPorServicio($servicio)
    {
        $ayer = date("Y-m-d H:m:s", strtotime('-1 days'));
        $hoy = date("Y-m-d H:m:s");
        $query = $this->em->createQueryBuilder()
            ->select('count(p.id)')
            ->from('App:Peticion', 'p')
            ->where('p.servicio = :servicio')
            ->andWhere('p.close_at >= :ayer')
            ->andWhere('p.close_at <= :hoy')
            ->setParameter('ayer', $ayer)
            ->setParameter('hoy', $hoy)
            ->setParameter('servicio', $servicio);
        return $query->getQuery()->getSingleScalarResult();
    }
}
