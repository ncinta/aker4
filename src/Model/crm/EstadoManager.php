<?php

namespace App\Model\crm;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Abonado;
use App\Entity\Peticion;
use App\Entity\Comentario;

class EstadoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAll()
    {
        return $this->em->getRepository('App:Estado')->findBy(array(), array('posicion' => 'ASC'));
    }

    public function findDefault()
    {
        return $this->em->getRepository('App:Estado')->findOneBy(array('isDefault' => true));
    }
    public function findById($id)
    {
        return $this->em->getRepository('App:Estado')->findOneBy(array('id' => $id));
    }
}
