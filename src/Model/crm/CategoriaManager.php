<?php

namespace App\Model\crm;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Categoria;
use App\Entity\Peticion;
use App\Entity\Comentario;

class CategoriaManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAll()
    {
        return $this->em->getRepository('App:Categoria')->findAll();
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Categoria')->findOneBy(array('id' => $id));
    }
}
