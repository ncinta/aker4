<?php

namespace App\Model\informe;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contacto as Contacto;
use App\Entity\Equipo as Equipo;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\ContactoManager;
use App\Model\app\EquipoManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\EventoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\GmapsManager;
use App\Model\app\BackendManager;
use App\Model\app\ChoferManager;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;

/**
 * Description of InformeManager
 *
 * @author nicolas
 */
class InformeManager
{
    protected $equipoManager;
    protected $servicioManager;
    protected $referenciaManager;
    protected $grupoReferenciaManager;
    protected $eventoManager;
    protected $contactoManager;
    protected $security;
    protected $em;
    protected $cliente;
    protected $container;
    protected $utilsManager;
    protected $doctrine;
    protected $userLoginManager;
    protected $gmapsManager;
    protected $backendManager;
    protected $choferManager;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        HttpClientInterface $cliente,
        EquipoManager $equipoManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoManager $eventoManager,
        ContactoManager $contactoManager,
        ContainerInterface $container,
        UtilsManager $utilsManager,
        ManagerRegistry $doctrine,
        UserLoginManager $userLoginManager,
        GmapsManager $gmapsManager,
        BackendManager $backendManager,
        ChoferManager $choferManager
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->equipoManager = $equipoManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoManager = $eventoManager;
        $this->contactoManager = $contactoManager;
        $this->cliente = $cliente;
        $this->container = $container;
        $this->utilsManager = $utilsManager;
        $this->doctrine = $doctrine;
        $this->userLoginManager = $userLoginManager;
        $this->gmapsManager = $gmapsManager;
        $this->backendManager = $backendManager;
        $this->choferManager = $choferManager;
    }

    public function getInbox($user)
    {
        $inbox = $this->em->getRepository('App:informe\Inbox')->findByAutor($user);

        if (count($inbox) == 0) {
            $inbox = $this->em->getRepository('App:informe\Inbox')->findByOrganizacion($user->getOrganizacion());
        }
        return $inbox;
    }

    public function findByUid($uid)
    {
        //  die('////<pre>' . nl2br(var_export($this->em->getRepository('App:informe\Inbox')->findByUid($uid), true)) . '</pre>////');
        return $this->em->getRepository('App:informe\Inbox')->findByUid($uid);
    }

    public function sendInforme($data, $tipo)
    {
        $arr = null;
        $response = null;
        switch ($tipo) {
            case Inbox::TIPO_PRUDENCIA_VIAL:
                $arr = $this->getArrayPrudenciaVial($data);
                $data['nombreServicio'] = $arr['nombreServicio'];
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL:
                $arr = $this->getArrayResponsabilidadVialGenerico($data, 'v1');
                $data['nombreServicio'] = $arr['nombreServicio'];
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2:
                $arr = $this->getArrayResponsabilidadVialGenerico($data, 'v2');
                $data['nombreChofer'] = $arr['nombreChofer'];
                break;
            case Inbox::TIPO_EXCESO_VELOCIDAD:
                $arr = $this->getArrayExcesoVelocidad($data);
                $data['nombreServicio'] = $arr['nombreServicio'];
                break;
            default:
                $form = null;
                break;
        }
        $data['nombreReferencia'] = $arr['nombreReferencia'];
        //dd($json);
        try {
            $data['informe'] = $data;
            $response = $this->sendData($arr);
        } catch (Exception $e) {
        }
        return $this->create($data, $response, $tipo);
    }

    public function sendData($arr)
    {
        //  die('////<pre>' . nl2br(var_export(json_encode($arr, true), true)) . '</pre>////');
        $result = 'ERROR';
        try {
            //  die('////<pre>' . nl2br(var_export($arr, true)) . '</pre>////');
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_addr_informes') . $arr['url'],
                array(
                    'body' => json_encode($arr, true),
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    //  'auth_basic' => array(
                    //      $this->container->getParameter('aker_usr_informes'),
                    //      $this->container->getParameter('aker_pass_informes'),
                    //  )
                )
            );
        } catch (TransportException $e) {
            return false;
        }

        return $response;
    }



    function getArrayPrudenciaVial($data)
    {
        $dataServicio =  $this->servicioManager->getIds($data['servicio']);
        $dataReferencia =  $this->referenciaManager->getIds($data['referencia']);
        $arr = array(
            "url" => "/v1/prudencia-vial",
            "solicitud" => array(
                "dominio" => "clientes",
                "entidad" => "usuario",
                "id_externo" => intval($data['usuario']->getId()) //ver estos datos
            ),
            "modelo" => "prudencia-vial-v1",
            "fecha_inicio" => $this->utilsManager->dateStringToIso($data['desde'], $this->userLoginManager->getTimezone()),
            "fecha_fin" => $this->utilsManager->dateStringToIso($data['hasta'], $this->userLoginManager->getTimezone()),
            "nombreServicio" => $dataServicio['nombre'],
            "nombreReferencia" => $dataReferencia['nombre'],
            "servicios" => $dataServicio['ids'],
            "referencias" => $dataReferencia['ids'],
            "parametros" => array(
                "vel_max_en_ruta" => intval($data['max_ruta']),
                "vel_max_en_avenida" => intval($data['max_avenida']),
                'habilitar_maxima_por_mapa' => isset($data['maxima_por_mapa']) && $data['maxima_por_mapa'] == '1' ? true : false
            )
        );
        return $arr;
    }

    function getArrayResponsabilidadVialGenerico($data, $tipo = 'v1')
    {
        // Definir valores específicos dependiendo del tipo de solicitud
        if ($tipo === 'v1') {
            $dataEntidad = $this->servicioManager->getIds($data['servicio']);
            $url = "/v1/responsabilidad-vial";
            $modelo = "responsabilidad-vial-v1";
            $nombreEntidad = 'nombreServicio';
            $entidadKey = 'servicios';
        } else {
            $dataEntidad = $this->choferManager->getIds($data['chofer']);
            $url = "/v2/responsabilidad-vial";
            $modelo = "responsabilidad-vial-v2";
            $nombreEntidad = 'nombreChofer';
            $entidadKey = 'choferes';
        }

        // Datos comunes
        $dataReferencia = $this->referenciaManager->getIds($data['referencia']);

        // Construcción del array
        $arr = array(
            "url" => $url,
            "solicitud" => array(
                "dominio" => "clientes",
                "entidad" => "usuario",
                "id_externo" => intval($data['usuario']->getId())
            ),
            "modelo" => $modelo,
            "fecha_inicio" => $this->utilsManager->dateStringToIso($data['desde'], $this->userLoginManager->getTimezone()),
            "fecha_fin" => $this->utilsManager->dateStringToIso($data['hasta'], $this->userLoginManager->getTimezone()),
            $nombreEntidad => $dataEntidad['nombre'],
            "nombreReferencia" => $dataReferencia['nombre'],
            $entidadKey => $dataEntidad['ids'],
            "referencias" => $dataReferencia['ids'],
            "parametros" => array(
                "vel_max_en_referencia" => intval($data['max_referencia']),
                "vel_max_fuera_referencia" => intval($data['max_resto']),
                "indice_leve" => intval($this->container->getParameter('responsabilidad_leve')),
                "indice_medio" => intval($this->container->getParameter('responsabilidad_medio')),
                "indice_alto" => intval($this->container->getParameter('responsabilidad_alto')),
                'habilitar_maxima_por_mapa' => isset($data['maxima_por_mapa']) && $data['maxima_por_mapa'] == '1' ? true : false
            )
        );
        // dd($arr);
        return $arr;
    }

    function getArrayExcesoVelocidad($data)
    {
        $dataServicio =  $this->servicioManager->getIds($data['servicio']);
        $dataReferencia =  $this->referenciaManager->getIds($data['referencia']);

        $arr = array(
            "url" => "/v1/exceso-velocidad",
            "solicitud" => array(
                "dominio" => "clientes",
                "entidad" => "usuario",
                "id_externo" => intval($data['usuario']->getId()) //ver estos datos
            ),
            "modelo" => "exceso-velocidad-v1",
            "fecha_inicio" => $this->utilsManager->dateStringToIso($data['desde'], $this->userLoginManager->getTimezone()),
            "fecha_fin" => $this->utilsManager->dateStringToIso($data['hasta'], $this->userLoginManager->getTimezone()),
            "nombreServicio" => $dataServicio['nombre'],
            "nombreReferencia" => $dataReferencia['nombre'],
            "servicios" => $dataServicio['ids'],
            "referencias" => $dataReferencia['ids'],
            "parametros" => array(
                "velocidad_maxima" => intval($data['velocidad_maxima']),
                "mostrar_direcciones" => isset($data['mostrar_direcciones']) ? true : false,
                'habilitar_maxima_por_mapa' => isset($data['maxima_por_mapa']) && $data['maxima_por_mapa'] == '1' ? true : false
            )
        );
        return $arr;
    }

    public function create($data, $response, $tipo)
    {

        $result = null;
        $statusCode = $response != null ? $response->getStatusCode() : null;
        if ($statusCode === 200) {
            $result = json_decode($response->getContent(), true);
        }
        $informe = new Inbox();
        $informe->setFechaDesde($data['desde']);
        $informe->setFechaHasta($data['hasta']);
        $informe->setUid($result != null ? $result['resultado']['uid'] : '---');
        $informe->setAutor($data['usuario']);
        $informe->setAsunto($data['asunto']);
        $informe->setOrganizacion($data['organizacion']);
        $informe->setEstado($result != null && $result['estado'] == 'ok' ? Inbox::ESTADO_EN_COLA : Inbox::ESTADO_ERROR);
        $informe->setTipo($tipo);
        unset($data['token']);
        unset($data['organizacion']);
        unset($data['usuario']);
        $informe->setDataInforme($data);

        return $this->save($informe);
    }

    public function save(Inbox $inbox)
    {
        try {
            $this->em->persist($inbox);
            $this->em->flush();
        } catch (Exception $e) {
            $inbox = false;
        }
        return $inbox;
    }

    public function getDetalle($servicio, $desde, $hasta, $posiciones)
    {
        $dataResumen = array();
        $dataDetalle = array();
        $dataDetalle['nombre'] = $servicio->getNombre();
        $dataDetalle['posiciones'] = [];
        $referencias = [];  //array con las referencias involucradas en los excesos
        foreach ($posiciones as $posicion) {
            $referencia = null;
            $fecha = $posicion->getFecha();
            // QUITAR IF CUANDO EMMA SOLUCUIONE EL PROBLEMA DE DUPLICADOS-- TRIPLICADOS
            if (!$this->fechaExiste($dataDetalle['posiciones'], $fecha)) { //eliminamos los duplicados, no sabemos porque se duplican
                if ($posicion->getReferencia()) {
                    $referencia = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    $referencias[$posicion->getReferencia()->getId()] = $referencia;
                }
                $dataDetalle['posiciones'][] = array(
                    'fecha' => $posicion->getFecha(),
                    'latitud' => $posicion->getLatitud(),
                    'longitud' => $posicion->getLongitud(),
                    'velocidad' => $posicion->getVelocidad(),
                    'velMaxPermitida' => $posicion->getMaximaPermitida(),
                    'tipoExceso' => $posicion->getTipoExceso(),
                    'ubicacion' => $posicion->getReferencia() ? $referencia->getNombre() : $posicion->getOsmDireccion(),
                    'riesgo' => $posicion->getRiesgo(),
                    'tipo' => $posicion->getTipoExceso(),
                    'referencia' => $referencia,
                );
            }
        }
        $referenciasShow = $this->gmapsManager->castReferencias($referencias);

        return  array(
            'data' => $dataDetalle,
            'referenciasShow' => $referenciasShow
        );
    }




    // Función para verificar si una fecha existe en el array
    public  function fechaExiste($array, $fecha)
    {
        foreach ($array as $item) {
            if ($item['fecha'] == $fecha) {
                return true;
            }
        }
        return false;
    }
}
