<?php

namespace App\Model\informe\responsabilidad_vial\v2;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\BackendManager;
use App\Model\app\ChoferManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use App\Model\informe\responsabilidad_vial\ResponsabilidadVialBaseManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;


class ResponsabilidadVialManager extends ResponsabilidadVialBaseManager
{



    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager,
        InformeManager $informeManager,
        UtilsManager $utilsManager,
        GmapsManager $gmapsManager,
        BackendManager $backendManager,
        ManagerRegistry $doctrine,
        ChoferManager $choferManager
    ) {
        // Simplemente llamamos al constructor de la clase base
        parent::__construct(
            $em,
            $servicioManager,
            $referenciaManager,
            $excelManager,
            $informeManager,
            $utilsManager,
            $gmapsManager,
            $backendManager,
            $doctrine,
            $choferManager
        );
    }
    
    public function getExcelResponsabilidadVial($data, $dataInforme)
    {
        // Extraer la configuración del encabezado a un método separado
        $xls = $this->createExcelWithHeader($dataInforme);
        
        $startRow = 9;
        $startColumn = 'B';
        $data['categorias'][0] = 'Fuera';

        // Configurar columna del chofer
        $xls->setBar($startRow, [
            'A' => ['title' => 'Chofer', 'width' => 30]
        ]);

        // Generar las columnas de categorías
        $currentColumnAscii = $this->generateCategoryColumns($xls, $data, $startRow, $startColumn);
        
        // Generar las columnas de totales
        $this->generateTotalColumns($xls, $startRow + 1, $currentColumnAscii);
            
        // Generar las filas de datos
        $this->generateDataRows($xls, $data, $startRow + 2, $startColumn);

        return $xls;
    }

    private function generateTotalColumns($xls, $row, $currentColumnAscii) {
        $currentColumn = $currentColumnAscii - 64; // Convertir a número (A=1, B=2, etc.)
        $xls->setBar($row, [
            $this->numberToColumnLetter($currentColumn) => ['title' => 'Total Hs.', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 1) => ['title' => 'Detenido', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 2) => ['title' => 'Ralentí', 'width' => 20], 
            $this->numberToColumnLetter($currentColumn + 3) => ['title' => 'Total Kms.', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 4) => ['title' => 'IR Total', 'width' => 20]
        ]);
    }

    private function generateDataRows($xls, $data, $startRow, $startColumn) {
        // Implementación de la generación de filas de datos
        // Generar las filas de datos
        $i = $startRow + 2;
        foreach ($data as $key => $row) {
            if ($key === 'categorias') continue;

            $currentColumn = ord($startColumn) - 64; // Convertimos a número
            $xls->setBar($i, ['A' => ['title' => $key, 'width' => 30]]);

            // Datos por categoría
            foreach ($data['categorias'] as $categoria) {
                $valores = $row['zonas'][$categoria] ?? null;
                $xls->setBar($i, [
                    $this->numberToColumnLetter($currentColumn) => ['title' => $valores ? $valores['leve']['cantidad'] : '---', 'width' => 10],
                    $this->numberToColumnLetter($currentColumn + 1) => ['title' => $valores ? $valores['medio']['cantidad'] : '---', 'width' => 10], 
                    $this->numberToColumnLetter($currentColumn + 2) => ['title' => $valores ? $valores['alto']['cantidad'] : '---', 'width' => 10],
                    $this->numberToColumnLetter($currentColumn + 3) => ['title' => $valores ? $valores['ir']['valor'] : '---', 'width' => 10]
                ]);
                $currentColumn += 4;
            }

            // Datos finales
            $xls->setBar($i, [
                $this->numberToColumnLetter($currentColumn) => ['title' => $row['total_hs'] ?? '---', 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 1) => ['title' => $row['detenido'] ?? '---', 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 2) => ['title' => $row['ralenti'] ?? '---', 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 3) => ['title' => $row['total_kms'] ?? '---', 'width' => 20], 
                $this->numberToColumnLetter($currentColumn + 4) => ['title' => $row['ir_total']['valor'] ?? '---', 'width' => 20]
            ]);

            $i++;
        }
    }

    private function createExcelWithHeader($dataInforme)
    {
        $xls = $this->excelManager->create("Informe de Responsabilidad Vial");
        $headerInfo = [
            'C2' => ['label' => 'Chofer', 'value' => $dataInforme['nombreChofer'] ?? '---'],
            'C3' => ['label' => 'Fecha desde', 'value' => $dataInforme['desde'] ?? '---'],
            'C4' => ['label' => 'Fecha hasta', 'value' => $dataInforme['hasta'] ?? '---'],
            'C5' => ['label' => 'Referencias', 'value' => $dataInforme['nombreReferencia'] ?? '---'],
            'C6' => ['label' => 'Máx Referencia', 'value' => $dataInforme['max_referencia'] ?? '---'],
            'C7' => ['label' => 'Máx Fuera', 'value' => $dataInforme['max_fuera'] ?? '---']
        ];

        $formattedHeader = [];
        foreach ($headerInfo as $cell => $info) {
            $formattedHeader[$cell] = $info['label'];
            $formattedHeader[chr(ord($cell[0]) + 1) . $cell[1]] = $info['value'];
        }
        
        $xls->setHeaderInfo($formattedHeader);
        return $xls;
    }

    private function generateCategoryColumns($xls, $data, $row, $startColumn)
    {
        $currentColumnAscii = ord($startColumn);
        
        foreach ($data['categorias'] as $categoria) {
            $width = 4;
            $currentColumnLetter = $this->numberToColumnLetter($currentColumnAscii - 64);
            
            // Configurar el título de la categoría
            $xls->setCellColspan($row, [
                $currentColumnLetter => ['title' => $categoria, 'width' => $width]
            ]);
            
            // Configurar las subcolumnas
            $subColumns = [
                'Leve', 'Medio', 'Alto', 'IR'
            ];
            
            $subColumnsConfig = [];
            for ($i = 0; $i < count($subColumns); $i++) {
                $subColumnsConfig[chr($currentColumnAscii + $i)] = [
                    'title' => $subColumns[$i],
                    'width' => 10
                ];
            }
            
            $xls->setBar($row + 1, $subColumnsConfig);
            $currentColumnAscii += $width;
        }
        
        return $currentColumnAscii;
    }

    public function getResumen($uid, $ids, $desde, $hasta)
    {
        //  dd($uid);
        $data = [];       
        foreach ($ids as $id) {
            $chofer = $this->choferManager->find(intval($id));
            $ks = $chofer->getNombre();
            $idChofer = $chofer->getId();

            // Inicializamos el array del chofer solo una vez
            $data[$ks]['total'] = 0;
            $data[$ks]['id'] = $idChofer;
            $data[$ks]['zonas'] = [];  // Esto debe estar vacío inicialmente

            $recorridos = $this->getRecorridosByChofer($uid, $chofer);
            $totalMetrosRecorridos = 0;
            $tiempoRalenti = 0;
            $tiempoDetenido = 0;
            $tiempoTotal = 0;
            $excesos = [];

            foreach ($recorridos as $recorrido) {
                $excesos = array_merge($excesos, $this->getExcesosByRecorrido($recorrido, 2));
                // Acumulamos los tiempos y metros recorridos
                $tiempoTotal += $recorrido->getTiempoTotal();
                $tiempoDetenido += $recorrido->getTiempoDetenido();
                $tiempoRalenti += $recorrido->getTiempoRalenti();
                $totalMetrosRecorridos += $recorrido->getMetrosRecorridos();
            }

            // Procesamos los excesos y zonas
            if (count($excesos) > 0) {
                $zonasProcesadas = $this->processExcesos($excesos);
                // Combinamos las zonas procesadas con las zonas ya existentes
                foreach ($zonasProcesadas as $zona => $detalleZona) {
                    if (!isset($data[$ks]['zonas'][$zona])) {
                        $data[$ks]['zonas'][$zona] = $detalleZona;
                    } else {
                        // Aquí puedes manejar cómo combinar los detalles de zonas ya existentes
                        $data[$ks]['zonas'][$zona] = array_merge($data[$ks]['zonas'][$zona], $detalleZona);
                    }
                }
            }

            foreach ($excesos as $exceso) {
                foreach ($exceso->getPosiciones() as $posicion) {
                    if ($posicion->getReferencia()) {
                        $data[$ks]['referenciasHidden'][] = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    }
                }
            }
            // Asignamos los datos de tiempos y distancias al chofer
            $data[$ks]['tiempo_total'] = $this->utilsManager->segundos2tiempo($tiempoTotal);
            $data[$ks]['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($tiempoDetenido);
            $data[$ks]['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo($tiempoRalenti);
            $data[$ks]['total_metros_recorridos'] = $totalMetrosRecorridos / 1000;

            // Calculamos IR Total
            $data[$ks]['ir_total'] = $this->calcularIrTotal($data[$ks]['zonas']);
            $data[$ks]['referenciasHidden'] = [];
            $data[$ks]['total']++;
        }
      
        $data['categorias'] = array_unique($this->categoriasCache);
        ksort($data['categorias']);

        return $data;
    }


    public function getPosicionesByChofer($uid, $chofer, $desde, $hasta)
    {
        //coloco solo las posiciones iniciales del exceso porque n oes necesario que sea tan detallado como prudencia vial
        $recorridos = $this->getRecorridosByChofer($uid, $chofer);
        $posiciones = [];
        foreach ($recorridos as $recorrido) {
            foreach ($recorrido->getExcesos() as $exceso) {
                $posInit = $exceso->getPosicionInicial()->getFecha();
                $posiciones[$posInit->getTimestamp()] =  $exceso->getPosicionInicial();
            }
        }
        ksort($posiciones);
        //   dd($posiciones);
        return $posiciones;
    }



    public function getDetalle($uid, $id, $desde, $hasta)
    {
        $chofer =  $this->choferManager->find(intval($id));
        $posiciones = $this->getPosicionesByChofer($uid, $chofer, $desde, $hasta);

        return $this->getData($chofer, $desde, $hasta, $posiciones);
    }

    public function getReferenciasHidden($uid, $ids, $desde, $hasta)
    {
        $resumen = $this->getResumen($uid, $ids, $desde, $hasta);
        $referencias = array();
        foreach ($ids as $id) {
            $chofer = $this->choferManager->find(intval($id));
            if (isset($resumen[$chofer->getNombre()])) {
                $referencias = array_merge($referencias, $resumen[$chofer->getNombre()]['referenciasHidden']);
            }
        }
        $referenciasHidden = $this->gmapsManager->castVisibilidadReferencias($referencias, false);

        return $referenciasHidden;
    }


    public function getData($chofer, $desde, $hasta, $posiciones)
    {
        $dataResumen = array();
        $dataDetalle = array();
        $dataDetalle['nombre'] = $chofer->getNombre();
        $dataDetalle['posiciones'] = [];
        $referencias = [];  //array con las referencias involucradas en los excesos
        foreach ($posiciones as $posicion) {
            $referencia = null;
            $fecha = $posicion->getFecha();

            $exceso = $posicion->getExceso();
            $recorrido = $exceso->getRecorrido();
            $servicio = $recorrido->getServicio();
            $velMaxPermitida = $posicion->getMaximaPermitida();
            $velMaxPromedio = $exceso->getMaximaPromedio();

            // QUITAR IF CUANDO EMMA SOLUCUIONE EL PROBLEMA DE DUPLICADOS-- TRIPLICADOS
            if (!$this->informeManager->fechaExiste($dataDetalle['posiciones'], $fecha)) { //eliminamos los duplicados, no sabemos porque se duplican
                if ($posicion->getReferencia()) {
                    $referencia = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    $referencias[$posicion->getReferencia()->getId()] = $referencia;
                }
                $dataDetalle['posiciones'][] = array(
                    'fecha' => $posicion->getFecha(),
                    'servicio' => $servicio->getNombre(),
                    'latitud' => $posicion->getLatitud(),
                    'longitud' => $posicion->getLongitud(),
                    'velocidad' => $velMaxPromedio,
                    'velMaxPermitida' => $velMaxPermitida,
                    'tipoExceso' => $posicion->getTipoExceso(),
                    'ubicacion' => $posicion->getReferencia() ? $referencia->getNombre() : $posicion->getOsmDireccion(),
                    'riesgo' => $this->getStrRiesgo($velMaxPromedio, $velMaxPermitida),
                    'tipo' => $posicion->getTipoExceso(),
                    'tiempoExceso' => $exceso != null ?  $this->utilsManager->segundos2tiempo($exceso->getTiempoExceso()) : 0,
                    'distanciaExceso' => $exceso != null ?  $exceso->getDistanciaRecorrida() / 1000 : 0,
                    'referencia' => $referencia,
                    'zona' => $referencia != null && $referencia->getCategoria() != null ? $referencia->getCategoria()->getNombre() : 'Referencias Sin Categoría',
                );
            }
        }

        $referenciasShow = $this->gmapsManager->castReferencias($referencias);

        return  array(
            'data' => $dataDetalle,
            'referenciasShow' => $referenciasShow
        );
    }


    public function getRecorridosByChofer($uid, $chofer)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\responsabilidad_vial\v2\Recorrido');
        $queryBuilder = $repository->createQueryBuilder('p');
        $queryBuilder
            ->where('p.chofer = :chofer')
            ->andWhere('p.informe = :uid')
            ->setParameter('chofer', $chofer->getId())
            ->setParameter('uid', $uid);

        return $queryBuilder->getQuery()->getResult();
    }

    
}
