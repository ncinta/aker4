<?php

namespace App\Model\informe\responsabilidad_vial;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\BackendManager;
use App\Model\app\ChoferManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Description of InformeManager
 *
 * @author nicolas
 */
class ResponsabilidadVialBaseManager
{
    public $servicioManager;
    public $referenciaManager;
    public $em;
    public $utilsManager;
    public $informeManager;
    public $excelManager;
    public $gmapsManager;
    public $backendManager;
    public $choferManager;
    public $doctrine;
    public $categoriasCache = [];

    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager,
        InformeManager $informeManager,
        UtilsManager $utilsManager,
        GmapsManager $gmapsManager,
        BackendManager $backendManager,
        ManagerRegistry $doctrine,
        ChoferManager $choferManager
    ) {

        $this->em = $em;
        $this->servicioManager = $servicioManager;
        $this->excelManager = $excelManager;
        $this->referenciaManager = $referenciaManager;
        $this->informeManager = $informeManager;
        $this->utilsManager = $utilsManager;
        $this->gmapsManager = $gmapsManager;
        $this->backendManager = $backendManager;
        $this->choferManager = $choferManager;
        $this->doctrine = $doctrine;
    }

    public function getExcesosByRecorrido($recorrido, $version)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\responsabilidad_vial\v' . $version . '\Exceso');
        $queryBuilder = $repository->createQueryBuilder('e');
        $queryBuilder->where('e.recorrido = :recorrido')  // Condición para el recorrido
            ->setParameter('recorrido', $recorrido);

        return $queryBuilder->getQuery()->getResult();
    }
    public function getInformeByUid($uid, $version)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\responsabilidad_vial\v' . $version . '\Informe');
        $result = $repository->findOneBy(array('uid' => $uid));
        //  die('////<pre>' . nl2br(var_export($result->getVelMaxRuta(), true)) . '</pre>////');
        return $result;
    }

    public function getCategoriaByReferenciaId($idRef)
    {

        if (!isset($this->categoriasCache[$idRef])) {
            $em = $this->doctrine->getManager('default');
            $repository = $em->getRepository('App\Entity\Referencia');
            $referencia = $repository->find($idRef);

            $this->categoriasCache[$idRef] = $referencia->getCategoria() ? $referencia->getCategoria()->getNombre() :  'Referencias Sin Categoría';
        }

        return $this->categoriasCache[$idRef];
    }

    public function processExcesos($excesos)
    {
        $arrayDefault = [
            'leve' => ['cantidad' => 0, 'tiempo' => 0, 'puntaje' => 0],
            'medio' => ['cantidad' => 0, 'tiempo' => 0, 'puntaje' => 0],
            'alto' => ['cantidad' => 0, 'tiempo' => 0, 'puntaje' => 0],
            'ir' => ['valor' => 0, 'color' => 'default'],
        ];

        $dataFuera = $dataDentro = [];       
        foreach ($excesos as $exceso) {
            if ($exceso->getReferencia() != null) {
                // Procesar datos dentro de referencias
                $idRef = $exceso->getReferencia()->getId();
                $categoria = $this->getCategoriaByReferenciaId($idRef);

                // Inicializamos si no existe
                if (!isset($dataDentro[$categoria])) {
                    $dataDentro[$categoria] = $arrayDefault;
                }

                $velMaxReferencia = $exceso->getPosicionInicial()->getMaximaPermitida();
                $velMax = $exceso->getMaximaPromedio();
                $tiempo = $exceso->getTiempoExceso();
                //    dd($exceso->getId(). ' '.$velMax);
                $strRiesgo = $this->getStrRiesgo($velMax, $velMaxReferencia);
         
                if ($strRiesgo != null) {
                    $dataDentro[$categoria][$strRiesgo]['cantidad']++;
                    $dataDentro[$categoria][$strRiesgo]['tiempo'] += $tiempo;
                    $dataDentro[$categoria][$strRiesgo]['puntaje'] += $this->getPuntaje($strRiesgo);
                    $dataDentro[$categoria][$strRiesgo][] = [
                        'prom' => $velMax,
                        'maxP' => $velMaxReferencia,
                        'tiempo' => $tiempo,
                        'f' => $exceso->getPosicionInicial()->getFecha()->format('d-M H:i:s')
                    ];
                }
            } else {
                // Procesar datos fuera de referencias
                if (!isset($dataFuera['Fuera'])) {
                    $dataFuera['Fuera'] = $arrayDefault;
                }

                $velMaxOsm = $exceso->getPosicionInicial()->getMaximaPermitida();
                $velMax = $exceso->getMaximaPromedio();
                $tiempo = $exceso->getTiempoExceso();
                $strRiesgo = $this->getStrRiesgo($velMax, $velMaxOsm);
                if ($strRiesgo != null) {
                    $dataFuera['Fuera'][$strRiesgo]['cantidad']++;
                    $dataFuera['Fuera'][$strRiesgo]['tiempo'] += $tiempo;
                    $dataFuera['Fuera'][$strRiesgo]['puntaje'] += $this->getPuntaje($strRiesgo);
                    $dataFuera['Fuera'][$strRiesgo][] = [
                        'prom' => $velMax,
                        'maxP' => $velMaxOsm,
                        'tiempo' => $tiempo,
                        'f' => $exceso->getPosicionInicial()->getFecha()->format('d-M H:i:s')
                    ];
                }
            }
        }       
     
        // Combinamos las zonas de fuera y dentro
        $zonas = $this->mergeZonas($dataFuera, $dataDentro);

        return $this->calcularIr($zonas);
    }

    /**
     * Combina las zonas acumuladas sin sobrescribir datos
     */
    private function mergeZonas($dataFuera, $dataDentro)
    {
        foreach ($dataDentro as $categoria => $valores) {
            if (!isset($dataFuera[$categoria])) {
                // Si no existe en $dataFuera, lo añadimos
                $dataFuera[$categoria] = $valores;
            } else {
                // Si ya existe, acumulamos los valores
                foreach ($valores as $nivel => $datos) {
                    if (isset($dataFuera[$categoria][$nivel])) {
                        // Acumulamos cantidades, tiempos y puntajes
                        $dataFuera[$categoria][$nivel]['cantidad'] += $datos['cantidad'];
                        $dataFuera[$categoria][$nivel]['tiempo'] += $datos['tiempo'];
                        $dataFuera[$categoria][$nivel]['puntaje'] += $datos['puntaje'];

                        // Añadimos los excesos
                        foreach ($datos as $key => $valor) {
                            if (is_int($key)) { // Evitamos sobreescribir los campos no numéricos (cantidad, tiempo, puntaje)
                                $dataFuera[$categoria][$nivel][] = $valor;
                            }
                        }
                    } else {
                        // Si no existe el nivel, lo añadimos completamente
                        $dataFuera[$categoria][$nivel] = $datos;
                    }
                }
            }
        }
        return $dataFuera;
    }

    public function getPuntaje($strRiesgo)
    {
        if ($strRiesgo == 'leve') {
            $puntaje = 1;
        } elseif ($strRiesgo == 'medio') {
            $puntaje = 1.5;
        } elseif ($strRiesgo == 'alto') {
            $puntaje = 2;
        }
        return $puntaje;
    }

    public function getStrRiesgo($velMedida, $velMaxPermitida)
    {
        // Calcular el límite tolerado para leve (10% adicional)
        $limiteLeve = floatval($velMaxPermitida * 0.1 + $velMaxPermitida);

        // Calcular el límite tolerado para medio (20% adicional)
        $limiteMedio = floatval($velMaxPermitida * 0.2 + $velMaxPermitida);
        // Definir los umbrales para clasificar el exceso de velocidad
        if ($velMedida <= $limiteLeve) {
            return "leve";  // El exceso está dentro del 10% tolerado
        } elseif ($velMedida > $limiteLeve && $velMedida <= $limiteMedio) {
            return "medio";  // El exceso está entre el 10% y el 20%
        } else {
            // dd("aca");
            return "alto";  // El exceso es mayor al 20%
        }
    }


    const COLOR_LEVE = 'success';
    const COLOR_MEDIO = 'warning';
    const COLOR_ALTO = 'danger';

    public function factorTiempo($tiempo)
    {
        if ($tiempo < 10) {  //segundos
            return 1;
        } elseif ($tiempo >= 10 && $tiempo < 30) {
            return 1.2;
        } elseif ($tiempo >= 30 && $tiempo < 60) {
            return 1.5;
        } else {
            return 2;
        }
    }

    public function calcularIr($zonas)
    {
        foreach ($zonas as $keyC => $categoria) {
            //  dd($categoria);
            if (isset($categoria['leve']) || isset($categoria['medio']) || isset($categoria['alto'])) {

                $leve = $categoria['leve']['puntaje']  * $this->factorTiempo($categoria['leve']['tiempo']);
                $medio = $categoria['medio']['puntaje']  * $this->factorTiempo($categoria['medio']['tiempo']);
                $alto = $categoria['alto']['puntaje'] * $this->factorTiempo($categoria['alto']['tiempo']);

                $ir = $leve + $medio + $alto;

                if ($ir < 500) {
                    $color = self::COLOR_LEVE;
                } elseif ($ir >= 500 && $ir < 1000) {
                    $color = self::COLOR_MEDIO;
                } else {
                    $color = self::COLOR_ALTO;
                }

                $zonas[$keyC]['ir'] = [
                    'valor' => $ir,
                    'color' => $color,
                ];
            }
        }
        return $zonas;
    }

    public function calcularIrTotal($zonas)
    {
        $data = ['valor' => 0, 'color' => 'defaut'];
        $ir = 0;
        foreach ($zonas as $zona) {
            $ir = $ir + $zona['ir']['valor'];

            $data['valor'] = $ir;
            if ($ir < 500) {
                $data['color'] = self::COLOR_LEVE;
            } elseif ($ir >= 500 && $ir < 1000) {
                $data['color'] = self::COLOR_MEDIO;
            } else {
                $data['color'] = self::COLOR_ALTO;
            }
        }
        return $data;
    }

    protected function numberToColumnLetter($n)
    {
        $result = '';
        while ($n > 0) {
            $n--;
            $result = chr(65 + ($n % 26)) . $result;
            $n = intdiv($n, 26);
        }
        return $result ?: 'A';
    }
}
