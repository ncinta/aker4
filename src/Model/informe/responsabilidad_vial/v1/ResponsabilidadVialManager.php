<?php

namespace App\Model\informe\responsabilidad_vial\v1;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\BackendManager;
use App\Model\app\ServicioManager;
use App\Model\app\ChoferManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use App\Model\informe\responsabilidad_vial\ResponsabilidadVialBaseManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Description of InformeManager
 *
 * @author nicolas
 */
class ResponsabilidadVialManager extends ResponsabilidadVialBaseManager
{




    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager,
        InformeManager $informeManager,
        UtilsManager $utilsManager,
        GmapsManager $gmapsManager,
        BackendManager $backendManager,
        ManagerRegistry $doctrine,
        ChoferManager $choferManager
    ) {
        // Simplemente llamamos al constructor de la clase base
        parent::__construct(
            $em,
            $servicioManager,
            $referenciaManager,
            $excelManager,
            $informeManager,
            $utilsManager,
            $gmapsManager,
            $backendManager,
            $doctrine,
            $choferManager
        );
    }

    public function getExcelResponsabilidadVial($data, $dataInforme)
    {
        $xls = $this->excelManager->create("Informe de Responsabilidad Vial");

        // Configuración del encabezado
        $xls->setHeaderInfo([
            'C2' => 'Servicio',
            'D2' => $dataInforme['nombreServicio'] ?? '---',
            'C3' => 'Fecha desde',
            'D3' => $dataInforme['desde'] ?? '---',
            'C4' => 'Fecha hasta',
            'D4' => $dataInforme['hasta'] ?? '---',
            'C5' => 'Referencias',
            'D5' => $dataInforme['nombreReferencia'] ?? '---',
            'C6' => 'Máx Referencia',
            'D6' => $dataInforme['max_referencia'] ?? '---',
            'C7' => 'Máx Fuera',
            'D7' => $dataInforme['max_fuera'] ?? '---',

        ]);

        $i = 9;

        // Aseguramos que 'Fuera' sea la primera categoría
        if (!isset($data['categorias'])) {
            $data['categorias'] = [];
        }
        $data['categorias'] = array_unique(array_merge(['Fuera'], $data['categorias']));

        // Configuración de columnas principales
        $xls->setBar($i, [
            'A' => ['title' => 'Servicio', 'width' => 30]
        ]);

        $startColumn = 'B';
        $currentColumnAscii = ord($startColumn);

        // Configuración de categorías y subcolumnas
        foreach ($data['categorias'] as $categoria) {
            $currentColumnLetter = $this->numberToColumnLetter($currentColumnAscii - 64);

            // Columna principal de categoría
            $xls->setCellColspan($i, [
                $currentColumnLetter => ['title' => $categoria, 'width' => 4]
            ]);

            // Subcolumnas
            $xls->setBar($i + 1, [
                chr($currentColumnAscii) => ['title' => 'Leve', 'width' => 10],
                chr($currentColumnAscii + 1) => ['title' => 'Medio', 'width' => 10],
                chr($currentColumnAscii + 2) => ['title' => 'Alto', 'width' => 10],
                chr($currentColumnAscii + 3) => ['title' => 'IR', 'width' => 10]
            ]);

            $currentColumnAscii += 4;
        }

        // Columnas finales usando numberToColumnLetter en lugar de chr()
        $currentColumn = $currentColumnAscii - 64; // Convertimos a número (A=1, B=2, etc.)
        $xls->setBar($i + 1, [
            $this->numberToColumnLetter($currentColumn) => ['title' => 'Total Hs.', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 1) => ['title' => 'Detenido', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 2) => ['title' => 'Ralentí', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 3) => ['title' => 'Total Kms.', 'width' => 20],
            $this->numberToColumnLetter($currentColumn + 4) => ['title' => 'IR Total', 'width' => 20]
        ]);

        // Datos de cada servicio
        $i += 2;
        foreach ($data as $key => $row) {
            if ($key === 'categorias') continue;

            $currentColumn = ord($startColumn) - 64; // Convertimos a número
            $xls->setBar($i, ['A' => ['title' => $key, 'width' => 30]]);

            // Datos por categoría
            foreach ($data['categorias'] as $categoria) {
                $valores = $row['zonas'][$categoria] ?? null;
                $xls->setBar($i, [
                    $this->numberToColumnLetter($currentColumn) => ['title' => $valores ? $valores['leve']['cantidad'] : '---', 'width' => 10],
                    $this->numberToColumnLetter($currentColumn + 1) => ['title' => $valores ? $valores['medio']['cantidad'] : '---', 'width' => 10],
                    $this->numberToColumnLetter($currentColumn + 2) => ['title' => $valores ? $valores['alto']['cantidad'] : '---', 'width' => 10],
                    $this->numberToColumnLetter($currentColumn + 3) => ['title' => $valores ? $valores['ir']['valor'] : '---', 'width' => 10]
                ]);
                $currentColumn += 4;
            }

            // Datos finales del servicio
            $xls->setBar($i, [
                $this->numberToColumnLetter($currentColumn) => ['title' => $row['tiempo_total'], 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 1) => ['title' => $row['tiempo_detenido'], 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 2) => ['title' => $row['tiempo_ralenti'], 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 3) => ['title' => $row['total_metros_recorridos'], 'width' => 20],
                $this->numberToColumnLetter($currentColumn + 4) => ['title' => $row['ir_total']['valor'], 'width' => 20]
            ]);

            $i++;
        }

        return $xls;
    }

    public function getResumen($uid, $ids, $desde, $hasta)
    {
        $data = [];
        foreach ($ids as $id) {    //recorro por servicios
            $servicio = $this->servicioManager->find(intval($id));
            $ks = $servicio->getNombre();
            $data[$ks]['total'] = 0;
            $data[$ks]['id'] = $servicio->getId();
            $data[$ks]['zonas'] = [];  // Esto debe estar vacío inicialmente

            $recorridos = $this->getRecorridosByServicio($uid, $servicio);   //obtengo los recorridos

            $totalMetrosRecorridos = 0;
            $tiempoRalenti = 0;
            $tiempoDetenido = 0;
            $tiempoTotal = 0;
            $excesos = [];

            foreach ($recorridos as $recorrido) {
                $excesos = array_merge($excesos, $this->getExcesosByRecorrido($recorrido, 1));  //traigo los excesos del recorrido                
                $tiempoTotal += $recorrido->getTiempoTotal();
                $tiempoDetenido += $recorrido->getTiempoDetenido();
                $tiempoRalenti += $recorrido->getTiempoRalenti();
                $totalMetrosRecorridos += $recorrido->getMetrosRecorridos();
            }
            // Procesamos los excesos y zonas
            if (count($excesos) > 0) {
                $zonasProcesadas = $this->processExcesos($excesos);
                // Combinamos las zonas procesadas con las zonas ya existentes
                foreach ($zonasProcesadas as $zona => $detalleZona) {
                    if (!isset($data[$ks]['zonas'][$zona])) {
                        $data[$ks]['zonas'][$zona] = $detalleZona;
                    } else {
                        // Aquí puedes manejar cómo combinar los detalles de zonas ya existentes
                        $data[$ks]['zonas'][$zona] = array_merge($data[$ks]['zonas'][$zona], $detalleZona);
                    }
                }
            }

            foreach ($excesos as $exceso) {
                foreach ($exceso->getPosiciones() as $posicion) {
                    if ($posicion->getReferencia()) {
                        $data[$ks]['referenciasHidden'][] = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    }
                }
            }

            // Asignamos los datos de tiempos y distancias al chofer
            $data[$ks]['tiempo_total'] = $this->utilsManager->segundos2tiempo($tiempoTotal);
            $data[$ks]['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($tiempoDetenido);
            $data[$ks]['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo($tiempoRalenti);
            $data[$ks]['total_metros_recorridos'] = $totalMetrosRecorridos / 1000;

            // Calculamos IR Total
            $data[$ks]['ir_total'] = $this->calcularIrTotal($data[$ks]['zonas']);
            $data[$ks]['referenciasHidden'] = [];
            $data[$ks]['total']++;
        }

        $data['categorias'] = array_unique($this->categoriasCache);
        ksort($data['categorias']);

        return $data;
    }




    public function getRecorridosByServicio($uid, $servicio)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\responsabilidad_vial\v1\Recorrido');
        $queryBuilder = $repository->createQueryBuilder('p')
            ->where('p.servicio = :servicio')
            ->andWhere('p.informe = :uid')
            ->setParameter('servicio', $servicio->getId())
            ->setParameter('uid', $uid);

        return $queryBuilder->getQuery()->getResult();
    }


    public function getPosicionesByServicio($uid, $servicio, $desde, $hasta)
    {
        //coloco solo las posiciones iniciales del exceso porque n oes necesario que sea tan detallado como prudencia vial
        $recorridos = $this->getRecorridosByServicio($uid, $servicio);
        $posiciones = [];
        foreach ($recorridos as $recorrido) {
            foreach ($recorrido->getExcesos() as $exceso) {
                $posInit = $exceso->getPosicionInicial()->getFecha();
                $posiciones[$posInit->getTimestamp()] =  $exceso->getPosicionInicial();
            }
        }
        ksort($posiciones);
        //   dd($posiciones);
        return $posiciones;
    }



    public function getDetalle($uid, $id, $desde, $hasta)
    {
        $servicio =  $this->servicioManager->find(intval($id));
        $posiciones = $this->getPosicionesByServicio($uid, $servicio, $desde, $hasta);
        return $this->getData($servicio, $desde, $hasta, $posiciones);
    }

    public function getReferenciasHidden($uid, $ids, $desde, $hasta)
    {

        $resumen = $this->getResumen($uid, $ids, $desde, $hasta);
        $referencias = array();
        foreach ($ids as $id) {
            $servicio = $this->servicioManager->find(intval($id));
            if (isset($resumen[$servicio->getNombre()])) {
                $referencias = array_merge($referencias, $resumen[$servicio->getNombre()]['referenciasHidden']);
            }
        }
        $referenciasHidden = $this->gmapsManager->castVisibilidadReferencias($referencias, false);
        // die('////<pre>' . nl2br(var_export(count($referencias), true)) . '</pre>////');
        return $referenciasHidden;
    }


    public function getData($servicio, $desde, $hasta, $posiciones)
    {
        $dataResumen = array();
        $dataDetalle = array();
        $dataDetalle['nombre'] = $servicio->getNombre();
        $dataDetalle['posiciones'] = [];
        $referencias = [];  //array con las referencias involucradas en los excesos
        foreach ($posiciones as $posicion) {
            $referencia = null;
            $fecha = $posicion->getFecha();

            $exceso = $posicion->getExceso();
            $velMaxPermitida = $posicion->getMaximaPermitida();
            $velMaxPromedio = $exceso->getMaximaPromedio();

            // QUITAR IF CUANDO EMMA SOLUCUIONE EL PROBLEMA DE DUPLICADOS-- TRIPLICADOS
            if (!$this->informeManager->fechaExiste($dataDetalle['posiciones'], $fecha)) { //eliminamos los duplicados, no sabemos porque se duplican
                if ($posicion->getReferencia()) {
                    $referencia = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    $referencias[$posicion->getReferencia()->getId()] = $referencia;
                }
                $dataDetalle['posiciones'][] = array(
                    'fecha' => $posicion->getFecha(),
                    'latitud' => $posicion->getLatitud(),
                    'longitud' => $posicion->getLongitud(),
                    'velocidad' => $velMaxPromedio,
                    'velMaxPermitida' => $velMaxPermitida,
                    'tipoExceso' => $posicion->getTipoExceso(),
                    'ubicacion' => $posicion->getReferencia() ? $referencia->getNombre() : $posicion->getOsmDireccion(),
                    'riesgo' => $this->getStrRiesgo($velMaxPromedio, $velMaxPermitida),
                    'tipo' => $posicion->getTipoExceso(),
                    'tiempoExceso' => $exceso != null ?  $this->utilsManager->segundos2tiempo($exceso->getTiempoExceso()) : 0,
                    'distanciaExceso' => $exceso != null ?  $exceso->getDistanciaRecorrida() / 1000 : 0,
                    'referencia' => $referencia,
                    'zona' => $referencia != null && $referencia->getCategoria() != null ? $referencia->getCategoria()->getNombre() : 'Referencias Sin Categoría',
                );
            }
        }
        // dd($dataDetalle);

        $referenciasShow = $this->gmapsManager->castReferencias($referencias);


        return  array(
            'data' => $dataDetalle,
            'referenciasShow' => $referenciasShow
        );
    }
   
}
