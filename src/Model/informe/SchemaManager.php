<?php
namespace App\Model\informe;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;

class SchemaManager
{
    private $connection;
    private $entityManager;

    public function __construct(Connection $connection, EntityManagerInterface $entityManager)
    {
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    public function changeSchema($schemaName = null)
    {
        //die('////<pre>' . nl2br(var_export(sprintf('SET search_path TO %s', $schemaName), true)) . '</pre>////');
        $this->connection->executeStatement(sprintf('SET search_path TO %s;', $schemaName != null ? $schemaName : 'public' ));
    }
}