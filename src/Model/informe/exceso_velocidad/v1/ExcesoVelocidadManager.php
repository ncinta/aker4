<?php

namespace App\Model\informe\exceso_velocidad\v1;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\BackendManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\GmapsManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * Description of InformeManager
 *
 * @author nicolas
 */
class ExcesoVelocidadManager
{

    protected $servicioManager;
    protected $referenciaManager;
    protected $em;
    protected $utilsManager;
    protected $informeManager;
    protected $excelManager;
    protected $gmapsManager;
    protected $backendManager;
    protected $doctrine;
    protected $userLoginManager;
    protected $categoriasCache = [];

    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager,
        InformeManager $informeManager,
        UtilsManager $utilsManager,
        GmapsManager $gmapsManager,
        BackendManager $backendManager,
        ManagerRegistry $doctrine,
        UserLoginManager $userLoginManager
    ) {

        $this->em = $em;
        $this->servicioManager = $servicioManager;
        $this->excelManager = $excelManager;
        $this->referenciaManager = $referenciaManager;
        $this->informeManager = $informeManager;
        $this->utilsManager = $utilsManager;
        $this->gmapsManager = $gmapsManager;
        $this->backendManager = $backendManager;
        $this->doctrine = $doctrine;
        $this->userLoginManager = $userLoginManager;
    }

    public function getExcelExcesoVelocidad($data, $dataInforme)
    {
        $xls = $this->excelManager->create("Informe de Exceso de Velocidad");
        $xls->setHeaderInfo(array(
            'C2' => 'Servicio',
            'D2' => $dataInforme['nombreServicio'] ?? '---',
            'C3' => 'Fecha desde',
            'D3' => $dataInforme['desde'] ?? '---',
            'C4' => 'Fecha hasta',
            'D4' => $dataInforme['hasta'] ?? '---',
            'C5' => 'Referencias',
            'D5' => $dataInforme['nombreReferencia'] ?? '---',
            'C6' => 'Velocidad Máx',
            'D6' => $dataInforme['velocidad_maxima'] ?? '---',
            'C7' => 'Mostrar Direcciones?',
            'D7' => isset($dataInforme['mostrar_direcciones']) && $dataInforme['mostrar_direcciones'] == '1'  ? 'Sí' : 'No',

        ));
        $i = 9;
        $xls->setBar($i, array(
            'A' => array('title' => 'Servicio', 'width' => 20),
            'B' => array('title' => 'Fecha', 'width' => 20),
            'C' => array('title' => 'Máxima Alcanzada', 'width' => 20),
            'D' => array('title' => 'Máxima Promedio', 'width' => 20),
            'E' => array('title' => 'Máxima Permitida', 'width' => 20),
            'F' => array('title' => 'Tiempo en Exceso', 'width' => 20),
            'G' => array('title' => 'Distancia en Exceso', 'width' => 20),
            'H' => array('title' => 'Ubicación', 'width' => 20),
        ));
        $i++;
        foreach ($data as  $row) {
            foreach ($row as $key => $servicio) {
                if ($key != 'total' && $key != 'servicio') {
                    foreach ($servicio as  $k => $exceso) {
                        if ($k != 'referenciasHidden') {
                            $xls->setRowValues($i, array(
                                'A' => array('value' => $key),
                                'B' => array('value' => $exceso['fecha']),
                                'C' => array('value' => $exceso['maxima_alcanzada']),
                                'D' => array('value' => $exceso['maxima_promedio']),
                                'E' => array('value' => $exceso['maxima_permitida']),
                                'F' => array('value' => $exceso['tiempo_total']),
                                'G' => array('value' => $exceso['total_metros_recorridos']),
                                'H' => array('value' => $exceso['ubicacion']),
                            ));
                            $i++;
                        }
                    }
                }
            }
        }


        return $xls;
    }

    public function getResumen($uid, $ids)
    {
        $dataFuera = [];
        $dataDentro = [];
        $data = [];
        foreach ($ids as $id) {
            $servicio = $this->servicioManager->find(intval($id));
            $recorridos = $this->getRecorridosByServicio($uid, $servicio);
            foreach ($recorridos as $recorrido) {
                $data[] = $this->processRecorrido($servicio, $recorrido);
            }
        }

        return $data;
    }

    private function processRecorrido($servicio, $recorrido)
    {
        $data = [];
        $data['total'] = 0;
        $data['servicio'] = $servicio->getId();
        $ks = $servicio->getNombre();
        $excesos = $this->getExcesosByRecorrido($recorrido);
        foreach ($excesos as $exceso) {
            $posInicial =  $exceso->getPosicionInicial();
            $referencia = $posInicial->getReferencia();
            $fecha =  $posInicial && $posInicial->getFecha() ? $posInicial->getFecha() : null;
            if ($fecha != null) { //sin fecha no sirve
                $fecha = $this->utilsManager->UTC2local($fecha, $this->userLoginManager->getTimezone());

                $kf = $fecha->getTimestamp(); // Usamos el timestamp para mayor eficiencia
                $data[$ks][$kf]['fecha'] = $fecha->format('d/m/Y H:i:s');
                $data[$ks][$kf]['maxima_alcanzada'] = $exceso->getMaximaAlcanzada() . ' km/h';
                $data[$ks][$kf]['maxima_promedio'] = $exceso->getMaximaPromedio() . ' km/h';
                $data[$ks][$kf]['tiempo_total'] = $this->utilsManager->segundos2tiempo($exceso->getTiempoExceso());
                $data[$ks][$kf]['total_metros_recorridos'] = ($exceso->getDistanciaRecorrida() / 1000) . ' kms';
                $data[$ks][$kf]['maxima_permitida'] = $referencia != null ? $referencia->getVelocidadMaxima() . ' km/h' : $posInicial->getMaximaPermitida() . ' km/h';;
                $data[$ks][$kf]['ubicacion'] = $referencia != null ? $referencia->getNombre() : $posInicial->getOsmDireccion();
                $data[$ks]['referenciasHidden'] = [];
                $data['total'] += 1;

                foreach ($exceso->getPosiciones() as $posicion) {
                    if ($posicion->getReferencia()) {
                        $data[$ks]['referenciasHidden'][] = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    }
                }
            }
        }

        // Ordenar por las claves $kf (que ahora son timestamps)
        //if (isset($data[$ks])) {
        //    ksort($data[$ks]);
        //}

        return $data;
    }

    public function getDetalle($uid, $id, $desde, $hasta)
    {
        $servicio =  $this->servicioManager->find(intval($id));
        $posiciones = $this->getPosicionesByServicio($uid, $servicio, $desde, $hasta);
        return $this->informeManager->getDetalle($servicio, $desde, $hasta, $posiciones);
    }


    public function getPosicionesByServicio($uid, $servicio, $desde, $hasta)
    {
        //coloco solo las posiciones iniciales del exceso porque n oes necesario que sea tan detallado como prudencia vial
        $recorridos = $this->getRecorridosByServicio($uid, $servicio);
        $posiciones = [];
        foreach ($recorridos as $recorrido) {
            foreach ($recorrido->getExcesos() as $exceso) {
                $posInit = $exceso->getPosicionInicial()->getFecha();
                $posiciones[$posInit->getTimestamp()] =  $exceso->getPosicionInicial();
            }
        }
        ksort($posiciones);
        // dd($posiciones);
        return $posiciones;
    }


    public function getInformeByUid($uid)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\exceso_velocidad\v1\Informe');
        $result = $repository->findOneBy(array('uid' => $uid));
        return $result;
    }

    public function getRecorridosByServicio($uid, $servicio)
    {
        $servicio = $servicio instanceof Servicio ?  $servicio->getId() : intval($servicio);
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\exceso_velocidad\v1\Recorrido');
        $queryBuilder = $repository->createQueryBuilder('p');
        $queryBuilder
            ->join('p.informe', 'i')
            ->where('p.servicio = :servicio')
            ->andWhere('i.uid = :uid')
            ->setParameter('servicio', $servicio)
            ->setParameter('uid', $uid);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getExcesosByRecorrido($recorrido)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\exceso_velocidad\v1\Exceso');
        $queryBuilder = $repository->createQueryBuilder('e')
            ->where('e.recorrido = :recorrido')  // Condición para el recorrido
            ->join('e.posicionInicial', 'pi')
            ->setParameter('recorrido', $recorrido)
            ->orderBy('pi.fecha', 'ASC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function getReferenciasHidden($uid, $ids, $desde, $hasta)
    {
        $resumen = $this->getResumen($uid, $ids, $desde, $hasta);
        $referencias = array();
        foreach ($ids as $id) {
            $servicio = $this->servicioManager->find(intval($id));
            //   die('////<pre>' . nl2br(var_export($resumen, true)) . '</pre>////');
            if (isset($resumen[$servicio->getNombre()])) {
                $referencias = array_merge($referencias, $resumen[$servicio->getNombre()]['referenciasHidden']);
            }
        }
        $referenciasHidden = $this->gmapsManager->castVisibilidadReferencias($referencias, false);
        // die('////<pre>' . nl2br(var_export(count($referencias), true)) . '</pre>////');
        return $referenciasHidden;
    }
}
