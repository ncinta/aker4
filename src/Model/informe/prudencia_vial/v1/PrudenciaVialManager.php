<?php

namespace App\Model\informe\prudencia_vial\v1;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\Evento as Evento;
use App\Entity\informe\Inbox;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use PhpParser\Node\Expr\FuncCall;

/**
 * Description of InformeManager
 *
 * @author nicolas
 */
class PrudenciaVialManager
{

    protected $servicioManager;
    protected $referenciaManager;
    protected $em;
    protected $utilsManager;
    protected $informeManager;
    protected $excelManager;
    protected $doctrine;
    protected $gmapsManager;

    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager,
        InformeManager $informeManager,
        UtilsManager $utilsManager,
        ManagerRegistry $doctrine,
        GmapsManager $gmapsManager
    ) {

        $this->em = $em;
        $this->servicioManager = $servicioManager;
        $this->excelManager = $excelManager;
        $this->referenciaManager = $referenciaManager;
        $this->informeManager = $informeManager;
        $this->utilsManager = $utilsManager;
        $this->doctrine = $doctrine;
        $this->gmapsManager = $gmapsManager;
    }

    public function getExcelPrudenciaVial($data, $dataInforme)
    {
        $xls = $this->excelManager->create("Informe de Prudencia Vial");
        $xls->setHeaderInfo(array(
            'C2' => 'Servicio',
            'D2' => $dataInforme['nombreServicio'] ?? '---',
            'C3' => 'Fecha desde',
            'D3' => $dataInforme['desde'] ?? '---',
            'C4' => 'Fecha hasta',
            'D4' => $dataInforme['hasta'] ?? '---',
            'C5' => 'Referencias',
            'D5' => $dataInforme['nombreReferencia'] ?? '---',
            'C6' => 'Máx Ruta',
            'D6' => $dataInforme['max_ruta'] ?? '---',
            'C7' => 'Máx Avenida',
            'D7' => $dataInforme['max_avenida'] ?? '---',

        ));
        $i = 9;
        foreach ($data as $row) {
            $xls->setBar($i, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Descripción', 'width' => 20),
                'C' => array('title' => 'Riesgo Leve', 'width' => 20),
                'D' => array('title' => 'Riesgo Medio', 'width' => 20),
                'E' => array('title' => 'Riesgo Alto', 'width' => 20),
            ));
            $i++;
            $xls->setRowValues($i, array(
                'A' => array('value' => $row['nombre']),
                'B' => array('value' => 'Referencias'),
                'C' => array('value' => $row['leve']['referencia']),
                'D' => array('value' => $row['medio']['referencia']),
                'E' => array('value' => $row['alto']['referencia']),

            ));
            $i++;
            $xls->setRowValues($i, array(
                'A' => array('value' => $row['nombre']),
                'B' => array('value' => 'Rutas y Avenidas'),
                'C' => array('value' => $row['leve']['osm']),
                'D' => array('value' => $row['medio']['osm']),
                'E' => array('value' => $row['alto']['osm']),

            ));
            $i = $i + 2;
        }
        return $xls;
    }

    public function getResumen($uid, $ids, $desde, $hasta)
    {
        $data = array();
        $dataResumen = array();
        $dataDetalle = array();
    
        foreach ($ids as $id) {
            $servicio =  $this->servicioManager->find(intval($id));
            //$posiciones = $this->getPosicionesByServicio($uid, $servicio->getId(), $desde, $hasta);
            $posiciones = $this->getPosicionesByExceso($uid, $servicio->getId(), $desde, $hasta);
            $dataDetalle['posiciones'] = [];
            $dataResumen[$id] = [
                'total' => 0,
                'nombre' => $servicio->getNombre(),
                'referenciasHidden' => [],
                'leve' => ['referencia' => 0, 'osm' => 0],
                'medio' => ['referencia' => 0, 'osm' => 0],
                'alto' => ['referencia' => 0, 'osm' => 0],
                // Asegúrate de incluir todas las posibles categorías de 'riesgo'
            ];
            $dataResumen[$id]['referenciasHidden'] = [];
            foreach ($posiciones as $posicion) {
                $fecha = $posicion->getFecha();
                // QUITAR IF CUANDO EMMA SOLUCUIONE EL PROBLEMA DE DUPLICADOS-- TRIPLICADOS
                if (!$this->informeManager->fechaExiste($dataDetalle['posiciones'], $fecha)) { // eliminamos los duplicados
                    if ($posicion->getReferencia()) {
                        $dataResumen[$id]['referenciasHidden'][] = $this->referenciaManager->find($posicion->getReferencia()->getId());
                    }
                    $dataDetalle['posiciones'][] = array('fecha' => $fecha);
                    // Actualizar el resumen de datos
                    $dataResumen[$id]['total'] += 1;
                    $riesgo = $posicion->getRiesgo();
                    $tipoReferencia = $posicion->getReferencia() ? 'referencia' : 'osm';
                    $dataResumen[$id][$riesgo][$tipoReferencia] += 1;
                }
            }
        }
        return $dataResumen;
    }

    public function getDetalle($uid, $id, $desde, $hasta)
    {
        $servicio =  $this->servicioManager->find(intval($id));
        //$posiciones = $this->getPosicionesByServicio($uid, $servicio->getId(), $desde, $hasta);
        $posiciones = $this->getPosicionesByExceso($uid, $servicio->getId());
        return $this->informeManager->getDetalle($servicio, $desde, $hasta, $posiciones);
    }

    public function getInformeByUid($uid)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\prudencia_vial\v1\Informe');
        $result = $repository->findOneBy(array('uid' => $uid));
        return $result;
    }

    public function getPosicionesByServicio($uid, $servicio, $desde, $hasta)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\prudencia_vial\v1\Posicion');
        $queryBuilder = $repository->createQueryBuilder('p');
        // die('////<pre>' . nl2br(var_export($uid, true)) . '</pre>////');

        $queryBuilder
            ->join('p.exceso', 'e')
            ->join('e.informe', 'i')
            ->where('p.servicio = :servicio')
            ->andWhere('i.uid = :uid')
            ->andWhere('p.fecha > :startDate AND p.fecha < :endDate')
            ->setParameter('uid', $uid)
            ->setParameter('servicio', $servicio)
            ->setParameter('startDate', $desde)
            ->setParameter('endDate', $hasta);
        return $queryBuilder->getQuery()->getResult();
    }

    public function getPosicionesByExceso($uid, $servicio)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $posiciones = [];
        $excesos = $this->getExcesos($uid, $servicio);
        foreach ($excesos as $exceso) {
            $posiciones[] = $exceso->getPosicionInicial();
        }
        return $posiciones;
    }

    public function getExcesos($uid, $servicio)
    {
        //cuando apuntamos a la nube de informes hay que seleccionar el manager de informes
        $em = $this->doctrine->getManager('informes');
        $repository = $em->getRepository('App\Entity\informe\prudencia_vial\v1\Exceso');
        $queryBuilder = $repository->createQueryBuilder('e');
        // die('////<pre>' . nl2br(var_export($uid, true)) . '</pre>////');

        $queryBuilder
            ->join('e.informe', 'i')
            ->where('e.servicio = :servicio')
            ->andWhere('i.uid = :uid')
            ->setParameter('uid', $uid)
            ->setParameter('servicio', $servicio)
        ;
        return $queryBuilder->getQuery()->getResult();
    }

    public function getReferenciasHidden($uid, $ids, $desde, $hasta)
    {
        $resumen = $this->getResumen($uid, $ids, $desde, $hasta);
        $referencias = array();
        foreach ($ids as $id) {
            $referencias = array_merge($referencias, $resumen[$id]['referenciasHidden']);
        }
        $referenciasHidden = $this->gmapsManager->castVisibilidadReferencias($referencias, false);
        // die('////<pre>' . nl2br(var_export(count($referencias), true)) . '</pre>////');
        return $referenciasHidden;
    }
}
