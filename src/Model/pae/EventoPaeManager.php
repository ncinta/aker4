<?php

namespace App\Model\pae;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\EventoManager;
use App\Model\app\ReferenciaManager;
use Psr\Log\LoggerInterface;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\ChoferManager;
use DateTime;
use DateTimeZone;

/**
 * Description of pae/EventoManager
 *
 * @author claudio
 */
class EventoPaeManager
{

    private $logear = true;

    protected $em;
    protected $userlogin;
    protected $utilsManager;
    protected $container;
    protected $eventoManager;
    private $logger;
    private $notificadorAgenteManager;
    protected $cliente;
    protected $referenciaManager;
    protected $choferManager;

    public function __construct(
        EntityManagerInterface $em,
        UserLoginManager $userlogin,
        UtilsManager $utilsManager,
        EventoManager $eventoManager,
        ContainerInterface $container,
        LoggerInterface $logger,
        NotificadorAgenteManager $notificadorAgenteManager,
        HttpClientInterface $cliente,
        ReferenciaManager $referenciaManager,
        ChoferManager $choferManager
    ) {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->utilsManager = $utilsManager;
        $this->container = $container;
        $this->eventoManager = $eventoManager;
        $this->logger = $logger;
        $this->cliente = $cliente;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->referenciaManager = $referenciaManager;
        $this->choferManager = $choferManager;
    }

    public function procesar($servicios)
    {
        if (empty($servicios)) {
            $this->log('NO VIENEN SERVICIOS PARA PROCESAR.');
            return false;
        }

        $this->log(sprintf('procesar %d servicios', count($servicios)));

        // Lista de eventos a procesar
        $eventos = [
            'pae_id_evento_referencia',
            'pae_id_evento_velocidad_referencia',
            'pae_id_evento_velocidad',
            'pae_id_evento_motivotx',
            'pae_id_evento_velocidad_extrema',
            'pae_id_evento_infraccion_velocidad',
            'pae_id_evento_infraccion_velocidad_referencia',
        ];

        foreach ($eventos as $eventoParam) {
            $evento = $this->obtenerEvento($this->container->getParameter($eventoParam));
            if ($evento) {
                $this->procesarEvento($evento, $servicios);
            }
        }

        return true;
    }

    private function procesarEvento($evento, $servicios)
    {
        if ($this->updateEvento($evento, $servicios)) {
            //hubieron cambios entonces se debe notificar el evento a los agentes.
            $this->log('hay cambios en el evento, actualizar agente');
            $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
        } else {
            $this->log('no hay cambios en el evento');
        }
    }

    public function updateEvento($evento, $serviciosActivos)
    {

        $hayCambios = false;
        //primer pasada -> borrar lo que no tengo activo
        foreach ($evento->getServicios() as $servicio) {
            //buscar el servicio del evento en el servicio activo
            if (!in_array($servicio, $serviciosActivos)) {   // no lo tengo                
                $this->log(sprintf('sacando servicio -> serv: %s evento: %d', $servicio->getNombre(), $evento->getId()));
                $this->eventoManager->removeServicioEv($servicio, $evento);  //borro el servicio del evento
                $hayCambios = true;   //levanto la bandera de cambios
            }
        }

        //segunda pasada -> agrego lo nuevo        
        foreach ($serviciosActivos as $servicioAdd) {
            //busco el servicio en el evento                       
            if (!$this->eventoManager->inServicio($servicioAdd, $evento)) {
                $this->log(sprintf('agregando servicio -> serv: %s evento: %d', $servicioAdd->getNombre(), $evento->getId()));
                $this->eventoManager->saveServicioEv($servicioAdd, $evento);
                $hayCambios = true;   //levanto la bandera de cambios
            }
        }

        return $hayCambios;
    }

    /**
     * Para un evento_id pasado por parametro obtiene el objeto evento para su procesamiento 
     */
    private function obtenerEvento($evento_id)
    {
        return $this->eventoManager->findById($evento_id);
    }

    private function log($str)
    {
        if ($this->logear) {
            $this->logger->notice('pae | ' . $str);
        }
    }

    public function isPae($evento)
    {
        $pae_id = $this->container->getParameter('pae_cliente_id');
        $habilitado = $this->container->getParameter('pae_habilitado');
        return $habilitado && $evento->getOrganizacion()->getId() == $pae_id;
    }

    private function obtenerConfiguracionEventos(): array
    {
        return [
            $this->container->getParameter('pae_id_evento_referencia') => [
                'EV_IO_REFERENCIA:INGRESO' => ['tipoEvento' => '2', 'datoKey' => 'hacia', 'campo' => 'id'],
                'EV_IO_REFERENCIA:SALIDA' => ['tipoEvento' => '3', 'datoKey' => 'desde', 'campo' => 'id'],
            ],
            $this->container->getParameter('pae_id_evento_motivotx') => [
                'EV_MOTIVOTX:MOTIVO' => [
                    'codename' => [
                        'VAR_APAGADO' => '5',
                        'VAR_ENCENDIDO' => '4',
                        'VAR_ACELERACION' => '9',
                        'VAR_FRENADA' => '10',
                        'VAR_LOGUEO' => '6',
                        'VAR_DESLOGUEO' => '6',
                        'VAR_VELOCIDAD_EXTREMA' => '11'
                    ],
                ],
            ],
            $this->container->getParameter('pae_id_evento_velocidad_referencia') => [
                'EV_EXCESO_VELOCIDAD_REFERENCIA:EXCESO_VELOCIDAD_REFERENCIA' => [
                    'tipoEvento' => '7',
                    'posicionInicial' => true,
                    'dato01' => 'velocidad_maxima|tiempo_total',
                ],
            ],
            $this->container->getParameter('pae_id_evento_velocidad') => [
                'EV_EXCESO_VELOCIDAD:EXCESO_VELOCIDAD' => [
                    'tipoEvento' => '8',
                    'posicionInicial' => true,
                    'dato01' => 'velocidad_maxima|tiempo_total',
                ],
            ],
            $this->container->getParameter('pae_id_evento_velocidad_extrema') => [
                'EV_EXCESO_VELOCIDAD:EXCESO_VELOCIDAD' => [
                    'tipoEvento' => '11',
                    'posicionInicial' => true,
                    'dato01' => 'velocidad_maxima|tiempo_total',
                ],
            ],
            $this->container->getParameter('pae_id_evento_infraccion_velocidad_referencia') => [
                'EV_EXCESO_VELOCIDAD_REFERENCIA:EXCESO_VELOCIDAD_REFERENCIA' => [
                    'tipoEvento' => '12',
                    'posicionInicial' => true,
                    'dato01' => 'velocidad_maxima|tiempo_total',
                ],
            ],
            $this->container->getParameter('pae_id_evento_infraccion_velocidad') => [
                'EV_EXCESO_VELOCIDAD:EXCESO_VELOCIDAD' => [
                    'tipoEvento' => '13',
                    'posicionInicial' => true,
                    'dato01' => 'velocidad_maxima|tiempo_total',
                ],
            ],
        ];
    }

    /**
     * Debo tomar el evento y hacer el procesamiento para PAE segun corresponda
     */
    public function procesarNotificacion($evento, $servicio, $fechaEV, $data)
    {
        $configuracionEventos = $this->obtenerConfiguracionEventos();
        // dd($configuracionEventos);
        $idEvento = $evento->getId();
        $tipoEvento = $dato01 = $dato02 = null;
        $latitud = $longitud = $rumbo = $velocidad = null;
        $codigoChofer = "T=01";

        // Validar posición genérica
        if (isset($data['data']['posicion'])) {
            $latitud = $data['data']['posicion']['latitud'];
            $longitud = $data['data']['posicion']['longitud'];
            $rumbo = $data['data']['posicion']['direccion'];
            $velocidad = $data['data']['posicion']['velocidad'];
        }

        // Procesar eventos según configuración        
        if (array_key_exists($idEvento, $configuracionEventos)) {
            $eventoConfig = $configuracionEventos[$idEvento];
            $mensaje = $data['mensaje'];
            if (isset($eventoConfig[$mensaje])) {

                $ok = true;
                $config = $eventoConfig[$mensaje];

                // Tipo de evento
                if (isset($config['tipoEvento'])) {
                    $tipoEvento = $config['tipoEvento'];
                }

                // Dato específico (como referencias)
                if (isset($config['datoKey'], $config['campo'])) {
                    $dato01 = $this->getNombreReferencia($data['data'][$config['datoKey']][0][$config['campo']]);
                }

                // Codename específico para ciertos eventos
                if (isset($config['codename'])) {
                    $codename = $data['data']['codename'] ?? null;
                    $tipoEvento = $config['codename'][$codename] ?? null;
                }

                // Datos de posición inicial
                if (!empty($config['posicionInicial'])) {
                    $latitud = $data['data']['posicion_inicial']['latitud'] ?? $latitud;
                    $longitud = $data['data']['posicion_inicial']['longitud'] ?? $longitud;
                    $rumbo = $data['data']['posicion_inicial']['direccion'] ?? $rumbo;
                    $velocidad = $data['data']['posicion_inicial']['velocidad'] ?? $velocidad;
                }

                // Dato adicional, como velocidad máxima y duración
                if (!empty($config['dato01'])) {
                    $campos = explode('|', $config['dato01']);
                    $dato01 = sprintf(
                        'MAX=%d|DUR=%s',
                        $data['data'][$campos[0]] ?? 0,
                        $this->convertirASegundos($data['data'][$campos[1]] ?? 0)
                    );
                }
                
                //buscar datos del chofer                                
                if (isset($data['reporte']['ibutton'])) {
                    $ibutton = $data['reporte']['ibutton'];
                    // Validar formato del iButton (F858N330500)
                    if (preg_match('/^F(\d+)N(\d+)$/', $ibutton, $matches)) {
                        $facility = $matches[1]; // números entre F y N
                        $numberTarget = $matches[2];  // números después de N
                        $codigoChofer = sprintf('T=02|F=%d|N=%d', $facility, $numberTarget);                                            
                    }
                }
            }
        }

        // Generar y enviar notificación si el evento es válido
        if ($ok ?? false) {
            $servidorFH = new \DateTime('now', new \DateTimeZone('UTC'));
            $patente = $servicio->getVehiculo()->getPatente() ?? $servicio->getNombre();
            $odometro = intval($servicio->getUltOdometro() ?? 0) / 1000;

            $dataEnvio = [
                'dominio' => $patente,
                'eventoFH' => $fechaEV->format('Y-m-d\TH:i:s'),
                'servidorFH' => $servidorFH->format('Y-m-d\TH:i:s'),
                'eventoTipo' => $tipoEvento,
                'latitud' => $latitud,
                'longitud' => $longitud,
                'velocidad' => $velocidad,
                'rumbo' => $rumbo,
                'odometro' => $odometro,
                'codigoChofer' => $codigoChofer,
            ];

            if (!is_null($dato01)) {
                $dataEnvio['dato01'] = trim($dato01);
            }
            if (!is_null($dato02)) {
                $dataEnvio['dato02'] = trim($dato02);
            }
          
            $this->log('notificacion -> ' . json_encode($dataEnvio));
            return $this->sendNotificacion($dataEnvio);
        }

        return true;
    }
  

    private function getNombreReferencia($idRef)
    {
        $referencia = $this->referenciaManager->find($idRef);
        if (!is_null($referencia->getCodigoExterno())) {
            return $referencia->getCodigoExterno();
        } else {
            return $referencia->getNombre();
        }
    }

    /**
     * Envia a PAE la notificacion de los eventos
     */
    private function sendNotificacion($json)
    {
        $result = [
            'statusCode' => null,
            'message' => 'ERROR',
        ];

        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('pae_endpoint') . 'evento',
                [
                    'body' => json_encode($json),
                    'timeout' => 10,  // 10 segundos de espera
                    'http_version' => '1.1', // Forzar HTTP/1.1
                    'headers' => [
                        'Access-Control-Allow-Origin' => '*',
                        'Content-Type' => 'application/json',
                        'api_key' => $this->container->getParameter('pae_apikey'),
                    ],
                ]
            );

            $result['statusCode'] = $response->getStatusCode();
            $result['message'] = $response->getContent();
            $this->log(sprintf('sendNotificacion -> (%d) %s', $result['statusCode'], $result['message']));

            if ($result['statusCode'] === 200) {
                return $result;
            } else {
                $this->log(sprintf('Error sendNotificacion -> (%d) %s', $result['statusCode'], $result['message']));
            }
        } catch (TransportException $e) {
            $this->log(sprintf(
                'Error sendNotificacion -> e: %s',
                $e->getMessage()
            ));
            $result['message'] = $e->getMessage();
        }

        return $result;
    }


    private function convertirASegundos($tiempo)
    {
        // Remover la 's' del final y convertir el valor a un entero
        $segundos = (int) rtrim($tiempo, 's');

        // Convertir los segundos a horas, minutos y segundos
        $horas = floor($segundos / 3600);
        $minutos = floor(($segundos % 3600) / 60);
        $segundosRestantes = $segundos % 60;

        // Formatear el resultado a HH:MM:SS
        return sprintf('%02d:%02d:%02d', $horas, $minutos, $segundosRestantes);
    }

    public function grabarRespuesta($evento, $historial, $result)
    {
        try {
            $historial->setFechaVista(new \DateTime('', new \DateTimeZone('UTC')));
            $historial->setRespuesta(sprintf('%d: %s', $result['statusCode'], $result['message']));
            $this->em->persist($historial);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            $this->log(sprintf('Error grabarRespuesta -> e: %s', $e->getMessage()));
            return false;
        }
    }
}
/**
 * EventoPaeManager.php on line 132:
array:7 [
  "uid" => "aaaaaaaa-bbbb-cccc-dddd-5"
  "mensaje" => "EV_IO_REFERENCIA:INGRESO"
  "version" => "1.0"
  "data" => array:2 [
    "hacia" => array:1 [
      0 => array:2 [
        "id" => 55771
        "nombre" => "(DEPOSITO) ZARATE (ZARCAM ZTE)"
      ]
    ]
    "posicion" => array:4 [
      "direccion" => 352
      "latitud" => -34.08259
      "longitud" => -59.06955
      "velocidad" => 10
    ]
  ]
  "estado" => array:2 [
    "sin_destinatarios" => true
    "middleware" => []
  ]
  "emails" => []
  "tokens" => []
]
 */

/**
 * REPORTE DE EGRESO DEL ÁREA OPERATIVA 03 POR EL ACCESO 02
{
"dominio": "ABC123DE",
"eventoFH": "2021-07-26T18:03:21",
"servidorFH": "2021-07-26T18:03:23",
"eventoTipo": "3",
"latitud": "-38.70082",
"longitud": "-68.50139",
"velocidad": "25",
"rumbo": "30",
"odometro": "95.9",
"choferCodigo": "T=03|N=12345678",
"dato01": "AREA=03|ACC=02"
}
 */

/**
 * REPORTE DE EXCESO DE VELOCIDAD EN RUTA
{
"dominio": "ABC123DE",
"eventoFH": "2021-07-26T18:03:21",
"servidorFH": "2021-07-26T18:03:23",
"eventoTipo": "8",
"latitud": "-38.86287",
"longitud": "-68.05958",
"velocidad": "119",
"rumbo": "144",
"odometro": "15695.00",
"choferCodigo": "T=03|N=12345678",
"dato01": "MAX=121|DUR=00:00:15"
}
 */
