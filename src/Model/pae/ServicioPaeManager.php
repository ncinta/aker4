<?php

namespace App\Model\pae;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UtilsManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use App\Model\app\ServicioManager;

/**
 * Description of pae/EventoManager
 *
 * @author claudio
 */
class ServicioPaeManager
{

    private $logear = true;

    protected $em;
    protected $utilsManager;
    protected $container;
    private $logger;
    protected $cliente;
    private $servicioManager;

    public function __construct(
        EntityManagerInterface $em,
        UtilsManager $utilsManager,
        ContainerInterface $container,
        LoggerInterface $logger,
        ServicioManager $servicioManager,
        HttpClientInterface $cliente
    ) {
        $this->em = $em;
        $this->utilsManager = $utilsManager;
        $this->container = $container;
        $this->logger = $logger;
        $this->cliente = $cliente;
        $this->servicioManager = $servicioManager;
    }

    /**
     * Pide a PAE el padron de vehiculos activos
     */
    private function getPadron()
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'GET',
                $this->container->getParameter('pae_endpoint') . 'vehiculosPadron',
                array(
                    'timeout' => 10,  //10seg de espera
                    'http_version' => '1.1', // Forzar HTTP/1.1
                    'headers' => array(
                        'Access-Control-Allow-Origin' => '*',
                        'Content-Type' => 'application/json',
                        'api_key' => $this->container->getParameter('pae_apikey')
                    )
                )
            );

            $statusCode = $response->getStatusCode();

            if ($statusCode === 200) {
                $content = $response->getContent();                
                $this->log('Content -> '. $content);
                return $content;
            } else {
                $this->log('Error getPadron -> '. $response->getStatusCode());
            }
        } catch (TransportException $e) {
            $this->log('Excception getPadron -> '. $e->getMessage());
            return false;
        }

        return $statusCode === 200 ? true : false;
    }

    /**
     * Pido el padron a PAE y busco los servicios devolviendo el array con los servicios activos
     */
    public function obtenerPadron()
    {
        $json = json_decode($this->getPadron(), true);
        $this->log('padron -> '. json_encode($json));
        $patentes = [];    
        $servicios = null;    
        if ($json) {
            
            //obtengo solo las patentes
            foreach ($json['grupos'] as $grps) {                
                foreach ($grps['dominios'] as $dominio) {
                    $patentes[] = $dominio;
                }                
                
            }

            $patentes[] = 'HXR249';   //agrego una patente de prueba servicio TAU de Sandoval
            //proceso las patentes y obtengo los servicios
            foreach ($patentes as $patente) {
                $servicios[] = $this->servicioManager->findByPatente($patente);
            }

        }

        return $servicios;
    }

    private function log($str)
    {
        if ($this->logear) {
            $this->logger->notice('pae_servicio| ' . $str);
        }
    }
}
