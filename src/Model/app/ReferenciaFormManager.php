<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Model\app\UserLoginManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class ReferenciaFormManager
{

    protected $userlogin;
    protected $formfactory;
    protected $em;

    public function __construct(UserLoginManager $userlogin, FormFactoryInterface $formfactory, EntityManagerInterface $em)
    {
        $this->userlogin = $userlogin;
        $this->formfactory = $formfactory;
        $this->em = $em;
    }

    /**
     * Crea el formulario para que se pueda crear una nueva referencia en el mapa.
     * @param Organizacion $org
     * @return type
     */
    public function createReferenciaForm($org)
    {
        $choice = array();
        $choice[$org->getId()] = $org->getNombre();
        if (!is_null($org->getOrganizacionPadre())) {
            $choice[$org->getOrganizacionPadre()->getNombre()] = $org->getOrganizacionPadre()->getId();
            //$choice[$org->getOrganizacionPadre()->getId()] = $org->getOrganizacionPadre()->getNombre();
        }
        $choice[$this->userlogin->getOrganizacion()->getNombre()] = $this->userlogin->getOrganizacion()->getId();

        $tipos = array();
        foreach ($this->em->getRepository('App:TipoReferencia')->findTipos() as $data) {
            $tipos[$data->getNombre()] = $data->getId();
        }

        $form = $this->formfactory->createNamedBuilder(
            'formcr',
            FormType::class,
            array(
                'id' => $org->getId()
            )
        )
            ->add('organizacion', ChoiceType::class, array(
                'preferred_choices' => array($org->getId()),
                'choices' => $choice,
            ))
            ->add('tipoDibujo', ChoiceType::class, array(
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'choices' => array(
                    'Circular' => 1,
                    'Poligonal' => 2,
                ),
                'required' => true,
            ))
            ->add('nombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => true,
            ))
            ->add('identificador', TextType::class, array(
                'label' => 'Identificador',
                'required' => false,
            ))
            ->add('clase', HiddenType::class)
            ->add('pathIcono', HiddenType::class)
            ->add('poligono', HiddenType::class)
            ->add('centro', HiddenType::class)
            ->add('radio', HiddenType::class)
            ->add('color', HiddenType::class)
            ->add('transparencia', HiddenType::class)
            ->add('area', HiddenType::class)
            ->add('categoria', EntityType::class, array(
                'label' => 'Categoria',
                'class' => 'App:Categoria',
                'required' => false,
                'query_builder' => $this->em->getRepository('App:Categoria')->getQueryCategoriasDisponibles($org),
            ))
            ->add('tipoReferencia', ChoiceType::class, array(
                'label' => 'Tipo',
                'choices' => $tipos,
                'preferred_choices' => array(1),
            ))
            ->add('codigoExterno', TextType::class, array(
                'label' => 'Identificador',
                'required' => false,
            ));

        return $form->getForm();
    }

    /**
     * Crea el formulario para que se pueda redibujar las referencias.
     * @param Organizacion $org
     * @return type
     */
    public function createRedibujoForm($org)
    {
        $form = $this->formfactory->createNamedBuilder(
            'formrr',
            FormType::class,
            array(
                'id' => $org->getId()
            )
        );
        $form->add('nombre', TextType::class, array(
            'label' => 'Nombre',
            'required' => true,
        ))
            ->add('referencia_id', HiddenType::class)
            ->add('pathIcono', HiddenType::class)
            ->add('poligono', HiddenType::class)
            ->add('centro', HiddenType::class)
            ->add('radio', HiddenType::class)
            ->add('color', HiddenType::class)
            ->add('transparencia', HiddenType::class)
            ->add('area', HiddenType::class);
        return $form->getForm();
    }
}
