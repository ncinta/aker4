<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion;
use App\Entity\HoraTaller;

/**
 * Description of HoraTallerManager
 *
 * @author nicolas
 */
class HoraTallerManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('h')
            ->from('App:horaTaller', 'h')
            ->join('h.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('h.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('h.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        return $query->getQuery()->getResult();
    }

    public function create(Organizacion $organizacion)
    {
        $hora = new HoraTaller();
        $hora->setOrganizacion($organizacion);

        return $hora;
    }

    public function save(HoraTaller $hora)
    {
        //die('////<pre>' . nl2br(var_export($r->getId(), true)) . '</pre>////');
        $this->em->persist($hora);
        $this->em->flush();
        return $hora;
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:HoraTaller')->byOrganizacion($organizacion, array('p.nombre' => 'ASC'));
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($organizacion);
            return $this->em->getRepository('App:HoraTaller')->byOrganizacion($organizacion->getId(), array('p.nombre' => 'ASC'));
        }
    }

    public function find($hora)
    {
        if ($hora instanceof HoraTaller) {
            return $this->em->getRepository('App:HoraTaller')->findOneBy(array('id' => $hora->getId()));
        } else {
            return $this->em->getRepository('App:HoraTaller')->findOneBy(array('id' => $hora));
        }
    }

    public function deleteById($id)
    {
        $horataller = $this->find($id);
        if (!$horataller) {
            throw $this->createNotFoundException('Código de hora taller no encontrado.');
        }
        return $this->delete($horataller);
    }

    public function delete(HoraTaller $horataller)
    {
        $this->em->remove($horataller);
        $this->em->flush();
        return true;
    }
}
