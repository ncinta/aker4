<?php

/**
 * Description of ServicioManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Servicio as Servicio;
use App\Entity\GrupoServicio as GrupoServicio;
use App\Entity\Usuario as Usuario;
use App\Entity\RegistroShell as RegistroShell;
use App\Entity\Parametro as Parametro;
use App\Entity\Variable as Variable;
use App\Entity\Vehiculo;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ProgramacionManager;
use App\Model\app\GmapsManager;
use App\Model\app\GeocoderManager;
use App\Model\app\ChoferManager;
use App\Model\fuel\TanqueManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ServicioManager
{

    protected $em;
    protected $userlogin;
    protected $servicio;
    protected $servicioOld;
    protected $utils;
    protected $bitacora;
    protected $prg;
    protected $gmaps;
    protected $geocoder;
    protected $chofer;
    protected $tanque;
    protected $grupoServicio;

    public function __construct(
        EntityManagerInterface $em,
        UserLoginManager $userlogin,
        UtilsManager $utils,
        BitacoraServicioManager $bitacora,
        ProgramacionManager $programador,
        GmapsManager $gmaps,
        GeocoderManager $geocoder,
        ChoferManager $chofer,
        TanqueManager $tanque,
        GrupoServicioManager $grupoServicio
    ) {
        $this->userlogin = $userlogin;
        $this->em = $em;
        $this->utils = $utils;
        $this->bitacora = $bitacora;
        $this->prg = $programador;
        $this->gmaps = $gmaps;
        $this->geocoder = $geocoder;
        $this->chofer = $chofer;
        $this->tanque = $tanque;
        $this->grupoServicio = $grupoServicio;
    }

    /**
     * Convierte la hora de los servicios a timezone del usuario
     * @param type $servicios
     * @return type $servicios
     */
    private function setTimeZoneforAll($servicios, $usr = null)
    {
        if ($servicios) {
            if ($this->userlogin instanceof Usuario) {
                //convierto la hora a la del usuario.
                foreach ($servicios as $servicio) {
                    $this->setTimeZone($servicio, $usr);
                }
            }
        }
        return $servicios;
    }

    /**
     * Convierte la hora del servicio a timezone del usuario 
     * @param type $servicio
     * @return Servicio $servicio
     */
    public function setTimeZone($servicio, $usr = null)
    {
        if (!is_null($servicio)) {
            if ($servicio->getUltfechahora()) {
                if ($usr != null) {
                    $servicio->getUltfechahora()->setTimezone(new \DateTimeZone($usr->getTimezone()));
                } else {
                    if ($this->userlogin->getUser() instanceof Usuario) {
                        $servicio->getUltfechahora()->setTimezone(new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
                    } else {
                        $servicio->getUltfechahora()->setTimezone(new \DateTimeZone('America/Buenos_Aires'));
                    }
                }
            }
        }
        return $servicio;
    }

    /**
     * Busca un determinado servicio y lo devuelve. 
     * @param \App\Entity\Servicio $servicio
     * @return \App\Entity\Servicio $servicio
     */
    public function find($servicio, $settimezone = true)
    {
        if ($servicio instanceof Servicio) {
            $this->servicio = $this->em->find('App:Servicio', $servicio->getId());
        } else {
            $this->servicio = $this->em->find('App:Servicio', $servicio);
        }
        if ($this->servicio) {
            $this->servicioOld = $this->servicio->data2array();
            if ($settimezone) {
                return $this->setTimeZoneforAll($this->servicio);
            } else {
                return $this->servicio;
            }
        } else {
            return null;
        }
    }

    public function findSinReporte($organizacion, $orderBy = null, $hours = null)
    {
        return $this->em->getRepository('App:Servicio')->sinReporte($organizacion, $orderBy, $hours);
    }

    /**
     * Devuelve un servicio segun su patente.
     * @param type $patente
     * @return Servicio
     */
    public function findByPatente($patente = null)
    {
        return $this->em->getRepository('App:Servicio')->findPatente($patente);
    }

    /**
     * Devuelve un servicio segun su patente.
     * @param type $nombre
     * @return null
     */
    public function findByNombre($nombre)
    {
        return $this->em->getRepository('App:Servicio')->findNombre($nombre);
    }

    public function existePatente($patente)
    {
        return !is_null($this->em->getRepository('App:Vehiculo')->existePatente($patente));
    }

    /**
     * Devuelve todos los servicios que tiene una organizacion. No importa su estado
     * Si la org = null entonces se devuelve de la organiacion logueada.
     * @param Organizacion $organizacion 
     */
    function findAllByOrganizacion(Organizacion $organizacion = null, array $orderBy = null)
    {
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion(); //busco la organizacion del usuario logueado.
        }
        $servicios = $this->em->getRepository('App:Servicio')
            ->findAllByOrganizacion($organizacion, $orderBy);
        return $this->setTimeZoneforAll($servicios);
    }

    function findActivos(Organizacion $organizacion)
    {
        $servicios = $this->em->getRepository('App:Servicio')->findActivos($organizacion);
        return $this->setTimeZoneforAll($servicios);
    }

    /**
     * Retorna todos los servicios para un determinado usuario. No importa su estado
     * En caso que el usuario = null entonces devuelve el usuario logueado.
     * Solo devuelve los equipos que tiene asociado. si es usuario master, entonces
     * devuelve todos los de la organizacion
     * @param type $usuario
     * @param array $orderBy
     * @return type
     */
    public function findAllByUsuario($usuario = null, array $orderBy = null)
    {
        if (!$usuario) {  //busco la organizacion del usuario logueado.
            $usuario = $this->userlogin->getUser();
        }

        if ($usuario->isMaster()) {
            $servicios = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($usuario->getOrganizacion());
        } else {
            $servicios = $this->em->getRepository('App:Servicio')->findAllByUsuario($usuario);
            if (count($servicios) == 0) {
                $servicios = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($usuario->getOrganizacion());
            }
        }
        return $this->setTimeZoneforAll($servicios, $usuario);
    }

    public function count($organizacion)
    {
        return $this->em->getRepository('App:Servicio')->contar($organizacion);
    }

    public function findSoloAsignadosUsuario($usuario)
    {
        return $this->em->getRepository('App:Servicio')->findAllByUsuario($usuario);
    }

    public function findSoloAsignadosGrupo($grupo)
    {
        if ($grupo != null) {
            return $this->em->getRepository('App:Servicio')->findAllByGrupo($grupo);
        } else {
            return null;
        }
    }

    // servicios que son vehiculos y no deshabilitados por user
    public function getServiciosVehiculos($organizacion)
    {
        return $this->em->getRepository('App:Servicio')->findServiciosVehiculos($organizacion);
    }

    // servicios configurados para reportar combustible
    public function findByCanbus($organizacion)
    {
        $servicios = array();
        $arr = $this->getServiciosVehiculos($organizacion);
        foreach ($arr as $serv) {
            if ($serv->getCanbus() || $serv->getMedidorCombustible()) { //puede que tenga varilla entonces tenemos que ver esa opción
                $servicios[] = $serv;
            }
        }
        return $servicios;
    }

    public function getStrTipoMedidor($servicio)
    {
        if (!$servicio->getMedicionCombustible()) {
            return 'El servicio no tiene configurado canbus ni varilla';
        } else {
            if ($servicio->getCanbus()) {
                return 'Datos extraídos de canbus';
            }
            if ($servicio->getMedidorCombustible()) { //varilla
                return 'Datos extraídos de varilla de combustible';
            }
        }
        return '';
    }

    public function findAllByGrupo($grupo)
    {
        return $this->em->getRepository('App:Servicio')->findAllByGrupo($grupo);
    }

    public function findAll($entity = null, array $orderBy = null)
    {
        if ($entity instanceof Usuario) {   //hay que consultar por usuario
            $servicios = $this->findAllByUsuario($entity, $orderBy);
        } else if ($entity instanceof Organizacion) {   //hay que consultar por organizacion
            $servicios = $this->findAllByOrganizacion($entity, $orderBy);
        } else if (is_null($entity)) {      //tomar los servicios de la organizacion actual.
            $user = $this->userlogin->getUser();
            $servicios = $this->findAllByUsuario($user, $orderBy);
        }
        return $this->setTimeZoneforAll($servicios);
    }

    /**
     * Obtengo SOLAMENTE los servicios habilitado para una organizacion. En caso
     * de no pasar la organizacion se toma la organizacion del usuario logueado.
     * @param type $entity 
     */
    public function findAllEnabledByOrganizacion($organizacion, $settimezone = true)
    {
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        $servicios = $this->em->getRepository('App:Servicio')
            ->findEnabledByOrganizacion($organizacion);
        if ($settimezone) {
            return $this->setTimeZoneforAll($servicios);
        } else {
            return $servicios;
        }
    }

    /**
     * Obtengo SOLAMENTE los servicios habilitados para un determinado usuario.
     * en caso de no pasar el usuario se obtiene el usuario logueado.
     * @param type $usuario
     * @param array $orderBy
     * @return type
     */
    public function findEnabledByUsuario($usuario = null, array $orderBy = null)
    {
        $servicios = array();
        $servTmp = $this->findAllByUsuario($usuario);
        foreach ($servTmp as $s) {
            if ($s->getEstado() == Servicio::ESTADO_HABILITADO) {
                $servicios[] = $s;
            }
        }
        return $this->setTimeZoneforAll($servicios, $usuario);
    }

    /** actualizados en los ultimos $time segundos */
    public function findLastUpdated($time = null, $id = null)
    {
        $usuario = $this->userlogin->getUser();
        $servicios = null;
        $lastupdate = null;
        if (!is_null($time)) {
            $lastupdate = new \DateTime('', new \DateTimeZone('UTC'));
            $lastupdate->modify('-' . $time . ' seconds');
        }
        if (is_null($id)) {
            if ($usuario->isMaster() || count($usuario->getServicios()) == 0) {
                $servicios = $this->em->getRepository('App:Servicio')->findLastUpdate($usuario->getOrganizacion(), $lastupdate);

                //$servicios = $this->em->getRepository('App:Servicio')->findAllByOrganizacion($usuario->getOrganizacion());
            } else {
                //si el usuario tiene servicios asignado consultar sobre esos servicios sino sobre los de la organizacion
                $servicios = $this->em->getRepository('App:Servicio')->findLastUpdate($usuario, $lastupdate);
            }
        } else {   //solo actualiza el servicio solicitado
            $servicios = $this->em->getRepository('App:Servicio')->findOneLastUpdate($id, $lastupdate);
        }
        return $this->setTimeZoneforAll($servicios);
    }

    /**
     * Retorna todos los servicios que no tengan asociado un equipo, es decir deshabilitado.
     * @param type $organizacion
     * @return type
     */
    public function findAllServiciosSinAsignar($organizacion = null)
    {
        if ($organizacion == null) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:Servicio')
            ->findAllServiciosSinAsignar($organizacion);
    }

    /**
     * Retorna todos los servicios que NO se encuentren asociados al usuario.
     * Sino tiene ningun asociado
     * @param App\Entity\Usuario $usuario
     * @return type
     */
    public function findAllSinAsignar2Usuario(Usuario $usuario)
    {
        $asignados = $this->findSoloAsignadosUsuario($usuario);   //obtengo los del usuario
        if ($asignados) {  //tiene asignados, devuelvo los que no estan asignados...
            $idsSelect = null;
            foreach ($asignados as $servicio) {
                $idsSelect[] = $servicio->getId();
            }
            return $this->em->getRepository('App:Servicio')
                ->findServiciosSinAsignar2Usuario($usuario, $idsSelect);
        } else {  //no tiene asignados... devuelvo todo el resto para la organiacion
            return $this->findAllByOrganizacion($usuario->getOrganizacion());
        }
    }

    /**
     * Retorna todos los servicios que NO se encuentren asociados al grupo.
     * Sino tiene ningun asociado
     * @param \App\Entity\GrupoServicio $gruposervicio
     * @return type
     */
    public function findAllSinAsignar2Grupo(GrupoServicio $grupo)
    {
        $asignados = $this->findSoloAsignadosGrupo($grupo);   //obtengo los del grupo
        if ($asignados) {  //tiene asignados, devuelvo los que no estan asignados...
            $idsSelect = null;
            foreach ($asignados as $servicio) {
                $idsSelect[] = $servicio->getId();
            }
            return $this->em->getRepository('App:Servicio')
                ->findServiciosSinAsignar2Grupo($grupo, $idsSelect);
        } else {  //no tiene asignados... devuelvo todo el resto para la organizacion
            return $this->findAllByOrganizacion($grupo->getOrganizacion());
        }
    }

    public function findById($id)
    {
        $servicio = $this->em->getRepository('App:Servicio')->find($id);
        return $this->setTimeZone($servicio);
    }

    public function create($equipo = null, $organizacion = null)
    {
        $this->servicio = new Servicio();
        if ($organizacion != null) {
            $this->servicio->setOrganizacion($organizacion);
        }
        if ($equipo) {
            $this->servicio->setOrganizacion($equipo->getOrganizacion());
            $this->servicio->setNombre($equipo->getMdmid());
            $this->servicio->setEquipo($equipo);
            //creo el servicio como habilitado.
            $this->servicio->setEstado(1);
        } else {
            $this->servicio->setEstado(0);
        }

        //establesco la fecha y hora de alta.
        if (is_null($this->servicio->getFechaAlta())) {
            $fecha = new \DateTime('', new \DateTimeZone($this->userlogin->getTimezone()));
            $fecha = date_format($fecha, 'd-m-Y H:i');
        } else {
            $fecha = $this->servicio->getFechaAlta()->format('d-m-Y H:i');
        }
        $this->servicio->setFechaAlta(date($fecha));


        return $this->servicio;
    }

    public function toArrayData($servicio, $action)
    { //1 UPDATE, 2 DELETE
        $data = array(
            'entity' => 'SERVICIO',
            'action' => null,
            'data' => null
        );
        if ($servicio) {
            $data['data']['id'] = $servicio instanceof Servicio ? $servicio->getId() : $servicio;
            $data['data']['updated_at'] =  $servicio instanceof Servicio && $servicio->getUpdatedAt() ? $servicio->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $servicio instanceof Servicio && $servicio->getCreatedAt() ? $servicio->getCreatedAt()->format('Y-m-d H:i:s') : null;
            if ($action == 2) {
                $data['action'] = 'DELETE';
                return $data;
            }
            $data['action'] = 'UPDATE';
            $data['data']['nombre'] = $servicio->getNombre();
            $data['data']['organizacion'] = array(
                'id' => $servicio->getOrganizacion()->getId(),
                'nombre' => $servicio->getOrganizacion()->getNombre()
            );
            $data['data']['clase'] = $servicio->getTipoServicio()->getNombre();
            $data['data']['estado'] = $servicio->getStrEstado();
            $data['data']['tipo'] = $servicio->getStrTipoObjetos();
            $data['data']['entradasDigitales'] = $servicio->getEntradasDigitales();
            $data['data']['botonPanico'] = $servicio->getBotonpanico();
            $data['data']['medidorCombustible'] = $servicio->getMedidorcombustible();
            $data['data']['corteMotor'] = $servicio->getCortemotor();
            $data['data']['corteCerrojo'] = $servicio->getCorteCerrojo();
            $data['data']['canbus'] = $servicio->getCanbus();
            $data['data']['patente'] = $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : null;
            $data['data']['mdmid'] = $servicio->getEquipo() ? $servicio->getEquipo()->getMdmid() : null;
            $data['data']['ultOdometro'] = $servicio->getUltOdometro();
            $data['data']['ultHorometro'] = $servicio->getUltHorometro();
            $data['data']['ultFechahora'] = $servicio->getUltFechaHora() ? $servicio->getUltFechaHora()->format('d/m/Y H:i:s') : null;
            $data['data']['ultBateria'] = $servicio->getUltBateria();
            $data['data']['ultEvento'] = $servicio->getUltEvento();
            $data['data']['ultLatitud'] = $servicio->getUltLatitud();
            $data['data']['ultLongitud'] = $servicio->getUltLongitud();
            $data['data']['ultDireccion'] = $servicio->getUltDireccion();
            $data['data']['ultIp'] = $servicio->getUltIp();
            $data['data']['ultPuerto'] = $servicio->getUltPuerto();
            $data['data']['ultTrama'] = json_decode($servicio->getUltTrama());

            $sensores = $this->getDataSensores($servicio);
            $inputs = $this->getDataInputs($servicio);

            if ($sensores) {
                $data['data']['sensores'] = $sensores;
            }
            if ($inputs) {
                $data['data']['inputs'] = $inputs;
            }
        }

        return $data;
    }

    public function getDataSensores($servicio)
    {
        if ($servicio->getSensores()) {
            foreach ($servicio->getSensores() as $sensor) {
                $modelo = $sensor->getModeloSensor(); //practicamente nunca tiene modelo, nose porque nc
                if (!$modelo) {
                    if ($sensor->getSerie()) {   //es seriado, debo buscar el valor en el packlet de sensores.
                        if ($sensor->getUltFechahora() != null) {
                            $sensores[$sensor->getSerie()] = array(
                                'nombre' => $sensor->getNombre(),
                                'valor' => round($sensor->getUltValor()),
                                'fecha' => !is_null($sensor->getUltFechahora()) ? date_format($sensor->getUltFechahora(), 'Y-m-d H:i:s') : null,
                                'bateria' => $sensor->getUltBateria(),
                            );
                        }
                    }
                } else {  //no es seriado
                    $campo = $modelo->getCampoTrama();
                    if ($campo) {
                        if (isset($ultTrama[$campo])) {
                            $sensores[$campo] = array(
                                'nombre' => $sensor->getNombre(),
                                'valor' => $ultTrama[$campo],
                            );
                        }
                    }
                }
            }
        }
        return isset($sensores) ? $sensores : false;
    }

    public function getDataInputs($servicio)
    {
        if ($servicio->getEntradasDigitales() === true) {
            foreach ($servicio->getInputs() as $input) {
                $valor = isset($ultTrama[$input->getModeloSensor()->getCampoTrama()]) ? $ultTrama[$input->getModeloSensor()->getCampoTrama()] : false;
                //aca armo la leyenda teneindo en cuenta si hay icono o no.
                $datos[$input->getModeloSensor()->getCampoTrama()] = array(
                    'nombre' => $input->getNombre(),
                    'valor' => $valor,
                );
            }
        }
        return isset($datos) ? $datos : false;
    }

    public function save($servicio = null)
    {
        //control cambio de dato fiscal.
        if (isset($this->servicioOld['datoFiscal']) && $this->servicioOld['datoFiscal'] !== $servicio->getDatoFiscal) {
        }

        if (is_null($servicio)) {  //si es nulo tomo el servicio de la class
            $servicio = $this->servicio;
        }

        if (!($servicio instanceof Servicio)) {
            $this->servicio = $this->find($servicio);
        }

        $fecha = $this->utils->datetime2sqltimestamp($servicio->getFechaAlta(), true);
        $fecha = new \DateTime($fecha, new \DateTimeZone($this->userlogin->getTimezone()));
        $servicio->setFechaAlta($fecha);

        //modifico el estado del equipo asociado si es que esta asociado.
        if (!is_null($servicio->getEquipo())) {
            $equipo = $servicio->getEquipo();
            $servicio->getEquipo()->setEstado($equipo::ESTADO_INSTALADO);
        }

        // se cambio el tipo de objeto y ahora es tipo_persona por lo que debo borrar la relacion con el vehiculo.
        if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_PERSONA && $servicio->getVehiculo() != null) {
            $vehiculo = $servicio->getVehiculo();
            $this->em->remove($vehiculo);
            $servicio->setVehiculo(null);
        }

        if (is_null($servicio->getId())) {  //si es nulo tomo el servicio de la class
            $alta = true;
        } else {
            //registro la modificacion del servicio
            $this->bitacora->servicioUpdate($this->userlogin->getUser(), $servicio, $this->servicioOld);
        }

        $this->em->persist($servicio);
        $this->em->flush();

        if (isset($alta) && $alta) {
            $this->bitacora->servicioNew($this->userlogin->getUser(), $servicio);
        }

        return $servicio;
    }

    /**
     * Coloca un servicio en estado 9 (eliminado) y borra fisicamente todas las
     * relaciones de otras tablas donde se encuentre el servicio.
     * @param \App\Entity\Servicio $servicio
     * @return boolean True si la eliminación es correcta
     */
    public function delete(Servicio $servicio)
    {
        //si tengo equipo asociado, devuelvo el equipo al deposito.
        if (!is_null($servicio->getEquipo())) {
            if ($servicio->getEquipo()->getId() != 0 || $servicio->getEquipo()->getId() != null) {
                //die('////<pre>'.nl2br(var_export($servicio->getEquipo()->getId(), true)).'</pre>////');
                $equipo = $servicio->getEquipo();
                $equipo->setEstado($equipo::ESTADO_DISPONIBLE);
                $equipo->setServicio(null);
                $servicio->setEquipo(null);
                $this->em->persist($equipo);
            }
        }

        //borro el vehiculo si es de tipo vehiculo.
        if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO && !is_null($servicio->getVehiculo())) {
            $v_id = $servicio->getVehiculo()->getId();
            $servicio->setVehiculo(null);
            $vehiculo = $this->em->getRepository('App:Vehiculo')->find($v_id);
            if ($vehiculo) {
                $this->em->remove($vehiculo);
                $this->em->flush();
            }
        }

        //borro los eventos donde esta el servicio.
        if ($servicio->getEventos()) {
            foreach ($servicio->getEventos() as $evento) {
                $evento->removeServicio($servicio);
                $this->em->persist($evento);
            }
        }

        //borro de los usuarios asignados.
        if ($servicio->getUsuarios()) {
            foreach ($servicio->getUsuarios() as $usuario) {
                $usuario->removeServicio($servicio);
                $this->em->persist($usuario);
            }
        }

        //borro los eventos historicos
        if ($servicio->getEventohistorial()) {
            $this->em->getRepository('App:EventoHistorial')->delete($servicio);
        }

        //borro las cargas combustible
        if ($servicio->getCargasCombustible()) {
            $this->em->getRepository('App:CargaCombustible')->delete($servicio);
        }

        //borro los panicos
        if ($servicio->getPanicos()) {
            $this->em->getRepository('App:Panico')->delete($servicio);
        }
        //borro los sensores
        if ($servicio->getSensores()) {
            $this->em->getRepository('App:Sensor')->delete($servicio);
        }

        //borro los inputs
        if ($servicio->getInputs()) {
            $this->em->getRepository('App:ServicioInputs')->delete($servicio);
        }

        //borro los programaciones
        if ($servicio->getProgramaciones()) {
            $this->em->getRepository('App:Programacion')->delete($servicio);
        }
        //borro los programaciones
        if ($servicio->getParametros()) {
            $this->em->getRepository('App:Parametro')->delete($servicio);
        }
        //borro los mantenimientos
        if ($servicio->getMantenimientos()) {
            $this->em->getRepository('App:ServicioMantenimiento')->delete($servicio);
        }
        //borro los mediciones de tanque
        if ($servicio->getMedicionestanque()) {
            $this->em->getRepository('App:ServicioTanque')->delete($servicio);
        }

        //borro los grupos de servicios
        if ($servicio->getGrupos()) {
            foreach ($servicio->getGrupos() as $grupo) {
                $servicio->removeGrupoServicios($grupo);
            }
        }

        //nunca se elimina el servicio, se pone como estado eliminado.
        $servicio->setEstado($servicio::ESTADO_ELIMINADO);
        $this->em->persist($servicio);
        //$this->em->remove($servicio);  esto estaba antes.
        $this->em->flush();
        return true;
    }

    public function baja(Servicio $servicio)
    {
        //proceso la devolucion del equipo
        if (!is_null($servicio->getEquipo())) {
            $equipo = $servicio->getEquipo();
            $equipo->setServicio(null);
            $equipo->setEstado($equipo::ESTADO_ENDEPOSITO);
            $this->em->persist($equipo);
        }

        $fecha = $this->utils->datetime2sqltimestamp($servicio->getFechaBaja(), true);
        $fecha = new \DateTime($fecha, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
        $servicio->setFechaBaja($fecha);
        $servicio->setEstado($servicio::ESTADO_BAJA);
        $servicio->setEquipo(null);
        $this->em->persist($servicio);
        $this->em->flush();

        $this->bitacora->servicioBaja($this->userlogin->getUser(), $servicio, null);

        return true;
    }

    public function deleteById($id)
    {
        $servicio = $this->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de servicio no encontrado.');
        }

        return $this->delete($servicio);
    }

    public function asignarUsuario($idServicio, Usuario $usuario)
    {
        $servicio = $this->find($idServicio);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de servicio no encontrado.');
        }
        $serv = $this->em->getRepository('App:Servicio')->findUsuario($servicio, $usuario);
        if (count($serv) == 0) {  //trae el usuario asociado.
            $usuario->addServicio($servicio);
            $this->em->persist($usuario);
            $this->em->flush();
            $this->bitacora->asignarUsuario($this->userlogin->getUser(), $servicio, $usuario);
            return true;
        } else {
            return false;
        }
    }

    public function eliminarUsuario($idServicio, Usuario $usuario)
    {
        $servicio = $this->find($idServicio);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de servicio no encontrado.');
        }
        $usuario->removeServicio($servicio);
        $this->bitacora->retirarUsuario($this->userlogin->getUser(), $servicio, $usuario);
        $this->em->persist($usuario);
        $this->em->flush();

        return true;
    }


    public function buscarCercaniaPosicion($latitud, $longitud, $referencias, $limit = null)
    {
        $centroServ = [
            'latitud' => $latitud,
            'longitud' => $longitud
        ];
        $tmp = [];

        foreach ($referencias as $ref) {
            $centroRef = $this->utils->getCentroReferencia($ref);
            $distancia = $this->utils->calcularDistancia($centroServ, $centroRef);

            // Determinar si el punto está dentro del polígono o dentro del radio
            if ($ref->getClase() === $ref::CLASE_POLIGONO) {
                $adentro = $this->utils->adentro_poligono($latitud, $longitud, $ref);
            } else {
                $adentro = $distancia <= $ref->getRadio();
            }

            $bearing = $this->utils->getBearing($centroServ, $centroRef);

            // Evitar llamar varias veces a los mismos métodos
            $nombre = $ref->getNombre();
            $id = $ref->getId();
            $codigo_eess = $ref->getCodigoExterno();
            $icono = $ref->getPathIcono();

            // Generar la información básica de cada referencia
            $tmp[] = [
                'id' => $id,
                'nombre' => $nombre,
                'latitud' => $centroRef['latitud'],
                'longitud' => $centroRef['longitud'],
                'distancia' => intval($distancia),
                'codigo_eess' => $codigo_eess,
                'icono' => $icono,
                'adentro' => $adentro,
                'bearing' => $bearing,
            ];
        }

        // Ordenar por distancia, asegurándose de que 'compararByDistancia' sea eficiente
        usort($tmp, array($this, 'compararByDistancia'));

        // Limitar los resultados si se especifica un límite
        if ($limit !== null) {
            $tmp = array_slice($tmp, 0, $limit);
        }

        // Post-procesamiento de cada entrada, optimizando las cadenas de distancia y leyendas
        foreach ($tmp as $i => $t) {
            $distancia = $t['distancia'];
            $tmp[$i]['strdistancia'] = ($distancia < 1000)
                ? number_format($distancia, 0) . ' mts'
                : number_format($distancia / 1000, 1) . ' kms';

            $leyenda = ($t['adentro'])
                ? sprintf('está dentro de %s', $t['nombre'])
                : sprintf('a %s al %s de %s', $tmp[$i]['strdistancia'], $t['bearing']['direccion'], $t['nombre']);

            $tmp[$i]['leyenda'] = $leyenda;
        }

        return $tmp;
    }


    public function buscarCercania($servicio, $referencias, $limit = null)
    {
        return $this->buscarCercaniaPosicion($servicio->getUltLatitud(), $servicio->getUltLongitud(), $referencias, $limit);
    }

    private function compararByDistancia($x, $y)
    {
        return $x['distancia'] - $y['distancia'];
    }


    public function informeReportes($servicio, $desde, $hasta, $data)
    {
        if (!$servicio && !$data)
            return null;

        if ($data['fecha_inicio'] == $data['fecha_fin']) {
            $dias_reportando = 0;
        } else {
            $dias_reportando = date_diff(date_create($data['fecha_inicio']), date_create($data['fecha_fin']), true);
            //aca considero que si no reporto un dia completo entonces veo si ha reportado horas o minutos
            if ($dias_reportando->format('%a') == 0 && ($dias_reportando->format('%h') * 3600 + $dias_reportando->format('%m') * 60) > 0) {
                $dias_reportando = 1;  //si no alcanzo para un dia, entonces pongo minimo 1 dia
            } else {
                //aca veo
                $ayer = date("Y-m-d", strtotime("$hasta - 1 days"));
                if (date_create($ayer) < date_create($data['fecha_fin'])) {
                    $dias_reportando = intval($dias_reportando->format('%a')) + 1;
                    //                die('////<pre>' . nl2br(var_export($dias_reportando, true)) . '</pre>////');
                } else {
                    $dias_reportando = intval($dias_reportando->format('%a'));
                }
            }
        }

        $dias_totales = intval(date_diff(date_create($desde), date_create($hasta), true)->format('%a'));
        if ($dias_totales == 0) {
            $dias_totales = 1;
        }

        //calcula la efectividad
        if ($dias_reportando != 0) {
            $efec = ($dias_reportando * 100) / $dias_totales;
        } else {
            $efec = 0;
        }
        if ($servicio->getVehiculo()) {
            $patente = $servicio->getVehiculo()->getPatente();
        } else {
            $patente = '';
        }
        if ($servicio->getEquipo()) {
            $equipo = sprintf('%s (%s)', $servicio->getEquipo()->getMdmid(), $servicio->getEquipo()->getModelo()->getNombre());
        } else {
            $equipo = '';
        }

        return array(
            'servicio_id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'estado' => $servicio->getStrEstado(),
            'estado_id' => $servicio->getEstado(),
            'patente' => $patente,
            'equipo' => $equipo,
            'tipo_servicio' => $servicio->getTipoServicio()->getNombre(),
            'data' => $data,
            'cant_dias' => $dias_reportando,
            'total_dias' => $dias_totales,
            'efectividad' => $efec,
        );
    }

    public function informeFacturacion($servicio, $desde, $hasta, $dataHist, $data)
    {
        if (!$servicio && !$dataHist)
            return null;

        if (!isset($dataHist['fecha_inicio']) || $dataHist['fecha_inicio'] == $dataHist['fecha_fin']) {
            $dias_reportando = 0;
        } else {
            $dias_reportando = date_diff(date_create($dataHist['fecha_inicio']), date_create($dataHist['fecha_fin']), true);
            //aca considero que si no reporto un dia completo entonces veo si ha reportado horas o minutos
            if ($dias_reportando->format('%a') == 0 && ($dias_reportando->format('%h') * 3600 + $dias_reportando->format('%m') * 60) > 0) {
                $dias_reportando = 1;  //si no alcanzo para un dia, entonces pongo minimo 1 dia
            } else {
                //aca veo
                $ayer = date("Y-m-d", strtotime("$hasta - 1 days"));
                if (date_create($ayer) < date_create($dataHist['fecha_fin'])) {
                    $dias_reportando = intval($dias_reportando->format('%a')) + 1;
                } else {
                    $dias_reportando = intval($dias_reportando->format('%a'));
                }
            }
        }

        $dias_totales = intval(date_diff(date_create($desde), date_create($hasta), true)->format('%a'));
        if ($dias_totales == 0) {
            $dias_totales = 1;
        }

        //calcula la efectividad
        if ($dias_reportando != 0) {
            $efec = ($dias_reportando * 100) / $dias_totales;
        } else {
            $efec = 0;
        }


        //grabo los datos nuevos obtenidos para ser devueltos.
        $data['data'] = $dataHist;
        $data['cant_dias'] = $dias_reportando;
        $data['total_dias'] = $dias_totales;
        $data['efectividad'] = $efec;
        return $data;
    }

    public function getMotivoTx2Str($trama)
    {
        $str = '';
        if (isset($trama['motivo'])) {
            $motivotx =  $this->em->getRepository('App:MotivoTx')->findOneBy(array('code' => intval($trama['motivo'])));
            if ($motivotx) {
                $str = $motivotx->getNombre();
            } else {
                $str = "Motivo: " . $trama['motivo'] . " Orig: " . (isset($trama["motivo_original"]) ? $trama["motivo_original"] : '???');
            }
        }
        //die('////<pre>' . nl2br(var_export($str, true)) . '</pre>////');
        return $str;
    }

    public function getStatusUltReporte($servicio, $minutos = null)
    {
        //armo el stats para el servicio
        if ($servicio->getUltfechahora()) {
            $intervalo = date_diff(date_create(), date_create($servicio->getUltfechahora()->format('Y-m-d H:i:s')));
            if (is_null($minutos) || $minutos == false) {
                $intervalo = $intervalo->days;
            } else {
                $intervalo = $intervalo->i;
            }
        } else {
            $intervalo = -1;
        }
        return $intervalo;
    }

    public function altaShell($servicio, $patente, $rut, $fecha)
    {
        if (is_null($servicio->getRegistroShell())) {
            $shell = new RegistroShell();
        } else {
            $shell = $servicio->getRegistroShell();
        }
        if ($patente != null && $rut != null && $fecha != null) {
            $fecha = $this->utils->datetime2sqltimestamp($fecha, true);
            $fecha = new \DateTime($fecha, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $shell->setFechaAlta($fecha);
            $shell->setFechaBaja(null);
            $shell->setProcesadoAlta(null);
            $shell->setProcesadoBaja(null);
            $shell->setPatente($patente);
            $shell->setDatoFiscal($rut);
            //$shell->setServicio($servicio);
            $servicio->setRegistroShell($shell);
            $this->em->persist($shell);
            $this->em->persist($servicio);
            $this->em->flush();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Se debe poner la fecha de baja.
     * @param \App\Entity\Servicio $servicio
     * @param type $fecha
     * @return boolean
     */
    public function bajaShell(Servicio $servicio, $fecha)
    {
        $fecha = $this->utils->datetime2sqltimestamp($fecha, true);
        $fecha = new \DateTime($fecha, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
        $servicio->getRegistroShell()->setFechaBaja($fecha);
        $this->em->persist($servicio);
        $this->em->flush();
        return true;
    }

    public function getMedicionesTanque($servicio)
    {
        return $this->em->getRepository('App:ServicioTanque')->findMediciones($servicio);
    }

    public function addParametro($variable, $valor, $servicio)
    {
        $param = $this->em->getRepository('App:Parametro')->findOneBy(array(
            'variable' => $variable->getId(),
            'servicio' => $servicio->getId()
        ));
        if (is_null($param)) {
            $param = new Parametro();
            $servicio->addParametro($param);
            $param->setServicio($servicio);
            $param->setVariable($variable);
            $param->setValorFuturo($valor);

            $this->em->persist($param);
            $this->em->persist($servicio);
            $this->em->flush();
        }
        return true;
    }

    /**
     *Devuelve todos los parametros de programacion
     */
    public function getParams($servicio)
    {
        $params = $this->em->getRepository('App:Parametro')->findBy(
            array('servicio' => $servicio->getId()),
            array('id' => 'asc')
        );
        $v = new Variable();
        $par = array();
        foreach ($params as $value) {
            switch ($value->getVariable()->getTipoVariable()) {
                case $v::TVAR_INTEGER:
                    $par[] = $value;
                    break;
                case $v::TVAR_TELECOMANDO:
                    if ($servicio->getCorteMotor() or $servicio->getCorteCerrojo()) {
                        $par[] = $value;
                    }
                    break;
                default:
                    break;
            }
        }
        return $par;
    }

    /**
     * Devuelve todos las variables (integer, string) que pueden ser programadas
     */
    public function getProgramacionVars($servicio)
    {
        $params = $this->em->getRepository('App:Parametro')->findBy(
            array('servicio' => $servicio->getId()),
            array('id' => 'asc')
        );
        $var = new Variable();
        $par = array();
        foreach ($params as $value) {
            switch ($value->getVariable()->getTipoVariable()) {
                case $var::TVAR_INTEGER:
                    if (is_null($value->getVariable()->getVariablePadre())) {
                        $par[] = $value;
                    }
                    break;
                case $var::TVAR_COMPUESTA:
                    $par[] = $value;
                    break;
                default:
                    break;
            }
        }
        return $par;
    }

    public function getStatusCerrojo($servicio, $ultTrama = null)
    {
        if ($servicio->getCorteCerrojo() == true && $ultTrama != null) {
            $status = isset($ultTrama['entrada_3']) ? $ultTrama['entrada_3'] : false;
            if ($status) {
                return 0;  //abierto
            } else {
                return 1;  //cerrado
            }
        } else {
            return true;  //siempre esta abierto.
        }
    }

    public function cambiarOdometro($servicio, $odometro_kms)
    {

        $odometro_mts = abs($odometro_kms * 1000);   //paso de kms -> mts.        
        $odometroGps = $servicio->getOdometroGps();    //obtengo el ult odometro del gps
        $vAjuste = $odometro_mts - $odometroGps;    //obtengo la nueva variable de ajuste desde el gps
        $ultOdometro = $servicio->getUltOdometro();
        $servicio->setUltOdometro($odometro_mts);
        $servicio->setAjusteOdometro($vAjuste);
        if ($servicio->getVehiculo() != null) {   //nose puede setear odometro si no es vehiculo.
            $servicio->getVehiculo()->setOdometro($odometro_kms);
        }
        $this->em->persist($servicio);
        $this->em->flush();
        $this->bitacora->cambioOdometro($this->userlogin->getUser(), $servicio, $ultOdometro, $odometro_kms, $vAjuste);
        return true;
    }

    public function cambiarHorometro($servicio, $minutos)
    {
        //tengo que tener la variable de ajuste
        $varAjuste = $servicio->getUltHorometro() - $servicio->getAjusteHorometro();      //obtengo la variable de ajuste usa antes
        $ultHorometro = $servicio->getUltHorometro();

        $ajuste = $minutos - $varAjuste;
        $servicio->setAjusteHorometro($ajuste);
        $servicio->setUltHorometro($minutos);

        if ($servicio->getVehiculo() != null) {                                         //nose puede setear odometro si no es vehiculo.
            $servicio->getVehiculo()->setHorometro($minutos);
        }
        $this->em->persist($servicio);
        $this->em->flush();
        $this->bitacora->cambioHorometro($this->userlogin->getUser(), $servicio, $ultHorometro, $minutos, $servicio->getAjusteOdometro());
        return true;
    }

    private function hacerProgramacion($servicio, $odometro, $parametro)
    {
        //mando el reset de la variable.
        $this->prg->initConfig($servicio);
        $param = $this->prg->getParamByCodename($servicio, $parametro);
        if ($param) {
            $this->prg->setValue($param->getId(), $odometro);
            if ($this->prg->setConfig($param)) {
                $idProg = $this->prg->executeConfig();
                if ($idProg !== false) {
                    $this->prg->runProgramacion($idProg);
                }
            }
        }
    }

    public function saveDatoExtra($dato)
    {
        $this->em->persist($dato);
        $this->em->flush();
    }

    public function deleteDatoExtra($dato)
    {
        $this->em->remove($dato);
        $this->em->flush();
    }

    public function getStatusServicios($servicios, $showDefaultCercania = null, $verCercana = false)
    {
        $status = array();
        foreach ($servicios as $servicio) {
            $status[$servicio->getId()] = $this->getStatusServicio($servicio, $showDefaultCercania, $verCercana);
        }
        return $status;
    }

    public function getStatusServicio($servicio, $showDefaultCercania = null, $verCercana = false)
    {
        if ($servicio) {
            return $this->procesarStatusServicio($servicio, $showDefaultCercania, $verCercana);
        } else {
            return null;
        }
    }

    private $referencias = array();

    private function procesarStatusServicio($servicio, $showDefaultCercania = null, $verCercana = false)
    {
        $status = array(
            'st_ultreporte' => $this->getStatusUltReporte($servicio),
            'direccion' => '',
            'icono' => $this->gmaps->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            'contacto' => false,
        );

        $status['direccion'] = $this->geocoder->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());
        //cargo las referencias si no las tengo.
        if ($verCercana) {
            if (count($this->referencias) == 0) {
                foreach ($servicio->getItinerarios() as $itinerario) {
                    foreach ($itinerario->getPois() as $poi) {
                        if ($itinerario->getEstado() < 9) {
                            $this->referencias[] = $poi->getReferencia();
                        }
                    }
                }
            }
            if (count($this->referencias) > 0) {
                $cercanas = $this->buscarCercania($servicio, $this->referencias);
                $i = 0;
                $this->referencias = array();
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        break;
                    }
                }

                if (!is_null($cercanas)) {
                    $status['cerca']['leyenda'] = $cercanas[$i]['leyenda'];
                    $status['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                }
            }
        }
        //ult_trama
        if (!is_null($servicio->getUltTrama())) {
            $ultTrama = json_decode($servicio->getUltTrama(), true);
            $status['contacto'] = isset($ultTrama['contacto']) ? $ultTrama['contacto'] : false;

            if (isset($ultTrama['ibutton']) && $ultTrama['ibutton'] != 0) {
                $status['chofer'] = $this->chofer->findByDallas($ultTrama['ibutton']);
            }
            //se visualiza el estado de los inputs.
            $inputs = array();
            if ($servicio->getEntradasDigitales() === true) {
                foreach ($servicio->getInputs() as $input) {
                    $valor = isset($ultTrama[$input->getModeloSensor()->getCampoTrama()]) ? $ultTrama[$input->getModeloSensor()->getCampoTrama()] : false;
                    //aca armo la leyenda teneindo en cuenta si hay icono o no.
                    $inputs[] = array(
                        'nombre' => $input->getNombre(),
                        'icono' => !is_null($input->getIcono()) ? ($valor ? $input->getIcono() : str_replace('input_on', 'input_off', $input->getIcono())) : null,
                        'leyenda' => $input->getAbbr() . ': ' . ($valor ? 'ON' : 'OFF')
                    );
                }
            }
            $status['inputs'] = $inputs;

            //============= cerrojo ==============                
            if ($servicio->getCorteCerrojo() === true) {
                $status['cerrojo'] = array(
                    'estado_cerrojo' => $this->getStatusCerrojo($servicio, $ultTrama),
                );
            }

            //========= sensores ==============
            $sensores = array();
            foreach ($servicio->getSensores() as $sensor) {
                $modelo = $sensor->getModeloSensor();
                if ($modelo->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                    if ($sensor->getUltFechahora() != null) {
                        $fecha = $this->utils->fechaUTC2local($sensor->getUltFechahora());
                        $sensores[$sensor->getAbbr()] = array(
                            'sensor' => $sensor,
                            'inrango' => $this->sensorInRange($sensor, $sensor->getUltValor()),
                            'dato' => round($sensor->getUltValor()),
                            'color' => $this->sensorColor($sensor, $sensor->getUltValor(), $sensor->getUltFechahora()),
                            'title' => sprintf('%s (%d a %d %s) bat: %d%% el %s', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida(), $sensor->getUltBateria(), date_format($fecha, 'd/m/Y H:i:s'))
                        );
                        if (!is_null($sensor->getUltBateria())) {
                            $sensores[$sensor->getAbbr()]['alertabateria'] = $sensor->getUltBateria() < 5;
                        }
                    } else {
                        $sensores[$sensor->getAbbr()] = array(
                            'sensor' => $sensor,
                            'dato' => '---',
                            'title' => sprintf('%s (%d a %d) No se ha informado.', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                        );
                    }
                } else {  //no es seriado
                    $campo = $sensor->getModeloSensor()->getCampoTrama();
                    if (isset($ultTrama[$campo])) {
                        $sensores[$sensor->getAbbr()] = array(
                            'sensor' => $sensor,
                            'inrango' => $this->sensorInRange($sensor, $ultTrama[$campo]),
                            'color' => $this->sensorColor($sensor, $ultTrama[$campo], null),
                            'dato' => $ultTrama[$campo],
                            'title' => sprintf('%s (%d a %d %s)', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                        );
                    } else {
                        $sensores[$sensor->getAbbr()] = array(
                            'sensor' => $sensor,
                            'dato' => '---',
                            'title' => sprintf('%s (%d a %d) No se ha informado.', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                        );
                    }
                }
                ksort($sensores);
            }
            $status['sensores'] = $sensores;
        }

        if ($servicio->getMedidorCombustible() || $servicio->getCanbus()) {
            $status['carcontrol']['tanque'] = array('porcentaje' => -1, 'title' => '');
            $tanque = array('porcentaje' => -1);
            if (isset($ultTrama['carcontrol']) && !is_null($ultTrama['carcontrol']) && isset($ultTrama['contacto']) && $ultTrama['contacto']) {
                $carcontrol = $ultTrama['carcontrol'];
                if ($servicio->getMedidorCombustible()) {
                    if (isset($carcontrol['porcentaje_tanque'])) {
                        $tanque = array(
                            'porcentaje' => $carcontrol['porcentaje_tanque'],
                            'title' => sprintf('%s %%', $carcontrol['porcentaje_tanque'])
                        );

                        if ($this->tanque->loadMediciones($servicio, $carcontrol['porcentaje_tanque'])) {
                            $medicion = $this->tanque->getLitros($carcontrol['porcentaje_tanque']);
                        }
                    }
                    $status[$servicio->getId()]['carcontrol']['tanque'] = $tanque;
                }
            } elseif (isset($ultTrama['canbusData'])) { //tiene datos de canbus osea tiene nivel de tanque
                $tanque = array(
                    'porcentaje' => $ultTrama['canbusData']['nivelCombustible'],
                    'title' => sprintf('%s %%', $ultTrama['canbusData']['nivelCombustible'])
                );
                if ($this->tanque->loadMediciones($servicio, $ultTrama['canbusData']['nivelCombustible'])) {
                    $medicion = $this->tanque->getLitros($ultTrama['canbusData']['nivelCombustible']);
                }
                $status['carcontrol']['tanque'] = $tanque;
            }
            if (isset($medicion)) {
                if ($medicion['modo'] == -1) {
                    $tanque['litros'] = '<' . $medicion['litros'];
                } elseif ($medicion['modo'] == 1) {
                    $tanque['litros'] = '>' . $medicion['litros'];
                } else {
                    $tanque['litros'] = $medicion['litros'];
                }
            }
            $status['carcontrol']['tanque'] = $tanque;
        }
        return $status;
    }

    public function procesarStatusServicioByMonitor($servicio, $showDefaultCercania = null, $verCercana = false, $inverso = true)
    {
        $status = array(
            'st_ultreporte' => $this->getStatusUltReporte($servicio),
            'direccion' => '---',
            'contacto' => false,
            'icono' => $this->gmaps->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
        );

        if ($inverso) {   //no quiero que traiga posiciones.
            $status['direccion'] = $this->geocoder->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());
        }


        //cargo las referencias si no las tengo.
        if ($verCercana) {
            //    die('////<pre>' . nl2br(var_export( $status['direccion'] , true)) . '</pre>////');
            if (count($this->referencias) == 0) {
                foreach ($servicio->getItinerarios() as $itserv) {
                    $itinerario = $itserv->getItinerario();
                    foreach ($itinerario->getPois() as $poi) {
                        if ($itinerario->getEstado() < 9) {
                            $this->referencias[] = $poi->getReferencia();
                        }
                    }
                }
            }
            if (count($this->referencias) > 0) {
                $cercanas = $this->buscarCercania($servicio, $this->referencias);
                $i = 0;
                $this->referencias = array();
                $dentro = false;
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        $dentro = true;
                        break;
                    }
                }

                if (!is_null($cercanas)) {
                    $status['cerca']['leyenda'] = $cercanas[$i]['leyenda'];
                    $status['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                    if ($dentro) {
                        $status['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                    }
                }
            }
        }

        return $status;
    }

    public function serviciosAJAXAction(Request $request)
    {
        $idOrg = $request->get('id');
        $servicios = $this->getServicios(null);
        $visibles = array();

        foreach ($servicios as $servicio) {
            $visibles[$servicio->getId()] = true;
        }
        //obtengo todos y seteo los que son visibles o no
        $html = array();
        foreach ($servicios as $servicio) {
            //aca armo el html para cada servicio.
            $status[$servicio->getId()] = $this->getStatusServicio($servicio, $this->gmaps->getShowReferencias(), true);

            $html[] = array(
                'id_sc' => $servicio->getId(),
                'data' => trim($this->get('templating')->render('App:Servicio:servicio_div.html.twig', array(
                    'servicio' => $servicio,
                    'status' => $status
                ))),
            );
        }
        return new Response(json_encode(array('html' => $html)));
    }

    public function sensorInRange($sensor, $valorSensor)
    {
        if (!is_null($sensor->getValorMinimo()) && !is_null($sensor->getValorMaximo())) {
            return $sensor->getValorMinimo() <= $valorSensor && $valorSensor <= $sensor->getValorMaximo();
        } else {
            return true;
        }
    }

    public function sensorColor($sensor, $valorSensor, $fecha)
    {
        $color = '#FFF';
        if ($this->sensorInRange($sensor, $valorSensor)) {
            $color = '#EEEEEE';
        } else {
            $color = '#F3583F';
        }
        //aca controlo la fecha
        if (!is_null($fecha)) {
            $intervalo = date_diff(date_create(), date_create($fecha->format('Y-m-d H:i:s')));
            $intervalo = $intervalo->format('%h');
            //die('////<pre>'.nl2br(var_export($intervalo, true)).'</pre>////');
            if ($intervalo > 2) {
                $color = 'yellow';
            }
        }
        //{{ campo.inrango is defined ? (campo.inrango ?  "#EEEEEE":  "#F13109") : "#F8F677"}}
        return $color;
    }

    public function updateValores($id, $horometro, $fecha, $odometro = null)
    {
        $serv = $this->find($id);
        $odometro_kms = $odometro / 1000;
        if ($odometro && $serv->getUltOdometro() != $odometro) {
            $this->cambiarOdometro($serv, $odometro_kms);
        }
        if ($horometro && $serv->getUltHorometro() != $horometro) {
            $this->cambiarHorometro($serv, $horometro);
        }

        $serv->setFechaUltCambio($fecha);
        $this->em->persist($serv);
        $this->em->flush();
        return $serv;
    }

    public function updatePatente($id, $patente)
    {
        $serv = $this->find($id);
        $vehiculo = $serv->getVehiculo();

        if (!$vehiculo) {
            $vehiculo = new Vehiculo();
            $vehiculo->setServicio($servicio);
            if ($servicio && $servicio->getTipoObjeto() == 0) { //si el servicio existe y  es de tipo persona, lo pasamos a vehículo
                $servicio->setTipoObjeto(1);
                $this->em->persist($servicio);
            }
        }

        $vehiculo->setPatente($patente);
        $this->em->persist($vehiculo);
        $this->em->flush();
        return $serv;
    }

    public function createFromImportIti($equipo, $organizacion, $tipoServicio, $transporte = null, $satelital = null, $data = null)
    {
        if ($data != null) {

            $vehiculo = new Vehiculo();
            $vehiculo->setPatente($data['patente']);
            $this->em->persist($vehiculo);

            $serv = $this->create($equipo, $organizacion);
            $serv->setColor($data['color']);
            $serv->setVehiculo($vehiculo);
            $serv->setTipoServicio($tipoServicio);
            $serv->setTipoObjeto(1); //por default es un vehiculo
            $serv->setFechaAlta(new \DateTime('', new \DateTimeZone($this->userlogin->getTimezone())));

            $this->em->persist($serv);
            $this->em->flush();
            return $serv;
        }
        return false;
    }

    public function findNotificaciones($servicio)
    {
        return null;
        // return $this->em->getRepository('App:Servicio')->findNotificaciones($servicio);
    }

    public function findPeticiones($servicio)
    {
        return $this->em->getRepository('App:Peticion')->findByServicio($servicio);
    }

    public function historico($usuario, $tipoServicio = null, $transporte = null, $satelital = null, $empresa = null, $itinerario = null, $servicio = null, $estado = null)
    {
        $servicios = $this->em->getRepository('App:Servicio')->findAllByUsuario($usuario);
        if (count($servicios) != 0) { //tiene servicios particulares para visualizar
            $servicios = $this->em->getRepository('App:Servicio')->historico($usuario, $tipoServicio, $transporte, $satelital, $empresa, $itinerario, $servicio, $estado);
        } else { //muestra todo porque no tiene restricciones
            $servicios = $this->em->getRepository('App:Servicio')->historicoByOrg($usuario->getOrganizacion(), $tipoServicio, $transporte, $satelital, $empresa, $itinerario, $servicio, $estado);
        }
        return $servicios;
    }

    public function filtrar($organizacion, $claseServicio = null, $tipoServicio = null, $estado = null, $transporte = null, $satelital = null, $empresa = null)
    {
        return $this->em->getRepository('App:Servicio')->filtrar($organizacion, $claseServicio, $tipoServicio, $estado, $transporte, $satelital, $empresa);
    }

    public function checkStatusServicioMonitor($servicios)
    {
        $actual = new \DateTime();
        $status = array();
        $contador = array(
            'reportando' => 0,
            'retrasado' => 0,
            'sin_reportar' => 0,
            'nunca' => 0
        );
        foreach ($servicios as $servicio) {
            $ultFecha = $servicio->getUltFechaHora();
            if (!is_null($ultFecha)) {
                $ultFecha->setTimezone(new \DateTimeZone('UTC'));
                $diferencia = $ultFecha->diff($actual);
                if (!(intval($diferencia->format('%d')) > 0 || intval($diferencia->format('%m')) > 0 || intval($diferencia->format('%y')) > 0)) {  //mayor a 1 dia a 1 mes a un año
                    if (intval($diferencia->format('%h')) > 0) {  //mayor a 1 hora
                        $status[$servicio->getId()] = $this->armarArrayStatus($servicio, 12);
                        $contador['sin_reportar'] += 1;
                    } else {
                        if ($diferencia->format('%i') > 10) { //mayor a 10 min amarillo
                            $status[$servicio->getId()] = $this->armarArrayStatus($servicio, 11);
                            $contador['retrasado'] += 1;
                        } else { //verde
                            $status[$servicio->getId()] = $this->armarArrayStatus($servicio, 10);
                            $contador['reportando'] += 1;
                        }
                    }
                } else { //rojo
                    $status[$servicio->getId()] = $this->armarArrayStatus($servicio, 12);
                    $contador['sin_reportar'] += 1;
                }
            } else { //gris, no reportaron nunva
                $status[$servicio->getId()] = $this->armarArrayStatus($servicio, 13);
                $contador['nunca'] += 1;
            }
        }

        return array('status' => $status, 'contador' => $contador);
    }

    public function getStatusLabel($servicio)
    {
        $actual = new \DateTime();
        $ultFecha = $servicio->getUltFechaHora();
        if (!is_null($ultFecha)) {
            $ultFecha->setTimezone(new \DateTimeZone('UTC'));
            $diferencia = $ultFecha->diff($actual);
            if (!(intval($diferencia->format('%d')) > 0 || intval($diferencia->format('%m')) > 0 || intval($diferencia->format('%y')) > 0)) {  //mayor a 1 dia a 1 mes a un año
                if (intval($diferencia->format('%h')) > 0) {  //mayor a 1 hora
                    return 12;
                } else {
                    if ($diferencia->format('%i') > 10) { //mayor a 10 min amarillo
                        return 11;
                    } else { //verde
                        return 10;
                    }
                }
            } else { //rojo
                return 12;
            }
        } else { //rojo
            return 12;
        }
    }

    public function checkStatusServicioItinerario($itinerario)
    {
        $ref = array();
        foreach ($itinerario->getPois() as $poi) {
            if ($itinerario->getEstado() < 9) {
                $ref[] = $poi->getReferencia();
            }
        }
        $status = array();
        foreach ($itinerario->getServicios() as $serv) {
            $servicio = $serv->getServicio();
            $estado = $this->getStatusLabel($servicio);

            //busco la cercania de las referencias-
            if (count($ref) > 0) {
                $cercanas = $this->buscarCercania($servicio, $ref);
                $i = 0;
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        break;
                    }
                }

                if (!is_null($cercanas)) {
                    $cerca['leyenda'] = $cercanas[$i]['leyenda'];
                    $cerca['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                }
            }

            $status[$servicio->getId()] = array(
                'labelColor' => $servicio->getStrColorStatus($estado),
                'color' => $servicio->getColorStatus($estado),
                'label' => $servicio->getStrStatus($estado),
                'servicio' => array(
                    'iscustodio' => $servicio->getIsCustodio(),
                    'ultlatitud' => $servicio->getUltLatitud(),
                    'ultlongitud' => $servicio->getUltLongitud(),
                    'ultFechaHora' => $servicio->getUltFechahora(),
                    'id' => $servicio->getId(),
                    'nombre' => $servicio->getNombre()
                ),
                'cerca' => isset($cerca) ? $cerca : null,
                'direccion' => $this->geocoder->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud())
            );
        }

        return $status;
    }

    private function armarArrayStatus($servicio, $estado)
    {
        return array(
            'labelColor' => $servicio->getStrColorStatus($estado),
            'color' => $servicio->getColorStatus($estado),
            'label' => $servicio->getStrStatus($estado),
            'servicio' => $servicio,
            'estado' => $estado
        );
    }

    /**
     * Devuelve todos los servicios que tiene una organizacion. No importa su estado
     * Si la org = null entonces se devuelve de la organiacion logueada.
     * @param Organizacion $organizacion 
     */
    function findAllByMonitor(Organizacion $organizacion = null, array $orderBy = null)
    {
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion(); //busco la organizacion del usuario logueado.
        }
        $servicios = $this->em->getRepository('App:Servicio')
            ->findAllByMonitor($organizacion, $orderBy);

        return $this->setTimeZoneforAll($servicios);
    }

    function getLtrTanque($servicio, $valor)
    {
        $litros = $valor;
        if (!is_null($servicio->getVehiculo()) && $servicio->getTipoMedidorCombustible() == 1) { // si viene el dato en litros lo convertimos
            $litros = $valor * intval($servicio->getVehiculo()->getLitrosTanque() / 100);
        }
        return $litros;
    }

    function getPorcentajeTanque($servicio, $valor)
    {
        $porcentaje = $valor;
        if (!is_null($servicio->getVehiculo()) && $servicio->getVehiculo()->getLitrosTanque() > 0 && $servicio->getTipoMedidorCombustible() == 2) { // si viene el dato en litros lo convertimos
            $porcentaje = $valor * 100 / intval($servicio->getVehiculo()->getLitrosTanque());
        }

        return $porcentaje;
    }


    public function findByTipoAndGrupo($organizacion, $contacto, $tipoVehiculo = null, $grupoServicio = null)
    {
        $servicios = array();
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        $data = $this->em->getRepository('App:Servicio')->findByTipoAndGrupo($organizacion, $tipoVehiculo, $grupoServicio);

        foreach ($data as $servicio) {
            if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
                $ultTrama = json_decode($servicio->getUltTrama(), true);
                if (!is_null($ultTrama)) {
                    if ($contacto == -1 || $ultTrama['contacto'] == $contacto) { //agrega el servicio si no filtra por contacto o si coinciden 
                        $servicio->setUltContacto($contacto);
                        $servicios[] = $servicio;
                    }
                }
            }
        }

        return $servicios;
    }

    public function findByTipoObjeto($organizacion, $tipoObjeto)
    {
        $servicios = array();
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:Servicio')->findByTipoObjeto($organizacion, $tipoObjeto);
    }

    public function getIds($id)
    {
        $ids = array();
        $servicios = array();
        $organizacion = $this->userlogin->getOrganizacion();
        $nombre = '';
        if (is_array($id)) {    //se paso un array de grupo o servicios
            $str = [];
            $servicios = [];
            foreach ($id as $item) {   //como es array debe recorrer uno a uno los items
                if ($item == '0') {
                    $servicios = $this->findAll();
                    $str[] = 'Todos los servicios';                  
                } elseif (substr($item, 0, 1) == 'g') {
                    $grupo = $this->grupoServicio->find(intval(substr($item, 1)));
                    if($grupo != null){
                       $servicios = array_merge($servicios, $this->findAllByGrupo($grupo));
                        $servicios = array_unique($servicios);   //elimina los duplicados
                        $str[] = 'Grupo ' . $grupo->getNombre();
                    }
                } else {
                    $servicio = $this->find(intval($item));
                    if ($servicio != null) {
                        $str[] = $servicio->getNombre();
                        $servicios = array_merge($servicios, array($servicio));
                        $servicios = array_unique($servicios);   //elimina los duplicados
                    }
                }
            }
            $nombre = implode(', ', $str);
        } else {
            $nombre = '';
            if ($id == '0') {
                $servicios = $this->findAll();
                $nombre = 'Todas los Servicios';
            } elseif (substr($id, 0, 1) == 'g') {
                $grupo = $this->grupoServicio->find(intval(substr($id, 1)));
                $servicios = $this->findAllByGrupo($grupo);
                $nombre = 'Grupo ' . $grupo->getNombre();
            } else {
                $servicio = $this->find(intval($id));
                if($servicio != null){
                    $servicios = array($servicio);
                    $nombre = $servicio->getNombre();
                }
            }
        }
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
      //  die('////<pre>' . nl2br(var_export($ids, true)) . '</pre>////');
        return array('nombre' => $nombre, 'ids' => $ids);
    }
}
