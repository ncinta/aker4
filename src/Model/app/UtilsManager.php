<?php

/**
 * Libreria de utilidades generales
 * @autor claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Model\app\UserLoginManager;
//use Psr\Container\ContainerInterface;

class UtilsManager
{

    protected $userlogin;
    protected $container;
    protected $tz_local;
    private $tz_utc;

    private $coloresPrecargados = array(
        0 => '#FF5733',
        1 => '#3498DB',
        2 => '#34495E',
        3 => '#27AE60',
        4 => '#9B59B6',
        5 => '#FFF',
        6 => '#000',
        7 => '#FFFF00',
        8 => '#FFA500',
        9 => '#FFC0CB',
        10 => '#8B4513',
        11 => '#40E0D0',
        12 => '#C0C0C0'
    );

    public function __construct(UserLoginManager $userlogin, ContainerInterface $container)
    {
        $this->userlogin = $userlogin;
        $this->container = $container;

        $this->tz_utc = new \DateTimeZone('UTC');
    }

    /**
     * Convierte un periodo de fechas desde/hasta en un array con todos los dias
     * en forma infividual
     * @param string $desde  formato yyyy-mm-dd hh:ii:ss
     * @param string $hasta
     * @return string 
     */
    public function periodo2array($desde, $hasta)
    {
        $parcial = false;
        $t_desde = strtotime($desde);
        if (substr($hasta, -8) == '00:00:00') {
            $t_hasta = strtotime($hasta) - 1;  //aca voy a tener todos los dias.
        } else {
            $parcial = true;
            $t_hasta = strtotime($hasta);  //aca voy a tener todos los dias.
        }
        $periodos = array();
        //obtener todos los dias entre las fechas.
        for ($i = $t_desde; $i <= $t_hasta; $i += 86400) {
            if ($i == $t_desde) {
                $tmpDesde = date("Y-m-d", $i) . ' ' . substr($desde, 11, 5) . ':00';
            } else {
                $tmpDesde = date("Y-m-d", $i) . ' 00:00:00';
            }
            if ($i >= $t_hasta) {
                $tmpHasta = date("Y-m-d", $i) . ' ' . substr($hasta, 11, 5) . ':00';
            } else if ($i < $t_hasta) {
                $tmpHasta = date("Y-m-d", $i) . ' 23:59:59';
            }
            $periodos[] = array('desde' => $tmpDesde, 'hasta' => $tmpHasta);
        }
        if (!isset($tmpDesde)) {
            //si entra aca es porque se selecciono mal la fecha desde
            $tmpDesde = date("Y-m-d", $t_hasta) . ' ' . substr($hasta, 11, 5) . ':00';
        }
        if ($parcial) {
            $periodos[count($periodos) - 1] = array(
                'desde' => $tmpDesde,
                'hasta' => date("Y-m-d", $t_hasta) . ' ' . substr($hasta, 11, 5) . ':00'
            );
        }

        return $periodos;
    }

    public function segundos2tiempo($segundos)
    {
        $minutos = intdiv($segundos, 60);
        $horas = floor(intdiv($minutos, 60));
        $minutos2 = $minutos % 60;
        $segundos_2 = $segundos % 60 % 60 % 60;

        //        $minutos = $segundos / 60;
        //      $horas = floor($minutos / 60);
        //     $minutos2 = $minutos % 60;
        //    $segundos_2 = $segundos % 60 % 60 % 60;

        //aca solo formateo el 0 antes del nro.
        if ($horas < 10)
            $horas = '0' . $horas;
        if ($minutos2 < 10)
            $minutos2 = '0' . $minutos2;
        if ($segundos_2 < 10)
            $segundos_2 = '0' . $segundos_2;

        $resultado = $horas . ':' . $minutos2 . ':' . $segundos_2;
        return $resultado;
    }

    public function minutos2tiempo($minutos)
    {
        $horas = floor($minutos / 60);
        $minutos2 = $minutos % 60;
        //aca solo formateo el 0 antes del nro.
        if ($horas < 10)
            $horas = '0' . $horas;
        if ($minutos2 < 10)
            $minutos2 = '0' . $minutos2;

        $resultado = $horas . 'h ' . $minutos2 . 'm';
        return $resultado;
    }

    public function tiempo2segundos($delimiter, $tiempo)
    {
        $part = explode($delimiter, $tiempo);
        $hor = isset($part[0]) ? $part[0] : 0;
        $min = isset($part[1]) ? $part[1] : 0;
        $seg = isset($part[2]) ? $part[2] : 0;
        return $seg + $min * 60 + $hor * 3600;
    }

    public function tiempo2horas($tiempo)
    {
        $part = explode(':', $tiempo);
        $hor = isset($part[0]) ? $part[0] : 0;
        $min = isset($part[1]) ? $part[1] : 0;
        if ($min != 0) {
            return $hor + ($min / 100);
        } else {
            return $hor + $min;
        }
    }

    public function simplexml2array($xml)
    {
        if ($xml instanceof SimpleXMLElement) {
            $attributes = $xml->attributes();
            foreach ($attributes as $k => $v) {
                if ($v)
                    $a[$k] = (string) $v;
            }
            $x = $xml;
            $xml = get_object_vars($xml);
        }
        if (is_array($xml)) {
            if (count($xml) == 0)
                return (string) $x; // for CDATA
            foreach ($xml as $key => $value) {
                $r[$key] = $this->simplexml2array($value);
            }
            if (isset($a))
                $r['@attributes'] = $a;    // Attributes
            return $r;
        }
        return (string) $xml;
    }

    public function datetime2sqltimestamp($date = null, $includeLastHour = false)
    {
        if (is_null($date)) {
            $date = date('dd/mm/yyyy HH:nn:ss', time());
        }
        if ($date instanceof \DateTime) {
            $date = date_format($date, 'd/m/Y H:i:s');
        }
        $hora = substr($date, 11, 5);
        if (strlen($hora) > 0) {
            if ($includeLastHour) {
                if (substr($date, 11, 5) == '00:00') {
                    $hora = substr($date, 11, 5) . ':00';
                } else {
                    $hora = substr($date, 11, 5) . ':59';
                }
            } else {
                $hora = substr($date, 11, 5) . ':00';
            }
        } else {
            if ($includeLastHour) {
                $hora = '00:00:00';
            } else {
                $hora = '23:59:59';
            }
        }
        return substr($date, 6, 4) . '-' . substr($date, 3, 2) . '-' . substr($date, 0, 2) . ' ' . $hora;
    }

    public function fechaUTC2local($fecha, $formato = null)
    {
        if ($fecha == '---' || is_null($fecha))
            return $fecha;

        $fecha = new \DateTime(date_format($fecha, 'Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $fecha->setTimezone(new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
        if (is_null($formato)) {
            return $fecha;
        } else {
            return date_format($fecha, $formato);
        }
    }

    public function fechaLocal2UTC($fecha, $formato = null)
    {
        if ($this->userlogin->getUser()->getTimezone() == null) {
            $timezone = 'America/Buenos_Aires';
        } else {
            $timezone = $this->userlogin->getUser()->getTimezone();
        }
        $fecha = new \DateTime(date_format($fecha, 'Y-m-d H:i:s'), new \DateTimeZone($timezone));
        $fecha->setTimezone(new \DateTimeZone('UTC'));
        if (is_null($formato)) {
            return $fecha;
        } else {
            return date_format($fecha, $formato);
        }
    }


    public function fecha_a_UTC($fecha)
    {
        if ($this->userlogin->getTimezone() == null) {
            $this->tz_local = 'America/Buenos_Aires';
        } else {
            $this->tz_local = new \DateTimeZone($this->userlogin->getTimeZone());
        }
        $fecha = new \DateTime($fecha, $this->tz_local);
        $fecha->setTimezone($this->tz_utc);
        return date_format($fecha, 'Y-m-d H:i:s');
    }

    public function fecha_a_local($fecha)
    {
        if ($this->userlogin->getTimezone() == null) {
            $this->tz_local = 'America/Buenos_Aires';
        } else {
            $this->tz_local = new \DateTimeZone($this->userlogin->getTimeZone());
        }
        if ($fecha == '---')
            return $fecha;
        $fecha = new \DateTime($fecha, $this->tz_utc);
        //die('////<pre>' . nl2br(var_export($fecha, true)) . '</pre>////');
        $fecha->setTimezone($this->tz_local);

        return date_format($fecha, 'Y-m-d H:i:s');
    }

    public function obj2array($obj)
    {
        $out = array();
        foreach ($obj as $key => $val) {
            switch (true) {
                case is_object($val):
                    $out[$key] = $this->obj2array($val);
                    break;
                case is_array($val):
                    $out[$key] = $this->obj2array($val);
                    break;
                default:
                    $out[$key] = $val;
            }
        }
        return $out;
    }

    public function separarNombre($nombreCompleto)
    {
        // Dividir el nombre completo en partes basadas en el espacio
        $partesNombre = explode(' ', $nombreCompleto);

        // Si solo hay una parte (no hay espacio), intentar dividir por coma
        if (count($partesNombre) === 1) {
            $partesNombre = explode(',', $nombreCompleto);
        }

        // El primer elemento del array será el nombre
        $nombre = $partesNombre[0];

        // El segundo elemento del array será el apellido
        $apellido = isset($partesNombre[1]) ? trim($partesNombre[1]) : '';

        return array("nombre" => $nombre, "apellido" => $apellido);
    }
    
    public function getCentroReferencia($referencia)
    {
        if (!is_null($referencia->getPoligono())) {
            $poli = explode(',', $referencia->getPoligono());

            $lonmin = $lonmax = $latmin = $latmax = false;
            if ($referencia->isOldPoligon()) {
                $poli = array_slice($poli, 3);  //saco los primeros elementos que no me interesan.
            }
            foreach ($poli as $key => $value) {
                //$val = intval($value);
                if ($key % 2 != 0) {
                    if ($lonmin === false) {
                        $lonmin = $lonmax = $value;
                    } else {
                        $lonmin = min($lonmin, $value);
                        $lonmax = max($lonmax, $value);
                    }
                } else {
                    if ($latmin === false) {
                        $latmin = $latmax = $value;
                    } else {
                        $latmin = min($latmin, $value);
                        $latmax = max($latmax, $value);
                    }
                }
            }
            $lat = (($latmax - $latmin) / 2) + $latmin;
            $lon = (($lonmax - $lonmin) / 2) + $lonmin;
            $res = array('latitud' => $lat, 'longitud' => $lon);
        } else {
            $res = array('latitud' => $referencia->getLatitud(), 'longitud' => $referencia->getLongitud());
        }
        return $res;
    }

    public function calcularDistancia($punto1, $punto2)
    {
        // Esta es la forma vieja de calculo, parece que tenia un problema importante
        // de distorsion.
        //        if ($punto1['latitud'] != $punto2['latitud'] || $punto1['longitud'] != $punto2['longitud']) {
        //            $METROS_POR_GRADO = 111111.111111;
        //            // Delta X y delta Y en grados
        //            $dx = $punto1['longitud'] - $punto2['longitud'];
        //            $dy = $punto1['latitud'] - $punto2['latitud'];
        //
        //            // Pasar a metros
        //            $dx = cos($dy / 180.0 * pi()) * $dx * $METROS_POR_GRADO;
        //            $dy = $dy * $METROS_POR_GRADO;
        //            return sqrt($dx * $dx + $dy * $dy);
        //        } else {
        //            return 0;
        //        }

        $pi80 = M_PI / 180;
        $lat1 = $punto1['latitud'] * $pi80;
        $lng1 = $punto1['longitud'] * $pi80;
        $lat2 = $punto2['latitud'] * $pi80;
        $lng2 = $punto2['longitud'] * $pi80;

        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlng = $lng2 - $lng1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return round($r * $c * 1000);
    }

    private function distance($lat1, $lng1, $lat2, $lng2)
    {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lng1 *= $pi80;
        $lat2 *= $pi80;
        $lng2 *= $pi80;

        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlng = $lng2 - $lng1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return round($r * $c * 1000);
    }

    public function adentro_poligono($lat, $lon, $referencia)
    {
        // Obtener y procesar el polígono solo una vez
        $poligono = explode(',', $referencia->getPoligono());

        if ($referencia->isOldPoligon()) {
            $poligono = array_slice($poligono, 3);  // Recortar si es necesario
        }

        // Obtener los puntos del polígono
        $puntos = [];
        for ($i = 0, $count = count($poligono) - 1; $i < $count; $i += 2) {
            $puntos[] = ['lat' => $poligono[$i], 'lon' => $poligono[$i + 1]];
        }

        // Inicialización del bounding box (bounding box check rápido)
        $sl = ['lat' => 90.0, 'lon' => 180.0];
        $ne = ['lat' => -90.0, 'lon' => -180.0];
        $vertices = [];

        foreach ($puntos as $pto) {
            $sl['lat'] = min($sl['lat'], $pto['lat']);
            $sl['lon'] = min($sl['lon'], $pto['lon']);
            $ne['lat'] = max($ne['lat'], $pto['lat']);
            $ne['lon'] = max($ne['lon'], $pto['lon']);

            // Calcular los vértices a la vez
            $vertices[] = [
                'lat' => $pto['lat'] - $sl['lat'],
                'lon' => $pto['lon'] - $sl['lon']
            ];
        }

        // Verificación rápida con bounding box
        if ($lat < $sl['lat'] || $lon < $sl['lon'] || $lat > $ne['lat'] || $lon > $ne['lon']) {
            return false;
        }

        // Ajustar el punto de entrada
        $pto = ['lat' => $lat - $sl['lat'], 'lon' => $lon - $sl['lon']];

        // Llamar a la función en_geopol para ver si el punto está dentro del polígono
        return $this->en_geopol($pto, $vertices);
    }

    private function en_geopol($pto, $vertices)
    {
        $in_polygon = false;
        $num_vertices = count($vertices);

        foreach ($vertices as $i => $a) {
            $n = ($i + 1) % $num_vertices;
            $b = $vertices[$n];

            // Verificamos si el punto está entre los límites de latitud de los vértices a y b
            if (($a['lat'] < $pto['lat'] && $b['lat'] >= $pto['lat']) ||
                ($b['lat'] < $pto['lat'] && $a['lat'] >= $pto['lat'])
            ) {

                // Calculamos la intersección del segmento con la línea de latitud del punto
                $sx = $a['lon'] + ($b['lon'] - $a['lon']) * ($pto['lat'] - $a['lat']) / ($b['lat'] - $a['lat']);

                // Cambiar el estado si la intersección está a la derecha del punto
                if ($sx >= $pto['lon']) {
                    $in_polygon = !$in_polygon;
                }
            }
        }

        return $in_polygon;
    }

    /**
    public function adentro_poligono($lat, $lon, $referencia)
    {
        $poli = explode(',', $referencia->getPoligono());

        if ($referencia->isOldPoligon()) {
            $poli = array_slice($poli, 3);  //saco los primeros elementos que no me interesan.
        } else {
        }
        //obtengo los puntos del poligono en un array
        $puntos = array();
        for ($i = 0; $i < count($poli) - 1; $i = $i + 2) {
            $puntos[] = array('lat' => $poli[$i], 'lon' => $poli[$i + 1]);
        }       

        //inicializo el vector
        $sl = array('lat' => 90.0, 'lon' => 180.0);
        $ne = array('lat' => -90.0, 'lon' => -180.0);
        foreach ($puntos as $key => $pto) {
            $sl = array(
                'lat' => min($sl['lat'], $pto['lat']),
                'lon' => min($sl['lon'], $pto['lon'])
            );
            $ne = array(
                'lat' => max($ne['lat'], $pto['lat']),
                'lon' => max($ne['lon'], $pto['lon'])
            );
        }

        // Prueba rápida
        // si da TRUE, es 100% seguro que está afuera
        // si da FALSE, puede estar fuera o dentro
        if (
            $lat < $sl['lat'] || $lon < $sl['lon'] ||
            $lat > $ne['lat'] || $lon > $ne['lon']
        ) {
            return false;
        } else {
            foreach ($puntos as $key => $pto) {
                $vertices[] = array(
                    'lat' => $pto['lat'] - $sl['lat'],
                    'lon' => $pto['lon'] - $sl['lon']
                );
            }
            $pto = array('lat' => $lat - $sl['lat'], 'lon' => $lon - $sl['lon']);
            return $this->en_geopol($pto, $vertices);
        }
    }

    private function en_geopol($pto, $vertices)
    {
        $dentro = false;
        $counter = 0;
        foreach ($vertices as $i => $v) {
            $n = ($i + 1) % count($vertices);
            $a = $vertices[$i];
            $b = $vertices[$n];
            if (($a['lat'] < $b['lat'] && $pto['lat'] >= $a['lat'] && $pto['lat'] < $b['lat']) ||
                ($b['lat'] < $b['lat'] && $pto['lat'] >= $b['lat'] && $pto['lat'] < $a['lat'])
            ) {

                $sx = $a['lon'] + ($b['lon'] - $a['lon']) * ($pto['lat'] - $a['lat']) / ($b['lat'] - $a['lat']);

                if ($sx >= $pto['lon']) {
                    $dentro = !$dentro;
                    $counter++;
                }
            }
        }
        return $dentro;
    }
     */
    /**
     * 
      Sabiendo coordenadas inicial (x1, y1) y la coordenadas finales (x2, y2) podrias calcular el Vector Direccion

      dx=x2-x1;
      dy=y2 - y1;

      teniendo el vector direccion dx:dy (ojo, no son coordenadas, es la direccion)

      Puedes calcular el angulo
      angle=atan2(dy, dx)

      y si lo quieres en radianes
      angled= atan2 (y,x) * 180 / PI;
     */
    public function getBearing($punto, $centro)
    {

        $dx = $centro['latitud'] - $punto['latitud'];
        $dy = $centro['longitud'] - $punto['longitud'];

        $angle = atan2($dy, $dx);
        $radian = ($angle * 180) / M_PI;
        if ($radian < 0) {
            $radian = 360 + $radian;
        }
        $grad = intval($radian);
        if (in_array($grad, range(0, 29)) || in_array($grad, range(330, 359))) {
            $d = 'Sur';
        } elseif (in_array($grad, range(30, 59))) {
            $d = 'Suroeste';
        } elseif (in_array($grad, range(60, 119))) {
            $d = 'Oeste';
        } elseif (in_array($grad, range(120, 149))) {
            $d = 'Noroeste';
        } elseif (in_array($grad, range(150, 209))) {
            $d = 'Norte';
        } elseif (in_array($grad, range(210, 239))) {
            $d = 'Noreste';
        } elseif (in_array($grad, range(240, 299))) {
            $d = 'Este';
        } elseif (in_array($grad, range(300, 329))) {
            $d = 'Sureste';
        } else {
            $d = 'N/A';
        }
        return array(
            'direccion' => $d,
            'grados' => $radian,
        );
    }

    public function getRumbo($radian)
    {

        $grad = intval($radian);
        if (in_array($grad, range(0, 29)) || in_array($grad, range(330, 359))) {
            $d = 'Norte';
        } elseif (in_array($grad, range(30, 59))) {
            $d = 'Noroeste';
        } elseif (in_array($grad, range(60, 119))) {
            $d = 'Oeste';
        } elseif (in_array($grad, range(120, 149))) {
            $d = 'Suroeste';
        } elseif (in_array($grad, range(150, 209))) {
            $d = 'Sur';
        } elseif (in_array($grad, range(210, 239))) {
            $d = 'Sureste';
        } elseif (in_array($grad, range(240, 299))) {
            $d = 'Este';
        } elseif (in_array($grad, range(300, 329))) {
            $d = 'Noreste';
        } else {
            $d = 'N/A';
        }
        return array(
            'direccion' => $d,
            'grados' => $radian,
        );
    }

    /**
     * Funcion que tomando un formato y una fecha me dice si tiene la pinta especificada
     * @param type $timestamp
     * @return bool True se es formato correcto False, sino.
     */
    public function isValidDate($fmto, $date)
    {
        $result = date_parse_from_format($fmto, $date);
        return isset($result) && $result['error_count'] == 0 && $result['warning_count'] == 0;
    }

    public function isValidDesdeHasta($fmto, $desde, $hasta)
    {
        $validFmto = $this->isValidDate($fmto, $desde) && $this->isValidDate($fmto, $hasta);
        if ($validFmto) {   //el formato es valido entro a ver que desde <= hasta
            $fDesde = \DateTime::createFromFormat($fmto, $desde);
            $fHasta = \DateTime::createFromFormat($fmto, $hasta);
            return $fDesde <= $fHasta;
        }
        return $validFmto;
    }

    public function getLimiteHistorial($user = null)
    {
        if ($user == null) {
            $user = $this->userlogin->getUser();
        }
        $limit = $user->getLimiteHistorial() !== 0 ? $user->getLimiteHistorial() : null;
        if ($this->container->hasParameter('calypso.limite_historial')) {
            $limitH = $this->container->getParameter('calypso.limite_historial');
            $p = new \DateTime();
            $p->setTimestamp($limitH);
            $now = new \DateTime();
            $limitH = $p->diff($now)->days;   //devuelvo la cantidad de dias 
        } else {
            $limitH = 730;  //2 años
        }


        return is_null($limit) ? $limitH * -1 : $limit * -1;
    }

    public function UTC2local($fecha, $timezone, $formato = null)
    {
        if (is_null($timezone) || $timezone == '') {
            $timezone = 'America/Buenos_Aires';
        }
        if ($fecha == '---' || is_null($fecha))
            return $fecha;

        $fecha = new \DateTime(date_format($fecha, 'Y-m-d H:i:s'), new \DateTimeZone('UTC'));
        $fecha->setTimezone(new \DateTimeZone($timezone));
        if (is_null($formato)) {
            return $fecha;
        } else {
            return date_format($fecha, $formato);
        }
    }

    public function Local2UTC($fecha, $timezone, $formato = null)
    {
        $fecha = new \DateTime(date_format($fecha, 'Y-m-d H:i:s'), new \DateTimeZone($timezone));
        $fecha->setTimezone($timezone);
        if (is_null($formato)) {
            return $fecha;
        } else {
            return date_format($fecha, $formato);
        }
    }

    public function getApiCode()
    {
        return (chr(rand(65, 90)) . rand(0, 9) . chr(rand(65, 90)) . chr(rand(65, 90)) . rand(0, 9) . rand(0, 9));
    }

    public function getColor($i)
    {
        return $this->coloresPrecargados[$i];
    }

    public function dateStringToIso($dateString, $timezone)
    {
        $date = \DateTime::createFromFormat('d/m/Y H:i', $dateString, new \DateTimeZone($timezone));

        // Verificar si la creación del objeto DateTime fue exitosa
        if ($date === false) {
            // Manejar el error de análisis aquí
            return '';
        } else {
            $date->setTimezone(new \DateTimeZone('UTC'));
            // Formatear la fecha en el formato ISO 8601
            $formattedDate = $date->format('Y-m-d\TH:i:s.v\Z');
            return $formattedDate;
        }
    }
}
