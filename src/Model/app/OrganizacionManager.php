<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraManager;

class OrganizacionManager
{

    protected $em;
    protected $security;
    protected $userlogin;
    protected $bitacora;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        UserLoginManager $userlogin,
        BitacoraManager $bitacora
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->bitacora = $bitacora;
    }

    public function findDistribuidoresRoot()
    {
        return $this->em->getRepository('App:Organizacion')->findDistribuidoresRoot();
    }

    public function find($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->find('App:Organizacion', $organizacion->getId());
        } else {
            return $this->em->find('App:Organizacion', $organizacion);
        }
    }

    public function findAll($organizacion, array $orderBy = null)
    {
        $org = $this->find($organizacion);
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('nombre' => 'ASC');
            }
            return $this->em->getRepository('App:Organizacion')
                ->findBy(
                    array('organizacion_padre' => $org->getId()),
                    $orderBy
                );
        } else {
            return null;
        }
    }

    public function findAllClientes($organizacion, array $orderBy = null)
    {
        $org = $this->find($organizacion);
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('nombre' => 'ASC');
            }
            return $this->em->getRepository('App:Organizacion')->findAllClientes($org, $orderBy);
        } else {
            return null;
        }
    }

    public function findAllDistribuidores($organizacion, array $orderBy = null)
    {
        $org = $this->find($organizacion);
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('nombre' => 'ASC');
            }
            return $this->em->getRepository('App:Organizacion')->findAllDistribuidores($org, $orderBy);
        } else {
            return null;
        }
    }

    //busca el distribuidor que coincide con el uri pasadp por parametro.
    public function findByDominio($dominio)
    {
        if ($dominio != null) {
            return $this->em->getRepository('App:Organizacion')
                ->findByDominio($dominio);
        } else {
            return null;
        }
    }

    public function delete($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            $organizacion = $this->em->find('App:Organizacion', $organizacion->getId());
        } else {
            $organizacion = $this->em->find('App:Organizacion', $organizacion);
        }
        $this->bitacora->organizacionDelete($this->userlogin->getUser(), $organizacion);
        $this->em->remove($organizacion);
        $this->em->flush();
    }

    public function create($padre, $tipo_organizacion = null)
    {
        $org = $this->find($padre);
        if ($org) {
            $organizacion = new Organizacion();
            $organizacion->setOrganizacionPadre($org);
            if (!$tipo_organizacion || $tipo_organizacion <= 0 || $tipo_organizacion > 2) {
                $tipo_organizacion = 2; //si no viene nada lo pone como cliente por default.
            }
            $organizacion->setTipoOrganizacion($tipo_organizacion);
            return $organizacion;
        } else {
            return null;
        }
    }

    /**
     * Persiste en BD la organizacion pasa por paramemetro
     * @param Organizacion $organizacion
     * @return $organizacion
     */
    public function save(Organizacion $organizacion, $diff_add = null, $diff_remove = null, $action = null)
    {
        $id = $organizacion->getId();

        if ($id == null) { //es una insercion.
            //busco el role_master...
            if ($organizacion->getTipoOrganizacion() == 1) {
                $strRole = 'ROLE_MASTER_DISTRIBUIDOR';
            } else {
                $strRole = 'ROLE_MASTER_CLIENTE';
            }
            //busco el ROLE_MASTER_CLIENTE para asignarselo al usuario del cliente creado
            $role_master = $this->em->getRepository('App:Permiso')
                ->findByCodename('ROLE_MASTER_CLIENTE');
            if (!$role_master) {
                return false;
            }

            $organizacion->getUsuarioMaster()->setOrganizacion($organizacion);    //se setea la organizacion del usuario principal
            $organizacion->getUsuarioMaster()->setPermisos(array($role_master));
            $organizacion->getUsuarioMaster()->setAlgorithm('sha512');
            if ($organizacion->getUsuarioMaster()->getTimeZone() == null) {
                $organizacion->getUsuarioMaster()->setTimeZone($organizacion->getTimeZone());
            }
            $organizacion->getUsuarioMaster()->addRole($strRole);  //se setea el role de master
        }

        if (is_null($organizacion->getApiKey())) {
            $organizacion->setApiKey($this->generateApiKey($organizacion));
        }
        //subida del logo si es un distribuidor
        if ($organizacion->getTipoOrganizacion() == 1) {
            $organizacion->getConfiguracionDistribuidor()->upload();   //aca hace la carga del logo
        }

        $this->em->persist($organizacion);
        $this->em->flush();
        if ($action == 1 || $action == 3) {
            $this->bitacora->organizacionUpdate($this->userlogin->getUser(), $organizacion, $action);
        } elseif ($action == 2) {
            $this->bitacora->organizacionUpdateModulos($this->userlogin->getUser(), $organizacion, $diff_add, $diff_remove);
        }
        return $organizacion;
    }

    /**
     * Busca todas las organizaciones que dependan de la organizacion pasada por parametro.
     * @param Organizacion $organizacion
     * @return type Array Organizaciones
     */
    public function getTreeOrganizaciones(Organizacion $organizacion = null, $orderByName = null)
    {
        if ($organizacion === null) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        $organizaciones = $this->em->getRepository('App:Organizacion')->buscarOrganizaciones($organizacion);

        if ($orderByName != null) {
            usort($organizaciones, array($this, 'compararByNombre'));
        }
        return $organizaciones;
    }

    /**
     * Busca todos los clientes que dependan de la organizacion pasada por parametro.
     * @param Organizacion $organizacion
     * @return type Array Organizaciones
     */
    public function getTreeClientes(Organizacion $organizacion, $orderByName = null)
    {
        $organizaciones = $this->em->getRepository('App:Organizacion')->buscarClientes($organizacion);

        if ($orderByName != null) {
            usort($organizaciones, array($this, 'compararByNombre'));
        }
        return $organizaciones;
    }

    function compararByNombre($x, $y)
    {
        return strcasecmp($x->getNombre(), $y->getNombre());
    }

    /**
     * Devuelve t/f dependiendo de si en el arbol de organizaciones del root se 
     * encuentra el child
     * @param \App\Entity\Organizacion $root Orgen del arbol
     * @param \App\Entity\Organizacion $child a buscar.
     */
    public function inTreeOrganizacion($root = null, Organizacion $child)
    {
        if (is_null($root)) {
            $root = $this->userlogin->getOrganizacion();
        }
        $arbol = $this->getTreeOrganizaciones($root);
        foreach ($arbol as $nodo) {
            if ($nodo->getId() == $child->getId()) {
                return true;
            }
        }
        return false;
    }

    public function searchInTree($arbol, $child)
    {
        if (is_null($child))
            return false;
        foreach ($arbol as $nodo) {
            if ($nodo->getId() == $child->getId()) {
                return true;
            }
        }
        return false;
    }

    public function getModulosActivos(Organizacion $organizacion = null)
    {
        return $this->em->getRepository('App:Organizacion')
            ->getModulosActivos($organizacion);
    }

    public function isModuloEnabled($organizacion, $modulo)
    {
        $mod = $this->em->getRepository('App:Organizacion')->getModuloinOrganizacion($organizacion, $modulo);
        if ($mod != null) {
            return true;
        } else {
            return false;
        }
    }

    public function generateApiKey($organizacion, $seed = null)
    {
        return sha1($organizacion->getNombre() . $seed);
    }

    public function findByApiKey($apikey)
    {
        return $this->em->getRepository('App:Organizacion')->findOneBy(array(
            'apiKey' => $apikey
        ));
    }

    public function getByModulo($codename)
    {
        return $this->em->getRepository('App:Organizacion')->findAllByModulo($codename);
    }
}
