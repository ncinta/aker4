<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contratista;
use App\Entity\Organizacion as Organizacion;

class ContratistaManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($contratista)
    {
        if ($contratista instanceof Contratista) {
            return $this->em->getRepository('App:Contratista')->findBy(array('id' => $contratista->getId()));
        } else {
            return $this->em->getRepository('App:Contratista')->find(array('id' => $contratista));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Contratista')->byOrganizacion($organizacion);
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($organizacion);
            return $this->em->getRepository('App:Contratista')->findBy(array('organizacion' => $org->getId()));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $contratista = new Contratista();
        $contratista->setOrganizacion($organizacion);

        return $contratista;
    }

    public function save(Contratista $contratista)
    {
        $this->em->persist($contratista);
        $this->em->flush();
        return $contratista;
    }

    public function delete(Contratista $contratista)
    {
        $this->em->remove($contratista);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $contratista = $this->find($id);
        if (!$contratista) {
            throw $this->createNotFoundException('Código de contratista no encontrado.');
        }
        return $this->delete($contratista);
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App:Contratista', 'c')
            ->join('c.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('c.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('c.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }
     
        return $query->getQuery()->getResult();
    }
}
