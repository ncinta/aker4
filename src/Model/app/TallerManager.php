<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Taller;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of TallerManager
 *
 * @author nicolas
 */
class TallerManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($taller)
    {
        $this->em->persist($taller);
        $this->em->flush();
        return $taller;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:Taller')->findByOrg($organizacion);
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:Taller')->findOneBy(array('id' => intval($id)));
    }

    public function deleteById($id)
    {
        $taller = $this->findOne($id);
        if (!$taller) {
            throw $this->createNotFoundException('Código de ibutton no encontrado.');
        }
        return $this->delete($taller);
    }

    public function delete($taller)
    {
        $this->em->remove($taller);
        $this->em->flush();
        return true;
    }
    public function create($organizacion)
    {
        $taller = new Taller();
        $taller->setOrganizacion($organizacion);
        return $taller;
    }
}
