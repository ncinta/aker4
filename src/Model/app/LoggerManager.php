<?php

/**
 * Libreria de utilidades generales
 * @autor claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Psr\Log\LoggerInterface;

class LoggerManager {

    protected $userlogin;
    protected $request;
    protected $logger;
    protected $utils;
    private $tinic;
    private $tgen;

    public function __construct(UserLoginManager $userlogin, RequestStack $request, LoggerInterface $logger, UtilsManager $utils) {
        $this->userlogin = $userlogin;
        $this->request = $request;
        $this->logger = $logger;
        $this->utils = $utils;
    }

    public function setTimeInicial() {
        $this->tinic = time();
    }

    public function getTimeGeneracion() {
        return $this->tgen;
    }

    public function logInforme($consulta) {
        $this->tgen = $this->utils->segundos2tiempo(time() - $this->tinic);
        $data = array(
            'tgen' => $this->tgen,
            'usr' => $this->userlogin->getUser()->getUsername(),
            'org' => $this->userlogin->getOrganizacion()->getNombre(),
            'consulta' => $consulta);
        $this->logger->info($this->request->getParentRequest('_controller') . ' -> ' . json_encode($data));
        return true;
    }

}
