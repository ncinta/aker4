<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BitacoraEquipo as BitacoraEquipo;
use App\Model\app\UtilsManager;

class BitacoraEquipoManager
{

    protected $em;
    protected $utils;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function getTipoEventos()
    {
        $bitacora = new BitacoraEquipo();
        return $bitacora->getArrayTipoEvento();
    }

    private function add($tipo_evento, $ejecutor, $equipo, $data = null)
    {
        $bitacora = new BitacoraEquipo();
        $bitacora->setTipoEvento($tipo_evento);
        $bitacora->setEquipo($equipo);
        $bitacora->setOrganizacion($equipo->getOrganizacion());

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->find($ejecutor));
            }
        }

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un equipo.
     * @param type $ejecutor
     * @param type $servicio
     * @param type $equipo
     */
    public function asignarServicio($ejecutor, $servicio, $equipo)
    {
        $data = array(
            'servicio' => array(
                'id' => $servicio->getId(),
                'nombre' => $servicio->getNombre(),
            ),
        );
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_SERVICIO_ASIGNAR, $ejecutor, $equipo, $data);
    }

    /**
     * Se ejecuta cuando se retira un equipo del servicio seleccionado
     * @param type $ejecutor
     * @param type $servicio
     * @param type $equipo
     * @param type $data
     */
    public function retirarServicio($ejecutor, $servicio, $equipo, $data)
    {
        $data['servicio'] = array(
            'id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
        );
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_SERVICIO_RETIRAR, $ejecutor, $equipo, $data);
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un equipo.
     * @param type $ejecutor
     * @param type $chip
     * @param type $equipo
     */
    public function asignarChip($ejecutor, $chip, $equipo)
    {
        $data = array(
            'chip' => array(
                'id' => $chip->getId(),
                'imei' => $chip->getImei(),
                'numeroTelefono' => $chip->getNumeroTelefono(),
            ),
        );
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_CHIP_ASIGNAR, $ejecutor, $equipo, $data);
    }
    /**
     * Se ejecuta cuando se retira un equipo del servicio seleccionado
     * @param type $ejecutor
     * @param type $chip
     * @param type $equipo
     * @param type $data
     */
    public function retirarChip($ejecutor, $chip, $equipo)
    {
        $data = array(
            'chip' => array(
                'id' => $chip->getId(),
                'imei' => $chip->getImei(),
                'numeroTelefono' => $chip->getNumeroTelefono(),
            ),
        );
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_CHIP_RETIRAR, $ejecutor, $equipo, $data);
    }

    public function equipoNew($ejecutor, $equipo)
    {
        $data = array(
            'equiop' => $equipo->data2array(),
        );
        $data['equipo']['created_at'] = $equipo->getCreatedAt()->format('d/m/Y H:i:s');
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_NEW, $ejecutor, $equipo, $data);
    }

    public function equipoUpdate($ejecutor, $equipo, $equipoOld)
    {
        if ($equipo && $equipoOld) {
            $data = array(
                'equipo' => $equipo->data2array(),
                'equipoOld' => $equipoOld,
                'diff' => $this->arrayRecursiveDiff($equipo->data2array(), $equipoOld),
            );

            $this->add(BitacoraEquipo::EVENTO_EQUIPO_UPDATE, $ejecutor, $equipo, $data);
        }
    }

    public function transferirEquipo($ejecutor, $equipo, $organizacionOld, $organizacionNew, $extra = null)
    {
        $data = array(
            'equipo' => $equipo->data2array(),
            'origen' => $organizacionOld->data2array(),
            'destino' => $organizacionNew->data2array(),
        );
        $this->add(BitacoraEquipo::EVENTO_EQUIPO_TRANSFERIR, $ejecutor, $equipo, $data);
    }

    /**
     * Obtiene los ultimos 100 registros de la bitacora, ordenados en orden cronologica descendiente.
     * @param type $equipo
     * @return type Registro
     */
    public function getRegistros($equipo)
    {
        return $this->em->getRepository('App:BitacoraEquipo')->findBy(array(
            'equipo' => $equipo->getId()
        ), array('created_at' => 'DESC'), 100);
    }

    public function obtener($organizacion, $desde, $hasta, $tevento = null)
    {
        $desde = $this->utils->datetime2sqltimestamp($desde);
        $hasta = $this->utils->datetime2sqltimestamp($hasta, true);
        return $this->em->getRepository('App:BitacoraEquipo')->byOrganizacion($organizacion, $desde, $hasta, $tevento);
    }

    public function parse($registro)
    {
        $str = '';
        $data = $registro->getData();
        switch ($registro->getTipoEvento()) {
            case $registro::EVENTO_EQUIPO_SERVICIO_ASIGNAR:
                $str = sprintf('Se asigna al servicio %s.', $data['servicio']['nombre']);
                break;
            case $registro::EVENTO_EQUIPO_SERVICIO_RETIRAR:
                $str = sprintf('Se retira el equipo %s por <b>%s</b>.', $data['servicio']['nombre'], isset($data['motivo']) ? $data['motivo'] : 'motivo desconocido');
                break;
            case $registro::EVENTO_EQUIPO_TRANSFERIR:
                $str = sprintf('Se transfiere equipo desde <b>%s</b> hasta <b>%s</b>.', $data['origen']['nombre'], $data['destino']['nombre']);
                break;
            case $registro::EVENTO_EQUIPO_UPDATE:
                if (isset($data['diff'])) {
                    $s = array();
                    foreach ($data['diff'] as $key => $dat) {
                        if ($this->getLabel($key)) {
                            $s[] = $this->getLabel($key) . ': ' . $dat;
                        }
                    }
                    if (count($s) > 0)
                        $str = 'Se cambio, ' . implode(', ', $s);
                }
                break;
            case $registro::EVENTO_EQUIPO_CHIP_ASIGNAR:
                $str = sprintf('Se asigna el chip %s con numero telefónico <b>%s</b>.', $data['chip']['imei'], isset($data['chip']['numeroTelefono']) ? $data['chip']['numeroTelefono'] : 'teléfono desconocido');
                break;
            case $registro::EVENTO_EQUIPO_CHIP_RETIRAR:
                $str = sprintf('Se retira el chip %s con numero telefónico <b>%s</b>.', $data['chip']['imei'], isset($data['chip']['numeroTelefono']) ? $data['chip']['numeroTelefono'] : 'teléfono desconocido');
                break;
            default:

                break;
        }
        return $str;
    }

    private $label = array(
        'nombre' => 'Nombre',
        'mdmid' => 'ModemId',
        'imei' => 'Imei',
        'marca' => 'Marca',
        'modelo' => 'Modelo',
        'anioFabricacion' => 'Año',
        'color' => 'Color',
        'rendimiento' => 'Rendimiento',
        'serie' => 'Serie',
        'fecha' => 'Fecha',
    );

    private function getLabel($key)
    {
        if (isset($this->label[$key])) {
            return $this->label[$key];
        } else {
            return false;
        }
    }

    private function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }
}
