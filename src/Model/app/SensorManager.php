<?php

/**
 * ChipManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Servicio;
use App\Entity\Sensor;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\UserLoginManager;

class SensorManager
{

    protected $em;
    protected $security;
    protected $bitacora;
    private $sensor;
    private $sensor_array;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        BitacoraServicioManager $bitacora,
        UserLoginManager $userlogin
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
    }

    /**
     * Buscan un chip por su id u objeto.
     * @param Chip|Int $sensor   el Chip o el id del chip
     * @return type 
     */
    public function find($sensor)
    {
        if ($sensor instanceof Chip) {
            $this->sensor = $this->em->getRepository('App:Sensor')->find($sensor->getId());
        } else {
            $this->sensor = $this->em->getRepository('App:Sensor')->find($sensor);
        }
        $this->sensor_array = $this->sensor->data2array();
        return $this->sensor;
    }

    public function findById($id)
    {
        $this->sensor = $this->em->getRepository('App:Sensor')->find($id);
        $this->sensor_array = $this->sensor->data2array();
        return $this->sensor;
    }

    public function getTiposSensores($servicio, $tipoSensor)
    {
        $modelo = $servicio->getEquipo()->getModelo();  //esto es el modelo del equipo
        return $this->em->getRepository('App:ModeloSensor')->findBy(array('modelo' => $modelo->getId(), 'tipoSensor' => $tipoSensor), array('nombre' => 'ASC'));
    }

    public function getModeloSensor($id)
    {
        return $this->em->getRepository('App:ModeloSensor')->find($id);
    }

    public function create(Servicio $servicio)
    {
        if ($servicio) {
            $sensor = new Sensor();
            $sensor->setServicio($servicio);
            return $sensor;
        } else {
            return null;
        }
    }

    public function save(Sensor $sensor)
    {
        $alta = is_null($sensor->getId());  //es para saber si es una alta.
        if (!$alta) {
            $this->sensor_array = $sensor->data2array();
        }

        $this->em->persist($sensor);
        $this->em->flush();

        if ($alta) {
            $this->bitacora->sensorNew($this->userlogin->getUser(), $sensor);
        } else {
            $this->bitacora->sensorUpdate($this->userlogin->getUser(), $sensor, $this->sensor_array);
        }
        return $sensor;
    }

    public function delete(Sensor $sensor)
    {
        $this->bitacora->sensorDelete($this->userlogin->getUser(), $sensor);
        $this->em->remove($sensor);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $chip = $this->findById($id);
        if (!$chip) {
            throw $this->createNotFoundException('Código de Sensor no encontrado.');
        }
        return $this->delete($chip);
    }

    public function getSensoresDigitales($equipo)
    {
        if (!is_null($equipo)) {
            return $this->em->getRepository('App:ModeloSensor')->findBy(array('modelo' => $equipo->getModelo()->getId(), 'tipoSensor' => 2), array('nombre' => 'ASC'));
        } else {
            return null;
        }
    }
}
