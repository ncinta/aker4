<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chip;
use App\Entity\Organizacion as Organizacion;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of BreadCrumbsManager
 *
 * @author yesica
 */
class BreadcrumbManager
{

    protected $security;
    protected $em;
    protected $session;
    protected $request;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, SessionInterface $session, RequestStack $request)
    {
        $this->security = $security;
        $this->em = $em;
        $this->session = $session;
        $this->request = $request;
    }

    public function top()
    {
        $pila = $this->session->get('pilaRetorno');
        if (!is_null($pila)) {
            $top = array_pop($pila);
            $volver = $top["path"];
        } else {
            $organizacion = $this->security->getToken()->getUser()->getOrganizacion();
            if ($organizacion->getTipoOrganizacion() == 1)
                $volver = '/dist/homepage';
            else {
                $volver = '/apmon/homepage';
            }
        }
        $this->session->set('volver', $volver);
    }

    public function pop()
    {
        $pila = $this->session->get('pilaRetorno');
        if (!is_null($pila)) {

            array_pop($pila);

            $this->session->set('pilaRetorno', $pila);

            $top = array_pop($pila);

            $volver = isset($top['path']) ? $top['path'] : '';

            $this->session->set('volver', $volver);

            $this->armarBreadcrumb($pila);
        }
    }

    public function push($path, $leyenda)
    {
        $pila = $this->session->get('pilaRetorno');
        if (!is_null($pila)) {
            $vista['path'] = $path;
            $vista['leyenda'] = $leyenda;
            if (in_array($vista, $pila)) {
                do {
                    $top = array_pop($pila);
                } while (strcasecmp($path, $top["path"]));

                $this->session->set('pilaRetorno', $pila);
            }

            $top = array_pop($pila); //este pop no saca nada de la pila verdadera es solo para obtener el top
            $pila = $this->session->get('pilaRetorno');

            if (isset($top['path'])) {
                $volver = $top['path'];
            } else {
                $volver = '/';
            }
            $this->session->set('volver', $volver);

            $pila[] = $vista;
            $this->session->set('pilaRetorno', $pila);

            $this->armarBreadcrumb($pila);
        }
    }
    public function pushPorto($path, $leyenda)
    {
        $pila = $this->session->get('pilaRetorno');
        if (!is_null($pila)) {
            $vista['path'] = $path;
            $vista['leyenda'] = $leyenda;
            if (in_array($vista, $pila)) {
                do {
                    $top = array_pop($pila);
                } while (strcasecmp($path, $top["path"]));

                $this->session->set('pilaRetorno', $pila);
            }

            $top = array_pop($pila); //este pop no saca nada de la pila verdadera es solo para obtener el top
            $pila = $this->session->get('pilaRetorno');

            if (isset($top['path'])) {
                $volver = $top['path'];
            } else {
                $volver = '/';
            }
            $this->session->set('volver', $volver);

            $pila[] = $vista;
            $this->session->set('pilaRetorno', $pila);

            $this->armarBreadcrumbPorto($pila);
        }
    }

    public function clear($path, $leyenda)
    {
        $vista['path'] = $path;
        $vista['leyenda'] = $leyenda;
        $pila[] = $vista;
        $this->session->set('pilaRetorno', $pila);
        $volver = null;
        $this->session->set('volver', $volver);
        $this->armarBreadcrumb($pila);
    }

    public function getVolver()
    {
        if (is_null($this->session->get('volver')) || $this->session->get('volver') == '') {
            return '/';
        }
        return $this->session->get('volver');
    }

    public function armarBreadcrumb($pila)
    {
        $bread = null;
        $top = array_pop($pila);
        $separator = '';
        foreach ($pila as $i => $vista) {
            if ($i > 0) {
                $separator = ' <div class="separator"> > </div> ';
            }
            $bread = $bread . $separator . '<a href="' . $vista['path'] . '">' . $vista['leyenda'] . '</a>';
        }

        if (isset($top['leyenda']) && !is_null($top['leyenda'])) {
            $bread = $bread . ' <div class="separator"> > </div> ' . isset($top['leyenda']) ? $top['leyenda'] : '---';
        }
        $bread = $this->session->set('breadcrumb', $bread);
    }


    public function armarBreadcrumbPorto($pila) //echo con chatgpt, una locura jaja
    {
        $pilaCount = count($pila);
        $bread = null;
        $top = array_pop($pila);
        $separator = '';

        // Obtener las últimas 4 páginas de la pila
        $lastPages = array_slice($pila, max(0, $pilaCount - 4));

        foreach ($lastPages as $i => $vista) {
            if ($i > 0) {
                $separator = '/ ';
            }
            $bread = $bread . $separator . '<li><a href="' . $vista['path'] . '">' . $vista['leyenda'] . '</a></li>';
        }

        $bread = $bread . '/' . $top['leyenda'];

        $this->session->set('breadcrumb', $bread);

        return $bread;
    }
}
