<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\VehiculoModelo;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of VehiculoModeloManager
 *
 * @author nicolas
 */
class VehiculoModeloManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($modelo)
    {
        //   die('////<pre>' . nl2br(var_export($modelo->getNombre(), true)) . '</pre>////');
        $this->em->persist($modelo);
        $this->em->flush();
        return $modelo;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:VehiculoModelo')->findByOrg($organizacion);
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:VehiculoModelo')->findOneBy(array('id' => $id * 1));
    }

    public function findByOrganizacion($organizacion)
    {
        return $this->em->getRepository('App:VehiculoModelo')->findByOrg($organizacion);
    }

    public function deleteById($id)
    {
        $modelo = $this->findOne($id);
        if (!$modelo) {
            throw $this->createNotFoundException('Código de modelo no encontrado.');
        }
        return $this->delete($modelo);
    }

    public function delete($modelo)
    {
        $this->em->remove($modelo);
        $this->em->flush();
        return true;
    }
    public function create($organizacion)
    {
        $modelo = new VehiculoModelo();
        $modelo->setOrganizacion($organizacion);
        return $modelo;
    }
}
