<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\HistoricoIbutton;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of HistoricoIbuttonManager
 *
 * @author nicolas
 */
class HistoricoIbuttonManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($historico)
    {
        $this->em->persist($historico);
        $this->em->flush();
        return $historico;
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:HistoricoIbutton')->findOneBy(array('id' => $id));
    }

    public function findByIbutton($ibutton)
    {
        return $this->em->getRepository('App:HistoricoIbutton')->findByIbutton($ibutton);
    }

    public function findIbuttonByChofer($chofer)
    {
        $result = array();
        $historico = $this->em->getRepository('App:HistoricoIbutton')->findByChofer($chofer);
        foreach ($historico as $data) {
            $result[] = $data->getIbutton()->getCodigo();
        }
        return $result;
    }

    public function findByIbuttonFecha($ibutton, $desde, $hasta)
    {
        if ($ibutton) {
            return $this->em->getRepository('App:HistoricoIbutton')->findByIbuttonFecha($ibutton, $desde, $hasta);
        }
        return null;
    }

    public function findByIbuttonChoferFecha($ibutton, $chofer, $desde, $hasta)
    {
        if ($ibutton) {
            return $this->em->getRepository('App:HistoricoIbutton')->findByIbuttonChoferFecha($ibutton, $chofer, $desde, $hasta);
        }
        return null;
    }

    public function deleteById($id)
    {
        $historico = $this->findOne($id);
        if (!$historico) {
            throw $this->createNotFoundException('Código de ibutton no encontrado.');
        }
        return $this->delete($historico);
    }

    public function delete($historico)
    {
        $this->em->remove($historico);
        $this->em->flush();
        return true;
    }
}
