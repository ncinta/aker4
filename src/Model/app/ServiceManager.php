<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\UtilsManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServiceManager
{

    protected $container;
    private $utils;
    private $cliente;

    public function __construct(RequestStack $request, UtilsManager $utils, ContainerInterface $container, HttpClientInterface $cliente)
    {
        $this->request = $request;
        $this->utils = $utils;
        $this->container = $container;
        $this->cliente = $cliente;
    }

    public function consulServicios()
    {
        $data = null;
        $status = $this->getStatusService();
        if ($status) {
            foreach ($status as $service) {
                if ($service['Status'] == 'passing') {
                    $data[1][] = [    //ok
                        'servicio' => $service['Name'],
                        'estado' => $service['Status']
                    ];
                } elseif ($service['Status'] == 'critical') {
                    $data[2][] = [    //caidos
                        'servicio' => $service['Name'],
                        'estado' => $service['Status']
                    ];
                } else {
                    $data[9][] = [    //otros
                        'servicio' => $service['Name'],
                        'estado' => $service['Status']
                    ];
                }
            }
        }
        //dd($status);
        //dd($data);
        return $data;
    }
    public function getStatusService()
    {
        //http://172.31.0.19:9121/v1/health/node/db876c5d10f8
        //http://172.31.0.19:9121/v1/agent/checks?filter=CheckID=="openstreetmap-check"
        //"Status":"passing" || "Status":"critical"
        $result = null;
        try {
            $response = $this->cliente->request(
                'GET',
                $this->container->getParameter('consul_addr_config'),
                [
                    'headers' => ['Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'],
                    'auth_basic' => [
                        $this->container->getParameter('consul_usr_config'),
                        $this->container->getParameter('consul_pass_config')
                    ]
                ]
            );
            //dd($response->getContent());
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                $result = json_decode($response->getContent(), true);
            }
        } catch (TransportException $e) {
            return false;
        }
        return $result;
    }

    public function obtenerDashboard($user)
    {
        return array();
    }

    /** obtiene el listado de clientes con mas servicios activos vs totales */
    private function obtenerTopClientes($user) {

    }
}
