<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\VehiculoModelo;
use App\Entity\VehiculoFichaTecnica;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of VehiculoFichaTecnicaManager
 *
 * @author nicolas
 */
class VehiculoFichaTecnicaManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($fichaTecnica)
    {
        $this->em->persist($fichaTecnica);
        $this->em->flush();
        return $fichaTecnica;
    }


    public function delFichaTecnica($idFicha)
    {
        $ficha = $this->em->getRepository('App:VehiculoFichaTecnica')->findOneBy(array(
            'id' => intval($idFicha)
        ));
        $this->em->remove($ficha);
        $this->em->flush();
        return true;
    }
}
