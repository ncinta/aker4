<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Mantenimiento;
use App\Entity\IndiceMantenimiento;
use App\Model\app\OrganizacionManager;

class MantenimientoManager
{

    protected $em;
    protected $organizacion;

    public function __construct(EntityManagerInterface $em, OrganizacionManager $organizacion)
    {
        $this->em = $em;
        $this->organizacion = $organizacion;
    }

    public function findAsociados($organizacion = null)
    {
        if (is_null($organizacion)) {    //le pase una organizacion.
            $organizacion = $this->organizacion->find($organizacion);
        }

        return $this->em->getRepository('App:Mantenimiento')->getAll($organizacion);
    }

    public function find($mantenimiento = null)
    {
        if ($mantenimiento instanceof Mantenimiento) {
            return $this->em->getRepository('App:Mantenimiento')->find($mantenimiento->getId());
        } else {
            return $this->em->getRepository('App:Mantenimiento')->find($mantenimiento);
        }
    }

    public function create($padre)
    {
        $org = $this->organizacion->find($padre);
        if ($org) {
            $mant = new Mantenimiento();
            $mant->setPropietario($org);
            $org->addMantenimiento($mant);
            return $mant;
        } else {
            return null;
        }
    }

    public function save(Mantenimiento $mantenimiento)
    {
        if ($mantenimiento->getPorDias() && is_null($mantenimiento->getIntervaloDias()) && is_null($mantenimiento->getAvisoDias()))
            return false;
        if ($mantenimiento->getPorHoras() && is_null($mantenimiento->getIntervaloHoras()) && is_null($mantenimiento->getAvisoHoras()))
            return false;
        if ($mantenimiento->getPorKilometros() && is_null($mantenimiento->getIntervaloKilometros()) && is_null($mantenimiento->getAvisoKilometros()))
            return false;
        if (is_null($mantenimiento->getIntervaloDias()) && is_null($mantenimiento->getIntervaloHoras()) && is_null($mantenimiento->getIntervaloKilometros())) {
            return false;
        }
        $this->em->persist($mantenimiento);
        $this->em->flush();
        return $mantenimiento;
    }

    public function delete($id)
    {
        $mantenim = $this->find($id);
        $this->em->remove($mantenim);
        $this->em->flush();
        return true;
    }
}
