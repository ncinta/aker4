<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Deposito as Deposito;
use App\Entity\Organizacion as Organizacion;

class DepositoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($deposito)
    {
        if ($deposito instanceof Deposito) {
            return $this->em->getRepository('App:Deposito')->findOneBy(array('id' => $deposito->getId()));
        } else {
            return $this->em->getRepository('App:Deposito')->findOneBy(array('id' => $deposito));
        }
    }

    public function findAllByOrganizacion($org)
    {
        if ($org instanceof Organizacion) {
            return $this->em->getRepository('App:Deposito')->byOrganizacion($org, array('d.id' => 'ASC'));
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($org);
            return $this->em->getRepository('App:Deposito')->byOrganizacion($org, array('d.id' => 'ASC'));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $deposito = new Deposito();
        $deposito->setOrganizacion($organizacion);

        return $deposito;
    }

    public function save(Deposito $deposito)
    {
        $this->em->persist($deposito);
        $this->em->flush();
        return $deposito;
    }

    public function delete(Deposito $deposito)
    {
        $this->em->remove($deposito);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $deposito = $this->find($id);
        if (!$deposito) {
            throw $this->createNotFoundException('Código de depósito no encontrado.');
        }
        return $this->delete($deposito);
    }
}
