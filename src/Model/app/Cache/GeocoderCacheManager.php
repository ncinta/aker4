<?php

namespace App\Model\app\Cache;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeocoderCacheManager
{
    private $cache;
    private $tiempoDeExpiracion = 3600; //1 hora

    public function __construct(CacheItemPoolInterface $cacheDb0)
    {
        $this->cache = $cacheDb0;
    }

    public function searchCache($lat, $lon)
    {
        $str = $lat . '|' . $lon;
        // Obtiene el valor de la caché utilizando la clave
        $item = $this->cache->getItem($str);
        $cacheValue = $item->get();

        if ($item->isHit()) {
            // El valor se encontraba en la caché y se almacenó en $cacheValue
            return $cacheValue;
        } else {
            return false;
        }

        return false;
    }

    public function addCache($lat, $lon, $data)
    {
        $str = $lat . '|' . $lon;
        //dd($data);
        $this->cache->getItem($str)->set($data);
        $item = $this->cache->getItem($str);
        $item->set($data);
        $item->expiresAfter($this->tiempoDeExpiracion);
        return $this->cache->save($item);
    }

}
