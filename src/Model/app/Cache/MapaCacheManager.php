<?php

namespace App\Model\app\Cache;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapaCacheManager
{
    private $cache;

    public function __construct(CacheItemPoolInterface $cacheDb2)
    {
        $this->cache = $cacheDb2;
    }

    public function getTimeCache($servicio)
    {
        $str = 's' . $servicio->getId();  
        // Obtiene el valor de la caché utilizando la clave
        $item = $this->cache->getItem($str);
        $cacheValue = $item->get();
        if ($item->isHit()) {            
            // El valor se encontraba en la caché y se almacenó en $cacheValue
            return $cacheValue;
        } else {
            return false;
        }
        return false;
    }

    public function update($servicio)
    {
        $str = 's' . $servicio->getId();        
        $timestamp = $servicio->getUltFechahora()->getTimestamp();
        $this->cache->getItem($str)->set($timestamp);
        $item = $this->cache->getItem($str);
        $item->set($timestamp);
       // $item->expiresAfter($this->tiempoDeExpiracion);
        return $this->cache->save($item);
    }

    public function its_caduced($servicio) {
        $timeServicio = $servicio->getUltFechahora()->getTimestamp();         
        return $this->getTimeCache($servicio) <= $timeServicio;
    }

}
