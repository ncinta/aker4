<?php

namespace App\Model\app\Cache;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class P44CacheManager
{
    private $cache;
    private $tiempoDeExpiracion = 3600; //1 hora

    public function __construct(CacheItemPoolInterface $cacheDb1)
    {
        $this->cache = $cacheDb1;
    }

    private $keyRedis = 'p44';

    public function saveToken2Redis($token, $expire)
    {
        //dd($data);
        $this->cache->getItem($this->keyRedis)->set($token);
        $item = $this->cache->getItem($this->keyRedis);
        $item->set($token);
        $item->expiresAfter($expire);

        $result = $this->cache->save($item);

        return $result;
    }

    public function getToken2Redis()
    {
        // Obtiene el valor de la caché utilizando la clave
        $item = $this->cache->getItem($this->keyRedis);
        $cacheValue = $item->get();
        if ($item->isHit()) {
            // El valor se encontraba en la caché y se almacenó en $cacheValue           
            return $cacheValue;
        } else {
            return false;
        }
        return false;
    }

}
