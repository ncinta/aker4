<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BitacoraServicio as BitacoraServicio;
use App\Entity\Chofer;
use App\Model\app\UtilsManager;

class BitacoraServicioManager
{

    protected $em;
    protected $utils;
    protected $isCliente = 0;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function getTipoEventos()
    {
        $bitacora = new BitacoraServicio();
        return $bitacora->getArrayTipoEvento();
    }

    private function add($tipo_evento, $ejecutor, $servicio, $data = null)
    {
        $bitacora = new BitacoraServicio();
        $bitacora->setTipoEvento($tipo_evento);
        $bitacora->setServicio($servicio);
        $bitacora->setOrganizacion($servicio->getOrganizacion());

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->find($ejecutor));
            }
        }

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    private function addFuelApp($tipo_evento, $ejecutor, $servicio, $data = null)
    {
        $bitacora = new BitacoraServicio();
        $bitacora->setTipoEvento($tipo_evento);
        $bitacora->setServicio($servicio);
        $bitacora->setOrganizacion($servicio->getOrganizacion());

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            //  die('////<pre>'.nl2br(var_export($ejecutor->getNombre(), true)).'</pre>////');
            if ($ejecutor instanceof Chofer) {
                $bitacora->setChofer($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->find($ejecutor));
            }
        }

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un equipo.
     * @param type $ejecutor
     * @param type $servicio
     * @param type $equipo
     */
    public function asignarEquipo($ejecutor, $servicio, $equipo)
    {
        $data = array(
            'equipo' => array(
                'id' => $equipo->getId(),
                'mdmid' => $equipo->getMdmid(),
            ),
        );
        $this->add(BitacoraServicio::EVENTO_SERVICIO_ASIGNAR_EQUIPO, $ejecutor, $servicio, $data);
    }

    /**
     * Se ejecuta cuando se retira un equipo del servicio seleccionado
     * @param type $ejecutor
     * @param type $servicio
     * @param type $equipo
     * @param type $data
     */
    public function retirarEquipo($ejecutor, $servicio, $equipo, $data)
    {
        $data['equipo'] = array(
            'id' => $equipo->getId(),
            'mdmid' => $equipo->getMdmid(),
        );
        $this->add(BitacoraServicio::EVENTO_SERVICIO_RETIRAR_EQUIPO, $ejecutor, $servicio, $data);
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un equipo.
     * @param type $ejecutor
     * @param type $servicio
     * @param type $usuario
     */
    public function asignarUsuario($ejecutor, $servicio, $usuario)
    {
        $data['usuario'] = array(
            'id' => $usuario->getId(),
            'usuario' => $usuario->getNombre(),
        );
        $this->add(BitacoraServicio::EVENTO_SERVICIO_ASIGNAR_USUARIO, $ejecutor, $servicio, $data);
    }

    /**
     * Se ejecuta cuando se retira un equipo del servicio seleccionado
     * @param type $ejecutor
     * @param type $servicio
     * @param type $usuario
     * @param type $data
     */
    public function retirarUsuario($ejecutor, $servicio, $usuario)
    {
        $data['usuario'] = array(
            'id' => $usuario->getId(),
            'usuario' => $usuario->getNombre(),
        );
        $this->add(BitacoraServicio::EVENTO_SERVICIO_RETIRAR_USUARIO, $ejecutor, $servicio, $data);
    }

    /**
     * Registra en la bitacora la carga de combustible manual.
     * @param type $ejecutor Usuario que hace la operacion
     * @param type $servicio Servicio que realizo la carga
     * @param type $carga Carga de combustible.
     */
    public function combustibleCreateManual($ejecutor, $servicio, $carga)
    {
        $data = array(
            'carga' => $carga->data2array(),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );
        $this->add(BitacoraServicio::EVENTO_COMBUSTIBLE_CREATE_MANUAL, $ejecutor, $servicio, $data);
    }

    /**
     * Registra en la bitacora la carga de combustible manual.
     * @param type $ejecutor Usuario que hace la operacion
     * @param type $servicio Servicio que realizo la carga
     * @param type $carga Carga de combustible.
     */
    public function combustibleCreateApp($ejecutor, $servicio, $carga)
    {
        $data = array(
            'carga' => $carga->data2array(),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );
        $this->addFuelApp(BitacoraServicio::EVENTO_COMBUSTIBLE_CREATE_APP, $ejecutor, $servicio, $data);
    }

    /**
     * Registra en la bitacora la carga de combustible automatica. Viene por webservice.
     * @param type $ejecutor Usuario que hace la operacion
     * @param type $servicio Servicio que realizo la carga
     * @param type $carga Carga de combustible.
     */
    public function combustibleCreateAutomatica($request, $servicio, $carga)
    {
        $data = array(
            'remote_ip' => $request->server->get('REMOTE_ADDR'),
            'server_ip' => $request->server->get('SERVER_ADDR'),
            'carga' => $carga->data2array(),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );

        $this->add(BitacoraServicio::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA, null, $servicio, $data);
    }

    /**
     * Se ejecuta cuando se realiza la modificación de cargas de combustible realizadas
     * anteriormente.
     * @param type $ejecutor
     * @param type $servicio
     * @param type $carga Object con la carga nueva.
     * @param type $cargaOld Array con la carga vieja.
     */
    public function combustibleUpdate($ejecutor, $servicio, $carga, $cargaOld = array())
    {
        if (is_null($cargaOld)) {
            $diff = $carga->data2array();
        } else {
            $diff = array_diff_assoc($carga->data2array(), $cargaOld);
        }
        //die('////<pre>'.nl2br(var_export($diff, true)).'</pre>////');

        $data = array(
            'carga' => $carga->getId(),
            'diff' => $diff,
        );
        $this->add(BitacoraServicio::EVENTO_COMBUSTIBLE_UPDATE, $ejecutor, $servicio, $data);
    }

    /**
     * Se ejecuta y registra la eliminación de una carga de combustible en el sistema.
     * @param type $ejecutor
     * @param type $servicio
     * @param type $carga
     */
    public function combustibleDelete($ejecutor, $servicio, $carga)
    {
        $data = array(
            'carga' => $carga->data2array(),
        );
        $this->add(BitacoraServicio::EVENTO_COMBUSTIBLE_DELETE, $ejecutor, $servicio, $data);
    }

    public function servicioNew($ejecutor, $servicio)
    {
        $data['servicio'] = $servicio->data2array();
        if ($servicio->getEquipo()) {
            $data['equipo'] = $servicio->getEquipo()->data2array();
        }
        $data['servicio']['created_at'] = $servicio->getCreatedAt()->format('d/m/Y H:i:s');
        $this->add(BitacoraServicio::EVENTO_SERVICIO_NEW, $ejecutor, $servicio, $data);
    }

    public function servicioUpdate($ejecutor, $servicio, $servicioOld)
    {
        if (is_null($servicioOld)) {
            $diff = $servicio->data2array();
        } else {
            $diff = $this->arrayRecursiveDiff($servicio->data2array(), $servicioOld);
        }

        $data = array(
            'servicio' => $servicio->data2array(),
            'anterior' => $servicioOld,
            'diff' => $diff,
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_UPDATE, $ejecutor, $servicio, $data);
    }

    public function programacion($ejecutor, $servicio, $prg)
    {
        $data = array(
            'servicio' => $servicio->data2array(),
            'prg' => $prg,
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_PRG, $ejecutor, $servicio, $data);
    }

    public function corteMotor($ejecutor, $servicio, $corte)
    {
        $data = array(
            'servicio' => $servicio->data2array(),
            'corte' => $corte,
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_CORTEMOTOR, $ejecutor, $servicio, $data);
    }

    /**
     * 
     * @param type $ejecutor
     * @param type $servicio
     * @param type $accion solo debe venir get cuando se pide y set cuando se setea
     * @param type $respuesta solo viene cuando se recibe la version desde el ws
     */
    public function queryVersion($ejecutor, $servicio, $accion, $respuesta = null)
    {
        $data = array(
            'servicio' => $servicio->data2array(),
            'accion' => $accion
        );
        if ($respuesta) {
            $data['version'] = $respuesta;
        }

        $this->add(BitacoraServicio::EVENTO_SERVICIO_QUERYVERSION, $ejecutor, $servicio, $data);
    }

    public function cerrojo($ejecutor, $servicio, $accion)
    {
        $data = array(
            'servicio' => $servicio->data2array(),
            'accion' => $accion,
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_CERROJO, $ejecutor, $servicio, $data);
    }

    public function sensorNew($ejecutor, $sensor)
    {
        $data = array(
            'sensor' => $sensor->data2array(),
        );
        $data['sensor']['created_at'] = $sensor->getCreatedAt()->format('d/m/Y H:i:s');
        $this->add(BitacoraServicio::EVENTO_SERVICIO_SENSOR_NEW, $ejecutor, $sensor->getServicio(), $data);
    }

    public function sensorUpdate($ejecutor, $sensor, $sensorOld)
    {
        $data = array(
            'sensor' => $sensor->data2array(),
            'sensorOld' => $sensorOld,
            'diff' => $this->arrayRecursiveDiff($sensor->data2array(), $sensorOld),
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_SENSOR_UPDATE, $ejecutor, $sensor->getServicio(), $data);
    }

    public function sensorDelete($ejecutor, $sensor)
    {
        $data = array(
            'sensor' => $sensor->data2array(),
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_SENSOR_DELETE, $ejecutor, $sensor->getServicio(), $data);
    }

    public function inputNew($ejecutor, $input)
    {
        $data = array(
            'input' => $input->data2array(),
        );
        $data['input']['created_at'] = $input->getCreatedAt()->format('d/m/Y H:i:s');
        $this->add(BitacoraServicio::EVENTO_SERVICIO_INPUT_NEW, $ejecutor, $input->getServicio(), $data);
    }

    public function inputUpdate($ejecutor, $input, $inputOld)
    {
        $data = array(
            'input' => $input->data2array(),
            'inputOld' => $inputOld,
            'diff' => $this->arrayRecursiveDiff($input->data2array(), $inputOld),
        );

        $this->add(BitacoraServicio::EVENTO_SERVICIO_INPUT_UPDATE, $ejecutor, $input->getServicio(), $data);
    }

    public function inputDelete($ejecutor, $input)
    {
        $data['input'] = $input->data2array();
        $this->add(BitacoraServicio::EVENTO_SERVICIO_INPUT_DELETE, $ejecutor, $input->getServicio(), $data);
    }

    public function servicioAlta($ejecutor, $servicio, $data)
    {
        $this->add(BitacoraServicio::EVENTO_SERVICIO_UPDATE, $ejecutor, $servicio, $data);
    }

    public function servicioBaja($ejecutor, $servicio, $data)
    {
        $data['fecha_baja'] = $servicio->getFechaBaja()->format('d/m/Y H:i');
        $this->add(BitacoraServicio::EVENTO_SERVICIO_BAJA, $ejecutor, $servicio, $data);
    }

    public function registrarMantenimiento($ejecutor, $tarea, $hist, $bitacora)
    {
        if ($tarea->getMantenimiento()->getPorHoras()) {
            $data['abr'] = 'hs';
        }
        if ($tarea->getMantenimiento()->getPorKilometros()) {
            $data['abr'] = 'kms';
        }
        $data['registro'] = $hist->data2array();
        $data['bitacora_registro'] = $bitacora;
        $this->add(BitacoraServicio::EVENTO_MANTENIMIENTO_REGISTRO, $ejecutor, $tarea->getServicio(), $data);
    }

    public function cambioOdometro($ejecutor, $servicio, $ultOdometro, $newOdometro, $ajuste)
    {
        $data = array(
            'ultOdometro' => $ultOdometro,
            'newOdometro' => $newOdometro,
            'ajuste' => $ajuste,
        );
        $this->add(BitacoraServicio::EVENTO_CAMBIO_ODOMETRO, $ejecutor, $servicio, $data);
    }

    public function cambioHorometro($ejecutor, $servicio, $ultHorometro, $newHorometro, $ajuste)
    {
        $data = array(
            'ultHorometro' => $ultHorometro,
            'newHorometro' => $newHorometro,
            'ajuste' => $ajuste,
        );
        $this->add(BitacoraServicio::EVENTO_CAMBIO_HOROMETRO, $ejecutor, $servicio, $data);
    }

    private function getDataTMant($tmant)
    {
        $mantenimiento = $tmant->getMantenimiento();
        $data = [
            'nombre' => $mantenimiento->getNombre(),
        ];

        if ($mantenimiento->getPorKilometros()) {
            $data['dato_proximo'] = $tmant->getProximoKilometros() . ' kms';
            $data['aviso'] = $tmant->getAvisoKilometros() . ' kms';
        } elseif ($mantenimiento->getPorHoras()) {
            $data['dato_proximo'] = $tmant->getProximoHoras() . ' hs';
            $data['aviso'] = $tmant->getAvisoHoras() . ' hs';
        } elseif ($mantenimiento->getPorDias()) {
            $data['dato_proximo'] = $tmant->getProximoDias()->format('d/m/Y');
            $data['aviso'] = $tmant->getAvisoDias() . ' dias';
        }

        return $data;
    }


    public function createTareaMant($ejecutor, $servicio, $tarea)
    {
        $data = $this->getDataTMant($tarea);

        $this->add(BitacoraServicio::EVENTO_SERVICIO_CREATE_TAREAMANT, $ejecutor, $servicio, $data);
    }

    public function deleteTareaMant($ejecutor, $servicio, $tarea)
    {
        $data = $this->getDataTMant($tarea);

        $this->add(BitacoraServicio::EVENTO_SERVICIO_DELETE_TAREAMANT, $ejecutor, $servicio, $data);
    }

    public function completeTareaMant($ejecutor, $servicio, $data) // funcion para registrar en la bitacora que la tarea de realizó
    {

        $this->add(BitacoraServicio::EVENTO_SERVICIO_COMPLETE_TAREAMANT, $ejecutor, $servicio, $data);
    }

    /**
     * Obtiene los ultimos 100 registros de la bitacora, ordenados en orden cronologica descendiente.
     * @param type $servicio
     * @return type Registro
     */
    public function getRegistros($servicio)
    {
        return $this->em->getRepository('App:BitacoraServicio')->findBy(array(
            'servicio' => $servicio->getId()
        ), array('created_at' => 'DESC'), 100);
    }

    public function obtenerUltByFecha($servicio, $tevento = null)
    {
        return $this->em->getRepository('App:BitacoraServicio')->getUltRegistroByFecha($servicio, $tevento);
    }
    public function obtener($organizacion, $desde, $hasta, $tevento = null)
    {
        return $this->em->getRepository('App:BitacoraServicio')->byOrganizacion($organizacion, $desde, $hasta, $tevento);
    }

    public function parse($registro)
    {
        $str = '';
        $data = $registro->getData();
        switch ($registro->getTipoEvento()) {
            case $registro::EVENTO_SERVICIO_ASIGNAR_EQUIPO:
                $str = sprintf('Se asigna el equipo %s.', $data['equipo']['mdmid']);
                break;
            case $registro::EVENTO_SERVICIO_RETIRAR_EQUIPO:
                $str = sprintf('Se retira el equipo %s por <b>%s</b>.', $data['equipo']['mdmid'], isset($data['motivo']) ? $data['motivo'] : 'motivo desconocido');
                break;
            case $registro::EVENTO_COMBUSTIBLE_CREATE_MANUAL:
                if (isset($data['carga'])) {
                    $str = sprintf('Se cargo %3.2f lts por $ %8.2f. Odometro %s kms.', $data['carga']['litros_carga'], $data['carga']['monto_total'], $data['carga']['odometro']);
                }
                break;
            case $registro::EVENTO_COMBUSTIBLE_CREATE_APP:
                if (isset($data['carga'])) {
                    $str = sprintf('Se cargo %3.2f lts por $ %8.2f. Odometro %s kms.', $data['carga']['litros_carga'], $data['carga']['monto_total'], $data['carga']['odometro']);
                }
                break;
            case $registro::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA:
                if (isset($data['carga'])) {
                    $str = sprintf('Se cargo %3.2f lts por $ %8.2f. Odometro %s kms.', $data['carga']['litros_carga'], $data['carga']['monto_total'], $data['carga']['odometro']);
                }
                break;
            case $registro::EVENTO_SERVICIO_UPDATE:
                if (isset($data['diff'])) {
                    $s = array();
                    foreach ($data['diff'] as $key => $dat) {
                        if ($this->getLabel($key)) {
                            $s[] = $this->getLabel($key) . ' de "<em>' . $this->getData($key, $data['anterior']) . '</em>" por "<b>' . $this->getData($key, $data['servicio']) . '</b>"';
                        } else {
                            if (is_array($dat)) {
                                foreach ($dat as $key => $dVehiculo) {
                                    if (isset($data['servicio']['vehiculo']) && isset($data['anterior']['vehiculo'])) {
                                        $s[] = $this->getLabel($key) . ' de "<em>' . $this->getData($key, $data['servicio']['vehiculo']) . '</em>" por "<b>' . $this->getData($key, $data['anterior']['vehiculo']) . '</b>"';
                                    }
                                }
                            }
                        }
                    }
                    if (count($s) > 0)
                        $str = 'Se cambio, ' . implode(', ', $s);
                }
                break;
            case $registro::EVENTO_COMBUSTIBLE_UPDATE:
                if (isset($data['diff'])) {
                    $s = array();
                    foreach ($data['diff'] as $key => $dat) {
                        if ($this->getLabel($key)) {
                            $s[] = $this->getLabel($key) . ': ' . $dat;
                        }
                    }
                    if (count($s) > 0) {
                        $str = 'Se cambio, ' . implode(', ', $s);
                    }
                }
                break;
            case $registro::EVENTO_COMBUSTIBLE_DELETE:
                if (isset($data['carga'])) {
                    $str = sprintf('Se eliminó carga del día %s de %3.2f lts por $ %8.2f. Odometro %s kms.', $data['carga']['fecha'], $data['carga']['litros_carga'], $data['carga']['monto_total'], $data['carga']['odometro']);
                } else {
                    $str = 'Sin datos disponibles.';
                }
                break;
            case $registro::EVENTO_SERVICIO_NEW:
                $str = sprintf('Creación servicio %s el %s (UTC) con equipo %s', $data['servicio']['nombre'], $data['servicio']['created_at'], isset($data['equipo']) ? $data['equipo']['mdmid'] : '');
                break;

            case $registro::EVENTO_SERVICIO_BAJA:
                $str = sprintf('Baja de servicio el <b>%s</b>', $data['fecha_baja']);
                break;
            case $registro::EVENTO_SERVICIO_PRG:
                foreach ($data['prg'] as $value) {
                    if (is_array($value['valor'])) {
                        foreach ($value['valor'] as $valor) {
                            $s[] = $valor['nombre'] . ': ' . $valor['valor'];
                        }
                    } else {
                        $s[] = $value['nombre'] . ': ' . $value['valor'];
                    }
                }
                if (isset($s) && count($s) > 0)
                    $str = 'Se programó, ' . implode(', ', $s);
                break;
            case $registro::EVENTO_SERVICIO_CORTEMOTOR:
                if ($data['corte'] === '1') {
                    $str = 'Se pidio <b>corte</b> de motor';
                } else {
                    $str = 'Se pidio <b>habilitación</b> de motor';
                }
                break;
            case $registro::EVENTO_SERVICIO_CERROJO:
                if ($data['accion'] === '0') {
                    $str = 'Se pidio <b>apertura</b> de cerrojo';
                } else {
                    $str = 'Se pidio <b>cierre</b> de cerrojo';
                }
                break;
            case $registro::EVENTO_SERVICIO_SENSOR_NEW:
                if (isset($data['sensor'])) {
                    $d = $data['sensor'];
                    $str = sprintf('Se creó %s (%s) Tipo: %s', $d['nombre'], isset($d['serie']) ? $d['serie'] : '---', isset($d['tipo']) ? $d['tipo'] : '---');
                }
                break;
            case $registro::EVENTO_SERVICIO_SENSOR_UPDATE;
            case $registro::EVENTO_SERVICIO_INPUT_UPDATE:
                if (isset($data['diff'])) {
                    $s = array();
                    foreach ($data['diff'] as $key => $dat) {
                        if ($this->getLabel($key)) {
                            $s[] = $this->getLabel($key) . ': ' . $dat;
                        }
                    }
                    if (count($s) > 0)
                        $str = 'Se cambio, ' . implode(', ', $s);
                }
                break;
            case $registro::EVENTO_SERVICIO_SENSOR_DELETE:
                if (isset($data['sensor'])) {
                    $d = $data['sensor'];
                    $str = sprintf('Se eliminó el sensor %s (%s) Tipo: %s', $d['nombre'], isset($d['serie']) ? $d['serie'] : '---', isset($d['tipo']) ? $d['tipo'] : '---');
                }
                break;
            case $registro::EVENTO_SERVICIO_INPUT_NEW:
                if (isset($data['input'])) {
                    $d = $data['input'];
                    $str = sprintf('Se creó %s Tipo: %s', $d['nombre'], isset($d['tipo']) ? $d['tipo'] : '---');
                }
                break;
            case $registro::EVENTO_SERVICIO_INPUT_DELETE:
                if (isset($data['input'])) {
                    $d = $data['input'];
                    $str = sprintf('Se eliminó la entrada %s Tipo: %s', $d['nombre'], isset($d['tipo']) ? $d['tipo'] : '---');
                }
                break;
            case $registro::EVENTO_MANTENIMIENTO_REGISTRO:
                if (isset($data['registro'])) {
                    $abr = isset($data['abr']) ? $data['abr'] : '';
                    $reg = $data['registro'];
                    $str = sprintf('Realizado: %s %s Próximo: %s %s Obs: %s Costo: $ %8.2f', $this->getData('datoRealizado', $reg), $abr, $this->getData('datoProximo', $reg), $abr, $this->getData('observacion', $reg), $this->getData('costo', $reg));
                    if (isset($data['bitacora_registro'])) {
                        if ($this->isCliente) {
                            $str = sprintf(' Último Estado: %s,  Ult. Odómetro: %s km, Ult. Horómetro: %s min', $this->getData('message', $data['bitacora_registro']['status']), $this->getData('odometro', $data['bitacora_registro']), $this->getData('horometro', $data['bitacora_registro']));
                        } else {
                            $str .= sprintf(' Último Estado: %s,  Ult. Odómetro: %s km, Ult. Horómetro: %s min', $this->getData('message', $data['bitacora_registro']['status']), $this->getData('odometro', $data['bitacora_registro']), $this->getData('horometro', $data['bitacora_registro']));
                        }
                    }
                }


                break;
            case $registro::EVENTO_CAMBIO_ODOMETRO:
                $ultOdometro =  $this->getData('ultOdometro', $data, 0);
                $newOdometro =  $this->getData('newHorometro', $data, 0);
                $ajuste =  $this->getData('ajuste', $data, '---');
                $str = sprintf('Ult. Registrado: %s Nuevo: %s Ajuste: %s (en kms)', $ultOdometro != null ?  number_format($ultOdometro / 1000, 2, ',', '.') : '---', $newOdometro != null ?  number_format($newOdometro / 1000, 2, ',', '.')  : '---', $ajuste != null ?  number_format($ajuste / 1000, 2, ',', '.') : '---');
                break;
            case $registro::EVENTO_CAMBIO_HOROMETRO:
                $ultHorometro =  $this->getData('ultHorometro', $data, '---');
                $newHorometro =  $this->getData('newHorometro', $data, '---');
                $ajuste =  $this->getData('ajuste', $data, '---');
                $str = sprintf('Ult. Registrado: %s Nuevo: %s Ajuste: %s (en horas)', $ultHorometro != null ? $ultHorometro : '---', $newHorometro != null ? $newHorometro : '---', $ajuste != null ? $ajuste : '---');
                break;
            case $registro::EVENTO_SERVICIO_QUERYVERSION:
                if (strtolower($data['accion']) == 'get') {
                    $str = 'Pedido de versión de firmware.';
                } elseif (strtolower($data['accion']) == 'set') {
                    $str = sprintf('Respuesta pedido versión firmware -> %s', $this->getData('version', $data));
                } else
                    $str = '';
                break;
            case $registro::EVENTO_SERVICIO_ASIGNAR_USUARIO:
                $str = sprintf('Se asigna el usuario %s.', $data['usuario']['usuario']);
                break;
            case $registro::EVENTO_SERVICIO_RETIRAR_USUARIO:
                $str = sprintf('Se retira el usuario %s.', $data['usuario']['usuario']);
                break;
            case $registro::EVENTO_SERVICIO_CREATE_TAREAMANT:
                $str = sprintf('Se crea tarea de mantenimiento para el servicio <b> %s </b>. Próximo mantenimiento <b> %s </b> y anticipo de <b> %s </b>', $data['nombre'], $data['dato_proximo'], $data['aviso']);
                break;
            case $registro::EVENTO_SERVICIO_DELETE_TAREAMANT:
                $str = sprintf('Se elimina tarea de mantenimiento para el servicio <b> %s </b>. Próximo mantenimiento <b> %s </b> y anticipo de <b> %s </b>', $data['nombre'], $data['dato_proximo'], $data['aviso']);
                break;
            case $registro::EVENTO_SERVICIO_COMPLETE_TAREAMANT:
                $str = sprintf('Se completa tarea de mantenimiento con  Observación <b> %s </b>, fecha de <b> %s </b>. Depósito <b> %s</b>. Taller <b>%s</b>', $data['observacion'], $data['fecha'], isset($data['deposito_nombre']) ? $data['deposito_nombre'] : '---', isset($data['taller_nombre']) ? $data['taller_nombre'] : '---');
                break;
            case $registro::EVENTO_EVENTO_ENABLED:               
                $data = json_decode($data, true);
                $str = sprintf('Evento <b> %s </b>, Activado', $data['evento']);
                break;
            case $registro::EVENTO_EVENTO_DISABLED:
                $data = json_decode($data, true);
                $str = sprintf('Evento <b> %s </b>, Desactivado', $data['evento']);
                break;
            case $registro::EVENTO_ORDENTRABAJO_NEW:
                $data = json_decode($data, true);
                $str = sprintf('<b> %s </b> crea orden de trabajo <b>#%s </b>, con fecha <b> %s </b>, (<i> %s </i>) ', $data['ejecutor'], $data['ordenTrabajo'],$data['fecha'], $data['descripcion']);
                break;
            case $registro::EVENTO_ORDENTRABAJO_EDIT:
                $data = json_decode($data, true);
                $str = sprintf('<b> %s </b> modificó datos de la  orden de trabajo <b>#%s </b>, con fecha <b> %s </b>, (<i> %s </i>) ', $data['ejecutor'], $data['ordenTrabajo'],$data['fecha'], $data['descripcion']);
                break;
            case $registro::EVENTO_ORDENTRABAJO_CLOSE:
                $data = json_decode($data, true);
                $str = sprintf('<b> %s </b>cierra orden de trabajo <b>#%s </b>, con fecha de cierre <b> %s </b>, fecha de cierre del sistema (<i> %s </i>) ', $data['ejecutor'], $data['ordenTrabajo'],$data['fechaCierre'], $data['fechaCierreSistema']);
                break;
            case $registro::EVENTO_ORDENTRABAJO_DELETE:
                $data = json_decode($data, true);
                $str = sprintf('<b> %s </b>elimina orden de trabajo <b>#%s </b>, con fecha <b> %s </b>, (<i> %s </i>) ', $data['ejecutor'], $data['ordenTrabajo'],$data['fecha'], $data['descripcion']);
                break;
            default:

                break;
        }
        return $str;
    }

    private function getData($key, $array, $default = null)
    {
        if ($key && $array) {
            // die('////<pre>'.nl2br(var_export($array[$key], true)).'</pre>////');
            return array_key_exists($key, $array) ? $array[$key] : $default;
        }
        return null;
    }

    private $label = array(
        'nombre' => 'Nombre',
        'dato_fiscal' => 'Id. Fiscal',
        'patente' => 'Patente',
        'odometo' => 'Odómetro',
        'marca' => 'Marca',
        'modelo' => 'Modelo',
        'anioFabricacion' => 'Año',
        'color' => 'Color',
        'litrosTanque' => 'Lts. Tanque',
        'numeroChasis' => 'Nro. Chasis',
        'numeroMotor' => 'Nro. Motor',
        'rendimiento' => 'Rendimiento',
        'abbr' => 'Abbr.',
        'valorMaximo' => 'Max.',
        'valorMinimo' => 'Min.',
        'serie' => 'Serie',
        'tipo' => 'Tipo',
        'fecha' => 'Fecha',
        'litros_carga' => 'Litros',
        'guia_despacho' => 'Guia Despacho',
        'codigo_autorizacion' => 'Cod. Autorización',
        'codigo_estacion' => 'Cod. EESS',
        'monto_total' => 'Importe',
        'modo_ingreso' => 'Modo Ingreso',
        'corte_motor' => 'Corte Motor',
        'boton_panico' => 'Pánico',
        'medidor_combustible' => 'Medidor Comb.:',
        'entradas_digitales' => 'Sensores',
        'datoProximo' => 'Próximo',
        'datoRealizado' => 'Realizado',
        'status' => 'Estado:',
        'datoUltimo' => 'Último',
        'observacion' => 'Observación',
        'ultOdometro' => 'Ult. Odometro',
        'newOdometro' => 'Nuevo Odom.',
        'ajuste' => 'Ajuste',
    );

    private function getLabel($key)
    {
        if (isset($this->label[$key])) {
            return $this->label[$key];
        } else {
            return false;
        }
    }

    private function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

    public function findByHistorico($servicio, $idMant, $evento, $tipoOrg)
    {
        $this->isCliente = $tipoOrg;
        $bitac_str = null;
        $bitacoras = $this->em->getRepository('App:BitacoraServicio')->get($servicio, $evento);
        foreach ($bitacoras as $bitacora) {
            $registro = $bitacora->getData()['registro'];
            if ($idMant == $registro['id']) {
                return $this->parse($bitacora);
            }
        }
        return null;
    }
}
