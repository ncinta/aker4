<?php

/**
 * MetricaManager
 *
 * @author Nicolas
 */

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Metrica;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UtilsManager;
use DateTimeZone;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MetricaManager
{

    protected $em;
    protected $security;
    protected $utils;
    protected $metrica;
    protected $userLoginManager;
    protected $informeUtilsManager;


    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        UtilsManager $utils,
        UserLoginManager $userLoginManager,
        InformeUtilsManager $informeUtilsManager
    ) {
        $this->security = $security;
        $this->utils = $utils;
        $this->em = $em;
        $this->userLoginManager = $userLoginManager;
        $this->informeUtilsManager = $informeUtilsManager;
    }

    public function setDataInforme($event, $tipoInforme)
    {

        $organizacion = $this->userLoginManager->getOrganizacion();

        $data = $this->buildDataInforme($tipoInforme, $event->getData());
        $tipoMetrica = Metrica::TIPO_INFORME;

        $metrica = $this->create($organizacion, $tipoMetrica, $data);
        $save = $this->save($metrica);
    }

    public function buildDataInforme($key, $data)
    {
        if (!is_null($data['desde']) && !is_null($data['hasta'])) {
            $hoy = new \DateTime();
            $desde = new \DateTime($this->utils->datetime2sqltimestamp($data['desde'], false));
            $hasta = new \DateTime($this->utils->datetime2sqltimestamp($data['hasta'], false));

            // Calcular la diferencia entre las fechas
            $diferencia = $hoy->diff($desde);
            $diferenciaConsulta = $hasta->diff($desde);
        } else {
            $diferencia = new \DateInterval('P0D');
            $diferenciaConsulta = new \DateInterval('P0D');
        }
        $arrServicios = $this->informeUtilsManager->obtenerServicios($data['servicio'], $this->userLoginManager->getUser());
        // die('////<pre>' . nl2br(var_export($arrServicios, true)) . '</pre>////');

        $arr = array(
            'tipoInforme' => $key,
            'servicio' => count($arrServicios['servicios']),
            'diasConsulta' => $diferenciaConsulta->days,
            'diasAtras' => $diferencia->days

        );

        $json = json_encode($arr);
        return $json;
    }


    public function create(Organizacion $organizacion, $tipo, $data)
    {
        $this->metrica = new Metrica();
        $this->metrica->setOrganizacion($organizacion);
        $this->metrica->setTipoMetrica($tipo);
        $this->metrica->setData($data);
        return $this->metrica;
    }

    public function save($metrica)
    {
        if (!is_null($metrica))
            $this->metrica = $metrica;

        $this->em->persist($this->metrica);
        $this->em->flush();
        return $this->metrica;
    }

    public function getMetricas($tipo, $organizacion = null, $desde = null, $hasta = null)
    {
        return $this->em->getRepository('App:Metrica')->getMetricas($tipo, $organizacion, $desde, $hasta);
    }


    public function getData($metricas)
    {
        $pieOrganizacion = [];
        $pieInformes = [];
        $dataInfoServicios = [];
        $dataInfoDias = [];

        foreach ($metricas as $metrica) {
            $datos = json_decode($metrica->getData(), true);
            $organizacion = $metrica->getOrganizacion()->getNombre();
            $tipoInforme = $datos['tipoInforme'];

            // Incrementar conteo de pieOrganizacion
            $pieOrganizacion[$organizacion] = ($pieOrganizacion[$organizacion] ?? 0) + 1;

            // Incrementar conteo de pieInformes
            $pieInformes[$tipoInforme] = ($pieInformes[$tipoInforme] ?? 0) + 1;

            // Actualizar dataInfoServicios
            if (!isset($dataInfoServicios[$tipoInforme]['servicio']) || $datos['servicio'] > $dataInfoServicios[$tipoInforme]['servicio']) {
                $dataInfoServicios[$tipoInforme]['servicio'] = $datos['servicio'];
                $dataInfoServicios[$tipoInforme]['organizacion'] = $organizacion;
                $dataInfoServicios[$tipoInforme]['fecha'] = $metrica->getCreatedAt()->format('d/m/Y H:m');
                $dataInfoServicios[$tipoInforme]['diasAtras'] = $datos['diasAtras'];
            }

            // Actualizar dataInfoDias
            if (!isset($dataInfoDias[$tipoInforme]['diasConsulta']) || $datos['diasConsulta'] > $dataInfoDias[$tipoInforme]['diasConsulta']) {
                $dataInfoDias[$tipoInforme]['diasConsulta'] = $datos['diasConsulta'];
                $dataInfoDias[$tipoInforme]['organizacion'] = $organizacion;
                $dataInfoDias[$tipoInforme]['fecha'] = $metrica->getCreatedAt()->format('d/m/Y H:m');
                $dataInfoDias[$tipoInforme]['diasAtras'] = $datos['diasAtras'];
            }
        }

        // Ordenar arrays
        arsort($pieOrganizacion);
        arsort($pieInformes);

        return [
            'pieInformes' => $pieInformes,
            'pieOrganizacion' => $pieOrganizacion,
            'dataInfoServicios' => $dataInfoServicios,
            'dataInfoDias' => $dataInfoDias,
        ];
    }

    public function getDataLastMetricas($metricas)
    {

        $data = array();

        foreach ($metricas as $metrica) {
            $datos = json_decode($metrica->getData(), true);

            $data[] = array(
                'fecha' =>  $metrica->getCreatedAt()->format('d/m/Y H:m'),
                'organizacion' =>  $metrica->getOrganizacion()->getNombre(),
                'tipoInforme' => $datos['tipoInforme'],
                'diasAtras' => $datos['diasAtras'],
                'dias' =>  $datos['diasConsulta'],
            );
        }

        return $data;
    }
}
