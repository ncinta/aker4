<?php

namespace App\Model\app;

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chofer;
use App\Entity\Notificacion;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Itinerario as Itinerario;
use App\Entity\Contacto as Contacto;
use App\Model\app\UtilsManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of 
 *
 * @author nicolas
 */
class NotificacionManager
{

    protected $em;
    protected $security;
    protected $utils;


    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, UtilsManager $utils)
    {
        $this->security = $security;
        $this->utils = $utils;
        $this->em = $em;
    }

    /**
     * Buscan un chofer por su id u objeto.
     * @param Chofer|Int $chofer   el Chofer o el id del chofer
     * @return type 
     */
    public function find($notificacion)
    {
        if ($notificacion instanceof Notificacion) {
            return $this->em->getRepository('App:Notificacion')->find($notificacion->getId());
        } else {
            return $this->em->getRepository('App:Notificacion')->find($notificacion);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Notificacion')->find($id);
    }


    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Notificacion')
            ->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function save($notificacion)
    {
        $this->em->persist($notificacion);
        $this->em->flush();
        return $notificacion;
    }

    public function delete($notificacion)
    {
        $this->em->remove($this->find($notificacion));
        $this->em->flush();
        return true;
    }
}
