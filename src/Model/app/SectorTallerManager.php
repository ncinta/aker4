<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\TallerSector;
use App\Entity\Organizacion as Organizacion;

class SectorTallerManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($sector)
    {
        $this->em->persist($sector);
        $this->em->flush();
        return $sector;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:TallerSector')->find($organizacion);
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:TallerSector')->findByOrg($organizacion);
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:TallerSector')->findOneBy(array('id' => $id * 1));
    }

    public function deleteById($id)
    {
        $sector = $this->findOne($id);
        if (!$sector) {
            throw $this->createNotFoundException('Código de ibutton no encontrado.');
        }
        return $this->delete($sector);
    }

    public function delete($sector)
    {
        $this->em->remove($sector);
        $this->em->flush();
        return true;
    }
    public function create($organizacion)
    {
        $taller = new TallerSector();
        $taller->setOrganizacion($organizacion);
        return $taller;
    }
}
