<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\ServicioMantenimiento;
use App\Entity\ServicioMantenimientoHistorico;
use App\Entity\ProductoServicioMantHist;
use App\Entity\MecanicoServicioMantHist;
use App\Entity\Mantenimiento;
use App\Entity\Servicio;
use App\Model\app\BitacoraManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;

class TareaMantManager
{

    protected $em;
    protected $utils;
    protected $userlogin;
    protected $bitacora;
    protected $bitacoraServicio;
    protected $colorRed = 'red'; //#F70202';
    protected $colorYellow = 'yellow'; //#DBCD02';
    protected $colorGreen = 'green'; //#00DE0F';

    public function __construct(
        EntityManagerInterface $em,
        UtilsManager $utils,
        UserLoginManager $userlogin,
        BitacoraManager $bitacora,
        BitacoraServicioManager $bitacoraServicio
    ) {
        $this->em = $em;
        $this->utils = $utils;
        $this->userlogin = $userlogin;
        $this->bitacora = $bitacora;
        $this->bitacoraServicio = $bitacoraServicio;
    }

    public function findByOrganizacion($organizacion = null)
    {
        return $this->em->getRepository('App:ServicioMantenimiento')->findByOrganizacion($organizacion);
    }

    public function findByServicio($servicio)
    {
        return $this->em->getRepository('App:ServicioMantenimiento')->findByServicio($servicio);
    }

    public function findHistByServicio($servicio)
    {
        return $this->em->getRepository('App:ServicioMantenimientoHistorico')->findByServicio($servicio);
    }

    public function find($tarea)
    {
        if ($tarea instanceof Mantenimiento) {
            return $this->em->getRepository('App:ServicioMantenimiento')->find($tarea->getId());
        } else {
            return $this->em->getRepository('App:ServicioMantenimiento')->find($tarea);
        }
    }

    public function findHistoricoById($tarHist)
    {
        return $this->em->getRepository('App:ServicioMantenimientoHistorico')->findOneBy(array('id' => $tarHist));
    }

    public function findHistoricoByFecha($servicio, $desde, $hasta)
    {
        return $this->em->getRepository('App:ServicioMantenimientoHistorico')->findByFecha($servicio, $desde, $hasta);
    }

    public function findHistoricoByProducto($producto)
    {
        return $this->em->getRepository('App:ProductoServicioMantHist')->findByProducto($producto);
    }

    public function create()
    {
        $mant = new ServicioMantenimiento();
        return $mant;
    }

    public function save(ServicioMantenimiento $tmant)
    {
        $this->em->persist($tmant);
        $this->em->flush();
        return $tmant;
    }

    /**
     * Para una determina tarea de mantenimiento, el sistema analiza para el 
     * servicio grabado como se encuentra la tarea con respecto a los parámetros
     * del servicio.
     * code => 0: sin analizar: 1: ok 2: en zona de aviso 3: vencida
     * @param \App\Entity\ServicioMantenimiento $tarea
     * @return array con el status de la tarea.
     */
    public function analizeMantenimiento(ServicioMantenimiento $tarea)
    {
        if (is_null($tarea->getMantenimiento())) {
            $status[] = array(
                'code' => 3,
                'color' => 'white',
                'nivel' => 'default',
                'message' => '',
            );
        } else {
            if ($tarea->getMantenimiento()->getPorKilometros()) {
                $odometro = $tarea->getServicio()->getUltOdometro();
                if (!is_null($odometro)) {
                    $odometro = round($odometro / 1000); //para hacerlo en kms. 
                    if ($odometro > $tarea->getProximoKilometros()) {
                        $status[] = array(
                            'code' => 3,
                            'message' => sprintf('%.0f kms', abs($odometro - $tarea->getProximoKilometros())),
                            'color' => $this->colorRed,
                            'nivel' => 'danger'
                        );
                    } elseif ($odometro < $tarea->getProximoKilometros() && abs($odometro - $tarea->getProximoKilometros()) < $tarea->getAvisoKilometros()) {
                        $status[] = array(
                            'code' => 2,
                            'message' => sprintf('%.0f kms', abs($odometro - $tarea->getProximoKilometros())),
                            'color' => $this->colorYellow,
                            'nivel' => 'warning'
                        );
                    } else {
                        $status[] = array(
                            'code' => 1,
                            'message' => sprintf('%.0f kms', abs($odometro - $tarea->getProximoKilometros())),
                            'color' => $this->colorGreen,
                            'nivel' => 'success'
                        );
                    }
                }
            }
            if ($tarea->getMantenimiento()->getPorDias()) {
                if (is_null($tarea->getProximoDias())) {
                    $fecha = $tarea->getCreatedAt();
                } else {
                    $fecha = $tarea->getProximoDias();
                }
                $now = new \DateTime();
                $dias = date_diff($fecha, $now);
                if ($dias->invert) {
                    if ($dias->days < $tarea->getAvisoDias()) {
                        $status[] = array(
                            'code' => 2,
                            'message' => sprintf('%d dias', $dias->days),
                            'color' => $this->colorYellow,
                            'nivel' => 'warning'
                        );
                    } else {
                        $status[] = array(
                            'code' => 1,
                            'message' => sprintf('%d dias', $dias->days),
                            'color' => $this->colorGreen,
                            'nivel' => 'success'
                        );
                    }
                } else {
                    $status[] = array(
                        'code' => 3,
                        'message' => sprintf('%d dias', $dias->days),
                        'color' => $this->colorRed,
                        'nivel' => 'danger'
                    );
                }
            }
            if ($tarea->getMantenimiento()->getPorHoras()) {
                if (is_null($tarea->getServicio()->getUltHorometro())) {
                    $horometro = 0;
                } else {
                    $horometro = round($tarea->getServicio()->getUltHorometro() / 60);
                }
                if ($horometro > $tarea->getProximoHoras()) {
                    $status[] = array(
                        'code' => 3,
                        'message' => $this->utils->minutos2tiempo(abs($horometro - $tarea->getProximoHoras()) * 60),
                        'color' => $this->colorRed,
                        'nivel' => 'danger'
                    );
                } elseif ($horometro < $tarea->getProximoHoras() && abs($horometro - $tarea->getProximoHoras()) < $tarea->getAvisoHoras()) {
                    $status[] = array(
                        'code' => 2,
                        'message' => $this->utils->minutos2tiempo(abs($horometro - $tarea->getProximoHoras()) * 60),
                        'color' => $this->colorYellow,
                        'nivel' => 'warning'
                    );
                } else {
                    $status[] = array(
                        'code' => 1,
                        'message' => $this->utils->minutos2tiempo(abs($horometro - $tarea->getProximoHoras()) * 60),
                        'color' => $this->colorGreen,
                        'nivel' => 'success'
                    );
                }
            }
        }
        $res = array('code' => 0, 'message' => '---');
        if (isset($status)) {
            foreach ($status as $key => $st) {
                if ($st['code'] > $res['code']) {
                    $res['code'] = $st['code'];
                    $res['color'] = $st['color'];
                    $res['nivel'] = $st['nivel'];
                }
                if ($key == 0) {
                    $res['message'] = $st['message'];
                } else {
                    $res['message'] = $st['message'] . ' o ' . $res['message'];
                }
            }
        }
        if ($res['code'] == 1) {
            $res['message'] = 'Ok, faltan ' . $res['message'];
        } elseif ($res['code'] == 2) {
            $res['message'] = 'Por cumplirse en ' . $res['message'];
        } elseif ($res['code'] == 3) {
            $res['message'] = 'Vencida por ' . $res['message'];
        }
        // dd($res);
        return $res;
    }

    /**
     * Para una determinada tarea de mantenimiento y datos con la siguiente 
     * estructura, este proceso graba el historial y actualiza los valores para 
     * la proxima tarea.
     * @param \App\Entity\ServicioMantenimiento $tarea
     * @param type $data array('observacion' => '','costo' => 100, 'fecha' => '01/07/2013',
     * 'dato_realizacion' => 67288, 'dato_proximo' => 72288)
     */
    public function registrarTarea(ServicioMantenimiento $tarea, $data)
    {

        $fecha = new \DateTime($this->utils->datetime2sqltimestamp(date($data['fecha']), true), null);

        $hist = new ServicioMantenimientoHistorico();
        $hist->setServicio($tarea->getServicio());
        $hist->setFecha($fecha);
        if (isset($data['deposito'])) {
            $deposito = isset($data['deposito']) ? $this->em->getRepository('App:Deposito')->find($data['deposito']) : null;
            $data['deposito_nombre'] = $deposito->getNombre();
            $hist->setDeposito($deposito);
        }
        if (isset($data['taller']) && $data['taller'] != '') {
            $taller = isset($data['taller']) ? $this->em->getRepository('App:Taller')->findOneBy(array('id' => $data['taller'])) : null;
            $data['taller_nombre'] = $taller->getNombre();
            $hist->setTaller($taller);
        }
        if ($tarea->getMantenimiento()->getPorKilometros()) {
            $hist->setDatoRealizado($data['dato_realizacion']);
            $hist->setDatoProximo($data['dato_proximo']);
            $hist->setDatoUltimo($tarea->getServicio()->getUltOdometro());   //es el odometro del servicio en el momento de la carga
            $tarea->setProximoKilometros($data['dato_proximo']);
        } elseif ($tarea->getMantenimiento()->getPorHoras()) {
            $hist->setDatoRealizado($data['dato_realizacion']);
            $hist->setDatoProximo($data['dato_proximo']);
            $hist->setDatoUltimo($tarea->getServicio()->getUltHorometro()); //es el horometro 
            $tarea->setProximoHoras($data['dato_proximo']);   //registra las horas proximas a las que tiene que hacer la alerta
        } elseif ($tarea->getMantenimiento()->getPorDias()) {
            $fechap = new \DateTime($this->utils->datetime2sqltimestamp(date($data['dato_proxima_fecha']), true), null);
            $hist->setFechaProximo($fechap);
            $tarea->setProximoDias($fechap);
        }

        $costoMeca = 0;
        if (isset($data['mecanico'])) {
            foreach ($data['mecanico'] as $id => $arr) {
                if ($arr['cantidad'] != '') {
                    $mecanico = $this->em->getRepository('App:Mecanico')->findOneBy(array('id' => $id));
                    $meca = new MecanicoServicioMantHist();
                    $meca->setMecanico($mecanico);
                    $meca->setHoras(intval($arr['cantidad']));
                    $meca->setCosto($arr['costoHora'] * $arr['cantidad']);
                    $meca->setMantenimiento($hist);
                    $this->em->persist($meca);
                    $hist->addMecanicos($meca);
                    $costoMeca += $meca->getCosto();
                }
            }
        }
        $user = $this->userlogin->getUser();
        $costoGral = $data['costo'] ? $data['costo'] : 0;
        $hist->setCosto($costoGral + $costoMeca);
        $hist->setObservacion($data['observacion']);
        $hist->setEjecutor($user);
        $hist->setServicioMantenimiento($tarea);
        $this->em->persist($hist);
        $this->em->persist($tarea);
        $this->bitacoraServicio->completeTareaMant($user, $tarea->getServicio(), $data);
        $this->em->flush();

        return $hist;
    }

    public function registrarEventual(Servicio $servicio, $data, $productos = null)
    {
        $hist = new ServicioMantenimientoHistorico();
        $fecha = new \DateTime($this->utils->datetime2sqltimestamp(date($data['fecha']), true), null);
        $hist->setFecha($fecha);
        $hist->setObservacion($data['observacion']);
        $hist->setServicio($servicio);
        if ($data['taller']) {
            $taller = $data['taller'] != '' ? $this->em->getRepository('App:Taller')->findOneBy(array('id' => $data['taller'])) : null;
            $data['taller_nombre'] = $taller->getNombre();
            $hist->setTaller($taller);
        }
        if (isset($data['mecanico'])) {
            $costoMeca = 0;
            foreach ($data['mecanico'] as $id => $arr) {
                if ($data['mecanico']['cantidad'] != '') {
                    $mecanico = $this->em->getRepository('App:Mecanico')->findOneBy(array('id' => $id));
                    $meca = new MecanicoServicioMantHist();
                    $meca->setMecanico($mecanico);
                    $meca->setHoras(intval($arr['cantidad']));
                    $meca->setCosto($arr['costoHora'] * $arr['cantidad']);
                    $meca->setMantenimiento($hist);
                    $this->em->persist($meca);
                    $hist->addMecanicos($meca);
                    $costoMeca += $meca->getCosto();
                }
            }
        }
        if ($productos) {
            foreach ($productos as $producto) {
                $productohist = new ProductoServicioMantHist();
                $productohist->setProducto($producto['producto']);
                $productohist->setMantenimiento($hist);
                $productohist->setCantidad($producto['cantidad']);
                $productohist->setDeposito($producto['deposito']);
                $this->em->persist($productohist);
                $hist->addHistoricos($productohist);
            }
        }
        $costoGral = $data['costo'] ? $data['costo'] : 0;
        $hist->setCosto($costoGral + $costoMeca);
        $user  = $this->userlogin->getUser();
        $hist->setEjecutor($user);
        $this->em->persist($hist);
        $this->bitacoraServicio->completeTareaMant($user, $tarea->getServicio(), $data);
        $this->em->flush();

        return true;
    }

    public function historico(ServicioMantenimiento $tarea)
    {
        return $this->em->getRepository('App:ServicioMantenimientoHistorico')->get($tarea);
    }

    public function saveHistorico(ServicioMantenimientoHistorico $mantH)
    {
        $this->em->persist($mantH);
        $this->em->flush();
        return $mantH;
    }

    public function delete($id)
    {
        $tarea = $this->find($id);
        foreach ($tarea->getOrdenesTrabajo() as $orden) {
            $this->em->remove($orden);
        }
        $this->em->remove($tarea);
        $this->em->flush();
        return true;
    }

    public function procesarStatusMantenimiento($servicios)
    {
        $informe = array('1' => null, '2' => null, '3' => null);
        foreach ($servicios as $servicio) {  //informe para cada servicio
            $statusmant = null;
            /* 11-06-2019 SE COMENTA ESTE IF PORQUE SINO NO SACA INFORME DE SERVICIOS SIN EQUIPO.
             * Y EL CLIENTE SEGAR UTILIZA SERVICIOS SIN EQUIPO */
            //  if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO) {
            //proceso los mantenimientos para el servicio y grabo en informe el resultado
            $mantenimientos = $this->findByServicio($servicio);
            if ($mantenimientos) {   //tengo mantenimientos para procesar.
                foreach ($mantenimientos as $tarea) {   //analizo cada mantenimiento.
                    $status = $this->analizeMantenimiento($tarea);
                    $add = true;
                    if ($status['code'] == 1) {
                        $add = isset($consulta['incluir_ok']) && $consulta['incluir_ok'];
                    }
                    if ($add) {
                        $informe[$status['code']][] = array(
                            'centroCosto' => $servicio->getCentrocosto() != null ? $servicio->getCentrocosto()->getNombre() : null,
                            'servicio' => $servicio->getNombre(),
                            'odometro' => is_null($servicio->getUltOdometro()) ? 0 : $servicio->getUltOdometro() / 1000,
                            'horometro' => $servicio->formatHorometro(),
                            'tipo_vehiculo' => $servicio->getTipoServicio()->getNombre(),
                            //'nombre' => $tarea->getMantenimiento()->getNombre(),
                            'nombre' => $tarea->getMantenimiento() != null ? $tarea->getMantenimiento()->getNombre() : '---',
                            'status' => $status,
                        );
                    }
                }
            }
        }
        //   }
        return $informe;
    }

    public function procesarStatusMantenimientoPorServicio($servicios, $consulta)
    {
        $informe = array();
        foreach ($servicios as $servicio) {  //informe para cada servicio
            $tVehiculo = null;
            $informe[$servicio->getId()] = array(
                'mantenimientos' => null,
                'servicio' => array(
                    'odometro' => is_null($servicio->getUltOdometro()) ? 0 : $servicio->getUltOdometro() / 1000,
                    'nombre' => $servicio->getNombre(),
                    'ult_fechahora' => $servicio->getUltFechahora(),
                    'horometro' => $servicio->formatHorometro(),
                    'tipo_vehiculo' => $servicio->getTipoServicio()->getNombre(),
                )
            );

            //proceso los mantenimientos para el servicio y grabo en informe el resultado
            $mantenimientos = $this->findByServicio($servicio);

            if ($mantenimientos) {   //tengo mantenimientos para procesar.
                foreach ($mantenimientos as $tarea) {   //analizo cada mantenimiento.
                    //die('////<pre>' . nl2br(var_export($consulta, true)) . '</pre>////');
                    $status = $this->analizeMantenimiento($tarea);
                    $add = true;
                    if ($status['code'] == 1) {
                        $add = isset($consulta['incluir_ok']) && $consulta['incluir_ok'];
                    }
                    if ($add) {
                        $lastTarea = $this->em->getRepository('App:ServicioMantenimientoHistorico')->last($tarea);
                        $informe[$servicio->getId()]['mantenimientos'][] = array(
                            'nombre' => $tarea->getMantenimiento() != null ? $tarea->getMantenimiento()->getNombre() : '---',
                            'id' => $tarea->getId(),
                            'status' => $status,
                            'ultimo' => is_null($lastTarea) ? null : $lastTarea->getFecha()
                        );
                    }
                }
            }
        }

        return $informe;
    }

    public function analizeMantenimientoAbbreviated(ServicioMantenimiento $tarea)
    {
        $status = array();
        if (is_null($tarea->getMantenimiento())) {
            $status['code'] = 3;
            $status['color'] = 'white';
        } else {
            if ($tarea->getMantenimiento()->getPorKilometros()) {
                $odometro = $tarea->getServicio()->getUltOdometro();
                if (!is_null($odometro)) {
                    $odometro = round($odometro / 1000); //para hacerlo en kms. 
                    if ($odometro > $tarea->getProximoKilometros()) {
                        $status[] = array(
                            'code' => 3,
                            'color' => $this->colorRed
                        );
                    } elseif ($odometro < $tarea->getProximoKilometros() && abs($odometro - $tarea->getProximoKilometros()) < $tarea->getAvisoKilometros()) {
                        $status[] = array(
                            'code' => 2,
                            'color' => $this->colorYellow
                        );
                    } else {
                        $status[] = array(
                            'code' => 1,
                            'color' => $this->colorGreen
                        );
                    }
                }
            }
        }
        return $status;
    }

    public function getProductoServicio($actividad, $servicio)
    {
        $modelo = !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getCarModelo() : null;   //tengo el modelo del vehiculo.

        $prod = null;
        if ($modelo) {
            foreach ($modelo->getFichaTecnica() as $ft) {
                if ($ft->getActividad()->getId() == $actividad->getId()) {
                    return array(
                        'producto' => $ft->getProducto(),
                        'fichaTecnica' => $ft
                    );
                }
            }
        }
        return $prod;
    }

    public function procesarIndiceMantenimiento($servicios)
    {
        $vencidas = $otras = 0;
        foreach ($servicios as $servicio) {  //informe para cada servicio
            if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO) {
                //proceso los mantenimientos para el servicio y grabo en informe el resultado
                $mantenimientos = $this->findByServicio($servicio);
                if ($mantenimientos) {   //tengo mantenimientos para procesar.
                    foreach ($mantenimientos as $tarea) {   //analizo cada mantenimiento.
                        $status = $this->analizeMantenimiento($tarea);
                        if ($status['code'] == 3) {
                            $vencidas++;
                        } else {
                            $otras++;
                        }
                    }
                }
            }
        }
        if ($vencidas + $otras > 0) {
            return array('v' => $vencidas, 't' => $vencidas + $otras);
        } else {
            return null;
        }
    }

    public function getCountManteNuevos($servicios)
    {
        $fecha = date("Y-m-d", strtotime('-1 days'));
        $ids = array();
        foreach ($servicios as $servicio) {  //informe para cada servicio
            if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO) {
                $ids[] = $servicio->getId();
            }
        }

        $query = $this->em->createQueryBuilder()
            ->select('count(i.id)')
            ->from('App:ServicioMantenimientoHistorico', 'i')
            ->where('i.servicio in (:ids)')
            ->andWhere('i.fecha = :fecha')
            ->setParameter('ids', $servicios)
            ->setParameter('fecha', $fecha);
        return $query->getQuery()->getSingleScalarResult();
    }

    public function filtrar($servicios, $estado = null)
    {
        $arr_tareas = array();
        foreach ($servicios as $servicio) {
            $tmant = $this->findByServicio($servicio);
            $data = $tmant ? $this->data($tmant, $estado) : null;

            if ($estado == 0) {
                $arr_tareas[] = array(
                    'servicio' => $servicio,
                    'tareas' => $data,
                );
            } else {
                if ($data != null) {
                    $arr_tareas[] = array(
                        'servicio' => $servicio,
                        'tareas' => $data,
                    );
                }
            }
        }

        return $arr_tareas;
    }

    public function data($tmant, $estado)
    {
        $d = null;
        foreach ($tmant as $tarea) {
            $status = $this->analizeMantenimiento($tarea);
            if (($estado > 0)) {
                if ($status['code'] == $estado) {
                    if (!is_null($tarea)) {
                        $d[] = array(
                            'plan' => $tarea,
                            'status' => $status,
                        );
                    }
                }
            } else {
                $d[] = array(
                    'plan' => $tarea,
                    'status' => $status,
                );
            }
        }
        return $d;
    }

    public function getLastHistorico($tarea)
    {
        return $this->em->getRepository('App:ServicioMantenimientoHistorico')->last($tarea);
    }
}
