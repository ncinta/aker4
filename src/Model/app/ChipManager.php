<?php

/**
 * ChipManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chip;
use App\Entity\Organizacion as Organizacion;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraEquipoManager;

class ChipManager
{

    protected $em;
    protected $security;
    protected $bitacoraEquipo;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, BitacoraEquipoManager $bitacoraEquipo, UserLoginManager $userlogin)
    {
        $this->security = $security;
        $this->em = $em;
        $this->bitacoraEquipo = $bitacoraEquipo;
        $this->userlogin = $userlogin;
    }

    /**
     * Buscan un chip por su id u objeto.
     * @param Chip|Int $chip   el Chip o el id del chip
     * @return type 
     */
    public function find($chip)
    {
        if ($chip instanceof Chip) {
            return $this->em->getRepository('App:Chip')->find($chip->getId());
        } else {
            return $this->em->getRepository('App:Chip')->find($chip);
        }
    }

    public function findByImei($imei)
    {
        return $this->em->getRepository('App:Chip')
            ->findOneBy(array('imei' => $imei));
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Chip')->find($id);
    }

    public function findAll(Organizacion $organizacion)
    {
        return $this->em->getRepository('App:Chip')
            ->findAllChips($organizacion);
    }

    public function findMisChips()
    {
        $user = $this->security->getToken()->getUser();
        return $this->em->getRepository('App:Chip')
            ->findAllChips($user->getOrganizacion());
    }

    public function createChip(Organizacion $propietario)
    {
        if ($propietario) {
            $chip = new Chip();
            $chip->setPropietario($propietario);
            $chip->setOrganizacion($propietario);
            $chip->setEstado(1);                    //el chip esta disponible

            return $chip;
        } else {
            return null;
        }
    }

    public function save(Chip $chip)
    {
        $id = $chip->getId();
        if ($id == null) {   //estoy en una insercion-
            if ($this->findByImei($chip->getImei())) {   //no debe haber otro imei igual.
                return false;   //entidad ya existente.
            }
        }
        $this->em->persist($chip);
        $this->em->flush();
        return $chip;
    }

    /**
     * Tranfiere un chip a un nuevo deposito.
     * @param Chip $chip
     * @param Organizacion $newOrg
     * @param integer $tipoTranf 
     */
    public function tranferir(Chip $chip, Organizacion $newOrg, $tipoTranf = null)
    {
        $chip->setOrganizacion($newOrg);
        if ($tipoTranf == 2) {              //venta
            $chip->setPropietario($newOrg);
        }

        return $this->save($chip);
    }

    public function delete(Chip $chip)
    {
        $this->em->remove($chip);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $chip = $this->findById($id);
        if (!$chip) {
            throw $this->createNotFoundException('Código de chip no encontrado.');
        }
        return $this->delete($chip);
    }

    /**
     * Retira un chip de donde se encuentra instalado.
     * @param type $chip
     * @return type 
     */
    public function retirar($chip)
    {
        $chip = $this->find($chip);
        if ($chip) {
            $this->bitacoraEquipo->retirarChip($this->userlogin->getUser(), $chip, $chip->getEquipo());
            $chip->setEquipo(null);
            $chip->setEstado($chip::ESTADO_DISPONIBLE);
            $this->em->persist($chip);
            $this->em->flush();

            return true;
        } else {
            return false;
        }
    }
}
