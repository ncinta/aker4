<?php

/**
 * MecanicoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Mecanico;
use App\Entity\Organizacion as Organizacion;

class MecanicoManager
{

    protected $em;
    protected $security;
    protected $mecanico;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security)
    {
        $this->security = $security;
        $this->em = $em;
    }

    /**
     * Buscan un mecanico por su id u objeto.
     * @param Mecanico|Int $mecanico   el Mecanico o el id del mecanico
     * @return type 
     */
    public function find($mecanico)
    {
        if ($mecanico instanceof Mecanico) {
            return $this->em->getRepository('App:Mecanico')->find($mecanico->getId());
        } else {
            return $this->em->getRepository('App:Mecanico')->find($mecanico);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Mecanico')->findOneBy(array('id' => $id));
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Mecanico')
            ->findBy(array('organizacion' => $organizacion->getId()));
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Mecanico', 'p')
            ->join('p.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        
        return $query->getQuery()->getResult();
    }

    public function create(Organizacion $propietario)
    {
        $this->mecanico = new Mecanico();
        $this->mecanico->setOrganizacion($propietario);
        return $this->mecanico;
    }

    public function save($mecanico)
    {
        if (!is_null($mecanico)) $this->mecanico = $mecanico;
        $this->em->persist($this->mecanico);
        $this->em->flush();
        return $this->mecanico;
    }

    public function delete($mecanico)
    {
        $this->em->remove($this->find($mecanico));
        $this->em->flush();
        return true;
    }
}
