<?php

/**
 * Libreria de utilidades generales
 * @autor claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;

class InformeUtilsManager
{

    protected $servicio;
    protected $grpservicio;
    protected $utils;
    protected $userlogin;
    protected $organizacion;

    public function __construct(
        ServicioManager $servicio,
        GrupoServicioManager $grpservicio,
        UtilsManager $utils,
        UserLoginManager $userlogin,
        OrganizacionManager $organizacion
    ) {
        $this->organizacion = $organizacion;
        $this->userlogin = $userlogin;
        $this->servicio = $servicio;
        $this->grpservicio = $grpservicio;
        $this->utils = $utils;
    }

    public function armarPeriodo($desde, $hasta, $destino, $desglosar)
    {
        if ($destino == '2') { //salida para graficos
            $periodo = array();
            $periodo[] = array('desde' => $desde, 'hasta' => $hasta);
        } else {
            if ($desglosar) {
                $periodo = $this->utils->periodo2array($desde, $hasta);
            } else {
                $periodo = array();
                $periodo[] = array('desde' => $desde, 'hasta' => $hasta);
            }
        }
        return $periodo;
    }

    public function obtenerServicios($servicios_pedido, $user = null)
    {      
        if ($servicios_pedido == '0' || $servicios_pedido == null) {  //informe para toda la flota
            if (is_null($user)) {
                $servicios = $this->servicio->findAllByUsuario();
            } else {
                $servicios = $this->servicio->findAllByUsuario($user);
            }
            $consulta = array(
                'servicionombre' => 'toda la flota',
                'servicios' => $servicios
            );
        } elseif (substr($servicios_pedido, 0, 1) === 'g') {   //es un grupo de servicios
            $grupo = $this->grpservicio->find(substr($servicios_pedido, 1) * 1);
            $consulta = array(
                'servicionombre' => 'grupo "' . $grupo->getNombre() . '"',
                'servicios' => $this->servicio->findSoloAsignadosGrupo($grupo)
            );
        } else {    //este reporte es para un solo vehiculo, lo muestra desglosado.            
            $servicio = $this->servicio->find($servicios_pedido * 1);
            $consulta = array(
                'servicionombre' => $servicio->getNombre(),
                'servicios' => array($servicio)
            );
        }
        return $consulta;
    }

    public function getUser2Organizacion($id)
    {
        $this->user = $this->userlogin->getUser();
        $organizacion = $this->organizacion->find($id);
        if ($organizacion != $this->userlogin->getOrganizacion()) {
            $this->user = $organizacion->getUsuarioMaster();
        }
        return $this->user;
    }
}
