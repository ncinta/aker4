<?php


namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evento as Evento;
use App\Entity\EventoTemporal;
use App\Entity\EventoHistorial;
use App\Entity\Servicio;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UsuarioManager;

class EventoTemporalManager
{

    protected $em;
    protected $security;
    protected $userlogin;
    protected $usuarioManager;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        UserLoginManager $userlogin,
        usuarioManager $usuarioManager
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->usuarioManager = $usuarioManager;
    }

    private function setTimeZoneforAll($eventos)
    {
        if ($eventos) {
            //convierto la hora a la del usuario.
            foreach ($eventos as $evento) {
                $this->setTimeZone($evento);
            }
        }
        return $eventos;
    }

    public function setTimeZone($eventoH)
    {
        if (!is_null($eventoH)) {
            if ($eventoH->getFecha()) {
                $eventoH->getFecha()->setTimezone(new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            }
        }
        return $eventoH;
    }

    public function listAll($organizacion, $desde = null, $hasta = null, $evento = null, $servicio = null)
    {
        if (!is_null($desde)) {
            $desde = new \DateTime($desde, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $desde->setTimezone(new \DateTimeZone('UTC'));
        }
        if (!is_null($hasta)) {
            $hasta = new \DateTime($hasta, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $hasta->setTimezone(new \DateTimeZone('UTC'));
        }

        $eventos = $this->em->getRepository('App:EventoTemporal')->obtener($organizacion, $desde->format('Y-m-d H:i:s'), $hasta->format('Y-m-d H:i:s'), $evento, $servicio);
        return $this->setTimeZoneforAll($eventos);
    }

    public function findById($id)
    {
        $evento = $this->em->getRepository('App:EventoTemporal')->find($id);
        return $this->setTimeZone($evento);
    }

    /** Agrega la notificacion para que aparezca el evento en la pantalla */
    public function add2evento($fecha, $titulo, $evento, $servicio, $historial)
    {
        $temporal = false;
        $usuarios  = array();
        if ($servicio) {
            
            //cuando el servicio esta asociado a usuarios
            foreach ($servicio->getUsuarios() as $usuario) {
                $usuarios[$usuario->getId()] = $usuario;
            }

            //siempre para el usuario master  TODO: ver si es importante. el master consulta el historico, no el tmp ??
            $usuarios[$servicio->getOrganizacion()->getUsuarioMaster()->getId()] = $servicio->getOrganizacion()->getUsuarioMaster();

            foreach ($usuarios as $usr) {
                $temporal =  $this->addTemporalToUser($usr, $fecha, $titulo, $servicio, $evento, $historial);
            }
        } else {
            return false;
        }


        return $temporal;
    }
    /**
     *  Agrega la notificacion para que aparezca el evento en la pantalla solo para cuando es itinerario
     *  $itinerario = el itinearios asociado al evento que se disparo.
     *  $historial = es el eventoitinerariohistorial grabado.
     */
    public function add2Itinerario($fecha, $titulo, $servicio, $itinerario, $historial, $evento, $eventoHistorial)
    {
        $temporal = false;
        $usuarios  = array();
        if ($servicio) {
            //cuando el servicio esta asociado a usuarios
            foreach ($servicio->getUsuarios() as $usuario) {
                $usuarios[$usuario->getId()] = $usuario;
            }

            //siempre para el usuario master  TODO: ver si es importante. el master consulta el historico, no el tmp ??
            $usuarios[$servicio->getOrganizacion()->getUsuarioMaster()->getId()] = $servicio->getOrganizacion()->getUsuarioMaster();

            //cuando el servicio esta en itinerarios
            foreach ($itinerario->getUsuarios() as $usuario) {
                if ($this->usuarioManager->isGranted($usuario, 'ROLE_MONITOR_OPERADOR')) {
                    $usuarios[$usuario->getId()] = $usuario;
                }
            }

            //grabo las notific para los usuarios cargados.
            foreach ($usuarios as $usr) {
                $this->addTemporalToUser($usr, $fecha, $titulo, $servicio, $evento, $eventoHistorial, $itinerario, $historial);
            }
        } else {
            return false;
        }


        return $temporal;
    }

    private function addTemporalToUser($usuario, $fecha, $titulo, $servicio, $evento = null, $historialEvento = null, $itinerario = null, $historialItinerario = null)
    {
        $temporal = new EventoTemporal();

        $temporal->setUsuario($usuario);
        $temporal->setFecha($fecha);
        $temporal->setTitulo($titulo);
        $temporal->setServicio($servicio);
        $temporal->setOrganizacion($servicio->getOrganizacion());
        $temporal->setClase($evento->getClase());

        //para cuando es un evento
        if (!is_null($evento)) {
            $temporal->setEvento($evento);
            if ($historialEvento) {
                $temporal->setData($historialEvento->getData());   //se puede sacar.
                $temporal->setEventoHistorial($historialEvento);
            }
        }

        //para cuando es un itinerario
        if (!is_null($itinerario)) {
            $temporal->setItinerario($itinerario);
            if ($historialItinerario) {
                $temporal->setData($historialItinerario->getData());   //se puede sacar.
                $temporal->setHistoricoItinerario($historialItinerario);
            }
        }

        $this->em->persist($temporal);
        $this->em->flush();

        return $temporal;
    }


    public function delete(EventoTemporal $evento)
    {
        $this->em->remove($evento);
        $this->em->flush();
        return true;
    }

    public function deleteAll(EventoHistorial $eventoHistorial)
    {
        $qb = $this->em->createQueryBuilder()
            ->delete()
            ->from(EventoTemporal::class, 'et')
            ->where('et.eventoHistorial = :id')
            ->setParameter('id', $eventoHistorial->getId())
            ->getQuery()
            ->execute();
        return true;
    }

    /**
     * trae los eventos que hay en eventotemporal. si se define un limit trae solamente esos ultimos
     * si paso -1 trae todos
     */
    public function findByUser($limit = 20, $orderBy = null)
    {
        $lastLogin = $this->userlogin->getUser();
        // dd($lastLogin->getId());
        //fijo la consulta a maximo 20... para no traer cosas al pedo
        if ($orderBy == null) {
            $orderBy = ['t.fecha' => 'DESC'];
        }
        return $this->em->getRepository('App:EventoTemporal')->findByUser($lastLogin, $limit, $orderBy);
    }

    public function contarByUser()
    {
        $query = $this->em->createQueryBuilder()
            ->select('count(t.id)')
            ->from('App:EventoTemporal', 't')
            ->where('t.usuario = ?1 ')            
            ->setParameter(1, $this->userlogin->getUser()->getId());
        return $query->getQuery()->getSingleScalarResult();
    }

    public function findByUserHist($usuario, $historico)
    {
        return $this->em->getRepository('App:EventoTemporal')->findByUserHist($usuario, $historico);
    }

    public function contarNuevos($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser();
        return $this->em->getRepository('App:EventoTemporal')->contarSinAtencion($servicios, $minNotif, $lastLogin);
    }

    public function contarTodos($servicios, $minNotif)
    {
        return $this->em->getRepository('App:EventoTemporal')->contarSinAtencion($servicios, $minNotif, null);
    }

    public function contarAnterioresSinAtencion($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser();
        return $this->em->getRepository('App:EventoTemporal')->contarAnterioresSinAtencion($servicios, $minNotif, $lastLogin);
    }

    /**
     * Obtiene el ultimo evento no registrado nuevo desde el ultimo login.
     * @param type $servicios
     */
    public function findLast($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser()->getLastLogin();
        return $this->em->getRepository('App:EventoTemporal')->obtenerUltimo($servicios, $minNotif, $lastLogin);
    }

    public function findByServicio($id, $cantidad)
    {
        $eventos = $this->em->getRepository('App:EventoTemporal')->findByServicio($id, $cantidad);
        return $eventos;
    }

    public function findNuevos($ultimoId)
    {
        $user = $this->userlogin->getUser();
        //dd($lastLogin);
        return $this->em->getRepository('App:EventoTemporal')->findNuevos($user, $ultimoId);
    }

    public function findByItinerarios($itinerarios = null) {
        
        $usuario = $this->userlogin->getUser();
        $query = $this->em->createQueryBuilder()
            ->select('t')
            ->from('App:EventoTemporal', 't')
            ->where('t.usuario = ?1 ')
            ->andWhere('t.clase = 3')   //eventos de itinerario
            ->addOrderBy('t.fecha', 'DESC')
            ->setParameter(1, $usuario->getId()
        );

        if (!is_null($itinerarios)) {
            $ids = [];
            foreach ($itinerarios as $iter) {
                $ids[] = $iter->getId();
            }
            $query->andWhere('t.itinerario in (?2)')->setParameter(2, $ids);            
        }
        return $query->getQuery()->getResult();
    }
}
