<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

class ApmonPrgManager
{

    const CARFINDER_SALIDA_OFF = 0x00; // Apagar la salida
    const CARFINDER_SALIDA_ON = 0x11; // Encender la salida
    const CARFINDER_SALIDA_SKIP = 0xFF; // Dejar la salida como está
    const CARBOLT_SALIDA_ON = 0x22; // Encender salida cerrojo

    var $id, $firmware;
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function initConfig($servicio)
    {
        list($tmp, $id, $puerto) = explode(":", $servicio->getEquipo()->getMdmid());
        $this->id = $id;
        $this->firmware = 198;

        if (!is_null($servicio->getUltTrama())) {
            $trama = json_decode($servicio->getUltTrama(), true);
            if (isset($trama['carfinder']['version_firmware'])) {
                $this->firmware = substr($trama['carfinder']['version_firmware'], 0, 3);
                //die('////<pre>'.nl2br(var_export( $this->firmware, true)).'</pre>////');
                return true;
            } else {
                return false;
            }
        } else {
            $this->firmware = 198; //asumo que tiene la ultima version
            return false;
        }
    }

    private function checksum($data)
    {
        $suma = 0;
        for ($i = 0; $i < strlen($data); $i++) {
            $suma += ord(substr($data, $i, 1));
        }
        return $suma & 0xFF;
    }

    private function trama_programacion($offset, $datos)
    {
        $trama = "$" . chr(0xFE);
        if ($this->firmware < 157) {
            $trama .= chr($offset);
        } else {
            $trama .= chr($offset >> 8) . chr($offset & 0xFF);
        }
        $trama .= chr(strlen($datos)) . $datos;
        $trama .= chr($this->checksum($trama)) . "&";
        if ($this->firmware < 188) {
            return '@' . base64_encode($trama);
        } else {
            return base64_encode($trama);
        }
    }

    private function trama_borrado($offset, $bytes)
    {
        $trama = "$" . chr(0xFB) .
            chr($offset >> 8) . chr($offset & 0xFF) .
            chr($bytes >> 8) . chr($bytes & 0xFF);
        $trama .= chr($this->checksum($trama)) . "&";
        if ($this->firmware < 188) {
            return '@' . base64_encode($trama);
        } else {
            return base64_encode($trama);
        }
    }

    // Usar CARFINDER_SALIDA_xxx
    private function trama_telecomando($salida1, $salida2, $salida3)
    {
        $trama = "$" . chr(0xF1) . chr($this->id >> 8) . chr($this->id & 0xFF) . chr($salida1) . chr($salida2) . chr($salida3);
        $trama .= chr($this->checksum($trama)) . "&";
        if ($this->firmware < 188) {
            return '@' . base64_encode($trama);
        } else {
            return base64_encode($trama);
        }
    }

    public function getProgramacion($parametro, $value)
    {
        switch ($parametro->getVariable()->getCodename()) {
            case 'distancia':
                return $this->Distancia($value);
                break;
            case 'tiempo':
                return $this->TiempoEnMovimiento($value);
                break;
            case 'tiempoReposo':
                return $this->TiempoEnReposo($value);
                break;
            case 'odometro':
                return $this->Odometro($value);    //en kms.
                break;
            case 'horometro':
                return $this->Horometro($value);    //en hs
                break;
            default:
                break;
        }
    }

    // distancia en metros
    private function Distancia($distancia)
    {
        return $this->trama_programacion(
            0x14,
            chr($distancia >> 8) . chr($distancia & 0xFF)
        );
    }

    // tiempo en minutos
    function TiempoEnMovimiento($tiempo)
    {
        return $this->trama_programacion(
            0x13,
            chr($tiempo)
        );
    }

    // tiempo en minutos
    function TiempoEnReposo($tiempo)
    {
        return $this->trama_programacion(
            0x1C,
            chr($tiempo >> 8) . chr($tiempo & 0xFF)
        );
    }

    // valor en kilómetros
    function Odometro($km)
    {
        $m = floor($km * 1000.0);
        if ($this->firmware < 157) {
            $m %= 10000000; // si va a quedar mal programado, por lo menos que tenga un valor razonable
            return $this->trama_programacion(
                0x18,
                chr(($m >> 16) & 0xFF) . chr(($m >> 8) & 0xFF) . chr(($m) & 0xFF) . chr(0)
            );
        } else {
            return $this->trama_programacion(
                0x18,
                chr(($m >> 24) & 0xFF) . chr(($m >> 16) & 0xFF) . chr(($m >> 8) & 0xFF) . chr(($m) & 0xFF)
            );
        }
    }

    // valor en horas
    private function Horometro($horas)
    {
        if ($this->firmware < 192) {
            die("Este equipo no soporta horometro."); // mejorar e integrar, devolviendo false, etc...
        } else {
            return $this->trama_programacion(
                0xF9,
                chr($horas >> 8) . chr($horas & 0xFF) . // horas en contacto
                    chr(0) . chr(0)    // horas detenido
            );
        }
    }

    private function encode_ll($ll)
    {
        $s = $ll < 0.0;
        $ll = abs($ll);
        $ll_g = floor($ll);
        $ll = ($ll - $ll_g) * 60.0;
        $ll_m = floor($ll);
        $ll_mm = ($ll - $ll_m) * 1000.0;
        return chr($ll_g) . chr($ll_m + ($s ? 0x00 : 0x80)) . chr($ll_mm >> 8) . chr($ll_mm & 0xFF);
    }

    private function MaximoGeocercas()
    {
        return 90;
    }

    public function BorrarGeocercas()
    {
        return $this->trama_borrado(0x400, 11 * 90);
    }

    public function Geocerca($indice, $latitud, $longitud, $radio, $velocidad_maxima = false, $corte_por_ralenti = false)
    {
        if ($indice < 0 || $indice >= $this->MaximoGeocercas())
            return;
        $tramas = array();
        $tramas[] = $this->trama_programacion(
            0x400 + 11 * $indice,
            $this->encode_ll($latitud) .
                $this->encode_ll($longitud) .
                chr($radio >> 8) .
                chr($radio & 0xFF) .
                chr($velocidad_maxima ? ($velocidad_maxima & 0xFF) : 0x00)
        );
        if ($this->firmware >= 194) {
            if (!$corte_por_ralenti)
                $corte_por_ralenti = 0;
            $tramas[] = $this->trama_programacion(
                0x1C0 + $indice,
                chr($corte_por_ralenti & 0xFF)
            );
        }
        return $tramas;
    }

    private function CortarMotor()
    {
        return $this->trama_telecomando(self::CARFINDER_SALIDA_OFF, self::CARFINDER_SALIDA_SKIP, self::CARFINDER_SALIDA_SKIP);
    }

    private function LiberarMotor()
    {
        return $this->trama_telecomando(self::CARFINDER_SALIDA_ON, self::CARFINDER_SALIDA_SKIP, self::CARFINDER_SALIDA_SKIP);
    }

    public function getCorteMotor($cortar)
    {
        if ($cortar == '1') {
            return $this->CortarMotor();
        } else {
            return $this->LiberarMotor();
        }
    }

    public function getAccionarCerrojo($cortar)
    {
        if ($cortar == '1') {
            return $this->trama_telecomando(self::CARFINDER_SALIDA_SKIP, self::CARBOLT_SALIDA_ON, self::CARFINDER_SALIDA_SKIP);
        } else {
            return $this->trama_telecomando(self::CARBOLT_SALIDA_ON, self::CARFINDER_SALIDA_SKIP, self::CARFINDER_SALIDA_SKIP);
        }
    }
}
