<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Servicio as Servicio;
use App\Entity\GrupoServicio as GrupoServicio;
use App\Entity\Usuario as Usuario;
use App\Model\app\UserLoginManager;

class GrupoServicioManager
{

    protected $em;
    protected $userlogin;
    private $grupo;



    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin)
    {
        $this->userlogin = $userlogin;
        $this->em = $em;
    }

    public function find($grupo)
    {
        if ($grupo instanceof GrupoServicio) {
            $this->grupo = $this->em->find('App:GrupoServicio', $grupo->getId());
        } else {
            $this->grupo = $this->em->find('App:GrupoServicio', $grupo);
        }
        return $this->grupo;
    }

    public function findAllByOrganizacion(Organizacion $organizacion = null, array $orderBy = null)
    {
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion(); //busco la organizacion del usuario logueado.
        }
        return $this->em->getRepository('App:GrupoServicio')
            ->findAllGruposServicios($organizacion);
    }

    /**
     * Trae todos los grupos de servicios de una organizacion. Si no se pasa 
     * organizacion se toma la organizacion del usuario logueado.
     * @param type $organizacion
     * @return type
     */
    public function findAsociadas($organizacion = null)
    {
        $usuario = $this->userlogin->getUser();

        if ($usuario->isMaster()) {
            $grupos = $this->em->getRepository('App:GrupoServicio')->findAllGruposServicios($usuario->getOrganizacion());
        } else {
            $grupos = $this->em->getRepository('App:GrupoServicio')->findByUsuario($usuario);
            if (count($grupos) == 0) {
                $grupos = $this->em->getRepository('App:GrupoServicio')->findAllGruposServicios($usuario->getOrganizacion());
                //   die('////<pre>' . nl2br(var_export('$lastupdate', true)) . '</pre>////');
            }
            //TODO: Esto debe implementarse.
            //$grupos = $this->em->getRepository('App:GrupoServicio')->findAllByUsuario($usuario);
        }
        return $grupos;
    }

    public function findAllSinAsignar2Usuario(Usuario $usuario)
    {
        $asignados = $this->findAllByUsuario($usuario);   //obtengo los del usuario
        if ($asignados) {  //tiene asignados, devuelvo los que no estan asignados...
            $idsSelect = null;
            foreach ($asignados as $servicio) {
                $idsSelect[] = $servicio->getId();
            }
            return $this->em->getRepository('App:GrupoServicio')
                ->findGruposSinAsignar2Usuario($usuario, $idsSelect);
        } else {  //no tiene asignados... devuelvo todo el resto para la organiacion
            return $this->findAllByOrganizacion($usuario->getOrganizacion());
        }
    }

    public function create($organizacion = null)
    {
        if (is_null($organizacion)) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        $this->grupo = new GrupoServicio();
        $this->grupo->setOrganizacion($organizacion);
        $organizacion->addGrupoServicio($this->grupo);

        return $this->grupo;
    }

    public function save(GrupoServicio $grupo = null)
    {
        if (is_null($grupo)) {
            $grupo = $this->grupo;
        }
        //        die('////<pre>'.nl2br(var_export(count($grupo->getServicios()), true)).'</pre>////');
        $this->em->persist($grupo);
        $this->em->flush();
        return $grupo;
    }

    public function delete($grupo)
    {
        if (!($grupo instanceof GrupoServicio)) {
            $grupo = $this->find($grupo);
        }
        $this->em->remove($grupo);
        $this->em->flush();
        $this->grupo = null;
    }

    public function addServicio($servicio)
    {
        if ($servicio instanceof Servicio) {
            $servicio = $this->em->find('App:Servicio', $servicio->getId());
        } else {
            $servicio = $this->em->find('App:Servicio', $servicio);
        }
        $servicio->addGrupoServicio($this->grupo);
        $this->grupo->addServicio($servicio);
        $this->save();

        return true;
    }

    public function removeServicio($servicio)
    {
        if ($servicio instanceof Servicio) {
            $servicio = $this->em->find('App:Servicio', $servicio->getId());
        } else {
            $servicio = $this->em->find('App:Servicio', $servicio);
        }
        $servicio->removeGrupoServicios($this->grupo);
        $this->grupo->removeServicio($servicio);
        $this->save();

        return true;
    }

    public function remover($grupo = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($grupo instanceof GrupoServicio)) {
            $grupo = $this->find($grupo);
        }

        $organizacion->removeGrupoServicio($grupo);
        $this->em->persist($organizacion);
        $this->em->flush();
        return true;
    }

    public function findAsociadasByUser($usuario)
    {
        $grupos = $this->em->getRepository('App:GrupoServicio')->findAllGruposServicios($usuario->getOrganizacion());
        return $grupos;
    }
}
