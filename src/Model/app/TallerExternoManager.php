<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\TallerExterno;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of TallerExternoManager
 *
 * @author nicolas
 */
class TallerExternoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($taller)
    {
        $this->em->persist($taller);
        $this->em->flush();
        return $taller;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:TallerExterno')->findByOrg($organizacion);
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:TallerExterno')->findOneBy(array('id' => intval($id)));
    }

    public function deleteById($id)
    {
        $taller = $this->findOne($id);
        if (!$taller) {
            throw $this->createNotFoundException('Código de taller no encontrado.');
        }
        return $this->delete($taller);
    }

    public function delete($taller)
    {
        $this->em->remove($taller);
        $this->em->flush();
        return true;
    }

    public function create($organizacion)
    {
        $taller = new TallerExterno();
        $taller->setOrganizacion($organizacion);
        return $taller;
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:TallerExterno', 'p')
            ->join('p.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        return $query->getQuery()->getResult();
    }
}
