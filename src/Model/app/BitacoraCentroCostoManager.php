<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BitacoraCentroCosto as BitacoraCentroCosto;
use App\Model\app\UtilsManager;

/**
 * Description of BitacoraCentroCostoManager
 *
 * @author nicolas
 */
class BitacoraCentroCostoManager
{

    protected $em;
    protected $utils;
    protected $isCliente = 0;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function getTipoEventos()
    {
        $bitacora = new BitacoraCentroCosto();
        return $bitacora->getArrayTipoEvento();
    }

    private function add($tipo_evento, $ejecutor, $centrocosto, $data = null)
    {
        $bitacora = new BitacoraCentroCosto();
        $bitacora->setTipoEvento($tipo_evento);
        $bitacora->setCentrocosto($centrocosto);

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->find($ejecutor));
            }
        }
        $bitacora->setOrganizacion($centrocosto != null ? $centrocosto->getOrganizacion() : $ejecutor->getOrganizacion());

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un centrocosto.
     * @param type $ejecutor
     * @param type $centrocosto
     * @param type $equipo
     */
    public function asignarServicio($ejecutor, $centrocosto, $servicio, $desde, $horasUso)
    {
        $data = array(
            'servicio' => array(
                'id' => $servicio->getId(),
                'nombre' => $servicio->getNombre(),
                'desde' => $desde,
                'horasUso' => $horasUso,
            ),
        );
        $this->add(BitacoraCentroCosto::EVENTO_CENTROCOSTO_ASIGNAR_SERVICIO, $ejecutor, $centrocosto, $data);
    }

    /**
     * Se ejecuta cuando se retira un equipo del servicio seleccionado
     * @param type $ejecutor
     * @param type $servicio
     * @param type $equipo
     * @param type $data
     */
    public function retirarServicio($ejecutor, $centrocosto, $servicio, $motivo, $finalizacion = null)
    {
        $data['servicio'] = array(
            'id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'motivo' => $motivo,
            'finalizacion' => $finalizacion,
        );
        $this->add(BitacoraCentroCosto::EVENTO_CENTROCOSTO_RETIRAR_SERVICIO, $ejecutor, $centrocosto, $data);
    }

    public function getRegistros($centrocosto)
    {
        return $this->em->getRepository('App:BitacoraCentroCosto')->findBy(array(
            'centrocosto' => $centrocosto->getId()
        ), array('created_at' => 'DESC'));
    }

    public function obtener($organizacion, $desde, $hasta, $tevento = null)
    {
        return $this->em->getRepository('App:BitacoraCentroCosto')->byOrganizacion($organizacion, $desde, $hasta, $tevento);
    }

    public function parse($registro)
    {
        $str = '';
        $data = $registro->getData();
        switch ($registro->getTipoEvento()) {
            case $registro::EVENTO_CENTROCOSTO_ASIGNAR_SERVICIO:
                $str = sprintf('Se asigna el servicio <b>%s</b>. Desde <b>%s</b>. Con <b>%s</b> horas de uso', $data['servicio']['nombre'], $data['servicio']['desde'], $data['servicio']['horasUso']);
                break;
            case $registro::EVENTO_CENTROCOSTO_RETIRAR_SERVICIO:
                $str = sprintf('Se retira el servicio <b>%s</b> por <b>%s</b>. Con fecha de finalizacion <b>%s</b>', $data['servicio']['nombre'], isset($data['servicio']['motivo']) ? $data['servicio']['motivo'] : 'motivo desconocido', isset($data['servicio']['finalizacion']) ? $data['servicio']['finalizacion'] : 'finalizacion desconocida');
                break;
            default:

                break;
        }
        return $str;
    }
}
