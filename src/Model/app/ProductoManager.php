<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Producto as Producto;
use App\Entity\Organizacion as Organizacion;

class ProductoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($producto)
    {
        if ($producto instanceof Producto) {
            return $this->em->getRepository('App:Producto')->findOneBy(array('id' => $producto->getId()));
        } else {
            return $this->em->getRepository('App:Producto')->findOneBy(array('id' => $producto));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Producto')->byOrganizacion($organizacion, array('p.nombre' => 'ASC'));
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($organizacion);
            return $this->em->getRepository('App:Producto')->byOrganizacion($organizacion->getId(), array('p.nombre' => 'ASC'));
        }
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Producto', 'p')
            ->join('p.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->orWhere($query->expr()->like('p.codigo', ':nombre'))
                ->orWhere($query->expr()->like('p.marca', ':nombre'))
                ->orWhere($query->expr()->like('p.codigoBarra', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        return $query->getQuery()->getResult();
    }

    public function create(Organizacion $organizacion)
    {
        $producto = new Producto();
        $producto->setTasaIva(21);
        $producto->setOrganizacion($organizacion);

        return $producto;
    }

    public function save(Producto $producto)
    {
        $this->em->persist($producto);
        $this->em->flush();
        return $producto;
    }

    public function delete(Producto $producto)
    {
        $this->em->remove($producto);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $producto = $this->find($id);
        if (!$producto) {
            throw $this->createNotFoundException('Código de producto no encontrado.');
        }
        return $this->delete($producto);
    }

    public function getHistorico($producto, $organizacion)
    {
        $query = $this->em->createQueryBuilder()
            ->select('o')
            ->from('App:OrdenTrabajoProducto', 'o')
            ->join('o.ordenesTrabajo', 'ot')
            ->join('o.producto', 'p')
            ->join('o.deposito', 'd')
            ->join('d.organizacion', 'org')
            ->where('p.id = :producto')
            ->andWhere('org.id = :organizacion')
            ->setParameter('producto', $producto->getId())
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('ot.fecha', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function getPrecio($deposito, $producto)
    {
        if ($producto != null and $deposito != null) {
            $query = $this->em->createQueryBuilder()
                ->select('s')
                ->from('App:Stock', 's')
                ->join('s.deposito', 'd')
                ->join('s.producto', 'p')
                ->where('d.id = :deposito')
                ->andWhere('p.id = :producto')
                ->setParameter('producto', $producto->getId())
                ->setParameter('deposito', $deposito->getId());

            return $query->getQuery()->getOneOrNullResult();
        } else {
            return null;
        }
    }
}
