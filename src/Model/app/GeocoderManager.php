<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use GMaps\Geocoder;
use App\Model\app\UtilsManager;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Predis\Client;
use Psr\Cache\CacheItemPoolInterface;
use App\Model\app\Cache\GeocoderCacheManager;

class GeocoderManager
{
    protected $container;
    private $utils;
    private $geocoder;
    private $userloginManager;
    private $cliente;
    private $logger;

    private $cacheGeocoder;

    public function __construct(
        UtilsManager $utils,
        ContainerInterface $container,
        UserLoginManager $userloginManager,
        LoggerInterface $logger,
        HttpClientInterface $cliente,
        GeocoderCacheManager $cache
    ) {
        $this->utils = $utils;
        $this->userloginManager = $userloginManager;
        $this->container = $container;
        $this->geocoder = new Geocoder();
        $this->cliente = $cliente;
        $this->logger = $logger;
        $this->cacheGeocoder = $cache;

        // Configura el cliente Predis
        // $redisClient = new Client([
        //     'scheme' => 'tcp',
        //     'host' => 'localhost',
        //     'port' => 6379,
        // ]);
        // $this->cache = new RedisAdapter($redisClient);
    }

    /*private function searchCache($lat, $lon)
    {
        $str = $lat . '|' . $lon;
        // Obtiene el valor de la caché utilizando la clave
        $item = $this->cache->getItem($str);
        $cacheValue = $item->get();

        if ($item->isHit()) {
            // El valor se encontraba en la caché y se almacenó en $cacheValue
            return $cacheValue;
        } else {
            return false;
        }

        return false;
    }

    private function addCache($lat, $lon, $data)
    {
        $str = $lat . '|' . $lon;
        //dd($data);
        $this->cache->getItem($str)->set($data);
        $item = $this->cache->getItem($str);
        $item->set($data);
        $item->expiresAfter($this->tiempoDeExpiracion);
        return $this->cache->save($item);
    }*/

    public function inverso($latitud, $longitud, $organizacion = null)
    {
        if ($organizacion) {
            $motor = $organizacion->getProveedorMapas();
        } else {
            $motor = $this->userloginManager->getOrganizacion()->getProveedorMapas();
        }
        $resultgeo = $this->cacheGeocoder->searchCache($latitud, $longitud);
        if ($resultgeo) {
            return $this->utils->simplexml2array($resultgeo['direccion']) . '*';   //se pone el * para saber que viene por la cache
        } else {
            $resultgeo = $this->geocoder->inverso($latitud, $longitud, $motor, $this->is_validOsm());
            if ($resultgeo['exito'] == true && $resultgeo['direccion'] != null) {
                $this->cacheGeocoder->addCache($latitud, $longitud, $resultgeo);            //guardo el json completo   
                return $this->utils->simplexml2array($resultgeo['direccion']);
            } else {
                return '---';
            }
        }
    }

    public function inversoRaw($latitud, $longitud)
    {
        $resultgeo = $this->cacheGeocoder->searchCache($latitud, $longitud);
        if ($resultgeo) {
            return $resultgeo;   //debo devolver el json completo
        } else {
            $resultgeo = $this->geocoder->inversoOSM($latitud, $longitud);
            if ($resultgeo['exito'] == true && $resultgeo['direccion'] != null) {
                $this->cacheGeocoder->addCache($latitud, $longitud, $resultgeo);            //guardo el json completo   
                return $resultgeo;
            } else {
                return '---';
            }
        }
    }

    /** data = ['direccion' => 'Av San Martin', 'posicion' => ['latitud' => -32.123, 'longitud' => -64.123]] */
    public function inversoBatch($data, $organizacion)
    {
        if ($organizacion) {
            $motor = $organizacion->getProveedorMapas();
        } else {
            $motor = $this->userloginManager->getOrganizacion()->getProveedorMapas();
        }
        $is_ValidOsm = $this->is_validOsm();    //consulto una sola vez por lote si es valido OSM
        foreach ($data as $i => $d) {
            $direccion = '---';
            $resultgeo = $this->cacheGeocoder->searchCache($d['posicion']['latitud'], $d['posicion']['longitud']);
            if ($resultgeo) {
                $direccion = $this->utils->simplexml2array($resultgeo['direccion']) . '*';   //se pone el * para saber que viene por la cache
            } else {
                $resultgeo = $this->geocoder->inverso($d['posicion']['latitud'], $d['posicion']['longitud'], $motor, $is_ValidOsm);
                if ($resultgeo['exito'] == true && $resultgeo['direccion'] != null) {
                    $this->cacheGeocoder->addCache($d['posicion']['latitud'], $d['posicion']['longitud'], $resultgeo);            //guardo el json completo   
                    $direccion = $this->utils->simplexml2array($resultgeo['direccion']);
                }
            }
            $data[$i]['direccion'] = $direccion;
        }
        return $data;
    }

    public function is_validOsm()
    {
        return true;   //siempre sera valido osm
        //http://172.31.0.19:9121/v1/health/node/db876c5d10f8
        //http://172.31.0.19:9121/v1/agent/checks?filter=CheckID=="openstreetmap-check"
        //"Status":"passing" || "Status":"critical"
        $result = null;
        try {
            $response = $this->cliente->request(
                'GET',
                $this->container->getParameter('consul_osm_addr_config'),
                [
                    'headers' => ['Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'],
                    'auth_basic' => [
                        $this->container->getParameter('consul_osm_usr_config'),
                        $this->container->getParameter('consul_osm_pass_config')
                    ]
                ]
            );
            //  dd($response->getContent());
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                $data = json_decode($response->getContent(), true);
                if (isset($data['openstreetmap-check']['Status'])) {
                    $result = $data['openstreetmap-check']['Status'] == "passing";
                } else {
                    return false;
                }
            }
        } catch (TransportException $e) {
            return false;
        }
        return $result;
    }


    private function getMemDir($key, $lat, $lon)
    {
        $memdir = $this->container->get('session')->get('geocodercache');
        if ($memdir != null) {
            foreach ($memdir as $pos) {
                if ($key == $pos['key']) {
                    $posicion = $pos;
                    break;
                }
            }
        }
        if (!isset($posicion)) {  //no existe posicion
            $posicion = array(
                'key' => $key,
                'latitud' => 0,
                'longitud' => 0,
                'direccion' => '---',
            );
            $memdir[] = $posicion;
        }

        //grabo el memdir en la session
        $this->container->get('session')->set('geocodercache', $memdir);
        return $posicion;
    }

    private function setMemDir($posicion)
    {
        $memdir = $this->container->get('session')->get('geocodercache');
        foreach ($memdir as $key => $value) {
            if ($value['key'] == $posicion['key']) {
                $memdir[$key] = $posicion;
                break;
            }
        }
        //grabo el memdir en la session
        $this->container->get('session')->set('geocodercache', $memdir);
    }
}
