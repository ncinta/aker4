<?php
/**
 * TipoVehiculoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\TipoVehiculo;
use App\Entity\Organizacion as Organizacion;

class TipoVehiculoManager {

    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    
    /**
     * Busco un un tipo de vehiculo por su id
     * @return TipoVehiculo
     */
    public function findById($id) {
        return $this->em->getRepository('App:TipoVehiculo')->find($id);
    }
    
    /**
     * Retorna todos los tipos de vehiculos que hay en el sistema.
     */
    public function findAll() {
        return $this->em->getRepository('App:TipoVehiculo')->findBy(array(), array('tipo' => 'ASC'));
    }

}
