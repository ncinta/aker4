<?php

namespace App\Model\app;

/**
 * Description of LicenciaManager
 *
 * @author nicolas
 */

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chofer;
use App\Entity\Licencia;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UtilsManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LicenciaManager
{

    protected $em;
    protected $utils;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function find($licencia)
    {
        if ($licencia instanceof Licencia) {
            return $this->em->getRepository('App:Licencia')->find($licencia->getId());
        } else {
            return $this->em->getRepository('App:Licencia')->find($licencia);
        }
    }

    public function delete($id)
    {
        $licencia = $this->find($id);
        $chofer = $licencia->getChofer();
        $this->em->remove($licencia);
        $this->em->flush();
        return $chofer;
    }

    public function save($licencia)
    {
        $this->em->persist($licencia);
        $this->em->flush();
        return true;
    }

    public function edit($licencia, $data)
    {
        $licencia->setNumero($data['numero']);
        $licencia->setCategoria($data['categoria']);
        $expedicion = $this->utils->datetime2sqltimestamp($data['expedicion'], true);
        $vencimiento = $this->utils->datetime2sqltimestamp($data['vencimiento'], true);
        $licencia->setExpedicion(new \DateTime($expedicion));
        $licencia->setVencimiento(new \DateTime($vencimiento));
        if ($data['aviso'] != '') {
            $licencia->setAviso(intval($data['aviso']));
        }
        $this->em->persist($licencia);
        $this->em->flush();
        return $licencia;
    }
}
