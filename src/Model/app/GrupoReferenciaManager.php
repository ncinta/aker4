<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Entity\Usuario as Usuario;
use App\Model\app\UserLoginManager;
use App\Model\app\ReferenciaManager;

class GrupoReferenciaManager
{

    protected $em;
    protected $userlogin;
    private $grupo;
    private $referencia;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, ReferenciaManager $referencia)
    {
        $this->userlogin = $userlogin;
        $this->em = $em;
        $this->referencia = $referencia;
    }

    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this->grupo;
    }

    public function find($grupo)
    {
        if ($grupo instanceof GrupoReferencia) {
            $this->grupo = $this->em->find('App:GrupoReferencia', $grupo->getId());
        } else {
            $this->grupo = $this->em->find('App:GrupoReferencia', $grupo);
        }
        return $this->grupo;
    }

    public function findByNombre($nombre)
    {
        $this->grupo = $this->em->getRepository('App:GrupoReferencia')->findOneBy(array('nombre' => $nombre));

        return $this->grupo;
    }

    public function findAllByOrganizacion(Organizacion $organizacion = null, array $orderBy = null)
    {
        if (!$organizacion) {
            $organizacion = $this->userlogin->getOrganizacion(); //busco la organizacion del usuario logueado.
        }
        return $this->em->getRepository('App:GrupoReferencia')
            ->findAllGruposReferencias($organizacion);
    }

    /**
     * Trae todos los grupos de referencias de una organizacion. Si no se pasa 
     * organizacion se toma la organizacion del usuario logueado.
     * @param type $organizacion
     * @return type
     */
    public function findAsociadas($organizacion = null)
    {
        if (is_null($organizacion)) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:GrupoReferencia')->findAllGruposReferencias($organizacion);
    }

    public function findAllByUsuario($usuario = null, array $orderBy = null)
    {
        if (!$usuario) {  //busco la organizacion del usuario logueado.
            $usuario = $this->userlogin->getUser();
        }
        if ($usuario->isMaster()) {
            $grupos = $this->em->getRepository('App:GrupoReferencia')
                ->findAllByUsuario($usuario);
        } else {
            $grupos = $this->em->getRepository('App:GrupoReferencia')
                ->findAllByUsuario($usuario);
        }
        return $grupos;
    }

    public function findAllSinAsignar2Usuario(Usuario $usuario)
    {
        $asignados = $this->findAllByUsuario($usuario);   //obtengo los del usuario
        if ($asignados) {  //tiene asignados, devuelvo los que no estan asignados...
            $idsSelect = null;
            foreach ($asignados as $servicio) {
                $idsSelect[] = $servicio->getId();
            }
            return $this->em->getRepository('App:GrupoReferencia')
                ->findGruposSinAsignar2Usuario($usuario, $idsSelect);
        } else {  //no tiene asignados... devuelvo todo el resto para la organiacion
            return $this->findAllByOrganizacion($usuario->getOrganizacion());
        }
    }

    public function create($organizacion = null)
    {
        if (is_null($organizacion)) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        $this->grupo = new GrupoReferencia();
        $this->grupo->setPropietario($organizacion);
        $organizacion->addGrupoReferencia($this->grupo);

        return $this->grupo;
    }

    public function save(GrupoReferencia $grupo = null)
    {
        if (is_null($grupo)) {
            $grupo = $this->grupo;
        }
        if (is_null($this->grupo)) {
            $grupo->addOrganizacion($grupo->getPropietario());
        }
        $this->em->persist($grupo);
        $this->em->flush();
        return $grupo;
    }

    public function delete($grupo)
    {
        if (!($grupo instanceof GrupoReferencia)) {
            $grupo = $this->find($grupo);
        }
        $this->em->remove($grupo);
        $this->em->flush();
        $this->grupo = null;
    }

    public function addReferencia($referencia)
    {
        if ($referencia instanceof Referencia) {
            $referencia = $this->em->find('App:Referencia', $referencia->getId());
        } else {
            $referencia = $this->em->find('App:Referencia', $referencia);
        }
        $this->grupo->addReferencia($referencia);
        $this->save();

        return $this->grupo;
    }

    public function removeReferencia($referencia)
    {
      //  $referencia = null;
        if ($referencia instanceof Referencia) {
            $referencia = $this->em->find('App:Referencia', $referencia->getId());
        } else {
            $referencia = $this->em->find('App:Referencia', $referencia);
        }
        if ($referencia) {

            $this->grupo->removeReferencia($referencia);
            $this->save();
        }

        return true;
    }

    public function compartir($grupo = null, $organizacion = null)
    {
        if ($grupo != null) {
            //busco la referencia sino viene en el parametro
            $grupo = $this->find($grupo);
        }

        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        if ($grupo && $organizacion) {
            //buscar si ya esta compartida.
            $yacompartida = false;
            $grpAsociadas = $organizacion->getGrupoReferencias();
            foreach ($grpAsociadas as $grp) {
                if ($grp == $grupo) {
                    $yacompartida = true;
                }
            }
            if ($yacompartida == false) {
                $grupo->addOrganizacion($organizacion);    //agrego la organizacion al grupo
                $organizacion->addGrupoReferencia($grupo);      //agrego el grupo a la organizacion
                $this->save($grupo);                       //grabo todo
                return true;
            } else {
                return -1;
            }
        } else {
            return false;
        }
    }

    public function mover($grupo = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($grupo instanceof GrupoReferencia)) {
            $grupo = $this->find($grupo);
        }

        if ($grupo && $organizacion) {

            $oldPropietario = $grupo->getPropietario();     //obtengo el prop actual
            //mover todas las referencias a la nueva organizacion.
            foreach ($grupo->getReferencias() as $referencia) {
                if ($referencia->getPropietario() != $oldPropietario) {
                    $this->referencia->compartir($referencia, $organizacion);
                } else {
                    $this->referencia->mover($referencia, $organizacion);
                }
            }
            //remuevo el propietario anterior del grupo 
            $oldPropietario->removeGrupoReferencia($grupo);   //desasocio el grp del viejo propietario
            $this->em->persist($oldPropietario);

            $grupo->getPropietario()->removeGrupoReferencia($grupo);
            $grupo->setPropietario($organizacion);

            //busco que no este compartido el grupo.
            $existe = $this->em->getRepository('App:GrupoReferencia')->ifExist($grupo, $organizacion);
            if (!$existe) {
                //debo asociar a la nueva org el grupo en cuestion
                $grupo->addOrganizacion($organizacion);
                $organizacion->addGrupoReferencia($grupo);
            }

            $this->save($grupo);
            return true;
        }
        return false;
    }

    public function copiar($grupo = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($grupo instanceof GrupoReferencia)) {
            $grupoOld = $this->find($grupo);
        } else {
            $grupoOld = $grupo;
        }

        if ($grupoOld && $organizacion) {

            $grpNew = $this->create($organizacion);   //creo el nuevo 
            $grpNew->setNombre($grupoOld->getNombre() . ' (Copia)');
            $this->em->persist($grpNew);

            //copiar todas las referencias 
            foreach ($grupoOld->getReferencias() as $referencia) {
                $refNew = $this->referencia->copiar($referencia, $organizacion);
                //agrego la nueva referencia al grupo.
                $grpNew->addReferencia($refNew);
                $refNew->addGrupoReferencia($grpNew);
            }

            $this->save($grpNew);
            return true;
        }
        return false;
    }

    public function remover($grupo = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($grupo instanceof GrupoReferencia)) {
            $grupo = $this->find($grupo);
        }
        try {
            //tengo que borrar todas las referencias de la organizacion.
            foreach ($grupo->getReferencias() as $ref) {
                $organizacion->removeReferencia($ref);
            }
            $organizacion->removeGrupoReferencia($grupo);
            $this->em->persist($organizacion);
            $this->em->flush();
            return true;
        } catch (\Exception $exc) {
            die('////<pre>' . nl2br(var_export($exc->getTraceAsString(), true)) . '</pre>////');
            return false;
        }
    }

    public function asignarUsuario($idGrpReferencia, Usuario $usuario)
    {
        $grupo = $this->find($idGrpReferencia);
        if (!$grupo) {
            throw $this->createNotFoundException('Código de Grupo de referencia no encontrado.');
        }
        $serv = $this->em->getRepository('App:GrupoReferencia')->findUsuario($grupo, $usuario);
        if (count($serv) == 0) {  //trae el usuario asociado.
            $usuario->addGrupoReferencias($grupo);
            $this->em->persist($usuario);
            $this->em->flush();

            return true;
        } else {
            return false;
        }
    }

    public function eliminarUsuario($idGrpReferencia, Usuario $usuario)
    {
        $grupo = $this->find($idGrpReferencia);
        if (!$grupo) {
            throw $this->createNotFoundException('Código de Grupo de referencia no encontrado.');
        }
        $usuario->removeGrupoReferencias($grupo);
        $this->em->persist($usuario);
        $this->em->flush();

        return true;
    }

    public function toArrayData($grupo, $action)
    {
        $data = array(
            'entity' => 'GRUPOREFERENCIA',
            'action' => null,
            'data' => null
        );
        $referencias = array();
        if ($grupo) {
            $data['data']['id'] = $grupo instanceof GrupoReferencia ? $grupo->getId() : $grupo;
            $data['data']['updated_at'] =  $grupo instanceof GrupoReferencia ? $grupo->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $grupo instanceof GrupoReferencia ? $grupo->getCreatedAt()->format('Y-m-d H:i:s') : null;
            if ($action == 2) {
                $data['action'] = 'DELETE';
                return $data;
            }
            $data['action'] = 'UPDATE';
            $data['data']['nombre'] = $grupo->getNombre();
            foreach ($grupo->getReferencias() as $referencia) {
                $referencias[] = $this->toArrayReferencia($referencia);
            }
            $data['data']['referencias'] = $referencias;
        }
        //    die('////<pre>' . nl2br(var_export(json_encode($data), true)) . '</pre>////');
        return $data;
    }

    public function toArrayReferencia($referencia)
    {
        $data = array();
        if ($referencia) {
            $data['id'] = $referencia instanceof Referencia ? $referencia->getId() : $referencia;
            $data['nombre'] = $referencia->getNombre();
            $data['velocidadMaxima'] = $referencia->getVelocidadMaxima();
            $data['latitud'] = $referencia->getLatitud();
            $data['longitud'] = $referencia->getLongitud();
            $data['clase'] = $referencia->getClase(); // 1 = circ, 2 = polig
            if ($referencia->getClase() == 1) {
                $data['radio'] = $referencia->getRadio(); //si es circular
            } else {
                $arrPolig = explode(',', $referencia->getPoligono());
                // te dice si los poligonos son de formato viejo o nuevo (- se refiere al primer caracter de la primera coord)
                if ($arrPolig[0][0] !== '-') {
                    $arrPolig = array_slice($arrPolig, 3); // si es formato viejo eliminamos los colores que estan al ppio
                }
                $i = 0;
                $j = 1;
                foreach ($arrPolig as $key => $value) {
                    if ($j <= count($arrPolig) - 1) {
                        $data['poligono'][] = array((float)$arrPolig[$i], (float) $arrPolig[$j]); //si es polig
                        $i = $i + 2;
                        $j = $j + 2;
                    }
                }
            }
        }
        return $data;
    }
}
