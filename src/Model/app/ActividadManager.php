<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Actividad as Actividad;
use App\Entity\Mantenimiento as Mantenimiento;

/**
 * Description of ActividadManager
 *
 * @author nicolas
 */
class ActividadManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($actividad)
    {
        if ($actividad instanceof Actividad) {
            return $this->em->getRepository('App:Actividad')->findBy(array('id' => $actividad->getId()));
        } else {
            return $this->em->getRepository('App:Actividad')->find(array('id' => $actividad));
        }
    }

    public function create(Mantenimiento $mantenimiento)
    {
        $actividad = new Actividad();
        $actividad->setMantenimiento($mantenimiento);

        return $actividad;
    }

    public function save(Actividad $actividad)
    {
        $this->em->persist($actividad);
        $this->em->flush();
        return $actividad;
    }

    public function delete(Actividad $actividad)
    {
        $this->em->remove($actividad);
        $this->em->flush();
        return true;
    }
}
