<?php

/**
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Servicio;
use App\Entity\ServicioInputs;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\UserLoginManager;

class ServicioInputManager
{

    protected $em;
    protected $security;
    protected $bitacora;
    protected $userlogin;
    private $input;
    private $input_array;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, BitacoraServicioManager $bitacora, UserLoginManager $userlogin)
    {
        $this->security = $security;
        $this->em = $em;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
    }

    public function find($input)
    {
        if ($input instanceof ServicioInputs) {
            $this->input = $this->em->getRepository('App:ServicioInputs')->find($input->getId());
        } else {
            $this->input = $this->em->getRepository('App:ServicioInputs')->find($input);
        }
        $this->input_array = $this->input !== null ? $this->input->data2array() : array();
        return $this->input;
    }

    public function findById($id)
    {
        $this->input = $this->em->getRepository('App:ServicioInputs')->find($id);
        $this->input_array = $this->input->data2array();
        return $this->input;
    }

    public function getModeloInputs($id)
    {
        return $this->em->getRepository('App:ModeloSensor')->find($id);
    }

    public function create(Servicio $servicio)
    {
        if ($servicio) {
            $input = new ServicioInputs();
            $input->setServicio($servicio);
            return $input;
        } else {
            return null;
        }
    }

    public function save(ServicioInputs $inputs)
    {
        $alta = is_null($inputs->getId());  //es para saber si es una alta.
        $this->em->persist($inputs);
        $this->em->flush();

        if ($alta) {
            $this->bitacora->inputNew($this->userlogin->getUser(), $inputs);
        } else {
            $this->bitacora->inputUpdate($this->userlogin->getUser(), $inputs, $this->input_array);
        }

        return $inputs;
    }

    public function delete(ServicioInputs $inputs)
    {
        $this->bitacora->inputDelete($this->userlogin->getUser(), $inputs);
        $this->em->remove($inputs);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $inputs = $this->findById($id);
        if (!$inputs) {
            throw $this->createNotFoundException('Código de Sensor no encontrado.');
        }
        return $this->delete($inputs);
    }

    public function getSensoresDigitales($equipo)
    {
        if (!is_null($equipo)) {
            return $this->em->getRepository('App:ModeloSensor')->findBy(array('modelo' => $equipo->getModelo()->getId(), 'tipoSensor' => 2), array('nombre' => 'ASC'));
        } else {
            return null;
        }
    }
}
