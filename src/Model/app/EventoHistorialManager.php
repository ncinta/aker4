<?php


namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evento as Evento;
use App\Entity\EventoHistorial;
use App\Entity\EventoHistorialNotificacion;
use App\Entity\EventoTemporal;
use App\Entity\HistoricoItinerario;
use App\Entity\Servicio;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\ContactoManager;
use App\Model\app\EventoTemporalManager;
use App\Model\app\UsuarioManager;

class EventoHistorialManager
{

    protected $em;
    protected $security;
    protected $userlogin;
    protected $evTemporalManager;
    protected $contactoManager;
    protected $usuarioManager;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        UserLoginManager $userlogin,
        EventoTemporalManager $evTemporalManager,
        ContactoManager $contactoManager,
        UsuarioManager $usuarioManager
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->evTemporalManager = $evTemporalManager;
        $this->contactoManager = $contactoManager;
        $this->usuarioManager = $usuarioManager;
    }

    private function setTimeZoneforAll($eventos)
    {
        if ($eventos) {
            //convierto la hora a la del usuario.
            foreach ($eventos as $evento) {
                $this->setTimeZone($evento);
            }
        }
        return $eventos;
    }

    public function setTimeZone($eventoH)
    {
        if (!is_null($eventoH)) {
            if ($eventoH->getFecha()) {
                $eventoH->getFecha()->setTimezone(new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            }
        }
        return $eventoH;
    }

    public function listAll($organizacion, $desde = null, $hasta = null, $evento = null, $servicio = null)
    {
        if (!is_null($desde)) {
            $desde = new \DateTime($desde, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $desde->setTimezone(new \DateTimeZone('UTC'));
        }
        if (!is_null($hasta)) {
            $hasta = new \DateTime($hasta, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $hasta->setTimezone(new \DateTimeZone('UTC'));
        }

        $eventos = $this->em->getRepository('App:EventoHistorial')->obtener($organizacion, $desde->format('Y-m-d H:i:s'), $hasta->format('Y-m-d H:i:s'), $evento, $servicio);
        return $this->setTimeZoneforAll($eventos);
    }

    public function findById($id)
    {
        $evento = $this->em->getRepository('App:EventoHistorial')->findOneBy(array('id' => $id));
        return $this->setTimeZone($evento);
    }

    /**
     * Registra el evento generado en el EventoHistorial
     */
    public function add($fecha, $evento, $servicio, $titulo, $data = null)
    {
        $historial = new EventoHistorial();
        $historial->setTitulo($titulo);
        $historial->setFecha($fecha);
        $historial->setEvento($evento);
        $historial->setServicio($servicio);

        if (isset($data['uid'])) {
            $uid = $data['uid'];   //rescato y persisto el uid del evento.
            $historial->setUid($uid);
        }

        if (!is_null($data)) {
            $historial->setData($data);
        }
        if ($evento->getNotificacionWeb() != null && $evento->getNotificacionWeb() != 0) { //hay que notificar. entonces lo agregamos al temporal
            //sino hago notifiaciones entonces lo pongo como visto.
            if ($evento->getNotificacionWeb() == 0 || is_null($evento->getNotificacionWeb())) {
                $historial->setFechaVista(new \DateTime('', new \DateTimeZone('UTC')));
            }
        }
        $this->em->persist($historial);
        $this->em->flush();

        if (isset($uid)) {    //si no tengo iud no tengo nada.
            //controlo si tenemos tokens que registrar notific
            foreach ($data['tokens'] as $token) {
                $contacto = $this->contactoManager->findById($token['id']);
                if ($contacto) {
                    $this->addNotificacion(EventoHistorialNotificacion::CANAL_PUSH, $contacto, $historial);   //canal=1 es token
                }
            }

            //controlo si tenemos emails que registrar notific
            foreach ($data['emails'] as $email) {
                $contacto = $this->contactoManager->findByEmail($email['email'], $historial->getEvento()->getOrganizacion());
                if ($contacto) {
                    $this->addNotificacion(EventoHistorialNotificacion::CANAL_EMAIL, $contacto, $historial);   //canal=2 es emails
                }
            }
        }

        return $historial;
    }

    public function findNotificacion($uid, $data, $canal, $evento)
    {
        //buscar la notificacion para alguno de esos contactos y el uid
        $query = $this->em->createQueryBuilder()
            ->select('e')
            ->from('App:EventoHistorialNotificacion', 'e')
            ->join('e.contacto', 'c')
            ->join('e.eventoHistorial', 'eh')
            ->where('eh.uid = ?1')   // por uid                        
            ->setParameter(1, $uid)
            ->andWhere('eh.evento = ?4')
            ->setParameter(4, $evento)
            ->andWhere('e.canal = ?3')
            ->setParameter(3, $canal);

        if ($canal == 1) {   //push
            //buscar el usuario con ese token
            $usuario = $this->usuarioManager->findByToken($data); //en este caso en DATA esta el token
            //dd($usuario->getTelefono());
            if(!$usuario){
                return null;
            }
            $query->andWhere('c.celular = ?2')->setParameter(2, $usuario->getTelefono());
        } elseif ($canal = 2) {
            $query->andWhere('c.email = ?2')->setParameter(2, $data);
        }     
        //dd($query->getQuery()->getSQL());   
        return $query->getQuery()->getResult();
    }
    
    /**
     * Graba la entidad de notificacion relacionada con el eventohistorial.
     */
    private function addNotificacion($canal, $contacto, $historial)
    {
        $evNotif = new EventoHistorialNotificacion();
        $evNotif->setUid($historial->getUid());
        $evNotif->setEstado(0); //estado inicial... solo se ha generado el evento.        
        $evNotif->setCanal($canal);   //(1=celular/push 2=email)
        $evNotif->setEventoHistorial($historial);
        $evNotif->setContacto($contacto);
        $this->em->persist($evNotif);
        $this->em->flush();
    }

    public function updateNotificacion($newEstado, $notificacion, $fecha)
    {
        //   dd($notificacion->getId());
        if ($newEstado == EventoHistorialNotificacion::ESTADO_ENVIADO) {
            $notificacion->setEstado($newEstado);
            $notificacion->setFechaEnvio($fecha);
        }
        $this->em->persist($notificacion);
        $this->em->flush();
    }


    /**
     * Recibe un evento historial y graba la respuesta
     */
    public function registrar($evHistorial, $respuesta)
    {
        $evHistorial->setEjecutor($this->userlogin->getUser());
        $evHistorial->setRespuesta($respuesta);
        $fecha = new \DateTime();
        $evHistorial->setFechaVista(new \DateTime());
        $this->em->persist($evHistorial);

        $this->em->flush();

        return true;
    }

    public function eventosEnItinerarios($itinerario, $limit)
    {
        //tengo que traer los ids de los servicios a consultar q esten asociados a los itinerarios pasados.
        //tener en cuenta que ahora tengo ItinerarioServicio en el medio.
        //dd($itinerario->getServicios());

        foreach ($itinerario->getServicios() as $itServicio) {
            $ids[] = $itServicio->getServicio()->getId();
        }
        // dd($this->userlogin->getUser());   //2041
        if (isset($ids)) {
            $query = $this->em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                //->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL')     //los no tratados
                ->Where('s.id IN (?1)')   // los que esten en los servicios              
                ->andWhere('h.fecha >= ?2 and h.fecha <= ?3')  //en el rango del itinerario                
                ->addOrderBy('h.id', 'DESC')    //ordenados por id para que sea mas rapido.
                ->setMaxResults($limit)
                ->setParameter(1, $ids)
                ->setParameter(2, $itinerario->getFechaInicio())
                ->setParameter(3, $itinerario->getFechaFin())
                ->join('h.eventoTemporal', 't')
                ->andWhere('t.usuario = ?4')
                ->setParameter(4, $this->userlogin->getUser());
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    /**
     * Devuelve todos los eventos nuevos y/o sin atención que se registren para
     * los servicios que son pasados como parametros. Lo de los servicios es 
     * importante porque se fijan en los servicios de dichos servicios solamente.
     */
    public function buscarNuevos($servicios, $minNotif, $limit = null)
    {
        $lastLogin = $this->userlogin->getUser();
        return $this->em->getRepository('App:EventoHistorial')->obtenerSinAtencion($servicios, $minNotif, $limit, $lastLogin);
    }

    public function contarNuevos($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser();
        return $this->em->getRepository('App:EventoHistorial')->contarSinAtencion($servicios, $minNotif, $lastLogin);
    }

    public function contarTodos($servicios, $minNotif)
    {
        return $this->em->getRepository('App:EventoHistorial')->contarSinAtencion($servicios, $minNotif, null);
    }

    public function contarAnterioresSinAtencion($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser();
        return $this->em->getRepository('App:EventoHistorial')->contarAnterioresSinAtencion($servicios, $minNotif, $lastLogin);
    }

    /**
     * Obtiene el ultimo evento no registrado nuevo desde el ultimo login.
     * @param type $servicios
     */
    public function findLast($servicios, $minNotif)
    {
        $lastLogin = $this->userlogin->getUser()->getLastLogin();
        return $this->em->getRepository('App:EventoHistorial')->obtenerUltimo($servicios, $minNotif, $lastLogin);
    }

    /**
     * Marco como visto el eventoHistorialTemporal. Esto viene desde el popup y lo que viene
     * es el eventoTemporal id
     */
    public function setupVistoTemporal($eventoTmp, $usuario)
    {
        if ($eventoTmp instanceof EventoTemporal) {
            // die('////<pre>' . nl2br(var_export($evento->getId(), true)) . '</pre>////');
            $evento = $eventoTmp->getEventoHistorial();
            //dd($evento->getId());
            $evento->setFechaVista(new \DateTime());
            $evento->setEjecutor($usuario);
            $this->em->persist($evento);

            $this->em->remove($eventoTmp);   //aca borro en el eventoTemporal  

            $this->em->flush();
        } else {
            return false;
        }
    }

    /**
     * Marco como visto el eventoHistorial. Normalmente esto viene desde el show del evento
     */
    public function setupVisto(EventoHistorial $evento, $usuario)
    {
        if ($evento) {
            // die('////<pre>' . nl2br(var_export($evento->getId(), true)) . '</pre>////');            
            $evento->setFechaVista(new \DateTime());
            $evento->setEjecutor($usuario);
            $this->em->persist($evento);

            //aca borro en el eventoTemporal
            $temporales = $this->evTemporalManager->findByUserHist($usuario, $evento);
            foreach ($temporales as $temporal) {
                $this->evTemporalManager->delete($temporal);
            }
            $this->em->flush();
        } else {
            return false;
        }
    }



    public function findByServicio($id, $cantidad)
    {
        $eventos = $this->em->getRepository('App:EventoHistorial')->findByServicio($id, $cantidad);
        return $eventos;
    }

    public function findAllByClientes($clientes, $fecha_desde = null, $fecha_hasta = null, $evento = null, $atendido = null)
    {
        $ids = $arr = array();
        foreach ($clientes as $cli) {
            $ids[] = $cli->getId();
        }
        $desde = \DateTime::createFromFormat('Y-m-d H:i:s', $fecha_desde, new \DateTimeZone($this->userlogin->getTimezone()));
        if ($desde) {
            $desde->setTimezone(new \DateTimeZone('UTC'));
        }

        $hasta = \DateTime::createFromFormat('Y-m-d H:i:s', $fecha_hasta, new \DateTimeZone($this->userlogin->getTimezone()));
        if ($hasta) {
            $hasta->setTimezone(new \DateTimeZone('UTC'));
        }
        $eventos = $this->em->getRepository('App:EventoHistorial')->obtenerAllClientes($ids, $desde, $hasta, $evento, $atendido);

        return $eventos;
    }

    public function delete($id)
    {
        $evento = $this->findById($id);
        if ($evento) {
            $this->em->remove($evento);
            $this->em->flush();
            return true;
        }
        return false;
    }

    public function listNoAtendidas($organizacion, $desde = null, $hasta = null)
    {
        if (!is_null($desde)) {
            $desde = new \DateTime($desde, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $desde->setTimezone(new \DateTimeZone('UTC'));
        }
        if (!is_null($hasta)) {
            $hasta = new \DateTime($hasta, new \DateTimeZone($this->userlogin->getUser()->getTimezone()));
            $hasta->setTimezone(new \DateTimeZone('UTC'));
        }

        $eventos = $this->em->getRepository('App:EventoHistorial')->obtenerNoAtendidas($organizacion, $desde->format('Y-m-d H:i:s'), $hasta->format('Y-m-d H:i:s'));
        return $this->setTimeZoneforAll($eventos);
    }
}
