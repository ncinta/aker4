<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Model\app\UserLoginManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Programacion;
use App\Entity\Modelo;
use App\Entity\Parametro as Parametro;
use App\Model\app\UtilsManager;
use App\Model\app\BitacoraServicioManager;

class ProgramacionManager
{

    protected $em;
    protected $utils;
    protected $programador_addr;
    protected $servicio;
    protected $container;
    protected $userlogin;
    protected $prog = null;
    protected $bitacora;

    public function __construct(
        EntityManagerInterface $em,
        UtilsManager $utils,
        ContainerInterface $container,
        UserLoginManager $userlogin,
        BitacoraServicioManager $bitacora
    ) {
        $this->em = $em;
        $this->utils = $utils;
        $this->programador_addr = $container->getParameter('aker_programador_addr');
        $this->container = $container;
        $this->userlogin = $userlogin;
        $this->bitacora = $bitacora;
    }

    /**
     * graba un panico en la base de datos
     * @param type $panico
     * @return boolean
     */
    public function save($panico)
    {
        $this->em->persist($panico);
        $this->em->flush();
        return true;
    }

    public function findVariable($idVar)
    {
        return $this->em->getRepository('App:Variable')->findOneBy(array('id' => $idVar));
    }

    public function getOnlyVariables($modelo)
    {
        return $this->em->getRepository('App:Variable')->getOnlyVariables($modelo);
    }

    public function findParametro($idParam)
    {
        return $this->em->getRepository('App:Parametro')->findOneBy(array('id' => $idParam));
    }

    /**
     * Setea todas las variables de configuracion remota del equiipo.
     * @param Servicio $servicio
     * @return Servicio 
     */
    public function setDefault($servicio)
    {
        $this->servicio = $servicio;
        //agrego todas las variables segun el modelo
        $modelo = $this->servicio->getEquipo()->getModelo();
        foreach ($modelo->getVariables() as $variable) {
            //            if ($variable->getTipoVariable() != $variable::TVAR_COMPUESTA) {
            //                $this->addParametro($variable, null, $this->servicio);
            //            }
            $this->addParametro($variable, null, $this->servicio);
        }
        return $this->servicio;
    }

    /**
     * Setea un valor para una determinada variable. Se persisten en la base de datos.
     */
    public function setValue($key, $value)
    {
        if (!is_null($value) && strlen($value) > 0) {
            //leer la variable
            $parametro = $this->findParametro(trim($key));
            //comparar los valores nuevos y viejos.
            if ($parametro->getValorActual() !== $value) {
                //grabo el valor en el param
                $parametro->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
                $parametro->setBlock(true);
                $parametro->setIntentos(1);
                $parametro->setValorFuturo($value);
                $this->em->persist($parametro);
                $this->em->flush();
                return $parametro;
            }
        }
        return false;
    }

    public function isChild($parametro)
    {
        //leer la variable
        if (!is_null($parametro->getVariable()->getVariablePadre())) {
            $padre = $parametro->getVariable()->getVariablePadre();
        }
        $x = isset($padre) && ($padre->getTipoVariable() == 5);
        //die('////<pre>' . nl2br(var_export($x, true)) . '</pre>////');
        return $x;
    }

    public function getParametroPadre($varPadre, $servicio)
    {
        return $this->em->getRepository('App:Parametro')->findPadre($varPadre, $servicio);
    }

    /**
     * Setea los parametros de programacion con el valor pasado.
     * @param type $param
     * @param type $valor
     * @param type $setIntentos
     * @return boolean
     */
    public function setConfig($param)
    {
        $var = $param->getVariable();
        if ($param->getVariable()->getTipoVariable() == 5) { //es una variable padre.
            //die('////<pre>' . nl2br(var_export($param->getId(), true)) . '</pre>////');
            $varPadre = $param->getVariable();
            $paramPadre = $param;
            $params = $this->em->getRepository('App:Parametro')->findAllChield($varPadre, $param->getServicio());
            $valor = null;
            foreach ($params as $hijo) {   //aca me aseguro que siempre viajen todos los valores.
                $val = $hijo->getValorFuturo();
                if (is_null($val)) {
                    $val = $hijo->getValorActual();
                }

                $valor[] = array(
                    //'idParametro' => (string) $hijo->getId(),
                    'nombre' => (string) $hijo->getVariable()->getCodename(),
                    'valor' => (string) $val,
                );
            }
            $data = array(
                'idParametro' => (int) $paramPadre->getId() * 1,
                'nombre' => (string) $varPadre->getCodename(),
                'valor' => $valor,
            );
            $id = $paramPadre->getId();
        } else {    //es una variable simple
            $id = $param->getId();
            switch ($var->getTipoVariable()) {
                case $var::TVAR_INTEGER:
                    $data = array(
                        'idParametro' => (int) $param->getId() * 1,
                        'nombre' => (string) $var->getCodename(),
                        'valor' => (string) $param->getValorFuturo(),
                    );
                    break;
                case $var::TVAR_TELECOMANDO:
                    if ($var->getCodename() === 'odometro') {
                        $data = array(
                            'idParametro' => (int) $param->getId() * 1,
                            'nombre' => (string) $var->getCodename(),
                            'valor' => (string) 1,
                        );
                    }
                    break;
                case $var::TVAR_COMPUESTA:
                    break;
                default:
                    break;
            }
        }

        if (isset($data)) {
            $this->prog[] = $data;

            //die('////<pre>' . nl2br(var_export($this->prog, true)) . '</pre>////');

            if (is_null($this->userlogin)) {
                $this->bitacora->programacion(null, $this->servicio, array($data));
            } else {
                $this->bitacora->programacion($this->userlogin->getUser(), $this->servicio, array($data));
            }
            //die('////<pre>' . nl2br(var_export($this->prog, true)) . '</pre>////');
            return true;
        } else {
            return false;
        }
    }

    /**
     * Para un determinado servicio setea el estado de la variable block en true o false
     * @param type $servicio
     * @param type $bloquear
     * @return boolean
     */
    public function setBlock($servicio, $bloquear)
    {
        $params = $servicio->getParametros();
        foreach ($params as $p) {
            $p->setBlock($bloquear);
            $this->em->persist($p);
        }
        $this->em->flush();
        return true;
    }

    public function initConfig($servicio)
    {
        if (is_null($servicio->getEquipo())) {
            return false;
        } else {
            $this->prog = null;
            $this->servicio = $servicio;
            return true;
        }
    }

    /**
     * Arma la programación remota y la graba en la BD para que sea enviada a posterior.
     * @return null
     */
    public function executeConfig()
    {
        if (!is_null($this->prog)) {
            if (is_null($this->servicio->getEquipo()))   //esto es para cuando no tiene equipo asignado.
                return null;

            $prog = new Programacion();
            $prog->setEstado($prog::INDEFINIDO);
            $prog->setServicio($this->servicio);
            $prog->setMdmid($this->servicio->getEquipo()->getMdmid());
            $this->em->persist($prog);

            //            foreach ($this->prog as $k => $value) {
            //                $this->prog[$k]['idParametro'] = $value['idParametro'] + $prog->getId();
            //            }
            $pack = array(
                'id' => (int) $prog->getId() * 1,
                'idEquipo' => (string) $this->servicio->getEquipo()->getMdmid(),
                'ip' => (string) $this->servicio->getUltIp(),
                'puerto' => $this->servicio->getUltPuerto(),
                'tipoEquipo' => $this->servicio->getEquipo()->getModelo()->getTipoProgramacion(),
                'programaciones' => $this->prog,
            );
            $prog->setProgramacion(json_encode($pack));
            $prog->setIntentos(0);
            $this->em->persist($prog);
            $this->em->flush();
            //die('////<pre>' . nl2br(var_export($pack, true)) . '</pre>////');
            return $prog->getId();
        } else {
            return null;
        }
    }

    /**
     * NOTA: puedo tener varios capt en la misma ip origen y puede parecer que no 
     * es suficiente con la ip que me viene en la trama para determinar el programador
     * pero si lo es y digo porque. En un server pueden haber muchos capt con misma ip pero
     * distintos puertos de captura. Pero para un mismo equipo solo me interesa saber 
     * en que otro equipo esta el programado. Asi que en el ejemplo de abajo, con tener
     * 192.168.1.81 puedo enviar las prog a 192.168.1.85 sin problema
     * capt 192.168.1.81:1724 -> prog: 192.168.1.85:4101
     * capt 192.168.1.81:1725 -> prog: 192.168.1.85:4101
     * @param Servicio $servicio
     * @return string programador ip:puerto del programador donde hayque mandar la prog.
     */
    private function obtenerProgramador($servicio)
    {
        $programador = $this->programador_addr; ///esto es por default
        //        $ultCapturador = null;
        //        if (is_null($servicio->getUltIpCapturador())) {
        //            $ultTrama = json_decode($servicio->getUltTrama(), true);
        //            if (!is_null($ultTrama)) {
        //                $ultCapturador = isset($ultTrama['ip_capturador']) ? $ultTrama['ip_capturador'] : null;
        //            }
        //            unset($ultTrama);
        //        } else {
        //            $ultCapturador = $servicio->getUltIpCapturador();
        //        }
        //        if ($ultCapturador != null) {
        //            $prog = $this->em->getRepository('AppBundle:Capturador')->findOneBy(array('ip' => $ultCapturador));
        //            if ($prog && !is_null($prog->getProgramador())) {
        //                $programador = $prog->getProgramador();
        //            }
        //        }
        return $programador;
    }

    /**
     * Esta funcion toma la programacion del ID y la envia al programador remoto.
     * @param type $id
     * @return type
     */
    public function runProgramacion($id)
    {
        $prog = $this->em->getRepository('App:Programacion')->findOneBy(array('id' => $id));
        if (!is_null($prog)) {
            //rearmo el buffer con los datos nuevos del equipo.
            $buff = json_decode($prog->getProgramacion(), true);

            $buff['ip'] = (string) $prog->getServicio()->getUltIp();
            $buff['puerto'] = $prog->getServicio()->getUltPuerto();

            $result = $this->send($this->obtenerProgramador($prog->getServicio()), json_encode($buff), $id);

            $int = $prog->getIntentos() + 1;

            $res = $prog::RECHAZADA;   //se pone por default en rechazada.
            if ($result != '' && !is_null($result)) {
                $json = json_decode($result, true);
                //die('////<pre>' . nl2br(var_export($json, true)) . '</pre>////');
                if (isset($json['result']) && strtoupper($json['result']) == 'OK') {
                    $res = $prog::DESPACHADA;
                } else {
                    $prog->setResultado($result);
                }
            } else {
                $res = $prog::INDEFINIDO;
            }
            $prog->setEstado($res);
            $prog->setIntentos($int);
            $this->em->persist($prog);
            $this->em->flush();

            return $res;
        } else {
            return false;
        }
    }

    public function getStatusProgramacion($id)
    {
        $prog = $this->em->getRepository('App:Programacion')->findOneBy(array('id' => $id));
        if ($prog) {
            return $prog->getEstado();
        } else {
            return false;
        }
    }

    private function get($url)
    {
        // die('////<pre>' . nl2br(var_export($url, true)) . '</pre>////');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Expect: 100-continue",
            "Accept: application/json"
            //"Content-length: " . strlen($data),
        ));
        //execute post
        if (!$result = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);
        // die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
        return $result;
    }

    public function getStatusProg($id, $idCom)
    {
        //$this->programador_addr
        $tmp = $this->get(sprintf('%s/%d/%d', $this->programador_addr, $id, $idCom));
        $result = json_decode($tmp, true);
        //die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
        //$prog = $this->em->getRepository('AppBundle:Programacion')->findOneBy(array('id' => $id));
        if ($result['result'] == 'ok') {
            if ($result['status'] == 'pending') {
                return 2; //pendiente
            } elseif ($result['status'] == 'confirmed') {
                return 5;  //terminada ok
            } elseif ($result['status'] == 'failed') {
                return -1; //rechazada
            } else {
                return 0; //indefinido
            }
        } else {
            return false;
        }
    }

    /**
     * Este proceso es el que se ejecuta luego de que se llame al webservice con el resultado
     * de las programaciones remotas realizadas en el programador.
     * @param type $id
     * @param type $status
     * @param type $prg
     */
    public function responseProgramacion($id, $idParametro, $status)
    {
        $prog = $this->em->getRepository('App:Programacion')->findOneBy(array('id' => $id));
        if ($prog) {
            //die('////<pre>'.nl2br(var_export($prog->getId(), true)).'</pre>////');
            switch ($status) {
                case '0':    ///se rechazo todo
                    $prog->setEstado($prog::DESPACHADA);   //graba 1
                    break;
                case '1':   ///se aprobo todo.
                    $prog->setEstado($prog::TERMINADAOK);    //graba 5
                    break;
                case '2':   /// es parcial
                    $prog->setEstado($prog::TERMINADAPARCIAL);   //graba 6
                    break;
                default:
                    break;
            }
            $prog->setEjecucionAt(new \DateTime());
            $int = $prog->getIntentos() + 1;
            $prog->setIntentos($int);
            $prog->setResultado($prg);
            $this->em->persist($prog);

            //aca armo un array con los id valor que necesito para tomar los valores.
            $progms = json_decode($prog->getProgramacion(), true);  //en progms tengo el array que envie
            $progValue = array();  //en progValue tendria los resultados
            foreach ($progms['programaciones'] as $value) {
                if (is_array($value['valor'])) {  //es un valor compuesto       
                    foreach ($value['valor'] as $val) {  //recupero lo que mande
                        if (in_array($val['nombre'], array('corteMotor', 'corte', 'corteCerrojo', 'cerrojo', 'cierre', 'tiempo', 'distancia'))) {
                            $progValue[$value['idParametro']] = $val['valor'];
                        }
                    }
                } else {
                    $progValue[$value['idParametro']] = $value['valor'];
                }
            }
            $reProg = null;   //se almacenan las reprogramaciones.
            //aca recorro las programaciones para setear los valores.            
            foreach ($prg as $value) {
                //aca leo el parametro que viene en la resp y tomo lo que tengo en la bd.
                $parm = $this->em->getRepository('App:Parametro')->findOneBy(array('id' => $value['idParametro']));
                if ($parm && isset($progValue[$value['idParametro']]) && $value['estado'] == '1') {   //exito en la prog.
                    //aca tengo que determinar si es una programacion padre o hija.
                    if ($parm->getVariable()->getTipoVariable() == 5) {   //esto es una prog padre por lo que hay que tomar los higos y setearlos como exitos.
                        //obtengo todos los hijos.
                        $params = $this->em->getRepository('App:Parametro')->findAllChield($parm->getVariable(), $parm->getServicio());
                        foreach ($params as $phijo) {  //recorro todos los hijos.
                            //actualizo las variables de cada uno de los hijos.
                            $phijo->setValorAnterior($phijo->getValorActual());
                            $phijo->setValorActual($phijo->getValorFuturo());
                            $phijo->setValorFuturo(null);
                            $phijo->setIntentos(null);
                            $phijo->setUltProgramacion(new \DateTime());  //seteo la fecha de ultima programacion.
                            $phijo->getServicio()->setFechaUltConfig(new \DateTime());   //hora utc de la ultima configuracion
                            $phijo->setBlock(false);    //esto se pone a false
                            $this->em->persist($phijo);
                            $this->em->flush();
                        }
                    } else {   //variable simple
                        $parm->setValorAnterior($parm->getValorActual());
                        if (!is_null($progValue[$idParametro])) {   //no es un valor compuesto
                            //die('////<pre>'.nl2br(var_export($value, true)).'</pre>////');
                            if (isset($value['valor'])) {
                                //debo poner el valor q viene en la programacion
                                $parm->setValorActual($value['valor']);

                                //como es un parametro de peticion se debe procesar la grabacion del valor devuelto donde corresponda en el sistema
                                $this->saveDataQuery($parm, $value['valor']);
                            } else {
                                //pongo en valor actual el valor que estaba grabado en la programación.
                                $parm->setValorActual($progValue[$idParametro]);
                            }
                        }

                        //controlo que el val futuro sea el que estoy programando, para vaciarlo.
                        if ($parm->getValorFuturo() == $parm->getValorActual()) {
                            $parm->setValorFuturo(null);
                        }
                        $parm->setIntentos(null);
                        $parm->setUltProgramacion(new \DateTime());  //seteo la fecha de ultima programacion.
                        $parm->getServicio()->setFechaUltConfig(new \DateTime());   //hora utc de la ultima configuracion
                    }
                } else {    //la programacion fallo.
                    $int = $parm->getIntentos() + 1;   //solo si fallo debo incrementar los intentos.
                    $parm->setIntentos($int);

                    //                    if ($prog->getEstado() == $prog::TERMINADAPARCIAL) {    //solo en los casos que sea parcial.
                    //                        //aca armo la nueva programacion con los fallados.                    
                    //                        if (!is_null($progValue[$value['idParametro']])) {   //NO es un valor compuesto
                    //                            $reProg[] = $this->getHistoryValue($value['idParametro'], $value['parametro'], $progValue[$value['idParametro']]);
                    //                        }
                    //                    }
                }
                $parm->setBlock(false);    //esto se pone a false
                $this->em->persist($parm);
            }

            $this->em->flush();

            $this->setupReprogramacion($prog->getServicio(), $reProg);
        }
    }

    /**
     * Toma un parametro y el valor devuelto por una programacion remota
     * y graba el valor en el lugar correspondiente del sistema. Esto puede ser
     * desde el simple pedido de version hasta pedido de configuraciones
     */
    private function saveDataQuery($param, $value)
    {
        if ($param->getVariable()->getCodename() == 'version') {  //es pedido de version.
            $equipo = $param->getServicio()->getEquipo();
            if ($equipo != null) {
                $equipo->setVersionFirmware($value);
                $this->em->persist($equipo);
                $this->em->flush();
                $this->bitacora->queryVersion(null, $param->getServicio(), 'set', $value);
            }
        }
        return true;
    }

    //  se levanta con esta linea
    // /home/yesica/ServerC/dist/Debug/GNU-Linux-x86$ ./serverc
    // y corre en el puerto 1799 de mapas1
    private function send($backend, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $backend);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Expect: 100-continue",
            "Accept: application/json",
            "Content-length: " . strlen($data),
        ));

        //execute post
        if (!$result = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);
        // die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
        return $result;
    }

    /**
     * Toma un servicio y los parametros y crea el formulario para que renderice en la vista
     * en el momento de configurar las variables de los equipos.
     * @param Servicio $servicio
     * @param Parametro $parametros Parametros que deben configurarse.
     * @param boolean $default Indica si se cargaran los valores por defecto.
     * @return form Formulario para ser renderizado en la vista.
     */
    public function createPrgForm($servicio, $parametros, $default = false)
    {
        $build = $this->container->get('form.factory')->createBuilder('form');
        foreach ($parametros as $param) {
            $var = $param->getVariable();
            if ($this->userlogin->isGranted($var->getRole())) {
                switch ($var->getTipoVariable()) {
                    case $var::TVAR_INTEGER:
                        if ($param->getVariable()->isUsable()) {
                            $build->add($param->getId() . '', IntegerType::class, $this->addOptionsForm($var, $default));
                        }
                        break;
                    case $var::TVAR_TELECOMANDO:
                        $build->add($param->getId() . '', ChoiceType::class, $this->addOptionsForm($var, $default));
                        break;
                    case $var::TVAR_COMPUESTA:   //me llega una variable compuesta por lo que debo armar el subform.
                        $params = $this->em->getRepository('App:Parametro')->findAllChield($var, $param->getServicio());
                        foreach ($params as $param) {
                            $options = $this->addOptionsForm($param->getVariable(), $default);
                            // $options['attr']['value'] = is_null($param->getValorActual()) ? $param->getValorFuturo() : $param->getValorActual();
                            $build->add($param->getId() . '', IntegerType::class, $options);
                        }
                        //die('////<pre>' . nl2br(var_export(count($params), true)) . '</pre>////');

                        break;
                    default:
                        break;
                }
            }
        }
        return $build->getForm();
    }

    private function addOptionsForm($variable, $default)
    {
        switch ($variable->getTipoVariable()) {
            case $variable::TVAR_INTEGER:
                $options = array(
                    'label' => $variable->getNombre(),
                    'required' => false,
                    'invalid_message' => 'Valores fuera de rango',
                    'attr' => array(
                        'min' => $variable->getCotaMinima(),
                        'max' => $variable->getCotaMaxima(),
                        'value' => $default ? $variable->getValorDefault() : '',
                        'help' => sprintf('Valores entre %d y %d. En %s (Default: %d)', $variable->getCotaMinima(), $variable->getCotaMaxima(), $variable->getUnidadMedida(), $variable->getValorDefault()),
                    )
                );
                if (!is_null($variable->getVariablePadre())) {
                    //  $options['required'] = true;
                }
                return $options;
                break;
            case $variable::TVAR_TELECOMANDO:
                return array(
                    'label' => $variable->getNombre(),
                    'required' => false,
                    'choices' => array('0' => 'Habilitar', '1' => 'Deshabilitar'),
                    'multiple' => false,
                    'attr' => array(
                        'help' => sprintf('Cortar / Habilitar (Default: Habilitar)'),
                    )
                );
                break;
        }
    }

    public function addParametro($variable, $valor, $servicio)
    {
        $param = $this->em->getRepository('App:Parametro')->findOneBy(array(
            'variable' => $variable->getId(),
            'servicio' => $servicio->getId()
        ));
        if (is_null($param)) {
            $param = new Parametro();
            $servicio->addParametro($param);
            $param->setServicio($servicio);
            $param->setVariable($variable);
            $param->setValorFuturo($valor);

            //$this->em->persist($param);
            $this->em->persist($servicio);
            $this->em->flush();
        }
        return true;
    }

    private function getHistoryValue($id, $codename, $valor)
    {
        return array(
            'idParametro' => (string) $id,
            'nombre' => (string) $codename,
            'valor' => (string) $valor,
        );
    }

    private function setupReprogramacion($servicio, $reProg)
    {
        if ($reProg != null) {
            $this->initConfig($servicio);
            $this->prog = $reProg;
            $this->executeConfig();
        }
        return false;
    }

    /**
     * Obtengo un parametro por codename de su tipo de variable. Sino tiene configuradas
     * las variables de programacion hace el seteo por default.
     * @param type $servicio
     * @param type $codename
     * @return type $parametro
     */
    public function getParamByCodename($servicio, $codename)
    {
        $var = $this->getVariable($servicio, $codename); //obtengo la variable
        //die('////<pre>' . nl2br(var_export($var, true)) . '</pre>////');
        if ($var) {
            $param = $this->em->getRepository('App:Parametro')->findOneBy(
                array(
                    'servicio' => $servicio->getId(),
                    'variable' => $var->getId()
                )
            );
            if (is_null($param)) {
                $this->setDefault($servicio);
                $param = $this->em->getRepository('App:Parametro')->findOneBy(
                    array(
                        'servicio' => $servicio->getId(),
                        'variable' => $var->getId()
                    )
                );
            }
            return $param;
        }
        return null;
    }

    public function existVariableByCodename($modelo, $codename)
    {
        return !is_null($this->em->getRepository('App:Variable')->findOneBy(array(
            'codename' => $codename,
            'modelo' => $modelo
        )));
    }

    private function getUidEquipo($servicio)
    {
        if ($servicio->getEquipo()->getModelo()->getTipoProgramacion() == Modelo::TPROG_CARFINDER) {
            $tmp = explode(':', $servicio->getEquipo()->getMdmid());
            return $tmp[1];
        } else {
            return $servicio->getEquipo()->getMdmid();
        }
    }

    private function getVariable($servicio, $codename)
    {
        //obtengo la variable
        if (!is_null($servicio->getEquipo())) {
            return $this->em->getRepository('App:Variable')->findOneBy(array(
                'modelo' => $servicio->getEquipo()->getModelo()->getId(),
                'codename' => $codename,
            ));
        }
        return null;
    }

    /**
     * Arma el array de programación remota generica compuesta de corte de motor
     * y lo persiste dejandolo en estado de pendiente para que sea procesado.
     * @param type $servicio Nombre del servicio
     * @param boolean $corte True = debe cortarse el motro
     * @return integer Id del entity Programacion donde se grabo el corte.
     */
    public function setCorteMotor($servicio, $corte)
    {
        $param = $this->getParamByCodename($servicio, 'corteMotor');
        if ($param) {
            $uidEquipo = $this->getUidEquipo($servicio);
            //seteo los valores de parametros.
            $param->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
            $param->setValorFuturo($corte);
            $param->setIntentos(1);
            $this->em->persist($param);
            $this->em->flush();

            $this->prog[] = array(
                'idParametro' => (int) $param->getId() * 1,
                'nombre' => 'corteMotor',
                'valor' => array(
                    array('nombre' => 'uidEquipo', 'valor' => (string) $uidEquipo),
                    array('nombre' => 'corte', 'valor' => (string) $corte),
                ),
            );

            if (is_null($this->userlogin)) {
                $this->bitacora->corteMotor(null, $this->servicio, $corte);
            } else {
                $this->bitacora->corteMotor($this->userlogin->getUser(), $this->servicio, $corte);
            }
            return $param->getId();
        }
        return false;
    }

    /**
     * Arma el array de programación remota generica compuesta de seteo de estado de cerrojo
     * y lo persiste dejandolo en estado de pendiente para que sea procesado.
     * @param type $servicio
     * @param type $accion
     * @return integer Id del entity Programacion donde se grabo el corte.
     */
    public function setCerrojo($servicio, $accion)
    {
        $param = $this->getParamByCodename($servicio, 'corteCerrojo');
        //die('////<pre>' . nl2br(var_export($param->getId(), true)) . '</pre>////');
        if ($param) {
            $uidEquipo = $this->getUidEquipo($servicio);

            //seteo los valores de parametros.
            $param->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
            $param->setValorFuturo($accion);
            $param->setIntentos(1);
            $this->em->persist($param);
            $this->em->flush();

            $this->prog[] = array(
                'idParametro' => (int) $param->getId() * 1,
                'nombre' => 'cerrojo',
                'valor' => array(
                    array('nombre' => 'uidEquipo', 'valor' => (string) $uidEquipo),
                    array('nombre' => 'cierre', 'valor' => (string) $accion),
                ),
            );

            if (is_null($this->userlogin)) {
                $this->bitacora->cerrojo(null, $this->servicio, $accion);
            } else {
                $this->bitacora->cerrojo($this->userlogin->getUser(), $this->servicio, $accion);
            }

            return $param->getId();
        }
        return false;
    }

    public function setQueryVersion($servicio)
    {
        $param = $this->getParamByCodename($servicio, 'version');
        if ($param) {
            //seteo los valores de parametros.
            $param->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
            $param->setValorFuturo('?');
            $param->setIntentos(1);
            $this->em->persist($param);
            $this->em->flush();

            $this->prog[] = array(
                'idParametro' => (int) $param->getId() * 1,
                'nombre' => 'version',
                'valor' => '',
            );

            if (is_null($this->userlogin)) {
                $this->bitacora->queryVersion(null, $this->servicio, 'get');
            } else {
                $this->bitacora->queryVersion($this->userlogin->getUser(), $this->servicio, 'get');
            }
            return 0;
        }
        return false;
    }

    public function setIpFota($servicio)
    {
        $param = $this->getParamByCodename($servicio, 'ipfota');
        if ($param) {
            //seteo los valores de parametros.
            $param->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
            $param->setValorFuturo('?');
            $param->setIntentos(1);
            $this->em->persist($param);
            $this->em->flush();

            //agrego la programacion a la cola de programaciones
            $this->prog[] = array(
                'idParametro' => (int) $param->getId() * 1,
                'nombre' => 'ipfota',
                'valor' => array(
                    array('nombre' => 'ip', 'valor' => '200.80.203.68'),
                    array('nombre' => 'puerto', 'valor' => '9500')
                ),
            );
            //die('////<pre>' . nl2br(var_export($this->prog, true)) . '</pre>////');
            //        if (is_null($this->userlogin)) {
            //            $this->bitacora->queryVersion(null, $this->servicio, 'get');
            //        } else {
            //            $this->bitacora->queryVersion($this->userlogin->getUser(), $this->servicio, 'get');
            //        }
            return 0;
        }
        return false;
    }

    public function setActFirmware($servicio)
    {
        $param = $this->getParamByCodename($servicio, 'firmware');
        if ($param) {
            //seteo los valores de parametros.
            $param->setUltSeteo(new \DateTime());   //seteo la fecha de la programacion.
            $param->setValorFuturo('?');
            $param->setIntentos(1);
            $this->em->persist($param);
            $this->em->flush();

            //agrego la programacion a la cola de programaciones
            $this->prog[] = array(
                'idParametro' => (int) $param->getId() * 1,
                'nombre' => 'firmware',
                'valor' => '',
            );
            //die('////<pre>' . nl2br(var_export($this->prog, true)) . '</pre>////');
            //        if (is_null($this->userlogin)) {
            //            $this->bitacora->queryVersion(null, $this->servicio, 'get');
            //        } else {
            //            $this->bitacora->queryVersion($this->userlogin->getUser(), $this->servicio, 'get');
            //        }
            return 0;
        }
        return false;
    }
}
