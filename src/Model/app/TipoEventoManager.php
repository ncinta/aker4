<?php

/**
 * TipoEventoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Evento;

class TipoEventoManager {

    protected $em;
    protected $security;
    protected $evento;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security) {
        $this->security = $security;
        $this->em = $em;
    }

    public function findAll() {
        return $this->em->getRepository('App:TipoEvento')->findBy(
                        array(), array('nombre' => 'ASC'));
    }

    public function findById($id) {
        return $this->em->getRepository('App:TipoEvento')->find($id);
    }

    public function findByCodename($codename) {
        return $this->em->getRepository('App:TipoEvento')->findOneBy(array(
                    'codename' => $codename,
        ));
    }

    public function findByCodenameOrganizacion($codename, $organizacion) {
        return $this->em->getRepository('App:TipoEvento')->findOneBy(array(
                    'codename' => $codename,
                    'organizacion' => $organizacion
        ));
    }

    public function findVariable($id) {
        return $this->em->getRepository('App:VariableEvento')->findOneBy(array(
                    'id' => $id,
        ));
    }

    public function findAllVariables() {
        return $this->em->getRepository('App:VariableEvento')->findAll();
    }

    public function setVariables($evento, $variables) {
        foreach ($variables as $variable) {
            $evento->addVariables($variable);
        }
        $this->em->persist($evento);
        $this->em->flush();
        return $evento;
    }
    
       public function save($evento) {
        $this->em->persist($evento);
        $this->em->flush();
        return $evento;
    }

}
