<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Panico;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;

class PaisManager
{

    protected $em;
    protected $userlogin;
    protected $utils;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->utils = $utils;
    }

    public function getArrayPaises()
    {
        $arr = array();
        $paises = $this->em->getRepository('App:Pais')->findBy(array(), array('nombre' => 'ASC'));
        foreach ($paises as $pais) {
            $arr[$pais->getNombre()] = $pais->getTimeZone();
        }
        return $arr;
    }
}
