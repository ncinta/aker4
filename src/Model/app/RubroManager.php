<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Rubro;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of RubroManager
 *
 * @author nicolas
 */
class RubroManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function save($rubro)
    {
        $this->em->persist($rubro);
        $this->em->flush();
        return $rubro;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:Rubro')->findByOrg($organizacion);
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:Rubro')->findOneBy(array('id' => intval($id)));
    }

    public function deleteById($id)
    {
        $rubro = $this->findOne($id);
        if (!$rubro) {
            throw $this->createNotFoundException('Código de rubro no encontrado.');
        }
        return $this->delete($rubro);
    }

    public function delete($rubro)
    {
        $this->em->remove($rubro);
        $this->em->flush();
        return true;
    }

    public function create($organizacion)
    {
        $rubro = new Rubro();
        $rubro->setOrganizacion($organizacion);
        return $rubro;
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Rubro', 'p')
            ->join('p.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        return $query->getQuery()->getResult();
    }

    private function getData($organizacion, $desde, $hasta, $rubro = null, $cc = null)
    {
        $query = $this->em->createQueryBuilder()
            ->select('otp')
            ->from('App:OrdenTrabajoProducto', 'otp')
            ->join('otp.ordenesTrabajo', 'ot')
            ->join('otp.producto', 'p')
            ->join('p.organizacion', 'o')
            ->join('p.rubro', 'r')
            ->where('o.id = :organizacion')
            ->andWhere('ot.fecha >= :desde')
            ->andWhere('ot.fecha <= :hasta')
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('organizacion', $organizacion->getId());

        if (!is_null($rubro) && $rubro != 0) {
            $query->andWhere('r.id = :idRubro')->setParameter('idRubro', intval($rubro));
        }
        if (!is_null($cc) && $cc != 0) {
            $query->join('ot.centroCosto', 'cc');
            $query->andWhere('cc.id = :idCCosto')->setParameter('idCCosto', intval($cc));
        }

        return $query->getQuery()->getResult();
    }

    public function informe($organizacion, $desde, $hasta, $rubro = null, $cc = null)
    {
        $data = $this->getData($organizacion, $desde, $hasta, intval($rubro), intval($cc));

        $informe = array();
        $totRubro = array();
        foreach ($data as $otp) {   //recorro todos los productos
            $rubro = $otp->getProducto()->getRubro();

            $informe[$rubro->getNombre()][] = array(
                'fecha' => $otp->getOrdenesTrabajo()->getFecha(),
                'servicio' => $otp->getOrdenesTrabajo()->getServicio()->getNombre(),
                'id' => $otp->getOrdenesTrabajo()->getId(),
                'nro_interno' => $otp->getOrdenesTrabajo()->getNumeroInterno(),
                'nro_externo' => $otp->getOrdenesTrabajo()->getNumeroExterno(),
                'centroCosto' => $otp->getOrdenesTrabajo()->getCentroCosto() ? $otp->getOrdenesTrabajo()->getCentroCosto()->getNombre() : '---',
                'cantidad' => $otp->getCantidad(),
                'producto' => $otp->getProducto()->getNombre(),
                'neto' => $otp->getCostoNeto(),
                'total' => $otp->getCostoTotal(),
            );

            $cCantAcu = isset($totRubro[$rubro->getNombre()]) ? $totRubro[$rubro->getNombre()]['cantidad'] : 0;
            $cTotalAcu = isset($totRubro[$rubro->getNombre()]) ? $totRubro[$rubro->getNombre()]['costoTotal'] : 0;
            $cNetoAcu = isset($totRubro[$rubro->getNombre()]) ? $totRubro[$rubro->getNombre()]['costoNeto'] : 0;

            $totRubro[$rubro->getNombre()] = array(
                'cantidad' => $cCantAcu + $otp->getCantidad(),
                'costoTotal' => $cTotalAcu + $otp->getCostoTotal(),
                'costoNeto' => $cNetoAcu + $otp->getCostoNeto(),
            );
        }

        return array('informe' => $informe, 'total' => $totRubro);
    }
}
