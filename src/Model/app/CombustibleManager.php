<?php

namespace App\Model\app;

use App\Entity\CargaCombustible;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Combustible as Combustible;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of CombustibleManager
 *
 * @author nicolas
 */
class CombustibleManager
{

    protected $em;
    protected $userlogin;
    protected $bitacora;

    public function __construct(EntityManagerInterface $em, BitacoraManager $bitacora, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
    }

    public function save($combustible)
    {
        if (is_null($combustible->getLitrosCarga()) || $combustible->getLitrosCarga() == 'NaN') {
            return false;
        }
        if ($combustible->getMontoTotal() == 'NaN') {
            return false;
        }
        $this->em->persist($combustible);
        $this->em->flush();
        $this->bitacora->combustibleCreateManual($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
        return true;
    }

    public function update($combustible)
    {
        $this->em->persist($combustible);
        $this->em->flush();
        $this->bitacora->combustibleUpdate($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
    }

    public function delete($combustible)
    {
        $this->em->remove($combustible);
        $this->em->flush();
        $this->bitacora->combustibleDelete($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
    }

    public function findByServicioYfecha($servicio, $desde, $hasta)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByFecha($servicio, $desde, $hasta);
    }

    public function findAllCargas($servicio)
    {
        return $this->em->getRepository('App:CargaCombustible')->findAllCargas($servicio);
    }

    public function findCarga($id)
    {
        return $this->em->getRepository('App:CargaCombustible')->findCarga($id);
    }

    
}
