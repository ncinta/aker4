<?php

namespace App\Model\app;

use Symfony\Component\HttpFoundation\RequestStack;
use GMaps\Map as Map;
use GMaps\Marker;
use GMaps\Icon;
use GMaps\Polyline;
use GMaps\Polygon;
use GMaps\Circle;
use GMaps\Geocoder;
use GMaps\ClusterIcon;
use App\Entity\Referencia as Referencia;
use App\Entity\Servicio as Servicio;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use Symfony\Component\Routing\RouterInterface;

class GmapsManager
{

    private $bundle_path;
    private $userlogin;
    private $request;
    private $utils;
    private $router;
    private $showReferencias = false;

    public function getShowReferencias()
    {
        return $this->showReferencias;
    }

    public function __construct(UserLoginManager $userlogin, RouterInterface $router, UtilsManager $utils, RequestStack $request)
    {
        $this->bundle_path = $request->getCurrentRequest()->getBasePath() . '/';
        $this->userlogin = $userlogin;
        $this->request = $request;
        $this->utils = $utils;
        $this->router = $router;
    }

    private function getBundlePath()
    {
        return $this->bundle_path;
    }

    private function getImagenTexto($texto)
    {
        return $this->getBundlePath() . 'images/texto.png.php?texto=' . $texto;
    }

    private function getImagenTextoReferencia($texto)
    {
        return $this->getBundlePath() . 'images/textoref.png.php?texto=' . $texto;
    }

    public function getIcono($direccion, $color = 0)
    {
        if ($direccion == -1 || $color == 0) {
            return $this->getBundlePath() . 'images/flechas/donut.png';
        } else {
            if ($color > 0 && $color <= 40) {
                $rgb = 0; //'#eefd9e';
            } elseif ($color > 40 && $color <= 60) {
                $rgb = 1; //'#e5ce0f';
            } elseif ($color > 60 && $color <= 80) {
                $rgb = 2; //'#DF7F07';
            } elseif ($color > 80 && $color <= 100) {
                $rgb = 3; //'#E56000';
            } elseif ($color > 100) {
                $rgb = 4; //'#E50D00';
            } else {
                $rgb = 0; //'#FFFFFF';
            }
            //$direccion = 10;
            $i = ($direccion > 0 && $direccion < 360) ? $direccion / 10.0 : 0;
            settype($i, "integer");
            return $this->getBundlePath() . "images/flechas/color$rgb/arrow$i.png";
            //return $this->getBundlePath() . 'images/icono.png.php?angulo=' . $direccion . '&color=' . $color;
        }
    }

    /**
     * Crea un mapa y setea lo valores iniciales.
     * @return \GMaps\Map
     */
    public function createMap($clusterado = false)
    {
        if (is_null($this->userlogin->getUser()->getShowReferencias())) {
            $this->showReferencias = false;
        } else {
            $this->showReferencias = $this->userlogin->getUser()->getShowReferencias();
        }
        $organizacion = $this->userlogin->getOrganizacion();
        $mapa = new Map();
        $mapa->setDebug(true);
        $mapa->setIncludeJQuery(true);
        $mapa->setPanControl(false);
        //$mapa->disableDefaultUI(true);
        //$mapa->setMapTypeStyle(Map::STYLE_CONTROL_MAP_DROPDOWN_MENU);
        $mapa->setIniLatitud($organizacion->getLatitud());
        $mapa->setIniLongitud($organizacion->getLongitud());
        $mapa->setZoomPos(Map::POS_RIGHT_CENTER);
        $mapa->setMapTypes(array(Map::TYPE_HYBRID, Map::TYPE_ROADMAP, Map::TYPE_SATELLITE));
        switch ($this->userlogin->getUser()->getVistaMapa()) {
            case 1:
                $mapa->setMapType(Map::TYPE_HYBRID);
                break;
            case 2:
                $mapa->setMapType(Map::TYPE_ROADMAP);
                break;
            case 3:
                $mapa->setMapType(Map::TYPE_SATELLITE);
                break;
            default:
                $mapa->setMapType(Map::TYPE_ROADMAP);
                break;
        }

        $mapa = $this->setClusterable($mapa, $clusterado);

        return $mapa;
    }

    private $clusterReferencia = [
        'nombre' => 'referencia',
        'url' => 'https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m1.png',
        'width' => 56,
        'height' => 55
    ];

    private $clusterServicio = [
        'nombre' => 'servicio',
        'url' => 'https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m2.png',
        'width' => 56,
        'height' => 55
    ];

    public function setClusterable($mapa, $clusterado = false)
    {
        //cluster de iconos.
        if ($clusterado) {
            $mapa->setClusterMinSize(4);   //por defaul esta desactivado el cluster.
        } else {
            $mapa->setClusterMinSize(200000);   //por defaul esta desactivado el cluster.
        }
        $mapa->setClusterGrid(150);
        //icono de los cluster.      
 
        $ci2 = new ClusterIcon($this->clusterReferencia['url']);        
        $ci2->setTitle($this->clusterReferencia['nombre']);
        $ci2->setWidth($this->clusterReferencia['width']);
        $ci2->setHeight($this->clusterReferencia['height']);
   
        $mapa->setClusterStyles(array($ci2));
        return $mapa;
    }

    public function fitBounds(Map $mapa, $puntos)
    {
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        foreach ($puntos as $pto) {
            if ($min_lat === false) {
                $min_lat = $max_lat = floatval($pto['latitud']);
                $min_lng = $max_lng = floatval($pto['longitud']);
            } else {
                $min_lat = min($min_lat, floatval($pto['latitud']));
                $max_lat = max($max_lat, floatval($pto['latitud']));
                $min_lng = min($min_lng, floatval($pto['longitud']));
                $max_lng = min($max_lng, floatval($pto['longitud']));
            }
        }
        $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
        return true;
    }

    private $min_lat = false;
    private $max_lat = false;
    private $min_lng = false;
    private $max_lng = false;

    public function setFitBounds($mapa, $lat, $lon)
    {
        if ($this->min_lat === false) {
            $this->min_lat = $this->max_lat = $lat;
            $this->min_lng = $this->max_lng = $lon;
        } else {
            $this->min_lat = min($this->min_lat, $lat);
            $this->max_lat = max($this->max_lat, $lat);
            $this->min_lng = min($this->min_lng, $lon);
            $this->max_lng = max($this->max_lng, $lon);
        }
        $mapa->fitBounds($this->min_lat, $this->min_lng, $this->max_lat, $this->max_lng);
        return true;
    }

    public function createPosicionIcon(Map $mapa, $nombre, $iconname)
    {
        $posicion = new Icon($nombre, $this->getBundlePath() . 'images/' . $iconname);
        $mapa->addIcon($posicion);

        return $posicion;
    }

    public function getDataIconoFlecha($direccion = null, $velocidad = 0)
    {
        $icono = array();
        if (!$velocidad) {
            $icono['icon_url'] = $this->getIcono(-1, 0);
        } else {
            $icono['icon_url'] = $this->getIcono($direccion, $velocidad);
        }
        $icono['icon_size_width'] = 24;
        $icono['icon_size_height'] = 24;
        $icono['icon_origin_x'] = 0;
        $icono['icon_origin_y'] = 0;
        $icono['icon_anchor_x'] = 12;
        $icono['icon_anchor_y'] = 12;
        return $icono;
    }

    public function addMarkerServicio(Map $mapa, Servicio $servicio, $visible, $labelClass = 'servicio')
    {
        if (!$servicio->getUltVelocidad()) {
            $url_icono = $this->getIcono(-1, 0);
        } else {
            $url_icono = $this->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad());
        }

        $icono = new Icon('servicio_' . $servicio->getId() . '_icono', $url_icono);
        $icono->setImageSize(24, 24);
        $icono->setImageAnchor(12, 12);
        $mapa->addIcon($icono);

        $marker = new Marker('servicio_' . $servicio->getId(), $servicio->getUltLatitud(), $servicio->getUltLongitud(), $servicio->getNombre(), $icono);
        //        $marker->setLabel($servicio->getNombre(), 'servicio' . $servicio->getColor(), 2);
        if (!$servicio->getIsCustodio()) {
            $marker->setLabel($servicio->getNombre(), 'servicio', 2);
        } else {
            $marker->setLabel($servicio->getNombre() . '-CUSTODIO', 'servicio', 2);
        }
        $marker->setLabelClase($labelClass);

        $marker->setClickable(true);
        $marker->setClickListener('showInfoServicio');
        $marker->setClusterable(false);        
        $marker->setVisible($visible);        
        $mapa->addMarker($marker);
        return $marker;
    }

    //este marker se usa para los historiales y recorridos de un equipo.
    public function addMarkerFlecha(Map $mapa, $id, $posicion, $visible)
    {
        if (is_null($posicion)) {
            $posicion = array(
                'direccion' => 0,
                'velocidad' => 0,
                'latitud' => 0,
                'longitud' => 0
            );
        }
        $dataIconoFlecha = $this->getDataIconoFlecha($posicion['direccion'], $posicion['velocidad']);
        $icono = new Icon('posicion_' . $id . '_icono', $dataIconoFlecha['icon_url']);
        $icono->setImageSize($dataIconoFlecha['icon_size_width'], $dataIconoFlecha['icon_size_height']);
        $icono->setImageAnchor($dataIconoFlecha['icon_anchor_x'], $dataIconoFlecha['icon_anchor_y']);

        $mapa->addIcon($icono);
        $marker = new Marker('posicion_' . $id, $posicion['latitud'], $posicion['longitud'], $id, $icono);
        $marker->setVisible($visible);
        $marker->setClickable(true);
        $marker->setClickListener('showInfoServicio');
        $mapa->addMarker($marker);
        return $marker;
    }

    public function addMarkerPosicion(Map $mapa, $id, $posicion, $icono, $label = null)
    {
        if (is_null($posicion)) {
            $market_punto = new Marker('punto_' . $id, 0, 0, $id, $icono);
            $market_punto->setVisible(false);
        } else {
            $market_punto = new Marker('punto_' . $id, $posicion['latitud'], $posicion['longitud'], $id, $icono);
            $market_punto->setVisible(true);
        }
        if (!is_null($label)) {
            $market_punto->setLabel($label, 'servicio', 2);
            $market_punto->setTitle($label);
        }
        $market_punto->setClickable(true);
        $market_punto->setClusterable(false);

        $mapa->addMarker($market_punto);
        return $market_punto;
    }

    public function addPolyline(Map $mapa, $nombre, $puntos)
    {
        $polilinea = new Polyline($nombre, $puntos);
        $polilinea->setStrokeColor('#0178d3');
        $polilinea->setStrokeWeight(3);
        $polilinea->setStrokeOpacity(0.8);
        $mapa->addPolyline($polilinea);
    }

    public function addRecorrido(Map $mapa, $nombre, $puntos, $color)
    {
        $polilinea = new Polyline($nombre, $puntos);
        $polilinea->setStrokeColor($color);
        $polilinea->setStrokeWeight(2);
        $polilinea->setStrokeOpacity(0.8);
        $mapa->addPolyline($polilinea);
    }

    public function addRecorridoWithStrokeWeight(Map $mapa, $nombre, $puntos, $color, $strokeWeight)
    {
        $polilinea = new Polyline($nombre, $puntos);
        $polilinea->setStrokeColor($color);
        $polilinea->setStrokeWeight($strokeWeight);
        $polilinea->setStrokeOpacity(0.8);
        $mapa->addPolyline($polilinea);
    }

    public function addCamino(Map $mapa, $nombre, $puntos, $peso)
    {
        foreach ($puntos as $pto) {
            $path[] = array($pto['latitud'], $pto['longitud']);
        }
        //agrego la polilinea
        $polilinea = new Polyline($nombre, $path);
        if ($peso == 0) {
            $polilinea->setStrokeColor('lime');
            $polilinea->setStrokeWeight(5);
        } else {
            $polilinea->setStrokeColor('#00AAD3');
            $polilinea->setStrokeWeight(2);
        }
        $polilinea->setStrokeOpacity(0.8);
        $mapa->addPolyline($polilinea);
        return $polilinea;
    }

    public function parsePoligono($referencia)
    {
        if ($referencia->isOldPoligon()) {
            $poli = $this->parseOldPoligono($referencia->getPoligono());
        } else {
            $poli = $this->parseNewPoligono($referencia);
        }
        return $poli;
    }

    private function parseOldPoligono($poligono)
    {
        $result = array();
        $arrayPoli = explode(',', $poligono);
        $i = 0;
        $result['linea_grosor'] = $arrayPoli[$i];
        //parametros de linea.
        if (count($arrayPoli/* [$i++] */) > 0) {
            $result['linea_color'] = substr($arrayPoli[$i++], 3, 6);
            $result['linea_transparencia'] = substr($arrayPoli[$i], 1, 2);
        } else {
            $result['linea_color'] = '#000000';
            $result['linea_transparencia'] = '0.7';
        }

        //parametros del color
        $result['relleno_color'] = substr($arrayPoli[$i++], 3, 6);
        $result['relleno_transparencia'] = 0.5;

        $minlat = $minlon = null;
        $maxlat = $maxlon = null;
        $puntos = array();

        for ($i = 3; $i < count($arrayPoli) - 1; $i = $i + 2) {
            $latlon = array($arrayPoli[$i], $arrayPoli[$i + 1]);
            $puntos[] = $latlon;

            if ($minlat == null) {
                $minlat = $maxlat = $latlon[0];
                $minlon = $maxlon = $latlon[1];
            } else {
                if ($latlon[0] < $minlat)
                    $minlat = $latlon[0];
                if ($latlon[1] < $minlon)
                    $minlon = $latlon[1];
                if ($latlon[0] > $maxlat)
                    $maxlat = $latlon[0];
                if ($latlon[1] > $maxlon)
                    $maxlon = $latlon[1];
            }
        }        
        $result['puntos'] = $puntos;
        $result['latitud'] = $minlat * 0.5 + $maxlat * 0.5;
        $result['longitud'] = $minlon * 0.5 + $maxlon * 0.5;
        return $result;
    }

    private function parseNewPoligono($referencia)
    {        
        $result = array();
        $arrayPoli = explode(',', $referencia->getPoligono());
        $i = 0;
        $result['linea_grosor'] = 5;   //queda fijo para siempre.
        //parametros de linea.
        $result['linea_color'] = $referencia->getColor();
        $result['linea_transparencia'] = $referencia->getTransparencia() + 0.2;

        //parametros del color
        $result['relleno_color'] = $referencia->getColor();
        $result['relleno_transparencia'] = $referencia->getTransparencia();

        $minlat = $minlon = null;
        $maxlat = $maxlon = null;
        $puntos = array();
       // die('////<pre>'.nl2br(var_export($arrayPoli, true)).'</pre>////');
        for ($i = 0; $i < count($arrayPoli) - 1; $i = $i + 2) {
            $latlon = array($arrayPoli[$i], $arrayPoli[$i + 1]);
            $puntos[] = $latlon;

            if ($minlat == null) {
                $minlat = $maxlat = $latlon[0];
                $minlon = $maxlon = $latlon[1];
            } else {
                if ($latlon[0] < $minlat)
                    $minlat = $latlon[0];
                if ($latlon[1] < $minlon)
                    $minlon = $latlon[1];
                if ($latlon[0] > $maxlat)
                    $maxlat = $latlon[0];
                if ($latlon[1] > $maxlon)
                    $maxlon = $latlon[1];
            }
        }      
       
        $result['puntos'] = $puntos;
        $result['latitud'] = $minlat * 0.5 + $maxlat * 0.5;
        $result['longitud'] = $minlon * 0.5 + $maxlon * 0.5;
        return $result;
    }

    public function getDataReferencia($referencia)
    {
        $polygon = $circle = array();
        if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
            $polygon['id_sc'] = 'p' . $referencia->getId();

            //puntos del poligono
            $poli = $this->parsePoligono($referencia);
            $polygon['puntos'] = $poli['puntos'];

            //relleno
            $polygon['fillColor'] = $poli['relleno_color'];
            $polygon['fillOpacity'] = floatval($poli['relleno_transparencia']);

            //linea
            $polygon['strokeColor'] = $poli['linea_color'];
            $polygon['strokeWeight'] = $poli['linea_grosor'];
            $polygon['strokeOpacity'] = $poli['linea_transparencia'];
            $polygon['visible'] = $referencia->getVisibilidad();
        } else {
            //es un circulo.
            $circle = array(
                'id_sc' => 'c' . $referencia->getId(),
                'latitud' => floatval($referencia->getLatitud()),
                'longitud' => floatval($referencia->getLongitud()),
                'radius' => intval($referencia->getRadio()),
                'strokeWeight' => 5,
                'strokeColor' => $referencia->getColor(),
                'strokeOpacity' => $referencia->getTransparencia() + 0.2, //es para remarcarla mas...
                'fillColor' => $referencia->getColor(),
                'fillOpacity' => floatval($referencia->getTransparencia()),
                'visible' => $referencia->getVisibilidad(),
            );
        }

        $icono = array(
            'id_sc' => 'i' . $referencia->getId(),
            'icon_url' => $referencia->getPathIcono() ? $referencia->getPathIcono() : $this->bundle_path . 'images/ball.png',
            //'icon_url' => $referencia->getPathIcono() ? $this->bundle_path . $referencia->getPathIcono() : $this->bundle_path . 'images/ball.png',
        );

        $newReferencia = array(
            'id_sc' => 'r' . $referencia->getId(),
            'latitud' => $referencia->getLatitud(),
            'longitud' => $referencia->getLongitud(),
            'icono' => 'i' . $referencia->getId(),
            'title' => $referencia->getNombre(),
            'label_texto' => $referencia->getNombre(),
            'label_clase' => 'referencia',
            'label_borde' => 2,
            'clusterable' => true,
            'visible' => $referencia->getVisibilidad(),
            'clickable' => true,
            'listener_click_me' => 'showInfoReferencia',
            'informacion' => $this->getInfoReferencia($referencia),
        );

        return array(
            'icono' => $icono,
            'polygon' => $polygon,
            'circle' => $circle,
            'referencia' => $newReferencia,
        );
    }
    //en referencia enviar entity referencia o referenciagpm
    public function addMarkerReferencia(Map $mapa, $referencia, $visible = true, $ver_info = false, $clusterable = true, $transparencia = null)
    {
        if ($mapa->existsMarker('r' . $referencia->getId())) {
            return null;
        }

        if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
            $poli = $this->parsePoligono($referencia);
            $poligon = new Polygon('p' . $referencia->getId(), $poli['puntos']);

            //relleno
            $poligon->setFillColor($poli['relleno_color']);
            if (is_null($transparencia)) {
                $poligon->setFillOpacity(floatval($poli['relleno_transparencia']));
            } else {
                $poligon->setFillOpacity(floatval($transparencia));
            }

            //linea
            $poligon->setStrokeColor($poli['linea_color']);
            $poligon->setStrokeWeight($poli['linea_grosor']);
            $poligon->setStrokeOpacity($poli['linea_transparencia']);
            $poligon->setVisible($visible && $referencia->getVisibilidad());
            $mapa->addPolygon($poligon);
        } else {
            //es un circulo.
            $circ = new Circle('c' . $referencia->getId(), $referencia->getLatitud(), $referencia->getLongitud());

            $circ->setRadius($referencia->getRadio());
            $circ->setStrokeWeight(5);

            //seteo de la linea
            $circ->setStrokeColor($referencia->getColor());
            $circ->setStrokeOpacity($referencia->getTransparencia() + 0.2); //es para remarcarla mas...
            //seteo del relleno
            $circ->setFillColor($referencia->getColor());
            $circ->setFillOpacity($referencia->getTipoReferencia());

            $circ->setVisible($visible && $referencia->getVisibilidad());
            $mapa->addCircle($circ);
        }
        $mapa->setLabelMaxLongitud(60);  //maxima longitud de un label
        $mapa->setLabelWrapLongitud(30); //maxima longitud de una linea, se introduce un salto si se pasa este valor

        if ($referencia->getPathIcono()) {
            $icono = new Icon('i' . $referencia->getId(), $referencia->getPathIcono());
        } else {
            $icono = new Icon('i' . $referencia->getId(), $this->bundle_path . 'images/ball.png');
        }

        $mapa->addIcon($icono);
        $marker = new Marker('r' . $referencia->getId(), $referencia->getLatitud(), $referencia->getLongitud(), $referencia->getNombre(), $icono);
        $marker->setLabel($referencia->getNombre(), 'referencia', 2);
        $marker->setVisible($visible);
        $marker->setVisibleLeyenda($visible);
        $marker->setVisibleMarcador($visible);
        $marker->setClickListener('showInfoReferencia');
        $marker->setClusterable($clusterable);
        if ($ver_info) {
            $marker->setInformacion($this->getInfoReferencia($referencia));
        }
        $mapa->addMarker($marker);
        return $marker;
    }

    private function getInfoReferencia($referencia)
    {
        $html = '<h4>' . $referencia->getNombre() . '</h4>';
        $html .= 'Organización: <b>' . $referencia->getPropietario()->getNombre() . '</b></br>';
        $html .= 'Clase: <b>' . $referencia->getStrClase() . '</b></br>';
        if ($referencia->getClase() == $referencia::CLASE_RADIAL) {
            $html .= 'Radio: <b>' . $referencia->getRadio() . ' mts.</b></br>';
        }
        $html .= 'Categoria: <b>' . $referencia->getCategoria() . '</b></br>';
        if (!is_null($referencia->getTipoReferencia())) {
            $html .= 'Tipo Referencia: <b>' . $referencia->getTipoReferencia() . '</b></br>';
        }
        if (!is_null($referencia->getStatus())) {
            $html .= 'Estado: <b>' . $referencia->getStrStatus() . '</b></br>';
        } else {
            $html .= 'Estado: <b>' . 'No Checkeada' . '</b></br>';
        }
        $html .= 'Lat/Lon: <b>' . $referencia->getLatitud() . ', ' . $referencia->getlongitud() . '</b></br>';
        if ($referencia->getArea() != null || $referencia->getArea() != 0) {
            $html .= sprintf('Area: <b>%10.2f m2 (%10.2f ha)</b></br>', $referencia->getArea(), $referencia->getArea() / 10000);
        }
        $parm = array(
            $referencia->getId(),
            $referencia->getLatitud(),
            $referencia->getLongitud(),
        );
        $redibujar = true;
        $path_delete = false;
        //tengo que redirigir a apmon o sauron segun corresponda al usuario logueado.
        if ($this->userlogin->isGranted('ROLE_REFERENCIA_EDITAR') && $referencia instanceof Referencia) {
            if ($this->userlogin->getOrganizacion()->getTipoOrganizacion() == 1) {
                $redibujar = true;
                $path_edit = $this->router->generate('referencia_edit', array('id' => $referencia->getId()));
            } else {
                if ($this->userlogin->getOrganizacion() == $referencia->getPropietario()) {
                    $redibujar = true;
                    $path_edit = $this->router->generate('referencia_edit', array('id' => $referencia->getId()));
                }
            }
        } else {
            $path_delete = true;
        }
        if (isset($redibujar) && $redibujar) {
            $html .= '<a href="#" onclick="redibujarReferencia(' . implode(',', $parm) . ')" title="Permite cambiar el tamaño del dibujo, color y transparencia."><i class="icon-pencil"></i>Redibujar</a>&nbsp';
        }
        if (isset($path_edit)) {
            $html .= '<a href="' . $path_edit . '" title="Cambia los datos de la referencia asociada." ><i class="icon-map-marker"></i>Editar</a>';
        }
        if ($path_delete) {
            $html .= '<a href="#" onclick="removeReferencia(' . $referencia->getId() . ',' . $referencia->getUnica() . ',null)">' . 'Eliminar</a>';
        }
        return $html;
    }

    public function castVisibilidadReferencias($referencias, $visible)
    {
        $icon = $polygon = $circle = $ref = array();
        foreach ($referencias as $referencia) {
            $icon[] = array(
                'id_sc' => 'i' . $referencia->getId(),
                'visible' => $visible && $referencia->getVisibilidad(),
            );
            $ref[] = array(
                'id_sc' => 'r' . $referencia->getId(),
                'visible' => $visible && $referencia->getVisibilidad(),
            );
            if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
                $polygon[] = array(
                    'id_sc' => 'p' . $referencia->getId(),
                    'visible' => $visible && $referencia->getVisibilidad(),
                );
            } else {
                //es para los circulos
                $circle[] = array(
                    'id_sc' => 'c' . $referencia->getId(),
                    'visible' => $visible && $referencia->getVisibilidad(),
                );
            }
        }
        return array(
            'icon' => $icon,
            'polygon' => $polygon,
            'circle' => $circle,
            'referencia' => $ref,
        );
    }

    public function castReferencias($referencias)
    {
        $visible = true;
        $icon = $polygon = $circle = $ref = array();
        foreach ($referencias as $referencia) {
            $data = $this->getDataReferencia($referencia);
            $icon[] = $data['icono'];
            if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
                $polygon[] = $data['polygon'];
            } else {
                $circle[] = $data['circle'];
            }
            $ref[] = $data['referencia'];
        }
        return array(
            'icon' => $icon,
            'polygon' => $polygon,
            'circle' => $circle,
            'referencia' => $ref,
        );
    }

    public function getDomicilio($lat, $lon, $organizacion = null)
    {
        $geocoder = new Geocoder();
        $request = $geocoder->inverso($lat, $lon);
        //$request = null;
        if (isset($request['exito']) && $request['exito'] == true && $request['direccion'] != null) {
            return $this->utils->simplexml2array($request['direccion']);
        } else {
            return '---';
        }
    }
}
