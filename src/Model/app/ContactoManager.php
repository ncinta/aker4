<?php

/**
 * ContactoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contacto;
use App\Entity\Empresa;
use App\Entity\Logistica;
use App\Entity\Transporte;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UsuarioManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ContactoManager
{

    protected $em;
    protected $security;
    protected $contacto;
    protected $usuarioManager;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        UsuarioManager $usuario
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->usuarioManager = $usuario;
    }

    /**
     * Buscan un contacto por su id u objeto.
     * @param Contacto|Int $contacto   el Contacto o el id del contacto
     * @return type 
     */
    public function find($contacto)
    {
        if ($contacto instanceof Contacto) {
            return $this->em->getRepository('App:Contacto')->find($contacto->getId());
        } else {
            return $this->em->getRepository('App:Contacto')->find($contacto);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Contacto')->find($id);
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Contacto')
            ->findBy(array('organizacion' => $organizacion->getId()));
    }

    public function getAllTipoNotif()
    {
        return $this->em->getRepository('App:TipoNotificacion')
            ->findAll();
    }

    public function create(Organizacion $propietario)
    {
        $this->contacto = new Contacto();
        $this->contacto->setOrganizacion($propietario);
        return $this->contacto;
    }

    public function findByPhone($phone, $organizacion)
    {
        return $this->em->getRepository('App:Contacto')->findOneBy(
            array(
                'celular' => $phone,
                'organizacion' => $organizacion
            )
        );
    }

    public function findByToken($token, $organizacion)
    {
        return $this->em->getRepository('App:Contacto')
            ->findOneBy(array('token' => $token, 'organizacion' => $organizacion));
    }

    public function findByEmail($email, $organizacion)
    {
        return $this->em->getRepository('App:Contacto')
            ->findOneBy(array('email' => $email, 'organizacion' => $organizacion));
    }

    public function setData($contacto, $data)
    {

        $contacto->setNombre($data['nombre']);
        $contacto->setCelular($data['celular']);
        $contacto->setEmail($data['email']);
        $contacto->setCargo($data['cargo']);
        $contacto->removeTipoNotificaciones();
        if (isset($data['tipoNotificaciones'])) {
            foreach ($data['tipoNotificaciones'] as $tipo) {
                //   die('////<pre>' . nl2br(var_export($tipo, true)) . '</pre>////');
                $tipoNotif = $this->em->getRepository('App:TipoNotificacion')->findOneBy(array('id' => intval($tipo)));
                $contacto->addTipoNotificaciones($tipoNotif);
            }
        }
        $this->contacto = $contacto;

        return $this->contacto;
    }


    public function save($contacto)
    {
        if (!is_null($contacto)) $this->contacto = $contacto;
        $this->em->persist($this->contacto);
        $this->em->flush();
        return $this->contacto;
    }

    public function delete($contacto)
    {
        $this->em->remove($this->find($contacto));
        $this->em->flush();
        return true;
    }

    public function toArrayData($contacto, $action)
    {
        $data = array(
            'entity' => 'CONTACTO',
            'action' => null,
            'data' => null
        );
        if ($contacto) {
            $data['data']['id'] = $contacto instanceof Contacto ? $contacto->getId() : $contacto;
            $data['data']['updated_at'] =  $contacto instanceof Contacto ? $contacto->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $contacto instanceof Contacto ? $contacto->getCreatedAt()->format('Y-m-d H:i:s') : null;
            $data['action'] = 'UPDATE';
            if ($action == 2) {
                $data['action'] = 'DELETE';
            }

            $data['data']['nombre'] = $contacto->getNombre();

            foreach ($contacto->getTipoNotificaciones() as $tipo) {
                if ($tipo->getCodename() == 'NOTIF_APP') {
                    $data['data']['celular'] = $contacto->getCelular();
                    if ($contacto->getCelular() != null) {  //tiene celular                
                        $usr = $this->usuarioManager->findByPhone($contacto->getCelular(), $contacto->getOrganizacion());
                        if ($usr && $usr->getToken() != null) {
                            $data['data']['token'] = $usr->getToken();
                        }
                    }
                } elseif ($tipo->getCodename() == 'NOTIF_EMAIL') {
                    $data['data']['email'] = $contacto->getEmail();
                } else {
                    // si no viene nada o si no viene ninguno de esos codename, que notifique por mail
                    $data['data']['email'] = $contacto->getEmail();
                }
            }
            //dd($data);
        }

        return $data;
    }
}
