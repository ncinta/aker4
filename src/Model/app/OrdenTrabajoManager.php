<?php

/**
 * OrdenTrabajoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\OrdenTrabajo;
use App\Entity\OrdenTrabajoProducto;
use App\Entity\OrdenTrabajoMecanico;
use App\Entity\OrdenTrabajoMantenimiento;
use App\Entity\OrdenTrabajoExterno;
use App\Entity\Organizacion as Organizacion;
use App\Entity\OrdenTrabajoHoraTaller;
use App\Event\OrdenTrabajoEvent;
use App\Event\ServicioEvent;
use App\Model\app\UserLoginManager;
use App\Model\crm\PeticionManager;
use App\Model\app\TareaMantManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrdenTrabajoManager
{

    protected $em;
    protected $userlogin;
    protected $utils;
    protected $orden;
    protected $tareamant;
    protected $peticion;
    protected $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        UserLoginManager $userlogin,
        TareaMantManager $tareaMant,
        PeticionManager $peticion,
        EventDispatcherInterface $eventDispatcher,
    ) {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->tareamant = $tareaMant;
        $this->peticion = $peticion;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Buscan un chofer por su id u objeto.
     * @param OrdenTrabajo|Int $orden   el OrdenTrabajo o el id del chofer
     * @return type 
     */
    public function find($orden)
    {
        if ($orden instanceof OrdenTrabajo) {
            return $this->em->getRepository('App:OrdenTrabajo')->find($orden->getId());
        } else {
            return $this->em->getRepository('App:OrdenTrabajo')->find($orden);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:OrdenTrabajo')->find($id);
    }

    public function findAll($organizacion, $orderBy = array(), $desde = null, $hasta = null)
    {
        return $this->em
            ->getRepository('App:OrdenTrabajo')
            ->findByOrganizacion($organizacion, $orderBy, $desde, $hasta);
    }

    public function create(Organizacion $organizacion)
    {
        $this->orden = new OrdenTrabajo();
        $this->orden->setOrganizacion($organizacion);
        return $this->orden;
    }

    public function save($orden)
    {
        if (is_null($this->orden)) {
            $this->orden = $orden;
        }

        $tz = new \DateTimeZone($this->userlogin->getTimezone());
        $t = null;
        $ts = null;
        $d = null;
        $c = null;
        if ($orden->getFecha()) {
            if ($orden->getFecha() instanceof \DateTime) {
                $f = new \DateTime($orden->getFecha()->format('Y-m-d'), $tz);
            } else {
                $f = new \DateTime($orden->getFecha(), $tz);
            }
            $this->orden->setFecha($f);
        }

        if ($orden->getFechaIngreso()) {
            if ($orden->getFechaIngreso() instanceof \DateTime) {
                $f = new \DateTime($orden->getFechaIngreso()->format('Y-m-d'), $tz);
            } else {
                $f = new \DateTime($orden->getFechaIngreso(), $tz);
            }
            $this->orden->setFechaIngreso($f);
        }

        if ($orden->getFechaEgreso()) {
            if ($orden->getFechaEgreso() instanceof \DateTime) {
                $f = new \DateTime($orden->getFechaEgreso()->format('Y-m-d'), $tz);
            } else {
                $f = new \DateTime($orden->getFechaEgreso(), $tz);
            }
            $this->orden->setFechaEgreso($f);
        }

        if ($orden->getTaller()) {
            if ($orden->getTaller() instanceof \App\Entity\Taller) {
                $t = $orden->getTaller();
            } else {
                $t = $this->em->getRepository('App:Taller')->findOneBy(array('id' => intval($orden->getTaller())));
            }
        }
        $this->orden->setTaller($t);

        if ($orden->getCentroCosto()) {
            if ($orden->getCentroCosto() instanceof \App\Entity\CentroCosto) {
                $c = $orden->getCentroCosto();
            } else {
                $c = $this->em->getRepository('App:CentroCosto')->findOneBy(array('id' => intval($orden->getCentroCosto())));
            }
            $this->orden->setCentroCosto($c);
        }

        if ($orden->getTallerSector()) {
            if ($orden->getTallerSector() instanceof \App\Entity\TallerSector) {
                $ts = $orden->getTallerSector();
            } else {
                $ts = $this->em->getRepository('App:TallerSector')->findOneBy(array('id' => intval($orden->getTallerSector())));
            }
            $this->orden->setTallerSector($ts);
        }

        if ($orden->getDeposito()) {
            if ($orden->getDeposito() instanceof \App\Entity\Deposito) {
                $d = $orden->getDeposito();
            } else {
                $d = $this->em->getRepository('App:Deposito')->findOneBy(array('id' => intval($orden->getDeposito())));
            }
            $this->orden->setDeposito($d);
        }
        if (is_null($orden->getIdentificador())) {
            $ID = $this->em->getRepository('App:OrdenTrabajo')->getMaxIdentificador($this->userlogin->getOrganizacion());
            $this->orden->setIdentificador(array_pop($ID) + 1);
        }

        $this->orden->setOdometro($orden->getOdometro());
        $this->orden->setHorometro($orden->getHorometro());
        $this->orden->setNumeroExterno($orden->getNumeroExterno());
        $this->orden->setNumeroInterno($orden->getNumeroInterno());

        $s = $this->em->getRepository('App:Servicio')->find($orden->getServicio());
        $this->orden->setServicio($s);

        $this->orden->setEjecutor($this->userlogin->getUser());
        $this->orden->setOrganizacion($s->getOrganizacion());    //grabo la organizacion
        
        $eventOt = new OrdenTrabajoEvent($s,$this->orden);
        if($this->orden->getEstado() == 0 || $this->orden->getEstado() == null){
            if($this->orden->getId() == null){
                $this->eventDispatcher->dispatch($eventOt, OrdenTrabajoEvent::OT_NEW);
            }else{
                $this->eventDispatcher->dispatch($eventOt, OrdenTrabajoEvent::OT_EDIT);
            }
        }else if($this->orden->getEstado() == 2){
            $this->eventDispatcher->dispatch($eventOt, OrdenTrabajoEvent::OT_CLOSE);
        }else{ 
            
        }
       
        $this->em->persist($this->orden);
        $this->em->flush();

        return $this->orden;
    }
    
    public function delete($orden)
    {
        $eventOt = new OrdenTrabajoEvent($orden->getServicio(),$orden);
        $this->eventDispatcher->dispatch($eventOt, OrdenTrabajoEvent::OT_DELETE);
        $this->em->remove($this->find($orden));
        $this->em->flush();
        return true;
    }

    public function delProducto($orden, $producto)
    {
        $this->em->remove($producto);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function delHoraTaller($orden, $hora)
    {
        $this->em->remove($hora);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function findProdOT($idOTP)
    {
        return $this->em->getRepository('App:OrdenTrabajoProducto')->findOneBy(array('id' => intval($idOTP)));
    }

    public function findHoraTallerOT($idOTHT)
    {
        return $this->em->getRepository('App:OrdenTrabajoHoraTaller')->findOneBy(array('id' => intval($idOTHT)));
    }

    public function addProducto($orden, $idProd, $cant, $precio = 0, $actPrecio = false)
    {
        $producto = $this->em->getRepository('App:Producto')->findOneBy(array(
            'id' => intval($idProd)
        ));
        $stock = $this->em->getRepository('App:Stock')->findByDepositoProducto($orden->getDeposito(), $producto);

        $unitario = $totalNeto = 0;
        if ($stock->getCosto() && $actPrecio) {
            if ($precio != 0) {  //deberia actualizar el precio
                $unitario = $precio;   //tomo el precio q viene del form
            } else {
                $unitario = $stock->getCosto(); //tomo el precio del stock
            }

            //si tengo el check de actualizar precio debo cambiar el stok.
            $stock->setCosto($precio);
            $stock->setUltCompra($orden->getFecha());
            $this->em->persist($stock);
        } else {
            $unitario = $precio;   //tomo el precio q viene del form
        }
        $totalNeto = $unitario * $cant;   //actualizo el total

        $otp = new OrdenTrabajoProducto();
        $otp->setCantidad($cant);
        $otp->setCostoUnitario($unitario);
        $otp->setCostoNeto($totalNeto);
        if (!is_null($stock->getProducto()->getTasaIva()) || $stock->getProducto()->getTasaIva() != 0) {
            $otp->setCostoTotal($totalNeto * (1 + $stock->getProducto()->getTasaIva() / 100));
        } else {
            $otp->setCostoTotal($totalNeto * 1.21);   //tasa de iva por default
        }
        $otp->setDeposito($orden->getDeposito());
        $otp->setProducto($producto);
        $otp->setOrdenesTrabajo($orden);
        $orden->addProductos($otp);
        $producto->addOrdenTrabajo($otp);

        $this->em->persist($otp);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function addHoraTaller($orden, $idHora, $cant, $precio = 0, $actPrecio = false)
    {
        $hora = $this->em->getRepository('App:HoraTaller')->findOneBy(array(
            'id' => intval($idHora)
        ));
        $totalNeto = $precio * $cant;
        $otp = new OrdenTrabajoHoraTaller();
        $otp->setCantidad($cant);
        $otp->setCostoUnitario($precio);
        $otp->setHoraTaller($hora);
        $otp->setCostoNeto($totalNeto);
        $otp->setCostoTotal($totalNeto * 1.21);
        $otp->setOrdenesTrabajo($orden);
        $orden->addHorasTaller($otp);
        $hora->addOrdenTrabajo($otp);

        $this->em->persist($otp);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function recalcularCostoOrden($orden)
    {
        $costoNeto = $costoP = $costoM = $costoTE = $costoHT = $costoNHT = $costoTT = $costoNT = 0;
        foreach ($orden->getProductos() as $prod) {
            $costoNeto += $prod->getCostoNeto() ? $prod->getCostoNeto() : 0;
            $costoP += $prod->getCostoTotal() ? $prod->getCostoTotal() : 0;
        }
        foreach ($orden->getMecanicos() as $meca) {
            $costoM += $meca->getCostoTotal() ? $meca->getCostoTotal() : 0;
        }

        foreach ($orden->getExternos() as $texterno) {
            $costoTE += $texterno->getCostoTotal() ? $texterno->getCostoTotal() : 0;
        }

        foreach ($orden->getHorasTaller() as $hora) {
            $costoHT += $hora->getCostoTotal() ? $hora->getCostoTotal() : 0;
            $costoNHT += $hora->getCostoNeto() ? $hora->getCostoNeto() : 0;
        }


        $orden->setCostoNeto($costoNeto + $costoM + $costoTE + $costoNHT + $costoNT);
        $orden->setCostoTotal($costoP + $costoM + $costoTE + $costoHT + $costoTT);
        $this->em->persist($orden);
        $this->em->flush();
    }

    public function addMecanico($orden, $idMeca, $cant, $precio = 0, $actPrecio = false)
    {
        $mecanico = $this->em->getRepository('App:Mecanico')->findOneById(array(
            'id' => intval($idMeca)
        ));

        if ($mecanico) {

            $unitario = $total = 0;
            if ($precio != 0) {  //deberia actualizar el precio
                $unitario = $precio;   //tomo el precio q viene del form
            } else {
                $unitario = $mecanico->getCostoHora(); //tomo el precio del mecanico
            }
            $total = $unitario * $cant;


            $tpm = new OrdenTrabajoMecanico();
            $tpm->setHoras($cant);
            $tpm->setCosto($unitario);
            $tpm->setCostoTotal($total);
            $tpm->setMecanico($mecanico);
            $tpm->setOrdenTrabajo($orden);
            $orden->addMecanicos($tpm);
            $mecanico->addTrabajos($tpm);

            $this->em->persist($tpm);
            $this->em->flush();
            $this->recalcularCostoOrden($orden);
            return true;
        } else {
            return false;
        }
    }

    public function delMecanico($orden, $idMeca)
    {
        $mecanico = $this->em->getRepository('App:OrdenTrabajoMecanico')->findOneBy(array(
            'id' => intval($idMeca)
        ));
        $this->em->remove($mecanico);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function addMantenimiento($orden, $tarea, $tipoMovimiento)
    {
        $otm = new OrdenTrabajoMantenimiento();
        $otm->setOrdenTrabajo($orden);
        $otm->setServicioMantenimiento($tarea);
        $orden->addMantenimiento($otm);
        $tarea->addOrdenTrabajo($otm);

        $this->em->persist($otm);
        $this->em->flush();

        //armo la data necesaria para registrar la tarea de mantenimiento
        $data = array(
            'observacion' => sprintf('%s OT: %d, %s', $tipoMovimiento, $orden->getId(), $orden->getDescripcion()),
            'costo' => $orden->getCostoTotal(),
            'fecha' => is_null($orden->getFechaEgreso()) ? $orden->getFecha()->format('d/m/Y') : $orden->getFechaEgreso()->format('d/m/Y'),
            'deposito' => is_null($orden->getDeposito()) ? null : $orden->getDeposito()->getId(),
            'taller' => is_null($orden->getTaller()) ? null : $orden->getTaller()->getId(),
        );

        if ($tarea->getMantenimiento()->getPorKilometros()) {
            $data['dato_realizacion'] = is_null($orden->getOdometro()) ? $orden->getServicio()->getUltOdometro() : $orden->getOdometro();
            $data['dato_proximo'] = $data['dato_realizacion'] + $tarea->getIntervaloKilometros();
        } elseif ($tarea->getMantenimiento()->getPorHoras()) {
            if (is_null($orden->getHorometro())) {
                $data['dato_realizacion'] = is_null($orden->getHorometro()) ? intval(($orden->getServicio()->getUltHorometro()) / 60) : $orden->getHorometro();
            } else {
                $data['dato_realizacion'] = $orden->getHorometro();
            }
            if (is_null($tarea->getProximoHoras())) {
                $data['dato_proximo'] = intval($tarea->getIntervaloHoras()) > 0 ? $data['dato_realizacion'] + $tarea->getIntervaloHoras() : $tarea->getProximoHoras();
            } else {
                $data['dato_proximo'] = $tarea->getProximoHoras();
            }
        } elseif ($tarea->getMantenimiento()->getPorDias()) {
            $data['dato_realizacion'] = $orden->getFecha();
            $proxVto = $orden->getFecha();
            $proxVto->add(new \DateInterval('P' . $tarea->getIntervaloDias() . 'D'));
            $data['dato_proximo'] = $proxVto;
            $data['dato_proxima_fecha'] = $proxVto->format('d-m-Y');
        }
       // dd($data);
        $this->tareamant->registrarTarea($tarea, $data);

        return true;
    }

    public function delMantenimiento($orden, $idMante)
    {
        $mante = $this->em->getRepository('App:OrdenTrabajoMantenimiento')->findOneBy(array(
            'id' => intval($idMante)
        ));
        $this->em->remove($mante);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function historico($organizacion, $desde, $hasta, $servicio, $tipo, $sector, $deposito, $centrocosto, $taller)
    {
        return $this->em->getRepository('App:OrdenTrabajo')->historico($organizacion, $desde, $hasta, $servicio, $tipo, $sector, $deposito, $centrocosto, $taller);
    }

    public function findByServicio($servicio)
    {
        return $this->em->getRepository('App:OrdenTrabajo')->byServicio($servicio);
    }

    public function findByMecanico($desde, $hasta, $mecanico)
    {
        return $this->em->getRepository('App:OrdenTrabajoMecanico')->byMecanico($desde, $hasta, $mecanico);
    }

    public function addTallerExternoProducto($orden, $idTaller, $cantidad = null, $descripcion = null, $total = null, $idProducto = null)
    {
        $taller = $this->em->getRepository('App:TallerExterno')->findOneBy(array(
            'id' => intval($idTaller)
        ));
        $producto = is_null($idProducto) ? null : $this->em->getRepository('App:Producto')->findOneBy(array(
            'id' => intval($idProducto)
        ));
        $otp = new OrdenTrabajoExterno();
        $otp->setDescripcion($descripcion);
        $otp->setCantidad(intval($cantidad));
        $otp->setCostoTotal($total);
        $otp->setTallerExterno($taller);
        $otp->setProducto($producto);
        $otp->setOrdenesTrabajo($orden);
        $orden->addExterno($otp);
        $taller->addOrdenTrabajo($otp);

        $this->em->persist($otp);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function delTallerExterno($orden, $id_externo)
    {
        $ote = $this->em->getRepository('App:OrdenTrabajoExterno')->findOneBy(array(
            'id' => intval($id_externo)
        ));
        $this->em->remove($ote);
        $this->em->flush();
        $this->recalcularCostoOrden($orden);
        return true;
    }

    public function addPeticion($orden, $peticiones)
    {
        foreach ($peticiones as $id) {
            $peticion = $this->em->getRepository('App:Peticion')->findOneBy(array(
                'id' => intval($id)
            ));
            $peticion->setOrdenTrabajo($orden);
            $orden->addPeticion($peticion);
            $this->em->persist($peticion);
        }
        $this->recalcularCostoOrden($orden);
        return $peticion;
    }

    public function toArray($ordenes)
    {
        $arr = array();
        foreach ($ordenes as $orden) {
            $mecanicos = array();
            $costo_mecanico = $horas_mecanico = 0;
            foreach ($orden->getMecanicos() as $otMecanico) {
                if (!in_array($otMecanico->getMecanico()->getNombre(), $mecanicos)) {
                    $mecanicos[] = $otMecanico->getMecanico()->getNombre();
                    $costo_mecanico += $otMecanico->getCostoTotal();
                    $horas_mecanico += $otMecanico->getHoras();
                }
            }
            $arr[$orden->getId()] = array(
                'id' => $orden->getId(),
                'identificador' => $orden->getIdentificador(),
                'numeroInterno' => is_null($orden->getNumeroInterno()) ? '---' : $orden->getNumeroInterno(),
                'numeroExterno' => is_null($orden->getNumeroExterno()) ? '---' : $orden->getNumeroExterno(),
                'servicio' => $orden->getServicio()->getNombre(),
                'tipo' => $orden->getStrTipo(),
                'fecha' => $orden->getFecha(),
                'fechaCierre' => $orden->getFechaCierre(),
                'taller' => !is_null($orden->getTaller()) ? $orden->getTaller()->getNombre() : '---',
                'sector' => !is_null($orden->getTallerSector()) ? $orden->getTallerSector()->getNombre() : '---',
                'deposito' => !is_null($orden->getDeposito()) ? $orden->getDeposito()->getNombre() : '---',
                'descripcion' => $orden->getDescripcion(),
                'costoTotal' => $orden->getCostoTotal(),
                'mecanico' => count($mecanicos) > 0 ? implode(",", $mecanicos) : '---',
                'horasMecanico' => $horas_mecanico,
                'costoMecanico' => $costo_mecanico,
                'estado' => $orden->getEstado(),
            );
        }
        return $arr;
    }

    public function getCountOtPorEstado($servicio, $fecha, $estado = null)
    {
        $query = $this->em->createQueryBuilder()
            ->select('count(ot.id)')
            ->from('App:OrdenTrabajo', 'ot')
            ->where('ot.servicio = :servicio')
            ->andWhere('ot.fecha <= :fecha')
            ->setParameter('fecha', $fecha)
            ->setParameter('servicio', $servicio);

        if ($estado == null) {
            $query->andWhere('ot.estado IS NULL');
        } else {
            $query->andWhere('ot.estado = :estado');
            $query->setParameter('estado', $estado);
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function getCerradasPorCloseAt($servicio)
    {
        $ayer = date("Y-m-d H:m:s", strtotime('-1 days'));
        $hoy = date("Y-m-d H:m:s");

        $query = $this->em->createQueryBuilder()
            ->select('count(ot.id)')
            ->from('App:OrdenTrabajo', 'ot')
            ->where('ot.servicio = :servicio')
            ->andWhere('ot.close_at >= :ayer')
            ->andWhere('ot.close_at <= :hoy')
            ->setParameter('ayer', $ayer)
            ->setParameter('hoy', $hoy)
            ->setParameter('servicio', $servicio);

        return $query->getQuery()->getSingleScalarResult();
    }

    public function getNuevasPorCreatedAt($servicio)
    {
        $ayer = date("Y-m-d H:m:s", strtotime('-1 days'));
        $hoy = date("Y-m-d H:m:s");
        $query = $this->em->createQueryBuilder()
            ->select('count(ot.id)')
            ->from('App:OrdenTrabajo', 'ot')
            ->where('ot.servicio = :servicio')
            ->andWhere('ot.created_at >= :ayer')
            ->andWhere('ot.created_at <= :hoy')
            ->setParameter('ayer', $ayer)
            ->setParameter('hoy', $hoy)
            ->setParameter('servicio', $servicio);

        return $query->getQuery()->getSingleScalarResult();
    }

    public function cambiarEstado($orden)
    {
        $estado = null;
        $encurso = 0;
        $nuevas = 0;
        foreach ($orden->getTareas() as $tarea) {
            if ($tarea->getEstado() == 1) { //encurso
                $encurso++;
            }

            if ($tarea->getEstado() != 2 && $tarea->getEstado() != 1) {
                $nuevas++;
            }
        }
        if ($encurso > 0) {
            $estado = 1; //en curso
        } else { // als 2 estan en 0
            $estado = null;
        }
        $orden->setEstado($estado);

        $this->em->persist($orden);
        $this->em->flush();
        return $orden;
    }
}
