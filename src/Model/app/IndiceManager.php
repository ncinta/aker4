<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\IndiceServicio;
use App\Entity\IndiceCc;
use App\Entity\IndiceGs;

/**
 * Description of IndiceManager
 *
 * @author nicolas
 */
class IndiceManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getLastInsertServicio($servicio)
    {
        return $this->em->getRepository('App:IndiceServicio')->getLastInsert($servicio);
    }

    public function getLastInsertCc($centroCosto)
    {
        return $this->em->getRepository('App:IndiceCc')->getLastInsert($centroCosto);
    }

    public function getLastInsertGs($grupoServicio)
    {
        return $this->em->getRepository('App:IndiceGs')->getLastInsert($grupoServicio);
    }

    private $buffer = null;
    const MAX_BUFFER_INDICE = 200;

    /** es una funcion de grabacion de indice generica que sirve para servicio, centro de costo y grupo de servicio.
     * por lo tanto el $indice es el que determina en que entidad debe grabarse los datos correspondientes.
     */
    public function grabarIndice($indice, $arrMante, $arrPet, $arrOt)
    {

        $fecha = (new \DateTime())->modify("-1 day");
        $fecha->setTime(0, 0, 0);

        // GRABO LOS INDICES DE SERVICIO, Gs, Cc        
        $indice->setFecha($fecha);
        $indice->setMantenimientoNuevo($arrMante['manteNuevos']);
        $indice->setMantenimientoPendiente($arrMante['mantePendientes']);
        $indice->setMantenimientoAsignado($arrMante['manteAsignados']);
        $indice->setMantenimientoRealizado($arrMante['manteRealizados']);
        $indice->setPeticionNueva($arrPet['petNuevas']);
        $indice->setPeticionPendiente($arrPet['petPendientes']);
        $indice->setPeticionEncurso($arrPet['petEnCurso']);
        $indice->setPeticionDetenida($arrPet['petDeten']);
        $indice->setPeticionRealizada($arrPet['petRealizada']);
        $indice->setOrdentrabajoNueva($arrOt['otNuevas']);
        $indice->setOrdentrabajoEncurso($arrOt['otEnCurso']);
        $indice->setOrdentrabajoRealizada($arrOt['otCerradas']);

        $this->buffer[] = $indice;
        if (count($this->buffer) == self::MAX_BUFFER_INDICE) {
            try {
                foreach ($this->buffer as $data) {
                    $this->em->persist($data);
                }
                $this->em->flush();
                $this->buffer = null;  /// vacio todo el buffer

            } catch (\PDOException $e) {
                echo ('error al persistir indiceServicio');
            }
        }

        return true;
    }

    public function forcePersistIndiceServicio()
    {
        try {
            foreach ($this->buffer as $data) {
                $this->em->persist($data);
            }
            $this->em->flush();
            $this->buffer = null;  /// vacio todo el buffer

        } catch (\PDOException $e) {
            echo ('error al persistir indice de servicio en forcePersist');
        }
    }

    public function getIndicesServicios($servicios, $desde, $hasta)
    {
        return $this->em->getRepository('App:IndiceServicio')->getIndicesServicios($servicios, $desde, $hasta);
    }

    public function getIndicesCc($centroscosto, $desde, $hasta)
    {
        return $this->em->getRepository('App:IndiceCc')->getIndicesCc($centroscosto, $desde, $hasta);
    }

    public function getIndicesGs($grupo, $desde, $hasta)
    {
        return $this->em->getRepository('App:IndiceGs')->getIndicesGs($grupo, $desde, $hasta);
    }

    public function ordenarIndices($indices)
    {
        $array = null;

        foreach ($indices as $indice) {
            $fecha = $indice->getFecha()->format('d-m-Y');
            $array[$fecha] = array(
                'mantenimiento_pendiente' => 0,
                'mantenimiento_asignado' => 0,
                'mantenimiento_realizado' => 0,
                'mantenimiento_nuevo' => 0,
                'mantenimiento_rn' => 0,
                'mantenimiento_rp' => 0,
                'peticion_pendiente' => 0,
                'peticion_nuevo' => 0,
                'peticion_encurso' => 0,
                'peticion_detenido' => 0,
                'peticion_realizado' => 0,
                'ordentrabajo_nuevo' => 0,
                'ordentrabajo_encurso' => 0,
                'ordentrabajo_realizado' => 0,
            );
        }

        foreach ($indices as $indice) {
            $fecha = $indice->getFecha()->format('d-m-Y');

            $array[$fecha]['mantenimiento_pendiente'] += $indice->getMantenimientoPendiente();
            $array[$fecha]['mantenimiento_asignado'] += $indice->getMantenimientoAsignado();
            $array[$fecha]['mantenimiento_realizado'] += $indice->getMantenimientoRealizado();
            $array[$fecha]['mantenimiento_nuevo'] += $indice->getMantenimientoNuevo();

            $array[$fecha]['peticion_pendiente'] += $indice->getPeticionPendiente();
            $array[$fecha]['peticion_nuevo'] += $indice->getPeticionNueva();
            $array[$fecha]['peticion_encurso'] += $indice->getPeticionEncurso();
            $array[$fecha]['peticion_detenido'] += $indice->getPeticionDetenida();
            $array[$fecha]['peticion_realizado'] += $indice->getPeticionRealizada();

            $array[$fecha]['ordentrabajo_nuevo'] += $indice->getOrdentrabajoNueva();
            $array[$fecha]['ordentrabajo_encurso'] += $indice->getOrdentrabajoEncurso();
            $array[$fecha]['ordentrabajo_realizado'] += $indice->getOrdentrabajoRealizada();
        }
        foreach ($indices as $indice) {
            $fecha = $indice->getFecha()->format('d-m-Y');

            $array[$fecha]['mantenimiento_rn'] = $array[$fecha]['mantenimiento_nuevo'] > 0 ?
                round($array[$fecha]['mantenimiento_realizado'] / $array[$fecha]['mantenimiento_nuevo'], 3) : 0;
            $array[$fecha]['mantenimiento_rp'] = $array[$fecha]['mantenimiento_pendiente'] > 0 ?
                round($array[$fecha]['mantenimiento_realizado'] / $array[$fecha]['mantenimiento_pendiente'], 3) : 0;

            $array[$fecha]['peticion_rn'] = $array[$fecha]['peticion_nuevo'] > 0 ?
                round($array[$fecha]['peticion_realizado'] / $array[$fecha]['peticion_nuevo'], 3) : 0;
            $array[$fecha]['peticion_rp'] = $array[$fecha]['peticion_pendiente'] > 0 ?
                round($array[$fecha]['peticion_realizado'] / $array[$fecha]['peticion_pendiente'], 3) : 0;
        }


        return $array;
    }

    public function dataParaBarras($indice)
    {
        $petTotal = $indice !== null ? $indice->getPeticionPendiente() + $indice->getPeticionRealizada() + $indice->getPeticionEncurso() + $indice->getPeticionDetenida() : 0;
        $otTotal = $indice !== null ? $indice->getOrdentrabajoNueva() + $indice->getOrdentrabajoEncurso() + $indice->getOrdentrabajoRealizada() : 0;

        return array(
            'fecha' => $indice !== null ? $indice->getFecha()->format('d-m-Y') : null,
            'mante_total' => $indice !== null && $indice->getMantenimientoAsignado() > 0 &&
                $indice->getMantenimientoAsignado() !== null ? $indice->getMantenimientoAsignado() : 0,
            'mant_cant_pendiente' => $indice !== null && $indice->getMantenimientoAsignado() > 0 && $indice->getMantenimientoAsignado() !== null ?
                $indice->getMantenimientoPendiente() : 0,
            'mante_pendiente' => $indice !== null && $indice->getMantenimientoAsignado() > 0 && $indice->getMantenimientoAsignado() !== null ?
                round(($indice->getMantenimientoPendiente() / $indice->getMantenimientoAsignado()) * 100) : 0,
            'pet_total' => $petTotal,
            'pet_pendiente' => $petTotal > 0 ? round(($indice->getPeticionPendiente() / $petTotal) * 100) : 0,
            'pet_encurso' => $petTotal > 0 ? round(($indice->getPeticionEncurso() / $petTotal) * 100) : 0,
            'pet_detenida' => $petTotal > 0 ? round(($indice->getPeticionDetenida() / $petTotal) * 100) : 0,
            'pet_realizada' => $petTotal > 0 ? round(($indice->getPeticionRealizada() / $petTotal) * 100) : 0,
            'ot_total' => $otTotal,
            'ot_nueva' => $otTotal > 0 ? round(($indice->getOrdentrabajoNueva() / $otTotal) * 100) : 0,
            'ot_encurso' => $otTotal > 0 ? round(($indice->getOrdentrabajoEncurso() / $otTotal) * 100) : 0,
            'ot_realizada' => $otTotal > 0 ? round(($indice->getOrdentrabajoRealizada() / $otTotal) * 100) : 0,
        );
    }

    public function borrarIndices()
    { //borramos para que no se dupliquen
        $fecha = (new \DateTime())->modify("-1 day");
        $fecha->setTime(0, 0, 0);

        $qb = $this->em->createQueryBuilder()
            ->delete('App:IndiceServicio', 'i')
            ->where('i.fecha = :fecha')
            ->setParameter('fecha', $fecha);

        $query = $qb->getQuery();
        $query->execute();

        $qbGs = $this->em->createQueryBuilder()
            ->delete('App:IndiceGs', 'i')
            ->where('i.fecha = :fecha')
            ->setParameter('fecha', $fecha);

        $queryGs = $qbGs->getQuery();
        $queryGs->execute();

        $qbCc = $this->em->createQueryBuilder()
            ->delete('App:IndiceCc', 'i')
            ->where('i.fecha = :fecha')
            ->setParameter('fecha', $fecha);

        $queryCc = $qbCc->getQuery();
        $queryCc->execute();
    }
}
