<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Model\app\UserLoginManager;
use Exception;

class ExcelManager
{

    private $xls_service;
    private $actSheet;
    private $border;
    private $styleFill;
    private $style;
    private $styleTotalFill;

    public function __construct(UserLoginManager $userlogin)
    {
        $this->userlogin = $userlogin;
        $this->xls_service = new Spreadsheet();

        $this->border = array(
            'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('rgb' => '000000')
        );
        $this->styleFill = array(
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startcolor' => array(
                    // 'rgb' => 'c3c3c3',
                    // 'rgb' => '157ab3',
                    'rgb' => 'CBD7EC',
                ),
            ),
        );
        $this->style = array(
            'borders' => array(
                'bottom' => $this->border,
                'top' => $this->border,
                'left' => $this->border,
                'right' => $this->border,
            )
        );
        $this->styleTotalFill = array(
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startcolor' => array(
                    // 'rgb' => 'c3c3c3',
                    // 'rgb' => '70c2e0',
                    'rgb' => 'B2BDCF',
                ),
            ),
        );
        $this->styleTotalBorder = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => array('rgb' => '000000')
                ),
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            )
        );
    }

    public function create($title)
    {
        $this->actSheet = $this->xls_service->getActiveSheet();
        $this->actSheet->setTitle(substr($title, 0, 30));
        $this->actSheet->getPageSetup()
            ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        // Set default font for the worksheet
        $this->xls_service->getDefaultStyle()->getFont()
            ->setName('Arial')
            ->setSize(10);

        $logo = $this->userlogin->getOrganizacion()->getFileLogo();
        if ($logo != '' && file_exists($logo)) {
            $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setCoordinates('A1');
            $objDrawing->setPath($this->userlogin->getOrganizacion()->getFileLogo());
            $objDrawing->setWidthAndHeight(200, 90);
            $objDrawing->setWorksheet($this->actSheet);
        }

        //title
        $this->actSheet->setCellValue('C1', $title);
        $this->actSheet->getStyle("C1")->getFont()->setBold(true)->setSize("14");
        $this->actSheet->getStyle('C1')->applyFromArray(array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrap' => true
            )
        ));
        $this->actSheet->getRowDimension(1)->setRowHeight(25);
        $this->actSheet->mergeCellsByColumnAndRow(2, 1, 5, 1);
        // die('////<pre>' . nl2br(var_export($title, true)) . '</pre>////');
        return $this;
    }

    public function setSubTitle($cell, $subTitle)
    {
        //title
        $this->actSheet->setCellValue($cell, $subTitle);
        $this->actSheet->getStyle($cell)->getFont()->setItalic(true)->setSize("12");
        $this->actSheet->getStyle($cell)->applyFromArray(array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrap' => true
            )
        ));
    }

    public function setHeaderInfo($info)
    {
        foreach ($info as $key => $value) {
            $this->actSheet->setCellValue($key, $value);
        }
    }

    public function setBar($row, array $bar)
    {
        $this->actSheet->getRowDimension($row)->setRowHeight(20);

        foreach ($bar as $key => $value) {
            $this->actSheet->setCellValue($key . $row, $value['title']);
            $this->actSheet->getColumnDimension($key)->setWidth($value['width']);
            $this->actSheet->getStyle($key . $row)->applyFromArray($this->style);
            $this->actSheet->getStyle($key . $row)->applyFromArray(array(
                'font' => array(
                    'color' => array('rgb' => isset($value['color']) ? $value['color'] : null),
                )
            ));
        }
    }

    public function setRowValues($row, array $values)
    {
        $this->actSheet->getRowDimension($row)->setRowHeight(18);
        foreach ($values as $key => $value) {
            $color = isset($value['color']) ? $value['color'] : null;
            if (!isset($value['format'])) {
                $this->setCellValue($key . $row, $value['value'], null, $color);
            } else {
                $this->setCellValue($key . $row, $value['value'], $value['format'], $color);
            }
        }
    }

    public function setRowTotales($row, array $values)
    {
        $this->actSheet->getRowDimension($row)->setRowHeight(18);
        foreach ($values as $key => $value) {
            if (!isset($value['format'])) {
                $this->setCellValue($key . $row, $value['value'], null);
            } else {
                $this->setCellValue($key . $row, $value['value'], $value['format']);
            }
            $this->actSheet->getStyle($key . $row)->applyFromArray($this->styleTotalBorder);
            $this->actSheet->getStyle($key . $row)->applyFromArray($this->styleTotalFill);
        }
    }

    public function setCellValue($cell, $value, $format = null, $color = null)
    {
        $this->actSheet->setCellValue($cell, $value);
        if ($format != null) {
            $this->actSheet->getStyle($cell)->getNumberFormat()->setFormatCode($format);
        }
        // se debe eliminar el # porque no lo toma con el mismo.
        if (!is_null($color)) {
            if (substr($color, 0, 1) == '#') {
                $color = substr($color, 1, strlen($color));
            }
            $this->actSheet->getStyle($cell)->applyFromArray(
                array(
                    'font' => array(
                        'color' => array('rgb' => $color),
                    )
                )
            );
        }
    }

    public function setCellColspan($row, $columns)
    {
        foreach ($columns as $startColumnLetter => $data) {
            // Obtener los datos del array
            $title = $data['title'];
            $width = $data['width'] ?? 1; // Usar 1 como valor por defecto si no hay 'width'

            // Calcular la columna final
            $startColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($startColumnLetter);
            $endColumnIndex = $startColumnIndex + $width - 1;
            $endColumnLetter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($endColumnIndex);

            // Fusionar las celdas
            $this->actSheet->mergeCells("$startColumnLetter$row:$endColumnLetter$row");

            // Establecer el valor
            $this->actSheet->setCellValue("$startColumnLetter$row", $title);
        }
    }

    public function getResponse()
    {
        return $this->createStreamedResponse($this->xls_service, 'Xlsx');
    }

    /**
     * Return a StreamedResponse containing the file
     * 
     * @param Spreadsheet $spreadsheet
     * @param string $type
     * @param number $status
     * @param array $headers
     * @param array $writerOptions
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function createStreamedResponse(Spreadsheet $spreadsheet, $type, $status = 200, $headers = array(), $writerOptions = array())
    {
        $writer = IOFactory::createWriter($spreadsheet, $type);
        if (!empty($writerOptions)) {
            $this->applyOptionsToWriter($writer, $writerOptions);
        }
        return new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            },
            $status,
            $headers
        );
    }
}
