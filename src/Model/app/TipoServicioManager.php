<?php
/**
 * TipoVehiculoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chip;
use App\Entity\Organizacion as Organizacion;

class TipoServicioManager {

    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    
 
    public function findById($id) {
        return $this->em->getRepository('App:TipoServicio')->find($id);
    }
    
    /**
     * Retorna todos los tipos de servicio que hay en el sistema.
     * @return TipoVehiculos
     */
    public function findAll() {
        return $this->em->getRepository('App:TipoServicio')->findBy(array(), array('nombre' => 'ASC'));
    }

}
