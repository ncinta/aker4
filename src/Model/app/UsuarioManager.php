<?php

namespace App\Model\app;

use App\Entity\Transporte;
use App\Entity\Logistica;
use App\Entity\Empresa;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Usuario as Usuario;
use App\Model\app\BitacoraManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioManager
{

    protected $em;
    protected $security;
    protected $organizacion;
    protected $bitacora;
    protected $usuario;
    protected $userlogin;
    protected $usuarioOld;   //datos del usuario viejo.
    private $encoder;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        OrganizacionManager $organizacion,
        BitacoraManager $bitacora,
        UserLoginManager $userlogin,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->organizacion = $organizacion;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
        $this->encoder = $encoder;
    }

    public function setLogin($request, $usr)
    {
        $usr->setUltIp($request->getClientIp());
        $usr->setUltHost($request->getHost());
        $this->em->persist($usr);
        $this->em->flush();
    }

    public function find($usuario)
    {
        if ($usuario instanceof Usuario) {
            $this->usuario = $this->em->find('App:Usuario', $usuario->getId());
        } else {
            $this->usuario = $this->em->find('App:Usuario', $usuario);
        }

        //backup del usuario anterior.        
        $this->usuarioOld = $this->bitacora->toArray($this->usuario);

        return $this->usuario;
    }

    public function findByUsername($username)
    {
        $this->usuario = $this->em->getRepository('App:Usuario')
            ->findOneBy(array('username' => $username));

        //backup del usuario anterior
        $this->usuarioOld = $this->bitacora->toArray($this->usuario);
        return $this->usuario;
    }

    public function findContactos($phone) {
        return $this->em->getRepository('App:Contacto')->findBy(['celular' => $phone]);
    }

    public function findByApicode($phone, $apicode)
    {
        $this->usuario = $this->em->getRepository('App:Usuario')
            ->findOneBy(array('telefono' => $phone, 'apiCode' => strtoupper($apicode)));
        return $this->usuario;
    }

    public function findByPhone($phone, $organizacion)
    {
        // (Nico)en algun momento eliminar organizacion, porque no se utiliza. no la saco ahora para no tener que tocar todas las llamadas a esta funcion y tocar prod
        $query = $this->em->createQueryBuilder('c')
            ->select('u')
            ->from('App:Usuario', 'u')
            ->where('u.telefono = ?1 AND u.token IS NOT NULL')
            ->setParameter(1, $phone);
        // die('////<pre>' . nl2br(var_export($estado, true)) . '</pre>////');
        $this->usuario = $query->getQuery()->getOneOrNullResult();
        return $this->usuario;
    }

    public function findByToken($token)
    {
        $query = $this->em->createQueryBuilder('c')
            ->select('u')
            ->from('App:Usuario', 'u')
            ->where('u.token = ?1')
            //->distinct()
            ->setParameter(1, $token);
            $rst = $query->getQuery()->getResult();
            //dd($rst[0]);
        $this->usuario = $rst[0];
        return $this->usuario;
    }


    /**
     * Obtiene el token de un usuario pasandole el telefono. Verifica que el token no sea null
     * No importa la organizacion en la que se encuentre el usuario, lo busca en toda la base de 
     * datos
     */
    public function findTokenByPhone($phone)
    {
        $query = $this->em->createQueryBuilder('c')
            ->select('u')
            ->from('App:Usuario', 'u')
            ->where('u.telefono = ?1 AND u.token IS NOT NULL')
            ->setParameter(1, $phone);
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findAll($organizacion, array $orderBy = null)
    {
        $org = $this->organizacion->find($organizacion);
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('nombre' => 'ASC');
            }
            return $this->em->getRepository('App:Organizacion')
                ->findAllUsuarios($org, $orderBy);
        } else {
            return null;
        }
    }

    public function create($organizacion = null)
    {
        $this->usuario = new Usuario();
        $this->usuario->setOrganizacion($organizacion);
        $this->usuario->setEnabled(true);

        return $this->usuario;
    }

    public function save(Usuario $usuario, $dif_add = null, $dif_remove = null)
    {
        try {
            $this->usuario = $usuario;
            //rescato el anterior antes de grabar.
            $usrLogueado = $this->security->getToken()->getUser();

            if ($this->usuario->getPermisos() != null) {
                foreach ($this->usuario->getPermisos() as $permiso) {
                    $this->usuario->addRole($permiso->getCodename());
                }
            }
            $this->usuario->addRole('ROLE_ADMIN');
            $this->em->persist($this->usuario);
            $this->em->flush();
            if (!is_null($dif_add) || !is_null($dif_remove)) {
                $this->bitacora->usuarioUpdateRoles($usrLogueado, $this->usuario, $dif_add, $dif_remove);
            } elseif ($this->usuario != null && $this->usuarioOld != null) {
                $this->bitacora->usuarioUpdate($usrLogueado, $this->usuario, $this->usuarioOld, $this->bitacora->toArray($this->usuario));
            }
            return true;
        } catch (UniqueConstraintViolationException) {
            return false;
        }
    }

    public function getTimezone()
    {
        if (is_null($this->usuario)) {
            return 'America/Santiago';
        } else {
            return $this->usuario->getTimeZone();
        }
    }

    public function getOrganizacion()
    {
        return $this->usuario->getOrganizacion();
    }

    public function delete($usuario)
    {
        $this->bitacora->usuarioDelete($this->userlogin->getUser(), $usuario);
        $this->em->remove($usuario);
        $this->em->flush();
        return true;
    }

    public function createForMonitor($tmp, $entity)
    {
        //      die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (
            $tmp['usuario']['nombre'] == '' || $tmp['usuario']['username'] == '' || $tmp['usuario']['email'] == '' ||
            $tmp['usuario']['plainPassword']['first'] == '' || $tmp['usuario']['plainPassword']['second'] == ''
        ) {
            return json_encode(array('error' => 'Faltan datos'));
        }

        if ($tmp['usuario']['plainPassword']['first'] !== $tmp['usuario']['plainPassword']['second']) {
            return json_encode(array('error' => 'las contraseñas no coinciden'));
        }

        $usuario = $this->create($this->userlogin->getOrganizacion());
        $usuario->setNombre($tmp['usuario']['nombre']);
        $usuario->setUsername($tmp['usuario']['username']);
        $usuario->setEmail($tmp['usuario']['email']);
        $usuario->setTelefono($tmp['usuario']['telefono']);
        $usuario->setPassword($this->encoder->encodePassword($usuario, $tmp['usuario']['plainPassword']['first']));
        $usuario->setEnabled(true);
        $usuario->setChangePassword(false);
        $usuario->setShowReferencias(true);
        $usuario->setTimeZone($tmp['usuario']['timeZone']);
        $usuario->setVistaMapa(1);
        $usuario->setLimiteHistorial(0);

        //genera el apicode para todos los usuarios a los que se les agregue el telefono.
        if (!is_null($tmp['usuario']['telefono'])) {
            $usuario->setApiCode($this->generarApiCode());
        }

        if ($entity instanceof Transporte) {
            $usuario->setTransporte($entity);
        } elseif ($entity instanceof Logistica) {
            $usuario->setLogistica($entity);
        } elseif ($entity instanceof Empresa) {
            $usuario->setEmpresa($entity);
        } else {
            return 'false';
        }

        $usuario = $this->addRoleMonitor($usuario, $tmp['usuario']);

        try {
            //  die('////<pre>' . nl2br(var_export($usuario->getnombre(), true)) . '</pre>////');
            $this->usuario = $usuario;
            //rescato el anterior antes de grabar.
            $usrLogueado = $this->security->getToken()->getUser();

            $this->em->persist($this->usuario);
            $this->em->flush();

            // $this->bitacora->usuarioUpdate($usrLogueado, $this->usuario, $this->userlogin->getUser(), $this->bitacora->toArray($this->usuario));

            return 'true';
        } catch (UniqueConstraintViolationException) {
            return json_encode(array('error' => 'Este usuario ya existe'));
        }
    }

    /** Para un usuario y un role (string) determina si el usuario tiene ese role asignado */
    public function isGranted($usuario, $role)
    {
        return in_array($role, $usuario->getRoles());
    }

    private function addRoleMonitor($usuario, $tmp)
    {

        $permiso = $this->em->getRepository('App:Permiso')->findOneBy(array('codename' => 'ROLE_MONITOR_ADMIN'));
        if (isset($tmp['itinerario_administrador'])) {
            $usuario->addRole('ROLE_MONITOR_ADMIN');
            $usuario->addPermiso($permiso);
        } else {
            $usuario->removeRole('ROLE_MONITOR_ADMIN');
            $usuario->removePermiso($permiso);
        }

        $permiso = $this->em->getRepository('App:Permiso')->findOneBy(array('codename' => 'ROLE_MONITOR_OPERADOR'));
        if (isset($tmp['itinerario_operador'])) {
            $usuario->addRole('ROLE_MONITOR_OPERADOR');
            $usuario->addPermiso($permiso);
        } else {
            $usuario->removeRole('ROLE_MONITOR_OPERADOR');
            $usuario->removePermiso($permiso);
        }
        return $usuario;
    }

    private function generarApiCode()
    {
        return (chr(rand(65, 90)) . rand(0, 9) . chr(rand(65, 90)) . chr(rand(65, 90)) . rand(0, 9) . rand(0, 9));
    }

    public function setData($usuario, $data)
    {
        $usuario->setNombre($data['nombre']);
        $usuario->setTelefono($data['telefono']);
        $usuario->setEmail($data['email']);
        $usuario = $this->addRoleMonitor($usuario, $data);
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');

        //genera el apicode para todos los usuarios a los que se les agregue el telefono.
        if (!is_null($data['telefono'])) {
            $usuario->setApiCode($this->generarApiCode());
        }

        $this->usuario = $usuario;

        return $this->usuario;
    }
}
