<?php

namespace App\Model\app;

/**
 * Description of PanicoControlManager
 *
 * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
 */

use App\Entity\Notificacion;
use App\Model\app\Base\NotificadorBaseManager;

class NotificadorManager extends NotificadorBaseManager
{

    private $userlogin;

    public function setUserlogin($userlogin)
    {
        $this->userlogin = $userlogin;
    }

    public function despacharSMS($destinos, $registrar = true)
    {
        parent::despacharSMS($destinos);
        if ($registrar) {
            $this->registrar($destinos);
        }
    }

    public function despacharPUSH($personas, $mensaje, $titulo, $evento = null)
    {
        parent::despacharPUSH($personas, $mensaje, $titulo, $evento);
        $this->registrar($personas);
    }

    public function despacharMail($destinos, $asunto, $cuerpo)
    {
        parent::despacharMail($destinos, $asunto, $cuerpo);
    }


    private function registrar($data)
    {
        $this->em->getConnection()->beginTransaction();
        foreach ($data as $d) {
            $reg = new Notificacion();
            if ($d['persona']) {
                $reg->setPersona($d['persona']);
            }
            $reg->setAbonado($d['abonado']);
            $reg->setMensaje($d['mensaje']);
            $reg->setEjecutor($this->userlogin->getUser());
            $reg->setTipo($d['tipo']);
            $reg->setDestino($d['destino']);
            $this->em->persist($reg);
            $this->em->flush();
        }
        $this->em->getConnection()->commit();
        return true;
    }

}
