<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Prestacion;
use App\Entity\ParadaPrestacion;
use App\Entity\Organizacion as Organizacion;

class PrestacionManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($prestacion)
    {
        if ($prestacion instanceof Prestacion) {
            return $this->em->getRepository('App:Prestacion')->findOneBy(array('id' => $prestacion->getId()));
        } else {
            return $this->em->getRepository('App:Prestacion')->findOneBy(array('id' => $prestacion));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Prestacion')->findBy(array('organizacion' => $organizacion->getId()), array('inicio_prestacion' => 'asc'));
        } else {
            return $this->em->getRepository('App:Prestacion')->findBy(array('organizacion' => $organizacion), array('inicio_prestacion' => 'asc'));
        }
    }

    const PRESTACION_ACTIVA = 1;
    const PRESTACION_DETENIDA = 0;

    public function create(Organizacion $organizacion)
    {
        $prestacion = new Prestacion();
        $prestacion->setOrganizacion($organizacion);
        $prestacion->setEstado(self::PRESTACION_DETENIDA);

        return $prestacion;
    }

    public function findServicios($prestacion)
    {
        return $this->em->getRepository('App:Servicio')->findByPrestacion($prestacion);
    }

    public function findByCcServicio($servicio, $centrocosto)
    {
        return $this->em->getRepository('App:Prestacion')->findOneBy(array('centroCosto' => $centrocosto->getId(), 'servicio' => $servicio->getId()));
    }

    public function save(Prestacion $prestacion)
    {
        $this->em->persist($prestacion);
        $this->em->flush();
        return $prestacion;
    }

    public function desactivarPrestacion($prestacion, $motivo, $hasta, $servicio = null)
    {
        if ($servicio) {
            //saco el servicio de la prestacion
            $servicio->setCentroCosto(null);
            $this->em->persist($servicio);
        }

        if (!is_null($prestacion)) {
            //calculo de horas totales de prestacion
            $diff = date_diff($prestacion->getInicio_prestacion(), $hasta, true);
            $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;

            //aca se deben calcular las paradas.
            //actualizo prestacion
            $prestacion->setHorasTotales($total);
            $prestacion->setFinPrestacion($hasta);
            $prestacion->setMotivo($motivo);

            //grabo todo
            $this->em->persist($prestacion);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * 
     * @param type $prestacion
     * @param type $motivo
     * @param type $desde
     * @return boolean
     */
    public function pausarPrestacion($prestacion, $motivo, $desde)
    {
        if (!is_null($prestacion)) {
            //aca se deben registrar las paradas
            $parada = new ParadaPrestacion();
            $parada->setInicioDetencion($desde);
            $parada->setMotivo($motivo);
            $parada->setPrestacion($prestacion);
            $this->em->persist($parada);

            //die('////<pre>' . nl2br(var_export($total, true)) . '</pre>////');                   
            //actualizo prestacion
            $prestacion->setEstado(self::PRESTACION_DETENIDA);
            $prestacion->setMotivo($motivo);

            //grabo todo
            $this->em->persist($prestacion);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * Inicia una prestacion que estaba detenida. Esto hace que tenga que 
     * buscar el registro por el cual se detuvo y lo cierre
     * @param type $prestacion
     * @return boolean
     */
    public function iniciarPrestacion($prestacion, $hasta)
    {
        if (!is_null($prestacion)) {
            //buscar la parada
            $parada = $this->findParadaActivo($prestacion);
            if ($parada) {
                //asignarle fecha de fin
                $parada->setFinDetencion($hasta);

                //hacer los calculos de horas
                $diff = date_diff($parada->getInicioDetencion(), $hasta, true);
                $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;
                $parada->sethorasParada($total);


                $this->em->persist($parada);
            }

            //tomas las horas totales de detenciones para actualizar.
            $totalPar = $this->getCalcularHorasParada($prestacion) + $total;
            $prestacion->setHorasParadas($totalPar);
            //actualizo prestacion
            $prestacion->setEstado(self::PRESTACION_ACTIVA);

            //grabo todo
            $this->em->persist($prestacion);
            $this->em->flush();
            return true;
        }
        return false;
    }

    private function getCalcularHorasParada($prestacion)
    {
        $query = $this->em->createQueryBuilder()
            ->select('SUM(pp.horasParada) as totalHoras')
            ->from('App:ParadaPrestacion', 'pp')
            ->join('pp.prestacion', 'p')
            ->where('p.id = ?1 ')
            ->setParameter('1', $prestacion->getId());

        return $query->getQuery()->getSingleScalarResult();
    }

    public function delete(Prestacion $prestacion)
    {
        $this->em->remove($prestacion);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $prestacion = $this->find($id);
        if (!$prestacion) {
            throw $this->createNotFoundException('Código de prestacion no encontrado.');
        }
        return $this->delete($prestacion);
    }

    public function findByServicio($servicio)
    {
        return $this->em->getRepository('App:Prestacion')->findByServicio($servicio);
    }

    public function findActivo($servicio)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Prestacion', 'p')
            ->where('p.servicio = ?1 ')
            ->andWhere('p.fin_prestacion is null')
            ->setParameter('1', $servicio->getId());

        return $query->getQuery()->getOneOrNullResult();
    }

    public function findParadaActivo($prestacion)
    {
        $query = $this->em->createQueryBuilder()
            ->select('pp')
            ->from('App:ParadaPrestacion', 'pp')
            ->join('pp.prestacion', 'p')
            ->where('p.id = ?1 ')
            ->andWhere('pp.finDetencion is null')
            ->setParameter('1', $prestacion->getId());

        return $query->getQuery()->getOneOrNullResult();
    }

    public function addStop($prestacion, $motivo, $desde, $hasta)
    {
        if (!is_null($prestacion)) {
            //aca se deben registrar las paradas
            $parada = new ParadaPrestacion();
            $parada->setInicioDetencion($desde);
            $parada->setFinDetencion($hasta);
            $parada->setMotivo($motivo);
            $parada->setPrestacion($prestacion);
            //hacer los calculos de horas
            $diff = date_diff($desde, $hasta, true);
            $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;
            $parada->sethorasParada($total);

            $this->em->persist($parada);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * Debe contar las horas para todas las prestaciones del servicio solo las 
     * horas que deberian estar en marcha, teniendo en cuenta las paradas.
     * @param type $servicio
     * @param type $desde
     * @param type $hasta
     */
    public function contarTiempoUso($servicio, $desde, $hasta)
    {
        //debo buscar todas las prestaciones entre esa fecha
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Prestacion', 'p')
            ->join('p.servicio', 's')
            ->where('s.id = ?1 ')
            ->andWhere('(p.fin_prestacion is null or p.fin_prestacion >= ?2)')
            ->setParameter('1', $servicio->getId())
            ->setParameter('2', $desde);

        $cuenta = 0;
        $q = $query->getQuery()->getResult();

        foreach ($q as $presta) {
            $parcial = 0;
            $tuso = $presta->getHorasUso() * 60;   //tiempo de uso en mins cada 24hs

            if (is_null($presta->getFin_Prestacion())) {   //no ha finalizado la prestacion todavia.
                if ($presta->getInicio_Prestacion() < $desde) {  //no ha terminado pero empezo antes del hasta (2)
                    $parcial = $this->diff($desde, $hasta);
                } elseif ($presta->getInicio_Prestacion() < $hasta) {  //calcular el desde/hasta completo porque todavia no ha terminado.
                    $parcial = $this->diff($presta->getInicio_Prestacion(), $hasta);
                }
            } elseif ($desde < $presta->getFin_Prestacion()) {   //termino despues del periodo (1)
                $parcial = $this->diff($presta->getFin_Prestacion(), $desde);
            } else {
            }
            $cuenta += ($parcial * $tuso) / 1440;
            $c[] = ($parcial * $tuso) / 1440;
        }

        return $cuenta;
    }

    private function diff($v1, $v2)
    {
        $diff = date_diff($v1, $v2, true);
        return (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i;  //en minutos
    }
}
