<?php

/**
 * Manejador de Equipos
 * @autor claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Equipo as Equipo;
use App\Entity\Chip as Chip;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ChipManager;
use App\Model\app\BitacoraEquipoManager;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\ServicioManager;

class EquipoManager
{

    protected $em;
    protected $userlogin;
    protected $organizacion;
    protected $chip;
    protected $servicio;
    protected $bitacoraEquipo;
    protected $bitacoraServicio;
    protected $equipo;
    protected $equipo_array = null;   //datos del equipo original    

    public function __construct(
        EntityManagerInterface $em,
        UserloginManager $userlogin,
        OrganizacionManager $organizacion,
        ChipManager $chip,
        ServicioManager $servicio,
        BitacoraEquipoManager $bitacoraEquipo,
        BitacoraServicioManager $bitacoraServicio
    ) {

        $this->userlogin = $userlogin;
        $this->organizacion = $organizacion;
        $this->em = $em;
        $this->chip = $chip;
        $this->servicio = $servicio;
        $this->bitacoraEquipo = $bitacoraEquipo;
        $this->bitacoraServicio = $bitacoraServicio;
    }

    public function ifExist($mdmid)
    {
        return $this->em->getRepository('App:Equipo')->ifExist($mdmid);
    }

    public function ifExistImei($imei)
    {
        return $this->em->getRepository('App:Equipo')->ifExistImei($imei);
    }

    public function toArrayData($equipo, $action)
    { //1 UPDATE, 2 DELETE
        $data = array(
            'entity' => 'EQUIPO',
            'action' => null,
            'data' => null,
        );
        if ($equipo) {
            $data['data']['id'] = $equipo instanceof Equipo ? $equipo->getId() : $equipo;
            $data['data']['updated_at'] =  $equipo instanceof Equipo ? $equipo->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $equipo instanceof Equipo ? $equipo->getCreatedAt()->format('Y-m-d H:i:s') : null;
            if ($action == 2) {
                $data['action'] = 'DELETE';
                return $data;
            }
            $data['data']['mdmid'] = $equipo->getMdmid();
            $data['action'] = 'UPDATE';
        }

        return $data;
    }

    /**
     * busca el equipo pasado por parámetro. Se puede pasar el id o el objeto y 
     * buscara uno u otro.
     * @param Equipo $equipo
     * @return type Equipo
     */
    public function find($equipo)
    {
        if ($equipo instanceof Equipo) {
            $this->equipo = $this->em->find('App:Equipo', $equipo->getId());
        } else {
            $this->equipo = $this->em->find('App:Equipo', $equipo);
        }
        if ($this->equipo) {
            $this->equipo_array = $this->equipo->data2array();
        }
        return $this->equipo;
    }

    public function findByMdmid($mdmid)
    {
        $this->equipo = $this->em->getRepository('App:Equipo')->findOneBy(array('mdmid' => $mdmid));
        if ($this->equipo) {
            $this->equipo_array = $this->equipo->data2array();
        }
        return $this->equipo;
    }

    /**
     * Busca todos los equipos de una organizacion. Si no se pasa organizacion o null
     * entonces busca los equipos del usuario logueado.
     * @param type $organizacion
     * @param array $orderBy
     * @return type Equipos
     */
    public function findAll($organizacion = null, array $orderBy = null)
    {
        if ($organizacion == null) {
            $org = $this->userlogin->getOrganizacion();
        } else {
            $org = $this->organizacion->find($organizacion);
        }
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('mdmid' => 'ASC');
            }
            return $this->em->getRepository('App:Equipo')
                ->findAllEquipos($org, $orderBy);
        } else {
            return null;
        }
    }

    /**
     * Busca todos los equipo del sistema pero por modelo del mismo.
     * @param type $organizacion
     * @param array $orderBy
     * @return type Equipos
     */
    public function findAllByModelo($modelo, $orderBy = null)
    {
        if (!$orderBy) {
            $orderBy = array('mdmid' => 'ASC');
        }
        return $this->em->getRepository('App:Equipo')->findAllByModelo($modelo, $orderBy);
    }

    public function findAllEnDeposito($organizacion = null)
    {
        if ($organizacion == null) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:Equipo')->findAllEnDeposito($organizacion, array('mdmid' => 'ASC'));
    }

    public function findAllEnDepositoWithProveedorAndPropietario($organizacion = null)
    {
        if ($organizacion == null) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:Equipo')->findAllEnDepositoWithProveedorAndPropietario($organizacion, array('mdmid' => 'ASC'));
    }

    /**
     * Inicializa un nuevo objeto Equipo. Setea lo valores por default.
     * $padre es el propietario del equipo y puede ser el id o la Organizacion
     * @param type $padre
     * @return Equipo 
     */
    public function create($padre)
    {
        $organizacion = $this->organizacion->find($padre);
        if ($organizacion) {
            $equipo = new Equipo();
            $equipo->setOrganizacion($organizacion);
            $equipo->setPropietario($organizacion);
            $equipo->setEstado($equipo::ESTADO_ENDEPOSITO); //lo setea en deposito.
            $this->equipo = $equipo;
            return $this->equipo;
        } else {
            return null;
        }
    }

    /**
     * Graba un equipo. 
     * @param Equipo $equipo
     * @return Equipo 
     */
    public function save(Equipo $equipo)
    {
        //registro el evento
        $alta = is_null($equipo->getId());

        $this->em->persist($equipo);
        $this->em->flush();

        if ($alta) {
            $this->bitacoraEquipo->equipoNew($this->userlogin->getUser(), $equipo);
        } else {
            $this->bitacoraEquipo->equipoUpdate($this->userlogin->getUser(), $this->equipo, $this->equipo_array);
        }
        return $equipo;
    }

    /**
     * Borra de la base de datos un equipo. 
     * Si tiene chip asociado entonces devuelve el chip al deposito. 
     * Si tiene un servicio asociado, lo deshabilita. 
     * @param type $equipo
     * @return type boolean
     */
    public function delete($equipo)
    {
        $equipo = $this->find($equipo);
        if (!$equipo) {
            return false;
        }

        foreach ($equipo->getChips() as $chip) {
            $chip->setEstado($chip::ESTADO_DISPONIBLE);
        }
        if ($equipo->getServicio()) {
            $servicio = $equipo->getServicio();
            $servicio->setEstado($servicio::ESTADO_DESHABILITADO);
        }
        $this->em->remove($equipo);
        $this->em->flush();
        return true;
    }

    /**
     * Asocia a un equipo, un chip o un array de chip.
     * @param type $equipo
     * @param Chip|Array|Int $chip Id del chip a asociar. Si es un array estara formado por el key 'chip'. 
     * @return type boolean
     */
    public function asociarChip($equipo, $chip)
    {
        $equipo = $this->find($equipo);
        if ($equipo) {
            //buscar el chip.
            if ($chip instanceof Chip) {   //lo que viene es un objeto Chip
                $chips = $this->em->getRepository('App:Chip')->find($chip->getId());
            } else if (is_array($chip)) {   //viene un array con los ids de los chips
                foreach ($chip as $key => $id) {
                    if ($key == 'chip') {
                        $chips[] = $this->em->getRepository('App:Chip')->find($id);
                    }
                }
            } else if ($chip) { //viene el id de un chip. 
                $chips = $this->em->getRepository('App:Chip')->find($chip);
            }

            if ($chips) {
                foreach ($chips as $chip) {     //recorro todos los chips y lo asocio con el equipo.
                    if ($chip->getEstado() != $chip::ESTADO_INSTALADO) {   //el chip no puede estar instalado.
                        $chip->setEquipo($equipo);
                        $chip->setEstado($chip::ESTADO_INSTALADO);
                        $this->em->persist($chip);
                    }
                }
                $this->em->flush();
                $this->bitacoraEquipo->asignarChip($this->userlogin->getUser(), $chip, $equipo);
                return true;
            } else {
                return false;
            }
        } else {
            return false;   //hay un problema que no tengo acceso al equipo.
        }
    }

    /**
     * Transfiere un equipo de organizacion. Si es por venta entonces se cambia al propietario.
     * Si el equipo tiene chips asociados, los mismos se tranfieren con el equipo.
     * Si no se quiere transferir los chips hay que retirarlos antes.
     * Nunca se pueden vender los chips.
     * @param type $equipo
     * @param type $newOrganizacion
     * @param type $tipo_transferencia
     * @return type boolean
     */
    public function transferir($equipo, $newOrganizacion, $tipo_transferencia)
    {
        $equipo = $this->find($equipo); //busco nuevamente el equipo.
        if ($equipo) {
            //debo buscar la organizacion
            $oldOrg = $equipo->getOrganizacion();
            $newOrg = $this->organizacion->find($newOrganizacion);
            if ($newOrg) {
                //puedo tener un servicio asociado al equipo debo desasociarlo y dejar el servicio
                $servicio = $equipo->getServicio();
                if ($servicio != null) {
                    //registro el evento "retirar equipo"    
                    $this->bitacoraEquipo->retirarServicio($this->userlogin->getUser(), $servicio, $equipo, array('motivo' => 'Transferencia'));
                    $this->bitacoraServicio->retirarEquipo($this->userlogin->getUser(), $servicio, $equipo, array('motivo' => 'Transferencia'));

                    //siempre tengo que desasociar el servicio.
                    $servicio->setEstado($servicio::ESTADO_DESHABILITADO);
                    $servicio->setEquipo(null);
                    $this->em->persist($servicio);
                }

                //cambio los chips que tenga asociados.
                foreach ($equipo->getChips() as $chip) {
                    $this->chip->tranferir($chip, $newOrg, 1);
                }

                if ($tipo_transferencia == 2) {              //venta
                    $equipo->setPropietario($newOrg);  //cambio al propietario
                }

                $equipo->setOrganizacion($newOrg);
                $equipo->setEstado($equipo::ESTADO_ENDEPOSITO);
                $this->em->persist($equipo);
                $this->em->flush();

                //registro el evento "transferir"
                $this->bitacoraEquipo->transferirEquipo($this->userlogin->getUser(), $equipo, $oldOrg, $newOrg);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retira un equipo del servicio. elimina la relacion que existe entre el 
     * equipo y el servicio.
     * @param type $equipo 
     */
    public function retirar($equipo, $dataRetiro)
    {
        $equipo = $this->find($equipo);
        if ($equipo) {
            $servicio = $equipo->getServicio();
            if ($servicio != null) {
                $servicio->setEstado($servicio::ESTADO_DESHABILITADO);
                $servicio->setEquipo(null);
                $equipo->setServicio(null);
                $equipo->setEstado($equipo::ESTADO_ENDEPOSITO);
                $this->em->persist($servicio);
                $this->em->persist($equipo);
                $this->em->flush();

                //registro el evento
                $this->bitacoraEquipo->retirarServicio($this->userlogin->getUser(), $servicio, $equipo, array('motivo' => $dataRetiro['motivo']));
                $this->bitacoraServicio->retirarEquipo($this->userlogin->getUser(), $servicio, $equipo, array('motivo' => $dataRetiro['motivo']));

                return true;
            }
        }
        return false;
    }

    public function asignarServicio($equipo, $servicio)
    {
        $equipo = $this->find($equipo);
        $servicio = $this->servicio->find($servicio);
        if ($equipo && $servicio) {

            $servicio->setEquipo($equipo);
            $servicio->setEstado($servicio::ESTADO_HABILITADO);
            $this->em->persist($servicio);

            $equipo->setServicio($servicio);
            $equipo->setEstado($equipo::ESTADO_INSTALADO);
            $this->em->persist($equipo);
            $this->em->flush();

            //registro el evento
            $this->bitacoraEquipo->asignarServicio($this->userlogin->getUser(), $servicio, $equipo);
            $this->bitacoraServicio->asignarEquipo($this->userlogin->getUser(), $servicio, $equipo);
            //$this->bitacora->asignarEquipoServicio($this->userlogin->getUser(), $servicio, $equipo);

            return true;
        }
        return false;
    }

    public function getAccesoriosModelo($equipo)
    {
        if (is_null($equipo)) {
            return null;
        }
        return $equipo->getModelo()->getAccesorios();
    }

    public function getModelos()
    {
        return $this->em->getRepository('App:Modelo')->findAll();
    }
}
