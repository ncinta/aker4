<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Panico;
use App\Model\app\UtilsManager;
use App\Model\app\UserLoginManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\EventoManager;

class PanicoManager
{

    protected $em;
    protected $userlogin;
    protected $utils;
    protected $tipoevento;
    protected $evento;
    protected $eventohistorial;

    public function __construct(
        EntityManagerInterface $em,
        UtilsManager $utils,
        UserLoginManager $userlogin,
        EventoManager $evento,
        TipoEventoManager $tipoevento,
        EventoHistorialManager $eventohistorial
    ) {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->utils = $utils;
        $this->evento = $evento;
        $this->tipoevento = $tipoevento;
        $this->eventohistorial = $eventohistorial;
    }

    /**
     * graba un panico en la base de datos
     * @param type $panico
     * @return boolean
     */
    public function save($panico)
    {
        $this->em->persist($panico);
        $this->em->flush();
        return true;
    }

    public function listHistorial($organizacion, $desde, $hasta, $servicio)
    {
        return $this->em->getRepository('App:EventoHistorial')->obtenerByCodename($organizacion, 'EV_PANICO', $desde, $hasta, $servicio);
    }

    /**
     * Devuelve la lista 
     * @param Servicio $servicios
     * @return Panico $panicos
     */
    public function listPendientes($servicios)
    {
        return $this->em->getRepository('App:EventoHistorial')->obtenerSinAtencion($servicios, 0, null, null, 'EV_PANICO');
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Panico')->find($id);
    }

    /**
     * Devuelve todos los panicos nuevos y/o sin atención que se registren para
     * los servicios que son pasados como parametros. Lo de los servicios es 
     * importante porque se fijan en los panicos de dichos servicios solamente.
     */
    public function findNew($servicios, $limit = 5)
    {
        return $this->em->getRepository('App:Panico')->obtenerSinAtencion($servicios, $limit);
    }

    public function countNews($servicios)
    {
        return $this->em->getRepository('App:Panico')->contarSinAtencion($servicios);
    }

    public function delete($id)
    {
        $panico = $this->findById($id);
        if (!$panico) {
            throw $this->createNotFoundException('Código de Panico no encontrado.');
        }
        try {
            $this->em->remove($panico);
            $this->em->flush();
            return true;
        } catch (\Exception $exc) {
            return false;
        }
    }
}
