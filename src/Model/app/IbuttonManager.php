<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Ibutton;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of IbuttonManager
 *
 * @author nicolas
 */
class IbuttonManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($organizacion)
    {
        return $this->em->getRepository('App:Ibutton')->findByOrg($organizacion);
    }

    public function create($organizacion)
    {
        $ibutton = new Ibutton();
        $ibutton->setOrganizacion($organizacion);
        return $ibutton;
    }
    public function save($ibutton)
    {
        $this->em->persist($ibutton);
        $this->em->flush();
        return $ibutton;
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:Ibutton')->findOneBy(array('id' => $id));
    }

    public function findByCodigo($codigo)
    {
        return $this->em->getRepository('App:Ibutton')->findOneBy(['codigo' => $codigo]);
    }

    public function findByDallas($dallas)
    {
        return $this->em->getRepository('App:Ibutton')->findOneBy(array('dallas' => $dallas));
    }

    public function deleteById($id)
    {
        $ibutton = $this->findOne($id);
        if (!$ibutton) {
            throw $this->createNotFoundException('Código de ibutton no encontrado.');
        }
        return $this->delete($ibutton);
    }

    public function delete($ibutton)
    {
        $this->em->remove($ibutton);
        $this->em->flush();
        return true;
    }

    public function findByEstado($estado, $organizacion = null)
    {
        return $this->em->getRepository('App:Ibutton')->findBy(array('estado' => $estado, 'organizacion' => $organizacion), array('dallas' => 'ASC'));
    }
    public function findByChofer($chofer)
    {
        return $this->em->getRepository('App:Ibutton')->findByChofer($chofer);
    }
}
