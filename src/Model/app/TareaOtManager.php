<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\OrdenTrabajoManager;
use App\Entity\TareaOt;
use App\Entity\TareaOtProducto;
use App\Entity\TareaOtMecanico;
use App\Entity\TareaOtComentario;
use App\Entity\Organizacion as Organizacion;

/**
 * @author nicolas
 */
class TareaOtManager
{

    protected $em;
    private $otManager;

    public function __construct(EntityManagerInterface $em, OrdenTrabajoManager $otManager)
    {
        $this->otManager = $otManager;
        $this->em = $em;
    }

    public function save($tareaOt)
    {
        $this->em->persist($tareaOt);
        $this->em->flush();
        return $tareaOt;
    }

    public function findOne($id)
    {
        return $this->em->getRepository('App:TareaOt')->findOneBy(array('id' => intval($id)));
    }

    public function deleteById($id)
    {
        $tarea = $this->findOne($id);
        if (!$tarea) {
            throw $this->createNotFoundException('Código de tarea no encontrado.');
        }
        return $this->delete($tarea);
    }

    public function delete($tarea)
    {
        $this->em->remove($tarea);
        $this->em->flush();
        return true;
    }

    public function cerrar($tarea)
    {
        //totalizar y procesar los insumos
        foreach ($tarea->getProductos() as $prod) {
            $this->otManager->addProducto($tarea->getOrdenTrabajo(), $prod->getProducto()->getId(), $prod->getCantidad(), $prod->getCostoNeto());
        }

        foreach ($tarea->getMecanicos() as $meca) {
            $this->otManager->addMecanico($tarea->getOrdenTrabajo(), $meca->getMecanico()->getId(), $meca->getHoras());
        }

        //totalizar y procesar los mecanicos

        $tarea->setEstado(2);
        $this->save($tarea);

        return true;
    }

    public function create($ot)
    {
        $tarea = new TareaOt();
        $tarea->setOrdenTrabajo($ot);
        return $tarea;
    }

    public function addProducto($tareaot, $idProd, $cant, $precio = 0, $actPrecio = false)
    {
        $producto = $this->em->getRepository('App:Producto')->findOneBy(array(
            'id' => intval($idProd)
        ));
        $stock = $this->em->getRepository('App:Stock')->findByDepositoProducto($tareaot->getOrdenTrabajo()->getDeposito(), $producto);

        $unitario = $totalNeto = 0;
        if ($stock->getCosto() && $actPrecio) {
            if ($precio != 0) {  //deberia actualizar el precio
                $unitario = $precio;   //tomo el precio q viene del form
            } else {
                $unitario = $stock->getCosto(); //tomo el precio del stock
            }

            //si tengo el check de actualizar precio debo cambiar el stok.
            $stock->setCosto($precio);
            $stock->setUltCompra($tareaot->getFecha());
            $this->em->persist($stock);
        } else {
            $unitario = $precio;   //tomo el precio q viene del form
        }
        $totalNeto = $unitario * $cant;   //actualizo el total

        $otp = new TareaOtProducto();
        $otp->setCantidad($cant);
        $otp->setCostoUnitario($unitario);
        $otp->setCostoNeto($totalNeto);
        if (!is_null($stock->getProducto()->getTasaIva()) || $stock->getProducto()->getTasaIva() != 0) {
            $otp->setCostoTotal($totalNeto * (1 + $stock->getProducto()->getTasaIva() / 100));
        } else {
            $otp->setCostoTotal($totalNeto * 1.21);
        }
        $otp->setDeposito($tareaot->getOrdenTrabajo()->getDeposito());
        $otp->setProducto($producto);
        $otp->setTarea($tareaot);
        $tareaot->addProductos($otp);
        $producto->addTareaOt($otp);

        $this->em->persist($otp);
        $this->em->flush();
        $this->recalcularTarea($tareaot);
        return true;
    }

    private function recalcularTarea($tareaot)
    {
        $costoNeto = $costoP = $costoM = $costoTE = $costoHT = $costoNHT = 0;
        foreach ($tareaot->getProductos() as $prod) {
            $costoNeto += $prod->getCostoNeto() ? $prod->getCostoNeto() : 0;
            $costoP += $prod->getCostoTotal() ? $prod->getCostoTotal() : 0;
        }
        foreach ($tareaot->getMecanicos() as $meca) {
            $costoM += $meca->getCostoTotal() ? $meca->getCostoTotal() : 0;
        }

        $tareaot->setCostoNeto($costoNeto + $costoM + $costoTE + $costoNHT);
        $tareaot->setCostoTotal($costoP + $costoM + $costoTE + $costoHT);

        $this->em->persist($tareaot);
        $this->em->flush();
    }

    public function findProdOT($idOTP)
    {
        return $this->em->getRepository('App:TareaOtProducto')->findOneBy(array('id' => intval($idOTP)));
    }

    public function delProducto($tarea, $producto)
    {
        $this->em->remove($producto);
        $this->em->flush();
        $this->recalcularTarea($tarea);
        return true;
    }

    public function addMecanico($tareaot, $idMeca, $cant, $precio = 0, $actPrecio = false)
    {
        $mecanico = $this->em->getRepository('App:Mecanico')->findOneById(array(
            'id' => intval($idMeca)
        ));
        if ($mecanico) {

            $unitario = $total = 0;
            if ($precio != 0) {  //deberia actualizar el precio
                $unitario = $precio;   //tomo el precio q viene del form
            } else {
                $unitario = $mecanico->getCostoHora(); //tomo el precio del mecanico
            }
            $total = $unitario * $cant;

            $tpm = new TareaOtMecanico();
            $tpm->setHoras($cant);
            $tpm->setCosto($unitario);
            $tpm->setCostoTotal($total);
            $tpm->setMecanico($mecanico);
            $tpm->setTarea($tareaot);
            $tareaot->addMecanicos($tpm);
            $mecanico->addTareas($tpm);

            $this->em->persist($tpm);
            $this->em->flush();

            $this->recalcularTarea($tareaot);
            return true;
        } else {
            return false;
        }
    }

    public function delMecanico($tareaot, $idMeca)
    {
        $mecanico = $this->em->getRepository('App:TareaOtMecanico')->findOneBy(array(
            'id' => intval($idMeca)
        ));
        $this->em->remove($mecanico);
        $this->em->flush();
        $this->recalcularTarea($tareaot);
        return true;
    }

    public function findMecanico($idMeca, $idTarea)
    {
        $mecanico = $this->em->getRepository('App:TareaOtMecanico')->findOneBy(array(
            'mecanico' => $idMeca,
            'tarea' => $idTarea
        ));

        return $mecanico;
    }

    public function findProducto($idProd, $idTarea)
    {
        $producto = $this->em->getRepository('App:TareaOtProducto')->findOneBy(array(
            'producto' => $idProd,
            'tarea' => $idTarea
        ));

        return $producto;
    }

    public function editMecanico($tareaMecanico, $cant)
    {
        $unitario = $tareaMecanico->getMecanico()->getCostoHora(); //tomo el precio del mecanico
        $total = $unitario * $cant;
        $tareaMecanico->setHoras($cant);
        $tareaMecanico->setCosto($unitario);
        $tareaMecanico->setCostoTotal($total);
        $this->em->persist($tareaMecanico);
        $this->em->flush();
        $this->recalcularTarea($tareaMecanico->getTarea());
        return $tareaMecanico;
    }

    public function editProducto($tareaProducto, $cant, $precio = 0, $actPrecio = false)
    {
        $stock = $this->em->getRepository('App:Stock')->findByDepositoProducto($tareaProducto->getTarea()->getOrdenTrabajo()->getDeposito(), $tareaProducto->getProducto());
        $unitario = $totalNeto = 0;

        if ($stock->getCosto()) {
            if ($precio != 0) {  //deberia actualizar el precio
                $unitario = $precio;   //tomo el precio q viene del form
            } else {
                $unitario = $stock->getCosto(); //tomo el precio del stock
            }
            $totalNeto = $unitario * $cant;

            if ($actPrecio) {  //si tengo el check de actualizar precio debo cambiar el stok.
                $stock->setCosto($precio);
                $stock->setUltCompra(new \DateTime());
                $this->em->persist($stock);
            }
        }
        $tareaProducto->setCantidad($cant);
        $tareaProducto->setCostoUnitario($unitario);
        $tareaProducto->setCostoNeto($totalNeto);
        if (!is_null($stock->getProducto()->getTasaIva()) || $stock->getProducto()->getTasaIva() != 0) {
            $tareaProducto->setCostoTotal($totalNeto * (1 + $stock->getProducto()->getTasaIva() / 100));
        } else {
            $tareaProducto->setCostoTotal($totalNeto * 1.21);
        }
        $tareaProducto->setDeposito($tareaProducto->getTarea()->getOrdenTrabajo()->getDeposito());

        $this->em->persist($tareaProducto);
        $this->em->flush();

        $this->recalcularTarea($tareaProducto->getTarea());
    }

    public function addComentario($tareaot, $descripcion, $ejecutor)
    {
        $comentario = new TareaOtComentario();
        $comentario->setFecha(new \DateTime());
        $comentario->setComentario($descripcion);
        $comentario->setEjecutor($ejecutor);
        $comentario->setTarea($tareaot);
        $this->em->persist($comentario);
        $this->em->flush();
        return true;
    }
}
