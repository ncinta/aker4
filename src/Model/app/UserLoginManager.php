<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbonadoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use App\Entity\Usuario;
use App\Entity\TutorialShell;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class UserLoginManager
{

    protected $em;
    protected $security;
    protected $authorization_checker;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, Security $authorization_checker)
    {
        $this->em = $em;
        $this->security = $security;
        $this->authorization_checker = $authorization_checker;
    }

    public function usuariologueado()
    {
        return !is_null($this->security->getToken()->getUser());
    }

    public function getUser()
    {
        return $this->security->getToken()->getUser();
    }

    public function getOrganizacion()
    {
        return $this->getUser()->getOrganizacion();
    }

    public function getLatitud()
    {
        if ($this->getOrganizacion()->getLatitud() == null) {
            return 0;
        } else {
            return $this->getOrganizacion()->getLatitud();
        }
    }

    public function getLongitud()
    {
        if ($this->getOrganizacion()->getLongitud() == null) {
            return 0;
        } else {
            return $this->getOrganizacion()->getLongitud();
        }
    }

    public function isModuloEnabled($modulo = null)
    {
        $mod = $this->em->getRepository('App:Organizacion')->getModuloinOrganizacion($this->getOrganizacion(), $modulo);
        if ($mod != null) {
            return true;
        } else {
            return false;
        }
    }

    public function isMaster()
    {
        return $this->getUser() == $this->getOrganizacion()->getUsuarioMaster();
    }

    public function isDistribuidor()
    {
        return $this->getOrganizacion()->getTipoOrganizacion() == 1; //distribuidor
    }

    public function isCliente()
    {
        return $this->getOrganizacion()->getTipoOrganizacion() == 2; //cliente
    }

    public function isGranted($role)
    {
        return $this->authorization_checker->isGranted($role);
    }

    public function getTimezone()
    {
        if (is_null($this->getUser())) {
            return 'America/Buenos_Aires';
        }
        if ($this->getUser()->getTimezone() === '') {
            return 'America/Buenos_Aires';
        } else {
            return $this->getUser()->getTimezone();
        }
    }

    private $data = array(
        'TUTOENEX_ALL' => array(
            'Cómo empezar ?' => 'www.controlenruta.cl/videos/Pros000/Pros000.html',
            'Cómo ver la flota ?' => 'www.controlenruta.cl/videos/Pros001/Pros001.html',
            'Cómo crear una referencia ?' => 'www.controlenruta.cl/videos/Pros006/Pros006.html',
            'Cómo acceder al historial ?' => 'www.controlenruta.cl/videos/Pros002/Pros002.html',
            'Cómo ver las últimas posiciones ?' => 'www.controlenruta.cl/videos/Pros003/Pros003.html',
            'Cómo ver equipos cercanos a una referencia ?' => 'www.controlenruta.cl/videos/Pros004/Pros004.html',
            'Cómo administrar mi flota ?' => 'www.controlenruta.cl/videos/Pros005/Pros005.html',
            'Cómo crear un usuario ?' => 'www.controlenruta.cl/videos/Pros007/Pros007.html',
            'Cómo obtener las distancias recorridas ?' => 'www.controlenruta.cl/videos/Inf001/Inf001.html',
            'Cómo obtener los tiempos y velocidades recoridas?' => 'www.controlenruta.cl/videos/Inf002/Inf002.html',
            'Cómo obtener los excesos de velocidad ?' => 'www.controlenruta.cl/videos/Inf003/Inf003.html',
            'Cómo saber por cuales pórticos he pasado ?' => 'www.controlenruta.cl/videos/Inf004/Inf004.html',
            'Cómo saber por cuales peajes he pasado ?' => 'www.controlenruta.cl/videos/Inf005/Inf005.html',
            'Cómo saber por cuales referencias he pasado ?' => 'www.controlenruta.cl/videos/Inf007/Inf007.html',
            'Cómo registrar una carga de combustible ?' => 'www.controlenruta.cl/videos/Pros008/Pros008.html',
            'Cómo saber el consumo de combustible ?' => 'www.controlenruta.cl/videos/Inf006/Inf006.html',
        ),
        'TUTOENEX_HOME' => 'www.controlenruta.cl/videos/Pros000/Pros000.html',
        'TUTOENEX_VERFLOTA' => array(
            'Ver Flota' => 'www.controlenruta.cl/videos/Pros001/Pros001.html',
            'Crear Referencia' => 'www.controlenruta.cl/videos/Pros006/Pros006.html',
        ),
        'TUTOENEX_HISTORIAL' => 'www.controlenruta.cl/videos/Pros002/Pros002.html',
        'TUTOENEX_ULTPOSICIONES' => 'www.controlenruta.cl/videos/Pros003/Pros003.html',
        'TUTOENEX_CERCANIA' => 'www.controlenruta.cl/videos/Pros004/Pros004.html',
        'TUTOENEX_ADMINFLOTA' => 'www.controlenruta.cl/videos/Pros005/Pros005.html',
        'TUTOENEX_CREARUSUARIO' => 'www.controlenruta.cl/videos/Pros007/Pros007.html',
        'TUTOENEX_CARGACOMBUSTIBLE' => 'www.controlenruta.cl/videos/Pros008/Pros008.html',
        'TUTOENEX_INFODISTANCIAS' => 'www.controlenruta.cl/videos/Inf001/Inf001.html',
        'TUTOENEX_INFOTIEMPOS' => 'www.controlenruta.cl/videos/Inf002/Inf002.html',
        'TUTOENEX_INFOEXCESOS' => 'www.controlenruta.cl/videos/Inf003/Inf003.html',
        'TUTOENEX_INFOPORTICOS' => 'www.controlenruta.cl/videos/Inf004/Inf004.html',
        'TUTOENEX_INFOPEAJES' => 'www.controlenruta.cl/videos/Inf005/Inf005.html',
        'TUTOENEX_INFOCONSUMO' => 'www.controlenruta.cl/videos/Inf006/Inf006.html',
        'TUTOENEX_INFOPASO' => 'www.controlenruta.cl/videos/Inf007/Inf007.html',
    );

    public function findHelpShell($codepage)
    {
        if ($this->isModuloEnabled('CLIENTE_SHELL') && isset($this->data[$codepage])) {
            $urlshell = array(
                //         'count' => count($this->data[$codepage]),
                'url' => $this->data[$codepage]
            );
        } else {
            $urlshell = null;
        }
        return $urlshell;
        //        return  'www.controlenruta.cl/videos/Pros001/Pros001.html';
    }

    public function setUltEventoVisto($id) {
        $usr = $this->getUser();
        $usr->setUltEventoVisto($id);
        $this->em->persist($usr);
        $this->em->flush();
        return true;
    }

    public function getUltEventoVisto() {
        return $this->getUser()->getUltEventoVisto();
    }
}
