<?php

namespace App\Model\app\Base;

/**
 * Description of PanicoControlManager
 *
 * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
 */

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Model\sms\SmsManager;
use Symfony\Component\Mime\Email;

class NotificadorBaseManager
{

    protected $em;
    private $sms;
    private $container;
    private $mailer;
    private $survey_key = array(
        30 => 'panico',
        31 => 'panico',
        32 => 'panico'
    );

    public function __construct(EntityManagerInterface $em, ContainerInterface $container, MailerInterface $mailer)
    {
        $this->em = $em;
        $this->container = $container;
        $this->mailer = $mailer;
    }


    public function despacharSMS($destinos)
    {
        if ($destinos != null) {   //tengo destinos de SMS para enviar.
            $dest = array();
            foreach ($destinos as $destino) {
                $dest[] = array(
                    'numero' => $destino['destino'],
                    'texto' => $destino['mensaje']
                );
            }
            //die('////<pre>' . nl2br(var_export($dest, true)) . '</pre>////');
            $this->sms->batchSend($dest);
        }
        return true;
    }

    private function getSurveyKey($evento)
    {
        if (array_key_exists($evento, $this->survey_key)) {
            return !is_null($this->survey_key[$evento]) ? $this->survey_key[$evento] : "sat";
        }
        return 'sat';
    }

    protected function despacharPUSH($personas, $mensaje, $titulo, $evento = null)
    {
        $url = $this->container->getParameter("push.url");
        $uri = $this->container->getParameter("push.ws.send_message");
        $fields = array(
            'body' => $mensaje,
            'type' => 0,
            'format' => 1,
            'survey_key' => $this->getSurveyKey($evento),
            'name' => $titulo
        );
        //open connection
        $ch = curl_init();
        foreach ($personas as $persona) {
            $fields['phone'] = $persona['destino'];

            //url-ify the data for the POST
            $fields_string = '';
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }

            rtrim($fields_string, '&');
            curl_setopt($ch, CURLOPT_URL, $url . $uri);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_ENCODING, 'application/x-www-form-urlencoded');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //execute post
            $resp = curl_exec($ch);
        }
        //close connection
        curl_close($ch);
        return true;
    }

    public function despacharMail($destinos, $asunto, $cuerpo)
    {
        // destinos debe ser un array con las direcciones, ejemplo array('ncinta@sec....com.ar','cbrandolin@....')
        if (count($destinos) > 0) {
            $email = (new Email())
                ->subject($asunto)
                ->from('akercontrol@gmail.com')
                ->to(...$destinos) // ... es el operador spread lo que hace es separar cada valor del array.  
                ->html($cuerpo);
            try {
                $this->mailer->send($email);
                return true;
            } catch (\Symfony\Component\Mailer\Exception\TransportException $e) {
                echo ("error " . $e->getMessage() . "<br>");
            }
        } else {
            return false;
        }
    }
}
