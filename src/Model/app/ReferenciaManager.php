<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Usuario as Usuario;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Model\app\UtilsManager;
use App\Model\app\BitacoraManager;
use App\Model\app\UsuarioManager;
use App\Model\app\UserLoginManager;
use App\Model\app\KmlParser;
use App\Model\app\OrganizacionManager;
use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReferenciaManager
{

    protected $em;
    protected $userlogin;
    protected $usuario;
    protected $organizacion;
    protected $kml;
    protected $utils;
    protected $bitacora;
    protected $cliente;
    protected $container;

    public function __construct(
        EntityManagerInterface $em,
        UserLoginManager $userlogin,
        OrganizacionManager $organizacion,
        KmlParser $kml,
        UsuarioManager $usuario,
        UtilsManager $utils,
        BitacoraManager $bitacora,
        HttpClientInterface $cliente,
        ContainerInterface $container,

    ) {
        $this->userlogin = $userlogin;
        $this->organizacion = $organizacion;
        $this->em = $em;
        $this->kml = $kml;
        $this->usuario = $usuario;
        $this->utils = $utils;
        $this->bitacora = $bitacora;
        $this->cliente = $cliente;
        $this->container = $container;
    }

    /**
     * Para un grupo de referencias las pasa a un array s
     * @param type $referencias
     * @param type $fields
     * @return array 
     */
    public function toArray($referencias)
    {
        $resultado = array();
        foreach ($referencias as $referencia) {
            $resultado[] = array(
                $referencia->getId(),
                $referencia->getNombre(),
                $referencia->getWebPath(),
            );
        }
        return $resultado;
    }

    public function toArrayData($referencia, $action)
    {
        $data = array(
            'entity' => 'REFERENCIA',
            'action' => null,
            'data' => null
        );
        if ($referencia) {
            $data['data']['id'] = $referencia instanceof Referencia ? $referencia->getId() : $referencia;
            $data['data']['updated_at'] =  $referencia instanceof Referencia ? $referencia->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $referencia instanceof Referencia ? $referencia->getCreatedAt()->format('Y-m-d H:i:s') : null;
            if ($action == 2) {
                $data['action'] = 'DELETE';
                return $data;
            }
            $data['action'] = 'UPDATE';
            $data['data']['nombre'] = $referencia->getNombre();
            $data['data']['velocidadMaxima'] = $referencia->getVelocidadMaxima();
            $data['data']['latitud'] = (float)$referencia->getLatitud();
            $data['data']['longitud'] = (float)$referencia->getLongitud();
            $data['data']['clase'] = (int)$referencia->getClase(); // 1 = circ, 2 = polig
            if ($referencia->getClase() == 1) {
                $data['data']['radio'] = (float)$referencia->getRadio(); //si es circular
            } else {
                $data['data']['poligono'] = $this->parsePoligono($referencia->getPoligono());
            }
        }
        return $data;
    }

    private function Obj2ArrRecursivo($Objeto)
    {
        if (is_object($Objeto))
            $Objeto = get_object_vars($Objeto);
        if (is_array($Objeto))
            foreach ($Objeto as $key => $value)
                $Objeto[$key] = $this->Obj2ArrRecursivo($Objeto[$key]);
        return $Objeto;
    }

    /**
     * Retorna todas las referencias visibles de la organizacion.
     */
    public function findAllVisibles($organizacion = null)
    {
        if ($organizacion == null) {
            $organizacion = $this->userlogin->getOrganizacion();
        }
        return $this->em->getRepository('App:Referencia')->getReferenciasVisibles($organizacion);
    }


    /**
     * Retorna todos los grupo de las referencias.
     * @return type 
     */
    public function findAllGrupos()
    {
        $organizacion = $this->userlogin->getOrganizacion();
        return $this->em->getRepository('App:GrupoReferencia')->findAllGruposReferencias($organizacion);
    }

    public function findAllByGrupo($grupo, $orderBy = null)
    {
        if ($grupo instanceof GrupoReferencia) {
            $grp = $grupo;
        } else {
            $grp = $this->em->find('App:GrupoReferencia', $grupo);
        }

        if (is_null($orderBy)) {
            $ordenBy = array('nombre' => 'ASC');
        }
        return $this->em->getRepository('App:Referencia')->getReferenciasEnGrupo($grp, $ordenBy);
    }

    /**
     * Retorna todas las referencias asociadas a la organixacion.
     * @param type $entity
     * @param array $orderBy
     * @return type Referencias asociadas a la or
     */
    public function findAsociadas($entity = null, $solovisibleMapa = null)
    {
        $referencias = null;
        //aca valido porque criterio tengo que buscar.
        if (is_null($entity)) {     //la cosa viene por el usuario logueado
            $usr = $this->userlogin->getUser();
        } elseif ($entity instanceof Usuario) {       //le pase un usuario, debo buscar todo por usuario.
            $usr = $this->usuario->find($entity);
        } elseif ($entity instanceof Organizacion) {    //le pase una organizacion.
            $org = $this->organizacion->find($entity);
        } else {
            $usr = $this->userlogin->getUser();
        }

        //estoy buscando por usuario  pero es un usuario master, entonces busco por org.
        // si el usr no tiene grupos asociados, entonces debo buscar todas las ref de la organizacion.
        if (isset($usr)) {
            if ($usr->isMaster() || count($usr->getOrganizacion()->getGrupoReferencias()) == 0) {
                $org = $this->organizacion->find($this->userlogin->getOrganizacion());
            }
        }

        if (isset($org)) {     //todo viene por organizacion o estoy viendo un master.
            $referencias = $this->getReferencia($org, $solovisibleMapa);
        } else {                //ahora viene por usuario.
            $referencias = $this->em->getRepository('App:Referencia')->getByUsuario($usr, array('nombre' => 'ASC'), $solovisibleMapa);
            if (count($referencias) == 0) {
                $referencias = $this->getReferencia($usr->getOrganizacion(), $solovisibleMapa);
            }
        }


        return $referencias;
    }

    private function getReferencia($org, $solovisibleMapa = null)
    {
        // Obtenemos tanto las referencias propias como heredadas en una sola consulta optimizada
        //$referencias = $this->em->getRepository('App:Referencia')->getPropiasYHeredadasByOrganizacion($org, $solovisibleMapa);
        $referencias = $this->em->getRepository('App:Referencia')->getPropiasYHeredadasByOrganizacion($org);

        // Utilizamos un array asociativo para evitar duplicados por el ID de la referencia
        $uniqueReferences = [];
        foreach ($referencias as $ref) {
            $uniqueReferences[$ref->getId()] = $ref;  // Guardamos solo referencias únicas
        }

        // Convierto el array asociativo en un array de objetos
        $referenciasUnicas = array_values($uniqueReferences);

        // Ordenamos las referencias por nombre (preferiblemente en la consulta de SQL)
        usort($referenciasUnicas, function ($x, $y) {
            return strcasecmp($x->getNombre(), $y->getNombre());
        });

        return $referenciasUnicas;
    }


    function compararByNombre($x, $y)
    {
        return strcasecmp($x->getNombre(), $y->getNombre());
    }

    function search_if_exist($item, $objects)
    {
        foreach ($objects as $key => $value) {
            if ($value->getId() === $item->getId()) {
                return true;
            }
        }
        return false;
    }

    public function findAllSinAsignar2Grupo($grupo, Organizacion $organizacion)
    {
        $allRef = $this->findAsociadas($organizacion);  //traigo todas.
        $allGrp = $this->findAllByGrupo($grupo);

        $tmp = array();
        foreach ($allRef as $value) {
            if (!$this->search_if_exist($value, $allGrp)) {
                $tmp[] = $value;
            }
        }
        usort($tmp, array($this, 'compararByNombre'));
        return $tmp;
    }

    /**
     * Retorna todas las referencias segun el tipoReferencia que se le haya pasado.
     * Los valores posibles 
     * @param int $tipo 1=Referencia 2=Portico 3=Peaje 4=EESS
     * @return array(referencia) todas las referencias que tienen el tipo pasado.
     */
    public function findByTipo($tipo)
    {
        return $this->em->getRepository('App:Referencia')->findAllByTipo($tipo);
    }

    public function findByCodigoExterno($codigo)
    {
        return $this->em->getRepository('App:Referencia')->findOneBy(array('codigo_externo' => $codigo));
    }

    public function updateCodigoExterno($id, $codigo, $ciudad = null, $provincia = null)
    {
        $referencia = $this->find($id);
        $referencia->setCodigoExterno($codigo);
        if ($ciudad != null && $ciudad != "") {
            $referencia->setCiudad($ciudad);
        }

        if ($provincia != null && $provincia != "") {
            $provincia = $this->em->getRepository("App:Provincia")->findOneBy(array("code" => $provincia));
            $referencia->setProvincia($provincia);
        }

        return $this->save($referencia);
    }

    /**
     * Retorna todas las referencias segun el tipoReferencia que se le haya pasado.
     * Los valores posibles 
     * @param int $tipo 0=No check 1=check 2=re draw
     * @return array(referencia) todas las referencias que tienen el status pasado.
     */
    public function findByStatus($status, $cantidad = null)
    {
        return $this->em->getRepository('App:Referencia')->findAllByStatus($status, $cantidad);
    }

    /**
     * Trae todos los portales del sistema. (porticos y peajes)
     */
    public function getPortales()
    {
        $portales = array();
        $porticos = $this->findByTipo(2); //porticos
        foreach ($porticos as $portico) {
            $portales[$portico->getId()] = $portico;
        }
        $peajes = $this->findByTipo(3); //peajes
        foreach ($peajes as $peaje) {
            $portales[$peaje->getId()] = $peaje;
        }
        //die('////<pre>'.nl2br(var_export($portales, true)).'</pre>////');
        return $portales;
    }

    /**
     * Retorna todas las estaciones de servicio que estan asociadas a un usuario.
     */
    public function findEstacionesMapa()
    {
        $allRef = $this->findAsociadas($this->userlogin->getUser());
        $tmp = array();
        foreach ($allRef as $value) {
            if (!is_null($value->getTipoReferencia()) && $value->getTipoReferencia()->getId() == 4) {         //el 4 son las estaciones de servicio
                $tmp[] = $value;
            }
        }
        usort($tmp, array($this, 'compararByNombre'));
        return $tmp;
    }

    public function findCodigoExterno($codigo_externo)
    {
        return $this->em->getRepository('App:Referencia')->findOneBy(array(
            'codigo_externo' => $codigo_externo
        ));
    }

    public function findByNombreAndUsuario($nombre, $user)
    {
        if ($user->isMaster() || count($user->getGruposReferencias()) == 0) {
            $org = $this->userlogin->getOrganizacion();
            return $this->em->getRepository('App:Referencia')->getByNombreAndOrganizacion($nombre, $org);
        } else {
            return $this->em->getRepository('App:Referencia')->getByNombreAndUsuario($nombre, $user);
        }
    }

    public function findByNombre($nombre)
    {
        return $this->em->getRepository('App:Referencia')->findOneBy(array('nombre' => $nombre));
    }

    public function find($referencia)
    {
        if ($referencia instanceof Referencia) {
            return $this->em->find('App:Referencia', $referencia->getId());
        } else {
            return $this->em->find('App:Referencia', $referencia);
        }
    }

    public function create($padre = null)
    {
        $org = $this->organizacion->find($padre);
        if ($org) {
            //setea la latitud y longitud inicial para la referencia.
            if ($org->getLatitud() != 0) {
                $latitud = $org->getLatitud();
                $longitud = $org->getLongitud();
            } else {
                $latitud = $this->userlogin->getLatitud();
                $longitud = $this->userlogin->getLongitud();
            }
            $referencia = new Referencia();
            $referencia->setLatitud($latitud);
            $referencia->setLongitud($longitud);
            //            $referencia->addOrganizacion($org);
            $referencia->setPropietario($org);
            $org->addReferencia($referencia);
            return $referencia;
        } else {
            return null;
        }
    }

    public function save(Referencia $referencia)
    {
        if ($referencia->getKml() != null) {
            $kml = $referencia->uploadkml();
            $polidata = $this->importar_kml($referencia->getKmlPathFile())[0]; //solamente traemos la primera
            if ($polidata != null) {
                $referencia->setLatitud($polidata['latitud']);
                $referencia->setLongitud($polidata['longitud']);
                $referencia->setPoligono($polidata['poligono']);
            }
        }
        $referencia->addOrganizacion($referencia->getPropietario());
        if (is_null($referencia->getLatitud()) || is_null($referencia->getLongitud())) {
            return false;
        } else {
            if (is_null($referencia->getId())) { //no existe por lo tanto es nuevo
                $action = 1;
            } else { //existe por lo tanto se actualiza
                $action = 2;
            }
            $this->em->persist($referencia);
            $this->em->flush();
            // $this->bitacora->updateReferencia($this->userlogin->getUser(),$this->userlogin->getUser()->getOrganizacion(),$referencia, $action);
            return $referencia;
        }
    }

    public function saveKml($archivo)
    {
        $organizacion = $this->userlogin->getOrganizacion();
        try {
            $referencias = array();
            $polidatas = $this->importar_kml($archivo);
            foreach ($polidatas as $polidata) {
                $referencia = new Referencia();
                $referencia->setLatitud($polidata['latitud']);
                $referencia->setLongitud($polidata['longitud']);
                $referencia->setPoligono($polidata['poligono']);
                $referencia->setClase(2); //poligono
                $referencia->setNombre($polidata['nombre']);
                $referencia->setVisibilidad(true);
                $tipoRef = $this->em->getRepository('App:TipoReferencia')->findOneBy(array('id' => 1)); //tipo referencia
                $referencia->setTipoReferencia($tipoRef);
                $referencia->setPropietario($organizacion);
                if (is_null($referencia->getLatitud()) || is_null($referencia->getLongitud())) {
                    return false;
                } else {
                    if (is_null($referencia->getId())) { //no existe por lo tanto es nuevo
                        $action = 1;
                    } else { //existe por lo tanto se actualiza
                        $action = 2;
                    }
                    // die('////<pre>' . nl2br(var_export($organizacion->getId(), true)) . '</pre>////');
                    $referencia->addOrganizacion($organizacion);
                    $organizacion->addReferencia($referencia);
                    $this->em->persist($referencia);
                    $this->em->persist($organizacion);
                    $this->em->flush();
                    $referencias[] = $referencia;

                    // $this->bitacora->updateReferencia($this->userlogin->getUser(),$this->userlogin->getUser()->getOrganizacion(),$referencia, $action);
                }
            }
            return $referencias;
        } catch (Exception $e) {
            return false;
        }
    }

    private function parsePoligono($poligono)
    {
        $data = null;
        $arrPolig = explode(',', $poligono);
        // te dice si los poligonos son de formato viejo o nuevo (- se refiere al primer caracter de la primera coord)
        if (isset($arrPolig[0][0]) && $arrPolig[0][0] !== '-') {
            $arrPolig = array_slice($arrPolig, 3); // si es formato viejo eliminamos los colores que estan al ppio
        }
        $i = 0;
        $j = 1;
        foreach ($arrPolig as $key => $value) {
            if ($j <= count($arrPolig) - 1) {
                $data[] = array((float)$arrPolig[$i], (float) $arrPolig[$j]); //si es polig
                $i = $i + 2;
                $j = $j + 2;
            }
        }
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        return $data;
    }

    public function dataApiReferencia($referencia)
    {
        if (!is_null($referencia->getPoligono())) {
            return array(
                'id' => $referencia->getId(),
                'nombre' => $referencia->getNombre(),
                'latitud' => (float)$referencia->getLatitud(),
                'longitud' => (float)$referencia->getLongitud(),
                'clase' => 2, // 1 = circ, 2 = polig
                'radio' => 0, // 1 = circ, 2 = polig
                'codigo_externo' => $referencia->getCodigoExterno(),
                'poligono' => $this->parsePoligono($referencia->getPoligono()),
                'tipo' => array(
                    'id' => $referencia->getTipoReferencia()->getId(),
                    'nombre' => $referencia->getTipoReferencia()->getNombre(),
                )
            );
        } else {
            return null;
        }
    }

    private function getResponse($uri, $json)
    {
        return $this->cliente->request(
            'POST',
            $this->container->getParameter('aker_api_referencias') . $uri,
            array(
                'body' => $json,
                'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
            )
        );
    }

    public function check(Referencia $referencia)
    {

        if ($referencia->getStatus() != 1) {
            $data = $this->dataApiReferencia($referencia);
            if ($data) {
                $json = json_encode($data);

                try {
                    $response = $this->getResponse('/check', $json);

                    $statusCode = $response->getStatusCode();
                    if ($statusCode === 200) {
                        $result = json_decode($response->getContent(), true);
                        //  die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
                        return $result['valid'];
                    }
                } catch (TransportException $e) {
                    return false;
                }
            }

            return false;
        }
        return true;
    }

    public function fix(Referencia $referencia)
    {
        $data = $this->dataApiReferencia($referencia);
        if ($data) {
            $json = json_encode($data);
            try {
                $response = $this->getResponse('/fix', $json);

                $statusCode = $response->getStatusCode();
                if ($statusCode === 200) {
                    $result = json_decode($response->getContent(), true);
                    $poligono = "";
                    foreach ($result['poligono'] as $key => $coords) {
                        $poligono .= $key < (count($result['poligono']) - 1) ? implode(",", $coords) . ',' : implode(",", $coords);
                    }
                    $referencia->setPoligono($poligono);
                    $referencia->setArea($result['area']);
                    $referencia->setLatitud($result['latitud']);
                    $referencia->setLongitud($result['longitud']);
                    $referencia->setStatus(2);
                }
                return $referencia;
            } catch (TransportException $e) {
                return false;
            }
        }
        return $referencia;
    }
    public function copiar($referencia = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($referencia instanceof Referencia)) {
            $referencia = $this->find($referencia);
        }

        if ($referencia && $organizacion) {
            $referenciaDuplicada = new Referencia();
            $referenciaDuplicada->setAnguloDeteccion($referencia->getAnguloDeteccion());
            if (!is_null($referencia->getCategoria())) {
                $referenciaDuplicada->setCategoria($referencia->getCategoria());
            }
            $referenciaDuplicada->setCortePorRalenti($referencia->getCortePorRalenti());
            $referenciaDuplicada->setCreatedAt($referencia->getCreatedAt());
            $referenciaDuplicada->setDescripcion($referencia->getDescripcion());
            $referenciaDuplicada->setEntrada($referencia->getEntrada());
            $referenciaDuplicada->setLatitud($referencia->getLatitud());
            $referenciaDuplicada->setLongitud($referencia->getLongitud());
            $referenciaDuplicada->setNombre($referencia->getNombre() . ' (Copia)');
            $referenciaDuplicada->setPathIcono($referencia->getPathIcono());
            $referenciaDuplicada->setPoligono($referencia->getPoligono());
            $referenciaDuplicada->setRadio($referencia->getRadio());
            $referenciaDuplicada->setSalida($referencia->getSalida());
            $referenciaDuplicada->setColor($referencia->getColor());
            $referenciaDuplicada->setClase($referencia->getClase());

            if (!is_null($referencia->getTipoReferencia())) {
                $referenciaDuplicada->setTipoReferencia($referencia->getTipoReferencia());
            }

            $referenciaDuplicada->setVelocidadMaxima($referencia->getVelocidadMaxima());
            $referenciaDuplicada->setVisibilidad($referencia->getVisibilidad());
            $referenciaDuplicada->addOrganizacion($organizacion);
            $referenciaDuplicada->setPropietario($organizacion);
            $organizacion->addReferencia($referenciaDuplicada);
            $this->em->persist($referenciaDuplicada);
            $this->em->flush();
            return $referenciaDuplicada;
        } else {
            return false;
        }
    }

    public function compartir($referencia = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($referencia instanceof Referencia)) {
            $referencia = $this->find($referencia);
        }

        if ($referencia && $organizacion) {
            //buscar si ya esta compartida.
            $yacompartida = false;
            $refAsociadas = $organizacion->getReferencias();
            foreach ($refAsociadas as $ref) {
                if ($ref == $referencia) {
                    $yacompartida = true;
                }
            }
            if ($yacompartida == false) {
                $referencia->addOrganizacion($organizacion);    //agrego la organizacion a la referencia
                $organizacion->addReferencia($referencia);      //agrego la referencia a la organizacion
                $this->save($referencia);                       //grabo todo
                return true;
            } else {
                return -1;
            }
        } else {
            return false;
        }
    }

    public function mover($referencia = null, $organizacion = null)
    {
        //busco la organizacion sino viene en el parametro
        if (!($organizacion instanceof Organizacion)) {
            $organizacion = $this->organizacion->find($organizacion);
        }

        //busco la referencia sino viene en el parametro
        if (!($referencia instanceof Referencia)) {
            $referencia = $this->find($referencia);
        }

        if ($referencia && $organizacion) {
            $referencia->getPropietario()->removeReferencia($referencia);
            $referencia->setPropietario($organizacion);

            //sino existe la relacion la agrego, sino, solo cambio los propietarios
            $existe = $this->em->getRepository('App:Referencia')->ifExist($referencia, $organizacion);
            if (!$existe) {
                $referencia->addOrganizacion($organizacion);
                $organizacion->addReferencia($referencia);
            }

            $this->save($referencia);
            return true;
        }
        return false;
    }

    public function delete($referencia)
    {
        if (!($referencia instanceof Referencia)) {
            $referencia = $this->find($referencia);
        }
        $this->bitacora->deleteReferencia($this->userlogin->getUser(), $this->userlogin->getUser()->getOrganizacion(), $referencia);
        $this->em->remove($referencia);
        $this->em->flush();
    }

    public function remover($referencia, $organizacion)
    {
        $organizacion->removeReferencia($referencia);
        $this->em->persist($organizacion);
        $this->em->flush();
        return true;
    }

    private function importar_kml($fld)
    {
        // die('////<pre>' . nl2br(var_export($fld, true)) . '</pre>////');
        $kml = $this->kml->parse_kml($this->kml->read_kml($fld));
        $poligono = "";
        $referencias = array();
        $minlat = 0;
        $maxlat = 0;
        $minlon = 0;
        $maxlon = 0;
        $nombre = null;
        foreach ($kml["placemark"] as $placemark) {
            if (isset($placemark["polygon"])) {
                $style = substr($placemark["styleUrl"], 1);
                if (isset($kml["stylemap"][$style])) {
                    $style = substr($kml["stylemap"][$style], 1);
                }
                if (isset($kml["style"][$style]["line_width"])) {
                    $line_width = $kml["style"][$style]["line_width"];
                } else {
                    $line_width = 2;
                }
                //  die('////<pre>' . nl2br(var_export($placemark, true)) . '</pre>////');
                $line_color = isset($kml["style"][$style]["line_color"]) ? $kml["style"][$style]["line_color"] : 'ff000ff';
                $poly_color = isset($kml["style"][$style]["poly_color"]) ? $kml["style"][$style]["poly_color"] : $line_color;
                //$poligono = $line_width . "," . $this->kml_color_to_hargb($line_color) . "," . $this->kml_color_to_hargb($poly_color);                
                $coords = explode(" ", $placemark["polygon"]);
                $nombre = $placemark['name'];
                $poligono = [];
                for ($i = 0; $i < count($coords); $i++) {
                    $coord = explode(",", $coords[$i]);
                    $lat = trim($coord[1]) * 1.0;
                    $lon = trim($coord[0]) * 1.0;
                    if ($i == 0) {
                        $minlat = $maxlat = $lat;
                        $minlon = $maxlon = $lon;
                    } else {
                        if ($lat < $minlat)
                            $minlat = $lat;
                        if ($lon < $minlon)
                            $minlon = $lon;
                        if ($lat > $maxlat)
                            $maxlat = $lat;
                        if ($lon > $maxlon)
                            $maxlon = $lon;
                    }
                    $poligono[] = $lat . "," . $lon;
                }
            }
            $latitud = $minlat * 0.5 + $maxlat * 0.5;
            $longitud = $minlon * 0.5 + $maxlon * 0.5;

            $referencias[] = array(
                "latitud" => $latitud,
                "longitud" => $longitud,
                "poligono" => implode(",", $poligono),
                "line_width" => $line_width,
                "line_color" => $line_color,
                "poly_color" => $poly_color,
                "nombre" => $nombre
            );
        }

        //$direccion = get_esquina($latitud, $longitud);
        return $referencias;
    }

    private function kml_color_to_hargb($color)
    {
        return "#" . substr($color, 0, 2) . substr($color, 6, 2) . substr($color, 4, 2) . substr($color, 2, 2);
    }

    public function buscarCercania($referencia, $servicios, $limit = null)
    {
        //obtengo el centro de la referencia.
        $centroRef = $this->utils->getCentroReferencia($referencia);
        $tmp = array();
        foreach ($servicios as $servicio) {
            if (!is_null($servicio->getUltLatitud())) {
                //esta es la ultima posicion del servicio.
                $centroServ = array(
                    'latitud' => $servicio->getUltLatitud(),
                    'longitud' => $servicio->getUltLongitud()
                );

                //obtengo la distancia de la referencia al 
                $distancia = $this->utils->calcularDistancia($centroServ, $centroRef);
                if ($referencia->getClase() == $referencia::CLASE_POLIGONO) { //es poligonal
                    $adentro = $this->utils->adentro_poligono($servicio->getUltLatitud(), $servicio->getUltLongitud(), $referencia);
                } else { //es radial
                    $adentro = $distancia <= $referencia->getRadio();
                }
                $bearing = $this->utils->getBearing($centroServ, $centroRef);
                $tmp[] = array(
                    'servicio_id' => $servicio->getId(), //es el id de la referencia
                    'nombre' => $servicio->getNombre(),
                    'latitud' => $centroServ['latitud'],
                    'longitud' => $centroServ['longitud'],
                    'distancia' => intval($distancia),
                    'adentro' => $adentro,
                    'bearing' => $bearing,
                );
            }
        }
        //ordeno las referencias por el nombre
        usort($tmp, array($this, 'compararByDistancia'));
        if ($limit != null) {
            $tmp = array_splice($tmp, 0, $limit);
        }
        foreach ($tmp as $i => $t) {
            if ($t['distancia'] < 1000) {
                $tmp[$i]['strdistancia'] = number_format($t['distancia'], 0) . ' mts';
            } else {
                $tmp[$i]['strdistancia'] = number_format($t['distancia'] / 1000, 1) . ' kms';
            }
        }
        return $tmp;
    }

    private function compararByDistancia($x, $y)
    {
        return $x['distancia'] - $y['distancia'];
    }

    public function buscarCercaniaByDespacho($referencia, $latitud, $longitud, $limit = null)
    {
        //obtengo el centro de la referencia.
        $centroRef = $this->utils->getCentroReferencia($referencia);
        $tmp = array();
        $centroServ = array(
            'latitud' => $latitud,
            'longitud' => $longitud
        );

        //obtengo la distancia de la referencia al 
        $distancia = $this->utils->calcularDistancia($centroServ, $centroRef);
        if ($referencia->getClase() == $referencia::CLASE_POLIGONO) { //es poligonal
            $adentro = $this->utils->adentro_poligono($latitud, $longitud, $referencia);
        } else { //es radial
            $adentro = $distancia <= $referencia->getRadio();
        }
        $bearing = $this->utils->getBearing($centroServ, $centroRef);
        $tmp[] = array(
            'distancia' => intval($distancia),
            'adentro' => $adentro,
            'bearing' => $bearing,
        );

        //ordeno las referencias por el nombre
        usort($tmp, array($this, 'compararByDistancia'));
        if ($limit != null) {
            $tmp = array_splice($tmp, 0, $limit);
        }
        foreach ($tmp as $i => $t) {
            if ($t['distancia'] < 1000) {
                $tmp[$i]['strdistancia'] = number_format($t['distancia'], 0) . ' mts';
            } else {
                $tmp[$i]['strdistancia'] = number_format($t['distancia'] / 1000, 1) . ' kms';
            }
        }
        return $tmp;
    }

    public function buscarCercaniaByDespachoPuntoCarga($puntoCarga, $tramaServicio, $tramaPuntoCarga, $limit = null)
    {
        //establezco la posicion del servicio
        $centroServ = array(
            'latitud' => isset($tramaServicio['posicion']) ? $tramaServicio['posicion']['latitud'] : null,
            'longitud' => isset($tramaServicio['posicion']) ? $tramaServicio['posicion']['longitud'] : null
        );

        //si el ptoCarga es una referencia
        if ($puntoCarga->getReferencia()) {
            //es una referencia entonces debo solo ejecutar lo que esta ya armado para referencias.
            return $this->buscarCercaniaByDespacho($puntoCarga->getReferencia(), $centroServ['latitud'], $centroServ['longitud']);
        } else {
            //busco el centro del pto de carga del servicio
            $centroPuntoCarga = array(
                'latitud' => isset($tramaPuntoCarga['posicion']) ? $tramaPuntoCarga['posicion']['latitud'] : null,
                'longitud' => isset($tramaPuntoCarga['posicion']) ? $tramaPuntoCarga['posicion']['longitud'] : null
            );

            $tmp = array();

            //obtengo la distancia de la referencia al 
            $distancia = $this->utils->calcularDistancia($centroServ, $centroPuntoCarga);
            $bearing = $this->utils->getBearing($centroServ, $centroPuntoCarga);
            $tmp[] = array(
                'distancia' => intval($distancia),
                'adentro' => $distancia <= 1000,   //pongo 1km como para considerarlo adentro.
                'bearing' => $bearing,
            );

            //ordeno las referencias por el nombre
            usort($tmp, array($this, 'compararByDistancia'));
            if ($limit != null) {
                $tmp = array_splice($tmp, 0, $limit);
            }
            foreach ($tmp as $i => $t) {
                if ($t['distancia'] < 1000) {
                    $tmp[$i]['strdistancia'] = number_format($t['distancia'], 0) . ' mts';
                } else {
                    $tmp[$i]['strdistancia'] = number_format($t['distancia'] / 1000, 1) . ' kms';
                }
            }
            return $tmp;
        }
    }

    public function getPreciosPortales($referencia)
    {
        return $this->em->getRepository('App:Referencia')->findPreciosPortales($referencia);
    }

    public function getIds($id)
    {

        $ids = array();
        $referencias = array();
        $organizacion = $this->userlogin->getOrganizacion();
        $nombre = '';

        if (is_array($id)) {    //se paso un array de grupo o referencias
            $str = [];
            $referencias = [];
            foreach ($id as $item) {   //como es array debe recorrer uno a uno los items
                if ($item == '-1') {
                    $str[] = 'Sin referencias';
                } else if ($item == '0') {
                    $referencias = $this->findAsociadas($organizacion);
                    $str[] = 'Todas las referencias';
                } elseif (substr($item, 0, 1) == 'g') {
                    $referencias = array_merge($referencias, $this->findAllByGrupo(intval(substr($item, 1))));
                    $referencias = array_unique($referencias);   //elimina los duplicados
                    $grupo = $this->getGrupobyId(intval(substr($item, 1)));
                    $str[] = 'Grupo ' . $grupo->getNombre();
                } else {
                    $referencia = $this->find(intval($item));
                    if ($referencia != null) {
                        $str[] = $referencia->getNombre();
                        $referencias = array_merge($referencias, array($referencia));
                        $referencias = array_unique($referencias);   //elimina los duplicados
                    }
                }
            }
            $nombre = implode(', ', $str);
        } else {
            if ($id == '-1') {
                $nombre = 'Sin referencias';
            } else if ($id == '0') {
                $referencias = $this->findAsociadas($organizacion);
                $nombre = 'Todas las referencias';
            } elseif (substr($id, 0, 1) == 'g') {
                $referencias = $this->findAllByGrupo(intval(substr($id, 1)));
                $grupos = $referencias[0]->getGrupos();
                foreach ($grupos as $grupo) {
                    if ($grupo->getId() == intval(substr($id, 1))) {
                        $nombre = 'Grupo ' . $grupo->getNombre();
                    }
                }
            } else {
                $referencia = $this->find(intval($id));
                if ($referencia != null) {
                    $referencias = array($referencia);
                    $nombre = $referencia->getNombre();
                }
            }
        }

        foreach ($referencias as $referencia) {
            if ($referencia != null) {
                if ($referencia->getStatus() == null) { //no está checkeada
                    $check = $this->check($referencia);
                    if ($check) { //chequeo correcto
                        $referencia->setStatus(Referencia::STATUS_CHECK);
                        $this->save($referencia);
                    } else { //redibujamos
                        $fix = $this->fix($referencia);
                    }
                }
                $ids[] = $referencia->getId();
            }
        }
       // die('////<pre>' . nl2br(var_export($ids, true)) . '</pre>////');
        return array('nombre' => $nombre, 'ids' => $ids);
    }

    public function getGrupobyId($id)
    {
        return $this->em->find('App:GrupoReferencia', $id);
    }
}
