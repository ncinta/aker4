<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Dominio as Dominio;

class DominioManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($dominio)
    {
        return $this->em->getRepository('App:Dominio')->findByDominio($dominio);
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Dominio')->find($id);
    }

    public function delete($id)
    {
        $dominio = $this->findById($id);
        if ($dominio) {
            $this->em->remove($dominio);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
