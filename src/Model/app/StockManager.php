<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Stock as Stock;
use App\Entity\Producto as Producto;
use App\Entity\Deposito as Deposito;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UserLoginManager as UserLoginManager;

class StockManager
{

    protected $em;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;

    }

    public function find($stock)
    {
        if ($stock instanceof Stock) {
            return $this->em->getRepository('App:Stock')->findBy(array('id' => $stock->getId()));
        } else {
            return $this->em->getRepository('App:Stock')->find(array('id' => $stock));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Stock')->findByOrganizacion($organizacion->getId());
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($organizacion);
            return $this->em->getRepository('App:Stock')->findByOrganizacion($organizacion->getId());
        }
    }

    public function findByDepositoProducto($deposito, $producto)
    {
        return $this->em->getRepository('App:Stock')->findOneBy(array(
            'deposito' => $deposito->getId(),
            'producto' => $producto->getId(),
        ));
    }

    public function findByDeposito($deposito)
    {
        return $this->em->getRepository('App:Stock')->findByDeposito($deposito);
    }

    public function findByProducto($producto)
    {
        $stocks = array();
        $arr =  $this->em->getRepository('App:Stock')->findByProducto($producto);
        $organizacion = $this->userlogin->getOrganizacion();
        foreach($arr as $stock){
            if($stock->getDeposito()->getOrganizacion() == $organizacion){
                $stocks[] = $stock;
                //die('////<pre>' . nl2br(var_export($organizacion->getNombre(), true)) . '</pre>////');
            }
        }
        return $stocks;
    }

    public function create()
    {
        $stock = new Stock();
        return $stock;
    }

    public function save(Stock $stock)
    {
        $this->em->persist($stock);
        $this->em->flush();
        return $stock;
    }

    public function delete(Stock $stock)
    {
        $this->em->remove($stock);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $stock = $this->find($id);
        if (!$stock) {
            throw $this->createNotFoundException('Código de stock no encontrado.');
        }
        return $this->delete($stock);
    }

    public function agregar(Deposito $deposito, Producto $producto, $cantidad, $fecha = null, $proveedor = null, $costo = null)
    {
        $stock = $this->findByDepositoProducto($deposito, $producto);
        if (!$stock) {
            $stock = new Stock();
            $stock->setDeposito($deposito);
            $stock->setProducto($producto);
        }

        //calculo stock nuevo
        $stock_actual = $stock->getStock();
        $stock->setStock($stock_actual + floatval($cantidad));

        $stock->setUltCompra($fecha);
        if ($costo) {
            $stock->setCosto($costo);
        }
        if ($proveedor) {
            $stock->setUltProveedor($proveedor);
        }
        return $this->save($stock);
    }

    public function restarStock(Deposito $deposito, $producto, $cantidad, $unitario = null)
    {
        $stock = $this->em->getRepository('App:Stock')->findByDepositoProducto($deposito, $producto);
        if (is_null($stock)) {
            $stock = new Stock();
            $stock->setDeposito($deposito);
            $stock->setProducto($producto);
        }
        $stock_actual = $stock->getStock();
        if (is_null($stock_actual)) {
            $stock->setStock($cantidad * -1);
        } else {
            $stock->setStock($stock_actual - $cantidad);
        }
        if (!is_null($unitario)) {
            $stock->setCosto($unitario);
        }
        return $this->save($stock);
    }

    public function actualizarPrecio($stock, $nuevoPrecio)
    {
       
        //calculo stock nuevo
        $stock->setCosto($nuevoPrecio);
        return $this->save($stock);
    }
}
