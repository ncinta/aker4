<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Actividad as Actividad;
use App\Entity\ActividadProducto as ActividadProducto;
use App\Entity\Mantenimiento as Mantenimiento;

/**
 * Description of ActividadProductoManager
 *
 * @author nicolas
 */
class ActividadProductoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($actividad)
    {
        if ($actividad instanceof ActividadProducto) {
            return $this->em->getRepository('App:ActividadProducto')->findBy(array('id' => $actividad->getId()));
        } else {
            return $this->em->getRepository('App:ActividadProducto')->find(array('id' => $actividad));
        }
    }

    public function save(ActividadProducto $actProd)
    {
        $this->em->persist($actProd);
        $this->em->flush();
        return $actProd;
    }

    public function delete(ActividadProducto $actProd)
    {
        $this->em->remove($actProd);
        $this->em->flush();
        return true;
    }
}
