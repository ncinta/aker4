<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contacto as Contacto;
use App\Entity\Equipo as Equipo;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Entity\Evento as Evento;
use App\Model\app\ContactoManager;
use App\Model\app\EquipoManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\EventoManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;

/**
 * Description of NotificadorAgenteManager
 *
 * @author nicolas
 */
class NotificadorAgenteManager
{

    protected $equipoManager;
    protected $servicioManager;
    protected $referenciaManager;
    protected $grupoReferenciaManager;
    protected $eventoManager;
    protected $contactoManager;
    protected $security;
    protected $em;
    protected $cliente;
    protected $container;
    protected $utilsManager;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        HttpClientInterface $cliente,
        EquipoManager $equipoManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoManager $eventoManager,
        ContactoManager $contactoManager,
        ContainerInterface $container,
        UtilsManager $utilsManager
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->equipoManager = $equipoManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoManager = $eventoManager;
        $this->contactoManager = $contactoManager;
        $this->cliente = $cliente;
        $this->container = $container;
        $this->utilsManager = $utilsManager;
    }

    public function notificar($entity, $action)
    {
        // dd(get_class($entity));
        $json = null;
        if ($entity instanceof Equipo) {
            $json = json_encode($this->equipoManager->toArrayData($entity, $action));
        } elseif ($entity instanceof Servicio) {
            $json = json_encode($this->servicioManager->toArrayData($entity, $action));
        } elseif ($entity instanceof Referencia) {
            $json = json_encode($this->referenciaManager->toArrayData($entity, $action));
        } elseif ($entity instanceof GrupoReferencia) {
            $json = json_encode($this->grupoReferenciaManager->toArrayData($entity, $action));
        } elseif ($entity instanceof Evento) {
            $json = json_encode($this->eventoManager->toArrayData($entity, $action));
        } elseif ($entity instanceof Contacto) {
            $json = json_encode($this->contactoManager->toArrayData($entity, $action));
        } else {
            return false;
        }
        //dd($json);
        return $this->sendData($json);
    }

    public function sendData($json)
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_aws_addr_config'),
                array(
                    'body' => $json,
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    'auth_basic' => array(
                        $this->container->getParameter('aker_aws_usr_config'),
                        $this->container->getParameter('aker_aws_pass_config'),
                    )
                )
            );

            $statusCode = $response->getStatusCode();

            if ($statusCode === 200) {
                $result = $response->getContent();
            }
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 ? true : false;
    }


    public function getTokenChr()
    {
        if ($this->container->getParameter('aker_addr_chr_enabled') == true) {
            $result = 'ERROR';
            $json = array(
                "client_id" => $this->container->getParameter('aker_usr_chr'),
                "client_secret" => $this->container->getParameter('aker_pass_chr'),
                "audience" =>  $this->container->getParameter('aker_audience_chr'),
                "grant_type" => "client_credentials"
            );
            try {
                $response = $this->cliente->request(
                    'POST',
                    $this->container->getParameter('aker_addr_chr_token'),
                    array(
                        'body' => json_encode($json),
                        'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    )
                );

                $statusCode = $response->getStatusCode();
                //  die('////<pre>'.nl2br(var_export($statusCode, true)).'</pre>////');

                if ($statusCode === 200) {
                    $json_result = $response->getContent();
                    $result = json_decode($json_result, true);
                    return isset($result['access_token']) ? $result['access_token'] : null;
                }
            } catch (TransportException $e) {
                return false;
            }

            return $statusCode === 200 ? true : false;
        } else {
            return false;
        }
    }

    public function sendDataChr($json, $token)
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_addr_chr'),
                array(
                    'timeout' => 10,  //10seg de espera                 
                    'body' => $json,
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    'auth_bearer' =>  $token
                )
            );
            $statusCode = $response->getStatusCode();
            //die('////<pre>'.nl2br(var_export($statusCode, true)).'</pre>////');
            if ($statusCode === 200 || $statusCode === 201) {
                $result = $response->getContent();
            }
            return $statusCode;
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 || $statusCode === 201 ? true : false;
    }


    public function getHashReingenio($user)
    {
        $nombre_apellido = $this->utilsManager->separarNombre($user->getNombre());
        //   die('////<pre>'.nl2br(var_export($nombre_apellido, true)).'</pre>////');
        $result = 'ERROR';
        $json = array(
            "firstname" => $nombre_apellido['nombre'],
            "lastname" => $nombre_apellido['apellido'],
            "doc" =>  0,
            "email" => $user->getEmail()
        );
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_addr_reingenio_hash'),
                array(
                    'body' => json_encode($json),
                    'headers' => array(
                        'Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json',
                        'api_key' =>  $this->container->getParameter('aker_reingenio_apikey'),
                    ),
                )
            );

            $statusCode = $response->getStatusCode();

            if ($statusCode === 200 || $statusCode === 201) {
                $json_result = $response->getContent();
                //   die('////<pre>'.nl2br(var_export($json_result, true)).'</pre>////');
                $result = json_decode($json_result, true);
                return isset($result['hash']) ? $result['hash'] : null;
            }
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 || $statusCode === 201 ? true : false;
    }

    public function getUrlReingenio($hash)
    {

        //   die('////<pre>'.nl2br(var_export($nombre_apellido, true)).'</pre>////');
        $result = 'ERROR';
        $url = null;
        try {
            $response = $this->cliente->request(
                'GET',
                $this->container->getParameter('aker_addr_reingenio_login') . $hash,
                array(
                    'headers' => array(
                        'Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json',
                        'api_key' =>  $this->container->getParameter('aker_reingenio_apikey'),
                    ),
                )
            );

            $statusCode = $response->getStatusCode();

            if ($statusCode === 200 || $statusCode === 201) {
                $url = $response->getContent();

                return $statusCode === 200 ? $url : null;
            }
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 ? $url : false;
    }

    public function getTokenWara()
    {
        if ($this->container->getParameter('aker_addr_wara_enabled') == true) {
            $result = 'ERROR';
            $json = array(
                "user" => $this->container->getParameter('aker_usr_wara'),
                "password" => $this->container->getParameter('aker_pass_wara'),

            );
            try {
                $response = $this->cliente->request(
                    'POST',
                    $this->container->getParameter('aker_addr_wara_token'),
                    array(
                        'body' => json_encode($json),
                        'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    )
                );

                $statusCode = $response->getStatusCode();

                if ($statusCode === 200) {
                    $json_result = $response->getContent();
                    $result = json_decode($json_result, true);
                    // die('////<pre>'.nl2br(var_export($result, true)).'</pre>////');
                    return isset($result['SessionToken']) ? $result['SessionToken'] : null;
                }
            } catch (TransportException $e) {
                return false;
            }

            return $statusCode === 200 ? true : false;
        } else {
            return false;
        }
    }

    public function getDataWara($token)
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_addr_wara'),
                array(
                    'timeout' => 10,  //10seg de espera                 
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    'auth_bearer' =>  $token
                )
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200 || $statusCode === 201) {
                $result = $response->getContent();
                //      die('////<pre>'.nl2br(var_export($result, true)).'</pre>////');
            }
            return $result;
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 ? true : false;
    }
}
