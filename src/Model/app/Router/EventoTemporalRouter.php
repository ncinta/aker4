<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;


/**
 * Description of EventoHistoricoRouter
 *
 * @author nicolas
 */
class EventoTemporalRouter extends BasicRouter
{
    public function btnVisto()
    {
        if ($this->userlogin->isGranted('ROLE_EVENTO_REGISTRAR')) {
            return array(
                'label' => 'Vistos',
                'imagen' => 'fa fa-eye',
                'url' => '#visto',               
                'extra' => 'onclick=batch("visto") id=btnVisto style=display:none',
            );
        }
    }

    public function btnCheckAll()
    {
        if ($this->userlogin->isGranted('ROLE_EVENTO_REGISTRAR')) {
            return array(
                'label' => 'Marcar todos',
                'imagen' => 'fa fa-check',
                'url' => '#selectall',
                'extra' => 'onclick=checkAll() id=hrefselectAll',
            );
        }
    }

    public function btnRefresh() {
        return array(
            'label' => '15',
            'imagen' => 'fa fa-refresh',            
            'url' => '#',
            'class' => 'btn btn-default btn-success',
            'extra' => 'onClick=reload() id=btnActualizar',
            'title' => 'Actualiza los datos de los eventos que no han sido atendidos'
        );        
    }

    public function btnPauseRefresh() {
        return array(
            'label' => 'Pausar',
            'imagen' => 'fa fa-pause',            
            'url' => '#',
            'extra' => 'onClick=pauseReload() id=btnPauseReload',
            'title' => 'Permite pausar/reanudar la recarga de datos automatica. Uselo para cuando tenga que hacer operaciones con los datos.'
        );        
    }
}
