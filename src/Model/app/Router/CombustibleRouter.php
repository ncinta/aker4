<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;
use Symfony\Component\Routing\RouterInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;

class CombustibleRouter extends BasicRouter
{

    public function __construct(RouterInterface $router, UserLoginManager $user, OrganizacionManager $organizacionManager)
    {
        parent::__construct($router, $user);
        $this->orgManager = $organizacionManager;
    }

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnNewCarga($servicio)
    {
        if ($this->orgManager->isModuloEnabled($servicio->getOrganizacion(), 'MANAGER_COMBUSTIBLE')) {  //solo en los clientes
            if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_ADMIN')) {
                return $this->armarArray('Carga', 'fa fa-plus', $this->router->generate('fuel_new', array('id' => $servicio->getId())));
            }
        }
        return null;
    }

    public function btnShow($carga)
    {
        if ($this->userlogin->isGranted('ROLE_MASTER_CLIENTE') || $this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
            return $this->armarArray('Ver', 'fa fa-eye', $this->router->generate('fuel_show', array('id' => $carga->getId())));
        }
        return null;
    }

    public function btnListCarga($servicio)
    {
        return $this->armarArray('Cargas de Combustible', 'fa fa-flask', $this->router->generate('fuel_list', array('id' => $servicio->getId())));
    }

    public function btnEdit($carga)
    {
        //        if ($this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
        return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('fuel_edit', array('id' => $carga->getId())));
        //        }
        //        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnInformeRendimientoCargas($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO')) {
            return $this->armarArray('Rendimiento por Cargas', 'fa fa-file-text-o', $this->router->generate('fuel_info_rendimientocargas', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnInformeRendimiento($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO')) {
            return $this->armarArray('Rendimiento', 'fa fa-file-text-o', $this->router->generate('fuel_info_rendimiento', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnGraficoConsumo($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_GRAFICO_CONSUMO')) {
            return $this->armarArray('Gráfico Consumo', 'fa fa-bar-chart', $this->router->generate('fuel_grafico', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnImportar($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_IMPORTACION')) {
            return $this->armarArray(
                'Importar Cargas', 
                'glyphicon glyphicon-import', 
                $this->router->generate('fuel_import_index', array('id' => $organizacion->getId())),
                'Importa las cargas desde un excel.'
            );
        }
        return null;
    }

    public function btnChecker($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_IMPORTACION')) {
            return $this->armarArray(
                'Verificador', 
                'fa fa-file-excel-o', 
                $this->router->generate('fuel_check_index', array('id' => $organizacion->getId())),
                'Importa y verifica las cargas desde un archivo excel.');
        }
        return null;
    }
    public function btnExportar($servicio, $desde, $hasta)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_EXPORTAR')) {
            return $this->armarArray('Exportar', 'fa fa-download', $this->router->generate('fuel_list_export', array(
                'idservicio' => $servicio->getId(),
                'desde' => $desde,
                'hasta' => $hasta
            )));
        }
        return null;
    }

    public function btnMasiva($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_AGREGAR')) {
            return $this->armarArray('Cargas', 'fa fa-plus', $this->router->generate('fuel_newmasiva', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnInformeCarga($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
            return $this->armarArray('Informe de Cargas', 'fa fa-file-text', $this->router->generate('fuel_informe_carga', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnInformeDespacho($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
            return $this->armarArray('Informe Despacho', 'fa fa-file-text ', $this->router->generate('fuel_informe_despacho', array('id' => $organizacion->getId())));
        }
        return null;
    }


}
