<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;


/**
 * Description of EventoHistoricoRouter
 *
 * @author claudio
 */
class EventoRouter extends BasicRouter
{
    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_EVENTO_AGREGAR')) {
            return $this->armarArray('Evento', 'fa fa-plus', $this->router->generate('main_evento_new', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }
}
