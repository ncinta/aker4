<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class GrupoServicioRouter extends BasicRouter
{

    public function btnNew()
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_GRUPO_AGREGAR')) {
            return $this->armarArray('Grupo Servicio', 'fa fa-plus', $this->router->generate('apmon_grupo_servicio_new'));
        } else {
            return null;
        }
    }

    public function btnEdit($grupoServ)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_GRUPO_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('apmon_grupo_servicio_edit', array('id' => $grupoServ->getId())));
        } else {
            return null;
        }
    }

    public function btnAgrupar($grupoServ)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_GRUPO_EDITAR')) {
            return $this->armarArray(
                'Agrupar Servicios',
                'fa fa-plus',
                $this->router->generate('apmon_grupo_servicio_agrupar', array('id' => $grupoServ->getId()))
            );
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_GRUPO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
