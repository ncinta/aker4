<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class TransporteRouter extends BasicRouter
{

    public function btnNew($transporte)
    {
        if ($this->userlogin->isGranted('ROLE_TRANSPORTE_AGREGAR')) {
            return $this->armarArray('Transporte', 'fa fa-plus', $this->router->generate('transporte_new', array('idOrg' => $transporte->getId())),'Agregar Transporte');
        } else {
            return null;
        }
    }

    public function btnEdit($transporte)
    {
        if ($this->userlogin->isGranted('ROLE_TRANSPORTE_EDITAR')) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('transporte_edit', array('id' => $transporte->getId())),'Modificar Transporte');
        }
        return null;
    }

    public function btnDelete($transporte)
    {
        if ($this->userlogin->isGranted('ROLE_TRANSPORTE_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'Title' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
