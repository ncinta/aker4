<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class InformeRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        return $this->armarArray('Informe', 'fa fa-plus', $this->router->generate('informe_new', array('id' => $organizacion->getId())), 'Nuevo Informe');

    }


}
