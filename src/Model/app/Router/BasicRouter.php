<?php

namespace App\Model\app\Router;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use App\Model\app\UserLoginManager;

abstract class BasicRouter
{

    protected $router;
    protected $userlogin;

    public function __construct(RouterInterface $router, UserLoginManager $user)
    {
        $this->router = $router;
        $this->userlogin = $user;
    }

    protected function armarArray($text, $image = null, $url = null, $title = null, $id = null)
    {
        if (is_null($url)) {
            return null;
        } else {
            return array(
                'label' => $text,
                'imagen' => $image,
                'url' => $url,
                'title' => $title,
                'id' => $id,
            );
        }
    }

    /**
     * Hace que el formato de la estrucutra de botones de aker pase a ser de calypso
     * lo que hace compatible hacia atras los templates. Se deja de usar esta funcion cuando
     * se implemente el nuevo sistema de botones.
     * @param type $menu
     * @return type
     */
    public function toCalypso($menu)
    {
        $tmp = array();
        foreach ($menu as $submenu) {
            foreach ($submenu as $item) {
                if (is_null($submenu) || is_null($item)) {
                } else {
                    $tmp[$item['label']] = $item;
                }
            }
        }
        return $tmp;
    }
}
