<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class LogisticaRouter extends BasicRouter
{

    public function btnNew($logistica)
    {
        if ($this->userlogin->isGranted('ROLE_EMPRESA_AGREGAR')) {
             return $this->armarArray('Logistica', 'fa fa-plus', $this->router->generate('logistica_new', array('idOrg' => $logistica->getId())),'Agregar Logística');
        } else {
            return null;
        }
    }

    public function btnEdit($logistica)
    {
        if ($this->userlogin->isGranted('ROLE_EMPRESA_EDITAR')) {
           return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('logistica_edit', array('id' => $logistica->getId())),'Modificar Logística');
        }
        return null;
    }

    public function btnDelete($logistica)
    {
        if ($this->userlogin->isGranted('ROLE_TRANSPORTE_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'title' => 'Eliminar Logística',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }


    public function btnNewContacto()
    {
        if ($this->userlogin->isGranted('ROLE_CONTACTO_AGREGAR')) {
            return array(
                'label' => 'Contacto',
                'title' => '',
                'imagen' => 'fa fa-plus',
                'url' => '#modalContacto',
                'extra' => 'data-toggle=modal',
            );
        } else {
            return null;
        }
    }
}
