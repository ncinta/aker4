<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class ContactoRouter extends BasicRouter
{

    public function btnPopuNew()
    {

        return array(
            'label' => 'Contacto',
            'title' => 'Agregar Contacto',
            'imagen' => 'fa fa-plus',
            'url' => '#modalContacto',
            'extra' => 'data-toggle=modal',
        );
    }

    public function btnNew($organizacion)
    {

        if ($this->userlogin->isGranted('ROLE_CONTACTO_AGREGAR')) {
            return $this->armarArray('Contacto', 'fa fa-plus', $this->router->generate('main_contacto_new', array('id' => $organizacion->getId())));
        }
        return null;
    }


    public function btnEdit($contacto)
    {
        if ($this->userlogin->isGranted('ROLE_CONTACTO_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('main_contacto_edit', array('id' => $contacto->getId())));
        }
        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_CONTACTO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
