<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class ChoferRouter extends BasicRouter
{

    public function btnNew()
    {
        return array(
            'label' => 'Chofer',
            'Title' => 'Chofer',
            'imagen' => 'fa fa-plus',
            'url' => '#modalChoferNew',
            'extra' => 'data-toggle=modal',
        );
    }
    public function btnExport()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_VER')) {
            return $this->armarArray('Exportar', 'fa fa-pencil-square-o',$this->router->generate('apmon_chofer_exportar'));
        }
        return null;
    }
       
    public function btnEdit($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Chofer', 'fa fa-pencil-square-o',$this->router->generate('apmon_chofer_edit',array('id'=> $chofer->getId())));
        }
        return null;
    }  

    public function btnContactos($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_VER')) {
            return $this->armarArray('Contactos', 'fa fa-address-card', $this->router->generate('apmon_chofer_notificacion_list', array('idorg' => $organizacion->getId())));
        }
        return null;
    }

    public function btnRetirarIbutton($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Retirar Ibutton', 'fa fa-minus', $this->router->generate('apmon_chofer_desasignar_ibutton', array('id' => $chofer->getId())));
        }
        return null;
    }
        public function btnAsignarIbutton($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Asignar Ibutton', 'fa fa-plus', $this->router->generate('apmon_chofer_asignar_ibutton', array('id' => $chofer->getId())));
        }
        return null;
    }   
     public function btnRetirarServicio($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Retirar Servicio', 'fa fa-minus', $this->router->generate('apmon_chofer_desasignar_servicio', array('id' => $chofer->getId())));
        }
        return null;
    }
        public function btnAsignarServicio($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Asignar Servicio', 'fa fa-plus', $this->router->generate('apmon_chofer_asignar_servicio', array('id' => $chofer->getId())));
        }
        return null;
    }   
     
    public function btnEntregarServicio($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Entregar', 'fa fa-minus', $this->router->generate('apmon_chofer_entregar_servicio', array('id' => $chofer->getId())), 'Se hace la entrega del servicio por parte del chofer actual.');
        }
        return null;
    }
        public function btnTomarServicio($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_EDITAR')) {
            return $this->armarArray('Tomar', 'fa fa-plus', $this->router->generate('apmon_chofer_tomar_servicio', array('id' => $chofer->getId())), 'Se hace la entrega del servicio por parte del chofer actual.');
        }
        return null;
    }
      
    public function btnApicode($chofer)
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_VER')) {
            return $this->armarArray('Apicode', 'fa fa-qrcode', $this->router->generate('apmon_chofer_generarapicode', array('id' => $chofer->getId())));
        }
        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'title' => 'Eliminar Chofer',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }


}
