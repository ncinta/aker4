<?php

namespace App\Model\app\Router;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use App\Model\app\Router\BasicRouter;

class DistribuidorRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_AGREGAR')) {
            return $this->armarArray('Agregar', 'fa fa-plus', $this->router->generate('distribuidor_new', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnShow($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_VER')) {
            return $this->armarArray('Ver', 'fa fa-eye', $this->router->generate('distribuidor_show', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnEdit($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('distribuidor_edit', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnAsociarModulo($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            return $this->armarArray('Modulos', 'fa fa-pencil-square-o', $this->router->generate('distribuidor_asociar_modulo', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
