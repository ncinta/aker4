<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class EquipoRouter extends BasicRouter
{

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_EQUIPO_AGREGAR')) {
            return $this->armarArray('Equipo', 'fa fa-plus', $this->router->generate('sauron_equipo_new', array('idorg' => $organizacion->getId())));
        }
        return null;
    }

    public function btnShow($carga)
    {
        if ($this->userlogin->isGranted('ROLE_MASTER_CLIENTE') || $this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
            return $this->armarArray('Ver', 'fa fa-eye', $this->router->generate('fuel_show', array('id' => $carga->getId())));
        }
        return null;
    }

    public function btnListCarga($servicio)
    {
        return $this->armarArray('Cargas de Combustible', 'fa fa-flask', $this->router->generate('fuel_list', array('id' => $servicio->getId())));
    }

    public function btnEdit($carga)
    {
        //        if ($this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
        return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('fuel_edit', array('id' => $carga->getId())));
        //        }
        //        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
