<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

// colocar acá todos los botones del panel de control (user AKER) para no tener que crear un router por cada configuración
class PanelControlRouter extends BasicRouter
{

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnNewMotivoTx()
    {
        if ($this->userlogin->isGranted('ROLE_ROOT')) {
            return $this->armarArray('Motivo Tx', 'fa fa-plus', $this->router->generate('motivotx_new', array()));
        }
        return null;
    }

    public function btnEditMotivoTx($motivotx)
    {
        if ($this->userlogin->isGranted('ROLE_ROOT')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('motivotx_edit', array('id' => $motivotx->getId())));
        }
        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_ROOT')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
