<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

/**
 * Description of ProyectoRouter
 *
 * @author nicolas
 */
class ProyectoRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Proyecto',
            'imagen' => 'fa fa-plus',
            'url' =>  $this->router->generate('gpm_proyecto_new', array('idorg' => $organizacion->getId())),
            'class' => 'btn btn-primary',
        );
        //        } else {
        //            return null;
        //        }
    }

    public function btnMotivo($organizacion)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Motivos',
            'imagen' => 'fa fa-plus',
            'url' =>  $this->router->generate('gpm_motivogpm_list', array('id' => $organizacion->getId())),
            'class' => 'btn btn-default',
        );
        //        } else {
        //            return null;
        //        }
    }
    public function btnContratista($organizacion)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Contratistas',
            'imagen' => 'fa fa-user',
            'url' =>  $this->router->generate('gpm_contratista_list', array('id' => $organizacion->getId())),
            'class' => 'btn btn-default',
        );
        //        } else {
        //            return null;
        //        }
    }
    public function btnExportar($informe, $filtro)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Exportar',
            'imagen' => 'fa fa-file',
            'url' =>  $this->router->generate('gpm_proyecto_exportar', array('informe' => $informe, 'filtro' => $filtro)),
            'class' => 'btn btn-default',
        );
        //        } else {
        //            return null;
        //        }
    }

    public function btnNewReferencia($organizacion)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Referencia General',
            'imagen' => 'fa fa-plus',
            'url' => '#modalReferenciaNew',
            'class' => 'btn btn-default',
            'extra' => 'data-toggle=modal',
            'title' => 'Referencia para cualquier actividad del sistema de proyectos',
            'id' => 'btnReferenciaGeneral'
        );
        //        } else {
        //            return null;
        //        }
    }
}
