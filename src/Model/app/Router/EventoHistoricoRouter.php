<?php

namespace App\Model\app\Router;


/**
 * Description of EventoHistoricoRouter
 *
 * @author nicolas
 */

use App\Model\app\Router\BasicRouter;

class EventoHistoricoRouter  extends BasicRouter
{

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_EVENTO_HISTORICO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
