<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class TareaMantRouter extends BasicRouter
{

    public function btnNew($organizacion, $servicio)
    {
        if ($this->userlogin->isGranted('ROLE_TAREAS_MANT_AGREGAR')) {
            return $this->armarArray('Tareas', 'fa fa-plus', $this->router->generate(
                'tareamant_newonly',
                array('idOrg' => $organizacion->getId(), 'id' => $servicio != null ? $servicio->getId() : 0)
            ));
        } else {
            return null;
        }
    }

    public function btnEdit($tarea)
    {
        if ($this->userlogin->isGranted('ROLE_TAREAS_MANT_EDITAR')) {
            return $this->armarArray('Tarea', 'fa fa-edit', $this->router->generate(
                'tareamant_edit',
                array('id' => $tarea->getId())
            ));
        } else {
            return null;
        }
    }

    public function btnInforme($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_ADMIN')) {
            return $this->armarArray('Informe de Mantenimiento', 'fa fa-plus', $this->router->generate('informe_mantenimiento', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_TAREAS_MANT_DELETE')) {
            return array(
                'label' => 'Tarea',
                'imagen' => 'fa fa-trash',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
