<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;


class HoraTallerRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_AGREGAR')) {
            return $this->armarArray('Horas de Taller', 'fa fa-plus', $this->router->generate('horataller_new', array('idorg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($taller)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('horataller_edit', array('id' => $taller->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_MANT_VER')) {
            return $this->armarArray('Talleres', 'fa fa-pencil-square-o', $this->router->generate('horataller_list'));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_MANT_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
