<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class SatelitalRouter extends BasicRouter
{

    public function btnNew($satelital)
    {
        if ($this->userlogin->isGranted('ROLE_SATELITAL_AGREGAR')) {
            return $this->armarArray('Satelital', 'fa fa-plus', $this->router->generate('satelital_new', array('idOrg' => $satelital->getId())),'Agregar Satelital');
        } else {
            return null;
        }
    }

    public function btnEdit($satelital)
    {
        if ($this->userlogin->isGranted('ROLE_SATELITAL_EDITAR')) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('satelital_edit', array('id' => $satelital->getId())),'Modificar Satelital');
        }
        return null;
    }

    public function btnDelete($satelital)
    {
        if ($this->userlogin->isGranted('ROLE_SATELITAL_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'title' => 'Eliminar Satelital',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
