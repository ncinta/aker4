<?php

namespace App\Model\app\Router;


use  App\Model\app\Router\BasicRouter;

/**
 * Description of MotivoRouter
 *
 * @author nicolas
 */
class MotivoRouter  extends BasicRouter
{

    public function btnNew($organizacion)
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Motivo',
            'imagen' => 'fa fa-plus',
            'url' =>  $this->router->generate('gpm_motivogpm_new', array('idorg' => $organizacion->getId())),
            'class' => 'btn btn-primary',
        );
        //        } else {
        //            return null;
        //        }
    }
}
