<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class ServicioRouter extends BasicRouter
{

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnFlotaEnMapa($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_SAURON_FLOTA_ADMIN')) {
            return $this->armarArray('Ver en mapa', 'icon-road icon-blue', $this->router->generate('sauron_mapa_flota', array('idorg' => $organizacion->getId())));
        }
        return null;
    }

    public function btnNew($equipo = null)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR')) {
            if (is_null($equipo)) {
                return $this->armarArray('Servicio', 'fa fa-plus', $this->router->generate('servicio_new', array('idequipo' => 0)));
            }
        }
        return null;
    }

    public function btnSelect($itinerario = null)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR')) {
            return array(
                'label' => 'Servicio',
                'title' => 'Agregar Servicio',
                'imagen' => 'fa fa-plus',
                'url' => '#modalServicio',
                'extra' => 'data-toggle=modal',
            );
            
        }
         return null;
    }

    public function btnEdit($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('servicio_edit', array('id' => $servicio->getId())));
        }
        return null;
    }

    public function btnSensor($servicio)
    {
        if (count($servicio->getEquipo()->getModelo()->getSensores()) > 0) {
            return $this->armarArray('Agregar Sensor', 'fa fa-pencil-square-o', $this->router->generate('sauron_sensor_new', array('id' => $servicio->getId())));
        }
    }

    public function btnInputs($servicio)
    {
        if ($servicio->getEntradasDigitales()) {
            return $this->armarArray('Agregar Entrada', 'fa fa-pencil-square-o', $this->router->generate('sauron_inputs_new', array('id' => $servicio->getId())));
        }
    }

    public function btnHistorico($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_VER')) {
            return $this->armarArray('Historico', 'fa fa-file-archive-o', $this->router->generate('apmon_historial', array('id' => $servicio->getId())));
        }
        return null;
    }

    public function btnConfigOdometro($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO && $servicio->getTipoObjeto() == 1) {
            return $this->armarArray('Odometro/Horometro', 'fa fa-clock-o', $this->router->generate('servicio_cambio_odometro', array('id' => $servicio->getId())));
        }
    }

    public function btnRetirarEquipo($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO && $this->userlogin->isGranted('ROLE_EQUIPO_FLOTA_RETIRAR')) {
            return $this->armarArray('Retirar Equipo', 'icon-minus icon-blue', $this->router->generate('sauron_equipo_retirar', array('id' => $servicio->getEquipo()->getId())));
        }
    }

    public function btnAsignarEquipo($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_EQUIPO_FLOTA_ASIGNAR') && $servicio->getEstado() != $servicio::ESTADO_HABILITADO) {
            return $this->armarArray(
                'Asociar Equipo',
                'icon-copy icon-blue',
                $this->router->generate('servicio_asignar_equipo', array('id' => $servicio->getId()))
            );
        }
    }

    public function btnProgramar($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
            if ($this->userlogin->isGranted('ROLE_SAURON_PROGRAMACION') && (count($servicio->getParametros())) > 0) {  //si hay parametros para configurar.
                return $this->armarArray(
                    'Programar',
                    'icon-cow icon-blue',
                    $this->router->generate('main_programacion_edit', array(
                        'id' => $servicio->getEquipo()->getId(), 'default' => '0'
                    ))
                );
            }
        }
    }

    public function btnCorteMotor($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
            if ($servicio->getCorteMotor() && $this->userlogin->isGranted('ROLE_VAR_CORTEMOTOR')) {
                return $this->armarArray('Corte de Motor', 'icon-cut icon-blue', $this->router->generate('main_programacion_corte', array('id' => $servicio->getId())));
            }
        }
    }

    public function btnCerrojo($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
            if ($servicio->getCorteMotor() && $this->userlogin->isGranted('ROLE_VAR_CERROJO')) {
                return $this->armarArray('Cerrojo', 'icon-padlock', $this->router->generate('main_programacion_cerrojo', array('id' => $servicio->getId())));
            }
        }
    }

    public function btnDatosExtra($servicio)
    {
        if (!is_null($servicio->getVehiculo())) {
            return $this->armarArray('Datos Extras', 'icon-plus icon-blue', $this->router->generate('main_vehiculoextra_new', array('id' => $servicio->getId())));
        }
    }

    public function btnBaja($servicio)
    {
        if ($servicio->getEstado() == $servicio::ESTADO_DESHABILITADO && is_null($servicio->getEquipo())) {
            if ($this->userlogin->isGranted('ROLE_FLOTA_ELIMINAR')) {
                return $this->armarArray('Baja', 'icon-minus icon-blue', $this->router->generate('servicio_baja', array('id' => $servicio->getId())));
            }
        }
        if ($servicio->getEstado() == $servicio::ESTADO_BAJA || $servicio->getEstado() == $servicio::ESTADO_DESHABILITADO && is_null($servicio->getEquipo())) {
            if ($this->userlogin->isGranted('ROLE_FLOTA_ELIMINAR')) {
                return array(
                    'label' => 'Eliminar',
                    'imagen' => 'fa fa-minus',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnExportar($servicio, $desde, $hasta)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_EXPORTAR')) {
            return $this->armarArray('Exportar Cargas', 'fa fa-dowload', $this->router->generate('fuel_list_export', array(
                'idservicio' => $servicio->getId(),
                'desde' => $desde,
                'hasta' => $hasta
            )));
        }
        return null;
    }

    public function btnExporQr($servicio)
    {
        return $this->armarArray('Descargar QR', 'fa fa-download', $this->router->generate('servicio_exportqr', array(
            'id' => $servicio->getId()
        )));
    }
}
