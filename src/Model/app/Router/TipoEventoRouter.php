<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class TipoEventoRouter extends BasicRouter
{

    public function btnNew()
    {
        if ($this->userlogin->isGranted('ROLE_SAURON_EVENTO_ADMIN')) {
            return $this->armarArray('Tipo Evento', 'fa fa-plus', $this->router->generate('tipoevento_new'),'Agregar Transporte');
        } else {
            return null;
        }
    }

    public function btnEdit($tipoEvento)
    {
        if ($this->userlogin->isGranted('ROLE_SAURON_EVENTO_ADMIN')) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('tipoevento_edit', array('id' => $tipoEvento->getId())),'Modificar Tipo de Evento');
        }
        return null;
    }

    public function btnDelete($transporte)
    {
        if ($this->userlogin->isGranted('ROLE_SAURON_EVENTO_ADMIN')) {
            return array(
                'label' => 'Eliminar',
                'Title' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
    public function btnAddVariable()
    {
        if ($this->userlogin->isGranted('ROLE_SAURON_EVENTO_ADMIN')) {
            return array(
                'label' => 'Variable',
                'Title' => 'Agregar Variable',
                'imagen' => 'fa fa-plus',
                'url' => '#modalAddVariable',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
