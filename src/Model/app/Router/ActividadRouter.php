<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class ActividadRouter extends BasicRouter
{

    public function btnNew($mantenimiento)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Actividad', 'fa fa-plus', $this->router->generate('actividad_new', array('id' => $mantenimiento->getId())));
        } else {
            return null;
        }
    }
}
