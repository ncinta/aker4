<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class UsuarioRouter extends BasicRouter
{



    public function btnNew()
    {
        // if ($this->userlogin->isGranted('ROLE_TRANSPORTE_EDITAR')) {
        return array(
            'label' => 'Usuario',
            'title' => 'Agregar Usuario',
            'imagen' => 'fa fa-plus',
            'url' => '#modalNewUsuario',
            'extra' => 'data-toggle=modal',
        );

        // }
        // return null;
    }

    public function btnEdit($usuario, $userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_EDITAR')) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('usuario_edit', array('id' => $usuario->getId())), 'Modificar Usuario');
        }
        return null;
    }

    public function btnEditPermiso($usuario, $userloginManager)
    {
        if ($usuario->getId() != $userloginManager->getUser()->getId() && $userloginManager->isGranted('ROLE_USUARIO_EDITAR')) {
            return $this->armarArray('Modificar Permiso', 'fa fa-stop-circle', $this->router->generate('usuario_permiso', array('id' => $usuario->getId())), 'Modificar Permisos');
        }
        return null;
    }

    public function btnChangePassword($usuario, $userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_CAMBIARCLAVE')) {
            return $this->armarArray('Modificar Clave', 'fa fa-key', $this->router->generate('user_profile_password_change', array('id' => $usuario->getId())), 'Modificar Clave');
        }
        return null;
    }

    public function btnResetClave($usuario, $userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_CAMBIARCLAVE')) {
            return $this->armarArray('Reset Clave', 'fa fa-key', $this->router->generate('usuario_reset', array('id' => $usuario->getId())), 'Reset Clave');
        }
        return null;
    }

    public function btnApicode($usuario, $userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_CAMBIARCLAVE')) {
            return $this->armarArray('Generar Apicode', 'fa fa-mobile-phone', $this->router->generate('usuario_generarapicode', array('id' => $usuario->getId())), 'Generar Apicode');
        }
        return null;
    }

    public function btnServicios($usuario, $userloginManager)
    {
        if ($usuario != $usuario->getOrganizacion()->getUsuarioMaster() && $usuario->getOrganizacion()->getTipoOrganizacion() == 2 && $userloginManager->isGranted('ROLE_USUARIO_SERVICIO_VER')) {
            return $this->armarArray('Servicios Asignados', 'fa fa-truck', $this->router->generate('usuario_servicios_asignados', array('id' => $usuario->getId())), 'Ver Servicios Asignados');
        }
        return null;
    }

    public function btnReferencias($usuario, $userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_REFERENCIA_VER')) {
            return $this->armarArray('Referencias Asignadas', 'fa fa-map-marker', $this->router->generate('usuario_referencias_asignadas', array('id' => $usuario->getId())), 'Ver Referencias Asignadas');
        }
        return null;
    }


    public function btnDelete($userloginManager)
    {
        if ($userloginManager->isGranted('ROLE_USUARIO_ELIMINAR')) {
            return array(
                'label' => 'Usuario',
                'title' => 'Eliminar Usuario',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
