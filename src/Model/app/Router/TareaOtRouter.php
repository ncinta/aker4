<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class TareaOtRouter extends BasicRouter
{

    public function btnEdit($tarea)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('tareaot_edit', array('id' => $tarea->getId())));
        } else {
            return null;
        }
    }


    public function btnInsumo()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Insumo',
                'imagen' => 'fa fa-cogs',
                'url' => '#modalProducto',
                'id' => 'btnModalProducto',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnMecanico()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Mecanico',
                'imagen' => 'fa fa-wrench',
                'url' => '#modalMecanico',
                'id' => 'btnModalMecanico',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnComentario()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Comentario',
                'imagen' => 'fa fa-comment',
                'url' => '#modalComentario',
                'id' => 'btnModalComentario',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnCerrar()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return array(
                'label' => 'Cerrar',
                'imagen' => 'fa fa-download',
                'url' => '#modalCerrar',
                'extra' => 'data-toggle=modal',
                'id' => 'btnCerrar',
            );
        }
    }

    public function btnIniciar()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return array(
                'label' => 'Iniciar',
                'imagen' => 'fa fa-play',
                'url' => '#modalIniciar',
                'extra' => 'data-toggle=modal',
                'id' => 'btnIniciar',
            );
        }
    }
}
