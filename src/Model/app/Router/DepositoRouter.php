<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class DepositoRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_STOCK_AGREGAR')) {
            return $this->armarArray('Depósito', 'fa fa-plus', $this->router->generate('deposito_new', array('idorg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($deposito)
    {
        if ($this->userlogin->isGranted('ROLE_STOCK_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('deposito_edit', array('id' => $deposito->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_STOCK_VER')) {
            return $this->armarArray('Depósitos', 'fa fa-pencil-square-o', $this->router->generate('deposito_list'));
        } else {
            return null;
        }
    }

    public function btnAddStock()
    {
        if ($this->userlogin->isGranted('ROLE_STOCK_AGREGAR')) {
            return array(
                'label' => 'Agregar Producto',
                'imagen' => 'fa fa-plus',
                'url' => '#modalProducto',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_STOCK_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
