<?php

namespace App\Model\app\Router;


use  App\Model\app\Router\BasicRouter;

/**
 * Description of MotivoRouter
 *
 * @author nicolas
 */
class ContratistaRouter  extends BasicRouter
{


    public function btnNew()
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Contratista',
            'imagen' => 'fa fa-plus',
            'url' => '#modalContratistaAgregar',
            'class' => 'btn btn-default',
            'extra' => 'data-toggle=modal',
            'title' => 'Agregar Contratista',
        );
        //        } else {
        //            return null;
        //        }
    }

    public function btnNewPersona()
    {
        //        if ($this->userlogin->isGranted('ROLE_PROYECTO_AGREGAR')) {
        return array(
            'label' => 'Personal',
            'imagen' => 'fa fa-plus',
            'url' => '#modalPersonaAgregar',
            'class' => 'btn btn-default',
            'extra' => 'data-toggle=modal',
            'title' => 'Agregar Personal',
        );
        //        } else {
        //            return null;
        //        }
    }
}
