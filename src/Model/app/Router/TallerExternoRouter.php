<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class TallerExternoRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_AGREGAR')) {
            return $this->armarArray('Taller Externo', 'fa fa-plus', $this->router->generate('tallerexterno_new', array('idorg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($taller)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('tallerexterno_edit', array('id' => $taller->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_VER')) {
            return $this->armarArray('Talleres', 'fa fa-pencil-square-o', $this->router->generate('tallerexterno_list'));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
