<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class ClienteRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_AGREGAR')) {
            return $this->armarArray('Agregar', 'fa fa-plus', $this->router->generate('distribuidor_cliente_new', array('id' => $organizacion->getId())));
        }
    }

    public function btnEdit($cliente)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('distribuidor_cliente_edit', array('id' => $cliente->getId())));
        }
        return null;
    }



    public function btnDelete($cliente, $cantidad_servicios = 0)
    {
        if ($this->userlogin->isGranted('ROLE_ORGANIZACION_ELIMINAR') && $cantidad_servicios == 0) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
