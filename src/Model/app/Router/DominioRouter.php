<?php

namespace App\Model\app\Router;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use App\Model\app\Router\BasicRouter;


class DominioRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_DOMINIO_AGREGAR')) {
            return $this->armarArray('Dominio', 'fa fa-plus', $this->router->generate('dominio_new', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($dominio)
    {
        if ($this->userlogin->isGranted('ROLE_DOMINIO_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('dominio_edit', array('id' => $dominio->getId())));
        } else {
            return null;
        }
    }

    public function btnList($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_DOMINIO_VER')) {
            return $this->armarArray('Dominios', 'fa fa-pencil-square-o', $this->router->generate('dominio_list', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_DOMINIO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
