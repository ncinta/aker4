<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

/**
 * Description of ItinerarioRouter
 *
 * @author nicolas
 */
class ItinerarioRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_AGREGAR')) {
            return $this->armarArray('Itinerario', 'fa fa-plus', $this->router->generate('itinerario_new', array('idOrg' => $organizacion->getId())), 'Nuevo Itinerario');
        } else {
            return null;
        }
    }

    public function btnAddsList($organizacion)
    {
        $menu = $this->armarArray('Agregar', 'fa fa-plus', '#');
        $menu['submenu'][] = $this->btnNew($organizacion);
        $menu['submenu'][] = $this->btnImport($organizacion);
        return $menu;
    }

    public function btnImport($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_AGREGAR')) {
            return $this->armarArray('Importar Itinerarios', 'glyphicon glyphicon-import', $this->router->generate('itinerario_import_index', array('id' => $organizacion->getId())), 'Importar excel de itinerarios');
        } else {
            return null;
        }
    }

    public function btnEdit($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('itinerario_edit', array('id' => $itinerario->getId())), 'Modificar');
        }
        return null;
    }


    public function btnDelete($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_ELIMINAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalDelete',
                'label' => 'Eliminar',
                'title' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnAdds($itinerario)
    {
        $menu = $this->armarArray('Agregar', 'fa fa-plus', '#');
        $menu['submenu'][] = $this->btnAddServicio($itinerario);
        $menu['submenu'][] = $this->btnAddCustodio($itinerario);
        $menu['submenu'][] = $this->btnAddReferencia($itinerario);
        $menu['submenu'][] = $this->btnAddUsuario($itinerario);
        $menu['submenu'][] = $this->btnAddContacto($itinerario);
        return $menu;
    }

    public function btnAddServicio($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalServicios',
                'label' => 'Servicio',
                'title' => 'Agregar Servicio',
                'imagen' => 'fa fa-truck',
                'url' => '#modalServicio',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnAddCustodio($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalCustodios',
                'label' => 'Custodio',
                'title' => 'Agregar Custodia',
                'imagen' => 'fa fa-car',
                'url' => '#modalCustodio',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnAddReferencia($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalReferencias',
                'label' => 'Punto de interés',
                'title' => 'Agregar Punto de Interés',
                'imagen' => 'fa fa-map-marker',
                'url' => '#modalReferencia',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnAddUsuario($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalUsuarios',
                'label' => 'Usuario',
                'title' => 'Agregar Usuario',
                'imagen' => 'fa fa-user',
                'url' => '#modalUsuario',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnAddContacto($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalContactos',
                'label' => 'Contacto',
                'title' => 'Agregar Contacto',
                'imagen' => 'fa fa-address-card-o',
                'url' => '#modalContacto',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnCerrar($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalCerrar',
                'label' => 'Cerrar',
                'title' => 'Cerrar Itinerario',
                'imagen' => 'fa fa-close',
                'url' => '#modalCerrar',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnChangeEstado()
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_ESTADO')) {
            return array(
                'label' => 'Estado',
                'id' => 'btnChangeEstado',
                'imagen' => 'fa fa-exchange',
                'url' => '#',

            );
        }
    }

    public function btnComentario()
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR')) {
            return array(
                'id' => 'btnComentario',
                'label' => 'Comentario',
                'title' => 'Agregar Comentario',
                'imagen' => 'fa fa-comment',
                'url' => '#modalNewComentario',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnAddEvento($itinerario)
    {
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_EDITAR') && $itinerario->getEstado() < 9) {
            return array(
                'id' => 'btnModalEventoItinerario',
                'label' => 'Eventos',
                'title' => 'Administrar eventos',
                'imagen' => 'fa fa-exclamation-triangle ',
                'url' => '#modalEventoItinerario',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }
}
