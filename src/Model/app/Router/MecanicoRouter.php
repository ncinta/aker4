<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class MecanicoRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MECANICO_AGREGAR')) {
            return $this->armarArray('Mecánico', 'fa fa-plus', $this->router->generate('mecanico_new', array('idOrg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($taller)
    {
        if ($this->userlogin->isGranted('ROLE_MECANICO_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('mecanico_edit', array('id' => $taller->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_MECANICO_VER')) {
            return $this->armarArray('Mecánicos', 'fa fa-pencil-square-o', $this->router->generate('mecanico_list'));
        } else {
            return null;
        }
    }

    public function btnInforme($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MECANICO_VER')) {
            return $this->armarArray('Informe', 'fa fa-print', $this->router->generate('mecanico_informe', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_MECANICO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
