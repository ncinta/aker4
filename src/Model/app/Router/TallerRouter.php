<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class TallerRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_AGREGAR')) {
            return $this->armarArray('Taller', 'fa fa-plus', $this->router->generate('taller_new', array('idorg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($taller)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('taller_edit', array('id' => $taller->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_VER')) {
            return $this->armarArray('Talleres', 'fa fa-pencil-square-o', $this->router->generate('taller_list'));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
