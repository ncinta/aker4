<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class MantenimientoRouter extends BasicRouter
{

    public function btnNewTarea($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_TAREAS_MANT_AGREGAR')) {
            return $this->armarArray(
                'Asignar Mantenimiento',
                'fa fa-plus',
                $this->router->generate('tareamant_newonly', array('id' => $servicio->getId(), 'idOrg' => $servicio->getOrganizacion()->getId()))
            );
        } else {
            return null;
        }
    }
}
