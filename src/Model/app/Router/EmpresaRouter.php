<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class EmpresaRouter extends BasicRouter
{

    public function btnNew($empresa)
    {
        if ($this->userlogin->isGranted('ROLE_EMPRESA_AGREGAR')) {
            return $this->armarArray('Cliente', 'fa fa-plus', $this->router->generate('empresa_new', array('idOrg' => $empresa->getId())),'Agregar Cliente');
        } else {
            return null;
        }
    }

    public function btnEdit($empresa)
    {
        if ($this->userlogin->isGranted('ROLE_EMPRESA_EDITAR')) {
            return $this->armarArray('Modificar', 'fa fa-pencil-square-o', $this->router->generate('empresa_edit', array('id' => $empresa->getId())),'Modificar Cliente');
        }
        return null;
    }

    public function btnDelete($empresa)
    {
        if ($this->userlogin->isGranted('ROLE_EMPRESA_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'title' => 'Eliminar Cliente',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnNewContacto()
    {
        if ($this->userlogin->isGranted('ROLE_CONTACTO_AGREGAR')) {
            return array(
                'label' => 'Contacto',
                'title' => '',
                'imagen' => 'fa fa-plus',
                'url' => '#modalContacto',
                'extra' => 'data-toggle=modal',
            );
        } else {
            return null;
        }
    }
}
