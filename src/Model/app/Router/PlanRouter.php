<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class PlanRouter extends BasicRouter
{

    public function btnPlanMantenimiento($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_ADMIN')) {
            return $this->armarArray('Plan de Mantenimiento', 'fa fa-pencil-square-o', $this->router->generate('plan_list', array('idOrg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_AGREGAR')) {
            return $this->armarArray('Plan', 'fa fa-plus', $this->router->generate('plan_new', array('idOrg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($mantenimiento)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Mantenimiento', 'fa fa-pencil-square-o', $this->router->generate('plan_edit', array('id' => $mantenimiento->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_MANT_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
