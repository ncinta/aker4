<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class CentroCostoRouter extends BasicRouter
{

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_AGREGAR')) {
            return $this->armarArray('Centro de Costo', 'fa fa-plus', $this->router->generate('centrocosto_new', array('idOrg' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($taller)
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('centrocosto_edit', array('id' => $taller->getId())));
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_VER')) {
            return $this->armarArray('Centros de Costo', 'fa fa-pencil-square-o', $this->router->generate('centrocosto_list', array(
                'idOrg' => $this->userlogin->getOrganizacion()->getId()
            )));
        } else {
            return null;
        }
    }

    public function btnServicioAsignar($centrocosto)
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_SERVICIO')) {
            return $this->armarArray(
                'Asignar Servicio',
                'fa fa-pencil-square-o',
                $this->router->generate('centrocosto_servicio_asignar', array('id' => $centrocosto->getId()))
            );
        } else {
            return null;
        }
    }

    public function btnServicioRetirarMany($centrocosto)
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_SERVICIO')) {
            return $this->armarArray(
                'Desafectar Servicios',
                'fa fa-pencil-square-o',
                $this->router->generate('centrocosto_servicio_retirarmany', array('id' => $centrocosto->getId()))
            );
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_CENTROCOSTO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
