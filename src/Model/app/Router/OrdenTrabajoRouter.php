<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class OrdenTrabajoRouter extends BasicRouter
{

    public function btnNewServicio($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return $this->armarArray('Orden de Trabajo', 'fa fa-plus', $this->router->generate('ordentrabajo_newservicio', array('id' => $servicio->getOrganizacion()->getId(), 'idservicio' => $servicio->getId())));
        } else {
            return null;
        }
    }
    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR') || $this->userlogin->isGranted('ROLE_TALLER_VER')) {
            return $this->armarArray('Orden de Trabajo', 'fa fa-plus', $this->router->generate('ordentrabajo_new', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnEdit($orden)
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('ordentrabajo_edit', array('id' => $orden->getId())), null, 'btnEdit');
        } else {
            return null;
        }
    }

    public function btnExport($orden)
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_VER')) {
            return $this->armarArray('Exportar', 'fa fa-download', $this->router->generate('ordentrabajo_export', array('id' => $orden->getId())), null);
        } else {
            return null;
        }
    }

    public function btnList()
    {
        if ($this->userlogin->isGranted('ROLE_TALLER_VER')) {
            return $this->armarArray('Talleres', 'fa fa-pencil-square-o', $this->router->generate('taller_list'));
        } else {
            return null;
        }
    }

    public function btnInsumo()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Insumo',
                'imagen' => 'fa fa-cogs',
                'url' => '#modalProducto',
                'id' => 'btnModalProducto',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnMecanico()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Mecanico',
                'imagen' => 'fa fa-wrench',
                'url' => '#modalMecanico',
                'id' => 'btnModalMecanico',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnProveedor()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Proveedores',
                'imagen' => 'fa fa-male',
                'url' => '#modalExterno',
                'id' => 'btnModalExterno',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnTaller()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Taller',
                'imagen' => 'fa fa-home',
                'url' => '#modalHorasTaller',
                'id' => 'btnModalHorasTaller',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnPreventivo($orden)
    {
        if ($orden->getTipo() == 1 && $this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Taller',
                'imagen' => 'fa fa-briefcase',
                'url' => '#modalMantenimiento',
                'id' => 'btnModalPreventivo',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnPeticion()
    {
        if ($this->userlogin->isGranted('ROLE_PETICION_VER')) {
            return array(
                'label' => 'Pedido de Reparación',
                'imagen' => 'fa fa-briefcase',
                'url' => '#modalPeticion',
                'id' => 'btnModalPeticion',
                'extra' => 'data-toggle=modal',
            );
        } else {
            return null;
        }
    }

    public function btnTareaOt()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_AGREGAR')) {
            return array(
                'label' => 'Tareas',
                'imagen' => 'fa fa-wrench',
                'url' => '#modalTareasOt',
                'id' => 'btnTareasOt',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'id' => 'btnEliminar',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnCerrar()
    {
        if ($this->userlogin->isGranted('ROLE_ORDENTRABAJO_ELIMINAR')) {
            return array(
                'label' => 'Cerrar Orden',
                'imagen' => 'fa fa-download',
                'url' => '#modalCerrarOt',
                'extra' => 'data-toggle=modal',
                'id' => 'btnCerrarOts',
            );
        }
    }
}
