<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;
use Symfony\Component\Routing\RouterInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;

/**
 * Description of NotificacionChoferRouter
 *
 * @author nicolas
 */
class NotificacionChoferRouter extends BasicRouter
{

    protected $userloginManager;

    public function __construct(RouterInterface $router, UserLoginManager $user, OrganizacionManager $organizacionManager)
    {
        parent::__construct($router, $user);
        $this->orgManager = $organizacionManager;
        $this->userloginManager = $user;
    }

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnNew()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_AGREGAR')) {
            return array(
                'label' => 'Grupo de Notificación',
                'imagen' => 'fa fa-plus',
                'url' => '#modalGrupoNew',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnEdit($id)
    {
        if ($this->userloginManager->isGranted('ROLE_CHOFER_AGREGAR')) {
            return $this->armarArray("Modificar", "fa fa-edit", $this->router->generate("apmon_notificacionchofer_edit", array("id" => $id)), "Modificar grupo de notificación");
        }
        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_AGREGAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnAddChofer()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_AGREGAR')) {
            return array(
                'label' => 'Chofer',
                'imagen' => 'fa fa-plus',
                'url' => '#modalAddChofer',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnAddContacto()
    {
        if ($this->userlogin->isGranted('ROLE_CHOFER_AGREGAR')) {
            return array(
                'label' => 'Contacto',
                'imagen' => 'fa fa-plus',
                'url' => '#modalAddContacto',
                'extra' => 'data-toggle=modal',
            );
        }
    }
}
