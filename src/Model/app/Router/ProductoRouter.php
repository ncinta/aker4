<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;

class ProductoRouter extends BasicRouter
{

    public function btnNew($organizacion, $modal = false)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            if ($modal) {
                return array(
                    'label' => 'Producto',
                    'imagen' => 'fa fa-plus',
                    'url' => '#modalProducto',
                    'extra' => 'data-toggle=modal',
                );
            } else {
                return $this->armarArray('Producto', 'fa fa-plus', $this->router->generate('producto_new', array('idorg' => $organizacion->getId())));
            }
        } else {
            return null;
        }
    }

    public function btnAddStock($producto, $modal = false)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            if ($modal) {
                return array(
                    'label' => 'Stock',
                    'imagen' => 'fa fa-plus',
                    'url' => '#modalStock',
                    'extra' => 'data-toggle=modal',
                );
            } else {
                return $this->armarArray('Stock', 'fa fa-plus', $this->router->generate('producto_add'));
            }
        } else {
            return null;
        }
    }

    public function btnEdit($producto)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('producto_edit', array('id' => $producto->getId())));
        } else {
            return null;
        }
    }

    public function btnRubro($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Rubro', 'fa fa-clipboard', $this->router->generate('app_rubro_list', array('id' => $organizacion->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnExport($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_MANT_EDITAR')) {
            return $this->armarArray('Exportar', 'fa fa-download', $this->router->generate('producto_export', array('id' => $organizacion->getId())), null);
        } else {
            return null;
        }
    }
}
