<?php

namespace App\Model\app\Router;

use App\Model\app\Router\BasicRouter;
use Symfony\Component\Routing\RouterInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;

class VerificaCombustibleRouter extends BasicRouter
{

    public function __construct(RouterInterface $router, UserLoginManager $user, OrganizacionManager $organizacionManager)
    {
        parent::__construct($router, $user);
        $this->orgManager = $organizacionManager;
    }

    public function toCalypso($menu)
    {
        return parent::toCalypso($menu);
    }

    public function btnUploadCargas($organizacion)
    {
        if ($this->orgManager->isModuloEnabled($organizacion, 'MANAGER_COMBUSTIBLE')) {  //solo en los clientes
            //return $this->armarArray('Cargas', 'fa fa-upload', $this->router->generate('fuel_check_index', array('id' => $organizacion->getId())));
            return array(
                'label' => 'Cargas',
                'imagen' => 'fa fa-upload',
                'url' => '#modalUploadCargas',
                'extra' => 'data-toggle=modal',
            );
        }
        return null;
    }

    public function btnShow($carga)
    {
        if ($this->userlogin->isGranted('ROLE_MASTER_CLIENTE') || $this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
            return $this->armarArray('Ver', 'fa fa-eye', $this->router->generate('fuel_show', array('id' => $carga->getId())));
        }
        return null;
    }

    public function btnListCarga($servicio)
    {
        return $this->armarArray('Cargas de Combustible', 'fa fa-flask', $this->router->generate('fuel_list', array('id' => $servicio->getId())));
    }

    public function btnEdit($carga)
    {
        //        if ($this->userlogin->isGranted('ROLE_ADMIN_COMBUSTIBLE')) {
        return $this->armarArray('Editar', 'fa fa-pencil-square-o', $this->router->generate('fuel_edit', array('id' => $carga->getId())));
        //        }
        //        return null;
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_ELIMINAR')) {
            return array(
                'label' => 'Cargas',
                'id'=> 'btnDeleteCarga',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnInformeRendimiento($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO')) {
            return $this->armarArray('Informe Rendimiento', 'icon-chevron-left icon-blue', $this->router->generate('fuel_info_rendimiento', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnGraficoConsumo($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_GRAFICO_CONSUMO')) {
            return $this->armarArray('Gráfico Consumo', 'icon-chevron-left icon-blue', $this->router->generate('fuel_grafico', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnImportar($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_IMPORTACION')) {
            return $this->armarArray('Importar Cargas', 'icon-chevron-left icon-blue', $this->router->generate('fuel_import_index', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnChecker($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_IMPORTACION')) {
            return $this->armarArray('Verificador cargas', 'icon-check icon-blue', $this->router->generate('fuel_check_index', array('id' => $organizacion->getId())));
        }
        return null;
    }
    public function btnExportar($servicio, $desde, $hasta)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_EXPORTAR')) {
            return $this->armarArray('Exportar Cargas', 'fa fa-dowload', $this->router->generate('fuel_list_export', array(
                'idservicio' => $servicio->getId(),
                'desde' => $desde,
                'hasta' => $hasta
            )));
        }
        return null;
    }

    public function btnMasiva($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_AGREGAR')) {
            return $this->armarArray('Cargas Masivas', 'fa fa-plus', $this->router->generate('fuel_newmasiva', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnInformeCarga($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
            return $this->armarArray('Informe Cargas', 'icon-chevron-left icon-blue', $this->router->generate('fuel_informe_carga', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnInformeDespacho($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
            return $this->armarArray('Informe Despacho', 'icon-chevron-left icon-blue', $this->router->generate('fuel_informe_despacho', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnPuntoCarga($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
            return $this->armarArray('Puntos de Carga', 'icon-chevron-left icon-blue', $this->router->generate('fuel_puntocarga_list', array('id' => $organizacion->getId())));
        }
        return null;
    }
}
