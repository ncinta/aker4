<?php

namespace App\Model\app\Router;

use  App\Model\app\Router\BasicRouter;

class PuntoCargaRouter extends BasicRouter
{

    public function btnList($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_PUNTOCARGA_VER')) {
            return $this->armarArray('Puntos de Carga', 'fa fa-building-o', $this->router->generate('fuel_puntocarga_list', array('id' => $organizacion->getId())));
        }
        return null;
    }

    public function btnNew($organizacion)
    {
        if ($this->userlogin->isGranted('ROLE_PUNTOCARGA_AGREGAR')) {
            return array(
                'label' => 'Agregar',
                'imagen' => 'fa fa-plus',
                'url' => '#modalAdd',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnEdit($punto)
    {
        if ($this->userlogin->isGranted('ROLE_PUNTOCARGA_AGREGAR')) {
            return $this->armarArray('Punto de  Carga', 'fa fa-pencil-square-o', $this->router->generate('fuel_edit_puntocarga', array('id' => $punto->getId())));
        } else {
            return null;
        }
    }

    public function btnDelete()
    {
        if ($this->userlogin->isGranted('ROLE_PUNTOCARGA_ELIMINAR')) {
            return array(
                'label' => 'Eliminar',
                'imagen' => 'fa fa-minus',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
    }

    public function btnAddCombustible($puntocarga)
    {
        if ($this->userlogin->isGranted('ROLE_PUNTOCARGA_VER')) {
            return array(
                'label' => 'Combustible',
                'imagen' => 'fa fa-plus',
                'url' => '#modalAddCombustible',
                'extra' => 'data-toggle=modal',
                'onclick'=> "newComb('" . $puntocarga->getId() . "')",
            );
        }
    }
}
