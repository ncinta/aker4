<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ServicioMantenimientoHistorico;
use App\Entity\Producto;
use App\Entity\ActividadHistorico;

/**
 * Description of ActividadManager
 *
 * @author nicolas
 */

/**
 * Description of ActividadHistorico
 *
 * @author nicolas
 */
class ActividadHistoricoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($historico)
    {
        if ($historico instanceof ActividadHistorico) {
            return $this->em->getRepository('App:ActividadHistorico')->findBy(array('id' => $historico->getId()));
        } else {
            return $this->em->getRepository('App:ActividadHistorico')->find(array('id' => $historico));
        }
    }

    public function create()
    {
        $historico = new ActividadHistorico();
        return $historico;
    }

    public function save(ActividadHistorico $historico)
    {
        $this->em->persist($historico);
        $this->em->flush();
        return $historico;
    }

    public function delete(ActividadHistorico $historico)
    {
        $this->em->remove($historico);
        $this->em->flush();
        return true;
    }

    public function findByProducto(Producto $producto)
    {
        return $this->em->getRepository('App:ActividadHistorico')->findByProducto($producto);
    }
}
