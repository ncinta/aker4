<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of ServicioCanbusManager
 *
 * @author nicolas
 */
class ServicioCanbusManager
{
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAll($idOrg)
    {
        return $this->em->getRepository('App:ServicioCanbus')->findByOrg($idOrg);
    }

    public function findByService($idServicio)
    {
        return $this->em->getRepository('App:ServicioCanbus')->findByService($idServicio);
    }

    public function find($id)
    {
        return $this->em->getRepository('App:ServicioCanbus')->findOneBy(array('id' => $id));
    }
}
