<?php

/**
 * ChoferManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chofer;
use App\Entity\ChoferServicioHistorico;
use App\Entity\Licencia;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UtilsManager;
use DateTimeZone;
use Exception;
use PhpParser\Node\Expr\FuncCall;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ChoferManager
{

    protected $em;
    protected $security;
    protected $utils;
    protected $chofer;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, UtilsManager $utils, UserLoginManager $userlogin)
    {
        $this->security = $security;
        $this->utils = $utils;
        $this->em = $em;
        $this->userlogin = $userlogin;
    }

    /**
     * Buscan un chofer por su id u objeto.
     * @param Chofer|Int $chofer   el Chofer o el id del chofer
     * @return type 
     */
    public function find($chofer)
    {
        if ($chofer instanceof Chofer) {
            return $this->em->getRepository('App:Chofer')->find($chofer->getId());
        } else {
            return $this->em->getRepository('App:Chofer')->find($chofer);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Chofer')->find($id);
    }

    public function findByNombre($nombre)
    {
        return $this->em->getRepository('App:Chofer')->findOneBy(['nombre' => $nombre]);
    }

    public function findByDocumento($dni)
    {
        return $this->em->getRepository('App:Chofer')->findOneBy(['documento' => trim($dni)]);
    }

    public function findByDocumentoOrg($dni, $organizacion)
    {
        return $this->em->getRepository('App:Chofer')->findOneBy(['documento' => trim($dni), 'organizacion' => $organizacion->getId()]);
    }

    public function findByDallas($dallas)
    {
        return $this->em->getRepository('App:Chofer')->byDallas(strtoupper($dallas));
    }

    public function findByIbuttonFecha($ibutton, $fecha)
    {
        return $this->em->getRepository('App:Chofer')->byIbuttonFecha($ibutton, $fecha);
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Chofer')
            ->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function create(Organizacion $organizacion)
    {
        $this->chofer = new Chofer();
        $this->chofer->setOrganizacion($organizacion);
        return $this->chofer;
    }

    public function save($chofer)
    {
        if (!is_null($chofer))
            $this->chofer = $chofer;

        if (is_null($this->chofer->getId())) {   //estoy en una insercion-
            if ($this->findByDallas($this->chofer->getDallas())) {   //no debe haber otro dallas igual.
                return false;   //entidad ya existente.
            }
        } else {   // es una edicion
            $tmp = $this->findByDallas($this->chofer->getDallas());
            if (!is_null($tmp) && $tmp !== $this->chofer) {   //no debe haber otro dallas igual.
                return false;   //entidad ya existente.
            }
        }

        $this->em->persist($this->chofer);
        $this->em->flush();
        return $this->chofer;
    }

    public function asociarServicio($chofer, $servicio)
    {
        $chofer->setServicio($servicio);
        $this->em->persist($chofer);
        $this->em->flush();
    }

    public function retirarServicio($chofer, $servicio)
    {
        $chofer->setServicio(null);
        $this->em->persist($chofer);

        $servicio->setChofer(null);
        $this->em->persist($servicio);

        $this->em->flush();
    }

    public function delete($chofer)
    {
        $this->em->remove($this->find($chofer));
        $this->em->flush();
        return true;
    }

    public function addLicencia($chofer, $data)
    {
        $licencia = new Licencia();
        $licencia->setChofer($chofer);
        $licencia->setNumero($data['numero']);
        $licencia->setCategoria($data['categoria']);
        $licencia->setAviso($data['aviso'] != '' ? intval($data['aviso']) : 45);

        $expedicion = $this->utils->datetime2sqltimestamp($data['expedicion'], true);
        $vencimiento = $this->utils->datetime2sqltimestamp($data['vencimiento'], true);

        $licencia->setExpedicion(new \DateTime($expedicion));
        $licencia->setVencimiento(new \DateTime($vencimiento));
        $this->em->persist($licencia);
        $this->em->flush();
        return $licencia;
    }

    public function addHistorico($chofer, $servicio, $motivo, $data)
    {
        try {
            $historico = new ChoferServicioHistorico();
            $historico->setChofer($chofer);
            $historico->setServicio($servicio);
            $historico->setMotivo($motivo);
            $historico->setEstado($data['estado']);
            //fecha            
            if ($data['fecha']) {
                $fecha = $this->formaFecha($data['fecha']);                
                if ($fecha) {
                    $historico->setFecha($fecha);
                }
            } else { //no viene la fecha, debe generarla yo
                $fecha = new \DateTime(null, new \DateTimeZone(('UTC')));
                $historico->setFecha($fecha);
            }
            //posición
            if (isset($data['posicion'])) {
                if (isset($data['posicion']['latitud']) && isset($data['posicion']['longitud'])) {
                    $historico->setLatitud(floatval($data['posicion']['latitud']));
                    $historico->setLongitud(floatval($data['posicion']['longitud']));
                }
            }

            //formulario
            if (isset($data['formulario'])) {
                $historico->setIdFormulario(isset($data['formulario']['id']) ? $data['formulario']['id'] : null);
                $historico->setNombre(isset($data['formulario']['nombre']) ? $data['formulario']['nombre'] : null);
                if (isset($data['formulario']['fecha_iniciado'])) {
                    $fechaInicio = $this->formaFecha($data['formulario']['fecha_iniciado']);
                    if ($fechaInicio) {
                        $historico->setFechaInicio($fechaInicio);
                    }
                }
                if (isset($data['formulario']['fecha_completado'])) {
                    $fechaFin = $this->formaFecha($data['formulario']['fecha_completado']);
                    if ($fechaFin) {
                        $historico->setFechaFin($fechaFin);
                    }
                }
            }
            $this->em->persist($historico);
            $this->em->flush();
            return $historico;
        } catch (Exception $e) {
            //  die('////<pre>' . nl2br(var_export($e, true)) . '</pre>////');
            return false;
        }
    }

    private function formaFecha($str)
    {
        $fecha = \DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $str);            
        if ($fecha instanceof \DateTime) {
            return $fecha;
        } else {
            return new \DateTime(null, new DateTimeZone('UTC'));            
        }        
    }

    public function findByApicode($phone, $apicode)
    {
        $this->chofer = $this->em->getRepository('App:Chofer')
            ->findOneBy(array('telefono' => $phone, 'apiCode' => strtoupper($apicode)));
        return $this->chofer;
    }

    public function historicoFilter($organizacion, $desde, $hasta, $chofer = null, $servicio = null, $estado = null)
    {
        return $this->em->getRepository('App:ChoferServicioHistorico')->historico($organizacion, $desde, $hasta, $chofer, $servicio, $estado);
    }

    public function findUltimoHistorico($servicio, $motivo)
    {
        return $this->em->getRepository('App:ChoferServicioHistorico')->findUltimo($servicio, $motivo);
    }

    public function getIds($id)
    {
        $ids = [];
        $choferes = [];
        $organizacion = $this->userlogin->getOrganizacion();
        $nombre = '';
        if (is_array($id)) {    //se paso un array de choferes
            $str = [];
            $choferes = [];
            foreach ($id as $item) {   //como es array debe recorrer uno a uno los items
                if ($item == '0') {
                    $choferes = $this->findAll($organizacion);
                    $str[] = 'Todos los Choferes';                  
                } else {
                    $chofer = $this->find(intval($item));
                    if ($chofer != null) {
                        $str[] = $chofer->getNombre();
                        $choferes = array_merge($choferes, array($chofer));
                        $choferes = array_unique($choferes);   //elimina los duplicados
                    }
                }
            }
            $nombre = implode(', ', $str);
        } else {
            if ($id == '0') {
                $choferes = $this->findAll($organizacion);
                $nombre = 'Todas los Choferes';
            } else {
                $chofer = $this->find(intval($id));
                $choferes = array($chofer);
                $nombre = $chofer ? $chofer->getNombre() : '---';
            }
        }
        foreach ($choferes as $chofer) {
            $ids[] = $chofer->getId();
        }
        return array('nombre' => $nombre, 'ids' => $ids);
    }
    

}
