<?php

namespace App\Model\app;

class KmlParser
{

    public function __construct()
    {
    }

    private function read_kmz($filename)
    {
        $zip = zip_open($filename);
        if (is_int($zip)) {
            return false;
        }
        $entry = zip_read($zip);
        zip_entry_open($zip, $entry, "r");
        $content = zip_entry_read($entry, zip_entry_filesize($entry));
        zip_entry_close($entry);
        zip_close($zip);
        return $content;
    }

    public function read_kml($filename)
    {
        $content = $this->read_kmz($filename);
        if ($content !== false) {
            return $content;
        } else {
            return file_get_contents($filename);
        }
    }

    public function parse_kml($xml)
    {
        $xml = $this->parse_xml($xml, true, true);
        $style = array();
        $style_map = array();
        $placemark = array();
        foreach ($xml as $tag) {
            /* kml */
            if ($tag["type"] == "element" && $tag["name"] == "kml") {
                $xml_kml = $tag["childs"];
                foreach ($xml_kml as $tag) {
                    /* Document */
                    if ($tag["type"] == "element" && $tag["name"] == "Document") {
                        $xml_Document = $tag["childs"];
                        foreach ($xml_Document as $tag) {
                            /* Style */
                            if ($tag["type"] == "element" && $tag["name"] == "Style" && 
                            isset($tag['attr']) && is_array($tag['attr']) && isset($tag['attr']['id'])) { // metemos esta verificación.) {
                                $a_style = array();
                                $a_style_id = $tag["attr"]["id"];
                                $xml_Style = $tag["childs"];
                                foreach ($xml_Style as $tag) {                                    
                                    /* LineStyle */
                                    if ($tag["type"] == "element" && $tag["name"] == "LineStyle") {
                                        $xml_LineStyle = $tag["childs"];
                                        foreach ($xml_LineStyle as $tag) {
                                            /* color */
                                            if ($tag["type"] == "element" && $tag["name"] == "color") {
                                                $a_style["line_color"] = $tag["text"];
                                            } else
                                                /* width */
                                                if ($tag["type"] == "element" && $tag["name"] == "width") {
                                                    $a_style["line_width"] = $tag["text"];
                                                }
                                        }
                                    } else
                                        /* PolyStyle */
                                        if ($tag["type"] == "element" && $tag["name"] == "PolyStyle") {
                                            $xml_PolyStyle = $tag["childs"];
                                            foreach ($xml_PolyStyle as $tag) {
                                                /* color */
                                                if ($tag["type"] == "element" && $tag["name"] == "color") {
                                                    $a_style["poly_color"] = $tag["text"];
                                                }
                                            }
                                        }
                                }
                                $style[$a_style_id] = $a_style;
                            } else {
                                /* StyleMap */
                                if (
                                    $tag["type"] == "element" && $tag["name"] == "StyleMap" &&
                                    isset($tag['attr']) && is_array($tag['attr']) && isset($tag['attr']['id'])
                                ) { // metemos esta verificación.
                                    //  die('////<pre>'.nl2br(var_export($tag, true)).'</pre>////');
                                    $a_style_map = false;
                                    $a_style_map_id = $tag["attr"]["id"];
                                    $xml_StyleMap = $tag["childs"];
                                    foreach ($xml_StyleMap as $tag) {
                                        /* Pair */
                                        if ($tag["type"] == "element" && $tag["name"] == "Pair") {
                                            $key = "";
                                            $styleUrl = false;
                                            $xml_Pair = $tag["childs"];
                                            foreach ($xml_Pair as $tag) {
                                                /* key */
                                                if ($tag["type"] == "element" && $tag["name"] == "key") {
                                                    $key = $tag["text"];
                                                } else
                                                    /* styleMap */
                                                    if ($tag["type"] == "element" && $tag["name"] == "styleUrl") {
                                                        $styleUrl = $tag["text"];
                                                    }
                                            }
                                            if ($key == "normal") {
                                                $a_style_map = $styleUrl;
                                            }
                                        }
                                    }
                                    $style_map[$a_style_map_id] = $a_style_map;
                                } else {
                                    /* Placemark */
                                    if ($tag["type"] == "element" && $tag["name"] == "Placemark") {
                                        $a_placemark = array();
                                        $xml_Placemark = $tag["childs"];
                                        //                                            die('////<pre>'.nl2br(var_export($xml_Placemark, true)).'</pre>////');
                                        foreach ($xml_Placemark as $tag) {
                                            /* name */
                                            if ($tag["type"] == "element" && $tag["name"] == "name") {
                                                $a_placemark["name"] = $tag["text"];
                                            } else {
                                                /* styleUrl */
                                                if ($tag["type"] == "element" && $tag["name"] == "styleUrl") {
                                                    $a_placemark["styleUrl"] = $tag["text"];
                                                } else {
                                                    /* Polygon */
                                                    if ($tag["type"] == "element" && $tag["name"] == "Polygon") {
                                                        $xml_Polygon = $tag["childs"];
                                                        foreach ($xml_Polygon as $tag) {
                                                            /* outerBoundaryIs */
                                                            if ($tag["type"] == "element" && $tag["name"] == "outerBoundaryIs") {
                                                                $xml_outerBoundaryIs = $tag["childs"];
                                                                foreach ($xml_outerBoundaryIs as $tag) {
                                                                    /* LinearRing */
                                                                    if ($tag["type"] == "element" && $tag["name"] == "LinearRing") {
                                                                        $xml_LinearRing = $tag["childs"];
                                                                        foreach ($xml_LinearRing as $tag) {
                                                                            /* coordinates */
                                                                            if ($tag["type"] == "element" && $tag["name"] == "coordinates") {
                                                                                $a_placemark["polygon"] = trim($tag["text"]);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $placemark[] = $a_placemark;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        /*
          echo "<!--\n";
          print_r ($style);
          print_r ($style_map);
          print_r ($placemark);
          echo "-->\n";
         */
       // dd($style_map);
        return array(
            "style" => $style,
            "stylemap" => $style_map,
            "placemark" => $placemark
        );
    }

    /* tinyXML desarrollado por Matias Moreno */
    /* http://www.matiasmoreno.com.ar/ */

    /* pass 1: convertir el texto en elementos (<?declaraciones?>, <!--comentarios-->, <tags/>, textos) */

    function _tiny_xml_pass_1($content, $skip_blanks = false)
    {
        $tags = array();
        $i = 0;
        while ($i < strlen($content)) {
            if (substr($content, $i, 2) == "<?") {
                $pos = strpos($content, "?>", $i) + 2;
                $tags[] = substr($content, $i, $pos - $i);
                $i = $pos;
            } else if (substr($content, $i, 4) == "<!--") {
                $pos = strpos($content, "-->", $i) + 3;
                $tags[] = substr($content, $i, $pos - $i);
                $i = $pos;
            } else if (substr($content, $i, 1) == "<") {
                $pos = strpos($content, ">", $i) + 1;
                $tags[] = substr($content, $i, $pos - $i);
                $i = $pos;
            } else {
                $pos = strpos($content, "<", $i);
                if ($pos === false) {
                    $pos = strlen($content);
                }
                $text = substr($content, $i, $pos - $i);
                if (!$skip_blanks || trim($text)) {
                    $tags[] = $text;
                }
                $i = $pos;
            }
        }
        return $tags;
    }

    /* pass 2: determinar la "profundidad" de cada elemento */

    function _tiny_xml_pass_2($tags)
    {
        $depth = array();
        $current_depth = 0;
        for ($i = 0; $i < count($tags); $i++) {
            if (substr($tags[$i], 0, 2) == "</") {
                $current_depth--;
            }
            $depth[$i] = $current_depth;
            if (
                substr($tags[$i], 0, 1) == "<" &&
                substr($tags[$i], 0, 2) != "</" &&
                substr($tags[$i], 0, 2) != "<?" &&
                substr($tags[$i], 0, 4) != "<!--" &&
                substr($tags[$i], -2, 2) != "/>"
            ) {
                $current_depth++;
            }
        }
        /*
          for ($i=0; $i<count($tags); $i++) {
          for ($j=0; $j<$depth[$i]; $j++) {
          printf ("\t");
          }
          printf ("%s\n", $tags[$i]);
          }
         */
        return $depth;
    }

    /* determinar si un caracter es un blanco... parece que PHP no trae isblank() o similar */
    /* se podria haber implementado como return trim($char) == ""; jajaja */

    function _tiny_xml_is_blank($char)
    {
        return $char == " " || $char == "\t" || $char == "\r" || $char == "\n";
    }

    /* parsear un elemento con nombre y atributos (<nombre attr1="valor1" attr2="valor2" .../>) */
    /* lo implemente como una maquina de estados */

    function _tiny_xml_parse_tag_stuff($text)
    {
        $text = substr($text, 1, -1);
        if (substr($text, 0, 1) == "?") {
            $text = substr($text, 1, -1);
            $type = "declaration";
        } else {
            $type = "element";
        }
        $MODE_TAG_NOTHING = -1;
        $MODE_TAG_NAME = 0;
        $MODE_ATTR_BLANKS = 4;
        $MODE_ATTR_NAME = 1;
        $MODE_ATTR_BQUOTE = 2;
        $MODE_ATTR_VALUE = 3;
        $mode = $MODE_TAG_NOTHING;
        $tag = array("type" => $type, "name" => "", "attr" => array());
        for ($i = 0; $i < strlen($text); $i++) {
            $attrname = "";
            $attrvalue = "";
            if ($mode == $MODE_TAG_NOTHING) {
                if (!$this->_tiny_xml_is_blank(substr($text, $i, 1))) {
                    $mode = $MODE_TAG_NAME;
                    $tag["name"] .= substr($text, $i, 1);
                }
            } else if ($mode == $MODE_TAG_NAME) {
                if (!$this->_tiny_xml_is_blank(substr($text, $i, 1))) {
                    $tag["name"] .= substr($text, $i, 1);
                } else {
                    $mode = $MODE_ATTR_BLANKS;
                    $attrname = "";
                }
            } else if ($mode == $MODE_ATTR_BLANKS) {
                if (!$this->_tiny_xml_is_blank(substr($text, $i, 1))) {
                    $mode = $MODE_ATTR_NAME;
                    $attrname .= substr($text, $i, 1);
                }
            } else if ($mode == $MODE_ATTR_NAME) {
                if (!$this->_tiny_xml_is_blank(substr($text, $i, 1)) && substr($text, $i, 1) != "=") {
                    $attrname .= substr($text, $i, 1);
                } else {
                    $mode = $MODE_ATTR_BQUOTE;
                }
            } else if ($mode == $MODE_ATTR_BQUOTE) {
                if (substr($text, $i, 1) == "\"") {
                    $attrvalue = "";
                    $mode = $MODE_ATTR_VALUE;
                }
            } else if ($mode == $MODE_ATTR_VALUE) {
                if (substr($text, $i, 1) == "\"") {
                    $mode = $MODE_ATTR_BLANKS;
                    $tag["attr"][$attrname] = $attrvalue;
                    $attrname = "";
                } else {
                    $attrvalue .= substr($text, $i, 1);
                }
            }
        }
        return $tag;
    }

    /* convertir un elemento representado como texto, en una representacion mas manejable */

    function _tiny_xml_parse_tag($text)
    {
        if (substr($text, 0, 4) == "<!--") {
            return array(
                "type" => "comment",
                "text" => substr($text, 4, -3)
            );
        } else if (substr($text, 0, 1) == "<") {
            return $this->_tiny_xml_parse_tag_stuff($text);
        } else {
            return array(
                "type" => "text",
                "text" => $text
            );
        }
    }

    /* armar el arbol de elementos */

    function _tiny_xml_pass_3($tags, $depth, &$offset)
    {
        $content = array();
        $first = $offset;
        for ($offset = $first; $offset < count($tags); $offset++) {
            if ($depth[$offset] < $depth[$first]) {
                return $content;
            }
            if ($depth[$offset] > $depth[$first]) {
                $content[count($content) - 1]["childs"] = $this->_tiny_xml_pass_3($tags, $depth, $offset);
            } else if (substr($tags[$offset], 0, 2) != "</") {
                $content[] = $this->_tiny_xml_parse_tag($tags[$offset]);
            }
        }
        return $content;
    }

    /* si un elemento tiene un unico hijo de tipo texto, eliminar tal hijo y crear una propiedad "text" con el valor de texto en el mismo elemento */
    /* <pepe>hongo</pepe> ==> tag["type"] = "element"; tag["name"] = "pepe"; tag["text"] = "hongo"; tag["childs"] = {}; */

    function _tiny_xml_pass_4(&$content)
    {
        if ($content) {
            for ($i = 0; $i < count($content); $i++) {
                //die('////<pre>'.nl2br(var_export($content, true)).'</pre>////');
                if (isset($content[$i]["childs"])) {
                    if (
                        count($content[$i]["childs"]) == 1 &&
                        $content[$i]["childs"][0]["type"] == "text"
                    ) {
                        $content[$i]["text"] = $content[$i]["childs"][0]["text"];
                        unset($content[$i]["childs"]);
                    }
                    $this->_tiny_xml_pass_4($content[$i]["childs"]);
                }
            }
        }
    }

    /**
     * Parsea un XML
     *
     * $text		El texto a parsear
     * $skip_blanks		Si se debe omitir los espacios en blanco y retornos de carro entre tags
     * $collapse_blanks	Para un tag con un unico hijo de tipo texto, eliminar el hijo y asignar el valor de texto al tag directamente
     *
     * @return		Un array con los elementos parseados
     */
    function parse_xml($text, $skip_blanks = false, $collapse_texts = false)
    {
        $tags = $this->_tiny_xml_pass_1($text, $skip_blanks);
        $depth = $this->_tiny_xml_pass_2($tags);
        $offset = 0;
        $content = $this->_tiny_xml_pass_3($tags, $depth, $offset);
        if ($collapse_texts) {
            $this->_tiny_xml_pass_4($content);
        }
        return $content;
    }
}
