<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Bitacora as Bitacora;
use App\Entity\Chofer as Chofer;
use App\Model\app\UtilsManager;
use DateTime;

class BitacoraManager
{

    protected $em;
    protected $utils;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    private function add($tipo_evento, $ejecutor, $organizacion, $data = null)
    {
        $bitacora = new Bitacora();
        $bitacora->setTipoEvento($tipo_evento);

        //grabo la organizacion.
        if ($organizacion instanceof Organizacion) {
            $bitacora->setOrganizacion($organizacion);
        } else {
            $bitacora->setOrganizacion($this->em->getRepository('App:Organizacion')->findOneBy(array('id' => $organizacion)));
        }

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->findOneBy(array('id' => $ejecutor)));
            }
        }

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    private function addChofer($tipo_evento, $ejecutor, $chofer, $data = null)
    {
        $bitacora = new Bitacora();
        $bitacora->setTipoEvento($tipo_evento);

        //grabo el chofer.
        if ($chofer instanceof Chofer) {
            $bitacora->setOrganizacion($chofer->getOrganizacion());
            $bitacora->setChofer($chofer);
        } else {
            $bitacora->setChofer($this->em->getRepository('App:Chofer')->findOneBy(array('id' => $chofer)));
        }

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->findOneBy(array('id' => $ejecutor)));
            }
        }

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    /**
     * @param type $usuario Usuario que se looguea en el sistema
     * @param type $data   es el request del login para poder sacar la info de logueo
     */
    public function usuarioLogin($usuario, $data)
    {
        if (!is_null($data)) {
            $data = array(
                'remote_ip' => $data->server->get('REMOTE_ADDR'),
                'server_ip' => $data->server->get('SERVER_ADDR'),
                'user_agent' => $data->server->get('HTTP_USER_AGENT'),
            );
        }
        // se ignora los logins porque son una bocha.
        //   $this->add(Bitacora::EVENTO_USUARIO_LOGIN, $usuario, $usuario->getOrganizacion(), $data);
    }

    /**
     * @param type $ejecutor Persona que hace el cambio de clave.
     * @param type $usuario Es al usuario que se le realiza el cambio de clave
     * @param type $clean Si es por ingreso de clean de clave
     */
    public function usuarioChangePassword($ejecutor, $usuario, $clean, $request)
    {
        $data = array(
            'remote_ip' => $request->server->get('REMOTE_ADDR'),
            'usuario' => $usuario->getId(),
            'nombre' => $usuario->getNombre(),
            'userlogin' => $usuario->getUsername(),
            'clean' => $clean,
        );
        $this->add(Bitacora::EVENTO_USUARIO_CHANGE_PASSWORD, $ejecutor, $usuario->getOrganizacion(), $data);
    }

    /**
     * @param type $ejecutor  Persona que hace el cambio de clave.
     * @param type $usuario Usuario al que se ha cambiado los datos.
     * @param type $arrOld  Array del objeto viejo
     * @param type $arrNew  Array del objeto nuevo. $this->bitacora->toArray($obj)
     */
    public function usuarioUpdate($ejecutor, $usuario, $arrOld, $arrNew)
    {
        //        if (is_null($arrOld)) {
        //            $diff = $arrNew;
        //        } else {
        //            $diff = array_diff_assoc($arrNew, $arrOld);
        //            die('////<pre>'.nl2br(var_export($diff, true)).'</pre>////');
        //        }
        //        if (!isset($diff['nombre'])) {
        //            $diff['nombre'] = $arrNew['nombre'];
        //        }
        //        $this->add(Bitacora::EVENTO_USUARIO_UPDATE, $ejecutor, $usuario->getOrganizacion(), $diff);
    }

    /**
     * Registra en la bitacora la carga de combustible manual.
     * @param type $ejecutor Usuario que hace la operacion
     * @param type $servicio Servicio que realizo la carga
     * @param type $carga Carga de combustible.
     */
    public function combustibleCreateManual($ejecutor, $servicio, $carga)
    {
        $data = array(
            'carga' => $this->toArray($carga),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );
        $this->add(Bitacora::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA, $ejecutor, $servicio->getOrganizacion(), $data);
    }

    /**
     * Registra en la bitacora la carga de combustible manual.
     * @param type $ejecutor Usuario que hace la operacion
     * @param type $servicio Servicio que realizo la carga
     * @param type $carga Carga de combustible.
     */
    public function combustibleCreateAutomatica($request, $servicio, $carga)
    {
        $data = array(
            'remote_ip' => $request->server->get('REMOTE_ADDR'),
            'server_ip' => $request->server->get('SERVER_ADDR'),
            'carga' => $this->toArray($carga),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );
        //die('////<pre>'.nl2br(var_export($data, true)).'</pre>////');
        $this->add(Bitacora::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA, null, $servicio->getOrganizacion(), $data);
    }

    /**
     * 
     * @param type $ejecutor
     * @param type $servicio
     * @param type $carga Object con la carga nueva.
     * @param type $cargaOld Array con la carga vieja.
     */
    public function combustibleUpdate($ejecutor, $servicio, $carga, $cargaOld = array())
    {
        if (is_null($cargaOld)) {
            $diff = $this->toArray($carga);
        } else {
            $diff = array_diff_assoc($this->toArray($carga), $cargaOld);
        }
        $data = array(
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
            'id_carga' => $carga->getId(),
            'diff' => $diff,
        );
        $this->add(Bitacora::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA, $ejecutor, $servicio->getOrganizacion(), $data);
    }

    public function combustibleDelete($ejecutor, $servicio, $carga)
    {
        $data = array(
            'carga' => $this->toArray($carga),
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null
            ),
        );
        //$this->add(Bitacora::EVENTO_COMBUSTIBLE_DELETE, $ejecutor, $servicio->getOrganizacion(), $data);
    }

    /**
     * @param type $ejecutor  Persona que hace el cambio de clave.
     * @param type $usuario Usuario al que se ha cambiado los datos.
     */
    public function usuarioDelete($ejecutor, $usuario)
    {
        $data = array(
            'usuario' => $this->toArray($usuario)
        );
        $this->add(Bitacora::EVENTO_USUARIO_BAJA, $ejecutor, $usuario->getOrganizacion(), $data);
    }

    /**
     * @param type $ejecutor  Persona que hace el cambio de clave.
     * @param type $usuario Usuario al que se ha cambiado los datos.
     */
    public function usuarioUpdateRoles($ejecutor, $usuario, $dif_add, $dif_remove)
    {
        $data = array(
            'usuario' => array(
                'nombre' => $usuario->getNombre(),
                'id' => $usuario->getId(),
            ),
            'permisos' => array('add' => $dif_add, 'remove' => $dif_remove),
        );

        $this->add(Bitacora::EVENTO_USUARIO_UPDATE_PERMISOS, $ejecutor, $usuario->getOrganizacion(), $data);
    }

    /**
     * @param type $ejecutor  Persona que hace el cambio de clave.
     * @param type $organizacion Organizacion al que se ha cambiado los datos.
     */
    public function organizacionUpdateModulos($ejecutor, Organizacion $organizacion, $dif_add, $dif_remove)
    {
        $data = array(
            'organizacion' => array(
                'nombre' => $organizacion->getNombre(),
                'id' => $organizacion->getId(),
            ),
            'modulos' => array('add' => $dif_add, 'remove' => $dif_remove),
        );

        $this->add(Bitacora::EVENTO_ORGANIZACION_UPDATE_MODULO, $ejecutor, $organizacion, $data);
    }

    public function webserviceAdd($evento, $data)
    {
        return $this->add($evento, null, null, $data);
    }

    public function toArray($obj)
    {

        if ($obj) {
            return $this->object2array($obj->getVars());
        }
        return array();
    }

    //FIXME: Hay un error con la version 5.4.6 de PHP con la funcion array_diff_assoc. Con esta correccion no se exportan mas de un nivel del array. Solucion provisora.
    protected function object2array($obj)
    {
        $arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
        $arr = null;
        foreach ($arrObj as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? $this->object2array($val) : $val;
            if (!is_null($val)) {
                $arr[$key] = $val;
            }
        }
        return $arr;
    }

    public function organizacionUpdate($ejecutor, $organizacion, $action)
    {
        $data = array(
            'organizacion' => array(
                'nombre' => $organizacion->getNombre(),
                'id' => $organizacion->getId(),
            )
        );
        switch ($action) {
            case 1:
                $this->add(Bitacora::EVENTO_ORGANIZACION_ALTA, $ejecutor, $organizacion, $data);
                break;
            case 3:
                $this->add(Bitacora::EVENTO_ORGANIZACION_UPDATE, $ejecutor, $organizacion, $data);
                break;
        }
    }

    public function organizacionDelete($ejecutor, $organizacion)
    {
        $data = array(
            'organizacion' => array(
                'nombre' => $organizacion->getNombre(),
                'id' => $organizacion->getId(),
            )
        );
        $this->add(Bitacora::EVENTO_ORGANIZACION_BAJA, $ejecutor, $organizacion, $data);
    }

    public function updateReferencia($ejecutor, $organizacion, $referencia, $action)
    {
        $data = array(
            'referencia' => array(
                'nombre' => $referencia->getNombre(),
                'latitud' => $referencia->getLatitud(),
                'longitud' => $referencia->getLongitud(),
                'direccion' => $referencia->getDireccion()
            )
        );
        if ($action == 1) {
            $this->add(Bitacora::EVENTO_REFERENCIA_ALTA, $ejecutor, $organizacion, $data);
        } elseif ($action == 2) {
            $this->add(Bitacora::EVENTO_REFERENCIA_UPDATE, $ejecutor, $organizacion, $data);
        }
    }

    public function deleteReferencia($ejecutor, $organizacion, $referencia)
    {
        $data = array(
            'referencia' => array(
                'nombre' => $referencia->getNombre(),
                'latitud' => $referencia->getLatitud(),
                'longitud' => $referencia->getLongitud(),
                'direccion' => $referencia->getDireccion(),
            )
        );
        $this->add(Bitacora::EVENTO_REFERENCIA_BAJA, $ejecutor, $organizacion, $data);
    }

    public function parse($registro)
    {
        $str = '';
        $data = $registro->getData();
        switch ($registro->getTipoEvento()) {
            case $registro::EVENTO_USUARIO_CHANGE_PASSWORD:
                $str = sprintf('Se modificó la contraseña de usuario: <b>%s</b> ( %s )', $data['nombre'], $data['userlogin']);
                break;
            case $registro::EVENTO_USUARIO_UPDATE:
                $str = sprintf('Se modificaron los datos de <b>%s</b>, %s', $data['nombre'], $this->convertUserToString($data));
                //   die('////<pre>'.nl2br(var_export($str, true)).'</pre>////');
                break;
            case $registro::EVENTO_USUARIO_BAJA:
                $str = sprintf('Se eliminó el usuario: <b>%s</b> ( %s )', $data['usuario']['nombre'], $data['usuario']['username']);
                break;
            case $registro::EVENTO_USUARIO_UPDATE_PERMISOS:
                $str = sprintf('Se modificaron los permisos de usuario: <b>%s</b> ( %s )', $data['usuario']['nombre'], $this->convertToString($data['permisos'], $registro->getTipoEvento()));
                break;
            case $registro::EVENTO_ORGANIZACION_UPDATE_MODULO:
                $str = sprintf('Se modificaron los modulos de organización: <b>%s</b> ( %s )', $data['organizacion']['nombre'], $this->convertToString($data['modulos'], $registro->getTipoEvento()));
                break;
            case $registro::EVENTO_ORGANIZACION_ALTA:
                $str = sprintf('Se agregó la organización: <b>%s</b>', $data['organizacion']['nombre']);
                break;
            case $registro::EVENTO_ORGANIZACION_UPDATE:
                $str = sprintf('Se modificó la organización: <b>%s</b>', $data['organizacion']['nombre']);
                break;
            case $registro::EVENTO_ORGANIZACION_BAJA:
                $str = sprintf('Se eliminó la organización: <b>%s</b>', $data['organizacion']['nombre']);
                break;
            case $registro::EVENTO_REFERENCIA_ALTA:
                $str = sprintf('Se agregó la referencia: <b>%s</b> con dirección %s (latitud %s, longitud %s)', $data['referencia']['nombre'], $data['referencia']['direccion'], $data['referencia']['latitud'], $data['referencia']['longitud']);
                break;
            case $registro::EVENTO_REFERENCIA_UPDATE:
                $str = sprintf('Se modificó la referencia: <b>%s</b> con dirección %s (latitud %s, longitud %s)', $data['referencia']['nombre'], $data['referencia']['direccion'], $data['referencia']['latitud'], $data['referencia']['longitud']);
                break;
            case $registro::EVENTO_REFERENCIA_BAJA:
                $str = sprintf('Se eliminó la referencia: <b>%s</b> con dirección %s (latitud %s, longitud %s)', $data['referencia']['nombre'], $data['referencia']['direccion'], $data['referencia']['latitud'], $data['referencia']['longitud']);
                break;
            case $registro::EVENTO_CHOFER_ASIGNAR_SERVICIO:
                if (isset($data['servicio'])) {
                    $str = sprintf('Se asigna servicio: <b>%s</b> con patente %s', $data['servicio']['nombre'], $data['servicio']['patente']);
                } else {
                    $str = 'Sin data';
                }
                break;
            case $registro::EVENTO_CHOFER_RETIRAR_SERVICIO:
                if (isset($data['servicio'])) {
                    if (isset($data['servicio']['nombre'])) {
                        $str = sprintf('Se retira servicio: <b>%s</b> con patente %s', $data['servicio']['nombre'], $data['servicio']['patente']);
                    } else {
                        $str = sprintf('Se retira servicio patente %s', $data['servicio']['patente']);
                    }
                } else {
                    $str = 'Sin data';
                }
                break;
            case $registro::EVENTO_EV_ALTA:
                $str = sprintf('Se crea evento: <b>%s</b>, tipo de evento %s, estado %s', $data['evento']['nombre'], $data['evento']['tipo_evento'], $data['evento']['estado']);
                break;
            case $registro::EVENTO_EV_UPDATE:
                $str = sprintf('Se modifica <b>%s</b> de evento: <b>%s</b>, tipo de evento %s, estado %s', $data['evento']['data'] ? $data['evento']['data'] : '', $data['evento']['nombre'], $data['evento']['tipo_evento'], $data['evento']['estado']);
                break;
            case $registro::EVENTO_EV_BAJA:
                $str = sprintf('Se elimina evento: <b>%s</b>, tipo de evento %s, estado %s', $data['evento']['nombre'], $data['evento']['tipo_evento'], $data['evento']['estado']);
                break;
            case $registro::EVENTO_CHOFER_ENTREGAR_SERVICIO:
                if (isset($data['servicio']['nombre'])) {                
                    if ($data['servicio']['fecha'] instanceof DateTime) {                        
                        $fecha = $data['servicio']['fecha']->format('d-m-Y');
                    } else {
                        $fecha = $data['servicio']['fecha'];
                    }
                    $str = sprintf('Se entrega servicio: <b>%s</b> con patente %s y fecha %s', $data['servicio']['nombre'], $data['servicio']['patente'], $fecha);
                } else {
                    $str = sprintf('Se entrega servicio: <b>%s</b> con patente %s y fecha %s', $data['servicio']['servicio'], $data['servicio']['patente'], $data['servicio']['fecha']);
                }
                break;
            case $registro::EVENTO_CHOFER_TOMAR_SERVICIO:
                if (isset($data['servicio']['nombre'])) {
                    $str = sprintf('Se toma servicio: <b>%s</b> con patente %s y fecha %s', $data['servicio']['nombre'], $data['servicio']['patente'], isset($data['servicio']['fecha']) ? $data['servicio']['fecha']->format('d-m-Y H:i') : '---');
                } else {
                    $str = sprintf('Se toma servicio: <b>%s</b> con patente %s y fecha %s', $data['servicio']['servicio'], $data['servicio']['patente'], $data['servicio']['fecha']);
                }
                break;
        }
        return $str;
    }

    public function getTipoEventos()
    {
        $bitacora = new Bitacora();
        return $bitacora->getArrayTipoEvento();
    }

    public function obtener($organizacion, $desde, $hasta, $tevento = null)
    {
        $desde = $this->utils->datetime2sqltimestamp($desde);
        $hasta = $this->utils->datetime2sqltimestamp($hasta, true);
        return $this->em->getRepository('App:Bitacora')->byOrganizacion($organizacion, $desde, $hasta, $tevento);
    }

    public function obtenerByChofer($chofer)
    {
        return $this->em->getRepository('App:Bitacora')->byChofer($chofer);
    }

    public function convertToString($array, $registro)
    {
        $str = '';
        $bitacora = new Bitacora();
        switch ($registro) {
            case $bitacora::EVENTO_USUARIO_UPDATE_PERMISOS:
                if ($array['add'] != null) {
                    $stradd = implode(', ', $array['add']);
                } else {
                    $stradd = 'Ninguno';
                }
                if ($array['remove'] != null) {
                    $strremove = implode(', ', $array['remove']);
                } else {
                    $strremove = 'Ninguno';
                }
                $str = 'se agregaron: ' . $stradd . ', se quitaron: ' . $strremove;
                break;

            case $bitacora::EVENTO_ORGANIZACION_UPDATE_MODULO:
                if ($array['add'] != null) {
                    $stradd = implode(', ', $array['add']);
                } else {
                    $stradd = 'Ninguno';
                }
                if ($array['remove'] != null) {
                    $strremove = implode(', ', $array['remove']);
                } else {
                    $strremove = 'Ninguno';
                }
                $str = 'se agregaron: ' . $stradd . ', se quitaron: ' . $strremove;
                break;
        }
        return $str;
    }

    public function convertUserToString($data)
    {
        $str = '';
        if (isset($data['username'])) {
            $str .= 'Login: ' . $data['nombre'] . ', ';
        }
        if (isset($data['timeZone'])) {
            $str .= 'TimeZone: ' . $data['timeZone'] . ', ';
        }
        if (isset($data['email'])) {
            $str .= 'Email: ' . $data['email'] . ', ';
        }
        if (isset($data['enabled'])) {
            if ($data['enabled'] == 1) {
                $str .= 'Estado: Habilitado' . ', ';
            } else {
                $str .= 'Estado: Deshabilitado' . ', ';
            }
        }
        if (isset($data['vista_mapa'])) {
            if ($data['vista mapa'] == 0) {
                $str .= 'Vista: Mapa y Satélite' . ', ';
            } elseif ($data['vista mapa'] == 1) {
                $str .= 'Vista: Mapa' . ', ';
            } else {
                $str .= 'Vista: Satélite' . ', ';
            }
        }
        if (isset($data['change password'])) {
            if ($data['change_password'] == 0) {
                $str .= 'Cambiar Pass: NO' . ', ';
            } elseif ($data['change_password'] == 1) {
                $str .= 'Cambiar Pass: SI' . ', ';
            }
        }
        if (isset($data['limite_historial'])) {
            $str .= 'Limite de historial: ' . $data['limite_historial'] . ' días' . ', ';
        }
        if (isset($data['showReferencias'])) {
            if ($data['enabled'] == 1) {
                $str .= 'Mostrar Referencias: Si' . ', ';
            } else {
                $str .= 'Mostrar Referencias: No' . ', ';
            }
        }
        if (isset($data['redirigir_login'])) {
            if ($data['enabled'] == 1) {
                $str .= 'Redirigir Login: No' . ', ';
            } else {
                $str .= 'Redirigir Login: A ver Flota' . ', ';
            }
        }
        if ($str != '') {
            $str = '( ' . $str . ' )';
        }
        return $str;
    }

    public function choferAsignarServicio($ejecutor, $chofer, $servicio)
    {
        $data = array(
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'id' => $servicio->getId(),
                'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : 'n/n',
            ),
        );
        $this->addChofer(Bitacora::EVENTO_CHOFER_ASIGNAR_SERVICIO, $ejecutor, $chofer, $data);
    }

    public function choferQuitarServicio($ejecutor, $chofer, $servicio)
    {
        $data = array(
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'id' => $servicio->getId(),
                'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : 'n/n',
            ),
        );
        $this->addChofer(Bitacora::EVENTO_CHOFER_RETIRAR_SERVICIO, $ejecutor, $chofer, $data);
    }

    public function choferServicio($ejecutor, $motivo, $historico)
    {
        $servicio = $historico->getServicio();
        $chofer = $historico->getChofer();
        $data = array(
            'servicio' => array(
                'nombre' => $servicio->getNombre(),
                'id' => $servicio->getId(),
                'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : 'n/n',
                'fecha' => $historico->getFecha()
            ),
        );
        $this->addChofer($motivo == 0 ? Bitacora::EVENTO_CHOFER_TOMAR_SERVICIO : Bitacora::EVENTO_CHOFER_ENTREGAR_SERVICIO, $ejecutor, $chofer, $data);
    }

    public function updateEvento($ejecutor, $evento, $action, $data = null)
    {
        $data = array(
            'evento' => array(
                'nombre' => $evento->getNombre(),
                'estado' => $evento->getActivo() ? 'Activo' : 'Desactivado',
                'tipo_evento' => $evento->getTipoEvento()->getNombre(),
                'data' => $data,

            )
        );
        if ($action == 1) {
            $this->add(Bitacora::EVENTO_EV_ALTA, $ejecutor, $evento->getOrganizacion(), $data);
        } elseif ($action == 2) {
            $this->add(Bitacora::EVENTO_EV_UPDATE, $ejecutor, $evento->getOrganizacion(), $data);
        }
    }

    public function deleteEvento($ejecutor, $evento)
    {
        $data = array(
            'evento' => array(
                'nombre' => $evento->getNombre(),
                'estado' => $evento->getActivo() ? 'Activo' : 'Desactivado',
                'tipo_evento' => $evento->getTipoEvento()->getNombre(),
            )
        );
        $this->add(Bitacora::EVENTO_EV_BAJA, $ejecutor, $evento->getOrganizacion(), $data);
    }
}
