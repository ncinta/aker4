<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Categoria;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\OrganizacionManager;

class CategoriaManager
{

    protected $em;
    protected $organizacion;

    public function __construct(EntityManagerInterface $em, OrganizacionManager $organizacion)
    {
        $this->organizacion = $organizacion;
        $this->em = $em;
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:Categoria')->find($id);
    }

    public function findAll($organizacion, array $orderBy = null)
    {
        $org = $this->organizacion->find($organizacion);
        if ($org) {
            if (!$orderBy) {
                $orderBy = array('nombre' => 'ASC');
            }
            return $this->em->getRepository('App:Categoria')
                ->findBy(
                    array('organizacion' => $org->getId()),
                    $orderBy
                );
        } else {
            return null;
        }
    }

    public function create($propietario)
    {
        $org = $this->organizacion->find($propietario);
        if ($org) {
            $categoria = new Categoria();
            $categoria->setOrganizacion($org);
            return $categoria;
        } else {
            return null;
        }
    }

    public function save(Categoria $categoria)
    {
        $id = $categoria->getId();
        $this->em->persist($categoria);
        $this->em->flush();
        return $categoria;
    }

    public function delete($categoria)
    {
        if ($categoria instanceof Categoria) {
            $categ = $categoria;
        } else {
            $categ = $this->findById($categoria);
            if (!$categ) {
                return false;
            }
        }

        if (count($categ->getReferencias()) > 0) {
            return false;
        }

        $this->em->remove($categ);
        $this->em->flush();
        return true;
    }
}
