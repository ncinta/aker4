<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CentroCosto;
use App\Entity\Prestacion;
use App\Entity\Organizacion as Organizacion;

class CentroCostoManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($centrocosto)
    {
        if ($centrocosto instanceof CentroCosto) {
            return $this->em->getRepository('App:CentroCosto')->findBy(array('id' => $centrocosto->getId()));
        } else {
            return $this->em->getRepository('App:CentroCosto')->find(array('id' => $centrocosto));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:CentroCosto')->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'asc'));
        } else {
            return $this->em->getRepository('App:CentroCosto')->findBy(array('organizacion' => $organizacion), array('nombre' => 'asc'));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $centrocosto = new CentroCosto();
        $centrocosto->setOrganizacion($organizacion);

        return $centrocosto;
    }

    public function save(CentroCosto $centrocosto)
    {
        $this->em->persist($centrocosto);
        $this->em->flush();
        return $centrocosto;
    }

    public function delete(CentroCosto $centrocosto)
    {
        $this->em->remove($centrocosto);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $centrocosto = $this->find($id);
        if (!$centrocosto) {
            throw $this->createNotFoundException('Código de centrocosto no encontrado.');
        }
        return $this->delete($centrocosto);
    }

    public function getServicios($centrocosto)
    {
        return $centrocosto->getServicios();
    }

    public function addServicio($centrocosto, $servicio)
    {
        if (true) {  //trae el usuario asociado.
            $servicio->setCentrocosto($centrocosto);
            $centrocosto->addServicio($servicio);
            $this->em->persist($servicio);
            $this->em->flush();
            return true;
        } else {
            return false;
        }
    }
}
