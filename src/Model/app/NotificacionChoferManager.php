<?php

namespace App\Model\app;

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Chofer;
use App\Entity\NotificacionChofer;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\UtilsManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of NotificacionChoferManager
 *
 * @author nicolas
 */
class NotificacionChoferManager
{

    protected $em;
    protected $security;
    protected $utils;
    protected $chofer;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $security, UtilsManager $utils)
    {
        $this->security = $security;
        $this->utils = $utils;
        $this->em = $em;
    }

    /**
     * Buscan un chofer por su id u objeto.
     * @param Chofer|Int $chofer   el Chofer o el id del chofer
     * @return type 
     */
    public function find($notificacion)
    {
        if ($notificacion instanceof NotificacionChofer) {
            return $this->em->getRepository('App:NotificacionChofer')->find($notificacion->getId());
        } else {
            return $this->em->getRepository('App:NotificacionChofer')->find($notificacion);
        }
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:NotificacionChofer')->find($id);
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:NotificacionChofer')
            ->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function findNotifChof($id)
    {
        return $this->em->getRepository('App:NotifChof')
            ->findOneBy(array('id' => $id));
    }

    public function findNotifContact($id)
    {
        return $this->em->getRepository('App:NotifContact')
            ->findOneBy(array('id' => $id));
    }

    public function deleteNotifChof($notifChof)
    {
        $this->em->remove($notifChof);
        $this->em->flush();
        return true;
    }

    public function deleteNotifContact($notifContact)
    {
        $this->em->remove($notifContact);
        $this->em->flush();
        return true;
    }

    public function create(Organizacion $organizacion)
    {
        $notificacion = new NotificacionChofer();
        $notificacion->setOrganizacion($organizacion);
        return $notificacion;
    }

    public function save($notificacion)
    {
        $this->em->persist($notificacion);
        $this->em->flush();
        return $notificacion;
    }

    public function addChofer($notificacion, $choferes)
    {
        foreach ($choferes as $chofer) {
            $notifChof = new \App\Entity\NotifChof();
            $notifChof->setChofer($chofer);
            $notifChof->setNotificacion($notificacion);
            $this->em->persist($notifChof);
        }
        $this->em->flush();
        return $notifChof;
    }

    public function addContacto($notificacion, $contactos)
    {
        foreach ($contactos as $contacto) {
            $notifContact = new \App\Entity\NotifContact();
            $notifContact->setContacto($contacto);
            $notifContact->setNotificacion($notificacion);
            $this->em->persist($notifContact);
        }
        $this->em->flush();
        return $notifContact;
    }

    public function delete($notificacion)
    {
        $this->em->remove($this->find($notificacion));
        $this->em->flush();
        return true;
    }
}
