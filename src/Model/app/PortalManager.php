<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\PrecioPortal;
use App\Model\app\UserLoginManager;

class PortalManager
{

    protected $em;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
    }


    private $arrayStrDiasSemana = array(
        'domingo',
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado'
    );

    public function getArrayStrDiasSemana()
    {
        return $this->arrayStrDiasSemana;
    }

    public function getArrayTipoPortal()
    {
        $p = new PrecioPortal();
        return $p->getArrayStrTipoPortal();
    }

    public function find($id)
    {
        return $this->em->getRepository('App:PrecioPortal')->find($id);
    }

    public function findAllByReferencia($idref = 0)
    {
        return $this->em->getRepository('App:PrecioPortal')->findByReferencia($idref);
    }

    public function save($portal)
    {
        $this->em->persist($portal);
        $this->em->flush();
        return (true);
    }
}
