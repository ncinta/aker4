<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Historial\BackendRequest as BackendRequest;
use Historial\ContextHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UtilsManager;

class BackendManager
{

    const OK = 0;
    const ERROR_BACKEND_PRINCIPAL = 1;
    const ERROR_BACKEND_SECUNDARIO = 2;

    protected $strLastError = array(
        self::ERROR_BACKEND_PRINCIPAL => 'Error Backend Principal',
        self::ERROR_BACKEND_SECUNDARIO => 'Error Backend Secundario'
    );

    protected $em;
    protected $usuario;
    protected $organizacion;
    private $tz_local, $tz_utc;
    protected $backend_addr;
    protected $backend_addr_backup;
    protected $aker_backend_dias;
    protected $fechaLimite;
    protected $utils;
    private $client;
    private $last_error = 0;

    public function __construct(
        EntityManagerInterface $em,
        OrganizacionManager $organizacion,
        ContainerInterface $container,
        UserLoginManager $userlogin,
        UtilsManager $utils,
        HttpClientInterface $client
    ) {
        $this->last_error = self::OK;

        $this->client = $client;
        $this->backend_addr = $container->getParameter('aker_backend_principal');
        $this->backend_addr_backup = $container->getParameter('aker_backend_secundario');
        $this->aker_backend_dias = $container->getParameter('aker_backend_dias');
        $this->em = $em;
        $this->usuario = $userlogin;
        $this->organizacion = $organizacion;
        $this->utils = $utils;
        $this->tz_local = new \DateTimeZone($this->usuario->getTimeZone());
        $this->tz_utc = new \DateTimeZone('UTC');

        $fecha_actual = date("d-m-Y 03:00:00");
        //es la fecha hasta donde tengo historial en el principal, sino buscar en el secundario
        $this->fechaLimite = strtotime($fecha_actual . "-" . $this->aker_backend_dias . " days");
        //die('////<pre>' . nl2br(var_export(date('Y-m-d H:i:s', $this->fechaLimite), true)) . '</pre>////');
    }

    private function fecha_a_UTC($fecha)
    {
        $fecha = new \DateTime($fecha, $this->tz_local);
        $fecha->setTimezone($this->tz_utc);
        return date_format($fecha, 'Y-m-d H:i:s');
    }

    private function fecha_a_local($fecha)
    {
        if ($fecha == '---')
            return $fecha;
        $fecha = new \DateTime($fecha, $this->tz_utc);
        //die('////<pre>' . nl2br(var_export($fecha, true)) . '</pre>////');
        $fecha->setTimezone($this->tz_local);

        return date_format($fecha, 'Y-m-d H:i:s');
    }

    public function getLastError()
    {
        return $this->last_error;
    }
    public function getStrLastError($error)
    {
        return array_search($error, $this->strLastError);
    }

    private function buscar_backend($servicio_id, $intentos = 0)
    {
        //$servicio_id es para redirigir al backend correspndiente del servicios
        if ($intentos == 0) {
            return $this->backend_addr;
        } else {
            return $this->backend_addr_backup;
        }
    }

    /**
     * Trabaja con las fechas y busca en los backend correspondientes segun necesite para
     * armar todo el request
     */
    private function obtenerRequest($servicio_id, $informe, $context)
    {
        $backend = $this->backend_addr;
        $result = null;
        $request = null;
        if (!isset($context['desde'])) {
            $backend = $this->backend_addr;
        } else {
            if ($this->fechaLimite <= strtotime($context['desde'])) {  //debo buscar en le principal.            
                $backend = $this->backend_addr;
            } else { //tengo consultas antes de fechaLimite, siempre busco en el viejo
                $backend = $this->backend_addr_backup;
            }
        }

        //hago la busqueda
        $request = new BackendRequest($this->client, $backend, $informe, $context);
        if (is_null($request->getResult())) {  //determino el error del backend principal
            $this->last_error = self::ERROR_BACKEND_PRINCIPAL;
            $result = array('fail' => self::ERROR_BACKEND_PRINCIPAL);
        } else {
            $result = $request->getResult();
        }

        //die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');        
        if (is_string($result)) {
            return  json_decode($result, true);
        } else {
            return $result;
        }
    }

    public function historial($servicio_id, $desde, $hasta, $referencias = array(), $opciones = array())
    {
        // TODO: considerar mover los métodos de ContextHelper a BackendManager
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias),
            'opcion' => $opciones,
        );
        //dd($context);
        $result = $this->obtenerRequest($servicio_id, 'historial', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            // die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');

            for ($i = 0; $i < count($result['historial']); $i++) {
                if (isset($result['historial'][$i]['fecha'])) {
                    $result['historial'][$i]['fecha'] = $this->fecha_a_local($result['historial'][$i]['fecha']);
                }
                if (isset($result['historial'][$i]['fecha_recepcion'])) {
                    $result['historial'][$i]['fecha_recepcion'] = $this->fecha_a_local($result['historial'][$i]['fecha_recepcion']);
                }
                if (isset($result['historial'][$i]['horometro'])) {
                    $result['historial'][$i]['horometro'] = $this->utils->minutos2tiempo($result['historial'][$i]['horometro']);
                }
            }
        }
        return isset($result['ok']) ? $result['historial'] : array();
    }
    //solamente tramas con canbusData
    public function historialCanbus($servicio_id, $desde, $hasta, $referencias = array(), $opciones = array())
    {

        $result = $this->historial($servicio_id, $desde, $hasta, $referencias, $opciones);
        $resultCanbus = array();
        foreach ($result as $key => $value) {
            if (isset($value['canbusData'])) {
                $resultCanbus[$key] = $result[$key];
            }
        }
        // die('////<pre>' . nl2br(var_export($resultCanbus, true)) . '</pre>////');
        return count($resultCanbus) > 0 ? $resultCanbus : array();
    }

    public function recorrido($servicio_id, $desde, $hasta, $referencias = array(), $opciones = array())
    {
        // TODO: considerar mover los métodos de ContextHelper a BackendManager
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias),
            'opcion' => $opciones,
        );
        $result = $this->obtenerRequest($servicio_id, 'recorrido', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['historial']); $i++) {
                $result['historial'][$i]['fecha'] = $this->fecha_a_local($result['historial'][$i]['fecha']);
                $result['historial'][$i]['fecha_recepcion'] = isset($result['historial'][$i]['fecha_recepcion']) ? $this->fecha_a_local($result['historial'][$i]['fecha_recepcion']) : null;
            }
        }
        return isset($result['ok']) ? $result['historial'] : array();
    }

    public function informeDistancias($servicio_id, $desde, $hasta, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias)
        );


        $result = $this->obtenerRequest($servicio_id, 'informe-kilometros', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            $result['informe']['desde'] = $this->fecha_a_local($result['informe']['desde']);
            $result['informe']['hasta'] = $this->fecha_a_local($result['informe']['hasta']);
        }
        return isset($result['ok']) ? $result['informe'] : array();
    }

    public function informeExcesosVelocidad($servicio_id, $desde, $hasta, $velocidad_maxima, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'velocidad_maxima' => $velocidad_maxima,
            'solo_referencias' => count($referencias) > 0,
            'referencia' => $helper->castReferencias($referencias),
        );
        //dd($context);

        $result = $this->obtenerRequest($servicio_id, 'informe-excesos-velocidad', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha'] = $this->fecha_a_local($result['informe'][$i]['fecha']);
            }
        }
        return isset($result['ok']) ? $result['informe'] : array();
    }

    public function informeDetenciones($servicio_id, $desde, $hasta, $tiempo_minimo, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'tiempo_minimo' => $tiempo_minimo,
            'referencia' => $helper->castReferencias($referencias)
        );
        //die('////<pre>' . nl2br(var_export($context, true)) . '</pre>////');
        $result = $this->obtenerRequest($servicio_id, 'informe-detenciones', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_ingreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_ingreso']);
                $result['informe'][$i]['fecha_egreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_egreso']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }

    public function informePortales($servicio_id, $desde, $hasta, $portales)
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'portal' => array(),
            'zona_horaria' => $this->usuario->getOrganizacion()->getTimeZone(), // zona horaria de los portales
            'referencia' => array()
        );

        $referencias = array();
        foreach ($portales as $portal) {
            $referencia = $portal->getReferencia();   //es la referencia actual.
            $context['portal'][] = array(
                'id' => $portal->getId(),
                'dias_semana' => $portal->getArrayDiasSemana(),
                // 'desde'y 'hasta' en "hora local", donde "hora local" está dado
                // por la zona horaria en $context['zona_horaria'] => $organizacion->timeZone
                // el backend se encarga de convertir la hora de la trama para comparar
                'desde' => $portal->getDesde()->format('H:i:s'),
                'hasta' => $portal->getHasta()->format('H:i:s'),
                'referencia_id' => $referencia->getId()
            );
            if (!is_null($referencia->getPoligono())) {
                $poli = $referencia->getClase() === $referencia::CLASE_POLIGONO && $referencia->isOldPoligon() ? $referencia->getPoligono() : '3,#ffff0000,#19ffffff,' . $referencia->getPoligono();
            } else {
                $poli = null;
            }
            if (!in_array($referencia->getId(), $referencias)) {
                $referencias[] = $referencia->getId();
                $context['referencia'][] = array(
                    'referencia_id' => $referencia->getId(),
                    'latitud' => $referencia->getLatitud(),
                    'longitud' => $referencia->getLongitud(),
                    'radio' => $referencia->getRadio(),
                    'poligono' => $poli,
                    'angulo_deteccion' => $referencia->getAnguloDeteccion(),
                );
            }
        }
        $result = $this->obtenerRequest($servicio_id, 'informe-portales-por-equipo', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha'] = $this->fecha_a_local($result['informe'][$i]['fecha']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }

    public function informeConsumoCombustible($servicio_id, $desde, $hasta, $carga_combustible, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'carga_combustible' => array(),
            'referencia' => $helper->castReferencias($referencias)
        );
        foreach ($carga_combustible as $carga) {
            if (is_array($carga)) {
                $fecha = $carga['fecha'];
                $fecha->setTimezone($this->tz_utc);
                $context['carga_combustible'][] = array(
                    'fecha' => $this->fecha_a_UTC($fecha->format('Y-m-d H:i:s')),
                    'guia_despacho' => $carga['guia_despacho'],
                    'litros_carga' => $carga['litros_carga'],
                    'monto_total' => $carga['monto_total'],
                    'odometro' => $carga['odometro']
                );
            } else {
                $context['carga_combustible'][] = array(
                    // TODO: determinar si estas fechas están almacenadas en UTC o en hora local
                    // FIXME: Agrege esta linea para pasar la fecha de la carga a utc. No se si esa correcto.
                    'fecha' => $this->fecha_a_UTC($carga->getFecha()->format('Y-m-d H:i:s')),
                    'guia_despacho' => $carga->getGuiaDespacho(),
                    'litros_carga' => $carga->getLitrosCarga(),
                    'monto_total' => $carga->getMontoTotal(),
                    'odometro' => $carga->getOdometro()
                );
            }
        }
        //die('////<pre>' . nl2br(var_export($context, true)) . '</pre>////');
        $result = $this->obtenerRequest($servicio_id, 'consumo-combustible', $context);

        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['consumo']); $i++) {
                $result['consumo'][$i]['carga1']['fecha'] = $this->fecha_a_local($result['consumo'][$i]['carga1']['fecha']);
                $result['consumo'][$i]['carga2']['fecha'] = $this->fecha_a_local($result['consumo'][$i]['carga2']['fecha']);
            }
        }
        return isset($result['consumo']) ? $result['consumo'] : array();
    }

    public function informeReportes($servicio_id, $desde, $hasta, $opciones = array())
    {
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'opcion' => $opciones,
        );
        $result = $this->obtenerRequest($servicio_id, 'informe-reportes', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            $result['informe']['fecha_inicio'] = $this->fecha_a_local($result['informe']['fecha_inicio']);
            $result['informe']['fecha_fin'] = $this->fecha_a_local($result['informe']['fecha_fin']);
            return $result['informe'];
        } else {
            return array();
            // FIXME: lanzar una excepción más específica
            // throw $this->createNotFoundHttpException(print_r($result, true));
        }
    }

    public function informeRalenti($servicio_id, $desde, $hasta, $tiempo_minimo, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'tiempo' => $tiempo_minimo,
            'referencia' => $helper->castReferencias($referencias)
        );
        $result = $this->obtenerRequest($servicio_id, 'informe-ralenti', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_inicio'] = $this->fecha_a_local($result['informe'][$i]['fecha_inicio']);
                $result['informe'][$i]['fecha_fin'] = $this->fecha_a_local($result['informe'][$i]['fecha_fin']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }

    public function trama($servicio_id, $oid)
    {
        $context = array(
            'servicio_id' => $servicio_id,
            'oid' => $oid,
        );
        //$result = $this->obtenerRequest($servicio_id, 'trama', $context);
        $request = new BackendRequest($this->client, $this->backend_addr, 'trama', $context);
        if (is_null($request->getResult())) {  //determino el error del backend principal
            $this->last_error = self::ERROR_BACKEND_PRINCIPAL;
            $result = array('fail' => self::ERROR_BACKEND_PRINCIPAL);
        } else {
            $result = json_decode($request->getResult(), true);
        }


        if ($result && isset($result['ok']) && $result['ok']) {
            return $result['trama'];
        } else {
            return array();
        }
    }

    public function informePasoReferencias($servicio_id, $desde, $hasta, $referencias = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias)
        );

        $result = $this->obtenerRequest($servicio_id, 'informe-referencias', $context);

        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_ingreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_ingreso']);
                $result['informe'][$i]['fecha_egreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_egreso']);
            }
        }
        //  dd($result);
        return isset($result['informe']) ? $result['informe'] : array();
    }

    public function informeInputs($servicio_id, $desde, $hasta, $referencias = array(), $options)
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'options' => $options,
            'referencia' => $helper->castReferencias($referencias),
        );

        $result = $this->obtenerRequest($servicio_id, 'informe-inputs', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_ingreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_ingreso']);
                $result['informe'][$i]['fecha_egreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_egreso']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }

    /**
     * fecha: debe estar en hora local y se convertira a utc
     */
    public function obtenerTramaAnterior($servicio_id, $fecha)
    {
        if ($fecha instanceof \DateTime) {
            $strfecha = $fecha->format('Y-m-d H:i:s');
        } else {
            $strfecha = $fecha;
        }
        $context = array(
            'servicio_id' => $servicio_id,
            'fecha' => $this->fecha_a_UTC($strfecha),
        );
        $result = $this->obtenerRequest($servicio_id, 'trama-anterior', $context);

        if ($result && isset($result['ok']) && $result['ok']) {
            if ($result['trama'] != null) {
                $result['trama']['fecha'] = $this->fecha_a_local($result['trama']['fecha']);
            }
        }
        //dd($result);

        if (isset($result['trama'])) {
            return $result['trama'];
        } else {
            return null;
        }
    }

    /**
     * A este informe no se le pasa el tiempo de permanencia ya que eso lo determina el controlador
     * luego de obtener todos los pasos. A mi entender esto deberia ser resuelto por el mismo backend.
     */
    public function informeEstadiaReferencias($servicio_id, $desde, $hasta, $referencias = array(), $options = null)
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'options' => $options,
            'referencia' => $helper->castReferencias($referencias)
        );

        $result = $this->obtenerRequest($servicio_id, 'informe-estadia', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_ingreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_ingreso']);
                $result['informe'][$i]['fecha_egreso'] = $this->fecha_a_local($result['informe'][$i]['fecha_egreso']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }

    public function informeEventos($servicio_id, $desde, $hasta, $referencias = array(), $eventos = array())
    {

        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias),
            'eventos' => $eventos,
        );
        //die('////<pre>' . nl2br(var_export($context, true)) . '</pre>////');
        $result = $this->obtenerRequest($servicio_id, 'eventos', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['historial']); $i++) {
                $result['historial'][$i]['fecha'] = $this->fecha_a_local($result['historial'][$i]['fecha']);
                $result['historial'][$i]['fecha_recepcion'] = $this->fecha_a_local($result['historial'][$i]['fecha_recepcion']);
            }
        }
        return isset($result['historial']) ? $result['historial'] : array();
    }

    public function informeChofer($servicio_id, $desde, $hasta, $referencias = array(), $ibuttons = array())
    {
        $helper = new ContextHelper();
        $context = array(
            'servicio_id' => $servicio_id,
            'ibuttons' => $ibuttons,
            'desde' => $this->fecha_a_UTC($desde),
            'hasta' => $this->fecha_a_UTC($hasta),
            'referencia' => $helper->castReferencias($referencias),
        );
        $result = $this->obtenerRequest($servicio_id, 'informe-chofer', $context);
        if ($result && isset($result['ok']) && $result['ok']) {
            for ($i = 0; $i < count($result['informe']); $i++) {
                $result['informe'][$i]['fecha_inicio'] = $this->fecha_a_local($result['informe'][$i]['inicio']['fecha']);
                $result['informe'][$i]['fecha_fin'] = $this->fecha_a_local($result['informe'][$i]['fin']['fecha']);
            }
        }
        return isset($result['informe']) ? $result['informe'] : array();
    }
}
