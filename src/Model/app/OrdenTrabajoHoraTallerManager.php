<?php

namespace App\Model\app;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Organizacion;
use App\Entity\OrdenTrabajoHoraTaller;

/**
 * Description of OrdenTrabajoHoraTallerManager
 *
 * @author nicolas
 */
class OrdenTrabajoHoraTallerManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:OrdenTrabajoHoraTaller')->byOrganizacion($organizacion, array('p.nombre' => 'ASC'));
        } else {
            $org = $this->em->getRepository('App:Organizacion')->find($organizacion);
            return $this->em->getRepository('App:OrdenTranajoHoraTaller')->byOrganizacion($organizacion->getId(), array('p.nombre' => 'ASC'));
        }
    }

    public function create()
    {
        $hora = new OrdenTrabajoHoraTaller();
        return $hora;
    }

    public function save(OrdenTrabajoHoraTaller $hora)
    {
        $this->em->persist($hora);
        $this->em->flush();
        return $hora;
    }
}
