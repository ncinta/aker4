<?php

/**
 * EventoManager
 *
 * @author Claudio
 */

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Evento as Evento;
use App\Entity\EventoItinerario;
use App\Entity\Servicio;
use App\Entity\Organizacion as Organizacion;
use App\Entity\EventoParametro;
use App\Entity\EventoNotificacion;
use App\Event\ServicioEvent;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\UsuarioManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EventoManager
{

    protected $em;
    protected $security;
    protected $evento;
    protected $grupoRefManager;
    protected $cliente;
    protected $container;
    protected $usuarioManager;
    protected $eventDispatcher;


    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        GrupoReferenciaManager $grupoRefManager,
        HttpClientInterface $cliente,
        ContainerInterface $container,
        UsuarioManager $usuario,
        EventDispatcherInterface $eventDispatcher,
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->grupoRefManager = $grupoRefManager;
        $this->cliente = $cliente;
        $this->container = $container;
        $this->usuarioManager = $usuario;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Evento')
            ->findBy(array('organizacion' => $organizacion->getId()));
    }

    public function findAllByItinerario($itinerario)
    {
        return $this->em->getRepository('App:EventoItinerario')
            ->findBy(array('itinerario' => $itinerario->getId()));
    }

    public function findById($id)
    {
        $this->evento = $this->em->getRepository('App:Evento')->findOneBy(array('id' => $id));
        return $this->evento;
    }

    public function findByTipo($tipoEvento)
    {
        return $this->em->getRepository('App:Evento')->findBy(array(
            'tipoEvento' => $tipoEvento
        ));
    }
    public function findByTipoOrganizacion($tipoEvento, $organizacion)
    {
        return $this->em->getRepository('App:Evento')->findBy(array(
            'tipoEvento' => $tipoEvento,
            'organizacion' => $organizacion
        ));
    }

    public function existInIt($itinerario, $codename)
    {
        return $this->em->getRepository('App:EventoItinerario')->existInIt($itinerario, $codename);
    }


    public function create($organizacion)
    {
        $this->evento = new Evento();
        $this->evento->setOrganizacion($organizacion);
        $this->evento->setActivo(false);    //siempre lo seteo a false hasta q se termine de configurar.
        $this->evento->setLunes(true);
        $this->evento->setMartes(true);
        $this->evento->setMiercoles(true);
        $this->evento->setJueves(true);
        $this->evento->setViernes(true);
        $this->evento->setSabado(true);
        $this->evento->setDomingo(true);
        $horaD = new \DateTime('', null);
        $this->evento->setHoraDesde($horaD->setTime(0, 0, 0));    //siempre lo seteo a activo
        $horaH = new \DateTime('', null);
        $this->evento->setHoraHasta($horaH->setTime(23, 59, 59));    //siempre lo seteo a activo
        return $this->evento;
    }

    public function setAll($datos)
    {
        if (isset($datos['nombre'])) {
            $this->evento->setNombre($datos['nombre']);
        }
        $this->evento->setActivo(isset($datos['activo']) && $datos['activo'] == '1');
        $this->evento->setLunes(isset($datos['lunes']) && $datos['lunes'] == '1');
        $this->evento->setMartes(isset($datos['martes']) && $datos['martes'] == '1');
        $this->evento->setMiercoles(isset($datos['miercoles']) && $datos['miercoles'] == '1');
        $this->evento->setJueves(isset($datos['jueves']) && $datos['jueves'] = '1');
        $this->evento->setViernes(isset($datos['viernes']) && $datos['viernes'] == '1');
        $this->evento->setSabado(isset($datos['sabado']) && $datos['sabado'] == '1');
        $this->evento->setDomingo(isset($datos['domingo']) && $datos['domingo'] == '1');

        $tipoEvento = $this->em->getRepository('App:TipoEvento')->find($datos['tipoEvento']);
        $this->evento->setTipoEvento($tipoEvento);
        $this->evento->setSonido(isset($datos['sonido']) ? $datos['sonido'] : '0');
        $this->evento->setRegistrar(isset($datos['registrar']) ? $datos['registrar'] : '0');
        $this->evento->setNotificacionWeb($datos['notificacionWeb']);

        $horaD = new \DateTime('', null);
        $f = explode(':', $datos['horaInicio']);
        if (isset($datos['horaInicio']))
            $this->evento->setHoraDesde($horaD->setTime(intval($f[0]), intval($f[1]), 0));

        $horaH = new \DateTime('', null);
        $f = explode(':', $datos['horaFin']);
        if (isset($datos['horaFin']))

            $this->evento->setHoraHasta($horaH->setTime(intval($f[0]), intval($f[1]), 59));

        return $this->evento;
    }

    public function setForItinerario($nombre, $tipoEv, $organizacion)
    {
        $evento = new Evento();
        $evento->setOrganizacion($organizacion);
        $evento->setNombre($nombre);
        $evento->setActivo(false); //arranca en false. cuando se pasa a auditoría o en curso se coloca en true
        $evento->setLunes(true);
        $evento->setMartes(true);
        $evento->setMiercoles(true);
        $evento->setJueves(true);
        $evento->setViernes(true);
        $evento->setSabado(true);
        $evento->setDomingo(true);

        $tipoEvento = $this->em->getRepository('App:TipoEvento')->findOneBy(array('codename' => $tipoEv));
        $evento->setTipoEvento($tipoEvento);
        $evento->setSonido(true);
        $evento->setRegistrar(true);
        $evento->setNotificacionWeb(2);
        $evento->setClase(3); //itinerario

        $horaD = new \DateTime('', null);
        $evento->setHoraDesde($horaD->setTime(0, 0, 0));

        $horaH = new \DateTime('', null);
        $evento->setHoraHasta($horaH->setTime(23, 59, 59));

        $this->em->persist($evento);
        $this->em->flush();

        return $evento;
    }

    public function setEventoItinerario($evento, $itinerario, $tipoEv)
    {
        $tipoEvento = $this->em->getRepository('App:TipoEvento')->findOneBy(array('codename' => $tipoEv));
        $eveItinerario = new EventoItinerario();
        $eveItinerario->setEvento($evento);
        $eveItinerario->setItinerario($itinerario);
        $eveItinerario->setTipoEvento($tipoEvento);

        $this->em->persist($eveItinerario);
        $this->em->flush();
        return $eveItinerario;
    }

    public function findParam($evento, $variable)
    {
        return $this->em->getRepository('App:EventoParametro')->findOneBy(
            array(
                'variable' => $variable->getId(), 'evento' => $evento->getId()
            )
        );
    }

    public function findParamById($id)
    {
        return $this->em->getRepository('App:EventoParametro')->findOneBy(array('id' => $id));
    }

    public function saveEvParam($evParam)
    {

        $this->em->persist($evParam);
        $this->em->flush();

        return $evParam;
    }

    public function addParam($evento, $key, $value)
    {
        $var = $this->em->getRepository('App:VariableEvento')->findOneBy(array('codename' => $key));
        $evp = new EventoParametro();

        $evp->setEvento($evento);
        $evp->setVariable($var);
        $evp->setValor($value);

        $this->evento = $evento;
        $this->evento->addParametro($evp);
        return $this->evento;
    }

    public function setParams($evento, $parametros)
    {
        $this->evento = $evento;
        $ahora = $this->evento->getParametros();  ///todos los parametros que tengo ahora.
        //debo borrar todos los que tengo....
        foreach ($ahora as $param) {
            $this->em->remove($param);
        }

        //debo agregar los seleccionados...
        $this->evento->setParametros($parametros);

        $this->em->persist($this->evento);
        $this->em->flush();
        return $this->evento;
    }

    /** Agrego los servicios al evento */
    public function addServicio($evento, $servicio)
    {
        $this->evento = $evento;
        $this->evento->addServicio($servicio);        
        return $this->evento;
    }

    public function setServicios($evento, $servicios)
    {
        $this->evento = $evento;
        $ahora = $this->evento->getServicios();  ///todos los servicios que tengo ahora.
        //debo borrar todos los que tengo....
        foreach ($ahora as $servicio) {
            $this->evento->removeServicio($servicio);
        }

        //debo agregar los seleccionados...
        $this->evento->setServicios($servicios);

        $this->em->persist($this->evento);
        $this->em->flush();
        return $this->evento;
    }

    public function addNotificacion($evento, $contacto, $tipo)
    {
        $this->evento = $evento != null ? $evento : $this->evento;
        if ($this->evento && !$this->findNotifByEvContacto($evento, $contacto)) { //si no existe la notificacion que la cree
            $notif = new EventoNotificacion();
            $notif->setEvento($evento);
            $notif->setContacto($contacto);
            $notif->setTipo($tipo);

            $this->evento->addNotificacion($notif);
        }
        return $this->evento;
    }

    public function findNotifByEvContacto($evento, $contacto)
    {
        if ($evento && $contacto) {
            return $this->em->getRepository('App:EventoNotificacion')
                ->findBy(array('evento' => $evento->getId(), 'contacto' => $contacto));
        }
        return null;
    }


    public function setNotificaciones($evento, $notificaciones)
    {
        $this->evento = $evento;
        $ahora = $this->evento->getNotificaciones();  ///todos las notif que tengo ahora.
        //debo borrar todos los que tengo....
        foreach ($ahora as $notif) {
            $this->em->remove($notif);
        }
        //debo agregar los seleccionados...
        $this->evento->setNotificaciones($notificaciones);
        $this->em->persist($this->evento);
        $this->em->flush();
        return $this->evento;
    }

    public function save($evento = null)
    {
        if ($evento != null) {   //el parametro no viene nulo, entonces usar este, sino usar local.
            $this->evento = $evento;
        }

        $this->em->persist($this->evento);
        $this->em->flush();

        return $this->evento;
    }

    public function delete($id)
    {
        $evento = $this->findById($id);
        if ($evento) {
            $this->em->remove($evento);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * Borra el evento utilizando sentencias sql para evitar duplicacion
     */
    public function remove($idEvento)
    {
        $this->em->createQuery('delete from App:EventoHistorial eh where eh.evento = ' . $idEvento)->execute();
        $this->em->createQuery('delete from App:EventoTemporal et where et.evento = ' . $idEvento)->execute();
        $this->em->createQuery('delete from App:Evento e where e.id = ' . $idEvento)->execute();
        return true;
    }

    public function inServicio($servicio, $evento)
    {
        $return = false;
        if (is_null($evento)) {
            return false;
        }

        $ev = $this->em->getRepository('App:Evento')->findByServicio($servicio, $evento);
        if (!is_null($ev)) {
            $return = true;   //existe el servicio en el evento
        }
        return $return;
    }

    public function saveServicioEv($servicio, $evento)
    {
        $servicio->addEvento($evento);
        $evento->addServicio($servicio);

        $this->em->persist($servicio);
        $this->em->persist($evento);
        $this->em->flush();

        return $servicio;
    }

    public function removeServicioEv($servicio, $evento)
    {
        $servicio->removeEvento($evento);
        $evento->removeServicio($servicio);
        $this->em->persist($servicio);
        $this->em->persist($evento);
        $this->em->flush();

        return $servicio;
    }

    public function saveServicioEvs(Servicio $servicio, $eventos)
    {
        $ids = array();
        foreach ($servicio->getEventos() as $ev) {
            $ids[] = $ev->getId();
        }
        foreach ($eventos as $evento) {
            if (!in_array($evento->getId(), $ids)) {
                $evento->addServicio($servicio);
                $servicio->addEvento($evento);
                $eventServicio = new ServicioEvent($servicio,$evento);
                $this->eventDispatcher->dispatch($eventServicio, ServicioEvent::EVENTO_ENABLED);
            }
        }
        $this->em->persist($evento);
        $this->em->persist($servicio);
        $this->em->flush();

        return $servicio;
    }

    public function removeServicioEvs(Servicio $servicio, $eventos)
    {
        foreach ($eventos as $evento) {
            $evento->removeServicio($servicio);
            $servicio->removeEvento($evento);
            $eventServicio = new ServicioEvent($servicio,$evento);
            $this->eventDispatcher->dispatch($eventServicio, ServicioEvent::EVENTO_DISABLED);
        }
        $this->em->persist($evento);
        $this->em->persist($servicio);
        $this->em->flush();

        return $servicio;
    }

    public function findAllByClientes($clientes)
    {
        $ids = array();
        foreach ($clientes as $cli) {
            $ids[] = $cli->getId();
        }
        $eventos = $this->em->getRepository('App:Evento')->obtenerAllClientes($ids);
        return $eventos;
    }

    public function setActivo($evento, $activo)
    {
        $evento->setActivo($activo);
        $this->em->persist($evento);
        $this->em->flush();
    }
    public function toArrayData($evento, $action)
    {
        $data = array(
            'entity' => 'EVENTO',
            'action' => null,
            'data' => null
        );
        if ($evento) {
            $data['data']['id'] = $evento instanceof Evento ? $evento->getId() : $evento * 1;
            $data['data']['updated_at'] =  $evento instanceof Evento ? $evento->getUpdatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['created_at'] =  $evento instanceof Evento ? $evento->getCreatedAt()->format('Y-m-d H:i:s') : null;

            //legacy
            $data['data']['createdAt'] = $evento instanceof Evento ? $evento->getCreatedAt()->format('Y-m-d H:i:s') : null;
            $data['data']['updatedAt'] = $evento instanceof Evento ? $evento->getUpdatedAt()->format('Y-m-d H:i:s') : null;

            $data['action'] = 'UPDATE';   //siempre arma con update pero puede cambiarlo en la otra linea.
            if ($action == 2) {    //la accion es borrar...
                $data['action'] = 'DELETE';
            }

            $data['data']['organizacion'] = array(
                'id' => $evento->getOrganizacion()->getId(),
                'nombre' => $evento->getOrganizacion()->getNombre(),
                'timeZone' => $evento->getOrganizacion()->getTimezone()
            );
            $data['data']['tipoEvento'] = array(
                'id' => $evento->getTipoEvento()->getId(),
                'nombre' => $evento->getTipoEvento()->getNombre(), 'codename' => $evento->getTipoEvento()->getCodename()
            );
            $data['data']['nombre'] = $evento->getNombre();
            $data['data']['activo'] = $evento->getActivo();
            $data['data']['lunes'] = $evento->getLunes();
            $data['data']['martes'] = $evento->getMartes();
            $data['data']['miercoles'] = $evento->getMiercoles();
            $data['data']['jueves'] = $evento->getJueves();
            $data['data']['viernes'] = $evento->getViernes();
            $data['data']['sabado'] = $evento->getSabado();
            $data['data']['domingo'] = $evento->getDomingo();
            $data['data']['horaDesde'] = $evento->getHoraDesde()->format('H:i:s');
            $data['data']['horaHasta'] = $evento->getHoraHasta()->format('H:i:s');
            $data['data']['registrar'] = $evento->getRegistrar();
            $data['data']['parametros'] = $this->getDataParam($evento->getParametros());
            $data['data']['servicios'] = $this->getDataServicios($evento->getServicios());
            $data['data']['notificacion'] = $this->getDataNotificacion($evento->getNotificaciones());
        }

        return $data;
    }

    function getDataParam($parametros)
    {
        $dataParam = array();
        $i = 0;
        if ($parametros) {
            foreach ($parametros as $parametro) {
                $datatype = $parametro->getVariable()->getDataType();
                $dataParam[$i] = array(
                    'id' => $parametro->getId(),
                    'nombre' => $parametro->getVariable()->getNombre(),
                    'codename' => $parametro->getVariable()->getCodename(),
                    'datatype' => $datatype,
                    'cotaMinima' => $parametro->getVariable()->getCotaMinima(),
                    'cotaMaxima' => $parametro->getVariable()->getCotaMaxima(),
                    'unidadMedida' => $parametro->getVariable()->getUnidadMedida(),
                    'valor' => $parametro->getValor()
                );
                if ($parametro->getVariable()->getCodename() === 'VAR_GRUPO_REFERENCIA_IN' || $parametro->getVariable()->getCodename() === 'VAR_GRUPO_REFERENCIA_OUT') {
                    $grupo = $this->grupoRefManager->find($parametro->getValor());
                    $dataParam[$i]['grupoReferencia'] = $this->grupoRefManager->toArrayData($grupo, 1);
                }
                $i++;
            }
        }
        return $dataParam;
    }

    function getDataServicios($servicios)
    {
        $dataServ = array();
        if ($servicios) {
            foreach ($servicios as $servicio) {
                $dataServ[] = array(
                    'id' => $servicio->getId(),
                    'nombre' => $servicio->getNombre(),
                    'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : null,
                    'ultFechahora' => $servicio->getUltFechahora() ? $servicio->getUltFechahora() : null,
                    'mdmid' => $servicio->getEquipo() ? $servicio->getEquipo()->getMdmid() : null
                );
            }
        }
        return $dataServ;
    }

    private function buscarContactoCanal($contacto, $codename)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App:Contacto', 'c')
            ->join('c.tipoNotificaciones', 't')
            ->where('t.codename = :codename')
            ->andWhere('t.id = :contacto')
            ->setParameter('codename', $codename)
            ->setParameter('contacto', $contacto->getId());
        if ($query->getQuery()->getOneOrNullResult()) {
            return true;
        } else {
            return false;
        }
    }

    function getDataNotificacion($notificaciones)
    {
        $dataNotif = array();
        if ($notificaciones) {
            foreach ($notificaciones as $notificacion) {
                $data = [];
                $data['id'] = $notificacion->getContacto()->getId();
                $data['notificacion_id'] = $notificacion->getId();
                $data['nombre'] = $notificacion->getContacto()->getNombre();
                $data['created_at'] = $notificacion->getCreatedAt() ? $notificacion->getCreatedAt()->format('Y-m-d H:i:s') : null;
                // die('////<pre>' . nl2br(var_export( $notificacion->getContacto()->getNombre() , true)) . '</pre>////');
                $data['updated_at'] = $notificacion->getUpdatedAt() ? $notificacion->getUpdatedAt()->format('Y-m-d H:i:s') : null;

                if ($notificacion->getContacto()) {
                    if ($notificacion->getTipo() == 1 && !is_null($notificacion->getContacto()->getCelular())) {   //push / celular
                        $usr = $this->usuarioManager->findTokenByPhone($notificacion->getContacto()->getCelular());
                        if ($usr && $usr->getToken() != null) {
                            $data['token'] = $usr->getToken();   ///agrego el token
                            $data['celular'] = $notificacion->getContacto()->getCelular();
                            $data['tipo'] = 1;
                        }
                    } elseif ($notificacion->getTipo() == 2 && !is_null($notificacion->getContacto()->getEmail())) { //email
                        $data['email'] = $notificacion->getContacto()->getEmail();
                        $data['tipo'] = 2;
                    } else {
                    }
                }
                $dataNotif[] = $data;
            }
        }
        //dd($dataNotif);
        return $dataNotif;
    }

    public function getStatusAgentes($evento)
    {
        $result = 'ERROR';
        if (is_null($evento)) {
            return array();
        }

        try {
            $response = $this->cliente->request(
                'GET',
                $this->container->getParameter('aker_api_eventos') . '/' . $evento->getId(),
                //$this->container->getParameter('aker_api_eventos') . '1209',
                array(
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                    'auth_basic' => array(
                        $this->container->getParameter('aker_aws_usr_config'),
                        $this->container->getParameter('aker_aws_pass_config'),
                    )
                )
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                // dd($response->getContent());
                $result = json_decode($response->getContent(), true);

                //return $result;
                //return $result['data']['servicios'];
            }
        } catch (TransportException $e) {
            return false;
        }

        if (isset($result['success'])) {
            return  isset($result['data']) ? $result['data'] : array();
        } else {
            return array();
        }
    }
}
