<?php

namespace App\Model\app;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Servicio as Servicio;

class SearchManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getServicios(array $datos)
    {
        $servicios = null;
        foreach ($datos as $value) {
            $servicios[] = $this->em->getRepository('App:Servicio')->find($value);
        }
        return $servicios;
    }

    public function ServicioByNombre($nombre)
    {
        if (!is_null($nombre)) {
            $rsm = new ResultSetMapping;
            $rsm->addEntityResult('App:Servicio', 's');
            $rsm->addFieldResult('s', 'id', 'id');
            $sql = "select id from servicio where servicio.estado < 9 and regexp_replace(lower(nombre), ' ', '', 'g') ~* regexp_replace(lower('" . $nombre . "'), ' ', '', 'g')";
            $result = $this->em->createNativeQuery($sql, $rsm)->getResult(2);
            $ids = null;
            foreach ($result as $value) {
                $ids[] = $value['id'];
            }
            return $ids;
        } else {
            return array();
        }
    }

    public function ServicioByPatente($nombre)
    {
        if (!is_null($nombre)) {
            $rsm = new ResultSetMapping;
            $rsm->addEntityResult('App:Servicio', 's');
            $rsm->addFieldResult('s', 'id', 'id');
            $sql = "select servicio.id as id from servicio inner join vehiculo on (servicio.vehiculo_id = vehiculo.id) where servicio.estado < 9 and regexp_replace(lower(patente), ' ', '', 'g') ~* regexp_replace(lower('" . $nombre . "'), ' ', '', 'g')";
            $result = $this->em->createNativeQuery($sql, $rsm)->getResult(2);
            $ids = null;
            foreach ($result as $value) {
                $ids[] = $value['id'];
            }
            return $ids;
        } else {
            return array();
        }
    }

    public function equipos($value)
    {
        if (!is_null($value)) {
            $query = $this->em->createQueryBuilder()
                ->select('e')
                ->from('App:Equipo', 'e')
                ->where('LOWER(e.mdmid) like ?1')
                ->orWhere('LOWER(e.imei) like ?1')
                ->orwhere('LOWER(e.numeroSerie) like ?1')
                ->setParameter(1, '%' . strtolower($value) . '%');
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    public function chips($value)
    {
        if (!is_null($value)) {
            $query = $this->em->createQueryBuilder()
                ->select('e')
                ->from('App:Chip', 'e')
                ->where('LOWER(e.numeroTelefono) like ?1')
                ->orWhere('LOWER(e.imei) like ?1')
                ->setParameter(1, '%' . strtolower($value) . '%');
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }
    public function referencias($value)
    {
        if (!is_null($value)) {
            $query = $this->em->createQueryBuilder()
                ->select('e')
                ->from('App:Referencia', 'e')
                ->where('LOWER(e.nombre) like ?1')
                ->setParameter(1, '%' . strtolower($value) . '%');
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }
    public function usuarios($value)
    {
        if (!is_null($value)) {
            $query = $this->em->createQueryBuilder()
                ->select('e')
                ->from('App:Usuario', 'e')
                ->where('LOWER(e.nombre) like ?1')
                ->orWhere('LOWER(e.username) like ?1')
                ->orWhere('LOWER(e.email) like ?1')
                ->orWhere('LOWER(e.telefono) like ?1')
                ->setParameter(1, '%' . strtolower($value) . '%');
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }
}
