<?php

namespace App\Model\fuel;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CargaCombustible as CargaCombustible;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraServicioManager;

/**
 * Description of CombustibleManager
 *
 * @author nicolas
 */
class VerificaCombustibleManager
{

    protected $em;
    protected $userlogin;
    protected $bitacora;

    public function __construct(EntityManagerInterface $em, BitacoraServicioManager $bitacora, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
    }

    public function save($combustible)
    {
        if (is_null($combustible->getFecha())) {
            return false;
        }
        if (is_null($combustible->getLitrosCarga()) || $combustible->getLitrosCarga() == 'NaN') {
            return false;
        }

        if ($combustible->getMontoTotal() == 'NaN') {
            return false;
        }
        $this->em->persist($combustible);
        $this->em->flush();
        $this->bitacora->combustibleCreateManual($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
        return true;
    }

    public function add($combustible)
    {
        $this->em->persist($combustible);
        $this->em->flush();
        return true;
    }

    public function update($combustible)
    {
        $this->em->persist($combustible);
        $this->em->flush();
        $this->bitacora->combustibleUpdate($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
    }

    public function delete($combustible)
    {
        $this->em->remove($combustible);
        $this->em->flush();
        if ($combustible->getCargaCombustible()) {
            $this->bitacora->combustibleDelete($this->userlogin->getUser(), $combustible->getCargaCombustible()->getServicio(), $combustible->getCargaCombustible());
        }
    }

    public function deleteById($id)
    {
        $combustible = $this->findById($id);
        $this->delete($combustible);
        return;
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:CheckCargaCombustible')->findOneBy(array('id' => $id));
    }

    public function findByOrganizacion($organizacion, $estado = null)
    {
        return $this->em->getRepository('App:CheckCargaCombustible')->findAllCargas($organizacion, $estado);
    }

    public function findByServicioYfecha($servicio, $desde, $hasta, $estacion = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByFecha($servicio, $desde, $hasta, $estacion);
    }

    public function findAllCargas($servicio = null, $desde = null, $hasta = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findAllCargas($servicio, $desde, $hasta);
    }

    public function findCarga($id)
    {
        return $this->em->getRepository('App:CargaCombustible')->findCarga($id);
    }

    public function findByEstacionYfecha($desde, $hasta, $ids, $idOrg, $tipoCombustible = null, $estacion = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByEstacionFecha($desde, $hasta, $ids, $idOrg, $tipoCombustible, $estacion);
    }

    public function findByPuntoYfecha($desde, $hasta, $idOrg, $tipoCombustible = null, $punto = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByPuntoFecha($desde, $hasta, $idOrg, $tipoCombustible, $punto);
    }

    public function findTipoCombustible($id)
    {
        return $this->em->getRepository('App:TipoCombustible')->findOneBy(array('id' => $id));
    }
}
