<?php

namespace App\Model\fuel;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CargaCombustible as CargaCombustible;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraServicioManager;
use DateInterval;
use DateTime;

/**
 * Description of CombustibleManager
 *
 * @author nicolas
 */
class CombustibleManager
{

    protected $em;
    protected $userlogin;
    protected $bitacora;

    public function __construct(EntityManagerInterface $em, BitacoraServicioManager $bitacora, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->bitacora = $bitacora;
        $this->userlogin = $userlogin;
    }

    public function save($combustible)
    {
        if (is_null($combustible->getFecha())) {
            return false;
        }
        if (is_null($combustible->getLitrosCarga()) || $combustible->getLitrosCarga() == 'NaN') {
            return false;
        }

        if ($combustible->getMontoTotal() == 'NaN') {
            return false;
        }
        $this->em->persist($combustible);
        $this->em->flush();
        $this->bitacora->combustibleCreateManual($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
        return true;
    }

    public function add($combustible)
    {
        $this->em->persist($combustible);
        $this->em->flush();
        return true;
    }

    public function update($combustible)
    {
        $this->em->persist($combustible);
        $this->em->flush();
        //descomentar bitacora cuando se actualice la base de datos. 27/09/2022 
        //   $this->bitacora->combustibleUpdate($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
    }

    public function delete($combustible)
    {
        $this->em->remove($combustible);
        $this->em->flush();
        $this->bitacora->combustibleDelete($this->userlogin->getUser(), $combustible->getServicio(), $combustible);
    }

    public function findByServicioYfecha($servicio, $desde, $hasta, $ingreso = null, $estacion = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByFecha($servicio, $desde, $hasta, $ingreso, $estacion);
    }

    public function findAllCargas($servicio = null, $desde = null, $hasta = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findAllCargas($servicio, $desde, $hasta);
    }

    public function findCarga($id)
    {
        return $this->em->getRepository('App:CargaCombustible')->findCarga($id);
    }

    public function findByEstacionYfecha($desde, $hasta, $ids, $idOrg, $tipoCombustible = null, $estacion = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByEstacionFecha($desde, $hasta, $ids, $idOrg, $tipoCombustible, $estacion);
    }

    public function findByPuntoYfecha($desde, $hasta, $idOrg, $tipoCombustible = null, $punto = null)
    {
        return $this->em->getRepository('App:CargaCombustible')->findByPuntoFecha($desde, $hasta, $idOrg, $tipoCombustible, $punto);
    }

    public function findTipoCombustible($id)
    {
        return $this->em->getRepository('App:TipoCombustible')->findOneBy(array('id' => $id));
    }

    public function findTipoCombustibleByNombre($nombre)
    {
        return $this->em->getRepository('App:TipoCombustible')->findOneBy(array('nombre' => $nombre));
    }

    public function findByServicioPuntoCarga($desde, $hasta, $organizacion, $servicios = null, $puntoscarga = null)
    {
        // die('////<pre>' . nl2br(var_export($servicios, true)) . '</pre>////');
        return $this->em->getRepository('App:CargaCombustible')->findByServicioPuntoCarga($desde, $hasta, $organizacion, $servicios, $puntoscarga);
    }

    public function generarDashboard($servicios)
    {
        $data = array();
        foreach ($servicios as $servicio) {
            $ultCarga = $this->em->getRepository('App:CargaCombustible')->findUltimasCargas($servicio, 1, false);
            $now = new DateTime();
            $hasta = $now->format('Y-m-d H:i:s');
            $desde = $now->sub(new DateInterval('P30D'));
            //  die('////<pre>' . nl2br(var_export($desde, true)) . '</pre>////');

            $cargas =  $this->em->getRepository('App:CargaCombustible')->findAllCargas($servicio, $desde, $hasta);
            $ultMes = null;
            if (count($cargas) > 0) {
                $litros = $monto = 0;
                foreach ($cargas as $carga) {
                    $litros += $carga->getLitroscarga();
                    $monto += $carga->getMontoTotal();
                }
                $ultMes = [
                    'cantidad' => count($cargas),
                    'litros' => $litros,
                    'importe' => $monto,
                ];
            }

            $data[$servicio->getId()] = [
                'ultCarga' => !is_null($ultCarga) ? $ultCarga : null,
                'ultMes' => !is_null($ultMes) ? $ultMes : null,
            ];
        }
        //dd($data);
        return $data;
    }

    public function obtenerCentroCosto(CargaCombustible $carga)
    {
        return $this->em->getRepository('App:Prestacion')->obtenerCentroCostoPorServicioEnFecha($carga->getServicio(), $carga->getFecha());
    }
}
