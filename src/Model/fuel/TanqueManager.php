<?php

/**
 * Description of ServicioManager
 *
 * @author Claudio
 */

namespace App\Model\fuel;

use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UtilsManager;

class TanqueManager
{

    protected $em;
    protected $utils;
    private $servicio;
    private $min = null;
    private $max = null;
    private $exacto = null;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function loadMediciones($servicio, $porcentaje = null)
    {
        $this->servicio = $servicio;

        if (!is_null($servicio->getVehiculo()) && $servicio->getVehiculo()->getLitrosTanque()) {  //uso el dato de capacidad de tanque para gestionar los litros y no el aforo
            return true;
        } else {
            if (!is_null($porcentaje)) {
                $this->exacto = $this->em->getRepository('App:ServicioTanque')->getExacto($servicio, $porcentaje);
            }
            if (is_null($this->exacto)) {
                $mediciones = $this->em->getRepository('App:ServicioTanque')->findMediciones($servicio);
                if (count($mediciones) == 0) {
                    return false;
                } else {
                    //tengo mediciones asi que hay que hacer las cargas.
                    $this->min = $this->em->getRepository('App:ServicioTanque')->getMinimo($servicio);
                    $this->max = $this->em->getRepository('App:ServicioTanque')->getMaximo($servicio);

                    return true;
                }
            } else {
                return true;
            }
        }
    }

    /**
     * Para un servicio y mediciones cargadas analiza el porcentaje y devuleve una 
     * estructura que determina la cantidad de litros exacta que posee el tanque.
     * @param  $porcentaje
     * @return array 
     */
    public function getLitros($porcentaje)
    {
        //die('////<pre>' . nl2br(var_export($porcentaje, true)) . '</pre>////');
        $data = null;
        if ($this->servicio->getVehiculo()->getLitrosTanque()) { //uso el dato del tanque con el % para generar litos
            $capacidad = $this->servicio->getVehiculo()->getLitrosTanque();
            $litros = ($porcentaje * $capacidad) / 100;
            $data = array('litros' => number_format($litros, 2), 'modo' => 0);
            //die('////<pre>' . nl2br(var_export($litros, true)) . '</pre>////');
        } else {
            if (is_null($this->exacto)) {   //no se tiene el porcentaje exacto, anular.
                $data = null;
                $exacto = $this->em->getRepository('App:ServicioTanque')->getExacto($this->servicio, $porcentaje);
                if (!is_null($exacto)) {
                    $data = array('litros' => number_format($exacto->getLitros(), 2), 'modo' => 0);
                } elseif ($porcentaje < $this->min->getPorcentaje()) {
                    $data = array('litros' => number_format($this->min->getLitros(), 2), 'modo' => -1);
                } elseif ($porcentaje > $this->max->getPorcentaje()) {
                    $data = array('litros' => number_format($this->max->getLitros(), 2), 'modo' => 1);
                } else {  //aca debo de hacer los calculos para obtencion de litros.
                    $menor = $this->em->getRepository('App:ServicioTanque')->getMenor($this->servicio, $porcentaje);
                    $mayor = $this->em->getRepository('App:ServicioTanque')->getMayor($this->servicio, $porcentaje);
                    if ($menor && $mayor) {
                        $difP = $mayor->getPorcentaje() - $menor->getPorcentaje();
                        $difLts = $mayor->getLitros() - $menor->getLitros();
                        if ($difP < 1) {
                            $ltsReal = (($porcentaje * $difLts) * $difP) / 100 + $menor->getLitros();
                        } else {
                            $difPMin = $porcentaje - $menor->getPorcentaje();
                            $ltsReal = ($difLts / $difP) * $difPMin + $menor->getLitros();
                        }
                        $data = array('litros' => number_format($ltsReal, 2), 'modo' => 0);
                    } else {
                        $data = array('litros' => number_format($this->min->getLitros(), 2), 'modo' => -1);
                    }
                    // die('////<pre>' . nl2br(var_export($ltsReal, true)) . '</pre>////');
                }
            } else {  //se tiene el % exacto, devolver esa cantidad de litros.
                $data = array('litros' => number_format($this->exacto->getLitros(), 2), 'modo' => 0);
            }
        }
        return $data;
    }

    /**
     * Para un servicio y mediciones cargadas analiza la cantidad de litros y devuleve una 
     * estructura que determina el porcentaje exacto que posee el tanque.
     * @param  $litros
     * @return array 
     */
    public function getPorcentaje($litros, $servicio)
    {
        $porcentaje = null;
        $capacidad = $servicio->getVehiculo()->getLitrosTanque();
        if (!is_null($capacidad)) {
            $porcentaje = ($litros / $capacidad) * 100;
        }
        // die('////<pre>' . nl2br(var_export($porcentaje, true)) . '</pre>////');
        return $porcentaje;
    }

    /*
     * Para una determinada medición y determinado  tipo de medidor 
     * se devuelve la misma en porcentaje y cantidad de litros
     */

    public function getCapacidad($medicion, $servicio)
    {
        $capacidad = array();
        $this->servicio = $servicio;
        if ($servicio->getTipoMedidorCombustible() == $servicio::TIPO_MEDIDOR_COMBUSTIBLE_PORCENTAJE) { //tiene medicion por porcentaje
            $litros = $this->getLitros($medicion);  //tengo el % -> lts
            $capacidad = array(
                'porcentaje' => $medicion,
                'litros' => $litros['litros'],
                'modo' => $litros['modo'],
                'title' => sprintf('%d%% %slts', $medicion, $litros['litros'])
            );
            //die('////<pre>' . nl2br(var_export($capacidad, true)) . '</pre>////');
        } elseif ($servicio->getTipoMedidorCombustible() == $servicio::TIPO_MEDIDOR_COMBUSTIBLE_LITROS) { //tiene medicion por litros
            $porcentaje = $this->getPorcentaje($medicion, $servicio);
            $capacidad = array(
                'porcentaje' => $porcentaje,
                'litros' => $medicion,
                'modo' => 0,
                'title' => sprintf('%d%% %s lts', $porcentaje, $medicion)
            );
            //   die('////<pre>' . nl2br(var_export($capacidad, true)) . '</pre>////');
        } elseif ($servicio->getTipoMedidorCombustible() == 3) { // por ambos
            $capacidad = array(
                'porcentaje' => 'n/d',
                'litros' => 'n/d',
                'modo' => 3,
                'title' => 'sin datos'
            );
        } else {
            $capacidad = array(
                'porcentaje' => 'n/d',
                'litros' => 'n/d',
                'modo' => 0,
                'title' => 'sin datos'
            );
        }

        return $capacidad;
    }

    public function getTanque($servicio, $ultTrama)
    {
        $tanque = null;
        if ($servicio->getMedidorCombustible()) {
            $tanque = array('porcentaje' => -1);  //onicializo con algo porque puede fallar despue (icono gris)
            if (isset($ultTrama['canbusData']) && !is_null($ultTrama['canbusData'])) {   //tengo datos del canbus
                //if (isset($ultTrama['contacto']) && $ultTrama['contacto']) {   //esta en contacto sino no tengo datos                
                $data = $ultTrama['canbusData'];
                if (isset($data['nivelCombustible'])) {
                    //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                    if ($this->loadMediciones($servicio, $data['nivelCombustible'])) {   //cargo mediciones si es necesario.
                        $medicion = $this->getCapacidad($data['nivelCombustible'], $servicio);    //obtengo la capacidad lts o %
                    }
                }
                //die('////<pre>' . nl2br(var_export($medicion, true)) . '</pre>////');
                //}
            }


            //**********************************************
            if (isset($medicion)) {
                if ($medicion['modo'] == -1) {
                    $tanque['litros'] = '<' . $medicion['litros'];
                } elseif ($medicion['modo'] == 1) {
                    $tanque['litros'] = '>' . $medicion['litros'];
                } else {
                    $tanque['litros'] = $medicion['litros'];
                }

                if (isset($medicion['porcentaje'])) {
                    $tanque['porcentaje'] = $medicion['porcentaje'];
                }
                if (isset($medicion['title'])) {
                    $tanque['title'] = $medicion['title'];
                }
            }
        }
        //die('////<pre>' . nl2br(var_export($tanque, true)) . '</pre>////');
        return $tanque;
    }
}
