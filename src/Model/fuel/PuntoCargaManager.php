<?php

namespace App\Model\fuel;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\PuntoCarga as PuntoCarga;
use App\Entity\PrecioTipoCombustible as PrecioTipoCombustible;
use App\Entity\Organizacion as Organizacion;

/**
 * Description of PuntoCargaManager
 *
 * @author nicolas
 */
class PuntoCargaManager
{

    protected $em;
    protected $organizacion;
    protected $puntocarga;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:PuntoCarga')
            ->findBy(array('organizacion' => $organizacion->getId()));
    }

    public function create(Organizacion $propietario)
    {
        $this->puntocarga = new PuntoCarga();
        $this->puntocarga->setOrganizacion($propietario);
        return $this->puntocarga;
    }

    public function save($puntocarga)
    {
        if (!is_null($puntocarga)) {
            $this->puntocarga = $puntocarga;
            $this->em->persist($this->puntocarga);
            $this->em->flush();
            return $this->puntocarga;
        }
        return null;
    }

    public function generarDashboard($puntoscarga)
    {
        $data = array();
        foreach ($puntoscarga as $puntoCarga) {
            $ultCarga = $this->em->getRepository('App:CargaCombustible')->findUltimasCargasByPuntoCarga($puntoCarga, 1, false);
            $now = new \DateTime();
            $hasta = $now->format('Y-m-d H:i:s');
            $desde = $now->sub(new \DateInterval('P30D'));
            $desde = $desde->format('Y-m-d H:i:s');

            $cargas =  $this->em->getRepository('App:CargaCombustible')->findByPuntoFecha($desde, $hasta, null, null, $puntoCarga);
            $ultMes = null;
            if (count($cargas) > 0) {
                $litros = $monto = 0;
                foreach ($cargas as $carga) {
                    $litros += $carga->getLitroscarga();
                    $monto += $carga->getMontoTotal();
                }
                $ultMes = [
                    'cantidad' => count($cargas),
                    'litros' => $litros,
                    'importe' => $monto,
                ];
            }

            $data[$puntoCarga->getId()] = [
                'ultCarga' => !is_null($ultCarga) ? $ultCarga : null,
                'ultMes' => !is_null($ultMes) ? $ultMes : null,
            ];
        }
        //dd($data);
        return $data;
    }

    public function findById($id)
    {
        return $this->em->getRepository('App:PuntoCarga')->findOneBy(array('id' => $id));
    }

    public function findByNombre($nombre)
    {
        return $this->em->getRepository('App:PuntoCarga')->findOneBy(array('nombre' => $nombre));
    }

    public function findByCodigo($codigo)
    {
        return $this->em->getRepository('App:PuntoCarga')->findOneBy(array('codigo' => $codigo));
    }

    public function getTipoCombustibles()
    {
        return $this->em->getRepository('App:TipoCombustible')->findAll();
    }

    public function findTipoCombustible($id)
    {
        return $this->em->getRepository('App:TipoCombustible')->findOneBy(array('id' => $id));
    }

    public function findPrecioTipoCombustible($id)
    {
        return $this->em->getRepository('App:PrecioTipoCombustible')->findOneBy(array('id' => $id));
    }

    public function findPrecio($ptoCarga_id, $combustible_id)
    {
        $precioTComb = $this->em->getRepository('App:PrecioTipoCombustible')->findOneBy(
            array(
                'puntoCarga' => $ptoCarga_id,
                'tipoCombustible' => $combustible_id
            )
        );
        if ($precioTComb) {
            return $precioTComb->getPrecio();
        }
        return 0;
    }

    public function deletePrecioTipoCombustible(PrecioTipoCombustible $comb)
    {
        $this->em->remove($comb);
        $this->em->flush();
        return true;
    }

    public function addCombustible(PuntoCarga $puntoCarga, $precio, $tipo)
    {
        $combustible = new \App\Entity\PrecioTipoCombustible();
        $combustible->setPuntoCarga($puntoCarga);
        $combustible->setPrecio($precio);
        $combustible->setTipoCombustible($tipo);
        $this->em->persist($combustible);
        $this->em->flush();
        return $combustible;
    }

    public function saveComb(PrecioTipoCombustible $comb)
    {
        if (!is_null($comb)) {
            $this->em->persist($comb);
            $this->em->flush();
            return $comb;
        }
        return null;
    }

    public function delete($id)
    {
        $this->em->remove($this->findById($id));
        $this->em->flush();
        return true;
    }
}
