<?php

namespace App\Model\fuel;

use App\Model\app\Router\BasicRouter;

class TanqueRouter extends BasicRouter
{

    public function btnEdit($servicio)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR')) {
            return $this->armarArray('Configurar', 'fa fa-plus', $this->router->generate('actividad_new', array('id' => $servicio->getId())));
        } else {
            return null;
        }
    }

    public function btnCopyFrom($servicio, $mediciones)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR') && count($mediciones) == 0) {
            return $this->armarArray('Copiar desde...', 'fa fa-import', $this->router->generate('sauron_tanque_copyfrom', array('id' => $servicio->getId())));
        } else {
            return null;
        }
    }

    public function btnCopyTo($servicio, $mediciones)
    {
        if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR') && count($mediciones) > 0) {
            return $this->armarArray('Copiar a...', 'fa fa-export', $this->router->generate('sauron_tanque_copyto', array('id' => $servicio->getId())));
        } else {
            return null;
        }
    }
}
