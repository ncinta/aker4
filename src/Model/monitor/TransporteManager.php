<?php

namespace App\Model\monitor;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Transporte;
use App\Entity\Organizacion as Organizacion;

class TransporteManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($transporte)
    {
        if ($transporte instanceof Transporte) {
            return $this->em->getRepository('App:Transporte')->findBy(array('id' => $transporte->getId()));
        } else {
            return $this->em->getRepository('App:Transporte')->find(array('id' => $transporte));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Transporte')->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'asc'));
        } else {
            return $this->em->getRepository('App:Transporte')->findBy(array('organizacion' => $organizacion), array('nombre' => 'asc'));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $transporte = new Transporte();
        $transporte->setOrganizacion($organizacion);

        return $transporte;
    }

    public function save(Transporte $transporte)
    {
        $this->em->persist($transporte);
        $this->em->flush();
        return $transporte;
    }

    public function delete(Transporte $transporte)
    {
        $this->em->remove($transporte);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $transporte = $this->find($id);
        if (!$transporte) {
            throw $this->createNotFoundException('Código de transporte no encontrado.');
        }
        return $this->delete($transporte);
    }

    public function findByIds($ids)
    {
        $transportes = array();
        foreach ($ids as $id) {
            $transportes[] = $this->find($id);
        }
        return $transportes;
    }
}
