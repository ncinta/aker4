<?php

namespace App\Model\monitor;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Itinerario;
use App\Entity\EventoItinerario as EventoItinerario;
use App\Entity\BitacoraItinerario as BitacoraItinerario;
use App\Entity\HistoricoItinerario as HistoricoItinerario;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Usuario as Usuario;
use App\Entity\Contacto as Contacto;
use App\Entity\Poi;
use App\Model\app\ServicioManager;
use App\Model\app\UserLoginManager;

class EventoItinerarioManager
{

    protected $em;
    protected $userlogin;

    public function __construct(
        EntityManagerInterface $em,
        UserLoginManager $userlogin
    ) {
        $this->em = $em;
        $this->userlogin = $userlogin;
    }
    public function create()
    {
    }

    public function findByEventoServicio($evento)
    {
        return $this->em->getRepository('App:EventoItinerario')->findOneBy(array('evento' => $evento));
    }

    /** esta funcion graba en el HistoricoItinerario una nueva entrada y lo asocia a eventoItinerario */
    public function add($fecha, $evento, $servicio, $titulo, $data = null)
    {
        $evIt = $this->em->getRepository('App:EventoItinerario')->findOneBy(array('evento' => $evento));
        if (!is_null($evIt)) { // hay itinerarios con este evento
            $itinerario = $evIt->getItinerario();
            if ($itinerario->getEstado() < 9) { //está abierto el itinerario
                $historial = new HistoricoItinerario();
                $historial->setTitulo($titulo);
                $historial->setFecha($fecha);
                $historial->setEventoItinerario($evIt);
                $historial->setServicio($servicio);
                $historial->setEvento($evento);

                //sino hago notificaciones entonces lo pongo como visto.
                if ($evento->getNotificacionWeb() == 0 || is_null($evento->getNotificacionWeb())) {
                    $historial->setFechaVista(new \DateTime('', new \DateTimeZone('UTC')));
                }

                // dd($data);
                if (!is_null($data)) {   //busco mas info dentro de la data....
                    $historial->setData($data);
                    if (isset($data['latitud'])) {
                        $historial->setLatitud($data['latitud']);
                    }
                    if (isset($data['longitud'])) {
                        $historial->setLongitud($data['longitud']);
                    }
                }
                //$historial = $this->createObjHistorial($historial, $evento, $servicio, $titulo, $fecha, $data);

                $this->em->persist($historial);
                $this->em->flush();
            }
        }

        return $historial;
    }



    public function setupVisto($eventosHistorial, $usuario, $historico)
    {
        if (count($eventosHistorial) > 0) {
            foreach ($eventosHistorial as $evento) {
                if ($evento->getFecha() == $historico->getFecha()) { // si tienen la misma fecha, estamos hablando del mismo evento
                    // die('////<pre>' . nl2br(var_export($evento->getId(), true)) . '</pre>////');            
                    $evento->setFechaVista(new \DateTime());
                    $evento->setEjecutor($usuario);
                    $this->em->persist($evento);
                }
            }
            $this->em->flush();
        } else {
            return false;
        }
    }

    /**
     * Recibe un evento historial itinerario y graba la respuesta
     */
    public function registrar($evItHistorial, $respuesta)
    {
        $evItHistorial->setEjecutor($this->userlogin->getUser());
        $evItHistorial->setRespuesta($respuesta);
        $evItHistorial->setFechaVista(new \DateTime());
        $this->em->persist($evItHistorial);

        $this->em->flush();

        return true;
    }
}
