<?php

namespace App\Model\monitor;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contacto as Contacto;
use App\Entity\Equipo as Equipo;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Entity\Evento as Evento;
use App\Model\app\ContactoManager;
use App\Model\app\EquipoManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\EventoManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\Cache\Adapter\RedisAdapter;

/**
 * Description of NotificadorTeknalManager
 *
 * @author Claudio
 */
class NotificadorTeknalManager
{

    protected $equipoManager;
    protected $servicioManager;
    protected $referenciaManager;
    protected $grupoReferenciaManager;
    protected $eventoManager;
    protected $contactoManager;
    protected $security;
    protected $em;
    protected $cliente;
    protected $container;
    protected $cacheTeknal;

    const TOKEN_TEKNAL = 'b132941d6f759e69cbfb6c2ecffcd10e';

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        HttpClientInterface $cliente,
        EquipoManager $equipoManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoManager $eventoManager,
        ContactoManager $contactoManager,
        ContainerInterface $container,        
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->equipoManager = $equipoManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoManager = $eventoManager;
        $this->contactoManager = $contactoManager;
        $this->cliente = $cliente;
        $this->container = $container;                
    }

    public function obtenerToken()
    {
        return self::TOKEN_TEKNAL;
    }

    public function sendData($json)
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_teknal_endpoint'),
                [
                    'body' => $json,
                    'timeout' => 10,  //10seg de espera                    
                    'headers' => [
                        'Access-Control-Allow-Origin' => '*',
                        'Content-Type' => 'application/json',
                    ]
                ]
            );
            $statusCode = $response->getStatusCode();
            
            if ($statusCode === 200 || $statusCode === 201) {
                $result = $response->getContent();
                return $result;
            } else {
                return 'Error: ' . $statusCode . ' - ' . $response->getContent(false);
            }
        } catch (TransportException $e) {
            return 'Error de conexión: ' . $e->getMessage();
        }

        return $statusCode === 200 || $statusCode === 201 ? true : false;
    }
   
}
