<?php

namespace App\Model\monitor;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Itinerario;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Usuario as Usuario;
use App\Entity\Contacto as Contacto;
use App\Entity\ItinerarioServicio;
use App\Entity\Poi;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\ServicioManager;
use App\Model\app\EventoManager;
use App\Model\app\UtilsManager;

class ItinerarioManager
{

    protected $em;
    protected $servicioManager;
    protected $notificadorAgenteManager;
    protected $eventoManager;
    protected $utilsManager;
    protected $arrCheck = array();

    public function __construct(
        EntityManagerInterface $em,
        ServicioManager $servicioManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        EventoManager $eventoManager,
        UtilsManager $utilsManager
    ) {
        $this->em = $em;
        $this->servicioManager = $servicioManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->eventoManager = $eventoManager;
        $this->utilsManager = $utilsManager;
    }

    public function find($itinerario)
    {
        if ($itinerario instanceof Itinerario) {
            return $this->em->getRepository('App:Itinerario')->findBy(array('id' => $itinerario->getId()));
        } else {
            return $this->em->getRepository('App:Itinerario')->find(array('id' => $itinerario));
        }
    }

    public function findByCodigo($codigo)
    {
        return $this->em->getRepository('App:Itinerario')->findOneBy(array('codigo' => $codigo));
    }

    public function findAllByOrganizacion($organizacion)
    {
        $query = $this->em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->leftJoin('i.servicios', 's')
            ->join('i.organizacion', 'o')
            ->where('o.id = :org')->setParameter('org', $organizacion->getId())
            ->andWhere('i.estado < 9')
            ->orderBy('i.created_at', 'DESC')
            ->addOrderBy('i.id', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function findItiServ($itinerario, $servicio)
    {
        $query = $this->em->createQueryBuilder()
            ->select('j')
            ->from('App:ItinerarioServicio', 'j')
            ->join('j.servicio', 's')
            ->join('j.itinerario', 'i')
            ->where('s.id = :servicio')->setParameter('servicio', $servicio->getId())
            ->andWhere('i.id = :itinerario')->setParameter('itinerario', $itinerario->getId());

        return $query->getQuery()->getOneOrNullResult();
    }

    public function create(Organizacion $organizacion, Usuario $userlogin = null)
    {
        $itinerario = new Itinerario();
        $itinerario->setOrganizacion($organizacion);
        $itinerario->setEstado($itinerario::ESTADO_NUEVO);
        if (!is_null($userlogin)) {
            if (!is_null($userlogin->getEmpresa())) {
                $itinerario->setEmpresa($userlogin->getEmpresa());
            }
        }
        return $itinerario;
    }

    /** 
     * procedimiento para cerrar el itinerario.
     */
    public function cerrar(Itinerario $itinerario, $fi, $ff, $nota)
    {
        $itinerario->setEstado($itinerario::ESTADO_CERRADO);
        if ($fi != null) {

            $itinerario->setFechaInicio($fi);
        }
        if ($ff != null) {
            $itinerario->setFechaFin($ff);
        }
        $itinerario->setNota($nota);

        $it = $this->save($itinerario);

        // borro los eventos asociados
        $toDelete = $this->deleteEvento($itinerario);

        //borro los grupos de referencias asociados al itinerario
        $delGrupoRef = $this->delGrupoRef($itinerario);
        return $it;
    }

    public function deleteEvento($itinerario)
    {
        //recorro todos los eventos del itineario para eliminarlos
        $eventosIt = $itinerario->getEventoItinerario();
        $toDelete = array();
        foreach ($eventosIt as $evIt) {
            //$evento = $evIt->getEvento();
            //notifico, si esta notificado ok entonces borro
            if ($this->notificadorAgenteManager->notificar($evIt->getEvento(), 2)) {
                $toDelete[] = $evIt->getEvento()->getId();   //guardo el id para borrarlo luego.

                foreach ($evIt->getHistoricoItinerario() as $historico) {
                    $historico->setEvento(null);
                    $this->em->persist($historico);
                }
                //saco el evento de evIt                
                $evIt->setEvento(null);
                $this->em->persist($evIt);
                $this->em->flush();
            }
        }

        foreach ($toDelete as $idEvento) {
            $this->eventoManager->remove($idEvento);
        }

        return $toDelete;
    }

    public function delGrupoRef($itinerario)
    {
        if ($itinerario->getGrupoReferencia()) {
            $grupo = $itinerario->getGrupoReferencia();
            if ($this->notificadorAgenteManager->notificar($grupo, 2)) {
                $itinerario->removeGrupoReferencia();
                $this->em->persist($itinerario);
                $this->em->flush();

                $this->em->remove($grupo);
                $this->em->flush();
            }
        }
        return true;
    }

    public function delUsuario(Itinerario $itinerario, Usuario $usuario)
    {
        $itinerario->removeUsuario($usuario);
        $it = $this->save($itinerario);
        return $it;
    }

    public function delContacto(Itinerario $itinerario,  $contacto)
    {
        $itinerario->removeContacto($contacto);
        $it = $this->save($itinerario);
        return $it;
    }

    public function save(Itinerario $itinerario)
    {
        $this->em->persist($itinerario);
        $this->em->flush();
        return $itinerario;
    }

    public function saveItiServ(ItinerarioServicio $itServ)
    {
        $this->em->persist($itServ);
        $this->em->flush();
        return $itServ;
    }

    public function delete(Itinerario $itinerario, $nota)
    {
        $itinerario->setEstado($itinerario::ESTADO_ELIMINADO);
        $itinerario->setNota($nota);
        $it = $this->save($itinerario);
        $delEv = $this->deleteEvento($itinerario);
        $delGrupoRef = $this->delGrupoRef($itinerario);
        return $it;
    }

    public function deleteById($id)
    {
        $itinerario = $this->find($id);
        if (!$itinerario) {
            throw $this->createNotFoundException('Código de itinerario no encontrado.');
        }
        return $this->delete($itinerario, '');
    }

    public function existUsuarioInItinerario($itinerario, $usuario)
    {
        $query = $this->em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.usuarios', 'u')
            ->where('i.id = ?1')
            ->andWhere('u.id = ?2')
            ->setParameter('1', $itinerario->getId())
            ->setParameter('2', $usuario->getId());
        return $query->getQuery()->getResult();
    }

    public function existContactoInItinerario($itinerario, $contacto)
    {
        $query = $this->em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.contactos', 'u')
            ->where('i.id = ?1')
            ->andWhere('u.id = ?2')
            ->setParameter('1', $itinerario->getId())
            ->setParameter('2', $contacto->getId());
        return $query->getQuery()->getResult();
    }

    public function addServicio($itinerario, $servicio)
    {
        $s = $this->findItiServ($itinerario, $servicio); //verifico que ese servicio no este en el itinerario
        //die('////<pre>' . nl2br(var_export(count($s), true)) . '</pre>////');
        if ($s == null) {
            $itiserv = new ItinerarioServicio();
            $itiserv->setServicio($servicio);
            $itiserv->setItinerario($itinerario);
            $itiserv->setEstado(0);
            $this->em->persist($itiserv);
            $this->em->flush();
        }
        return true;
    }

    public function addUsuario($itinerario, $usuario)
    {
        $s = $this->existUsuarioInItinerario($itinerario, $usuario); //verifico que ese usuario no este en el itinerario
        //die('////<pre>' . nl2br(var_export(count($s), true)) . '</pre>////');
        if (count($s) == 0 || $s == null) {
            $itinerario->addUsuario($usuario);

            $this->em->persist($itinerario);
            $this->em->flush();
        }
        return true;
    }

    public function addContacto($itinerario, $contacto)
    {
        $s = $this->existContactoInItinerario($itinerario, $contacto); //verifico que ese contacto no este en el itinerario
        //die('////<pre>' . nl2br(var_export(count($s), true)) . '</pre>////');
        if (count($s) == 0 || $s == null) {
            $itinerario->addContacto($contacto);

            $this->em->persist($itinerario);
            $this->em->flush();
        }
        return true;
    }

    public function delServicio($itinerario, $servicio)
    {
        $itiserv = $this->findItiServ($itinerario, $servicio);
        $this->em->remove($itiserv);
        $this->em->flush();
        return true;
    }

    public function historico($organizacion, $estado, $usuario = null, $servicio = null, $tipoServicio = null, $transporte = null, $satelital = null, $empresa = null, $fecha_desde = null, $fecha_hasta = null)
    {
        return $this->em->getRepository('App:Itinerario')->historico($organizacion, $estado, $usuario, $servicio, $tipoServicio, $transporte, $satelital, $empresa, $fecha_desde, $fecha_hasta);
    }

    public function addPoi($itinerario, $referencia, $orden = null, $eta = null, $stopType = null)
    {
        $paso = $this->findPoiByItiRef($itinerario, $referencia);
        if (is_null($paso)) {
            $paso = new Poi();
        }
        $paso->setItinerario($itinerario);
        $paso->setReferencia($referencia);
        if ($orden != null) {
            //die('////<pre>' . nl2br(var_export($orden, true)) . '</pre>////');
            $paso->setOrden($orden);
        } else {
            $paso->setOrden(0); //si viene null le ponemos 0
        }

        if ($eta) {
            $fechaEta = new \DateTime(
                $this->utilsManager->datetime2sqltimestamp($eta, false),
                new \DateTimeZone($itinerario->getOrganizacion()->getTimezone())
            );
            $paso->setFechaIngreso($fechaEta);
        }

        if ($stopType) {
            $paso->setStopType($stopType);
        }

        $this->em->persist($paso);
        $this->em->flush();
        return $paso;
    }

    public function findPoi($poi)
    {
        return $this->em->getRepository('App:Poi')->findOneBy(array('id' => $poi));
    }

    public function savePoi($poi)
    {
        $this->em->persist($poi);
        $this->em->flush();
        return $poi;
    }

    public function findPoiByItiRef($itinerario, $referencia)
    {
        return $this->em->getRepository('App:Poi')->findOneBy(array('itinerario' => $itinerario->getId(), 'referencia' => $referencia->getId()));
    }

    public function upReferencia($itinerario, $poi)
    {
        $itActual = $this->em->getRepository('App:Poi')->find($poi);
        $itAnt = $this->em->getRepository('App:Poi')->findOneBy(array(
            'itinerario' => $itinerario->getId(),
            'orden' => $itActual->getOrden() - 1
        ));
        if ($itAnt) {
            $orden = $itAnt->getOrden();
            $itAnt->setOrden($itActual->getOrden());
            $itActual->setOrden($orden);

            $this->em->persist($itAnt);
            $this->em->persist($itActual);
            $this->em->flush();
        }
        $this->reorder($itinerario);
        return true;
    }

    public function downReferencia($itinerario, $poi)
    {
        $itActual = $this->em->getRepository('App:Poi')->find($poi);
        if ($itActual) {
            $query = $this->em->createQueryBuilder()
                ->select('p')
                ->from('App:Poi', 'p')
                ->where('p.itinerario = :itinerario')->setParameter('itinerario', $itinerario->getId())
                ->andWhere('p.orden > :orden')->setParameter('orden', $itActual->getOrden())
                ->setMaxResults(1)
                ->orderBy('p.orden', 'ASC');
            $itSig = $query->getQuery()->getOneOrNullResult();


            $orden = $itSig->getOrden();
            $itSig->setOrden($itActual->getOrden());
            $itActual->setOrden($orden);

            $this->em->persist($itSig);
            $this->em->persist($itActual);
            $this->em->flush();
            $this->reorder($itinerario);
            return true;
        }
        return false;

        //die('////<pre>' . nl2br(var_export($itNext->getId(), true)) . '</pre>////');
    }

    public function delReferencia($itinerario, $id)
    {
        $referencia = null;
        $poi = $this->em->getRepository('App:Poi')->find($id);
        if ($poi !== null) {
            $referencia = $poi->getReferencia();
            $this->em->remove($poi);
            $this->em->flush();
            $this->reorder($itinerario);
        }
        return $referencia;
    }

    public function setClock($poi, $hArribo, $hPartida)
    {
        $poi->setFechaIngreso($hArribo);
        $poi->setFechaEgreso($hPartida);
        $this->em->persist($poi);
        $this->em->flush();
        return true;
    }

    public function reorder($itinerario)
    {
        $ref = $this->findPois($itinerario);
        $i = 1;
        foreach ($ref as $r) {
            $r->setOrden($i++);
            $this->em->persist($r);
        }
        $this->em->flush();
        return true;
    }

    public function findPois($itinerario)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Poi', 'p')
            ->join('p.itinerario', 'i')
            ->where('i.id = ?1')
            ->orderBy('p.orden')
            ->setParameter('1', $itinerario->getId());
        return $query->getQuery()->getResult();
    }

    public function checkOrderReferencia(Itinerario $itinerario)
    {
        $pois = $this->findPois($itinerario);
        $itFechaIn = $itinerario->getFechaInicio();
        $itFechaOut = $itinerario->getFechaFin();
        foreach ($pois as $key => $poi) {
            $this->arrCheck[$poi->getId()] = array(
                'poi' => $poi,
                'label' => $itinerario->getStrOrder(0),
                'color' => $itinerario->getColorOrder(0)
            );
            $status = $this->checkRank($itFechaIn, $itFechaOut, $poi, $itinerario);
            //die('////<pre>' . nl2br(var_export($this->arrCheck, true)) . '</pre>////');
            if (!$status) {
                $prevItRef = ($key == 0) ? false : $pois[$key - 1]; //tomo el anterior
                $status = $this->checkLess($prevItRef, $poi, $itinerario);
            }
            if (!$status) {
                $nextItRef = ($key == count($pois) - 1) ? false : $pois[$key + 1];
                $status = $this->checkGreater($nextItRef, $poi, $itinerario);
            }
        }
        return $this->arrCheck;
    }

    public function checkRank($itFechaIn, $itFechaOut, $poi, $itinerario)
    {
        if (
            $poi->getFechaIngreso() <= $itFechaIn || $poi->getFechaEgreso() <= $itFechaIn ||
            $poi->getFechaIngreso() >= $itFechaOut || $poi->getFechaEgreso() >= $itFechaOut
        ) {
            $this->arrCheck[$poi->getId()] = array(
                'poi' => $poi,
                'label' => $itinerario->getStrOrder(1),
                'color' => $itinerario->getColorOrder(1),
            );
            return true;
        }
        return false;
    }

    public function checkLess($itrPrev, $poi, $itinerario)
    {
        if ($itrPrev) { // no es el primero
            if ((($poi->getFechaIngreso() <= $itrPrev->getFechaIngreso()) || ($poi->getFechaEgreso() <= $itrPrev->getFechaIngreso()) ||
                ($poi->getFechaIngreso() <= $itrPrev->getFechaEgreso()) || ($poi->getFechaEgreso() <= $itrPrev->getFechaEgreso()))) {
                $this->arrCheck[$poi->getId()] = array(
                    'poi' => $poi,
                    'label' => $itinerario->getStrOrder(3),
                    'color' => $itinerario->getColorOrder(3)
                );
                return true;
            }
            return false;
        }
        return false;
    }

    public function checkGreater($itrNext, $poi, $itinerario)
    {
        if ($itrNext) {
            if ((($poi->getFechaIngreso() >= $itrNext->getFechaIngreso()) || ($poi->getFechaEgreso() >= $itrNext->getFechaIngreso()) ||
                ($poi->getFechaIngreso() >= $itrNext->getFechaEgreso()) || ($poi->getFechaEgreso() >= $itrNext->getFechaEgreso()))) {
                $this->arrCheck[$poi->getId()] = array(
                    'poi' => $poi,
                    'label' => $itinerario->getStrOrder(2),
                    'color' => $itinerario->getColorOrder(2)
                );
                return true;
            }
            return false;
        }
        return false;
    }

    public function findOpens($user = null)
    {
        return $this->em->getRepository('App:Itinerario')->findOpens($user);
    }

    public function findInCurso($user = null)
    {
        return $this->em->getRepository('App:Itinerario')->findInCurso($user);
    }

    public function findAllByUser($user)
    {
        return $this->em->getRepository('App:Itinerario')->findByUser($user);
    }

    public function findByOptions($organizacion, $estado, $empresas = null, $satelitales = null, $transportes = null)
    {
        return $this->em->getRepository('App:Itinerario')->findByOptions($organizacion, $estado, $empresas, $satelitales, $transportes);
    }


    public function getServiciosByEntity($usuario, $getCustodios, $itinerario = null)
    {

        $optionsServicio = array();

        $servicios = $this->em->getRepository('App:Servicio')->findAllByUsuario($usuario);

        if (count($servicios) > 0) {
            foreach ($servicios as $servicio) {
                if ($servicio->getEstado() == 1) {
                    $orgNombre = '';
                    if ($usuario->getTransporte()) {
                        $orgNombre = $usuario->getTransporte()->getNombre();
                    } elseif ($usuario->getLogistica()) {
                        $orgNombre = $usuario->getLogistica()->getNombre();
                    } else {
                        $orgNombre = $usuario->getOrganizacion()->getNombre();
                    }

                    if ($servicio->getIsCustodio() == $getCustodios) {
                        if (!$itinerario) {
                            $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getTransporte() . ')';
                        } else {
                            $itiServ = $this->findItiServ($itinerario, $servicio);
                            if ($itiServ == null) {
                                $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getTransporte() . ')';
                            }
                        }
                    }
                }
            }
        } else {
            $optionsServicio = $this->getOptionsServicio($usuario, $getCustodios, $itinerario);
        }


        return $optionsServicio;
    }

    public function getChoferesByEntity($choferes)
    {

        $optionsChoferes = array();
        if (count($choferes) > 0) {
            //die('////<pre>' . nl2br(var_export(count($choferes), true)) . '</pre>////');
            foreach ($choferes as $chofer) {
                $orgId = $chofer->getTransporte() ? $chofer->getTransporte()->getId() : $chofer->getOrganizacion()->getId();
                $orgNombre = $chofer->getTransporte() ? $chofer->getTransporte()->getNombre() : $chofer->getOrganizacion()->getNombre();

                $lista = $this->getLicencias($chofer);
                $txt_lista = $lista != null ? '<ul>' . $lista . '</ul>' : '';

                $optionsChoferes[$orgId][] = array(
                    'id' => $chofer->getId(),
                    'html' => '<div><h6>' . $chofer->getNombre() . ' (' . $orgNombre . ')'  . '</h6>' . $txt_lista . '</div>',
                    'text' => '<div><h6>' . $chofer->getNombre() . ' (' . $orgNombre . ')'  . '</h6></div>'


                );
            }
        }
        return $optionsChoferes;
    }

    private function getLicencias($chofer)
    {
        $lista = null;

        foreach ($chofer->getLicencias() as $licencia) {
            $color = 'black';
            if (new \DateTime() >= $licencia->getVencimiento()) {
                $color = 'red';
            } else {
                $str_avio = (string)$licencia->getAviso();
                $aviso = (new \DateTime())->add(new \DateInterval('P' . $str_avio . 'D'));
                if ($aviso >= $licencia->getVencimiento()) {
                    $color = '#b5a73b';
                }
            }
            $now = $licencia->getVencimiento() == new \DateTime();
            $lista .= '<li><i style=' . "color:$color;" . '>Licencia: ' . $licencia->getCategoria() . ', Vto: ' . $licencia->getVencimiento()->format('d/m/Y') . '</i> </li>';
        }

        return $lista;
    }

    private function getOptionsServicio($usuario, $getCustodios, $itinerario = null)
    {
        $optionsServicio = array();

        if ($usuario->getTransporte()) { //es usuario de transporte solo traer servicios de el mismo
            $optionsServicio = $this->getServiciosByTransporte($usuario, $getCustodios, $itinerario);
        } elseif ($usuario->getLogistica()) { //es usuario de logistica. traer solo los de la logistica
            $optionsServicio = $this->getServiciosByLogistica($usuario, $getCustodios, $itinerario);
        } else {

            $organizacion = $usuario->getOrganizacion();
            $orgNombre = $organizacion->getNombre();
            $optionsServicio = $this->getServiciosByTransporte($organizacion, $getCustodios, $itinerario);

            foreach ($organizacion->getServicios() as $servicio) { // organizacion padre
                if ($servicio->getEstado() == 1) {
                    if ($servicio->getIsCustodio() == $getCustodios) {
                        if (!$itinerario) {
                            if (!$this->ifExist($optionsServicio, $servicio)) {
                                $optionsServicio[$orgNombre][$servicio->getId()] =  $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $organizacion->getNombre() . ')';
                            }
                        } else {
                            $itiServ = $this->findItiServ($itinerario, $servicio);
                            if ($itiServ == null) {
                                if (!$this->ifExist($optionsServicio, $servicio)) {
                                    $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $organizacion->getNombre() . ')';
                                }
                            }
                        }
                    }
                }
            }
        }

        return $optionsServicio;
    }

    private function ifExist($optionsServicio, $servicio)
    {
        $existe = false;
        foreach ($optionsServicio as $org) {
            foreach ($org as $key => $value) {
                if ($key == $servicio->getId()) {
                    //die('////<pre>' . nl2br(var_export($key, true)) . '</pre>////');
                    $existe = true;
                }
            }
        }
        return $existe;
    }

    private function getServiciosByLogistica($usuario, $getCustodios, $itinerario)
    {
        $optionsServicio = array();
        $orgNombre = $usuario->getLogistica()->getNombre();
        foreach ($usuario->getLogistica()->getServicios() as $servicio) {
            if ($servicio->getEstado() == 1) {
                if ($servicio->getIsCustodio() == $getCustodios) {
                    if (!$itinerario) {
                        $optionsServicio[$usuario->getLogistica()->getNombre()][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                    } else {
                        $itiServ = $this->findItiServ($itinerario, $servicio);
                        if ($itiServ == null) {
                            if (!$this->ifExist($optionsServicio, $servicio)) {
                                $optionsServicio[$usuario->getLogistica()->getNombre()][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                            }
                        }
                    }
                }
            }
        }
        return $optionsServicio;
    }
    private function getServiciosByTransporte($entity, $getCustodios, $itinerario)
    {

        $optionsServicio = array();
        if ($entity instanceof Usuario) {
            $usuario = $entity;
            $orgNombre = $usuario->getTransporte()->getNombre();
            foreach ($usuario->getTransporte()->getServicios() as $servicio) {
                if ($servicio->getEstado() == 1) {
                    if ($servicio->getIsCustodio() == $getCustodios) {
                        if (!$itinerario) {
                            $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                        } else {
                            $itiServ = $this->findItiServ($itinerario, $servicio);
                            if ($itiServ == null) {
                                if (!$this->ifExist($optionsServicio, $servicio)) {
                                    $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                                }
                            }
                        }
                    }
                }
            }
        } else { //organizacion
            $organizacion = $entity;
            foreach ($organizacion->getTransportes() as $transporte) {
                $orgNombre = $transporte->getNombre();
                foreach ($transporte->getServicios() as $servicio) {
                    if ($servicio->getEstado() == 1) {
                        if ($servicio->getIsCustodio() == $getCustodios) {
                            if (!$itinerario) {
                                $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $transporte->getNombre() . ')';
                            } else {
                                $itiServ = $this->findItiServ($itinerario, $servicio);
                                if ($itiServ == null) {
                                    $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $transporte->getNombre() . ')';
                                }
                            }
                        }
                    }
                }
            }
            foreach ($organizacion->getLogisticas() as $logistica) {
                $orgNombre = $logistica->getNombre();
                foreach ($logistica->getServicios() as $servicio) {
                    if ($servicio->getEstado() == 1) {
                        if ($servicio->getIsCustodio() == $getCustodios) {
                            if (!$itinerario) {
                                $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $logistica->getNombre() . ')';
                            } else {
                                $itiServ = $this->findItiServ($itinerario, $servicio);
                                if ($itiServ == null) {
                                    $optionsServicio[$orgNombre][$servicio->getId()] = $servicio->getId()  . ' - ' . $servicio->getNombre() . ' (' . $logistica->getNombre() . ')';
                                }
                            }
                        }
                    }
                }
            }
        }
        return $optionsServicio;
    }

    public function getUsuarios($usuario, $itinerario = null)
    {

        $optionsUsuario = array();
        if ($usuario->getLogistica()) { //es usuario de logistica

            foreach ($usuario->getLogistica()->getUsuarios() as $usuario) {
                if (!$itinerario) {
                    $optionsUsuario['Logisticas'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                } else {
                    if (!$itinerario->getUsuarios()->contains($usuario)) {
                        $optionsUsuario['Logisticas'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                    }
                }
            }
        } elseif ($usuario->getEmpresa()) { //es usuario de cliente

            foreach ($usuario->getEmpresa()->getUsuarios() as $usuario) {
                if (!$itinerario) {
                    $optionsUsuario['Clientes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getEmpresa()->getNombre() . ')';
                } else {
                    if (!$itinerario->getUsuarios()->contains($usuario)) {
                        $optionsUsuario['Clientes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getEmpresa()->getNombre() . ')';
                    }
                }
            }
        } elseif ($usuario->getTransporte()) { //es usuario de transporte

            foreach ($usuario->getTransporte()->getUsuarios() as $usuario) {
                if (!$itinerario) {
                    $optionsUsuario['Transportes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                } else {
                    if (!$itinerario->getUsuarios()->contains($usuario)) {
                        $optionsUsuario['Transportes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                    }
                }
            }
        } else { // es usuario de sadi (master itinerario)
            foreach ($usuario->getOrganizacion()->getUsuarios() as $usuario) {
                if (!$itinerario) {
                    $optionsUsuario[$usuario->getOrganizacion()->getNombre()][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre();
                } else {
                    if (!$itinerario->getUsuarios()->contains($usuario)) {
                        $optionsUsuario[$usuario->getOrganizacion()->getNombre()][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre();
                    }
                }
            }
            foreach ($usuario->getOrganizacion()->getEmpresas() as $empresa) {
                foreach ($empresa->getUsuarios() as $usuario) {
                    if (!$itinerario) {
                        $optionsUsuario['Clientes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre();
                    } else {
                        if (!$itinerario->getUsuarios()->contains($usuario)) {
                            $optionsUsuario['Clientes'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $empresa->getNombre() . ')';
                        }
                    }
                }
            }
            foreach ($usuario->getOrganizacion()->getLogisticas() as $logistica) {
                foreach ($logistica->getUsuarios() as $usuario) {
                    if (!$itinerario) {
                        $optionsUsuario['Logisticas'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $logistica->getNombre() . ')';
                    } else {
                        if (!$itinerario->getUsuarios()->contains($usuario)) {
                            $optionsUsuario['Logisticas'][$usuario->getId()] = $usuario->getId()  . ' - ' . $usuario->getNombre() . ' (' . $logistica->getNombre() . ')';
                        }
                    }
                }
            }
        }
        return $optionsUsuario;
    }

    public function getContactos($usuario, $itinerario = null)
    {
        $optionsContacto = array();
        if ($usuario->getLogistica()) { //es contacto de logistica
            foreach ($usuario->getLogistica()->getContactos() as $contacto) {
                if (!$itinerario) {
                    $optionsContacto['Logisticas'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                } else {
                    if (!$itinerario->getContactos()->contains($contacto)) {
                        $optionsContacto['Logisticas'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getLogistica()->getNombre() . ')';
                    }
                }
            }
        } elseif ($usuario->getEmpresa()) { //es contacto de cliente
            foreach ($usuario->getEmpresa()->getContactos() as $contacto) {
                if (!$itinerario) {
                    $optionsContacto['Clientes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getEmpresa()->getNombre() . ')';
                } else {
                    if (!$itinerario->getContactos()->contains($contacto)) {
                        $optionsContacto['Clientes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getEmpresa()->getNombre() . ')';
                    }
                }
            }
        } elseif ($usuario->getTransporte()) { //es contacto de transporte
            foreach ($usuario->getTransporte()->getContactos() as $contacto) {
                if (!$itinerario) {
                    $optionsContacto['Transportes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                } else {
                    if (!$itinerario->getContactos()->contains($contacto)) {
                        $optionsContacto['Transportes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $usuario->getTransporte()->getNombre() . ')';
                    }
                }
            }
        } else { //es contacto de sadi

            foreach ($usuario->getOrganizacion()->getContactos() as $contacto) {
                if (!$itinerario) {
                    $optionsContacto[$usuario->getOrganizacion()->getNombre()][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre();
                } else {
                    if (!$itinerario->getContactos()->contains($contacto) &&  !$contacto->getEmpresa() && !$contacto->getLogistica()) {
                        $optionsContacto[$usuario->getOrganizacion()->getNombre()][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre();
                    }
                }
            }
            foreach ($usuario->getOrganizacion()->getEmpresas() as $empresa) {
                foreach ($empresa->getContactos() as $contacto) {
                    if (!$itinerario) {
                        $optionsContacto['Clientes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $empresa->getNombre() . ')';
                    } else {
                        if (!$itinerario->getContactos()->contains($contacto)) {
                            $optionsContacto['Clientes'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $empresa->getNombre() . ')';
                        }
                    }
                }
            }
            foreach ($usuario->getOrganizacion()->getLogisticas() as $logistica) {
                foreach ($logistica->getContactos() as $contacto) {
                    if (!$itinerario) {
                        $optionsContacto['Logisticas'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $logistica->getNombre() . ')';
                    } else {
                        if (!$itinerario->getContactos()->contains($contacto)) {
                            $optionsContacto['Logisticas'][$contacto->getId()] = $contacto->getId()  . ' - ' . $contacto->getNombre() . ' (' . $logistica->getNombre() . ')';
                        }
                    }
                }
            }
        }

        return $optionsContacto;
    }

    public function getServicios($itinerario = null)
    {
        //obtengo el array de los equipos del itinerario
        $si = array();
        if ($itinerario != null) {
            foreach ($itinerario->getServicios() as $s) {
                $si[$s->getServicio()->getId()] = $s->getServicio()->getId();
            }
        }

        $servicios = array();
        $serv = $this->servicioManager->findEnabledByUsuario();
        foreach ($serv as $s) {
            if (!in_array($s->getId(), $si)) {
                $servicios[] = $s;
            }
        }
        return $servicios;
    }


    public function getCustodiosFree($itinerario)
    {
        //obtengo el array de los equipos del itinerario
        $si = array();
        if ($itinerario != null && $itinerario->getServicios() != null) {
            foreach ($itinerario->getServicios() as $s) {
                $si[$s->getServicio()->getId()] = $s->getServicio()->getId();
            }
        }
        $custodios = array();
        $serv = $this->servicioManager->findEnabledByUsuario();

        foreach ($serv as $s) {
            if ($s->getIsCustodio()) {
                if (!in_array($s->getId(), $si)) {
                    $custodios[] = $s;
                }
            }
        }
        return $custodios;
    }

    /**
     * Busca por logistica todos los itinerarios. Si se le pasa estado entonces solo trae 
     * los itinerarios de esa logistica que tengan esos estados.
     */
    public function findAllByLogistica($logistica, $estado = null)
    {
        $query = $this->em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->where('i.logistica = :logi')->setParameter('logi', $logistica);
        if ($estado) {
            $query->andWhere('i.estado in (:estado)')->setParameter('estado', $estado);
        } else {
            $query->andWhere('i.estado < 9');
        }
        return $query->getQuery()->getResult();
    }

    public function findProvinciaByNombre($nombre)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Provincia', 'p')
            ->where('p.nombre = :nombre')->setParameter('nombre', $nombre);
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findLastPoi($itinerario)
    {
        $pois = $this->em->getRepository('App:Poi')->findBy(
            array('itinerario' => $itinerario->getId()),
            array('orden' => 'DESC')
        );
        return reset($pois);
    }
}
