<?php

namespace App\Model\monitor;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Logistica;
use App\Entity\Organizacion as Organizacion;

class LogisticaManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($logistica)
    {
        if ($logistica instanceof Logistica) {
            return $this->em->getRepository('App:Logistica')->findOneBy(array('id' => $logistica->getId()));
        } else {
            return $this->em->getRepository('App:Logistica')->find($logistica);
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Logistica')->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'asc'));
        } else {
            return $this->em->getRepository('App:Logistica')->findBy(array('organizacion' => $organizacion), array('nombre' => 'asc'));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $logistica = new Logistica();
        $logistica->setOrganizacion($organizacion);

        return $logistica;
    }

    public function save(Logistica $logistica)
    {
        $this->em->persist($logistica);
        $this->em->flush();
        return $logistica;
    }

    public function delete(Logistica $logistica)
    {
        $this->em->remove($logistica);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $logistica = $this->find($id);
        if (!$logistica) {
            throw $this->createNotFoundException('Código de logistica no encontrado.');
        }
        return $this->delete($logistica);
    }

    public function findByIds($ids)
    {
        $logisticas = array();
        foreach ($ids as $id) {
            $logisticas[] = $this->find($id);
        }
        return $logisticas;
    }
}
