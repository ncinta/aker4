<?php

namespace App\Model\monitor;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contacto as Contacto;
use App\Entity\Equipo as Equipo;
use App\Entity\Servicio as Servicio;
use App\Entity\Referencia as Referencia;
use App\Entity\GrupoReferencia as GrupoReferencia;
use App\Entity\Evento as Evento;
use App\Model\app\ContactoManager;
use App\Model\app\EquipoManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\EventoManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Predis\Client;
use Psr\Cache\CacheItemPoolInterface;
use App\Model\app\Cache\P44CacheManager;

/**
 * Description of NotificadorP44Manager
 *
 * @author nicolas
 */
class NotificadorP44Manager
{

    protected $equipoManager;
    protected $servicioManager;
    protected $referenciaManager;
    protected $grupoReferenciaManager;
    protected $eventoManager;
    protected $contactoManager;
    protected $security;
    protected $em;
    protected $cliente;
    protected $container;
    protected $cacheP44;
    private $tokenP44 = null;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $security,
        HttpClientInterface $cliente,
        EquipoManager $equipoManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoManager $eventoManager,
        ContactoManager $contactoManager,
        ContainerInterface $container,
        P44CacheManager $p44Cache
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->equipoManager = $equipoManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoManager = $eventoManager;
        $this->contactoManager = $contactoManager;
        $this->cliente = $cliente;
        $this->container = $container;        
        $this->cacheP44 = $p44Cache;
    }

    

    /**
     * Devuelve el token que debo enviar con los otros datos.
     * Si tengo token en redis uso ese token, sino me encargo de pedir otro nuevo
     */
    public function obtenerToken()
    {
        $token = $this->cacheP44->getToken2Redis();
        if (!$token) {
            $token = $this->getToken();
        }
        return $token;
    }

    /** el token de autorizacion para el envio de los otros datos.     
     */
     
    private function getToken()
    {
        if ($this->container->getParameter('aker_p44_enabled') == true) {
            $result = 'ERROR';
            try {
                $response = $this->cliente->request(
                    'POST',
                    $this->container->getParameter('aker_p44_endpoint') . '/api/v4/oauth2/token',
                    array(
                        'body' => [
                            "client_id" => $this->container->getParameter('aker_p44_client'),
                            "client_secret" => $this->container->getParameter('aker_p44_secret'),
                            "grant_type" => "client_credentials"
                        ],
                        'headers' => [
                            'Access-Control-Allow-Origin' => '*',
                            'Content-Type' => 'application/x-www-form-urlencoded'
                        ]
                    )
                );

                $statusCode = $response->getStatusCode();
                if ($statusCode === 200) {
                    $json_result = $response->getContent();
                    $result = json_decode($json_result, true);
                    if (isset($result['access_token'])) {
                        //debo grabar el token en redis
                        $this->cacheP44->saveToken2Redis($result['access_token'], $result['expires_in']);
                        return $result['access_token'];
                    } else {
                        return null;
                    }
                }
            } catch (TransportException $e) {
                return false;
            }

            return $statusCode === 200 ? true : false;
        } else {
            return false;
        }
    }

    public function sendData($json, $token)
    {
        $result = 'ERROR';
        try {
            $response = $this->cliente->request(
                'POST',
                $this->container->getParameter('aker_p44_endpoint') . '/api/v4/capacityproviders/tl/shipments/statusUpdates',
                [
                    'body' => $json,
                    'timeout' => 10,  //10seg de espera                    
                    'headers' => [
                        'Access-Control-Allow-Origin' => '*',
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                    'auth_bearer' =>  $token
                ]
            );
            $statusCode = $response->getStatusCode();
            //die('////<pre>'.nl2br(var_export($statusCode, true)).'</pre>////');
            if ($statusCode === 200 || $statusCode === 201) {
                $result = $response->getContent();
            }
            return $statusCode;
        } catch (TransportException $e) {
            return false;
        }

        return $statusCode === 200 || $statusCode === 201 ? true : false;
    }
   
}
