<?php

namespace App\Model\monitor;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Entity\TemplateItinerario;
use Exception;
use App\Model\app\ServicioManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\app\ReferenciaManager;
use Doctrine\ORM\EntityManagerInterface;

class ImportRobinsonManager
{
    private $servicioManager;
    private $itinerarioManager;
    private $referenciaManager;
    private $em;
    // ... otros managers necesarios

    const IMPORT_TYPE_CHR = 'chr';
    const IMPORT_TYPE_P44 = 'p44';
    const IMPORT_TYPE_TEKNAL = 'teknal';
    // ... otros tipos de importación

    private const EXPECTED_HEADERS = [
        self::IMPORT_TYPE_CHR => [
            'A' => 'LOAD NUM',
            'B' => 'STOP NUM',
            'C' => 'STOP TYPE',
            'D' => 'WAREHOUSE CODE',
            'E' => 'NAME',
            'F' => 'ADDRESS1',
            'G' => 'CITY',
            'H' => 'STATE',
            'I' => 'LATITUDE DEC',
            'J' => 'LONGITUDE DEC',
            'K' => 'PRODUCT1',
            'L' => 'REF NUM',
            'M' => 'ORD PALLET POSITIONS',
            'N' => 'PALLETS STACKABLE',
            'O' => 'ACTUAL WEIGHT',
            'P' => 'ACTUAL PIECES',
            'Q' => 'ACTUAL PALLETS',
            'R' => 'ACTIVITY DATE',
            'S' => 'ETA',
            'T' => 'DRIVER NAME',
            'U' => 'DRIVER PAGER',
            'V' => 'TRACTOR NUM',
            'W' => 'TRAILER NUM',
            'X' => 'CETDESCRIPTION',
            'Y' => 'CARRIER CODE',
            'Z' => 'CARRIER NAME'
        ],
        self::IMPORT_TYPE_P44,
        self::IMPORT_TYPE_TEKNAL => [
            'A' => 'LOAD NUM',
            'B' => 'STOP NUM',
            'C' => 'STOP TYPE',
            'D' => 'WAREHOUSE CODE',
            'E' => 'NAME',
            'F' => 'ADDRESS1',
            'G' => 'CITY',
            'H' => 'STATE',
            'I' => 'LATITUDE DEC',
            'J' => 'LONGITUDE DEC',
            'K' => 'REF NUM',
            'L' => 'ACTIVITY DATE',
            'M' => 'ETA',
            'N' => 'DRIVER NAME',
            'O' => 'TRACTOR NUM',
            'P' => 'TRAILER NUM',
            'Q' => 'CUSTOMER ID'
        ]
    ];

    public function __construct(
        ServicioManager $servicioManager,
        ItinerarioManager $itinerarioManager,
        ReferenciaManager $referenciaManager,
        EntityManagerInterface $em
    ) {
        $this->servicioManager = $servicioManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->referenciaManager = $referenciaManager;
        $this->em = $em;
        // ... inicializar otros managers
    }

    public function processFile($file, $type, $organizacion)
    {
        if (!file_exists($file)) {
            throw new Exception("No existe el archivo -> " . $file);
        }

        $spreadsheet = $this->loadSpreadsheet($file);
        $worksheet = $spreadsheet->getActiveSheet();
        
        // Validar encabezados antes de procesar
        $validacion = $this->validateHeaders($worksheet, $type);
        if (!$validacion['isValid']) {
            return $validacion;
        }

        switch ($type) {
            case self::IMPORT_TYPE_CHR:
                return $this->processChr($spreadsheet, $organizacion);
            case self::IMPORT_TYPE_P44:
                return $this->processP44($spreadsheet, $organizacion);
            case self::IMPORT_TYPE_TEKNAL:
                return $this->processTeknal($spreadsheet, $organizacion);
                // ... otros casos
            default:
                throw new Exception("Tipo de importación no soportado");
        }
    }

    private function loadSpreadsheet($file)
    {
        try {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $reader->setReadDataOnly(true);
            return $reader->load($file);
        } catch (Exception $e) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            $reader->setReadDataOnly(true);
            return $reader->load($file);
        }
    }

    private function processChr($spreadsheet, $organizacion)
    {

        $data = [];
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $pois = array();
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellFecha = $worksheet->getCell('R' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)
                    $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellFecha)) : '';
                    $fechaFin = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . ' 23:59:59' : '';
                    $codigo =  trim($worksheet->getCell('A' . $i)->getCalculatedValue());
                    $itinerario = $this->itinerarioManager->findByCodigo($codigo);
                    $patente = trim($worksheet->getCell('V' . $i)->getCalculatedValue());
                    $servicio = $this->servicioManager->findByPatente($patente);

                    //dd($cellLibre);
                    $data[$codigo] = array(
                        'tipo' => 'chr',
                        'existeItinerario' => $itinerario != null ? true : false,
                        'cellCodigo' => $codigo, //LOADNUM
                        'cellChofer' => $worksheet->getCell('T' . $i)->getCalculatedValue(), //DRIVERNAME
                        'existeServicio' => $servicio != null ? true : false,
                        'cellServicio' => $patente, //TRACTORNUM 
                        'cellAcoplado' => $worksheet->getCell('W' . $i)->getCalculatedValue(), //TRAILERNUM 
                        'cellFechaInicio' =>  $fechaInicio,
                        'cellFechaFin' =>  $fechaFin,
                        'pois' => null
                    );
                }
            }
        }
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellPoiEta = $worksheet->getCell('S' . $i)->getCalculatedValue(); // cell ETA. es la  entrada a poi
                    $fechaEta = $cellPoiEta != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellPoiEta)) : '';
                    $codigoExterno =  trim($worksheet->getCell('D' . $i)->getCalculatedValue());
                    $poi = $this->referenciaManager->findByCodigoExterno($codigoExterno);
                    $codigo =  $worksheet->getCell('A' . $i)->getCalculatedValue();
                    $orden =  $worksheet->getCell('B' . $i)->getCalculatedValue();
                    $pois = array(
                        'existePoi' => $poi != null ? true : false,
                        'cellPoiOrden' => $orden, //STOPNUM
                        'cellPoiCode' => $codigoExterno,  //WAREHOUSECODE
                        'cellPoiStopType' => trim($worksheet->getCell('C' . $i)->getCalculatedValue()), //STOPTYPE
                        'cellPoiName' => $worksheet->getCell('E' . $i)->getCalculatedValue(), //NAME
                        'CellPoiAddress' => $worksheet->getCell('F' . $i)->getCalculatedValue(),  //ADDRESS1
                        'cellPoiCity' => $worksheet->getCell('G' . $i)->getCalculatedValue(), //CITY
                        'cellPoiState' => $worksheet->getCell('H' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLat' => $worksheet->getCell('I' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLong' => $worksheet->getCell('J' . $i)->getCalculatedValue(), //LONGITUDEDEC
                        'cellPoiEta' => $fechaEta, //ETA (FECHA INGRESO AL POI)                    
                    );
                    $data[$codigo]['pois'][$orden] = $pois;
                    if ($orden == 0) {
                        $cellFecha = $worksheet->getCell('R' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)
                        $pos = strpos($fechaEta, ' '); //buscamos el id del usuario
                        $horaEta0 = substr($fechaEta, $pos + 1);
                        // actualizamos la fecha de inicio del itinerario con la hora de la eta. 
                        $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . " " . $horaEta0 : '';
                        // die('////<pre>' . nl2br(var_export($fechaInicio, true)) . '</pre>////');
                        $data[$codigo]['cellFechaInicio'] = $fechaInicio;
                    }
                }
            }
        }

        foreach ($data as $key => $dataIt) {
            $template = new TemplateItinerario();
            $template->setOrganizacion($organizacion);
            $template->setNombre($key);
            $json_template = json_encode($data[$key], true);
            $template->setData($json_template);
            $template->setBloqueado(1);
            $this->em->persist($template);
        }
        $this->em->flush();
        unset($spreadsheet);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.       

        return $data;
    }

    private function processP44($spreadsheet, $organizacion)
    {
        $data = [];
        //primer recorrido para armado de la cabercera del itinerario
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $pois = array();
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos

                    //fecha
                    $cellFecha = $worksheet->getCell('L' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)                    
                    $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellFecha)) : '';
                    $fechaFin = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . ' 23:59:59' : '';

                    //itinerario
                    $codigo =  trim($worksheet->getCell('A' . $i)->getCalculatedValue());
                    $itinerario = $this->itinerarioManager->findByCodigo($codigo);

                    //servicio
                    $patente = trim($worksheet->getCell('O' . $i)->getCalculatedValue());
                    $servicio = $this->servicioManager->findByPatente($patente);

                    $data[$codigo] = array(
                        'tipo' => 'p44',
                        'existeItinerario' => $itinerario != null ? true : false,
                        'cellCodigo' => $codigo, //LOADNUM
                        'cellChofer' => $worksheet->getCell('N' . $i)->getCalculatedValue(), //DRIVERNAME
                        'existeServicio' => $servicio != null ? true : false,
                        'cellServicio' => $patente, //TRACTORNUM 
                        'cellAcoplado' => $worksheet->getCell('P' . $i)->getCalculatedValue(), //TRAILERNUM 
                        'cellFechaInicio' => $fechaInicio,
                        'cellFechaFin' => $fechaFin,
                        'cellReferencia' => strval($worksheet->getCell('K' . $i)->getCalculatedValue()), //Ref Num 
                        'cellCustomerId' => strval($worksheet->getCell('Q' . $i)->getCalculatedValue()), //CustomerId
                        'pois' => null
                    );
                }
            }
            //  dd($data);
        }
        //segundo recorrido para armado de los pois de cada itinerario
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellPoiEta = $worksheet->getCell('M' . $i)->getCalculatedValue(); // cell ETA. es la  entrada a poi
                    $fechaEta = $cellPoiEta != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellPoiEta)) : '';
                    $codigoExterno =  trim($worksheet->getCell('D' . $i)->getCalculatedValue());
                    $poi = $this->referenciaManager->findByCodigoExterno($codigoExterno);
                    $codigo =  $worksheet->getCell('A' . $i)->getCalculatedValue();
                    $orden =  $worksheet->getCell('B' . $i)->getCalculatedValue();
                    $pois = array(
                        'existePoi' => $poi != null ? true : false,
                        'cellPoiOrden' => $orden, //STOPNUM
                        'cellPoiCode' => $codigoExterno,  //WAREHOUSECODE
                        'cellPoiStopType' => trim($worksheet->getCell('C' . $i)->getCalculatedValue()), //STOPTYPE
                        'cellPoiName' => $worksheet->getCell('E' . $i)->getCalculatedValue(), //NAME
                        'CellPoiAddress' => $worksheet->getCell('F' . $i)->getCalculatedValue(),  //ADDRESS1
                        'cellPoiCity' => $worksheet->getCell('G' . $i)->getCalculatedValue(), //CITY
                        'cellPoiState' => $worksheet->getCell('H' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLat' => $worksheet->getCell('I' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLong' => $worksheet->getCell('J' . $i)->getCalculatedValue(), //LONGITUDEDEC
                        'cellPoiEta' => $fechaEta, //ETA (FECHA INGRESO AL POI)                    
                    );
                    $data[$codigo]['pois'][$orden] = $pois;
                    if ($orden == 0) {
                        $cellFecha = $worksheet->getCell('M' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)
                        $pos = strpos($fechaEta, ' '); //buscamos el id del usuario
                        $horaEta0 = substr($fechaEta, $pos + 1);
                        // actualizamos la fecha de inicio del itinerario con la hora de la eta.                         
                        $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . " " . $horaEta0 : '';
                        $data[$codigo]['cellFechaInicio'] = $fechaInicio;
                    }
                }
            }
        }

        foreach ($data as $key => $dataIt) {
            $template = new TemplateItinerario();
            $template->setOrganizacion($organizacion);
            $template->setNombre($key);
            $json_template = json_encode($data[$key], true);
            $template->setData($json_template);
            $template->setBloqueado(1);
            $this->em->persist($template);
        }
        $this->em->flush();
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.       

        return $data;
    }


    //TEKNAL
    private function processTeknal($spreadsheet, $organizacion)
    {
        $data = [];
        //primer recorrido para armado de la cabercera del itinerario
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $pois = array();
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos

                    //fecha
                    $cellFecha = $worksheet->getCell('L' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)                    
                    $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellFecha)) : '';
                    $fechaFin = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . ' 23:59:59' : '';

                    //itinerario
                    $codigo =  trim($worksheet->getCell('A' . $i)->getCalculatedValue());
                    $itinerario = $this->itinerarioManager->findByCodigo($codigo);

                    //servicio
                    $patente = trim($worksheet->getCell('O' . $i)->getCalculatedValue());
                    $servicio = $this->servicioManager->findByPatente($patente);

                    $data[$codigo] = array(
                        'tipo' => 'teknal',
                        'existeItinerario' => $itinerario != null ? true : false,
                        'cellCodigo' => $codigo, //LOADNUM
                        'cellChofer' => $worksheet->getCell('N' . $i)->getCalculatedValue(), //DRIVERNAME
                        'existeServicio' => $servicio != null ? true : false,
                        'cellServicio' => $patente, //TRACTORNUM 
                        'cellAcoplado' => $worksheet->getCell('P' . $i)->getCalculatedValue(), //TRAILERNUM 
                        'cellFechaInicio' => $fechaInicio,
                        'cellFechaFin' => $fechaFin,
                        'cellReferencia' => strval($worksheet->getCell('K' . $i)->getCalculatedValue()), //Ref Num 
                        'cellCustomerId' => strval($worksheet->getCell('Q' . $i)->getCalculatedValue()), //CustomerId
                        'pois' => null
                    );
                }
            }
        }
        //segundo recorrido para armado de los pois de cada itinerario
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellPoiEta = $worksheet->getCell('M' . $i)->getCalculatedValue(); // cell ETA. es la  entrada a poi
                    $fechaEta = $cellPoiEta != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp($cellPoiEta)) : '';
                    $codigoExterno =  trim($worksheet->getCell('D' . $i)->getCalculatedValue());
                    $poi = $this->referenciaManager->findByCodigoExterno($codigoExterno);
                    $codigo =  $worksheet->getCell('A' . $i)->getCalculatedValue();
                    $orden =  $worksheet->getCell('B' . $i)->getCalculatedValue();
                    $pois = array(
                        'existePoi' => $poi != null ? true : false,
                        'cellPoiOrden' => $orden, //STOPNUM
                        'cellPoiCode' => $codigoExterno,  //WAREHOUSECODE
                        'cellPoiStopType' => trim($worksheet->getCell('C' . $i)->getCalculatedValue()), //STOPTYPE
                        'cellPoiName' => $worksheet->getCell('E' . $i)->getCalculatedValue(), //NAME
                        'CellPoiAddress' => $worksheet->getCell('F' . $i)->getCalculatedValue(),  //ADDRESS1
                        'cellPoiCity' => $worksheet->getCell('G' . $i)->getCalculatedValue(), //CITY
                        'cellPoiState' => $worksheet->getCell('H' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLat' => $worksheet->getCell('I' . $i)->getCalculatedValue(), //LATITUDEDEC
                        'cellPoiLong' => $worksheet->getCell('J' . $i)->getCalculatedValue(), //LONGITUDEDEC
                        'cellPoiEta' => $fechaEta, //ETA (FECHA INGRESO AL POI)                    
                    );
                    $data[$codigo]['pois'][$orden] = $pois;
                    if ($orden == 0) {
                        $cellFecha = $worksheet->getCell('M' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)
                        $pos = strpos($fechaEta, ' '); //buscamos el id del usuario
                        $horaEta0 = substr($fechaEta, $pos + 1);
                        // actualizamos la fecha de inicio del itinerario con la hora de la eta.                         
                        $fechaInicio = $cellFecha != '' ? gmdate('d/m/Y', Date::excelToTimestamp($cellFecha)) . " " . $horaEta0 : '';
                        $data[$codigo]['cellFechaInicio'] = $fechaInicio;
                    }
                }
            }
        }

        foreach ($data as $key => $dataIt) {
            if ($key !== '') {
                $template = new TemplateItinerario();
                $template->setOrganizacion($organizacion);
                $template->setNombre($key);
                $json_template = json_encode($data[$key], true);
                $template->setData($json_template);
                $template->setBloqueado(1);
                $this->em->persist($template);
            }
        }
        $this->em->flush();
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.       

        return $data;
    }

    private function validateHeaders($worksheet, $type): array 
    {
        $result = [
            'isValid' => true,
            'errors' => [],
            'headers' => []
        ];
        
        foreach (self::EXPECTED_HEADERS[$type] as $column => $expectedHeader) {
            $actualHeader = trim($worksheet->getCell($column . '1')->getValue());
            $result['headers'][$column] = [
                'expected' => $expectedHeader,
                'actual' => $actualHeader,
                'isValid' => (strtoupper($actualHeader) === strtoupper($expectedHeader))
            ];
            
            if (!$result['headers'][$column]['isValid']) {
                $result['isValid'] = false;
                $result['errors'][] = "Columna $column: se esperaba '$expectedHeader', se encontró '$actualHeader'";
            }
        }
        
        return $result;
    }
}
