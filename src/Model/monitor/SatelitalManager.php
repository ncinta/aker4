<?php

namespace App\Model\monitor;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Satelital;
use App\Entity\Organizacion as Organizacion;

class SatelitalManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($satelital)
    {
        if ($satelital instanceof Satelital) {
            return $this->em->getRepository('App:Satelital')->findOneBy(array('id' => $satelital->getId()));
        } else {
            return $this->em->getRepository('App:Satelital')->findOneBy(array('id' => $satelital));
        }
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Satelital')->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'asc'));
        } else {
            return $this->em->getRepository('App:Satelital')->findBy(array('organizacion' => $organizacion), array('nombre' => 'asc'));
        }
    }

    public function create(Organizacion $organizacion)
    {
        $satelital = new Satelital();
        $satelital->setOrganizacion($organizacion);

        return $satelital;
    }

    public function save(Satelital $satelital)
    {
        $this->em->persist($satelital);
        $this->em->flush();
        return $satelital;
    }

    public function delete(Satelital $satelital)
    {
        $this->em->remove($satelital);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $satelital = $this->find($id);
        if (!$satelital) {
            throw $this->createNotFoundException('Código de satelital no encontrado.');
        }
        return $this->delete($satelital);
    }
}
