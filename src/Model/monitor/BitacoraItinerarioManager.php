<?php

namespace App\Model\monitor;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BitacoraItinerario as BitacoraItinerario;
use App\Model\app\UtilsManager;

/**
 * Description of BitacoraItinerarioManager
 *
 * @author nicolas
 */
class BitacoraItinerarioManager
{

    protected $em;
    protected $utils;
    protected $isCliente = 0;

    public function __construct(EntityManagerInterface $em, UtilsManager $utils)
    {
        $this->em = $em;
        $this->utils = $utils;
    }

    public function getTipoEventos()
    {
        $bitacora = new BitacoraItinerario();
        return $bitacora->getArrayTipoEvento();
    }

    private function add($tipo_evento, $ejecutor, $itinerario, $data = null)
    {
        $bitacora = new BitacoraItinerario();
        $bitacora->setTipoEvento($tipo_evento);
        $bitacora->setItinerario($itinerario);

        //grabo el usuario si viene
        if (!is_null($ejecutor)) {
            if ($ejecutor instanceof $ejecutor) {
                $bitacora->setEjecutor($ejecutor);
            } else {
                $bitacora->setEjecutor($this->em->getRepository('App:Usuario')->find($ejecutor));
            }
        }
        $bitacora->setOrganizacion($itinerario != null ? $itinerario->getOrganizacion() : $ejecutor->getOrganizacion());

        if (!is_null($data)) {
            $bitacora->setData($data);
        }

        $this->em->persist($bitacora);
        $this->em->flush();
        return true;
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un centrocosto.
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $servicios
     */
    public function updateServicio($ejecutor, $itinerario, $servicios)
    {
        $arr_servicios = array();

        //  die('////<pre>' . nl2br(var_export(count($itinerario->getPois()), true)) . '</pre>////');
        foreach ($servicios as $servicio) {
            $nombre = $servicio->getNombre();
            if (count($servicio->getLogisticas()) > 0) {
                foreach ($servicio->getLogisticas() as $logistica) {
                    $nombre .= ' (Logistica ' . $logistica->getNombre() . ')';
                }
            } elseif ($servicio->getTransporte()) {
                $nombre .= ' (Transporte ' . $servicio->getTransporte()->getNombre() . ')';
            } else {
                $nombre .= ' (' . $servicio->getOrganizacion()->getNombre() . ')';
            }

            $arr_servicios[] =  $nombre;
        }

        $data = array(
            'servicios' => $arr_servicios
        );
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_SERVICIO_UPDATE, $ejecutor, $itinerario, $data);
    }


    /**
     * Registra en la bitacora cuando se asigna un servicio a un centrocosto.
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $pois
     */
    public function updateReferencia($ejecutor, $itinerario, $pois)
    {
        $arr_referencias = array();
        //  die('////<pre>' . nl2br(var_export(count($itinerario->getPois()), true)) . '</pre>////');
        foreach ($pois as $poi) {
            $arr_referencias[] = $poi->getReferencia()->getNombre();
        }

        $data = array(
            'referencias' => $arr_referencias
        );
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_REFERENCIA_UPDATE, $ejecutor, $itinerario, $data);
    }

    /**
     * Registra en la bitacora cuando se asigna un servicio a un centrocosto.
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $evento
     */
    public function updateEvento($ejecutor, $itinerario, $evento, $estado)
    {
        $data = array(
            'nombre' => $evento->getNombre(),
            'estado' => $estado,
        );



        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_EVENTO_UPDATE, $ejecutor, $itinerario, $data);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $servicios
     */
    public function updateCustodios($ejecutor, $itinerario, $servicios)
    {
        $arr_custodios = array();

        //  die('////<pre>' . nl2br(var_export(count($itinerario->getPois()), true)) . '</pre>////');
        foreach ($servicios as $servicio) {
            if ($servicio->getIsCustodio()) {
                $nombre = $servicio->getNombre();
                if (count($servicio->getLogisticas()) > 0) {
                    foreach ($servicio->getLogisticas() as $logistica) {
                        $nombre .= ' (Logistica ' . $logistica->getNombre() . ')';
                    }
                } elseif ($servicio->getTransporte()) {
                    $nombre .= ' (Transporte ' . $servicio->getTransporte()->getNombre() . ')';
                } else {
                    $nombre .= ' (' . $servicio->getOrganizacion()->getNombre() . ')';
                }

                $arr_custodios[] =  $nombre;
            }
        }

        $data = array(
            'custodios' => $arr_custodios
        );
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_CUSTODIO_UPDATE, $ejecutor, $itinerario, $data);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $usuarios
     */
    public function updateUsuarios($ejecutor, $itinerario, $usuarios)
    {
        $arr_usuarios = array();

        //  die('////<pre>' . nl2br(var_export(count($itinerario->getPois()), true)) . '</pre>////');
        foreach ($usuarios as $usuario) {
            $nombre = $usuario->getNombre();
            if ($usuario->getEmpresa()) {
                $nombre .= ' ( Cliente ' . $usuario->getEmpresa()->getNombre() . ')';
            } elseif ($usuario->getLogistica()) {
                $nombre .= ' (Logistica ' . $usuario->getLogistica()->getNombre() . ')';
            } elseif ($usuario->getTransporte()) {
                $nombre .= ' (Transporte ' . $usuario->getTransporte()->getNombre() . ')';
            } else {
                $nombre .= ' (' . $usuario->getOrganizacion()->getNombre() . ')';
            }

            $arr_usuarios[] =  $nombre;
        }

        //  die('////<pre>' . nl2br(var_export($arr_usuarios, true)) . '</pre>////');
        $data = array(
            'usuarios' => $arr_usuarios
        );
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_USUARIO_UPDATE, $ejecutor, $itinerario, $data);
    }
    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $usuarios
     */
    public function updateContactos($ejecutor, $itinerario, $contactos)
    {
        $arr_contactos = array();

        //  die('////<pre>' . nl2br(var_export(count($itinerario->getPois()), true)) . '</pre>////');
        foreach ($contactos as $contacto) {
            $nombre = $contacto->getNombre();
            if ($contacto->getEmpresa()) {
                $nombre .= ' ( Cliente ' . $contacto->getEmpresa()->getNombre() . ')';
            } elseif ($contacto->getLogistica()) {
                $nombre .= ' (Logistica ' . $contacto->getLogistica()->getNombre() . ')';
            } elseif ($contacto->getTransporte()) {
                $nombre .= ' (Transporte ' . $contacto->getTransporte()->getNombre() . ')';
            } else {
                $nombre .= ' (' . $contacto->getOrganizacion()->getNombre() . ')';
            }

            $arr_contactos[] =  $nombre;
        }

        $data = array(
            'contactos' => $arr_contactos
        );
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_CONTACTO_UPDATE, $ejecutor, $itinerario, $data);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     */
    public function newItinerario($ejecutor, $itinerario)
    {
        $data = array(
            'codigo' => $itinerario->getCodigo(),
            'organizacion' => $itinerario->getOrganizacion()->getNombre(),
            'fecha_inicio' => $itinerario->getFechaInicio()->format('d/m/Y H:i'),
            'fecha_fin' => $itinerario->getFechaFin()->format('d/m/Y H:i'),
            'cliente' => $itinerario->getEmpresa() ? $itinerario->getEmpresa()->getNombre() : '---',
            'logistica' => $itinerario->getLogistica() ? $itinerario->getLogistica()->getNombre() : '---',
            'nota' => $itinerario->getNota() ? $itinerario->getNota() : '---'
        );


        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_NEW, $ejecutor, $itinerario, $data);
    }


    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $itinerarioOld
     */
    public function updateItinerario($ejecutor, $itinerario, $itinerarioOld)
    {
        $logisticaOld = $itinerarioOld->getLogistica() ? $itinerarioOld->getLogistica()->getNombre() : '---'; //pueden ser null por eso las tengo que colocar aca
        $logistica = $itinerario->getLogistica() ? $itinerario->getLogistica()->getNombre() : '---';
        
        $empresaOld = $itinerarioOld->getEmpresa() ? $itinerarioOld->getEmpresa()->getNombre() : '---'; //pueden ser null por eso las tengo que colocar aca
        $empresa = $itinerario->getEmpresa() ? $itinerario->getEmpresa()->getNombre() : '---';

        $fechaInicioOld = $itinerarioOld->getFechaInicio() ? $itinerarioOld->getFechaInicio()->format('d/m/Y H:i') : '---';
        $fechaInicio = $itinerario->getFechaInicio() ? $itinerario->getFechaInicio()->format('d/m/Y H:i') : '---';
        
        $fechaFinOld = $itinerarioOld->getFechaFin() ? $itinerarioOld->getFechaFin()->format('d/m/Y H:i') : '---';
        $fechaFin = $itinerario->getFechaFin() ? $itinerario->getFechaFin()->format('d/m/Y H:i') : '---';

        $data = array(
            'Codigo' => $itinerarioOld->getCodigo() != $itinerario->getCodigo() ? $itinerarioOld->getCodigo() . ' a ' . $itinerario->getCodigo() : null,
            'Inicio' =>  $fechaInicioOld != $fechaInicio ? $fechaInicioOld . ' a ' .$fechaInicio: null,
            'Fin' => $fechaFinOld != $fechaFin ? $fechaFinOld . ' a ' . $fechaFin : null,
            'Cliente' => $itinerarioOld->getEmpresa() &&  $itinerarioOld->getEmpresa() != $itinerario->getEmpresa() ? $empresaOld . ' a ' . $empresa : null,
            'Logistica' => $itinerarioOld->getLogistica() &&  $itinerarioOld->getLogistica() != $itinerario->getLogistica() ? $logisticaOld . ' a ' . $logistica : null,
            'Nota' => $itinerarioOld->getNota() != $itinerario->getNota() ? $itinerarioOld->getNota() . ' a ' . $itinerario->getNota() : null,

        );
        


        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_UPDATE, $ejecutor, $itinerario, $data);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     */
    public function cerrar($ejecutor, $itinerario)
    {

        $data = array(
            'codigo' => $itinerario->getCodigo(),
            'organizacion' => $itinerario->getOrganizacion()->getNombre(),
            'fecha_inicio' => $itinerario->getFechaInicio()->format('d/m/Y H:i'),
            'fecha_fin' => $itinerario->getFechaFin()->format('d/m/Y H:i'),
            'cliente' => $itinerario->getEmpresa() ? $itinerario->getEmpresa()->getNombre() : '---',
            'logistica' => $itinerario->getLogistica() ? $itinerario->getLogistica()->getNombre() : '---',
            'nota' => $itinerario->getNota() ? $itinerario->getNota() : '---'
        );


        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_CERRAR, $ejecutor, $itinerario, $data);
    }


    /**
     * @param type $ejecutor
     * @param type $itinerario
     */
    public function delete($ejecutor, $itinerario)
    {

        $data = array(
            'codigo' => $itinerario->getCodigo(),
            'organizacion' => $itinerario->getOrganizacion()->getNombre(),
            'fecha_inicio' => $itinerario->getFechaInicio()->format('d/m/Y H:i'),
            'fecha_fin' => $itinerario->getFechaFin()->format('d/m/Y H:i'),
            'cliente' => $itinerario->getEmpresa()->getNombre(),
            'logistica' => $itinerario->getLogistica() ? $itinerario->getLogistica()->getNombre() : '---',
            'nota' => $itinerario->getNota() ? $itinerario->getNota() : '---'
        );


        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_DELETE, $ejecutor, $itinerario, $data);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $datos
     */
    public function updateEstado($ejecutor, $itinerario, $dato)
    {
        // dd($datos);

        /**$data = array(
            'codigo' => $dato['codigo'],
            'estado_old' => $dato['estado_old'],
            'estado' => $dato['tr_estado'],
            'fecha_inicio' => $dato['fecha_inicio'],
            'fecha_fin' => $dato['fecha_fin'],
            'nota' => $dato['nota'],
        );*/
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_ESTADO_UPDATE, $ejecutor, $itinerario, $dato);
    }

    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $datos
     */
    public function updateEstadoServicio($ejecutor, $itinerario, $dato)
    {

        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_SERVICIO_ESTADO_UPDATE, $ejecutor, $itinerario, $dato);
    }


    /**
     * @param type $ejecutor
     * @param type $itinerario
     * @param type $comentario
     */
    public function addComentario($ejecutor, $itinerario, $comentario)
    {
        $data = array('comentario' => $comentario);
        $this->add(BitacoraItinerario::EVENTO_ITINERARIO_COMENTARIO, $ejecutor, $itinerario, $data);
    }


    public function getRegistros($itinerario)
    {
        if ($itinerario) {
            return $this->em->getRepository('App:BitacoraItinerario')->findBy(array(
                'itinerario' => $itinerario->getId()
            ), array('created_at' => 'DESC'));
        }
        return null;
    }

    public function obtener($organizacion, $desde = null, $hasta = null, $tevento = null)
    {
        return $this->em->getRepository('App:BitacoraItinerario')->byOrganizacion($organizacion, $desde, $hasta, $tevento);
    }

    public function get($itinerario, $tevento = null)
    {
        return $this->em->getRepository('App:BitacoraItinerario')->get($itinerario, $tevento);
    }

    public function parse($registro)
    {
        $str = '';
        $data = $registro->getData();
        switch ($registro->getTipoEvento()) {
            case $registro::EVENTO_ITINERARIO_NEW:
                $str = sprintf('Se crea itinerario <b>%s</b>, con inicio <b>%s</b> y fin <b>%s</b>, con logística <b>%s</b> para el cliente <b>%s</b>. Nota: %s', $data['codigo'], $data['fecha_inicio'], $data['fecha_fin'], $data['logistica'], $data['cliente'], $data['nota']);
                break;
            case $registro::EVENTO_ITINERARIO_UPDATE:
                $str = $this->getStr($data);
                $str = sprintf('Se modifican los datos ' . $str);
                break;
            case $registro::EVENTO_ITINERARIO_SERVICIO_UPDATE:
                $str = sprintf('Se modifican los servicios, permanecen <b>%s</b>', count($data['servicios']) > 0 ?  implode(",", $data['servicios']) : 'ninguno');
                break;
            case $registro::EVENTO_ITINERARIO_REFERENCIA_UPDATE:
                $str = sprintf('Se  modifican los puntos de referencia, permanecen <b>%s</b>',  count($data['referencias']) > 0 ?  implode(",", $data['referencias']) : 'ninguno');
                break;
            case $registro::EVENTO_ITINERARIO_CUSTODIO_UPDATE:
                $str = sprintf('Se  modifican los custodios,  permanecen <b>%s</b>',  count($data['custodios']) > 0 ?  implode(",", $data['custodios']) : 'ninguno');
                break;

            case $registro::EVENTO_ITINERARIO_USUARIO_UPDATE:
                $str = sprintf('Se  modifican los usuarios,  permanecen <b>%s</b>',  count($data['usuarios']) > 0 ?  implode(",", $data['usuarios']) : 'ninguno');
                break;
            case $registro::EVENTO_ITINERARIO_CONTACTO_UPDATE:
                $str = sprintf('Se  modifican los contactos,  permanecen <b>%s</b>',  count($data['contactos']) > 0 ?  implode(",", $data['contactos']) : 'ninguno');
                break;
            case $registro::EVENTO_ITINERARIO_CERRAR:
                $str = sprintf('Se cierra itinerario <b>%s</b>, con inicio <b>%s</b> y fin <b>%s</b>, con logística <b>%s</b> para el cliente <b>%s</b>. Nota: %s',  $data['codigo'], $data['fecha_inicio'], $data['fecha_fin'], $data['logistica'], $data['cliente'], $data['nota']);
                break;
            case $registro::EVENTO_ITINERARIO_DELETE:
                $str = sprintf('Se elimina itinerario <b>%s</b>, con inicio <b>%s</b> y fin <b>%s</b>, con logística <b>%s</b> para el cliente <b>%s</b>. Nota: %s',  $data['codigo'], $data['fecha_inicio'], $data['fecha_fin'], $data['logistica'], $data['cliente'], $data['nota']);
                break;
            case $registro::EVENTO_ITINERARIO_ESTADO_UPDATE:
                //$str = sprintf('Se modifica el estado itinerario <b>%s</b>, con inicio <b>%s</b> y fin <b>%s</b>, de estado <b>%s</b> a  <b>%s</b>. Nota: %s',  $data['codigo'], $data['fecha_inicio'], $data['fecha_fin'], $data['estado_old'], $data['estado'], $data['nota']);
                $str = sprintf('Se modifica el estado itinerario <b>%s</b>. ', $data['codigo']);
                $str .= sprintf('<br><b>%s</b> -> <b>%s</b>', $data['estado_old'], $data['estado']);
                if (isset($data['fecha_inicio']))
                    $str .= sprintf('<br> Nuevo Inicio <b>%s</b>.', $data['fecha_inicio']->format('d-m-Y H:i'));
                if (isset($data['fecha_fin']))
                    $str .= sprintf('<br> Nuevo Fin <b>%s</b>.', $data['fecha_fin']->format('d-m-Y H:i'));
                $str .= sprintf('<br><i>%s</i>', $data['nota']);
                break;
            case $registro::EVENTO_ITINERARIO_SERVICIO_ESTADO_UPDATE:
                //$str = sprintf('Se modifica el estado itinerario <b>%s</b>, con inicio <b>%s</b> y fin <b>%s</b>, de estado <b>%s</b> a  <b>%s</b>. Nota: %s',  $data['codigo'], $data['fecha_inicio'], $data['fecha_fin'], $data['estado_old'], $data['estado'], $data['nota']);
                $str = sprintf('Se modifica el estado del servicio <b>%s</b>. ', $data['servicio']);
                $str .= sprintf('<br><b>%s</b> -> <b>%s</b>', $data['estado_old'], $data['estado']);
                break;

            case $registro::EVENTO_ITINERARIO_EVENTO_UPDATE:
                //   die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                // die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                $str = sprintf('Se modifica el evento  <b>%s</b>, con estado <b>%s</b>',  $data['nombre'], $data['estado']);
                break;
            case $registro::EVENTO_ITINERARIO_COMENTARIO:
                // die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                $str = sprintf('Se agrega comentario:  %s', $data['comentario']);
                break;

            default:

                break;
        }
        return $str;
    }

    private function getStr($data)
    {
        $str = '';
        foreach ($data as $key => $value) {
            if ($value != null) {
                $str .= $key . ': ' . $value . ', ';
            }
        }
        return $str;
    }
}
