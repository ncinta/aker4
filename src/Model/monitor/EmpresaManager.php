<?php

namespace App\Model\monitor;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Empresa;
use App\Entity\Organizacion as Organizacion;

class EmpresaManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find($empresa)
    {
        if ($empresa instanceof Empresa) {
            return $this->em->getRepository('App:Empresa')->findBy(array('id' => $empresa->getId()));
        } else {
            return $this->em->getRepository('App:Empresa')->find($empresa);
        }
    }

    public function findByNombre($nombre)
    {
        return $this->em->getRepository('App:Empresa')->findOneBy(array('nombre' => $nombre));
    }

    public function findAllByOrganizacion($organizacion)
    {
        if ($organizacion instanceof Organizacion) {
            return $this->em->getRepository('App:Empresa')->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'asc'));
        } else {
            return $this->em->getRepository('App:Empresa')->findBy(array('organizacion' => $organizacion), array('nombre' => 'asc'));
        }
    }

    /**
     * Busca las empresas que tiene asociadas el usuario segun su tipo de usuario.
     * Si el usuario logueado es un cliente, entonces solo debe ver a su cliente.
     */
    public function findByUsuario($usuario)
    {

        return $usuario->getEmpresa();
    }

    public function create(Organizacion $organizacion)
    {
        $empresa = new Empresa();
        $empresa->setOrganizacion($organizacion);

        return $empresa;
    }

    public function save(Empresa $empresa)
    {
        $this->em->persist($empresa);
        $this->em->flush();
        return $empresa;
    }

    public function delete(Empresa $empresa)
    {
        $this->em->remove($empresa);
        $this->em->flush();
        return true;
    }

    public function deleteById($id)
    {
        $empresa = $this->find($id);
        if (!$empresa) {
            throw $this->createNotFoundException('Código de empresa no encontrado.');
        }
        return $this->delete($empresa);
    }

    public function findByIds($ids)
    {
        $empresas = array();
        foreach ($ids as $id) {
            $empresas[] = $this->find($id);
        }
        return $empresas;
    }
}
