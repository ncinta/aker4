<?php

namespace App\Model\gpm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Entity\Proyecto;
use App\Entity\ActividadGpm;
use App\Entity\ActgpmServicio;
use App\Entity\ActgpmRefgpm;
use App\Entity\Personal;
use App\Entity\ResponsableGpm;
use App\Entity\Persona;
use App\Entity\Organizacion;
use App\Entity\ActividadManual;

/**
 * Description of ProyectoManager
 *
 * @author nicolas
 */
class ActividadManager
{

    protected $em;
    protected $userlogin;
    protected $utilsManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, UtilsManager $utilsManager)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->utilsManager = $utilsManager;
    }

    public function findByProyecto($proyecto)
    {
        return $this->em->getRepository('App:ActividadGpm')
            ->findBy(array('proyecto' => $proyecto->getId()), array('nombre' => 'ASC'));
    }

    public function find($id)
    {
        return $this->em->getRepository('App:ActividadGpm')
            ->findOneBy(array('id' => $id));
    }

    public function save($actividad, $data)
    {
        $now = new \DateTime();
        $opciones = null;
        $id = $actividad->getId() != null ? $actividad->getId() : 0;
        if ($data != null) {
            $inicio = $data['inicio'] != '' ? $data['inicio'] . ':00' : '00:00:00';
            $fin = $data['fin'] != '' ? $data['fin'] . ':00' : '00:00:00';
            $actividad->setNombre($data['nombre']);
            $actividad->setCodigo($data['codigo']);
            $actividad->setDescripcion($data['descripcion']);
            $actividad->setFechaInicio($data['fecha'] != '' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($data['fecha'] . ' ' . $inicio, false)) : null);
            $actividad->setFechaFin($data['fecha'] != '' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($data['fecha'] . ' ' . $fin, false)) : null);
        }

        if (isset($data['responsable'])) {
            $responsable = $this->em->getRepository('App:ResponsableGpm')
                ->findOneBy(array('id' => intval($data['responsable'])));
            $actividad->setResponsable($responsable);
        }
        if (isset($data['contratista'])) {
            $contratista = $this->em->getRepository('App:Contratista')
                ->findOneBy(array('id' => intval($data['contratista'])));
            $actividad->setContratista($contratista);
        }


        if (isset($data['nivelTension'])) {
            if (intval($data['nivelTension']) == 1) {
                $opciones['NIVEL_TENSION'] = array('key' => 1, 'label' => 'BT', 'text' => 'BAJA TENSION');
            } elseif (intval($data['nivelTension']) == 2) {
                $opciones['NIVEL_TENSION'] = array('key' => 2, 'label' => 'MT', 'text' => 'MEDIA TENSION');
            } elseif (intval($data['nivelTension']) == 3) {
                $opciones['NIVEL_TENSION'] = array('key' => 3, 'label' => 'AT', 'text' => 'ALTA TENSION');
            }
        }

        if (isset($data['corteEnergia'])) {
            $opciones['CORTE_ENERGIA'] = true;
        }
        $actividad->setOpciones($opciones);

        if (isset($data['anchoTrabajo'])) {
            $data['anchoTrabajo'] = str_replace(',', '.', $data['anchoTrabajo']);
            $actividad->setAnchoTrabajo(floatval($data['anchoTrabajo']));
        }



        if ($actividad->getFechaInicio() != null && $actividad->getFechaFin() != null) {
            if ($now >= $actividad->getFechaInicio() && $now < $actividad->getFechaFin()) {
                $actividad->setEstado(1); //en curso
            } elseif ($now > $actividad->getFechaFin()) {
                $actividad->setEstado(2); //finalizado
            } elseif ($now < $actividad->getFechaInicio()) {
                $actividad->setEstado(0); //programado
            }
        } else {
            $actividad->setEstado(0); //programado
        }

      
        $motivo = isset($data['motivos']) ? $data['motivos'] : null;
        $actividad->setMotivo($this->em->getRepository('App:MotivoActividadGpm')
            ->findOneBy(array('id' => intval($motivo))));

        $ok = $this->removeAllServicio($actividad);
        if (isset($data['actgpmServicios'])) {
            foreach ($data['actgpmServicios'] as $serv) {
                $servicio = $this->em->getRepository('App:Servicio')
                    ->findOneBy(array('id' => intval($serv)));

                $actServicio = new ActgpmServicio();
                $actServicio->setActividad($actividad);
                $actServicio->setServicio($servicio);
                $this->em->persist($actServicio);
                $actividad->addActServicios($actServicio);
            }
        }

        $ok = $this->removeAllReferencia($actividad);
        if (isset($data['actgpmRefgpm'])) {
            foreach ($data['actgpmRefgpm'] as $ref) {
                $referencia = $this->em->getRepository('App:ReferenciaGpm')
                    ->findOneBy(array('id' => intval($ref)));
                $actReferencia = new ActgpmRefgpm();
                $actReferencia->setActividad($actividad);
                $actReferencia->setReferencia($referencia);
                $this->em->persist($actReferencia);
                $actividad->addActgpmRefgpm($actReferencia);
            }
        }

        if (isset($data['personal'])) {
            $ok = $this->removeAllPersonal($actividad);
          
            foreach ($data['personal'] as $value) {
                $persona = $this->em->getRepository('App:Persona')
                    ->findOneBy(array('id' => intval($value)));
                $exist = $this->em->getRepository('App:Personal')->ifExist($id, $persona->getId());
                if ($exist === null) { //existe, entonces actualizamos las horas nomas
                    $personal = new Personal();
                    $personal->setActividad($actividad);
                    $personal->setPersona($persona);
                    $actividad->addPersonal($personal);
                    $this->em->persist($personal);
                }
            }
        }
        $this->em->persist($actividad);
        $this->em->flush();

        return $actividad;
    }

    public function create($proyecto)
    {
        $actividad = new ActividadGpm();
        $actividad->setNombre($proyecto->getNombre() . ', Actividad ' . strval(count($proyecto->getActividades()) + 1));
        $actividad->setProyecto($proyecto);
        $actividad->setResponsable($proyecto->getResponsable());
        $actividad->setContratista($proyecto->getContratista());
        $this->em->persist($actividad);
        $this->em->flush();
        return $actividad;
    }

    public function persist($actividad)
    {
        $this->em->persist($actividad);
        $this->em->flush();
        return $actividad;
    }

    public function removeAllReferencia($actividad)
    {
        if ($actividad->getActgpmRefgpm() != null) { //borramos las servicios para cargarlas de nuevo porque es un edit
            foreach ($actividad->getActgpmRefgpm() as $refact) { //borramos las referencias para cargarlas de nuevo porque es un edit
                if ($refact->getReferencia() && $refact->getReferencia()->getUnica() != 1) { //que no sea unica. sino va a a eliminar la unica
                    $this->em->remove($refact);
                }
            }
        }
        return true;
    }

    public function removeAllServicio($actividad)
    {
        if ($actividad->getActgpmServicios() != null) { //borramos las servicios para cargarlas de nuevo porque es un edit
            foreach ($actividad->getActgpmServicios() as $serv) { //borramos las servicios para cargarlas de nuevo porque es un edit
                $this->em->remove($serv);
            }
        }
        return true;
    }

    public function removeAllPersonal($actividad)
    {
        if ($actividad->getPersonal() != null) { //borramos las servicios para cargarlas de nuevo porque es un edit
            foreach ($actividad->getPersonal() as $personal) { //borramos las servicios para cargarlas de nuevo porque es un edit
                $this->em->remove($personal);
                $this->em->flush();
            }
        }
        return true;
    }

    public function cambiarEstado(ActividadGpm $actividad, $estado)
    {
        $actividad->setEstado($estado);
        $this->em->persist($actividad);
        $this->em->flush();
        return $actividad;
    }

    public function findAllResponsablesQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App:ResponsableGpm', 'c')
            ->join('c.organizacion', 'o')
            ->join('c.persona', 'p')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }
      
        return $query->getQuery()->getResult();
    }

    public function saveResponsable(Organizacion $organizacion, Persona $persona)
    {
        $responsable = new ResponsableGpm();
        $responsable->setOrganizacion($organizacion);
        $responsable->setPersona($persona);

        $this->em->persist($responsable);
        $this->em->flush();
        return $responsable;
    }

    public function exist($codigo)
    {
        return $this->em->getRepository('App:ActividadGpm')
            ->ifExist($codigo);
    }

    public function addIngresoManual(ActividadGpm $actividad, $data)
    {
        $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($data['fecha'] . ' 00:00:00'));
        $tiempo = intval(substr($data['tiempo'], 0, 2)) * 60 + intval(substr($data['tiempo'], 3, 4));
        $hectareas = floatval(str_replace(',', '.', $data['hectarea']));
        $distancia = intval(floatval($data['distancia']) * 1000); //en metros

        $ingreso = new ActividadManual();
        $ingreso->setActividad($actividad);
        $ingreso->setFecha($fecha);
        $ingreso->setTiempo($tiempo);
        $ingreso->setDistancia($distancia);
        $ingreso->setHectareas($hectareas);
        $ingreso->setChofer($data['chofer']);
        $ingreso->setOperario($data['operario']);
        $actividad->addIngreso($ingreso);
        $this->em->persist($ingreso);
        $this->em->persist($actividad);
        $this->em->flush();

        return $actividad;
      
    }
}
