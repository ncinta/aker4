<?php

namespace App\Model\gpm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GmapsManager;
use App\Model\gpm\ActividadManager;
use App\Entity\ReferenciaGpm;
use App\Entity\ActgpmRefgpm;
use App\Entity\Organizacion;

/**
 * Description of ReferenciaGpmManager
 *
 * @author nicolas
 */
class ReferenciaGpmManager {

    protected $em;
    protected $userlogin;
    protected $organizacionManager;
    protected $gmapsManager;
    protected $actividadManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, UtilsManager $utilsManager,
            OrganizacionManager $organizacionManager, GmapsManager $gmapsManager, ActividadManager $actividadManager) {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->organizacionManager = $organizacionManager;
        $this->utilsManager = $utilsManager;
        $this->gmapsManager = $gmapsManager;
        $this->actividadManager = $actividadManager;
    }

    public function findAll(Organizacion $organizacion) {
        return $this->em->getRepository('App:ReferenciaGpm')
                        ->findBy(array('propietario' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function find($id) {
        return $this->em->getRepository('App:ReferenciaGpm')
                        ->findOneBy(array('id' => $id));
    }

    public function findAllQuery($organizacion, $search) {
        $query = $this->em->createQueryBuilder()
                ->select('r')
                ->from('App:ReferenciaGpm', 'r')
                ->join('r.organizaciones', 'o')
                ->where('o.id = :organizacion')
                ->setParameter('organizacion', $organizacion->getId())
                ->addOrderBy('r.nombre', 'ASC');

        if ($search != '') {
            $query
                    ->andWhere($query->expr()->like('r.nombre', ':nombre'))
                    ->setParameter('nombre', '%' . $search . '%');
        }
        return $query->getQuery()->getResult();
    }

    public function findByProyecto($proyecto) {
        $referencias = array();
        $actref = $this->em->getRepository('App:ActgpmRefgpm')
                ->findByProyecto($proyecto);
        foreach ($actref as $value) {
            $referencias[] = $value->getReferencia();
        }
        return $referencias;
    }

    public function create($padre = null) {
        $org = $this->organizacionManager->find($padre);
        if ($org) {
            //setea la latitud y longitud inicial para la referencia.
            if ($org->getLatitud() != 0) {
                $latitud = $org->getLatitud();
                $longitud = $org->getLongitud();
            } else {
                $latitud = $this->userlogin->getLatitud();
                $longitud = $this->userlogin->getLongitud();
            }
            $referencia = new ReferenciaGpm();
            $referencia->setLatitud($latitud);
            $referencia->setLongitud($longitud);
            $referencia->addOrganizacion($org);
            $referencia->setPropietario($org);
            $org->addReferenciaGpm($referencia);
            // $this->em->persist($org);
            return $referencia;
        } else {
            return null;
        }
    }

    public function save(ReferenciaGpm $referencia, $data) {
        $referencia->setTipoReferencia($this->em->getRepository('App:TipoReferencia')
                        ->findOneBy(array('id' => intval($data['tipoReferencia'])), array('nombre' => 'ASC')));
        $referencia->setCategoria($this->em->getRepository('App:Categoria')
                        ->findOneBy(array('id' => intval($data['categoria'])), array('nombre' => 'ASC')));
        $referencia->setNombre($data['nombre']);
        $referencia->setPathIcono($data['pathIcono']);
        $referencia->setPoligono($data['poligono']);
        $referencia->setArea(isset($data['area']) ? $data['area'] : 0.0);
        $referencia->setVisibilidad(true);
        $referencia->setColor(isset($data['color']) ? $data['color'] : '#ffffff');
        $referencia->setTransparencia(isset($data['transparencia']) ? $data['transparencia'] : 0.5);
        $referencia->setClase(2); // clase poligono
        //hago esto para traer el punto central del poligono.
        $puntos = $this->gmapsManager->parsePoligono($referencia);
        $referencia->setLatitud($puntos['latitud']);
        $referencia->setLongitud($puntos['longitud']);
        $referencia->setUnica(intval($data['unica']));
        $this->em->persist($referencia);

        if (intval($data['actividad'])) { //es referencia unica. la agregamos a la actividad
            $actividad = $this->actividadManager->find(intval($data['actividad']));
            $actRef = new ActgpmRefgpm();
            $actRef->setActividad($actividad);
            $actRef->setReferencia($referencia);
            $this->em->persist($actRef);
            $referencia->addActgpmRefgpm($actRef);
            $this->em->persist($referencia);
            $actividad->addActgpmRefgpm($actRef);
            $this->em->persist($actividad);
            
        }
        $this->em->flush();
        // $this->bitacora->updateReferencia($this->userlogin->getUser(),$this->userlogin->getUser()->getOrganizacion(),$referencia, $action);
        return $referencia;
    }

}
