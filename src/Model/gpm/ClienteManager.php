<?php

namespace App\Model\gpm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Entity\Cliente;
use App\Entity\Organizacion;

/**
 * Description of ClienteManager
 *
 * @author nicolas
 */
class ClienteManager
{

    protected $em;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
    }

    public function create(Organizacion $organizacion)
    {
        $cliente = new Cliente();
        $cliente->setOrganizacion($organizacion);

        return $cliente;
    }

    public function save(Cliente $cliente)
    {
        $this->em->persist($cliente);
        $this->em->flush();
        return $cliente;
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Cliente')
            ->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function find($cliente)
    {
        return $this->em->getRepository('App:Cliente')
            ->findOneBy(array('id' => $cliente));
    }

    public function findAllQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App:Cliente', 'c')
            ->join('c.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('c.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('c.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }

        return $query->getQuery()->getResult();
    }
}
