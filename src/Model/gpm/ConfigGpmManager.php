<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\gpm;

use App\Entity\ConfigGpm;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of ConfigGpmManager
 *
 * @author nicolas
 */
class ConfigGpmManager
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create($organizacion)
    {
        return !is_null($organizacion->getConfigGpm()) ? $organizacion->getConfigGpm() : new ConfigGpm();
    }

    public function save($config)
    {
        $this->em->persist($config);
        $this->em->persist($config->getOrganizacion());
        $this->em->flush();
        return $config;
    }
}
