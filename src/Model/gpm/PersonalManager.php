<?php

namespace App\Model\gpm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Entity\Persona;
use App\Entity\Personal;
use App\Entity\ActividadPersonal;
use App\Entity\Organizacion;

/**
 * Description of PersonalManager
 *
 * @author nicolas
 */
class PersonalManager
{

    protected $em;
    protected $userlogin;
    protected $proyectoManager;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, ProyectoManager $proyectoManager)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->proyectoManager = $proyectoManager;
    }

    public function findAllPersonas($contratista)
    {
        return $this->em->getRepository('App:Persona')
            ->findBy(array('contratista' => $contratista->getId()));
    }

    public function findPersona($id)
    {
        return $this->em->getRepository('App:Persona')
            ->findOneBy(array('id' => $id));
    }

    public function setHorasPersonal($personas, $actividad = null)
    {

        foreach ($personas as $personal) {
            $horas = 0;
            if ($personal->getActividades() != null) {
                foreach ($personal->getActividades() as $trabajo) {
                    if ($trabajo->getActividad() != null) {
                        if ($trabajo->getActividad()->getEstado() == 1) { //esta en curso
                            if ($actividad != null && $trabajo->getActividad()->getId() != $actividad->getId()) {
                                $horas += $trabajo->getHoras();
                            }
                        }
                    }
                }
                $personal->setHorasTrabajoTemp($personal->getHorasTrabajoTemp() + $horas);
            } else {
                $personal->setHorasTrabajoTemp($horas);
            }
        }


        return true;
    }

    public function create($data, $contratista = null)
    {
        $persona = new Persona();
        if ($contratista !== null) { //es un contratista, si pasa de largo es responsable
            $persona->setContratista($contratista);
        }
        $persona->setNombre($data['nombre']);
        $persona->setTelefono($data['telefono']);
        $persona->setDocumento($data['documento']);
        $persona->setCuit($data['cuit']);
        $this->em->persist($persona);
        $this->em->flush();
        return $persona;
    }


    public function findAllQuery($contratista, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App:Persona', 'p')
            ->join('p.contratista', 'c')
            ->where('c.id = :contratista')
            ->setParameter('contratista', $contratista->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }
        //     die('////<pre>' . nl2br(var_export(count($query->getQuery()->getResult()), true)) . '</pre>////');
        return $query->getQuery()->getResult();
    }
}
