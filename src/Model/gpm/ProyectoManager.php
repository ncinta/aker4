<?php

namespace App\Model\gpm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Entity\Proyecto;
use App\Entity\ResponsableGpm;
use App\Entity\Persona;
use App\Entity\Organizacion;

/**
 * Description of ProyectoManager
 *
 * @author nicolas
 */
class ProyectoManager
{

    protected $em;
    protected $userlogin;

    public function __construct(EntityManagerInterface $em, UserLoginManager $userlogin, UtilsManager $utilsManager)
    {
        $this->em = $em;
        $this->userlogin = $userlogin;
        $this->utilsManager = $utilsManager;
    }

    public function findAll($organizacion)
    {
        return $this->em->getRepository('App:Proyecto')
            ->findBy(array('organizacion' => $organizacion->getId()), array('nombre' => 'ASC'));
    }

    public function find($id)
    {
        return $this->em->getRepository('App:Proyecto')
            ->findOneBy(array('id' => $id));
    }

    public function exist($proyecto)
    {
        return $this->em->getRepository('App:Proyecto')
            ->ifExist($proyecto);
    }

    public function obtener($filtro)
    {
        
        return $this->em->getRepository('App:Proyecto')->filter($filtro);
    }

    public function save($proyecto)
    {
        $desde = $proyecto->getFechaInicio() != null ? $proyecto->getFechaInicio() : null;
        $hasta = $proyecto->getFechaFin() != null ? $proyecto->getFechaFin() : null;
        $proyecto->setFechaInicio($desde != null ? new \DateTime($this->utilsManager->datetime2sqltimestamp($desde, true)) : null);
        $proyecto->setFechaFin($hasta != null ? new \DateTime($this->utilsManager->datetime2sqltimestamp($hasta, false)) : null);
        $this->em->persist($proyecto);
        $this->em->flush();
        return $proyecto;
    }

    public function create($organizacion)
    {
        $proyecto = new Proyecto();
        $proyecto->setOrganizacion($organizacion);
        return $proyecto;
    }

    public function getProgressbar($proyecto)
    {
        $countActTotales = $countActFinalizadas = 0;
        if ($proyecto != null && $proyecto->getActividades() != null) {
            foreach ($proyecto->getActividades() as $actividad) {
                if ($actividad->getEstado() > 1) {
                    $countActFinalizadas++;
                }
                $countActTotales++;
            }
        }

        return array('progreso' => $countActTotales > 0 ? round(($countActFinalizadas * 100) / $countActTotales) : 0, 'countActTotales' => $countActTotales, 'countActFinalizadas' => $countActFinalizadas);
    }

    public function updateEstadoACtividades($proyectos)
    {
        $now = new \DateTime();
        foreach ($proyectos as $proyecto) {
            if ($proyecto->getFechaInicio() != null && $proyecto->getFechaFin() != null) {
                if ($now >= $proyecto->getFechaInicio() && $now <= $proyecto->getFechaFin()) {
                    $proyecto->setEstado(1); //en curso
                } elseif ($now > $proyecto->getFechaFin()) {
                    $proyecto->setEstado(2); //finalizado
                }
            } else {
                $proyecto->setEstado(0); //programado
            }
            if ($proyecto->getActividades()) {
                foreach ($proyecto->getActividades() as $actividad) {
                    if ($actividad->getFechaInicio() != null && $actividad->getFechaFin() != null) {
                        if ($now >= $actividad->getFechaInicio() && $now < $actividad->getFechaFin()) {
                            $actividad->setEstado(1); //en curso
                        } elseif ($now > $actividad->getFechaFin()) {
                            $actividad->setEstado(2); //finalizado
                        }
                    } else {
                        $actividad->setEstado(0); //programado
                    }
                    $this->em->persist($actividad);
                }
            }
            $this->em->persist($proyecto);
        }
        $this->em->flush();
    }

    public function findAllResponsablesQuery($organizacion, $search)
    {
        $query = $this->em->createQueryBuilder()
            ->select('c')
            ->from('App:ResponsableGpm', 'c')
            ->join('c.organizacion', 'o')
            ->join('c.persona', 'p')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId())
            ->addOrderBy('p.nombre', 'ASC');

        if ($search != '') {
            $query
                ->andWhere($query->expr()->like('p.nombre', ':nombre'))
                ->setParameter('nombre', '%' . $search . '%');
        }
        //     die('////<pre>' . nl2br(var_export(count($query->getQuery()->getResult()), true)) . '</pre>////');
        return $query->getQuery()->getResult();
    }

    public function saveResponsable(Organizacion $organizacion, Persona $persona)
    {
        $responsable = new ResponsableGpm();
        $responsable->setOrganizacion($organizacion);
        $responsable->setPersona($persona);

        $this->em->persist($responsable);
        $this->em->flush();
        return $responsable;
    }

    public function findResponsable($id)
    {
        return $this->em->getRepository('App:ResponsableGpm')
            ->findOneBy(array('id' => $id));
    }
}
