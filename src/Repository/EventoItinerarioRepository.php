<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of EventoItinerarioRepository
 *
 * @author nicolas
 */
class EventoItinerarioRepository extends EntityRepository
{


    public function existInIt($itinerario, $codename)
    {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                    ->select('e')
                    ->from('App:EventoItinerario', 'e')
                    ->join('e.itinerario', 'i')
                    ->join('e.tipoEvento', 't')
                    ->where('i.id = ?1 AND t.codename = ?2')
                    ->setParameter('1', $itinerario->getId())
                    ->setParameter('2', $codename);

            return $query->getQuery()->getOneOrNullResult();
    }


}