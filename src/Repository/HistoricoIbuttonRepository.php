<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of HistoricoIbuttonRepository
 *
 * @author nicolas
 */
class HistoricoIbuttonRepository extends EntityRepository
{

    public function findByIbutton($ibutton)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:HistoricoIbutton', 'h')
            ->join('h.ibutton', 'i')
            ->where('i.id = ?1')
            ->setParameter('1', $ibutton->getId());

        return $query->getQuery()->getResult();
    }

    public function findByIbuttonFecha($ibutton, $fecha, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:HistoricoIbutton', 'h')
            ->join('h.ibutton', 'i')
            ->where('h.desde <= ?1')
            ->andWhere('h.hasta >= ?2')
            ->andWhere('i.id = ?3')
            ->setParameter('1', $fecha)
            ->setParameter('2', $hasta)
            ->setParameter('3', $ibutton->getId());

        return $query->getQuery()->getOneOrNullResult();
    }

    public function findByIbuttonChoferFecha($ibutton, $chofer, $fecha, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:HistoricoIbutton', 'h')
            ->join('h.ibutton', 'i')
            ->join('h.chofer', 'c')
            ->where('h.desde <= ?1')
            ->andWhere('h.hasta >= ?2')
            ->andWhere('i.id = ?3')
            ->andWhere('c.id = ?4')
            ->setParameter('1', $fecha)
            ->setParameter('2', $hasta)
            ->setParameter('3', $ibutton)
            ->setParameter('4', $chofer);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function findByChofer($chofer)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:HistoricoIbutton', 'h')
            ->join('h.chofer', 'c')
            ->where('c.id = ?1')
            ->setParameter('1', $chofer->getId());

        return $query->getQuery()->getResult();
    }
}
