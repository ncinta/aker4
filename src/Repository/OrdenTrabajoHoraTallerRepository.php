<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class OrdenTrabajoHoraTallerRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:OrdenTrabajoHoraTaller', 'p')
            ->join('p.horaTaller', 'h')
            ->join('h.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        return $query;
    }

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($organizacion, $orderBy)->getQuery()->getResult();
    }
}
