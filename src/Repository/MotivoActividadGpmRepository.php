<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ActgpmRefgpmRepository
 *
 * @author nicolas
 */
class MotivoActividadGpmRepository extends EntityRepository
{


    public function getQueryByOrganizacion($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('m')
            ->from('App:MotivoActividadGpm', 'm')
            ->join('m.organizacion', 'org')
            ->where('org.id = :id') //que traiga las que no son unicas
            ->setParameter('id', $organizacion->getId());
        $query->addOrderBy('m.' . 'nombre', 'ASC');
      
        return $query;
    }
}
