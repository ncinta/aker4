<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of GrupoReferenciaRepository
 *
 * @author yesica
 */
class GrupoReferenciaRepository extends EntityRepository
{

    function getQueryGruposDisponibles($organizacion, array $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('App:GrupoReferencia', 'g')
            ->join('g.organizaciones', 'org')
            ->where('org.id = :id')
            ->orderBy('g.nombre')
            ->setParameter('id', $organizacion->getId());

        if ($orderBy) {
            foreach ($orderBy as $key => $value) {
                $query->addOrderBy('r.' . $key, $value);
            }
        }
        return $query;
    }

    /*
     * Trae todos los grupos asociados a la organizacion.
     */
    function findAllGruposReferencias($organizacion, array $orderBy = null)
    {
        return $this->getQueryGruposDisponibles($organizacion, $orderBy)
            ->getQuery()
            ->getResult();
    }

    function ifExist($grupo, $organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('App:GrupoReferencia', 'g')
            ->join('g.organizaciones', 'org')
            ->where('org.id = :idorg and g.id = :idgrp')
            ->setParameters(array(
                'idorg' => $organizacion->getId(),
                'idgrp' => $grupo->getId(),
            ));
        $result = $query->getQuery()->getResult();
        return !(is_null($result) || count($result) == 0);
    }

    public function findAllByUsuario($usuario = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT g FROM App:GrupoReferencia g
            JOIN g.usuarios u
            WHERE u.id = :id 
            ORDER BY g.nombre
            ')->setParameter('id', $usuario->getId());
        return $query->getResult();
    }

    function findGruposSinAsignar2Usuario($usuario, $idsSelect)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('App:GrupoReferencia', 'g')
            ->join('g.organizaciones', 'o')
            ->where('o.id = ?1 and g.id NOT IN (?2)')
            ->orderBy('g.nombre')
            ->setParameter(1, $usuario->getOrganizacion()->getId())
            ->setParameter(2, $idsSelect);
        return $query->getQuery()->getResult();
    }

    /*
     * Trae un usuario asociado
     */

    function findUsuario($grupo, $usuario)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT s FROM App:GrupoReferencia s
            JOIN s.usuarios u
            WHERE u.id = :idU AND s.id = :idG')
            ->setParameters(array(
                'idU' => $usuario->getId(),
                'idG' => $grupo->getId()
            ));
        return $query->getResult();
    }
}
