<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of IndiceServicioRepository
 *
 * @author nicolas
 */
class IndiceServicioRepository extends EntityRepository
{

    public function getLastInsert($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceServicio', 'i')
            ->where('i.servicio = :serv')
            ->addOrderBy('i.fecha', 'DESC')
            ->setMaxResults(1)
            ->setParameter('serv', $servicio);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getIndicesServicios($servicios, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceServicio', 'i')
            ->where('i.servicio in (?1)')
            ->setParameter('1', $servicios)
            ->addOrderBy('i.fecha', 'ASC');
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('i.fecha >= :desde');
            $query->andWhere('i.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }
        return $query->getQuery()->getResult();
    }
}
