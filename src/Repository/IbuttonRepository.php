<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class IbuttonRepository extends EntityRepository
{

    public function findByOrg($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Ibutton', 'i')
            ->where('i.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('i.id', 'DESC');
        }
        return $query->getQuery()->getResult();
    }

    public function findByChofer($chofer)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Ibutton', 'i')
            ->join('i.chofer', 'c')
            ->where('c.id = id')
            ->setParameter('id', $chofer->getId());

        return $query->getQuery()->getOneOrNullResult();
    }
}
