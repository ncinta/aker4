<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ActividadRepository extends EntityRepository
{

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('a')
            ->from('App:Actividad', 'a')
            ->join('a.mantenimiento', 'm')
            ->join('m.organizaciones', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('a.nombre', 'ASC');
        } else {
            foreach ($orderBy as $k => $order) {
                $query->addOrderBy($k, $order);
            }
        }
      
        return $query->getQuery()->getResult();
    }
}
