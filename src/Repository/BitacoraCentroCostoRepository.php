<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class BitacoraCentroCostoRepository extends EntityRepository
{

    public function byOrganizacion($organizacion, $desde = null, $hasta = null, $evento_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:BitacoraCentroCosto', 'h')
            ->where('h.organizacion = :org')
            ->addOrderBy('h.created_at', 'ASC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.created_at >= :desde');
            $query->andWhere('h.created_at <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }
       
        if (!is_null($evento_id) && $evento_id != 0) {
            $query->andWhere('h.tipo_evento = :tev')->setParameter('tev', $evento_id);
        }

        return $query->getQuery()->getResult();
    }


    public function get($centrocosto, $evento = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:BitacoraCentroCosto', 'h')
            ->where('h.centrocosto = :centrocosto')
            ->addOrderBy('h.created_at', 'ASC')
            ->setParameter('centrocosto', $centrocosto->getId());

        if (!is_null($evento)) {
            $query->andWhere('h.tipo_evento = :evento');
            $query->setParameter('evento', $evento);
        }

        return $query->getQuery()->getResult();
    }
}
