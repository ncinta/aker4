<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ServicioTanqueRepository extends EntityRepository
{

    public function findMediciones($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->orderBy('t.porcentaje')
            ->setParameter(1, $servicio->getId());
        return $query->getQuery()->getResult();
    }

    /**
     * Devuelve la medicion menor que se encuentra registrada en la tabla de mediciones
     * @param type $servicio
     * @return type $serviciotanque
     */
    public function getMinimo($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->orderBy('t.porcentaje')
            ->setMaxResults(1)
            ->setParameter(1, $servicio->getId());

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getMenor($servicio, $porcentaje)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->andWhere('t.porcentaje < ?2')
            ->orderBy('t.porcentaje', 'DESC')
            ->setMaxResults(1)
            ->setParameter(1, $servicio->getId())
            ->setParameter(2, $porcentaje);

        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * Devuelve la medicion mayor que se encuentra registra en la tabla de mediciones
     * @param type $servicio
     * @return type serviciotanque
     */
    public function getMaximo($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->orderBy('t.porcentaje', 'DESC')
            ->setMaxResults(1)
            ->setParameter(1, $servicio->getId());
        return $query->getQuery()->getOneOrNullResult();
    }

    public function getMayor($servicio, $porcentaje)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->andWhere('t.porcentaje > ?2')
            ->orderBy('t.porcentaje', 'ASC')
            ->setMaxResults(1)
            ->setParameter(1, $servicio->getId())
            ->setParameter(2, $porcentaje);
        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * Devuelve la medicion exacta en caso de que exista.
     * @param type $servicio
     * @return type serviciotanque
     */
    public function getExacto($servicio, $porcentaje)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->andWhere('t.porcentaje = ?2')
            ->setParameter(1, $servicio->getId())
            ->setParameter(2, $porcentaje);
        return $query->getQuery()->getOneOrNullResult();
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:ServicioTanque', 'm')
            ->where('m.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }
}
