<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class EstadoRepository extends EntityRepository
{

    public function getDefault()
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Estado', 'p')
            ->where('p.isDefault = true');
        return $query->getQuery()->getOneOrNullResult();
    }

    public function getCerrado()
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Estado', 'p')
            ->where('p.isCerrado = true');
        return $query->getQuery()->getOneOrNullResult();
    }
}
