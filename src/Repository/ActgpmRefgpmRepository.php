<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ActgpmRefgpmRepository
 *
 * @author nicolas
 */
class ActgpmRefgpmRepository extends EntityRepository
{

        public function ifExist($actividad, $referencia)
        {
                if ($actividad != 0) {
                        $em = $this->getEntityManager();
                        $query = $em->createQueryBuilder()
                                ->select('f')
                                ->from('App:ActgpmRefgpm', 'f')
                                ->join('f.actividad', 'a')
                                ->join('f.referencia', 's')
                                ->where('a.id= :actividad')
                                ->andWhere('s.id= :referencia')
                                ->setParameter('actividad', $actividad)
                                ->setParameter('referencia', $referencia);
                        $result = $query->getQuery()->getOneOrNullResult();
                        return $result;
                } else {
                        return null; //la actividad es nueva
                }
        }

        public function findByActividad($actividad)
        {

                $em = $this->getEntityManager();
                $query = $em->createQueryBuilder()
                        ->select('f')
                        ->from('App:ActgpmRefgpm', 'f')
                        ->join('f.actividad', 'a')
                        ->where('a.id= :actividad')
                        ->setParameter('actividad', $actividad);

                $result = $query->getQuery()->getResult();
                return $result;
        }

        public function findByProyecto($proyecto)
        {

                $em = $this->getEntityManager();
                $query = $em->createQueryBuilder()
                        ->select('f')
                        ->from('App:ActgpmRefgpm', 'f')
                        ->join('f.actividad', 'a')
                        ->join('a.proyecto', 'p')
                        ->where('p.id= :proyecto')
                        ->setParameter('proyecto', $proyecto);

                $result = $query->getQuery()->getResult();
                return $result;
        }
}
