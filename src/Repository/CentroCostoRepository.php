<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Prestacion;

/**
 * CentroCostoRepository
 */
class CentroCostoRepository extends EntityRepository
{

    public function findByOrg($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:CentroCosto', 'i')
            ->where('i.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('i.nombre', 'ASC');
        }
        return $query->getQuery()->getResult();
    }



}
