<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ActividadGpmRepository extends EntityRepository
{

    public function ifExist($codigo)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('a')
            ->from('App:ActividadGpm', 'a')
            ->where('a.codigo = :codigo')
            ->setParameter('codigo', $codigo);
        $result = $query->getQuery()->getOneOrNullResult();
        //  die('////<pre>' . nl2br(var_export($nombre.' '.$codigo, true)) . '</pre>////');
        return $result;
    }
}
