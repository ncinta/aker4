<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PanicoRepository extends EntityRepository
{

    public function obtener($organizacion, $desde = null, $hasta = null, $servicio_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:Panico', 'h')
            ->join('h.servicio', 's')
            ->where('s.organizacion = :org')
            ->addOrderBy('h.fecha_panico', 'ASC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha_panico >= :desde');
            $query->andWhere('h.fecha_panico <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($servicio_id) && $servicio_id != 0) {
            $query->andWhere('s.id = :sid')->setParameter('sid', $servicio_id);
        }
        $query->getQuery()->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
        return $query->getQuery()->getResult();
    }

    /**
     * Obtiene todos los panicos que estan para los servicios pasados por parametro
     * @param type $servicios
     * @param type $limit es la cantidad de panicos que se devuelve, si es 0 se devuelven todos.
     * @return $panicos
     */
    public function obtenerSinAtencion($servicios, $limit)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:Panico', 'h')
                ->join('h.servicio', 's')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->addOrderBy('h.fecha_panico', 'ASC')
                ->setParameter(1, $ids);
            if ($limit > 0) {
                $query->setMaxResults($limit);
            }
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    public function contarSinAtencion($servicios)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('count(h.id)')
                ->from('App:Panico', 'h')
                ->join('h.servicio', 's')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->setParameter(1, $ids);
            return $query->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:Panico', 'p')
            ->where('p.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }
}
