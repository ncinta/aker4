<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class TipoCombustibleRepository extends EntityRepository
{

    public function getQuery($orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:TipoCombustible', 'c');

        if (is_null($orderBy)) {
            $query->addOrderBy('c.nombre', 'ASC');
        }
        return $query;
    }
}
