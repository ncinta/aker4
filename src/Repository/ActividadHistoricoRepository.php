<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\app\Producto;

class ActividadHistoricoRepository extends EntityRepository
{

    function findByProducto(Producto $producto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('ah')
            ->from('App:ActividadHistorico', 'ah')
            ->join('ah.historico', 'h')
            ->join('ah.producto', 'p')
            ->where('p.id = ?1')
            ->orderBy('h.fecha', 'ASC')
            ->setParameter(1, $producto->getId());

        return $query->getQuery()->getResult();
    }
}
