<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class HoraTallerRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:HoraTaller', 'p')
            ->join('p.organizacion', 'o')
            ->where('o.id = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('p.nombre', 'ASC');
        } else {
            foreach ($orderBy as $k => $order) {
                $query->addOrderBy($k, $order);
            }
        }
       
        return $query;
    }

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($organizacion, $orderBy)->getQuery()->getResult();
    }
}
