<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClienteRepository
 *
 * @author nicolas
 */
class PersonalRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:Personal', 'c')
            ->where('c.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('c.nombre', 'ASC');
        }
        return $query;
    }

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($organizacion, $orderBy)->getQuery()->getResult();
    }


    public function ifExist($actividad, $persona)
    {
        if ($actividad != 0) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('f')
                ->from('App:Personal', 'f')
                ->join('f.actividad', 'a')
                ->join('f.persona', 'p')
                ->where('a.id= :actividad')
                ->andWhere('p.id= :persona')
                ->setParameter('actividad', $actividad)
                ->setParameter('persona', $persona);
            $result = $query->getQuery()->getOneOrNullResult();
            return $result;
        } else {
            return null; //la actividad es nueva
        }
    }
}
