<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ServicioMantenimientoHistoricoRepository extends EntityRepository
{

    public function get($tarea)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ServicioMantenimientoHistorico', 'h')
            ->join('h.servicioMantenimiento', 'sm')
            ->where('sm.id = ?1')
            ->orderBy('h.fecha')
            ->setParameter(1, $tarea->getId());

        return $query->getQuery()->getResult();
    }

    public function findByServicio($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ServicioMantenimientoHistorico', 'h')
            ->join('h.servicio', 'sm')
            ->where('sm.id = ?1')
            ->orderBy('h.fecha')
            ->setParameter(1, $servicio->getId());

        return $query->getQuery()->getResult();
    }

    public function findOcasional($servicio, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ServicioMantenimientoHistorico', 'h')
            ->join('h.servicio', 'sm')
            ->where('sm.id = ?1')
            ->andWhere('h.fecha = ?2')
            ->andWhere('h.fecha = ?3')
            ->orderBy('h.fecha')
            ->setParameter(1, $servicio->getId())
            ->setParameter(2, $desde)
            ->setParameter(3, $hasta);

        return $query->getQuery()->getResult();
    }

    public function last($tarea)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ServicioMantenimientoHistorico', 'h')
            ->join('h.servicioMantenimiento', 'sm')
            ->where('sm.id = ?1')
            ->orderBy('h.fecha', 'DESC')
            ->setMaxResults(1)
            ->setParameter(1, $tarea->getId());
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findByFecha($servicio, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ServicioMantenimientoHistorico', 'h')
            ->leftJoin('h.servicioMantenimiento', 'sm')
            ->leftJoin('sm.servicio', 's')
            ->where('s.id = ?1 or h.servicio = ?1')
            ->andWhere('h.fecha >= ?2')
            ->andWhere('h.fecha <= ?3')
            ->orderBy('h.fecha')
            ->setParameter(1, $servicio->getId())
            ->setParameter(2, $desde)
            ->setParameter(3, $hasta);

        return $query->getQuery()->getResult();
    }
}
