<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClienteRepository
 *
 * @author nicolas
 */
class ClienteRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:Cliente', 'c')
            ->where('c.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('c.nombre', 'ASC');
        }
        return $query;
    }

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($organizacion, $orderBy)->getQuery()->getResult();
    }
}
