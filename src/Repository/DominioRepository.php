<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DominioRepository
 */
class DominioRepository extends EntityRepository
{

    public function findByDominio($dominio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT d FROM App:Dominio d
            WHERE d.dominio = :d 
            ')->setParameter('d', $dominio);
        return $query->getOneOrNullResult();
    }
}
