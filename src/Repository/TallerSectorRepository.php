<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of TallerSectorRepository
 *
 * @author nicolas
 */
class TallerSectorRepository extends EntityRepository
{

    public function findByOrg($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:TallerSector', 'i')
            ->where('i.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('i.nombre', 'ASC');
        }
        return $query->getQuery()->getResult();
    }
}
