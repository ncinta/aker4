<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriaRepository
 *
 * @author yesica
 */
class CategoriaRepository extends EntityRepository
{

    function getQueryCategoriasDisponibles($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:Categoria', 'c')
            ->where('c.organizacion = ?1')
            ->orderBy('c.nombre')
            ->setParameter(1, $organizacion->getId());
        return $query;
    }

    function findAllCategorias($organizacion)
    {
        return $this->getQueryCategoriasDisponibles($organizacion)
            ->getQuery()
            ->getResult();
    }
}
