<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class SensorRepository extends EntityRepository {

    public function delete($servicio) {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
                ->delete('App:Sensor', 'c')
                ->where('c.servicio = :s')
                ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }

}
