<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of TareaOtRepository
 *
 * @author nicolas
 */
class TareaOtRepository extends EntityRepository
{

    public function findByOrg($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:TallerExterno', 'i')
            ->where('i.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('i.nombre', 'ASC');
        }
        return $query->getQuery()->getResult();
    }
}
