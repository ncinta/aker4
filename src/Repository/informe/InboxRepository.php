<?php

namespace App\Repository\informe;

use Doctrine\ORM\EntityRepository;

/**
 * Description of CapturadorRepository
 *
 * @author nicolas
 */
class InboxRepository extends EntityRepository
{

    public function findByAutor($usuario)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:informe\Inbox', 'i')
            ->where('i.autor = ?1')
            ->setParameter('1', $usuario)
            ->orderBy('i.created_at', 'DESC');


        return $query->getQuery()->getResult();
    }
    public function findByOrganizacion($organizacion, $desde  = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:informe\Inbox', 'i')
            ->where('i.organizacion = ?1')
            ->andWhere('i.organizacion = ?1')
            ->setParameter('1', $organizacion)
            ->orderBy('i.created_at', 'DESC');
        //aca en algun momento podesmos pasar fechs para mostrar por ejemplo los ultimos meses, por el momento no
        if (!is_null($desde) && !is_null($hasta)) {
            $query
                ->andWhere('i.created_at >= ?2')
                ->andWhere('i.created_at <= ?3')
                ->setParameter('2', $desde)
                ->setParameter('3', $hasta);
        }

        return $query->getQuery()->getResult();
    }


    public function findByUid($uid)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:informe\Inbox', 'e')
            ->where('e.uid = ?1')
            ->setParameter('1', $uid);
        
        return $query->getQuery()->getOneOrNullResult();
    }
}
