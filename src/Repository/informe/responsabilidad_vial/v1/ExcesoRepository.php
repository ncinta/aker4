<?php

namespace App\Repository\informe\responsabilidad_vial\v1;

use App\Entity\informe\prudencia_vial\v1\Exceso;
use Doctrine\ORM\EntityRepository;

class ExcesoRepository extends EntityRepository
{

    public function findByUid($uid)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:informe\responsabilidad_vial\v1\Exceso', 'e')
            ->where('e.uid = ?1')
            ->setParameter('1', $uid);


        return $query->getQuery()->getResult();
    }
}