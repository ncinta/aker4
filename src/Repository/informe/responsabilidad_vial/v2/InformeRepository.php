<?php

namespace App\Repository\informe\responsabilidad_vial\v2;

use App\Entity\informe\responsabilidad_vial\v2\Exceso;
use Doctrine\ORM\EntityRepository;

class InformeRepository extends EntityRepository
{

    // si el informe con el uid se encuentra en la tabla de informes quiere decir que está finalizado según emma
    public function findByUid($uid)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:informe\responsabilidad_vial\v2\Informe', 'e')
            ->where('e.id = ?1')
            ->setParameter('1', $uid);

        // die('////<pre>' . nl2br(var_export($query->getQuery()->getOneOrNullResult(), true)) . '</pre>////');
        return $query->getQuery()->getOneOrNullResult();
    }
}
