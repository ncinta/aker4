<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of IndiceGsRepository
 *
 * @author nicolas
 */
class IndiceGsRepository extends EntityRepository
{

    public function getLastInsert($grupoServicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceGs', 'i')
            ->where('i.grupoServicio = :grupoServicio')
            ->addOrderBy('i.fecha', 'DESC')
            ->setMaxResults(1)
            ->setParameter('grupoServicio', $grupoServicio);

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getIndicesGs($grupo, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceGs', 'i')
            ->where('i.grupoServicio = ?1')
            ->setParameter('1', $grupo->getId())
            ->addOrderBy('i.fecha', 'ASC');
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('i.fecha >= :desde');
            $query->andWhere('i.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }
        return $query->getQuery()->getResult();
    }
}
