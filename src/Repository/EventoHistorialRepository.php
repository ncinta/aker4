<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class EventoHistorialRepository extends EntityRepository
{

    public function obtener($organizacion, $desde = null, $hasta = null, $evento_id = null, $servicio_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoHistorial', 'h')
            ->join('h.evento', 'e')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($evento_id) && $evento_id != 0) {
            $query->andWhere('e.id = :id')->setParameter('id', $evento_id);
        }

        if (!is_null($servicio_id) && $servicio_id != 0) {
            $query->andWhere('s.id = :sid')->setParameter('sid', $servicio_id);
        }
        return $query->getQuery()->getResult();
    }


    public function obtenerByCodename($organizacion, $codename, $desde = null, $hasta = null, $servicio_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoHistorial', 'h')
            ->join('h.evento', 'e')
            ->join('e.tipoEvento', 'te')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->andWhere('te.codename = :codename')
            ->addOrderBy('h.fecha', 'ASC')
            ->setParameter('org', $organizacion->getId())
            ->setParameter('codename', $codename);

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($servicio_id) && $servicio_id != 0) {
            $query->andWhere('s.id = :sid')->setParameter('sid', $servicio_id);
        }

        return $query->getQuery()->getResult();
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:EventoHistorial', 'h')
            ->where('h.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }

    /**
     * Obtiene todos los eventos que estan sin atender para los servicios pasados por parametro
     * @param type $servicios
     * @param type $limit es la cantidad de panicos que se devuelve, si es 0 se devuelven todos.
     * @return $panicos
     */
    public function obtenerSinAtencion($servicios, $minNotif, $limit, $lastLogin, $codename = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('e.notificacionWeb >= ?2')
                ->addOrderBy('h.fecha', 'DESC')
                ->setParameter(1, $ids)
                ->setParameter(2, $minNotif);
            if ($limit > 0) {
                $query->setMaxResults($limit);
            }
            if ($codename != null) {
                $query->join('e.tipoEvento', 'te')
                    ->andWhere('te.codename = ?3')
                    ->setParameter(3, $codename);
            }
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    public function obtenerUltimo($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('e.notificacionWeb >= ?2')
                ->addOrderBy('h.fecha', 'DESC')
                ->setMaxResults(1)
                ->setParameter(1, $ids)
                ->setParameter(2, $minNotif);
            if ($lastLogin != null) {   //solo cuando tengo ultimo login
                $query->andWhere('h.fecha >= ?3')
                    ->setParameter(3, $lastLogin);
            }
            return $query->getQuery()->getOneOrNullResult();
        } else {
            return null;
        }
    }

    public function contarSinAtencion($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('count(h.id)')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('e.notificacionWeb >= ?2')
                ->setParameter(1, $ids)
                ->setParameter(2, $minNotif);

            if ($lastLogin != null) {   //solo cuando tengo ultimo login
                $query->andWhere('h.fecha >= ?3')
                    ->setParameter(3, $lastLogin);
            }
            return $query->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }

    public function contarAnterioresSinAtencion($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('count(h.id)')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('h.fecha <= ?2')
                ->andWhere('e.notificacionWeb >= ?3')
                ->setParameter(1, $ids)
                ->setParameter(2, $lastLogin)
                ->setParameter(3, $minNotif);
            return $query->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }

    public function findByServicio($id, $cantidad)
    {
        //recorro todos los servicios para armar el array con los ids
        if (isset($id)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoHistorial', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->addOrderBy('h.fecha', 'DESC')
                ->setMaxResults($cantidad)
                ->setParameter(1, $id);

            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    public function obtenerAllClientes($ids, $desde = null, $hasta = null, $evento_id = null, $atendido = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoHistorial', 'h')
            ->join('h.evento', 'e')
            ->where('e.organizacion in (?1)')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('1', $ids);

        if ($evento_id != 0) {
            $query->andWhere('e.id = :id')->setParameter('id', $evento_id);
        }
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde->format('Y-m-d H:i:s'));
            $query->setParameter('hasta', $hasta->format('Y-m-d H:i:s'));
        }

        if ($atendido == 1) {
            $query->andWhere('h.respuesta IS NOT NULL');
        } else {
            $query->andWhere('h.respuesta IS NULL');
        }

        return $query->getQuery()->getResult();
    }

    public function obtenerNoAtendidas($organizacion, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoHistorial', 'h')
            ->join('h.evento', 'e')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->andWhere('h.fecha_vista IS NULL')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }


        return $query->getQuery()->getResult();
    }
}
