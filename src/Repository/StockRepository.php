<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class StockRepository extends EntityRepository
{

    public function findByDepositoProducto($deposito, $producto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Stock', 'i')
            ->join('i.deposito', 'd')
            ->join('i.producto', 'p')
            ->where('d.id = :deposito')
            ->andWhere('p.id = :producto')
            ->setParameter('deposito', $deposito->getId())
            ->setParameter('producto', $producto->getId());

       
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findByDeposito($deposito)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('s')
            ->from('App:Stock', 's')
            ->join('s.deposito', 'd')
            ->join('s.producto', 'p')
            ->where('d.id = :deposito')
            ->orderBy('p.nombre', 'ASC')
            ->setParameter('deposito', $deposito->getId());

      
        return $query->getQuery()->getResult();
    }

    public function findByProducto($producto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('s')
            ->from('App:Stock', 's')
            ->join('s.deposito', 'd')
            ->join('s.producto', 'p')
            ->where('p.id = :producto')
            ->orderBy('d.nombre', 'ASC')
            ->setParameter('producto', $producto->getId());

       
        return $query->getQuery()->getResult();
    }

    public function findByOrganizacion($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('s')
            ->from('App:Stock', 's')
            ->join('s.deposito', 'd')
            ->join('d.organizacion', 'o')
            ->join('s.producto', 'p')
            ->where('o.id = :organizacion')
            ->orderBy('p.nombre', 'ASC')
            ->setParameter('organizacion', $organizacion);

        return $query->getQuery()->getResult();
    }
}
