<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PermisoRepository
 */
class PermisoRepository extends EntityRepository
{

    /**
     * Devuelve todos los permisos de los modulos asociados a la organizacion
     * que se paso por parametro.
     */
    function getQueryByModulosAsociados($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Permiso', 'p')
            ->join('p.modulo', 'm')
            ->join('m.aplicacion', 'a')
            ->join('m.organizaciones', 'o')
            ->where('o.id = ?1')
            ->orderBy('a.nombre')
            ->addOrderBy('m.nombre')
            ->addOrderBy('p.descripcion')
            ->setParameter(1, $organizacion->getId());
        return $query;
    }

    function findByCodename($codename = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
                SELECT p FROM App:Permiso p
                WHERE p.codename = :code
                ')->setParameter('code', $codename);
        return $query->getOneOrNullResult();
    }
}
