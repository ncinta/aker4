<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Abonado;

class PeticionRepository extends EntityRepository
{

    public function findAllByIds($ids)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->where('p.id in (?1)')
            ->setParameter('1', $ids)
            //->orderBy('p.created_at', 'DESC')
        ;

        return $query->getQuery()->getResult();
    }

    public function findByServicio($servicio, $estado = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->where('p.servicio = ?1')
            ->setParameter('1', $servicio)
            ->orderBy('p.created_at', 'DESC');

        if (!is_null($estado)) {
            if ($estado == -1) { //estado abierto = nuevo || encurso
                $query
                    ->join('p.estado', 'e')
                    ->andWhere('e.isCerrado = false');
            } else {
                $query->andWhere('p.estado = ?3')->setParameter('3', $estado);
            }
        }


        return $query->getQuery()->getResult();
    }

    public function findByOrganizacion($organizacion, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->join('p.servicio', 's')
            ->where('s.organizacion = ?1')
            ->andWhere('s.organizacion = ?1')
            ->setParameter('1', $organizacion)
            ->orderBy('p.created_at', 'DESC');
        if (!is_null($desde) && !is_null($hasta)) { //estado abierto = nuevo || encurso
            $query
                ->andWhere('p.created_at >= ?2')
                ->andWhere('p.created_at <= ?3')
                ->setParameter('2', $desde)
                ->setParameter('3', $hasta);
        }

        return $query->getQuery()->getResult();
    }

    public function findNueva($organizacion, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->join('p.servicio', 's')
            ->join('p.estado', 'e')
            ->where('s.organizacion = ?1')
            ->andWhere('e.isDefault = ?2')
            ->setParameter('1', $organizacion)
            ->setParameter('2', true)
            ->orderBy('p.created_at', 'DESC');
        if (!is_null($desde) && !is_null($hasta)) { //estado abierto = nuevo || encurso
            $query
                ->andWhere('p.created_at >= ?3')
                ->andWhere('p.created_at <= ?4')
                ->setParameter('3', $desde)
                ->setParameter('4', $hasta);
        }

        return $query->getQuery()->getResult();
    }

    public function findEnCurso($organizacion, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->join('p.servicio', 's')
            ->join('p.estado', 'e')
            ->where('s.organizacion = ?1')
            ->andWhere('e.isCurso = ?2')
            ->setParameter('1', $organizacion)
            ->setParameter('2', true)
            ->orderBy('p.fecha_inicio', 'DESC');
        if (!is_null($desde) && !is_null($hasta)) { //estado abierto = nuevo || encurso
            $query
                ->andWhere('p.fecha_inicio >= ?3')
                ->andWhere('p.fecha_inicio <= ?4')
                ->setParameter('3', $desde)
                ->setParameter('4', $hasta);
        }

        return $query->getQuery()->getResult();
    }

    public function findCerrada($organizacion, $desde, $hasta)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->join('p.servicio', 's')
            ->join('p.estado', 'e')
            ->where('s.organizacion = ?1')
            ->andWhere('e.isCerrado = ?2')
            ->setParameter('1', $organizacion)
            ->setParameter('2', true)
            ->orderBy('p.fecha_fin', 'DESC');
        if (!is_null($desde) && !is_null($hasta)) { //estado abierto = nuevo || encurso
            $query
                ->andWhere('p.fecha_fin >= ?3')
                ->andWhere('p.fecha_fin <= ?4')
                ->setParameter('3', $desde)
                ->setParameter('4', $hasta);
        }

        return $query->getQuery()->getResult();
    }

    public function findAutorAll($usuario)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->where('p.autor = ?1')
            ->setParameter('1', $usuario)
            ->orderBy('p.created_at', 'DESC')
            ->orderBy('p.prioridad', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function filter($filtro , $servicios = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->orderBy('p.fecha_inicio', 'ASC');
        //->orderBy('p.prioridad', 'ASC');

        if ($filtro->getAsignadoA() && $filtro->getAsignadoA() != 0) {
            $query
                ->andWhere('p.asignado_a = ?1')
                ->setParameter('1', $filtro->getAsignadoA());
        }
        if ($filtro->getServicio() && $filtro->getServicio() > 0) {

            $query
                ->andWhere('p.servicio IN (?2)')
                ->setParameter('2', $servicios);
        }
        if ($filtro->getEstado() != null) {
            if ($filtro->getEstado() == -1) { //estado abierto = nuevo || encurso
                $query
                    ->join('p.estado', 'e')
                    ->andWhere('e.isCerrado = false');
            } else {
                $query->andWhere('p.estado = ?3')->setParameter('3', $filtro->getEstado());
            }
        }
        if ($filtro->getCategoria()) {
            $query->andWhere('p.categoria = ?4')->setParameter('4', $filtro->getCategoria());
        }
        if (!is_null($filtro->getPrioridad()) && $filtro->getPrioridad() >= 0) {
            $query->andWhere('p.prioridad = ?5')->setParameter('5', $filtro->getPrioridad());
        }
        if ($filtro->getDesde() && $filtro->getHasta()) {
            $query->andWhere('p.fecha_inicio >= ?6');  //solo para ese dia
            $query->andWhere('p.fecha_inicio <= ?7');  //solo para ese dia
            $query->setParameter('6', $filtro->getDesde());
            $query->setParameter('7', $filtro->getHasta());
        }
        if ($filtro->getTipo() >= 0) {

            $query->andWhere('p.tipo = ?8')->setParameter('8', $filtro->getTipo());
        }
        if ($filtro->getCentroCosto() > 0) {
            $query
                ->join('p.servicio', 's')
                ->andWhere('s.centrocosto = ?9')
                ->setParameter('9', $filtro->getCentroCosto());
        }
        if ($filtro->getOrganizacion() != null) {
            $query->andWhere('p.organizacion = ?10')->setParameter('10', $filtro->getOrganizacion());
        }

        return $query->getQuery()->getResult();
    }

    public function findOrganizacionAll($usuario, $organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Peticion', 'p')
            ->join('p.servicio', 's')
            ->where('p.autor = ?1')
            ->andWhere('s.organizacion = ?2')
            ->setParameter('1', $usuario)
            ->setParameter('2', $organizacion)
            ->orderBy('p.created_at', 'DESC')
            ->orderBy('p.prioridad', 'ASC');

        return $query->getQuery()->getResult();
    }
}
