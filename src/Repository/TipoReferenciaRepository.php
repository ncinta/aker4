<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class TipoReferenciaRepository extends EntityRepository
{

    function getQueryTipos()
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:TipoReferencia', 'c');
        return $query;
    }

    function findTipos()
    {
        return $this->getQueryTipos()->getQuery()->getResult();
    }
}
