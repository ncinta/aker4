<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class MetricaRepository extends EntityRepository
{

    public function getMetricas($tipo,  $organizacion = null, $desde = null, $hasta = null)
    {
      //  die('////<pre>' . nl2br(var_export($desde, true)) . '</pre>////');
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('m')
            ->from('App:Metrica', 'm')
            ->join('m.organizacion', 'o')
            ->where('m.tipo_metrica = :tipo')
            ->andWhere('m.created_at >= :desde and m.created_at <= :hasta')
            ->setParameter('tipo', $tipo)
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta);
            
            if($organizacion != null){
                $query->andWhere('o.id = :organizacion');
                $query->setParameter('organizacion', $organizacion->getId());
            }
            $query->addOrderBy('m.created_at', 'DESC');
      
        return $query->getQuery()->getResult();
    }
}
