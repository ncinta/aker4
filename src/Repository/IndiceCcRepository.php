<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of IndiceCcRepository
 *
 * @author nicolas
 */
class IndiceCcRepository extends EntityRepository
{

    public function getLastInsert($centroCosto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceCc', 'i')
            ->where('i.centroCosto = :centroCosto')
            ->addOrderBy('i.fecha', 'DESC')
            ->setMaxResults(1)
            ->setParameter('centroCosto', $centroCosto);

        return $query->getQuery()->getOneOrNullResult();
    }


    public function getIndicesCc($centros, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:IndiceCc', 'i')
            ->where('i.centroCosto in (?1)')
            ->setParameter('1', $centros)
            ->addOrderBy('i.fecha', 'ASC');
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('i.fecha >= :desde');
            $query->andWhere('i.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }
        return $query->getQuery()->getResult();
    }
}
