<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * PrestacionRepository
 */
class PrestacionRepository extends EntityRepository
{

    public function findByServicio($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Prestacion', 'p')
            ->join('p.servicio', 's')
            ->where('s.id = ?1 ')
            ->orderBy('p.inicio_prestacion', 'DESC')
            ->orderBy('p.id', 'DESC')
            //->setMaxResults(10)
            ->setParameter('1', $servicio);
        return $query->getQuery()->getResult();
    }

    public function obtenerCentroCostoPorServicioEnFecha($servicio, \DateTime $fechaCarga)
    {
        // Obtener el EntityManager desde el repositorio o el servicio
        $em = $this->getEntityManager(); // Esto solo funciona si estás en un repositorio de Doctrine

        $prestacion = $em->createQueryBuilder()
            ->select('p') // Seleccionamos la entidad 'Prestacion'
            ->from('App:Prestacion', 'p') // Especificamos la entidad 'Prestacion'
            ->innerJoin('p.centroCosto', 'cc') // Relacionamos con 'CentroCosto'
            ->where('p.servicio = :servicio') // Filtro por servicio
            ->andWhere('p.inicio_prestacion <= :fechaCarga') // Filtro por fecha de inicio
            ->andWhere('p.fin_prestacion >= :fechaCarga OR p.fin_prestacion IS NULL') // Filtro por fecha de fin
            ->setParameter('servicio', $servicio)
            ->setParameter('fechaCarga', $fechaCarga)
            ->orderBy('p.inicio_prestacion', 'DESC') // Ordenamos por fecha de inicio descendente
            ->setMaxResults(1) // Solo obtenemos la prestación más reciente
            ->getQuery()
            ->getOneOrNullResult(); // Ejecutamos la consulta y obtenemos el resultado

        if ($prestacion) {
            return $prestacion->getCentroCosto(); // Si hay una prestación, devolvemos el CentroCosto
        }

        return null; // Si no se encuentra ninguna prestación, retornamos null
    }
}
