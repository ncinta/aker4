<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ModuloRepository extends EntityRepository
{

    function getAll($aplicacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('m')
            ->from('App:Modulo', 'm')
            ->join('m.aplicacion', 'a')
            ->where('a.id = ?1')
            ->orderBy('m.nombre')
            ->setParameter(1, $aplicacion->getId());
        return $query->getQuery()->getResult();
    }
}
