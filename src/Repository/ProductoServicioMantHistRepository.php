<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of ProductoServicioMantHistRepository
 *
 * @author nicolas
 */
class ProductoServicioMantHistRepository extends EntityRepository
{

    public function findByProducto($producto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:ProductoServicioMantHist', 'h')
            ->join('h.producto', 'p')
            ->join('h.mantenimiento', 'm')
            ->where('p.id = ?1')
            ->orderBy('m.fecha')
            ->setParameter(1, $producto->getId());
        
        return $query->getQuery()->getResult();
    }
}
