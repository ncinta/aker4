<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ActgpmServicioRepository
 *
 * @author nicolas
 */
class ActgpmServicioRepository extends EntityRepository
{

    public function ifExist($actividad, $servicio)
    {
        if ($actividad != 0) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('f')
                ->from('App:ActgpmServicio', 'f')
                ->join('f.actividad', 'a')
                ->join('f.servicio', 's')
                ->where('a.id= :actividad')
                ->andWhere('s.id= :servicio')
                ->setParameter('actividad', $actividad)
                ->setParameter('servicio', $servicio);
            $result = $query->getQuery()->getOneOrNullResult();
            return $result;
        } else {
            return null; //la actividad es nueva
        }
    }

    public function findByActividad($actividad)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('f')
            ->from('App:ActgpmServicio', 'f')
            ->join('f.actividad', 'a')
            ->where('a.id= :actividad')
            ->setParameter('actividad', $actividad);

        $result = $query->getQuery()->getOneOrNullResult();
        return $result;
    }
}
