<?php



namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 *
 * @author nicolas
 */
class ResponsableGpmRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:ResponsableGpm', 'c')
            ->join('c.persona', 'p')
            ->where('c.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('p.nombre', 'ASC');
        }
        return $query;
    }
}
