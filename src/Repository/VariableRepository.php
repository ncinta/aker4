<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class VariableRepository extends EntityRepository
{
    public function getOnlyVariables($modelo)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('v')
            ->from('App:Variable', 'v')
            ->join('v.modelo', 'm')
            ->where('m.id = ?1')
            ->andWhere('v.tipoVariable = 1 or v.tipoVariable = 2')
            ->setParameter(1, $modelo->getId());
        return $query->getQuery()->getResult();
    }
}
