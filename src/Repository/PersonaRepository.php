<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ClienteRepository
 *
 * @author nicolas
 */
class PersonaRepository extends EntityRepository
{

    public function getQueryByContratista($contratista, $orderBy = null)
    {
        $em = $this->getEntityManager();
        if ($contratista !== null) {
            $query = $em->createQueryBuilder()
                ->select('p')
                ->from('App:Persona', 'p')
                ->where('p.contratista = :contratista')
                ->setParameter('contratista', $contratista->getId());

            if (is_null($orderBy)) {
                $query->addOrderBy('p.nombre', 'ASC');
            }
            return $query;
        }
        return null;
    }

    public function byContratista($contratista, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($contratista, $orderBy)->getQuery()->getResult();
    }
}
