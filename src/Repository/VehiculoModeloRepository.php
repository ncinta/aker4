<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of VehiculoModeloRepository
 *
 * @author nicolas
 */
class VehiculoModeloRepository extends EntityRepository
{

    public function findByOrg($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:VehiculoModelo', 'i')
            ->where('i.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('i.id', 'DESC');
        }
   
        return $query->getQuery()->getResult();
    }
}
