<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ServicioCanbusRepository extends EntityRepository
{

    public function findMediciones($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioTanque', 't')
            ->where('t.servicio = ?1')
            ->orderBy('t.porcentaje')
            ->setParameter(1, $servicio->getId());
        return $query->getQuery()->getResult();
    }

    public function findByOrg($idOrg)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:ServicioCanbus', 't')
            ->join('t.servicio', 's')
            ->join('s.organizacion', 'o')
            ->where('o.id = ?1')
            ->orderBy('s.ultFechaHora', 'desc')
            ->setParameter(1, $idOrg);
        return $query->getQuery()->getResult();
    }
    public function findByService($idServicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:ServicioCanbus', 'c')
            ->join('c.servicio', 's')
            ->where('s.id = ?1')
            ->setParameter(1, $idServicio);
      
        return $query->getQuery()->getOneOrNullResult();
    }
}
