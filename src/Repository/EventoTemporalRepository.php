<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class EventoTemporalRepository extends EntityRepository
{

    public function obtener($organizacion, $desde = null, $hasta = null, $evento_id = null, $servicio_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoTemporal', 'h')
            ->join('h.evento', 'e')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($evento_id) && $evento_id != 0) {
            $query->andWhere('e.id = :id')->setParameter('id', $evento_id);
        }

        if (!is_null($servicio_id) && $servicio_id != 0) {
            $query->andWhere('s.id = :sid')->setParameter('sid', $servicio_id);
        }
        return $query->getQuery()->getResult();
    }


    public function obtenerByCodename($organizacion, $codename, $desde = null, $hasta = null, $servicio_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoTemporal', 'h')
            ->join('h.evento', 'e')
            ->join('e.tipoEvento', 'te')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->andWhere('te.codename = :codename')
            ->addOrderBy('h.fecha', 'ASC')
            ->setParameter('org', $organizacion->getId())
            ->setParameter('codename', $codename);

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($servicio_id) && $servicio_id != 0) {
            $query->andWhere('s.id = :sid')->setParameter('sid', $servicio_id);
        }

        return $query->getQuery()->getResult();
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:EventoTemporal', 'h')
            ->where('h.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }

    public function findByUser($usuario, $limit = null, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:EventoTemporal', 't')
            ->where('t.usuario = ?1 ')
            ->addOrderBy('t.fecha', 'DESC')
            ->setParameter(1, $usuario->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('t.fecha', 'DESC');
        } else {
            foreach ($orderBy as $k => $order) {
                $query->addOrderBy($k, $order);
            }
        }
        if ($limit > 0) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()->getResult();
    }

    public function findByUserHist($usuario, $eventoHist)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:EventoTemporal', 't')
            ->join('t.usuario', 'u')
            ->join('t.eventoHistorial', 'e')
            ->where('u.id = ?1 and e.id= ?2')
            ->setParameter(1, $usuario->getId())
            ->setParameter(2, $eventoHist->getId());

        return $query->getQuery()->getResult();
    }

    public function obtenerUltimo($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoTemporal', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('e.notificacionWeb >= ?2')
                ->addOrderBy('h.fecha', 'DESC')
                ->setMaxResults(1)
                ->setParameter(1, $ids)
                ->setParameter(2, $minNotif);
            if ($lastLogin != null) {   //solo cuando tengo ultimo login
                $query->andWhere('h.fecha >= ?3')
                    ->setParameter(3, $lastLogin);
            }
            return $query->getQuery()->getOneOrNullResult();
        } else {
            return null;
        }
    }

    public function contarSinAtencion($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('count(h.id)')
                ->from('App:EventoTemporal', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('e.notificacionWeb >= ?2')
                ->setParameter(1, $ids)
                ->setParameter(2, $minNotif);

            if ($lastLogin != null) {   //solo cuando tengo ultimo login
                $query->andWhere('h.fecha >= ?3')
                    ->setParameter(3, $lastLogin);
            }
            return $query->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }

    public function contarAnterioresSinAtencion($servicios, $minNotif, $lastLogin = null)
    {
        //recorro todos los servicios para armar el array con los ids
        foreach ($servicios as $servicio) {
            $ids[] = $servicio->getId();
        }
        if (isset($ids)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('count(h.id)')
                ->from('App:EventoTemporal', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->andWhere('h.fecha <= ?2')
                ->andWhere('e.notificacionWeb >= ?3')
                ->setParameter(1, $ids)
                ->setParameter(2, $lastLogin)
                ->setParameter(3, $minNotif);
            return $query->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }

    public function findByServicioUsuario($servicio, $usuario, $fecha)
    {
        if (isset($servicio)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoTemporal', 'h')
                ->join('h.servicio', 's')
                ->join('h.usuario', 'u')
                ->where('u.id = ?1 and s.id = ?2 and h.fecha = ?3')
                ->addOrderBy('h.fecha', 'DESC')
                ->setParameter(1, $usuario->getId())
                ->setParameter(2, $servicio->getId())
                ->setParameter(3, $fecha);
            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }

    public function findByServicio($id, $cantidad)
    {
        //recorro todos los servicios para armar el array con los ids
        if (isset($id)) {
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder()
                ->select('h')
                ->from('App:EventoTemporal', 'h')
                ->join('h.servicio', 's')
                ->join('h.evento', 'e')
                ->where('h.fecha_vista IS NULL and s.id IN (?1)')
                ->addOrderBy('h.fecha', 'DESC')
                ->setMaxResults($cantidad)
                ->setParameter(1, $id);

            return $query->getQuery()->getResult();
        } else {
            return null;
        }
    }


    public function obtenerAllClientes($ids, $desde = null, $hasta = null, $evento_id = null, $atendido = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoTemporal', 'h')
            ->join('h.evento', 'e')
            ->where('e.organizacion in (?1)')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('1', $ids);

        if ($evento_id != 0) {
            $query->andWhere('e.id = :id')->setParameter('id', $evento_id);
        }
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde->format('Y-m-d H:i:s'));
            $query->setParameter('hasta', $hasta->format('Y-m-d H:i:s'));
        }

        if ($atendido == 1) {
            $query->andWhere('h.respuesta IS NOT NULL');
        } else {
            $query->andWhere('h.respuesta IS NULL');
        }

        return $query->getQuery()->getResult();
    }

    public function obtenerNoAtendidas($organizacion, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:EventoTemporal', 'h')
            ->join('h.evento', 'e')
            ->join('h.servicio', 's')
            ->where('e.organizacion = :org')
            ->andWhere('h.fecha_vista IS NULL')
            ->addOrderBy('h.fecha', 'DESC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.fecha >= :desde');
            $query->andWhere('h.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }


        return $query->getQuery()->getResult();
    }

    /** 
     * Obtiene todos los eventos nuevos que se encuentran en el temporal para el usuario
     * pero que tengan un ultimoId mayor del pasado, de esta forma se que son nuevos eventos
     * que el usuario no ha visto, sin importar la fecha.
     */
    public function findNuevos($usuario, $ultimoId, $limit = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('App:EventoTemporal', 't')            
            ->where('t.usuario = ?1 ')
            ->andWhere('t.id > ?2')
            ->addOrderBy('t.id', 'DESC')
            ->setParameter(1, $usuario->getId())
            ->setParameter(2, $ultimoId);

        if ($limit > 0) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()->getResult();
    }
}
