<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of BitacoraRepository
 *
 * @author nicolas
 */
class BitacoraRepository extends EntityRepository
{

    public function byOrganizacion($organizacion, $desde = null, $hasta = null, $evento_id = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:Bitacora', 'h')
            ->where('h.organizacion = :org')
            ->addOrderBy('h.created_at', 'ASC')
            ->setParameter('org', $organizacion->getId());

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('h.created_at >= :desde');
            $query->andWhere('h.created_at <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }

        if (!is_null($evento_id) && $evento_id != 0) {
            $query->andWhere('h.tipo_evento = :tev')->setParameter('tev', $evento_id);
        }else{
            $query->andWhere('h.tipo_evento != :tev')->setParameter('tev', 10); //esto es un parche porque el evento 10 graba con error

        }

        return $query->getQuery()->getResult();
    }

    public function byChofer($chofer)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('h')
            ->from('App:Bitacora', 'h')
            ->where('h.chofer = :chof')
            ->addOrderBy('h.created_at', 'DESC')
            ->setParameter('chof', $chofer->getId());

        return $query->getQuery()->getResult();
    }
}
