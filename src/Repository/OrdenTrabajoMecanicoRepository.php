<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class OrdenTrabajoMecanicoRepository extends EntityRepository
{

    public function findByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:Mecanico', 'm')
            ->join('m.organizacion', 'o')
            ->where('o.organizacion = ?1')
            ->orderBy('m.nombre', 'ASC')
            ->setParameter('1', $organizacion->getId());

        return $query->getQuery()->getResult();
    }

    public function byMecanico($desde, $hasta, $mecanico)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:OrdenTrabajoMecanico', 'e')
            ->join('e.mecanico', 'm')
            ->join('e.ordenTrabajo', 'o')
            ->where('m.id = ?1')
            ->andWhere('o.fecha >= ?2')
            ->andWhere('o.fecha <= ?3')
            ->orderBy('o.fecha', 'ASC')
            ->setParameter('1', $mecanico->getId())
            ->setParameter('2', $desde)
            ->setParameter('3', $hasta);

        return $query->getQuery()->getResult();
    }
}
