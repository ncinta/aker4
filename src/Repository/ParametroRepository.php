<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ParametroRepository extends EntityRepository
{

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:Parametro', 'p')
            ->where('p.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }

    public function findAllChield($padre, $servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Parametro', 'p')
            ->join('p.variable', 'v')
            ->where('p.servicio = :s')
            ->andWhere('v.variable_padre = :p')
            ->setParameter('s', $servicio)
            ->setParameter('p', $padre);
      
        return $query->getQuery()->execute();
    }

    public function findPadre($padre, $servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Parametro', 'p')
            ->where('p.servicio = :s')
            ->andWhere('p.variable = :p')
            ->setParameter('s', $servicio)
            ->setParameter('p', $padre);
        return $query->getQuery()->getOneOrNullResult();
    }
}
