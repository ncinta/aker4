<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class OrdenTrabajoRepository extends EntityRepository
{

    public function byServicio($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:OrdenTrabajo', 'e')
            ->where('e.servicio = ?1')
            ->orderBy('e.fecha', 'ASC')
            ->setParameter('1', $servicio);

        return $query->getQuery()->getResult();
    }

    public function findByOrganizacion($organizacion, $orderBy = null, $desde = null, $hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:OrdenTrabajo', 'e')
            ->join('e.servicio', 's')
            ->where('s.organizacion = ?1')
            ->orderBy('e.fecha', 'DESC')
            ->addOrderBy('e.id', 'DESC')
            ->setParameter('1', $organizacion->getId());
        if (!is_null($desde) && !is_null($hasta)) {
            $query
                ->andWhere('e.fecha >= ?2')
                ->andWhere('e.fecha <= ?3')
                ->setParameter('2', $desde)
                ->setParameter('3', $hasta);
        }

        return $query->getQuery()->getResult();
    }

    public function historico($organizacion, $desde, $hasta, $servicio, $tipo, $sector, $deposito, $centrocosto, $taller)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:OrdenTrabajo', 'e')
            ->join('e.servicio', 's')
            ->where('s.organizacion = ?1')
            ->orderBy('e.fecha', 'DESC')
            ->addOrderBy('e.id', 'DESC')
            ->setParameter('1', $organizacion->getId());
        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('e.fecha >= :desde');
            $query->andWhere('e.fecha <= :hasta');
            $query->setParameter('desde', $desde);
            $query->setParameter('hasta', $hasta);
        }
        if (!is_null($servicio)) {
            $query->andWhere('s.id = :servicio')->setParameter('servicio', $servicio->getId());
        }
        if (!is_null($tipo)) {
            // die('////<pre>' . nl2br(var_export($tipo, true)) . '</pre>////');
            $query->andWhere('e.tipo = :tipo')->setParameter('tipo', $tipo);
        }
        if (!is_null($sector)) {
            $query->join('e.tallerSector', 't');
            $query->andWhere('t.id = :sector')->setParameter('sector', $sector->getId());
        }
        if (!is_null($deposito)) {
            $query->join('e.deposito', 'd');
            $query->andWhere('d.id = :deposito')->setParameter('deposito', $deposito->getId());
        }
        if (!is_null($centrocosto)) {
            $query->join('e.centroCosto', 'cc');
            $query->andWhere('cc.id = :centrocosto')->setParameter('centrocosto', $centrocosto->getId());
        }
        if (!is_null($taller)) {
            $query->join('e.taller', 'tt');
            $query->andWhere('tt.id = :taller')->setParameter('taller', $taller->getId());
        }


        return $query->getQuery()->getResult();
    }

    public function getMaxIdentificador($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('MAX(e.identificador)')
            ->from('App:OrdenTrabajo', 'e')
            ->join('e.servicio', 's')
            ->where('s.organizacion = ?1')
            ->setParameter('1', $organizacion->getId());

        //die('////<pre>' . nl2br(var_export( $organizacion->getId(), true)) . '</pre>////');
        return $query->getQuery()->getOneOrNullResult();
    }
}
