<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ItinerarioRepository
 *
 * @author nicolas
 */
class ItinerarioRepository extends EntityRepository
{

    public function historico($organizacion, $estado, $usuario = null, $servicio = null, $tipoServicio = null, $transporte = null, $satelital = null, $empresa = null, $fecha_desde = null, $fecha_hasta = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->leftJoin('i.servicios', 's')
            ->join('i.organizacion', 'o')
            ->where('o.id = :org')->setParameter('org', $organizacion->getId())
            ->orderBy('i.created_at', 'DESC')
            ->addOrderBy('i.id', 'DESC');

        //die('////<pre>' . nl2br(var_export($estado, true)) . '</pre>////');
        if ($estado != -1) {    //abiertos
            $query->andWhere('i.estado = :estado')->setParameter('estado', $estado);
        }

        if (!is_null($usuario)) {
            $query
                ->join('i.usuarios', 'u')
                ->andWhere('u.id = ?1')->setParameter('1', $usuario->getId());
        }
        if (!is_null($fecha_desde) && !is_null($fecha_hasta)) {
            $query->andWhere('i.fechaInicio >= :desde')
                ->setParameter('desde', $fecha_desde);
            $query->andWhere('i.fechaInicio <= :hasta')
                ->setParameter('hasta', $fecha_hasta);
        }

        if (!is_null($servicio)) {
            $query->andWhere('s.id = :servicio')->setParameter('servicio', $servicio->getId());
        }

        if (!is_null($tipoServicio)) {
            $query->join('s.tipoServicio', 'ts');
            $query->andWhere('ts.id = :tipo')->setParameter('tipo', $tipoServicio->getId());
        }
        if (!is_null($transporte) || !is_null($satelital)) {
            $query->join('s.servicio', 'ss');
            if (!is_null($transporte)) {                
                $query->join('ss.transporte', 't');
                $query->andWhere('t.id = ?2')->setParameter('2', $transporte->getId());
            }
            if (!is_null($satelital)) {                
                $query->join('ss.satelital', 'sa');
                $query->andWhere('sa.id = :satelital')->setParameter('satelital', $satelital->getId());
            }
        }
        if (!is_null($empresa)) {
            $query->join('i.empresa', 'e');
            $query->andWhere('e.id = :empresa')->setParameter('empresa', $empresa->getId());
        }
        //        if (!is_null($organizacion)) {
        //            $query->andWhere('u.id = :usuario')->setParameter('usuario', $organizacion->getId());
        //        }

        // die('////<pre>' . nl2br(var_export($query->getQuery()->getSql(), true)) . '</pre>////');
        return $query->getQuery()->getResult();
    }

    public function findOpens($user = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->where('i.estado < 9')
            ->orderBy('i.created_at', 'DESC');

        if ($user) {
            $query
                ->join('i.usuarios', 'u')
                ->andWhere('u.id = ?1')
                ->setParameter('1', $user->getId());
        }
        return $query->getQuery()->getResult();
    }
    
    public function findInCurso($user = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->where('i.estado > 0 and i.estado < 9') // todos menos los nuevos y los cerrados (en este caso auditoria y en curso)
            ->orderBy('i.created_at', 'DESC');

        if ($user) {
            $query
                ->join('i.usuarios', 'u')
                ->andWhere('u.id = ?1')
                ->setParameter('1', $user->getId());
        }
        return $query->getQuery()->getResult();
    }

    public function findByUser($user, $estado = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.usuarios', 'u')
            ->where('u.id = ?1')
            ->orderBy('i.created_at', 'DESC')
            ->setParameter('1', $user->getId());

        return $query->getQuery()->getResult();
    }

    public function findOpenByServicio($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.servicios', 'is')
            ->join('is.servicio', 's')
            ->where('s.id = ?1 and i.estado < 9')
            ->orderBy('i.created_at', 'DESC')
            ->setParameter('1', $servicio->getId());

        return $query->getQuery()->getResult();
    }

    public function findByOptions($organizacion, $estado, $empresas = null, $satelitales = null, $transportes = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->leftJoin('i.servicios', 's')
            ->join('i.organizacion', 'o')
            ->where('o.id = :org')->setParameter('org', $organizacion->getId())
            ->orderBy('i.created_at', 'DESC')
            ->addOrderBy('i.id', 'DESC');

        if ($estado == 0) {    //abiertos
            $query->andWhere('i.estado < 9');
        } elseif ($estado == 1) {    //cerrados
            $query->andWhere('i.estado >= 9');
        }elseif($estado == '-1'){ // que traiga los que estan en movimiento (auditoria, en curso, etc)
            $query->andWhere('i.estado > 0 and i.estado < 9');
        }
        
        if (!is_null($transportes) || !is_null($satelitales)) {
            $query->join('s.servicio', 'ss');
            if (!is_null($transportes)) {
                $query->andwhere($query->expr()->in('ss.transporte', $transportes));
            }
            
            if (!is_null($satelitales)) {
              //  die('////<pre>' . nl2br(var_export($satelitales, true)) . '</pre>////');
                $query->andwhere($query->expr()->in('ss.satelital', $satelitales));
              
            }    
        }
        
        if (!is_null($empresas)) {
            $query->andwhere($query->expr()->in('i.empresa', $empresas));
        }
        //  die('////<pre>' . nl2br(var_export($query->getQuery()->getArrayResult(), true)) . '</pre>////');
        //        if (!is_null($organizacion)) {
        //            $query->andWhere('u.id = :usuario')->setParameter('usuario', $organizacion->getId());
        //        }
        //   die('////<pre>' . nl2br(var_export($query->getQuery()->getSQL(), true)) . '</pre>////');
        return $query->getQuery()->getResult();
    }
}
