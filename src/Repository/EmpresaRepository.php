<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class EmpresaRepository extends EntityRepository
{

    public function getEmpresas($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('App:Empresa', 'e')
            ->join('e.organizacion', 'o')
            ->where('o.id = ?1')
            ->setParameter('1', $organizacion->getId());
        return $query->getQuery()->getResult();
    }

    public function getQueryEmpresas($userlogin)
    {
        if (!is_null($userlogin->getEmpresa())) {   //es un usuario de un cliente
            $query = $this->getEntityManager()->createQueryBuilder()
                ->select('e')
                ->from('App:Empresa', 'e')
                ->where('e.id = ?1')
                ->setParameter('1', $userlogin->getEmpresa()->getId());
        } else {
            $query = $this->getEntityManager()->createQueryBuilder()
                ->select('e')
                ->from('App:Empresa', 'e')
                ->join('e.organizacion', 'o')
                ->where('o.id = ?1')
                ->orderBy('e.nombre', 'ASC')
                ->setParameter('1', $userlogin->getOrganizacion()->getId());
        }
        return $query->getQuery()->getResult();
    }
}
