<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class DepositoRepository extends EntityRepository
{

    public function getQueryByOrganizacion($organizacion, $orderBy = null)
    {
    //    die('////<pre>' . nl2br(var_export($organizacion->getId(), true)) . '</pre>////');
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('d')
            ->from('App:Deposito', 'd')
            ->where('d.organizacion = :organizacion')
            ->setParameter('organizacion', $organizacion->getId());

        if (is_null($orderBy)) {
            $query->addOrderBy('d.nombre', 'ASC');
        } else {
            foreach ($orderBy as $k => $order) {
                $query->addOrderBy($k, $order);
            }
        }
        return $query;
    }

    public function byOrganizacion($organizacion, $orderBy = null)
    {
        return $this->getQueryByOrganizacion($organizacion, $orderBy)->getQuery()->getResult();
    }
}
