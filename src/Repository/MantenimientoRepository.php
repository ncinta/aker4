<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * MantenimientoRepository
 */
class MantenimientoRepository extends EntityRepository
{

    function getAll($organizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('m')
            ->from('App:Mantenimiento', 'm')
            ->join('m.organizaciones', 'org')
            ->where('org.id = ?1')
            ->orderBy('m.nombre')
            ->setParameter(1, $organizacion->getId());
        return $query->getQuery()->getResult();
    }
}
