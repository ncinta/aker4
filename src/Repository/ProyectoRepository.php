<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ProyectoRepository
 *
 * @author nicolas
 */
class ProyectoRepository extends EntityRepository
{

    public function ifExist($proyecto)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Proyecto', 'p')
            ->where('p.codigo = :codigo')
            ->setParameter('codigo', $proyecto->getCodigo());
        $result = $query->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function filter($filtro)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('App:Proyecto', 'p')
            ->orderBy('p.fechaInicio', 'ASC');
        //->orderBy('p.prioridad', 'ASC');

        if ($filtro->getResponsable() && $filtro->getResponsable() != 0) {
            $query
                ->join('p.responsable', 'r')
                ->andWhere('r.id = ?1')
                ->setParameter('1', $filtro->getResponsable());
        }
        if ($filtro->getContratista() && $filtro->getContratista() > 0) {
            $query
                ->join('p.contratista', 'c')
                ->andWhere('c.id = ?2')
                ->setParameter('2', $filtro->getContratista());
        }

        if ($filtro->getCliente() && $filtro->getCliente() > 0) {
            $query
                ->join('p.cliente', 'e')
                ->andWhere('e.id = ?3')
                ->setParameter('3', $filtro->getCliente());
        }

        if ($filtro->getEstado() != -1) { //estado abierto = nuevo || encurso
            $query->andWhere('p.estado = ?4')->setParameter('4', $filtro->getEstado());
        }


        if ($filtro->getDesde() && $filtro->getHasta()) {
            $query->andWhere('p.fechaInicio >= ?6');  //solo para ese dia
            $query->andWhere('p.fechaInicio <= ?7');  //solo para ese dia
            $query->setParameter('6', $filtro->getDesde());
            $query->setParameter('7', $filtro->getHasta());
        }

        if ($filtro->getOrganizacion() != null) {
            $query->andWhere('p.organizacion = ?10')->setParameter('10', $filtro->getOrganizacion());
        }

        return $query->getQuery()->getResult();
    }
}
