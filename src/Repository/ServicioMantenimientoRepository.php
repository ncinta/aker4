<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ServicioMantenimientoRepository extends EntityRepository
{

    public function findByOrganizacion($organizacion, $orderBy = null)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('sm')
            ->from('App:ServicioMantenimiento', 'sm')
            ->join('sm.servicio', 's')
            ->join('s.organizacion', 'o')
            ->where('o.id = ?1')
            //->orderBy('s.nombre')
            ->setParameter(1, $organizacion->getId());

        if ($orderBy) {
            foreach ($orderBy as $key => $value) {
                $query->addOrderBy('s.' . $key, $value);
            }
        }


        return $query->getQuery()->getResult();
    }

    public function findByServicio($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('sm')
            ->from('App:ServicioMantenimiento', 'sm')
            ->join('sm.servicio', 's')
            ->where('sm.servicio = ?1')
            //->orderBy('s.nombre')
            ->setParameter(1, $servicio->getId());

        return $query->getQuery()->getResult();
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:ServicioMantenimiento', 'm')
            ->where('m.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }
}
