<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\Servicio;
use App\Entity\CargaCombustible;
use App\Entity\PuntoCarga;

/**
 * ContactoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CargaCombustibleRepository extends EntityRepository
{

    public function findAllCargas($servicio = null, $desde = null, $hasta = null)
    {
        //no tocar orderBy porque se usa para informe de rendimiento
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->orderBy('c.fecha', 'ASC');

        if (!is_null($desde) && !is_null($hasta)) {
            $query->andWhere('c.fecha >= :desde and c.fecha <= :hasta');
            $query->setParameters(array(
                'desde' => $desde,
                'hasta' => $hasta,
            ));
        }

        if (!is_null($servicio)) {
            $query->andWhere('c.servicio = ' . $servicio->getId());
        }
        return $query->getQuery()->getResult();
    }

    public function findUltimasCargas(Servicio $servicio = null, $limit = 2, $asArray = false)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->where('c.servicio = ?1')
            ->orderBy('c.fecha', 'desc')
            ->setMaxResults($limit)
            ->setParameter(1, $servicio->getId());
        if ($asArray) {
            return $query->getQuery()->getArrayResult();
        } else {
            if ($limit = 1) {
                return $query->getQuery()->getOneOrNullResult();
            } else {
                return $query->getQuery()->getResult();
            }
            
        }
    }    
    
    public function findUltimasCargasByPuntoCarga(PuntoCarga $punto = null, $limit = 2, $asArray = false)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->where('c.puntoCarga = ?1')
            ->orderBy('c.fecha', 'desc')
            ->setMaxResults($limit)
            ->setParameter(1, $punto->getId());
        if ($asArray) {
            return $query->getQuery()->getArrayResult();
        } else {
            if ($limit = 1) {
                return $query->getQuery()->getOneOrNullResult();
            } else {
                return $query->getQuery()->getResult();
            }
            
        }
    }

    /**
     * Retorna la carga anterior para el servicio para una fecha dada.
     * @param \App\Entity\Servicio $servicio
     * @param type $carga
     * @param type $asArray
     * @return type 
     */
    public function findCargaAnterior(Servicio $servicio = null, CargaCombustible $carga = null, $asArray = false)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->where('c.servicio = ?1 and c.fecha < ?2')
            ->orderBy('c.fecha', 'desc')
            ->setMaxResults(1)
            ->setParameters(array(
                '1' => $servicio->getId(),
                '2' => $carga->getFecha()
            ));

        if ($asArray) {
            return $query->getQuery()->getArrayResult();
        } else {
            return $query->getQuery()->getResult();
        }
    }

    public function findCarga($carga_id, $asArray = false)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->where('c.id = ?1')
            ->setParameter(1, $carga_id);
        if ($asArray) {
            return $query->getQuery()->getArrayResult();
        } else {
            return $query->getQuery()->getOneOrNullResult();
        }
    }

    public function findByCodigoAutorizacion($codigo_autorizacion)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT c FROM App:CargaCombustible c
            WHERE c.codigo_autorizacion = :codigo
            ')->setParameter('codigo', $codigo_autorizacion);
        return $query->getResult();
    }

    public function findByMultiple($idServicio, $fecha, $codigo_eess, $litros)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
            SELECT c FROM App:CargaCombustible c
            WHERE c.servicio = :id
                and c.codigo_estacion = :codigo_eess
                and c.litros_carga = :litros
                and c.fecha = :fecha
            ')->setParameters(
            array(
                'id' => $idServicio,
                'codigo_eess' => $codigo_eess,
                'litros' => $litros,
                'fecha' => $fecha,
            )
        );

        return $query->getResult();
    }

    public function existeCarga($id, $fecha, $codigo_eess, $litros, $codigo_autorizacion = null)
    {
        if (strlen($codigo_autorizacion) > 0) {
            $carga = $this->findByCodigoAutorizacion($codigo_autorizacion);
        } else {
            if (!is_null($id) && !is_null($fecha) && !is_null($codigo_eess) && !is_null($litros)) {
                $carga = $this->findByMultiple($id, $fecha, $codigo_eess, $litros);
            } else {
                return true; //hay algun error por eso lo toma como duplicada.
            }
        }

        if (count($carga) == 0) {
            return false;   //la carga no existe...
        } else {
            return true;    //la carga si existe...
        }
    }

    public function delete($servicio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:CargaCombustible', 'c')
            ->where('c.servicio = :s')
            ->setParameter('s', $servicio);
        return $query->getQuery()->execute();
    }

    public function deleteCarga($carga)
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->delete('App:CargaCombustible', 'c')
            ->where('c.id = ?1')
            ->setParameter('1', $carga->getId());
        return $query->getQuery()->execute();
    }

    public function findByFecha($servicio, $desde, $hasta, $ingreso = null, $referencia_id = null)
    {
        if ($desde instanceof \DateTime) {
            $desdeDate = $desde;
        } else {
            $desdeDate = new \DateTime($desde);
        }
        if ($hasta instanceof \DateTime) {
            $hastaDate = $hasta;
        } else {
            $hastaDate = new \DateTime($hasta);
        }

        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->join('c.servicio', 's')
            ->where('s.id = ?1')
            ->orderBy('c.fecha', 'ASC')
            ->andWhere('c.fecha >= ?2 AND c.fecha <= ?3')
            ->setParameter('1', $servicio->getId())
            ->setParameter('2', $desdeDate)
            ->setParameter('3', $hastaDate);


        if ($ingreso != null && $ingreso >= 0) {
            $query
                ->andWhere('c.modo_ingreso = ?4')
                ->setParameter('4', $ingreso);
        }
        if ($referencia_id != 0) {
            $query
                ->join('c.referencia', 'r')
                ->andWhere('r.id = ?5')
                ->setParameter('5', intval($referencia_id));
        }

        return $query->getQuery()->execute();
    }

    public function findByEstacionFecha($desde, $hasta, $ids, $idOrg, $tipoCombustible, $estacion = null)
    {
        $desdeDate = new \DateTime($desde);
        $hastaDate = new \DateTime($hasta);
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->join('c.servicio', 's')
            ->join('s.organizacion', 'o')
            ->join('c.referencia', 'r')
            ->andWhere('c.fecha >= ?1 AND c.fecha <= ?2')
            ->orderBy('c.fecha', 'ASC')
            ->orderBy('c.servicio', 'ASC')
            ->setParameter('1', $desdeDate)
            ->setParameter('2', $hastaDate);
        if ($estacion) {
            $query
                ->andWhere('r.id = ?3')
                ->setParameter('3', $estacion->getId());
        } else { //que solamente busque en referencias de ese usuario
            if (count($ids) > 0) {
                $query
                    ->andWhere('r.id IN (?4)')
                    ->setParameter('4', $ids);
            } else {
                $query
                    ->andWhere('o.id = ?5')
                    ->setParameter('5', $idOrg);
            }
        }
        if ($tipoCombustible) {
            $query
                ->join('c.tipoCombustible', 't')
                ->andWhere('t.id = ?6')
                ->setParameter('6', $tipoCombustible->getId());
        }

        return $query->getQuery()->execute();
    }

    public function findByPuntoFecha($desde, $hasta, $idOrg, $tipoCombustible = null, $puntocarga = null)
    {
        $desdeDate = new \DateTime($desde);
        $hastaDate = new \DateTime($hasta);
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')
            ->join('c.servicio', 's')
            ->join('c.puntoCarga', 'p')
            ->join('s.organizacion', 'o')
            ->andWhere('c.fecha >= ?1 AND c.fecha <= ?2')
            ->orderBy('c.fecha', 'DESC')
            //  ->orderBy('c.servicio', 'ASC')
            ->setParameter('1', $desdeDate)
            ->setParameter('2', $hastaDate);
        if ($puntocarga) {
            $query
                ->andWhere('p.id = ?3')
                ->setParameter('3', $puntocarga->getId());
        } else { //que solamente busque en referencias de ese usuario
            $query
                ->andWhere('o.id = ?5')
                ->setParameter('5', $idOrg);
        }
        if ($tipoCombustible) {
            $query
                ->join('c.tipoCombustible', 't')
                ->andWhere('t.id = ?6')
                ->setParameter('6', $tipoCombustible->getId());
        }

        return $query->getQuery()->execute();
    }

    public function findByServicioPuntoCarga($desde, $hasta, $organizacion, $servicios = null, $puntoscarga = null)
    {
        $desdeDate = new \DateTime($desde);
        $hastaDate = new \DateTime($hasta);
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('c')
            ->from('App:CargaCombustible', 'c')            
            ->join('c.servicio', 's')
            ->join('s.organizacion', 'o')
            ->andWhere('c.fecha >= ?1 AND c.fecha <= ?2')
            ->orderBy('c.fecha', 'ASC')
            //  ->orderBy('c.servicio', 'ASC')
            ->setParameter('1', $desdeDate)
            ->setParameter('2', $hastaDate);
            
        if ($servicios) {            
            $query                
                ->andWhere('s.id IN (?4)')
                ->setParameter('4', $servicios);
        }
        
        if ($puntoscarga) { //si vienen en null que solamente busque or org
            if ($puntoscarga) {
                $query
                    ->join('c.puntoCarga', 'p')
                    ->andWhere('p.id IN (?3)')
                    ->setParameter('3', $puntoscarga);
            }
        } else {
            $query
                ->andWhere('o.id = ?5')
                ->setParameter('5', $organizacion->getId());
        }

        return $query->getQuery()->getResult();
    }
}
