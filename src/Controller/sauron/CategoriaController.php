<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\sauron\CategoriaType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\CategoriaManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Description of CategoriaController
 *
 * @author yesica
 */
class CategoriaController extends AbstractController
{

    /**
     * @Route("/sauron/categoria/{idorg}/list", name="sauron_categoria_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function listAction(
        Request $request,
        $idorg,
        OrganizacionManager $organizacionManager,
        CategoriaManager $categoriaManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Categorias");

        //recupero la organizacion.
        $organizacion = $organizacionManager->find($idorg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Código de Organizacion no accesible.');
        }

        //obtengo las categorias asociadas a la organizacion.
        $categorias = $categoriaManager->findAll($organizacion);

        return $this->render('sauron/Categoria/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'categorias' => $categorias,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        $menu['Agregar Categoria'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('sauron_categoria_new', array(
                'idorg' => $organizacion->getId()
            ))
        );
        return $menu;
    }

    /**
     * @Route("/sauron/categoria/{id}/show", name="sauron_categoria_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function showAction(
        Request $request,
        $id,
        CategoriaManager $categoriaManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $categoria = $categoriaManager->findById($id);
        if (!$categoria) {
            throw $this->createNotFoundException('Código de Categoria no encontrado.');
        }

        $breadcrumbManager->push($request->getRequestUri(), $categoria->getNombre());

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sauron/Categoria/show.html.twig', array(
            'menu' => $this->getShowMenu($categoria),
            'categoria' => $categoria,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $categoria 
     * @return array $menu
     */
    private function getShowMenu($categoria)
    {
        $menu = array();
        $menu['Editar'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('sauron_categoria_edit', array(
                'id' => $categoria->getId()
            ))
        );

        if (count($categoria->getReferencias()) == 0) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return $menu;
    }

    /**
     * @Route("/sauron/categoria/{idorg}/show", name="sauron_categoria_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function newAction(
        Request $request,
        $idorg,
        CategoriaManager $categoriaManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $categoria = $categoriaManager->create($idorg);
        if (!$categoria) {
            throw $this->createNotFoundException('No se puede crear la categoria.');
        }
        $form = $this->createForm(CategoriaType::class, $categoria);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categoriaManager->save($categoria);
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nueva Categoria");

        return $this->render('sauron/Categoria/new.html.twig', array(
            'categoria' => $categoria,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/sauron/categoria/{id}/edit", name="sauron_categoria_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function editAction(
        Request $request,
        $id,
        CategoriaManager $categoriaManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $categoria = $categoriaManager->findById($id);
        if (!$categoria) {
            throw $this->createNotFoundException('Código de Categoria no encontrado.');
        }
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categoria = $categoriaManager->save($categoria);

            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($categoria->getNombre())));

            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }

        $breadcrumbManager->push($request->getRequestUri(), "Editar Categoria");

        return $this->render('sauron/Categoria/edit.html.twig', array(
            'categoria' => $categoria,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/categoria/{id}/delete", name="sauron_categoria_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function deleteAction(
        Request $request,
        $id,
        CategoriaManager $categoriaManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            if (!$categoriaManager->delete($id)) {
                $this->setFlash('error', 'La categoria esta siendo usada, no se puede eliminar.');
            }
        }
        return $this->redirect($categoriaManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
