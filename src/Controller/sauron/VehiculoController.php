<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Vehiculo;
use App\Form\app\VehiculoType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\OrganizacionManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class VehiculoController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Vehiculo');
    }

    /**
     * @Route("/sauron/vehiculo/{idservicio}/new", name="sauron_vehiculo_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_AGREGAR") 
     */
    public function newAction(
        Request $request,
        $idservicio,
        ServicioManager $ServicioManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $servicio = $ServicioManager->find($idservicio);

        $vehiculo = new Vehiculo();
        $vehiculo->setServicio($servicio);

        $form = $this->getVehiculoForm($servicio->getOrganizacion(), $vehiculo, $servicio, $organizacionManager);
        $form->handleRequest($request);

        $patente = $ServicioManager->existePatente($vehiculo->getPatente());

        if ($patente != null) {
            $this->setFlash('error', sprintf('La patente <b>%s</b> ya se encuentra registrada para <b>%s</b>', $vehiculo->getPatente(), $vehiculo->getServicio()));
        } else {
            if ($form->isSubmitted() && $form->isValid()) {
                $breadcrumbManager->pop();
                foreach ($vehiculo->getDatosExtra() as $dato) {
                    $dato->setVehiculo($vehiculo);
                }
                $servicio->setVehiculo($vehiculo);
                $em = $this->getEntityManager();
                $em->persist($form->getData());
                $em->flush();
                $this->setFlash('success', sprintf('La configuración del vehiculo <b>%s</b> ha sido exitoso.', strtoupper($vehiculo->getServicio())));

                if ($vehiculo->getOdometro() != null) {   //cambio el odometro por variable de ajuste
                    $ServicioManager->cambiarOdometro($servicio, $vehiculo->getOdometro());
                }
                if ($vehiculo->getHorometro() != null) {   //cambio el odometro por variable de ajuste
                    $ServicioManager->cambiarHorometro($servicio, $vehiculo->getHorometro());
                }

                return $this->redirect($this->generateUrl('servicio_show', array(
                    'id' => $vehiculo->getServicio()->getId(),
                    'tab' => 'main',
                )));
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), 'Nuevo Vehiculo');

        return $this->render('sauron/Vehiculo/new.html.twig', array(
            'servicio' => $servicio,
            'vehiculo' => $vehiculo,
            'form' => $form->createView()
        ));
    }

    private function getVehiculoForm($organizacion, $vehiculo, $servicio, $organizacionManager)
    {
        $options = array(
            'required_patente' => $organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_SHELL'),
            'required_portal' => $organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_PORTAL'),
            'new' => true,
            'em' => $this->getEntityManager(),
            'organizacion' => $vehiculo->getServicio()->getOrganizacion(),
            'requiredTanque' => ($servicio->getTipoMedidorCombustible() == 2 ||
                $servicio->getTipoMedidorCombustible() == 3) ? true : false
        );
        return $this->createForm(VehiculoType::class, $vehiculo, $options);
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
