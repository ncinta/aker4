<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\sauron\ServicioTanqueType;
use App\Entity\ServicioTanque;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\BreadcrumbManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class TanqueController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\ServicioTanque');
    }

    /**
     * @Route("/sauron//tanque/{id}/edit", name="sauron_tanque_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function editAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, ServicioManager $servicioManager)
    {

        $servicio = $servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        $form = $this->createForm(ServicioTanqueType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $d = $form->getData();
                $medicion = new ServicioTanque();
                $medicion->setServicio($servicio);
                $medicion->setPorcentaje(intval($d['porcentaje']));
                $medicion->setLitros(intval($d['litros']));
                $em = $this->getEntityManager();
                $em->persist($medicion);
                $em->flush();
                $this->setFlash('success', sprintf('Se ha agregado una medición de tanque a <b>%s</b>.', strtoupper($servicio)));
            } else {
                $this->setFlash('error', 'Error en la grabación de la medición');
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Config. Tanque");

        return $this->render('sauron/ServicioTanque/edit.html.twig', array(
            'servicio' => $servicio,
            'tanque' => $servicioManager->getMedicionestanque($servicio),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron//tanque/{id}/delete", name="sauron_tanque_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_ELIMINAR") 
     */
    public function deleteAction(Request $request, $id, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getEntityManager();
        $tanque = $em->getRepository('App:ServicioTanque')->find(array('id' => $id));
        if (!$tanque) {
            throw $this->createNotFoundException('Imposible encontrar la medición solicitada.');
        } else {
            $servicio = $tanque->getServicio();
            $em->remove($tanque);
            $em->flush();
        }
        $breadcrumbManager->pop();

        return $this->redirect($this->generateUrl('sauron_tanque_edit', array('id' => $servicio->getId())));
    }

    /**
     * @Route("/sauron//tanque/{id}/copyto", name="sauron_tanque_copyto",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function copytoAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        OrganizacionMAnager $organizacionManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Copiar a...");
        $servicio = $servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        if ($request->getMethod() == 'POST') {
            $servicioid = $request->get('new_servicio');
            $serviciohasta = $servicioManager->find(intval($servicioid));
            if (!is_null($serviciohasta)) {
                $breadcrumbManager->pop();
                if ($this->copyTanque($servicio, $serviciohasta, $servicioManager) === true) {
                    $this->setFlash('success', sprintf('Las mediciones han sido copiadas a <b>%s</b>', strtoupper($serviciohasta)));
                } else {
                    $this->setFlash('error', 'Error en la grabación de la copia de mediciones');
                }
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
        return $this->render('sauron/ServicioTanque/copyto.html.twig', array(
            'servicio' => $servicio,
            'datos' => $this->getEquipoTanque($servicioManager, $organizacionManager),
        ));
    }

    /**
     * @Route("/sauron//tanque/{id}/copyfrom", name="sauron_tanque_copyfrom",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function copyfromAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        OrganizacionMAnager $organizacionManager
    ) {
        $breadcrumbManager->push($request->getRequestUri(), "Copiar desde...");
        $servicio = $servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        if ($request->getMethod() == 'POST') {
            $servicioid = $request->get('new_servicio');
            $serviciodesde = $servicioManager->find(intval($servicioid));
            if (!is_null($serviciodesde)) {
                $breadcrumbManager->pop();
                if ($this->copyTanque($serviciodesde, $servicio, $servicioManager) === true) {
                    $this->setFlash('success', sprintf('Las mediciones han sido copiadas desde <b>%s</b>', strtoupper($serviciodesde)));
                } else {
                    $this->setFlash('error', 'Error en la grabación de la copia de mediciones');
                }
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        return $this->render('sauron/ServicioTanque/copyfrom.html.twig', array(
            'servicio' => $servicio,
            'datos' => $this->getEquipoTanque($servicioManager, $organizacionManager),
        ));
    }

    private function copyTanque($desde, $hasta, $servicioManager)
    {
        $mediciones = $servicioManager->getMedicionestanque($desde);
        if (!is_null($mediciones) && count($mediciones) > 0) {
            $em = $this->getEntityManager();
            foreach ($mediciones as $medicion) {
                $new = new ServicioTanque();
                $new->setServicio($hasta);
                $new->setPorcentaje($medicion->getPorcentaje());
                $new->setLitros($medicion->getLitros());
                $em->persist($new);
                $em->flush();
            }
            return true;
        } else {
            return true;
        }
    }

    private function getEquipoTanque($servicioManager, $organizacionManager)
    {
        $data = null;
        //aca obtengo datos de los destinos.
        $organizaciones = $organizacionManager->getTreeOrganizaciones();
        foreach ($organizaciones as $organizacion) {
            if ($organizacion->getTipoOrganizacion() == 2) {
                $servicios = $servicioManager->findAllByOrganizacion($organizacion, array('nombre' => 'ASC'));
                foreach ($servicios as $ser) {
                    if ($ser->getMedidorCombustible() && $ser->getEstado() == $ser::ESTADO_HABILITADO) {
                        $data[] = array(
                            'id' => $ser->getId(),
                            'label' => $organizacion->getNombre() . ' => ' . $ser->getNombre(),
                        );
                    }
                }
            }
        }
        return $data;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
