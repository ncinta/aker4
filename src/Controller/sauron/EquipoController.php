<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\sauron\EquipoType;
use App\Form\dist\ChipDisponibleType;
use App\Form\sauron\InformeEquipoStockType;
use App\Form\sauron\EquipoRetiroType;
use App\Form\sauron\ServicioSinAsignarType;
use App\Model\app\NotificadorAgenteManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\EquipoManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\BitacoraEquipoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\ChipManager;
use App\Model\app\LoggerManager;
use App\Model\app\ProgramacionManager;
use App\Entity\Equipo;

class EquipoController extends AbstractController
{

    private $notificadorAgenteManager;
    private $breadcrumbManager;
    private $equipoManager;
    private $servicioManager;
    private $userloginManager;
    private $utilsManager;
    private $organizacionManager;
    private $bitacoraEquipoManager;
    private $chipManager;
    private $loggerManager;
    private $programacionManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        EquipoManager $equipoManager,
        BitacoraEquipoManager $bitacoraEquipoManager,
        ServicioManager $servicioManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UtilsManager $utilsManager,
        ChipManager $chipManager,
        LoggerManager $loggerManager,
        ProgramacionManager $programacionManager
    ) {
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->equipoManager = $equipoManager;
        $this->servicioManager = $servicioManager;
        $this->userloginManager = $userloginManager;
        $this->utilsManager = $utilsManager;
        $this->organizacionManager = $organizacionManager;
        $this->bitacoraEquipoManager = $bitacoraEquipoManager;
        $this->chipManager = $chipManager;
        $this->loggerManager = $loggerManager;
        $this->programacionManager = $programacionManager;
    }

    /**
     * @Route("/sauron/equipo/list", name="sauron_equipo_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER")
     */
    public function listAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Listado Equipos");

        $organizacion = $this->userloginManager->getOrganizacion();
        $equipos = $this->equipoManager->findAll();
        $equiposDep = $this->equipoManager->findAllEnDeposito($organizacion);
        $servDisp = (array)$this->servicioManager->findAllServiciosSinAsignar($organizacion);

        return $this->render('sauron/Equipo/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'equipos' => $equipos,
            'equiposdep' => $equiposDep,
            'servicios_disponibles' => count($servDisp) > 0,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if (!is_null($organizacion) && $this->userloginManager->isGranted('ROLE_EQUIPO_AGREGAR')) {
            $menu['Agregar Equipo'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('sauron_equipo_new', array(
                    'idorg' => $organizacion->getId()
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/sauron/equipo/listall", name="sauron_equipo_list_all")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER")
     */
    public function listallAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Todos los Equipos");

        $data_modelos = array();
        $organizacion = $this->userloginManager->getOrganizacion();
        $treeOrg = $this->organizacionManager->getTreeOrganizaciones();
        $modelos = $this->equipoManager->getModelos();
        foreach ($modelos as $modelo) {   //lo recorre para cada modelo registrado
            $data = array();
            if (
                $this->userloginManager->isGranted('ROLE_SAURON_EQUIPO_VER_CARFINDER') &&
                $modelo->getTipoProgramacion() == $modelo::TPROG_CARFINDER
            ) {
                $equipos = $this->equipoManager->findAllByModelo($modelo, array('numeroSerie' => 'ASC'));
                //recorro todos los equipos.
                foreach ($equipos as $equipo) {
                    //determino si se puede ver la organizacion
                    $orgInTree = $this->organizacionManager->searchInTree($treeOrg, $equipo->getOrganizacion());  //para saber si esta la org en mi organizacion

                    $id = explode(':', $equipo->getMdmid());
                    if ($equipo->getServicio() != null) {
                        //obtengo los datos de la ultima trama.
                        $ultTrama = is_null($equipo->getServicio()->getUltTrama()) ? null : json_decode($equipo->getServicio()->getUltTrama(), true);
                        if ($ultTrama != null) {
                            $versionFirmware = isset($ultTrama['carfinder']['version_firmware']) ? $ultTrama['carfinder']['version_firmware'] : '';
                            $ultOdometro = isset($ultTrama['odometro']) ? $ultTrama['odometro'] / 1000 : '';
                            $ultTransmision = $this->utilsManager->fechaUTC2local($equipo->getServicio()->getUltFechahora())->format('d-m-Y H:i:s');
                            $ultBateria = $equipo->getServicio()->getUltBateria();
                            $ultServidor = isset($ultTrama['ip_capturador']) ? $ultTrama['ip_capturador'] : '';
                        }
                    } else {
                        $versionFirmware = $equipo->getVersionFirmware();
                        $ultOdometro = '---';
                        $ultTransmision = '---';
                        $ultBateria = '---';
                        $ultServidor = '---';
                    }

                    //armo los chips.
                    $chips = array();
                    foreach ($equipo->getChips() as $chip) {
                        $chips[] = array(
                            'imei' => $chip->getImei(),
                            'telefono' => $chip->getNumeroTelefono()
                        );
                    }

                    if ($orgInTree) {
                        $organizacion = $equipo->getOrganizacion();
                        if ($organizacion->getTipoOrganizacion() == 1) {
                            $organizacion = sprintf('<a href=<b>%s</b>>%s</a>', $this->generateUrl('distribuidor_show', array('id' => $organizacion->getId(), 'tab' => 'main')), $organizacion->getNombre());
                        } else {
                            $organizacion = sprintf('<a href=<b>%s</b>>%s</a>', $this->generateUrl('distribuidor_cliente_show', array('id' => $organizacion->getId(), 'tab' => 'main')), $organizacion->getNombre());
                        }
                    } else {
                        $organizacion = null;
                    }
                    $data[] = array(
                        'id' => (count($id) == 3) ? $id[1] : $equipo->getMdmid(),
                        'mdmid' => $orgInTree ? sprintf('<a href=<b>%s</b>>%s</a>', $this->generateUrl('sauron_equipo_show', array('id' => $equipo->getId())), $equipo->getMdmid()) : $equipo->getMdmid(),
                        'strEstado' => $equipo->getStrEstado(),
                        'organizacion' => $organizacion,
                        'chips' => $chips,
                    );
                }  //el equipo
            }  //el modelo
            //sgrego los equipos al modelo.
            if (count($data) > 0) {
                usort($data, array($this, 'compararById'));
                $data_modelos[] = array(
                    'nombre' => $modelo->getNombre(),
                    'equipos' => null,
                    //'equipos' => $data
                );
            }
            unset($data);
        }

        return $this->render('sauron/Equipo/list_all.html.twig', array(
            'menu' => array(),
            'modelos' => $data_modelos,
        ));
    }

    private function compararById($x, $y)
    {
        return (int) $x['id'] > (int) $y['id'];
    }

    /**
     * Se recupera el equipo asociado al id pasado por parametro. Si no se tiene
     * acceso o bien no hay equipo ingresado, se devuelve una excepcion.
     * @param type $id
     * @return Equipo $equipo
     */
    private function getEquipo($id)
    {
        $equipo = $this->equipoManager->find($id);

        if (!$equipo) {
            throw $this->createNotFoundException('Código de Equipo no encontrado.');
        }

        return $equipo;
    }

    /**
     * @Route("/sauron/equipo/{id}/show", name="sauron_equipo_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER")
     */
    public function showAction(Request $request, $id)
    {

        $equipo = $this->getEquipo($id);
        $this->breadcrumbManager->push($request->getRequestUri(), $equipo->getMdmid());

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sauron/Equipo/show.html.twig', array(
            'menu' => $this->getShowMenu($equipo),
            'equipo' => $equipo,
            'delete_form' => $deleteForm->createView(),
            'tab' => 'main',
        ));
    }

    /**
     * @Route("/sauron/equipo/{id}/show/{tab}", name="sauron_equipo_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER")
     */
    public function showbitacoraAction(Request $request, $id, $tab)
    {

        $equipo = $this->equipoManager->find($id);
        if (!$equipo) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        $this->breadcrumbManager->pop();
        $this->breadcrumbManager->push($request->getRequestUri(), $equipo->getMdmid() . '(Bitacora)');

        $registro = $this->bitacoraEquipoManager->getRegistros($equipo);
        $bitacora = array();
        foreach ($registro as $value) {
            $bitacora[] = array(
                'registro' => $value,
                'descripcion' => $this->bitacoraEquipoManager->parse($value),
            );
        }

        return $this->render('sauron/Equipo/show.html.twig', array(
            'menu' => $this->getShowMenu($equipo),
            'equipo' => $equipo,
            'delete_form' => $this->createDeleteForm($id)->createView(),
            'tab' => $tab,
            'bitacora' => $bitacora,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($equipo)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('sauron_equipo_edit', array(
                    'id' => $equipo->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_TRANSFERIR')) {
            $menu['Transferir'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('sauron_equipo_transferir', array(
                    'id' => $equipo->getId()
                ))
            );
        }
        if ($equipo->getServicio() == null) {
            if ($this->userloginManager->isGranted('ROLE_FLOTA_AGREGAR')) {
                $menu['Crear servicio'] = array(
                    'imagen' => '',
                    'url' => $this->generateUrl('servicio_new', array(
                        'idequipo' => $equipo->getId()
                    ))
                );
            }
            if ($this->userloginManager->isGranted('ROLE_EQUIPO_FLOTA_ASIGNAR')) {
                $qbServicios = $this->servicioManager->findAllServiciosSinAsignar($equipo->getOrganizacion());
                if ($qbServicios != null) {
                    $menu['Asignar servicio'] = array(
                        'imagen' => '',
                        'url' => $this->generateUrl('sauron_equipo_asignarservicio', array(
                            'id' => $equipo->getId()
                        ))
                    );
                }
            }
        } else {
            if ($this->userloginManager->isGranted('ROLE_EQUIPO_FLOTA_RETIRAR')) {
                $menu['Retirar Equipo'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => $this->generateUrl('sauron_equipo_retirar', array('id' => $equipo->getId())),
                );
            }
        }
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_CHIP_ASIGNAR')) {
            $menu['Asociar Chip'] = array(
                'imagen' => '',
                'url' => $this->generateUrl('sauron_equipo_asociarchip', array(
                    'idEquipo' => $equipo->getId(),
                    'crearservicio' => 0
                ))
            );
        }

        if ($this->userloginManager->isGranted('ROLE_EQUIPO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return $menu;
    }

    /**
     * Agrega un equipo en una organizacion determinada. {$idorg}
     * Se establece como propietario a la organizacion donde se esta creando.
     * En caso de que no sea asi, generarlo una transferencia de equipo.
     * 
     * @Route("/sauron/equipo/{idorg}/new", name="sauron_equipo_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_AGREGAR")
     */
    public function newAction(Request $request, $idorg)
    {

        $equipo = $this->equipoManager->create($idorg);
        if (!$equipo) {
            throw $this->createNotFoundException('Error en la creación del equipo.');
        }
        $org = $equipo->getOrganizacion();
        $options['organizaciones'] = $this->organizacionManager->getTreeOrganizaciones($org);
        $form = $this->createForm(EquipoType::class, $equipo, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!$this->equipoManager->ifExist($equipo->getMdmid())) {
                    if (!$this->equipoManager->ifExistImei($equipo->getImei())) {
                        $this->breadcrumbManager->pop();
                        if ($this->equipoManager->save($equipo)) {
                            $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                            $this->setFlash('success', sprintf('Se ha creado el equipo <b>%s</b> en el sistema.', strtoupper($equipo)));
                            if ($request->get('asociar_chip') == '1') {
                                if ($request->get('crear_servicio') === '1') {
                                    $servicio = 1;
                                } else {
                                    $servicio = 0;
                                }
                                return $this->redirect($this->generateUrl('sauron_equipo_asociarchip', array(
                                    'idEquipo' => $equipo->getId(),
                                    'crearservicio' => $servicio
                                )));
                            } else {
                                if ($request->get('crear_servicio') === '1') {
                                    return $this->redirect($this->generateUrl('servicio_new', array(
                                        'idequipo' => $equipo->getId(), 'tab' => 'main'
                                    )));
                                } else {
                                    return $this->redirect($this->generateUrl('sauron_equipo_show', array(
                                        'id' => $equipo->getId(),
                                        'tab' => 'main'
                                    )));
                                }
                            }
                        }
                    } else {
                        $this->setFlash('error', 'Ya existe un equipo con ese IMEI, verifique. Gracias.');
                    }
                } else {
                    $this->setFlash('error', 'Ya existe un equipo con ese Mdmid, verifique. Gracias.');
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Equipo");

        return $this->render('sauron/Equipo/new.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/sauron/equipo/{id}/edit", name="sauron_equipo_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_EDITAR")
     */
    public function editAction(Request $request, $id)
    {

        $equipo = $this->getEquipo($id);
        $org = $this->userloginManager->getOrganizacion();
        $options['organizaciones'] = $this->organizacionManager->getTreeOrganizaciones($org);
        $form = $this->createForm(EquipoType::class, $equipo, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->breadcrumbManager->pop();
                if ($this->equipoManager->save($equipo)) {
                    $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($equipo)));
                } else {
                    $this->setFlash('success', sprintf('No se ha podido actualiar los datos de %s', strtoupper($equipo)));
                }
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Equipo");

        return $this->render('sauron/Equipo/edit.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/equipo/{id}/delete", name="sauron_equipo_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_ELIMINAR")
     */
    public function deleteAction(Request $request, Equipo $equipo)
    {
        $form = $this->createDeleteForm($equipo->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $eq = new Equipo();
            $eq->setId($equipo->getId());
            $eq->setCreatedAt($equipo->getCreatedAt());
            $eq->setUpdatedAt($equipo->getUpdatedAt());

            $equipo = $this->equipoManager->delete($equipo->getId());
            if ($equipo !== null) {
                $notificar = $this->notificadorAgenteManager->notificar($eq, 2);
                $this->setFlash('success', 'El equipo ha sido eliminado.');
            }

            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/sauron/equipo/{idEquipo}/{crearservicio}/asociarchip", name="sauron_equipo_asociarchip")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_CHIP_ASIGNAR")
     */
    public function asociarchipAction(Request $request, $idEquipo, $crearservicio)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Asociar Chip");
        $equipo = $this->getEquipo($idEquipo);

        //esto trae todos los chips disponibles.
        $qbChips = $this->getDoctrine()->getManager()->getRepository('App:Chip')
            ->findAllChipsDisponibles($equipo->getOrganizacion());
        if ($qbChips != null) {
            $options['queryResult'] = $qbChips;
            $form = $this->createForm(ChipDisponibleType::class, null, $options);

            $form->handleRequest($request);
            // verificamos si el formulario enviado es valido
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $this->breadcrumbManager->pop();

                    if ($this->equipoManager->asociarChip($equipo, $request->get('chip'))) {
                        if ($crearservicio == '1') {
                            return $this->redirect($this->generateUrl('servicio_new', array(
                                'idequipo' => $equipo->getId()
                            )));
                        } else {
                            return $this->redirect($this->breadcrumbManager->getVolver());
                        }
                    } else {
                        $this->setFlash('error', 'No se ha podido asocia el chip al equipo.');
                        return $this->redirect($this->breadcrumbManager->getVolver());
                    }
                } else {
                    $this->setFlash('error', 'Los datos no son válidos');
                }
            } else {
                return $this->render('sauron/Equipo/asociarchip.html.twig', array(
                    'form' => $form->createView(),
                    'equipo' => $equipo,
                    'crearservicio' => $crearservicio,
                ));
            }
        } else {
            $this->setFlash('success', sprintf('No existen chips disponibles para asociar al equipo.'));
            if ($crearservicio == '1') {
                return $this->redirect($this->generateUrl('servicio_new', array(
                    'idequipo' => $equipo->getId()
                )));
            } else {
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/sauron/equipo/{idChip}/retirarchip", name="sauron_equipo_retirarchip")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_CHIP_RETIRAR")
     */
    public function retirarchipAction(Request $request, $idChip)
    {

        $this->breadcrumbManager->top();
        if ($this->chipManager->retirar($idChip)) {
            $this->setFlash('success', 'Chip retirado del equipo con éxito.');
        } else {
            $this->setFlash('success', 'Problemas al retirar el chip del equipo.');
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
        //return $this->redirect($this->generateUrl('sauron_equipo_show', array('id' => $id)));
    }

    /**
     * @Route("/sauron/equipo/{id}/transferirequipo", name="sauron_equipo_transferir")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_TRANSFERIR")
     */
    public function transferirequipoAction(Request $request, $id)
    {

        //este es el equipo que hay que transferir.
        $equipo = $this->getEquipo($id);

        if ($request->getMethod() == 'POST') {

            $this->breadcrumbManager->pop();

            $new_deposito_id = $request->get('new_organizacion');
            $tipo_transfer = $request->get('tipo_tranferencia');
            $crear_servicio = $request->get('crear_servicio');

            //sino se transfieren los chips entonces se deben dejar los chip donde estan.
            if ($request->get('transferir_chips') == '0') {
                foreach ($equipo->getChips() as $chip) {
                    $this->chipManager->retirar($chip);
                }
            }

            //transfiero el equipo.
            if ($this->equipoManager->transferir($id, $new_deposito_id, $tipo_transfer)) {
                $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                $this->setFlash('success', sprintf('El equipo <b>%s</b> ha sido transferido a la organizacion', strtoupper($equipo)));
                if ($crear_servicio === '1') {  //se tiene que crear el servicio.
                    return $this->redirect($this->generateUrl('servicio_new', array(
                        'idequipo' => $equipo->getId(),
                    )));
                } else {
                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Transferir Equipo");

        return $this->render('sauron/Equipo/transferir_equipo.html.twig', array(
            'user' => $this->userloginManager->getUser(),
            'organizaciones' => $this->organizacionManager->getTreeOrganizaciones(),
            'equipo' => $equipo,
        ));
    }

    /**
     *
     * @Route("/sauron/equipo/{id}/retiro", name="sauron_equipo_retirar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_RETIRAR") 
     */
    public function retiroequipoAction(Request $request, Equipo $equipo)
    {
        $form = $this->createForm(EquipoRetiroType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $retiro = $form->getData();
            if ($this->equipoManager->retirar($equipo->getId(), $retiro)) {
                $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                $this->setFlash('success', 'Equipo retirado del servicio');
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Error al retirar el equipo del servicio');
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Retiro Equipo");

        return $this->render('sauron/Equipo/retiroequipo.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/equipo/{id}/asignarservicio", name="sauron_equipo_asignarservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_ASIGNAR")
     */
    public function asignarservicioAction(Request $request, $id)
    {

        $equipo = $this->getEquipo($id);
        $this->breadcrumbManager->push($request->getRequestUri(), "Asignar Servicio");
        $qbServicios = (array)$this->servicioManager->findAllServiciosSinAsignar($equipo->getOrganizacion());
        if (!is_null($qbServicios) && count($qbServicios) >= 0) {
            $options['queryResult'] = $qbServicios;
            $form = $this->createForm(ServicioSinAsignarType::class, null, $options);

            // verificamos si el formulario enviado es valido
            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $idServicio = $request->get('servicio');
                    if ($this->equipoManager->asignarServicio($equipo, $idServicio["servicio"])) {
                        $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                        $this->setFlash('success', 'El equipo ha sido asociacido al servicio seleccionado');
                        return $this->redirect($this->breadcrumbManager->getVolver());
                    } else {
                        $this->setFlash('error', 'Error al asociar el servicio al equipo');
                    }
                } else {
                    $this->setFlash('error', 'Los datos no son válidos');
                }
            }
            return $this->render('sauron/Equipo/asignarservicio.html.twig', array(
                'form' => $form->createView(),
                'equipo' => $equipo,
            ));
        } else {
            $this->setFlash('success', sprintf('No existen servicios disponibles para asociar al equipo.'));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * @Route("/sauron/equipo/{id}/formstock", name="sauron_equipo_form_stock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_ASIGNAR")
     */
    public function formstockAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de equipos es stock");
        $organizacion = $this->userloginManager->getOrganizacion();
        //traigo todo los servicios existentes.
        $options['organizaciones'] = $this->organizacionManager->getTreeOrganizaciones(null, true);
        $form = $this->createForm(InformeEquipoStockType::class, null, $options);

        return $this->render('sauron/Equipo/form_stock.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/equipo/liststock", name="sauron_equipo_list_stock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_ASIGNAR")
     */
    public function liststockAction(Request $request)
    {

        $this->loggerManager->setTimeInicial();
        $organizacion = $this->userloginManager->getOrganizacion();
        //traigo todo los servicios existentes.
        $options['organizaciones'] = $this->organizacionManager->getTreeOrganizaciones(null, true);
        $form = $this->createForm(InformeEquipoStockType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $consulta['organizacion'] = $form->getData()['organizacion'];
                $consulta['destino'] = $form->getData()['destino'];
                $consulta['organizacion'] ? 0 : $consulta['organizacion'] = $organizacion->getId();
                $form_org = $this->organizacionManager->find($consulta['organizacion']);
                //se ejecuta la recoleccion de datos.
                $informe = $this->getDataDistribuidor($form_org);
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('sauron/Equipo/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '2':  //sale a grafico.
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {
                            $template = 'informe/Distancias/grafico_flota.html.twig';
                        } else {
                            //obtengo el servicio
                            $consulta['servicionombre'] = $this->servicioManager->find(intval($consulta['servicio']))->getnombre();
                            $template = 'informe/Distancias/grafico.html.twig';
                        }
                        return $this->rende(
                            $template,
                            array(
                                'consulta' => $consulta,
                                'time' => $this->loggerManager->getTimeGeneracion(),
                                'dt' => $this->getGraficoInforme($consulta, $informe)
                            )
                        );
                        break;
                    case '5': //sale a excel

                        break;
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
    }

    /**
     * Devuelve la estructura del distribuidor.
     * @param Organizacion $org
     * @return array $infor
     */
    private function getDataDistribuidor($org)
    {
        $dists = $clies = null;
        $orgs = $this->organizacionManager->findAll($org, array('nombre' => 'ASC'));
        foreach ($orgs as $child) {
            if ($child->getTipoOrganizacion() == 1) {
                $dists[] = $this->getDataDistribuidor($child, $this->organizacionManager);
            } else {   //es un cliente
                $c = $this->getDataCliente($child);
                if (!is_null($c)) {
                    $clies[] = $c;
                }
            }
        }
        return array(
            'distribuidor' => $org->getNombre(),
            'distribuidores' => $dists,
            'clientes' => $clies,
        );
    }

    /**
     * Devuelve los datos del los servicios del cliente que no reportan
     * @param  Organizacion $org
     * @return array $informe
     */
    private function getDataCliente($org)
    {
        $equipos = $this->equipoManager->findAllEnDepositoWithProveedorAndPropietario($org);
        if ($equipos == null) {
            return null;
        } else {
            return array('cliente' => $org->getNombre(), 'equipos' => $equipos);
        }
    }

    /**
     * @Route("/sauron/equipo/batch", name="sauron_equipo_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SECURITY")
     */
    public function batchAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $checksFirmware = $request->get('checks_firmware');
            $checksIpFota = $request->get('checks_act_ipfota');
            $checksActFirmware = $request->get('checks_act_firmware');
            $checksMerge = array_unique(array_merge((array) $checksFirmware, (array) $checksActFirmware, (array) $checksIpFota));

            //die('////<pre>' . nl2br(var_export($checksMerge, true)) . '</pre>////');

            $this->breadcrumbManager->pop();
            foreach ($checksMerge as $id) {
                $servicio = $this->servicioManager->find(intval($id));
                if ($this->programacionManager->initConfig($servicio)) {   //inicializo las progremotas.
                    if (!is_null($checksFirmware) && in_array($id, $checksFirmware)) {
                        $this->programacionManager->setQueryVersion($servicio);
                    }
                    if (!is_null($checksIpFota) && in_array($id, $checksIpFota)) {
                        $this->programacionManager->setIpFota($servicio);
                    }
                    if (!is_null($checksActFirmware) && in_array($id, $checksActFirmware)) {
                        $this->programacionManager->setActFirmware($servicio);
                    }
                }
                $idProg = $this->programacionManager->executeConfig();
                $res = $this->programacionManager->runProgramacion($idProg);
            }

            $this->setFlash('success', 'Se ha realizado la operación.');
            return $this->redirect($this->breadcrumbManager->getVolver());
        } else {
            $this->breadcrumbManager->push($request->getRequestUri(), "Pedir datos");
            $checks = explode(' ', $request->get('checks'));
            $servicios = array();
            foreach ($checks as $id) {
                $servicio = $this->servicioManager->find($id);
                if (!is_null($servicio->getEquipo())) {
                    $servicios[] = array(
                        'servicio' => $servicio,
                        'firmware' => $this->programacionManager->existVariableByCodename($servicio->getEquipo()->getModelo()->getId(), 'version'),
                    );
                }
            }
            //die('////<pre>' . nl2br(var_export($servicios, true)) . '</pre>////');
            return $this->render("sauron/Equipo/pedir_datos.html.twig", array(
                'servicios' => $servicios,
            ));
        }
    }
}
