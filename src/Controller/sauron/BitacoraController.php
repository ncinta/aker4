<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\sauron\BitacoraFormType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\BitacoraEquipoManager;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\BitacoraManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class BitacoraController extends AbstractController
{

    /**
     * @Route("/sauron/bitacora/{id}/list", name="sauron_bitacora_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function listAction(
        Request $request,
        $id,
        OrganizacionManager $organizacionManager,
        BitacoraManager $bitacoraManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $organizacion = $organizacionManager->find($id);
        $breadcrumbManager->push($request->getRequestUri(), "Bitacora Gral");
        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());
        $evento = 0;
        $ok = true;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('bitacoraform');
            $ok = $utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
            }
            $evento = $form['evento'];
        }
        if ($ok) {
            $eventos = array();
            //traigo las organizaciones hijas
            $child = $organizacionManager->getTreeOrganizaciones($organizacion);
            foreach ($child as $orgChild) {
                $datos = $bitacoraManager->obtener($orgChild, $fecha_desde . ' 00:00:00', $fecha_hasta . ' 23:59:59', $evento);
                foreach ($datos as $value) {
                    $eventos[] = array(
                        'nombre' => $orgChild->getNombre(),
                        'fecha' => !is_null($value->getCreatedAt()) ? $value->getCreatedAt() : null,
                        'operador' => $value->getEjecutor(),
                        'tipoEvento' => $value->getStrTipoEvento(),
                        'descripcion' => $bitacoraManager->parse($value),
                    );
                }
            }
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
            $eventos = null;
        }
        $options['eventos'] = $bitacoraManager->getTipoEventos();

        return $this->render('sauron/Bitacora/list.html.twig', array(
            'organizacion' => $organizacion,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'eventos' => $eventos,
            'form' => $this->createForm(BitacoraFormType::class, null, $options)->createView(),
        ));
    }


    /**
     * @Route("/sauron/bitacoraservicio/{id}/list", name="sauron_bitacoraservicio_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function listservicioAction(
        Request $request,
        $id,
        BitacoraServicioManager $bitacoraservicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $organizacion = $organizacionManager->find($id);

        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());
        $evento = 0;
        $ok = true;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('bitacoraform');
            $ok = $utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
            }
            $evento = $form['evento'];
        }
        if ($ok) {
            $eventos = array();
            //traigo las organizaciones hijas
            $child = $organizacionManager->getTreeOrganizaciones($organizacion);
            $fdesde = $utilsManager->datetime2sqltimestamp($fecha_desde . ' 00:00:00');
            $fhasta = $utilsManager->datetime2sqltimestamp($fecha_hasta . ' 23:59:59');
            foreach ($child as $orgChild) {
                $datos = $bitacoraservicioManager->obtener($orgChild, $fdesde, $fhasta, $evento);
                foreach ($datos as $value) {
                    $eventos[] = array(
                        'nombre' => $orgChild->getNombre(),
                        'operador' => $value->getEjecutor(),
                        'fecha' => $value->getCreatedAt(),
                        'tipoEvento' => $value->getStrTipoEvento(),
                        'servicio' => $value->getServicio(),
                        'descripcion' => $bitacoraservicioManager->parse($value),
                    );
                }
            }
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
            $eventos = null;
        }

        $breadcrumbManager->push($request->getRequestUri(), "Bitacora de Servicios");

        $options['eventos'] = $bitacoraservicioManager->getTipoEventos();


        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());

        return $this->render('sauron/Bitacora/list_servicio.html.twig', array(
            'organizacion' => $organizacion,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'eventos' => $eventos,
            'form' => $this->createForm(BitacoraFormType::class, null, $options)->createView(),
        ));
    }

    /**
     * @Route("/sauron/bitacoraequipo/{id}/list", name="sauron_bitacoraequipo_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function listequipoAction(
        Request $request,
        $id,
        BitacoraEquipoManager $bitacoraequipoManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $organizacion = $organizacionManager->find($id);
        $breadcrumbManager->push($request->getRequestUri(), "Bitacora de Equipos");
        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());
        $evento = 0;
        $ok = true;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('bitacoraform');
            $ok = $utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
            }
            $evento = $form['evento'];
        }
        if ($ok) {
            $eventos = array();
            //traigo las organizaciones hijas
            $child = $organizacionManager->getTreeOrganizaciones($organizacion);
            foreach ($child as $orgChild) {
                $datos = $bitacoraequipoManager->obtener($orgChild, $fecha_desde . ' 00:00:00', $fecha_hasta . ' 23:59:59', $evento);
                foreach ($datos as $value) {
                    $eventos[] = array(
                        'nombre' => $orgChild->getNombre(),
                        'fecha' => $value->getCreatedAt(),
                        'operador' => $value->getEjecutor(),
                        'tipoEvento' => $value->getStrTipoEvento(),
                        'equipo' => $value->getEquipo(),
                        'descripcion' => $bitacoraequipoManager->parse($value),
                    );
                }
            }
            //die('////<pre>' . nl2br(var_export($eventos, true)) . '</pre>////');
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
            $eventos = null;
        }

        $options['eventos'] = $bitacoraequipoManager->getTipoEventos();
        //die('////<pre>'.nl2br(var_export($options, true)).'</pre>////');
        $breadcrumbManager->push($request->getRequestUri(), "Bitacora de Equipos");

        return $this->render('sauron/Bitacora/list_equipo.html.twig', array(
            'organizacion' => $organizacion,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'eventos' => $eventos,
            'form' => $this->createForm(BitacoraFormType::class, null, $options)->createView(),
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
