<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Form\sauron\GrupoReferenciaType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of GrupoReferenciaController
 *
 * @author yesica
 */
class GrupoReferenciaController extends AbstractController {

    private function getEntityManager() {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository() {
        return $this->getEntityManager()->getRepository('App\Entity\GrupoReferencia');
    }

    private function getSecurityContext() {
        return $this->get('security.token_storage');
    }

    /**
     * @Route("/sauron/grupo_referencia/{idorg}/list", name="sauron_grupo_referencia_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_VER")
     */
    public function listAction(Request $request, $idorg, BreadcrumbManager $breadcrumbManager, GrupoReferenciaManager $gruporeferenciaManager,
            UserloginManager $userloginManager, OrganizacionManager $organizacionManager) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Grupos");

        $organizacion = $organizacionManager->find($idorg);
        $gruposref = $gruporeferenciaManager->findAsociadas($organizacion);

        return $this->render('sauron/GrupoReferencia/list.html.twig', array(
                    'menu' => $this->getListMenu($organizacion, $userloginManager),
                    'gruposref' => $gruposref,
                    'organizacion' => $organizacion,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, $userloginManager) {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_AGREGAR')) {
            $menu['Agregar Grupo'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('sauron_grupo_referencia_new', array(
                    'idorg' => $organizacion->getId())));
        }
        if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_COMPARTIR')) {
            $menu['Compartir'] = array(
                'imagen' => '',
                'extra' => 'onclick=batch(' . $organizacion->getId() . ',"compartir") id=btnCompartir style=display:none',
                'url' => '#compartir',
            );
        }
        if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_COPIAR')) {
            $menu['Copiar'] = array(
                'imagen' => '',
                'extra' => 'onclick=batch(' . $organizacion->getId() . ',"copiar") id=btnCopiar style=display:none',
                'url' => '#copiar',
            );
        }
        if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_MOVER')) {
            $menu['Mover'] = array(
                'imagen' => '',
                'extra' => 'onclick=batch(' . $organizacion->getId() . ',"mover") id=btnMover style=display:none',
                'url' => '#mover',
            );
        }

        return $menu;
    }

    /**
     * @Route("/sauron/grupo_referencia/{idgrp}/{idorg}/show", name="sauron_grupo_referencia_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_VER")
     */
    public function showAction(Request $request, $idgrp, $idorg, BreadcrumbManager $breadcrumbManager, GrupoReferenciaManager $gruporeferenciaManager,
            UserloginManager $userloginManager, OrganizacionManager $organizacionManager) {

        $grupo = $gruporeferenciaManager->find($idgrp);
        $organizacion = $organizacionManager->find($idorg);
        if (!$grupo) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $breadcrumbManager->push($request->getRequestUri(), $grupo->getNombre());

        return $this->render('sauron/GrupoReferencia/show.html.twig', array(
                    'menu' => $this->getShowMenu($grupo, $organizacion, $userloginManager),
                    'gruporef' => $grupo,
                    'delete_form' => $this->createDeleteForm($idgrp)->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $grupoRef 
     * @return array $menu
     */
    private function getShowMenu($grupoRef, $organizacion, $userloginManager) {
        $menu = array();
        if ($grupoRef->getPropietario()->getId() == $organizacion->getId()) {
            if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_EDITAR')) {
                $menu['Editar'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl('sauron_grupo_referencia_edit', array(
                        'id' => $grupoRef->getId())));
            }
            if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_AGRUPAR')) {
                $menu['Agrupar Referencias'] = array(
                    'imagen' => '',
                    'url' => $this->generateUrl('sauron_grupo_referencia_agrupar', array(
                        'idgrp' => $grupoRef->getId(),
                        'idorg' => $organizacion->getId())));
            }
            if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_ELIMINAR')) {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
        return $menu;
    }

    /**
     * @Route("/sauron/grupo_referencia/{idorg}/new", name="sauron_grupo_referencia_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGREGAR")
     */
    public function newAction(Request $request, $idorg, BreadcrumbManager $breadcrumbManager, GrupoReferenciaManager $gruporeferenciaManager,
            OrganizacionManager $organizacionManager) {

        $organizacion = $organizacionManager->find($idorg);
        $grupoRef = $gruporeferenciaManager->create($organizacion);
        $form = $this->createForm(GrupoReferenciaType::class, $grupoRef);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $gruporeferenciaManager->save();

            $this->setFlash('success', sprintf('Se ha creado el grupo <b>%s</b> en el sistema.', strtoupper($grupoRef->getNombre())));
            return $this->redirect($this->generateUrl('sauron_grupo_referencia_agrupar', array(
                                'idgrp' => $grupoRef->getId(),
                                'idorg' => $organizacion->getId())));
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Grupo");

        return $this->render('sauron/GrupoReferencia/new.html.twig', array(
                    'gruporef' => $grupoRef,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/sauron/grupo_referencia/{id}/edit", name="sauron_grupo_referencia_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_EDITAR")
     */
    public function editAction(Request $request, $id, BreadcrumbManager $breadcrumbManager,
            GrupoReferenciaManager $gruporeferenciaManager) {

        $grupoRef = $gruporeferenciaManager->find($id);
        $form = $this->createForm(GrupoReferenciaType::class, $grupoRef);
        $form->handleRequest($request);
        if (!$grupoRef) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $gruporeferenciaManager->save();
            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($grupoRef->getNombre())));

            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Editar Grupo");

        return $this->render('sauron/GrupoReferencia/edit.html.twig', array(
                    'gruporef' => $grupoRef,
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/grupo_referencia/{idgrp}/{idorg}/delete", name="sauron_grupo_referencia_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_ELIMINAR")
     */
    public function deleteAction(Request $request, $idgrp, $idorg, BreadcrumbManager $breadcrumbManager,
            GrupoReferenciaManager $gruporeferenciaManager) {

        $grupoRef = $gruporeferenciaManager->find($idgrp);
        if (!$grupoRef) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $form = $this->createDeleteForm($idgrp);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $gruporeferenciaManager->delete($grupoRef);
        }

        return $this->redirect($this->generateUrl('sauron_grupo_referencia_list', array('idorg' => $idorg)));
    }

    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
                        ->getForm()
        ;
    }

    protected function setFlash($action, $value) {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/sauron/grupo_referencia/{idgrp}/{idorg}/agrupar", name="sauron_grupo_referencia_agrupar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGRUPAR")
     */
    public function agruparAction(Request $request, $idgrp, $idorg, BreadcrumbManager $breadcrumbManager, GrupoReferenciaManager $gruporeferenciaManager,
            OrganizacionManager $organizacionManager, ReferenciaManager $referenciaManager) {

        $grupo = $gruporeferenciaManager->find($idgrp);
        $organizacion = $organizacionManager->find($idorg);
        if (!$grupo || !$organizacion) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $breadcrumbManager->push($request->getRequestUri(), "Agrupar Referencias");

        $disponibles = $referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion);
        $asignados = $referenciaManager->findAllByGrupo($grupo);

        return $this->render('sauron/GrupoReferencia/agrupar.html.twig', array(
                    'grupo' => $grupo,
                    'disponibles' => $disponibles,
                    'asignados' => $asignados,
                    'organizacion' => $organizacion,
        ));
    }

    /**
     * @Route("/sauron/grupo_referencia/asignar", name="sauron_grupo_referencias_asignarreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGRUPAR")
     */
    public function asignarReferenciaAction(Request $request, GrupoReferenciaManager $gruporeferenciaManager,
            OrganizacionManager $organizacionManager, ReferenciaManager $referenciaManager) {

        $grupo = $gruporeferenciaManager->find($request->get('idgrp'));
        $organizacion = $organizacionManager->find($request->get('idorg'));

        $idsReferencias = unserialize($request->get('referencias'));
        if ($grupo && count($idsReferencias) > 0) {
            foreach ($idsReferencias as $key => $value) {
                $gruporeferenciaManager->addReferencia($value);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('sauron/GrupoReferencia/show_disponibles.html.twig', array(
                'disponibles' => $referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion)
            )),
            'asignados' => $this->renderView('sauron/GrupoReferencia/show_asignados.html.twig', array(
                'asignados' => $referenciaManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/sauron/grupo_referencia/retirar", name="sauron_grupo_referencias_retirarreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_DESAGRUPAR")
     */
    public function retirarReferenciaAction(Request $request, GrupoReferenciaManager $gruporeferenciaManager,
            OrganizacionManager $organizacionManager, ReferenciaManager $referenciaManager) {

        $grupo = $gruporeferenciaManager->find($request->get('idgrp'));
        $organizacion = $organizacionManager->find($request->get('idorg'));

        $idsReferencias = unserialize($request->get('referencias'));
        if ($grupo && count($idsReferencias) > 0) {
            foreach ($idsReferencias as $key => $value) {
                $gruporeferenciaManager->removeReferencia($value);
            }
        }

        $respuesta = array(
            'disponibles' => $this->render('sauron/GrupoReferencia/show_disponibles.html.twig', array(
                'disponibles' => $referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion)
            )),
            'asignados' => $this->render('sauron/GrupoReferencia/show_asignados.html.twig', array(
                'asignados' => $referenciaManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/sauron/grupo_referencia/{idref}/{idgrp}/{idorg}/sacargrupo", name="sauron_grupo_referencias_referencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_DESAGRUPAR")
     */
    public function sacarreferenciaAction($idref, $idgrp, $idorg, GrupoReferenciaManager $gruporeferenciaManager,
            ReferenciaManager $referenciaManager, BreadcrumbManager $breadcrumbManager) {

        $referencia = $referenciaManager->find($idref);
        if (!$referencia) {
            throw $this->createNotFoundException('Código de Referencia no encontrado.');
        }
        $grupo = $gruporeferenciaManager->find($idgrp);
        if (!$grupo) {
            throw $this->createNotFoundException('Código de Grupo no encontrado.');
        }

        $gruporeferenciaManager->removeReferencia($referencia);

        $breadcrumbManager->pop();

        return $this->redirect($breadcrumbManager->getVolver());
    }

    /**
     * @Route("/sauron/grupo_referencia/batch", name="sauron_grupo_referencias_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_SHARE")
     */
    public function batchAction(Request $request, GrupoReferenciaManager $gruporeferenciaManager, UserLoginManager $userloginManager,
            OrganizacionManager $organizacionManager, BreadcrumbManager $breadcrumbManager) {

        $checks = $request->get('checks');
        $accion = $request->get('accion');
        $idorg = $request->get('idorg');
        if ($request->getMethod() == 'POST') {   //aca se llega por post al retorno de la vista.
            $breadcrumbManager->pop();

            $organizacion = $organizacionManager->find($request->get('new_organizacion'));
            $result = true;
            foreach ($checks as $key => $value) {
                if ($key >= 0) {
                    if ($accion == 'copiar') {
                        $result = $result && $gruporeferenciaManager->copiar($value, $organizacion);
                    } elseif ($accion == 'mover') {
                        $result = $result && $gruporeferenciaManager->mover($value, $organizacion);
                    } elseif ($accion == 'compartir') {
                        $result = $result && $gruporeferenciaManager->compartir($value, $organizacion);
                    } else {
                        $this->setFlash('error', 'Operacion no soportada');
                        return $this->redirect($breadcrumbManager->getVolver());
                    }
                }
            }

            if ($result == true) {
                $this->setFlash('success', sprintf('Se ha realizado la operacion de <b>%s</b> para los grupos hacia la Organizacion <b>%s</b>', strtoupper($accion), strtoupper($organizacion->getNombre())));
            } else {
                $this->setFlash('error', 'Se ha producido un error en la copia de las referencias');
            }
            return $this->redirect($breadcrumbManager->getVolver());
        } else {
            $organizacion = $organizacionManager->find($idorg);
            $orgLogin = $userloginManager->getOrganizacion();
            $checks = explode(' ', $request->get('checks'));
            $grupos = array();
            foreach ($checks as $id) {
                $grupo = $gruporeferenciaManager->find($id);
                if (in_array($accion, array('copiar', 'mover')) && $idorg == $orgLogin->getId()) {   //estoy trabajando sobre una cuenta mia.
                    //el propietario del grupo esta en mi arbol de organizacion ???
                    if (!$organizacionManager->inTreeOrganizacion($orgLogin, $grupo->getPropietario())) {
                        //no, esta....
                        $id = -1;
                    }
                }

                if ($id >= 0) {
                    $grupos[] = array(
                        'id' => $id,
                        'nombre' => $grupo->getNombre());
                }
            }
            if (count($grupos) > 0) {   //tiene que haber algun grupo para procesar.
                $breadcrumbManager->push($request->getRequestUri(), $accion . " grupo");

                //aca armo el listado de todas las distribuciones a las que se puede mandar el grupo, listas para ser mostradas en la vista.            
                $tree = $organizacionManager->getTreeOrganizaciones();
                foreach ($tree as $org) {
                    if ($orgLogin->getId() == $org->getId()) {
                        $treeOrg[] = array(
                            'id' => $orgLogin->getId(),
                            'nombre' => $orgLogin->getNombre() . ' (Yo) ',
                        );
                    } else {
                        $str = '';
                        if ($org->getId() != $idorg) {    //para no hacer operaciones con la misma organizacion
                            $str = ($org->getTipoOrganizacion() == 1 ? ' (Dist) ' : ' (Cliente) ');
                        }
                        $treeOrg[] = array(
                            'id' => $org->getId(),
                            'nombre' => $org->getNombre() . $str,
                        );
                    }
                }

                return $this->render("sauron/GrupoReferencia/transferir_batch.html.twig", array(
                            'organizaciones' => $treeOrg,
                            'organizacion' => $userloginManager->getOrganizacion(),
                            'funcion' => $accion,
                            'checks' => $checks,
                            'grupos' => $grupos,
                ));
            } else {  //no se puede procesar ningun grupo, asi que vuelvo al list.
                $this->setFlash('error', sprintf('No hay grupos en condiciones de %s', $accion));
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
    }

    /**
     * @Route("/sauron/grupo_referencia/{idgrp}/{idorg}/remover", name="sauron_grupo_referencia_remover")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_DESAGRUPAR")
     */
    public function removerAction(Request $request,$idgrp, $idorg, GrupoReferenciaManager $gruporeferenciaManager,
            OrganizacionManager $organizacionManager, BreadcrumbManager $breadcrumbManager) {

        $organizacion = $organizacionManager->find($idorg);
        $grupo = $gruporeferenciaManager->find($idgrp);
        $result = $gruporeferenciaManager->remover($grupo, $organizacion);
        if (!$result) {
            $this->setFlash('error', 'Se ha producido un error al remover el grupo');
        } else {
            $this->setFlash('success', sprintf('Se ha removido el grupo de la organización <b>%s</b>', $organizacion));
        }
        $breadcrumbManager->push($request->getRequestUri(), $grupo->getNombre());
        return $this->redirect($breadcrumbManager->getVolver());
    }

}
