<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\sauron\InputsType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ServicioInputManager;
use App\Model\app\SensorManager;
use App\Model\app\UserLoginManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InputsController extends AbstractController
{

    private $breadcrumbManager;
    private $userloginManager;
    private $servicioinputManager;
    private $sensorManager;
    private $servicioManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        UserloginManager $userloginManager,
        ServicioInputManager $servicioinputManager,
        SensorManager $sensorManager,
        ServicioManager $servicioManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userloginManager = $userloginManager;
        $this->servicioinputManager = $servicioinputManager;
        $this->sensorManager = $sensorManager;
        $this->servicioManager = $servicioManager;
    }

    /**
     * @Route("/sauron/inputs/{id}/show", name="sauron_inputs_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_VER")
     */
    public function showAction(Request $request, $id)
    {

        $input = $this->servicioinputManager->findById($id);
        $this->breadcrumbManager->push($request->getRequestUri(), $input->getNombre());
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('sauron/Inputs/show.html.twig', array(
            'menu' => $this->getShowMenu($input),
            'input' => $input,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $input 
     * @return array $menu
     */
    private function getShowMenu($input)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_SENSOR_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('sauron_inputs_edit', array(
                    'id' => $input->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_SENSOR_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/sauron/inputs/{id}/new", name="sauron_inputs_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_AGREGAR")
     */
    public function newAction(Request $request, $id)
    {

        $servicio = $this->servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('No se tiene acceso a este servicio');
        }
        $input = $this->servicioinputManager->create($servicio);
        $options['modelos'] = $this->sensorManager->getTiposSensores($servicio, 2);  //digital
        $form = $this->createForm(InputsType::class, $input, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //obtengo el modeloInputs
                $data = $request->get('inputs');
                $modSens = $this->sensorManager->getModeloSensor($data['modeloSensor']);
                $input->setModeloSensor($modSens);
                $this->breadcrumbManager->pop();
                $input = $this->servicioinputManager->save($input);
                if ($input) {
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        sprintf('Se ha creado la entrada digital <b>%s</b> en el servicio <b>%s</b>.', strtoupper($input), strtoupper($servicio))
                    );
                    return $this->redirect($this->breadcrumbManager->getVolver());
                } else {
                    $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para crear la entrada digital <b>%s</b> en el sistema.', strtoupper($input)));
                }
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Inputs");

        return $this->render('sauron/Inputs/new.html.twig', array(
            'input' => $input,
            'iconos' => glob('images/input_on_*.png'),
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/sauron/inputs/{id}/edit", name="sauron_inputs_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_EDITAR")
     */
    public function editAction(Request $request, $id)
    {

        $input = $this->servicioinputManager->findById(array('id' => $id));
        if (!$input) {
            throw $this->createNotFoundException('Código de Inputs no encontrado.');
        }
        $options['modelos'] = $this->sensorManager->getTiposSensores($input->getServicio(), 2);  //digital
        $form = $this->createForm(InputsType::class, $input, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->breadcrumbManager->pop();
                $input = $this->servicioinputManager->save($input);
                if ($input) {
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($input)));
                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Inputs");

        return $this->render('sauron/Inputs/edit.html.twig', array(
            'input' => $input,
            'iconos' => glob('images/input_on_*.png'),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/inputs/{id}/delete", name="sauron_inputs_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_ELIMINAR")
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->servicioinputManager->deleteById($id);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
