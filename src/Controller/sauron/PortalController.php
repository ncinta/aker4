<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\sauron\PortalCambioPrecioType;
use App\Form\sauron\PortalType;
use App\Entity\PrecioPortal as PrecioPortal;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\PortalManager;
use App\Model\app\UserLoginManager;
use App\Model\app\TipoVehiculoManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class PortalController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/sauron/portal/{idref}/list", name="sauron_portal_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function listAction(
        Request $request,
        $idref,
        BreadcrumbManager $breadcrumbManager,
        PortalManager $portalManager,
        UserLoginManager $userloginManager,
        ReferenciaManager $referenciaManager
    ) {

        if ($idref != 0) {
            $referencia = $referenciaManager->find($idref);
        } else {
            $referencia = null;
            $breadcrumbManager->pop();
        }

        //obtengo todos los porticos
        $porticos = $portalManager->findAllByReferencia();

        //obtengo las refererencias de los porticos.
        $referencias[0] = 'Todos...';
        foreach ($porticos as $ref) {
            $referencias[$ref->getReferencia()->getId()] = $ref->getReferencia()->getNombre();
        }

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Porticos y Peajes");

        return $this->render('sauron/Portal/list.html.twig', array(
            'menu' => $this->getListMenu($userloginManager),
            'portales' => $portalManager->findAllByReferencia($idref),
            'referencia' => $referencia,
            'referencias' => $referencias,
        ));
    }

    /**
     * Arma y retorna el menu de opciones que se muestra en el list.
     * @return menu
     */
    public function getListMenu($userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_ADMIN_PORTAL')) {
            $menu['Agregar Portal'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('sauron_portal_new')
            );
        }
        return $menu;
    }

    /**
     * @Route("/sauron/portal/new", name="sauron_portal_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function newAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        PortalManager $portalManager,
        ReferenciaManager $referenciaManager
    ) {
        //uso el form como esqueleto de datos, no para validación de los mismos.
        $form = $this->createForm(PortalType::class);

        $breadcrumbManager->push($request->getRequestUri(), "Agregar Portal");

        return $this->render('sauron/Portal/new.html.twig', array(
            'portales' => $referenciaManager->getPortales(),
            'diasSemana' => $portalManager->getArrayStrDiasSemana(),
            'tiposVehiculos' => $this->getEntityManager()->getRepository('App:TipoVehiculo')->findAll(array(), array('tipo', 'asc')),
            'tipoPortal' => $portalManager->getArrayTipoPortal(),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/portal/new", name="sauron_portal_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function createAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        PortalManager $portalManager,
        ReferenciaManager $referenciaManager,
        TipoVehiculoManager $tipoVehiculoManager
    ) {

        $precioportal = $request->get('precioportal');

        if ($precioportal != null && $request->getMethod() == 'POST') {
            $breadcrumbManager->pop();
            $portal = new PrecioPortal();
            $portal->setReferencia($referenciaManager->find($precioportal['referencia']));
            $portal->setTipoVehiculo($tipoVehiculoManager->findById($precioportal['tipoVehiculo']));
            $portal->setTipoPortal($precioportal['tipoPortal']);
            $portal->setDiasSemana($precioportal['dias']);
            $portal->setDesde(new \DateTime($precioportal['desde']));
            $portal->setHasta(new \DateTime($precioportal['hasta']));
            $portal->setPrecio($precioportal['precio']);

            if ($portalManager->save($portal)) {
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($portal->getReferencia()->getNombre())));
                return $this->redirect($breadcrumbManager->getVolver());
            }
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }

        $form = $this->createForm(new PortalType());

        return $this->render('sauron/Portal/new.html.twig', array(
            'portales' => $referenciaManager->getPortales(),
            'diasSemana' => $portalManager->getArrayStrDiasSemana(),
            'tiposVehiculos' => $this->getEntityManager()->getRepository('App:TipoVehiculo')->findAll(array(), array('tipo', 'asc')),
            'tipoPortal' => $portalManager->getArrayTipoPortal(),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/portal/{id}/edit", name="sauron_portal_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function editAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        PortalManager $portalManager,
        ReferenciaManager $referenciaManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Editar Portal");

        $portal = $portalManager->find($id);
        if (!$portal) {
            throw $this->createNotFoundException('Código de Portal no encontrado.');
        }

        //uso el form como esqueleto de datos, no para validación de los mismos.
        $form = $this->createForm(PortalType::class);

        return $this->render('sauron/Portal/edit.html.twig', array(
            'portal' => $portal,
            'portales' => $referenciaManager->getPortales(),
            'diasSemana' => $portalManager->getArrayStrDiasSemana(),
            'tiposVehiculos' => $this->getEntityManager()->getRepository('App:TipoVehiculo')->findAll(array(), array('tipo', 'asc')),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/portal/{id}/update", name="sauron_portal_update")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function updateAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        PortalManager $portalManager,
        ReferenciaManager $referenciaManager,
        TipoVehiculoManager $tipoVehiculoManager
    ) {

        $precioportal = $request->get('precioportal');

        $portal = $portalManager->find($id);
        if (!$portal) {
            throw $this->createNotFoundException('Código de Portal no encontrado.');
        }

        if ($precioportal != null && $request->getMethod() == 'POST') {
            $breadcrumbManager->pop();

            $portal->setReferencia($referenciaManager->find($precioportal['referencia']));
            $portal->setTipoVehiculo($tipoVehiculoManager->findById($precioportal['tipoVehiculo']));
            $portal->setTipoPortal($precioportal['tipoPortal']);
            $portal->setDiasSemana($precioportal['dias']);
            $portal->setDesde(new \DateTime($precioportal['desde']));
            $portal->setHasta(new \DateTime($precioportal['hasta']));
            $portal->setPrecio($precioportal['precio']);

            if ($portalManager->save($portal)) {
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($portal->getReferencia()->getNombre())));
                return $this->redirect($breadcrumbManager->getVolver());
            }
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }

        $form = $this->createForm(new PortalType());

        return $this->render('sauron/Portal/edit.html.twig', array(
            'portal' => $portal,
            'portales' => $referenciaManager->getPortales(),
            'diasSemana' => $portal->getArrayStrDiasSemana(),
            'tiposVehiculos' => $this->getEntityManager()->getRepository('App:TipoVehiculo')->findAll(array(), array('tipo', 'asc')),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/portal/{id}/cambiarprecio", name="sauron_portal_cambiar_precio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_PORTAL")
     */
    public function cambiarprecioAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, PortalManager $portalManager)
    {

        $portal = $portalManager->find($id);
        if (!$portal) {
            throw $this->createNotFoundException('No se tiene acceso a este portal');
        }
        $form = $this->createForm(PortalCambioPrecioType::class, $portal);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //llego aca porque se tiene que cambiar el precio del portal.
            $formportal = $request->get('precioportal');
            $portal->setPrecio($formportal['precio']);
            $portalManager->save($portal);
            $this->setFlash('success', sprintf('Se ha cambiado el precio del portal <b>%s</b>', strtoupper($portal->getReferencia()->getNombre())));
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Cambiar Precio");

        return $this->render('sauron/Portal/cambiarprecio.html.twig', array(
            'portal' => $portal,
            'form' => $form->createView()
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
