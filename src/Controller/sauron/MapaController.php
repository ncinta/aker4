<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
//use de mapas
use GMaps\Map;
//use propias
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ReferenciaFormManager;
use App\Model\app\GeocoderManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MapaController extends AbstractController
{

    /**
     * @Route("/sauron/mapa/patente", name="sauron_mapa_patente")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function patenteAction(Request $request, BreadcrumbManager $breadcrumbManager, ReferenciaManager $referenciaManager)
    {

        $breadcrumbManager->push($request->getRequestUri(), "Control Ubicación");
        //tengo que incorporar todas las referencias 
        $referencias = $referenciaManager->findEstacionesMapa();   //solo las estaciones.
        return $this->render('sauron/Mapa/patente.html.twig', array(
            'referencias' => $referencias,
        ));
    }

    /**
     * @Route("/sauron/mapa/patente/consulta", name="sauron_mapa_patente_consultar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function patenteconsultaAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        OrganizacionManager $organizacionManager,
        ReferenciaManager $referenciaManager,
        GeocoderManager $geocoderManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Control Ubicación");
        $mapa = $gmapsManager->createMap();
        $mapa->setIncludeJQuery(true);        //para referencias
        $mapa->setZoomChangedListener('cambioZoom');
        $mapa->setIniZoom(12);


        $patente = trim($request->get('inputPatente'));
        $optionsRef = $request->get('optionsRadios');

        //obtengo el servicio asociado a la patente.
        $servicio = $servicioManager->findByPatente($patente);
        //inicio los ckequeos de consulta.
        if (!is_null($servicio)) {
            $centroServicio = array(
                'latitud' => $servicio->getUltLatitud(),
                'longitud' => $servicio->getUltLongitud(),
            );
            if (!$servicio->getUltLatitud()) {
                $error = 'El equipo no ha reportado nunca.';
            } else {
                //aca checkeo que el servicio este en mi arbol.
                $servicio = $servicioManager->setTimezone($servicio);
                $org = $servicio->getOrganizacion();
                $myTree = $organizacionManager->getTreeOrganizaciones();
                $ids = array();
                foreach ($myTree as $o) {
                    $ids[] = $o->getId();
                }

                if (!array_search($org->getId(), $ids, true)) {
                    $error = 'El equipo esta fuera de su alcance. Consulte a su distribuidor';
                }
            }
        } else {
            $error = 'El equipo que esta consultando no existe.';
        }

        //proceso sobre que referencia debo trabajar.
        if ($optionsRef == 'all') {
            $referencias = $referenciaManager->findEstacionesMapa();   //solo las estaciones.
            if (!$referencias) {
                $error = 'No hay estaciones cargadas para controlar.';
            }
        } else {
            $idRef = $request->get('selectReferencia');
            $referencias[] = $referenciaManager->find($idRef);
        }


        if (!isset($error)) {
            $min_lat = $max_lat = $min_lng = $max_lng = false;
            $marker = $gmapsManager->addMarkerServicio($mapa, $servicio, true);

            $cercanas = $servicioManager->buscarCercania($servicio, $referencias, 5);
            foreach ($cercanas as $key => $punto) {
                //agrego la estacion como visible
                $gmapsManager->addMarkerReferencia($mapa, $referenciaManager->find($punto['id']), true);

                //meto la polinilea en el mapa.
                $gmapsManager->addCamino($mapa, $punto['id'], array(
                    isset($centroServicio) ? $centroServicio : array(),
                    array('latitud' => $punto['latitud'], 'longitud' => $punto['longitud'])
                ), $key);
            }
            $min_lat = min($servicio->getUltlatitud(), $cercanas[0]['latitud']);
            $max_lat = max($servicio->getUltlatitud(), $cercanas[0]['latitud']);
            $min_lng = min($servicio->getUltlongitud(), $cercanas[0]['longitud']);
            $max_lng = max($servicio->getUltlongitud(), $cercanas[0]['longitud']);
            $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);

            //obtengo la direccion del vehiculo

            $direc = $geocoderManager->inverso($servicio->getUltlatitud(), $servicio->getUltLongitud());

            $calle = $direc;


            //agrego las otras referencias por si las quiere ver en el mapa.
            foreach ($referencias as $referencia) {
                //agrego las referencias
                $gmapsManager->addMarkerReferencia($mapa, $referencia, false);
            }

            return $this->render('sauron/Mapa/patentemapa.html.twig', array(
                'mapa' => $mapa,
                'servicio' => $servicio,
                'calle' => $calle,
                'cercanas' => $cercanas,
            ));
        } else {
            //no se cumple alguna condicion.
            return $this->render('sauron/Mapa/patente.html.twig', array(
                'referencias' => $referencias = $referenciaManager->findEstacionesMapa(),
                'error' => $error
            ));
        }
    }

    /**
     * @Route("/sauron/mapa/{idorg}/flota", name="sauron_mapa_flota")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function flotaAction(
        Request $request,
        $idorg,
        BreadcrumbManager $breadcrumbManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        OrganizacionManager $organizacionManager,
        ReferenciaManager $referenciaManager,
        UserLoginManager $userloginManager,
        ReferenciaFormManager $referenciaformManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Flota en mapa");
        $mapa = $gmapsManager->createMap();
        //cambio de zoom

        $organizacion = $organizacionManager->find($idorg);

        //tengo que incorporar todas las referencias en el mapa, luego las muestro
        //u oculto por ajax. Pero si o si tiene que estar credas.
        $referencias = $referenciaManager->findAsociadas($organizacion);
        foreach ($referencias as $referencia) {
            //agrego las referencias
            $gmapsManager->addMarkerReferencia($mapa, $referencia, false, false);
        }

        //grabo el idorg en la session para que se perdurre en los refresh
        $request->getSession()->set('sauron_mapa_organizacion', $idorg);

        //empiezo el tratamiento de los servicios.
        $servicios = $servicioManager->findAll($organizacion);
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $status = array();
        foreach ($servicios as $servicio) {
            if (!$servicio->getUltLatitud())
                continue; // omitir si el equipo nunca reportó posición



            //armo el stats para el servio
            if ($servicio->getUltfechahora()) {
                $intervalo = date_diff(date_create(), date_create($servicio->getUltfechahora()->format('Y-m-d H:i:s')));
                $intervalo = $intervalo->format('%d');
            } else {
                $intervalo = -1;
            }
            $status[$servicio->getId()] = array(
                'st_ultreporte' => $intervalo,
                'direccion' => '',
                'icono' => $gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            );

            $marker = $gmapsManager->addMarkerServicio($mapa, $servicio, true);

            if ($min_lat === false) {
                $min_lat = $max_lat = $servicio->getUltLatitud();
                $min_lng = $max_lng = $servicio->getUltLongitud();
            } else {
                $min_lat = min($min_lat, $servicio->getUltLatitud());
                $max_lat = max($max_lat, $servicio->getUltLatitud());
                $min_lng = min($min_lng, $servicio->getUltLongitud());
                $max_lng = min($max_lng, $servicio->getUltLongitud());
            }
        }
        $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);

        return $this->render('sauron/Mapa/flota.html.twig', array(
            'mapa' => $mapa,
            'servicios' => $servicios,
            'organizacion' => $organizacion,
            'distribuidor' => $userloginManager->getOrganizacion(),
            'status' => $status,
            'referencias' => $referencias,
            'formcr_referencia' => $referenciaformManager->createReferenciaForm($organizacion)->createView(),
            'formrr_referencia' => $referenciaformManager->createRedibujoForm($organizacion)->createView(),
        ));
    }

    /**
     * @Route("/sauron/mapa/equiposAJAX", name="sauron_mapa_equiposAJAX")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function equiposAJAXAction(Request $request, OrganizacionManager $organizacionManager, GmapsManager $gmapsManager)
    {
        $idorg = $request->getSession()->get('sauron_mapa_organizacion');
        $organizacion = $organizacionManager->find($idorg);
        $servicios = $gmapsManager->findAll($organizacion);

        $respuesta = array();
        foreach ($servicios as $servicio) {
            $respuesta[] = array(
                'id_sc' => 'servicio_' . $servicio->getId(),
                'latitud' => $servicio->getUltLatitud(),
                'longitud' => $servicio->getUltLongitud(),
                'texto' => $servicio->getUltVelocidad() . ' km/h',
                'new_icono' => $gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            );
        }
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/sauron/mapa/refresh", name="sauron_mapa_equipos_refresh")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function refreshAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GeocoderManager $geocoderManager
    ) {

        $mapa = new Map();

        $idorg = $request->getSession()->get('sauron_mapa_organizacion');
        $organizacion = $organizacionManager->find($idorg);
        $servicios = $servicioManager->findAll($organizacion);
        $referencias = $referenciaManager->findAsociadas($organizacion);

        // si tengo que refrescar con direccion.
        $direcciones = array();
        $status = array();
        foreach ($servicios as $servicio) {

            //armo el stats para el servicio
            if ($servicio->getUltfechahora()) {
                $intervalo = date_diff(date_create(), date_create($servicio->getUltfechahora()->format('Y-m-d H:i:s')));
                $intervalo = $intervalo->format('%d');
            } else {
                $intervalo = -1;
            }
            $status[$servicio->getId()]['st_ultreporte'] = $intervalo;
            $status[$servicio->getId()]['icono'] = $gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad());

            //aca empieza el tema de la direccion.
            if ($request->get('ver') == '1') {
                $status[$servicio->getId()]['direccion'] = $geocoderManager->inverso($servicio->getNombre(), $servicio->getUltLatitud(), $servicio->getUltLongitud());
            }

            if ($request->get('cercana') == '1') {
                $cercanas = $servicioManager->buscarCercania($servicio, $referencias);
                $i = 0;
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        break;
                    }
                }
                if (!is_null($cercanas)) {
                    $status[$servicio->getId()]['cerca']['leyenda'] = $cercanas[$i]['leyenda'];
                    $status[$servicio->getId()]['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                }
            }
        }

        $response = $this->render('sauron/Mapa/flota_list.html.twig', array(
            'servicios' => $servicios,
            'organizacion' => $organizacion,
            'status' => $status,
            'mapa' => $mapa,
        ));
        return $response;
    }

    /**
     * @Route("/sauron/mapa/referenciasviewAJAX", name="sauron_referencias_viewAJAX")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MASTER_DISTRIBUIDOR")
     */
    public function referenciasviewAJAXAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        GmapsManager $gmapsManager,
        ReferenciaManager $referenciaManager
    ) {

        if ($request->get('visible') == '1') {
            $visible = true;
        } else {
            $visible = false;
        }

        //para referencias
        $idorg = $request->getSession()->get('sauron_mapa_organizacion');
        $organizacion = $organizacionManager->find($idorg);
        $referencias = $referenciaManager->findAsociadas($organizacion);

        //aca armo la el array con los datos
        $respuesta[] = $gmapsManager->castVisibilidadReferencias($referencias, $visible);

        return new Response(json_encode($respuesta));
    }
}
