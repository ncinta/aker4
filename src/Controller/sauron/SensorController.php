<?php

namespace App\Controller\sauron;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Sensor as Sensor;
use App\Entity\Servicio as Servicio;
use App\Form\sauron\SensorType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\SensorManager;

class SensorController extends AbstractController
{

    /**
     * @Route("/sauron/sensor/{id}/show", name="sauron_sensor_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_VER") 
     */
    public function showAction(
        Request $request,
        Sensor $sensor,
        UserLoginManager $userlogin,
        BreadcrumbManager $breadcrumbManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), $sensor->getNombre());
        $deleteForm = $this->createDeleteForm($sensor->getId());
        return $this->render('sauron/Sensor/show.html.twig', array(
            'menu' => $this->getShowMenu($sensor, $userlogin),
            'sensor' => $sensor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $sensor 
     * @return array $menu
     */
    private function getShowMenu($sensor, $userlogin)
    {
        $menu = array();
        if ($userlogin->isGranted('ROLE_SENSOR_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('sauron_sensor_edit', array(
                    'id' => $sensor->getId()
                ))
            );
        }
        if ($userlogin->isGranted('ROLE_SENSOR_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/sauron/sensor/{id}/new", name="sauron_sensor_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_AGREGAR") 
     */
    public function newAction(
        Request $request,
        Servicio $servicio,
        SensorManager $sensorManager,
        UserLoginManager $userlogin,
        BreadcrumbManager $breadcrumbManager
    ) {
        if (!$servicio) {
            throw $this->createNotFoundException('No se tiene acceso a este servicio');
        }
        $sensor = $sensorManager->create($servicio);
        $options['modelos'] = $sensorManager->getTiposSensores($servicio, 1);  //analogicos
        $form = $this->createForm(SensorType::class, $sensor, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //obtengo el modeloSensor
            $data = $request->get('sensor');
            $modSens = $sensorManager->getModeloSensor($data['modeloSensor']);
            $sensor->setModeloSensor($modSens);
            $breadcrumbManager->pop();
            if ($sensorManager->save($sensor)) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado el sensor <b>%s</b> en el servicio <b>%s</b>.', strtoupper($sensor), strtoupper($servicio))
                );
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para crear el sensor <b>%s</b> en el sistema.', strtoupper($sensor)));
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Sensor");
        return $this->render('sauron/Sensor/new.html.twig', array(
            'sensor' => $sensor,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/sauron/sensor/{id}/edit", name="sauron_sensor_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_EDITAR") 
     */
    public function editAction(
        Request $request,
        Sensor $sensor,
        SensorManager $sensorManager,
        UserLoginManager $userlogin,
        BreadcrumbManager $breadcrumbManager
    ) {
        $options['modelos'] = $sensorManager->getTiposSensores($sensor->getServicio(), 1);
        $form = $this->createForm(SensorType::class, $sensor, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                if ($sensorManager->save($sensor)) {
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($sensor)));
                    return $this->redirect($breadcrumbManager->getVolver());
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $breadcrumbManager->push($request, "Editar Sensor");
        return $this->render('sauron/Sensor/edit.html.twig', array(
            'sensor' => $sensor,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/sauron/sensor/{id}/delete", name="sauron_sensor_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SENSOR_ELIMINAR") 
     */
    public function deleteAction(
        Request $request,
        Sensor $sensor,
        SensorManager $sensorManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        $form = $this->createDeleteForm($sensor->getId());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $sensorManager->deleteById($sensor->getId());
        }
        return ($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
