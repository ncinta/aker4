<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Entity\MotivoActividadGpm as MotivoActividadGpm;
use App\Model\app\BreadcrumbManager;
use App\Model\app\Router\MotivoRouter;
use App\Form\gpm\MotivoActividadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of MotivoController
 *
 * @author nicolas
 */
class MotivoController extends AbstractController
{

    private $breadcrumbManager;
    private $motivoRouter;

    function __construct(BreadcrumbManager $breadcrumbManager, MotivoRouter $motivoRouter)
    {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->motivoRouter = $motivoRouter;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\MotivoActividadGpm');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/gpm/motivos/{id}/list", name="gpm_motivogpm_list")
     * @Method({"GET", "POST"}) 
     */
    public function listmotivosAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de motivos de actividades");
        $menu = array(
            1 => array(
                $this->motivoRouter->btnNew($organizacion),
            )
        );
        return $this->render('gpm/MotivosGpm/list.html.twig', array(
            'menu' => $menu,
            'organizacion' => $organizacion
        ));
    }

    /**
     * @Route("/gpm/{idorg}/newmotivo", name="gpm_motivogpm_new")
     * @Method({"GET", "POST"})
     */
    public function newmotivoAction(Request $request, $idorg)
    {
        $em = $this->getEntityManager();
        $organizacion = $em->getRepository('App:Organizacion')
            ->findOrganizacionById($idorg);

        $motivo = new \App\Entity\MotivoActividadGpm();
        $motivo->setOrganizacion($organizacion);

        $form = $this->createForm(MotivoActividadType::class, $motivo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $em->persist($motivo);
            $em->flush();

            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo motivo de actividad");
        return $this->render('gpm/MotivosGpm/new.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/gpm/motivo/{id}/edit", name="gpm_motivogpm_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function editmotivoAction(Request $request, MotivoActividadGpm $motivo)
    {
        $em = $this->getEntityManager();

        if (!$motivo) {
            throw $this->createNotFoundException('Código de Motivo no encontrado.');
        }
        $form = $this->createForm(MotivoActividadType::class, $motivo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $em->persist($motivo);
            $em->flush();
            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($motivo->getNombre())));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Modificar Categoria");
        return $this->render('gpm/MotivosGpm/edit.html.twig', array(
            'motivo' => $motivo,
            'form' => $form->createView(),
        ));
    }
}
