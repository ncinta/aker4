<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Proyecto;
use App\Entity\ActividadGpm;
use App\Model\gpm\ProyectoManager;
use App\Model\gpm\ActividadManager;
use App\Model\gpm\PersonalManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ContratistaManager;
use App\Model\app\BackendManager;
use App\Model\gpm\ReferenciaGpmManager;
use App\Model\app\Router\ProyectoRouter;
use App\Form\gpm\ProyectoNewType;
use App\Form\gpm\ActividadType;
use App\Form\gpm\ActividadEditType;
use App\Form\gpm\ReferenciaGpmType;
use App\Form\gpm\IngresoManualType;
use App\Model\app\ReferenciaFormManager;
use GMaps\Geocoder;
use Doctrine\Common\Util\ClassUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ActividadController
 *
 * @author nicolas
 */
class ActividadController extends AbstractController
{

    protected $colores = array(
        1 => '#0178d3',
        2 => '#0A0',
        3 => '#A00',
        4 => '#00A',
        5 => '#000',
        6 => '#FFF',
    );
    protected $actividadManager = null;
    protected $personalManager = null;
    protected $breadcrumbManager = null;
    protected $contratistaManager = null;
    protected $organizacionManager = null;
    protected $proyectoManager = null;
    protected $backendManager = null;
    protected $referenciaGpmManager = null;
    protected $gmapsManager = null;
    protected $utilsManager = null;
    protected $userloginManager = null;
    protected $referenciaFormManager = null;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ActividadManager $actividadManager,
        GmapsManager $gmapsManager,
        UserLoginManager $userloginManager,
        ProyectoManager $proyectoManager,
        OrganizacionManager $organizacionManager,
        ReferenciaGpmManager $referenciaGpmManager,
        ReferenciaFormManager $referenciaFormManager,
        PersonalManager $personalManager,
        ContratistaManager $contratistaManager,
        BackendManager $backendManager,
        UtilsManager $utilsManager
    ) {
        $this->actividadManager = $actividadManager;
        $this->proyectoManager = $proyectoManager;
        $this->personalManager = $personalManager;
        $this->contratistaManager = $contratistaManager;
        $this->organizacionManager = $organizacionManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->referenciaGpmManager = $referenciaGpmManager;
        $this->gmapsManager = $gmapsManager;
        $this->utilsManager = $utilsManager;
        $this->userloginManager = $userloginManager;
        $this->referenciaFormManager = $referenciaFormManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\ActividadGpm');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/gpm/actividad/{id}/new", name="gpm_actividad_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Proyecto $proyecto)
    {
        $em = $this->getEntityManager();
        $organizacion = $proyecto->getOrganizacion();
        $configuracion = $organizacion->getConfigGpm()->getVista_actividad();
        $options = array(
            'organizacion' => $organizacion,
            'em' => $em,
            'configuracion' => $configuracion,
            'contratista' => $proyecto->getContratista(),
        );
        $form = $this->createForm(ActividadType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $proy = $this->proyectoManager->updateEstadoActividades($organizacion->getProyectos()); //actualizo estado de actividades para ver si el personal se desocupó
            $act = $this->actividadManager->find(intval($request->get('actividad')['id']));
            $actividad = $this->actividadManager->save($act, $request->get('actividad'));
            $this->setFlash('success', sprintf('Actividad creada correctamente'));

            $this->breadcrumbManager->pop();
            return $this->redirect($this->generateUrl('gpm_proyecto_show', array('id' => $actividad->getProyecto()->getId())));
        }

        $actividad = $this->actividadManager->create($proyecto);
        $formReferencia = $this->getReferenciaForm(null, $this->userloginManager);
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nueva Actividad");

        return $this->render('gpm/Actividad/new.html.twig', array(
            'actividad' => $actividad,
            'organizacion' => $organizacion,
            'mapa' => $this->getMapa(),
            'geocoder' => new Geocoder(),
            'form' => $form->createView(),
            'iconos' => glob('images/iconos/*.*'),
            'formReferencia' => $formReferencia->createView(),
            'configuracion' => $configuracion,
        ));
    }

    public function getMapa($referencia = null)
    {
        $mapa = $this->gmapsManager->createMap(true);
        $mapa->setSize('100%', '250px');
        $mapa->setIniZoom(10);
        if (!is_null($referencia)) {
            if (is_null($referencia->getLatitud()) || $referencia->getLatitud() == 0) {
                $referencia->setLatitud($referencia->getPropietario()->getLatitud());
                $referencia->setLongitud($referencia->getPropietario()->getLongitud());
            }
            $mapa->setIniLatitud($referencia->getPropietario()->getLatitud() != null ? $referencia->getPropietario()->getLatitud() : -32.890722);
            $mapa->setIniLongitud($referencia->getPropietario()->getLongitud() != null ? $referencia->getPropietario()->getLongitud() : -68.839352);
        }
        return $mapa;
    }

    private function getReferenciaForm($referencia, $userlogin)
    {
        $options = array(
            'organizacion' => $userlogin->getOrganizacion(),
            'em' => $this->getEntityManager(),
        );
        return $this->createForm(ReferenciaGpmType::class, null, $options);
    }

    /**
     * @Route("/gpm/actividad/{id}/edit", name="gpm_actividad_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getEntityManager();
        $actividad = $this->actividadManager->find($id); //busco la actividad porque si la paso por parametro me maneja el objeto cuando viene por post
        $organizacion = $this->userloginManager->getOrganizacion();
        $configuracion = $organizacion->getConfigGpm()->getVista_actividad();

        $options = array(
            'em' => $em,
            'organizacion' => $organizacion,
            'configuracion' => $configuracion,
            'contratista' => $actividad->getContratista(),
        );
        $form = $this->createForm(ActividadType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $proy = $this->proyectoManager->updateEstadoActividades($organizacion->getProyectos()); //actualizo estado de actividades para ver si el personal se desocupó
            $actividad = $this->actividadManager->save($actividad, $request->get('actividad'));
            $this->setFlash('success', sprintf('Actividad modificada correctamente'));
            $this->breadcrumbManager->pop();

            return $this->redirect($this->generateUrl('gpm_actividad_show', array('id' => $actividad->getId())));
        }

        $formReferencia = $this->getReferenciaForm(null, $this->userloginManager);
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Modificar Actividad");

        return $this->render('gpm/Actividad/edit.html.twig', array(
            'actividad' => $actividad,
            'organizacion' => $organizacion,
            'mapa' => $this->getMapa(),
            'geocoder' => new Geocoder(),
            'form' => $form->createView(),
            'iconos' => glob('images/iconos/*.*'),
            'formReferencia' => $formReferencia->createView(),
            'configuracion' => $configuracion,
            'trabajos' => $actividad->getPersonal(),
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($this->userloginManager->getOrganizacion())->createView()
        ));
    }

    /**
     * @Route("/gpm/actividad/{id}/copiar", name="gpm_actividad_copiar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function copiarAction(Request $request, $id)
    {
        $em = $this->getEntityManager();
        $organizacion = $this->userloginManager->getOrganizacion();
        $configuracion = $organizacion->getConfigGpm()->getVista_actividad();
        $act = $this->actividadManager->find($id); //busco la actividad porque si la paso por parametro me maneja el objeto cuando viene por post
        $actividad = clone $act;
        $options = array(
            'organizacion' => $organizacion,
            'em' => $em,
            'configuracion' => $configuracion,
            'contratista' => $actividad->getContratista(),
        );

        $form = $this->createForm(ActividadType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $proy = $this->proyectoManager->updateEstadoActividades($organizacion->getProyectos()); //actualizo estado de actividades para ver si el personal se desocupó
            //    die('////<pre>' . nl2br(var_export($request->get('actividad'), true)) . '</pre>////');
            $act = $this->actividadManager->find(intval($request->get('actividad')['id']));
            $actividad = $this->actividadManager->save($act, $request->get('actividad'));
            $this->setFlash('success', sprintf('Actividad modificada correctamente'));
            $this->breadcrumbManager->pop();
            return $this->redirect($this->generateUrl('gpm_actividad_show', array('id' => $actividad->getId())));
        }

        $actividad->setNombre($act->getNombre() . ' Copia');
        $actividad->setCodigo($act->getCodigo());
        $actividad->setResponsable($act->getResponsable());
        $actividad->setContratista($act->getContratista());
        $actividad->setId(null);
        $actividad->setEstado(0);
        $formReferencia = $this->getReferenciaForm(null, $this->userloginManager);
        $actividad = $this->actividadManager->persist($actividad);
        foreach ($act->getPersonal() as $pers) {
            $personal = new \App\Entity\Personal();
            $personal->setPersona($pers->getPersona());
            //  die('////<pre>' . nl2br(var_export($pers->getPersona()->getId(),true)) . '</pre>////');
            $personal->setActividad($actividad);
            $this->getEntityManager()->persist($personal);
            $this->getEntityManager()->flush();
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Copiar Actividad");

        return $this->render('gpm/Actividad/copiar.html.twig', array(
            'actividad' => $actividad,
            'act' => $act,
            'organizacion' => $organizacion,
            'mapa' => $this->getMapa(),
            'geocoder' => new Geocoder(),
            'form' => $form->createView(),
            'iconos' => glob('images/iconos/*.*'),
            'formReferencia' => $formReferencia->createView(),
            'configuracion' => $configuracion,
        ));
    }

    /**
     * @Route("/gpm/actividad/{id}/show", name="gpm_actividad_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getEntityManager();
        $actividad = $this->actividadManager->find($id); //busco la actividad porque si la paso por parametro me maneja el objeto cuando viene por post
        $organizacion = $actividad->getProyecto()->getOrganizacion();
        $configuracion = $organizacion->getConfigGpm()->getVista_actividad();
        $referencias = array();
        $puntos = array();
        $servicios = array();
        $movimientos = null;
        $movim = null;
        $acumuladores = null;
        $informe = null;
        $desde = $actividad->getFechaInicio() != null ? date_format($actividad->getFechaInicio(), 'Y/m/d H:i:s') : null;
        $hasta = $actividad->getFechaFin() != null ? date_format($actividad->getFechaFin(), 'Y/m/d H:i:s') : null;

        if ($actividad->getActgpmRefgpm()) {
            foreach ($actividad->getActgpmRefgpm() as $actref) {
                if ($actref->getReferencia() != null) {
                    $referencias[] = $actref->getReferencia();
                    $puntos[] = array('latitud' => $actref->getReferencia()->getLatitud(), 'longitud' => $actref->getReferencia()->getLongitud());
                }
            }
        }

        if ($actividad->getActgpmServicios()) {
            foreach ($actividad->getActgpmServicios() as $serv) {
                $servicios[] = $serv->getServicio();
            }
        }

        $mapa = $this->crear_mapa($puntos);
        //trae el historial si esta cerrada o finalizada
        if ($actividad->getEstado() === 3 || $actividad->getEstado() === 2) {
            $movimientos = $this->getMovimientos($actividad);
            $informe = $this->agregar_recorrido($mapa, $servicios, $desde, $hasta);
            $this->agregar_detenciones($mapa, $movimientos);
            $acumuladores = $movimientos['acumuladores'];
            $movim = $movimientos['movimientos'];
        }
        foreach ($referencias as $referencia) {
            $this->agregar_referencias($mapa, $referencia);
        }

        $formManual = $this->createForm(IngresoManualType::class);

        foreach ($actividad->getIngresos() as $ingreso) {
            $ingreso->setTiempo($this->utilsManager->minutos2tiempo($ingreso->getTiempo()));
            $ingreso->setDistancia(($ingreso->getDistancia() / 1000) . ' kms');
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $actividad->getNombre());

        return $this->render('gpm/Actividad/show.html.twig', array(
            'iconos' => glob('images/iconos/*.*'),
            'actividad' => $actividad,
            'organizacion' => $organizacion,
            'mapa' => $mapa,
            'informe' => $informe,
            'movimientos' => $movim,
            'totales' => $acumuladores,
            'configuracion' => $configuracion,
            'formManual' => $formManual->createView(),
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($this->userloginManager->getOrganizacion())->createView(),
        ));
    }

    private function crear_mapa($puntos)
    {
        $mapa = $this->gmapsManager->createMap();
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $this->gmapsManager->fitBounds($mapa, $puntos);

        return $mapa;
    }

    private function agregar_detenciones($mapa, $movimientos)
    {
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        foreach ($movimientos as $vehiculo) {
            foreach ($vehiculo as $key => $movimiento) {
                $id = 1;
                if (isset($movimiento['detenciones'])) {
                    foreach ($movimiento['detenciones'] as $id => $detencion) {
                        $detencion['direccion'] = '0';
                        $detencion['velocidad'] = '0';
                        $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $detencion, $icon);
                        $mark->setTitle($movimiento['nombre'] . ' detencion ' . $id . ' (' . (new \DateTime($detencion['fecha_ingreso']))->format('d/m/Y H:i') . '),' . (intval($detencion['segundos_detenido']) / 60) . 'min');

                        if ($detencion['referencia_id'] != null) {
                            $this->gmapsManager->addMarkerReferencia($mapa, $this->referenciaGpmManager->find($detencion['referencia_id']), true);
                        }

                        $this->gmapsManager->setFitBounds($mapa, $detencion['latitud'], $detencion['longitud']);
                        $id++;
                    }
                }
            }
        }
        return $mapa;
    }

    private function agregar_referencias($mapa, $referencia)
    {
        //agrego las referencias al mapa
        $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, true, true, 0.3);
        $mapa_ = $this->gmapsManager->setClusterable($mapa, true);
        return $mapa;
    }

    private function agregar_recorrido($mapa, $servicios, $desde, $hasta)
    {
        $arr_servicios = array();
        $arr_polilineas = array();
        $i = 1;
        foreach ($servicios as $servicio) {
            $historial = $this->getHistorial($servicio, $desde, $hasta, $mapa);
            foreach ($historial as $key => $recorrido) {
                $i = $i < 7 ? $i : 1;
                $this->gmapsManager->addRecorrido($mapa, $key, $recorrido['puntos_polilinea'], $this->colores[$i]);
                $arr_polilineas[] = $key;
                $i++;
            }
            if (count($historial)) {
                $arr_servicios[$servicio->getNombre()] = $historial;
            }
        }
        $inf_bottom = array('servicios' => $arr_servicios, 'polilineas' => json_encode($arr_polilineas));

        return $inf_bottom;
    }

    private function getHistorial($servicio, $desde, $hasta, $mapa)
    {
        try {
            $consHistorial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), array());
        } catch (\Exception $exc) {
            return null;
        }
        $historial = array();
        $puntoA = null;
        $puntoB = null;
        if (!is_null($consHistorial)) {
            reset($consHistorial);
            while ($trama = current($consHistorial)) {
                next($consHistorial);
                $dia = new \DateTime($trama['fecha']);
                if (isset($trama['posicion'])) {
                    $key = $servicio->getId();
                    //   die('////<pre>' . nl2br(var_export($trama, true)) . '</pre>////');
                    $historial[$key]['fecha'] = $dia->format('d-m-Y');
                    $historial[$key]['puntos_polilinea'][] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                    if ($puntoA == null) {
                        $puntoA = array('trama' => $trama, 'servicio' => $servicio);
                    }
                    $puntoB = array('trama' => $trama, 'servicio' => $servicio);
                }
            }
        }
        if ($puntoA['servicio'] != null && $puntoB['servicio'] != null) {
            $puntoinicio = $this->gmapsManager->createPosicionIcon($mapa, 'puntoA', 'iconA.png');
            $this->gmapsManager->addMarkerPosicion($mapa, $puntoA['trama']['oid'], $puntoA['trama']['posicion'], $puntoinicio, $puntoA['servicio']->getNombre());

            $puntofinal = $this->gmapsManager->createPosicionIcon($mapa, 'puntoB', 'iconB.png');
            $this->gmapsManager->addMarkerPosicion($mapa, $puntoB['trama']['oid'], $puntoB['trama']['posicion'], $puntofinal, $puntoB['servicio']->getNombre());
        }
        unset($consHistorial);

        return $historial;
    }

    private function getMovimientos($actividad)
    {
        $superficie = 0;
        $rendimiento = 0;
        $movimientos = array();
        $referencias = array();
        $acumuladores = array(
            'rendimiento' => 0, 'superficie' => 0, 'duracion' => array('total' => 0, 'horas' => 0, 'minutos' => 0),
            'distancia' => 0, 'combustible' => 0, 'translado' => 0, 'detenciones' => array('afuera' => 0, 'referencia' => 0, 'total' => 0)
        );
        if ($actividad && $actividad->getActgpmRefgpm()) {
            foreach ($actividad->getActgpmRefgpm() as $actref) {
                $referencias[] = $actref->getReferencia();
            }
        }

        if ($actividad && $actividad->getActgpmServicios()) {
            foreach ($actividad->getActgpmServicios() as $actserv) {
                $movimientos[$actserv->getServicio()->getId()] = array(
                    'nombre' => $actserv->getServicio()->getNombre(),
                    'detenciones' => $this->backendManager->informeDetenciones(
                        $actserv->getServicio()->getId(),
                        date_format($actividad->getFechaInicio(), 'Y/m/d H:i:s'),
                        date_format($actividad->getFechaFin(), 'Y/m/d H:i:s'),
                        300,
                        $referencias
                    ),
                    'pasos' => $this->backendManager->informePasoReferencias(
                        $actserv->getServicio()->getId(),
                        date_format($actividad->getFechaInicio(), 'Y/m/d H:i:s'),
                        date_format($actividad->getFechaFin(), 'Y/m/d H:i:s'),
                        $referencias
                    ),
                    'distancia' => $this->backendManager->informeDistancias(
                        $actserv->getServicio()->getId(),
                        date_format($actividad->getFechaInicio(), 'Y/m/d H:i:s'),
                        date_format($actividad->getFechaFin(), 'Y/m/d H:i:s'),
                        array()
                    )
                );
            }
        }
        foreach ($movimientos as $key => $mov) {
            $movimientos[$key]['distancia']['kms_calculada'] = intval(str_replace('.', ',', $movimientos[$key]['distancia']['kms_calculada']));
            //die('////<pre>' . nl2br(var_export($mov, true)) . '</pre>////');
            if ($mov['pasos'] != null) {
                foreach ($mov['pasos'] as $keyPaso => $paso) {
                    $referencia = $this->referenciaGpmManager->find(intval($paso['referencia_id']));
                    $movimientos[$key]['pasos'][$keyPaso]['fecha_ingreso'] = new \DateTime($movimientos[$key]['pasos'][$keyPaso]['fecha_ingreso']);
                    $movimientos[$key]['pasos'][$keyPaso]['fecha_egreso'] = new \DateTime($movimientos[$key]['pasos'][$keyPaso]['fecha_egreso']);
                    $duracion = $movimientos[$key]['pasos'][$keyPaso]['fecha_egreso']->diff($movimientos[$key]['pasos'][$keyPaso]['fecha_ingreso']);
                    $movimientos[$key]['pasos'][$keyPaso]['duracion'] = $duracion->h . " horas," . $duracion->i . ' minutos';
                    $movimientos[$key]['pasos'][$keyPaso]['distancia'] = intval($movimientos[$key]['pasos'][$keyPaso]['distancia']) / 1000;
                    $movimientos[$key]['pasos'][$keyPaso]['translado'] = $movimientos[$key]['distancia']['kms_calculada'] - $movimientos[$key]['pasos'][$keyPaso]['distancia'];
                    $movimientos[$key]['pasos'][$keyPaso]['referencia'] = array(
                        'nombre' => $referencia->getNombre(),
                        'id' => $referencia->getId(),
                        'latitud' => $referencia->getLatitud(),
                        'longitud' => $referencia->getLongitud()
                    );
                    $acumuladores['duracion']['horas'] += $duracion->h;
                    $acumuladores['duracion']['minutos'] += $duracion->i;
                    $acumuladores['distancia'] += $movimientos[$key]['pasos'][$keyPaso]['distancia'];
                    $acumuladores['translado'] += $movimientos[$key]['pasos'][$keyPaso]['translado'];
                }
            }
            if ($mov['detenciones'] !== null) {
                foreach ($movimientos[$key]['detenciones'] as $keyDet => $movimiento) {
                    if (isset($movimientos[$key]['detenciones'][$keyDet]['referencia_id']) && $movimientos[$key]['detenciones'][$keyDet]['referencia_id'] != null) {
                        $acumuladores['detenciones']['referencia'] += 1;
                    } else {
                        $acumuladores['detenciones']['afuera'] += 1;
                    }
                    $acumuladores['detenciones']['total'] += 1;
                }
            }
        }
        if (isset($duracion)) {
            $horaA = $acumuladores['duracion']['horas'];
            $minutos = $acumuladores['duracion']['minutos'];
            if ($acumuladores['duracion']['minutos'] >= 60) {
                $hora = floor($acumuladores['duracion']['minutos'] / 60);
                $minutos = floor($acumuladores['duracion']['minutos'] - $hora * 60);
                $horaA = $horaA + $hora;
            }
            $acumuladores['duracion']['total'] = $horaA . "h, " . $minutos . "m";
        }
        if (floatval($actividad->getAnchoTrabajo()) > 0 && floatval($acumuladores['distancia']) > 0) {
            $superficie = round(((floatval($actividad->getAnchoTrabajo()) * floatval($acumuladores['distancia']) * 1000) / 10000), 2);
            $acumuladores['superficie'] = $superficie;
        }
        if (floatval($superficie) > 0 && floatval($acumuladores['duracion']['horas']) > 0 || floatval($acumuladores['duracion']['minutos']) > 0) {
            $rendimiento = floatval($superficie) / (floatval($acumuladores['duracion']['horas'] + ($acumuladores['duracion']['minutos']) / 60));
            $acumuladores['rendimiento'] = round(floatval($rendimiento), 2);
        }

        //        die('////<pre>' . nl2br(var_export($acumuladores, true)) . '</pre>////');

        return array('movimientos' => $movimientos, 'acumuladores' => $acumuladores);
    }

    //  'horas' => 10,
    //    'minutos' => 122,

    /**
     * @Route("/gpm/actividad/getactividad", options={"expose"=true}, name="gpm_actividad_getactividad")
     * @Method({"GET", "POST"})
     */
    public function getactividadAction(Request $request)
    {
        $id = $request->get('id');
        $movimientos = null;
        $movi = null;
        $acumuladores = null;
        //  die('////<pre>' . nl2br(var_export($request->get('id'), true)) . '</pre>////');
        $actividad = $this->actividadManager->find(intval($id));
        $organizacion = $actividad->getProyecto()->getOrganizacion();
        $configuracion = $organizacion->getConfigGpm()->getVista_actividad();
        if ($actividad->getEstado() === 3 || $actividad->getEstado() === 2) {
            $movimientos = $this->getMovimientos($actividad);
            $movi = $movimientos['movimientos'];
            $acumuladores = $movimientos['acumuladores'];
        }

        return new Response(json_encode(array(
            'htmlTituloSidebar' => $this->renderView('gpm/Actividad/titulo_actividad_sidebar.html.twig', array(
                'actividad' => $actividad
            )),
            'htmlSidebar' => $this->renderView('gpm/Actividad/actividad_sidebar.html.twig', array(
                'actividad' => $actividad,
                'movimientos' => $movi,
                'totales' => $acumuladores,
                'configuracion' => $configuracion
            )),
        )));
    }

    /**
     * @Route("/gpm/actividad/finalizar", options={"expose"=true}, name="gpm_actividad_finalizar")
     * @Method({"GET", "POST"})
     */
    public function finalizarAction(Request $request)
    {
        $id = $request->get('id');
        $actividad = $this->actividadManager->find(intval($id));
        $ok = $this->actividadManager->cambiarEstado($actividad, 2);
        $proyecto = $actividad->getProyecto();
        $proyecto->setProgreso($this->proyectoManager->getProgressbar($proyecto)['progreso']);
        //die('////<pre>' . nl2br(var_export($actividad->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $actividad->getId(),
        )));
    }

    /**
     * @Route("/gpm/actividad/cerrar", options={"expose"=true}, name="gpm_actividad_cerrar")
     * @Method({"GET", "POST"})
     */
    public function cerrarAction(Request $request)
    {
        $id = $request->get('id');
        $actividad = $this->actividadManager->find(intval($id));
        $ok = $this->actividadManager->cambiarEstado($actividad, 3);
        $proyecto = $actividad->getProyecto();
        $proyecto->setProgreso($this->proyectoManager->getProgressbar($proyecto)['progreso']);
        //die('////<pre>' . nl2br(var_export($actividad->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $actividad->getId(),
            'htmlProgreso' => $this->renderView('gpm/Proyecto/barra_progreso.html.twig', array(
                'proyecto' => $proyecto
            )),
        )));
    }

    /**
     * @Route("/gpm/actividad/removerservicio", options={"expose"=true}, name="gpm_actividad_removerservicio")
     * @Method({"GET", "POST"})
     */
    public function removerservicioAction(Request $request)
    {
        $idAct = $request->get('id');
        $idActgpmServ = $request->get('idServicio');
        $actividad = $this->actividadManager->find(intval($idAct));
        $actgpmServicio = $this->getEntityManager()->getRepository('App:ActgpmServicio')->find(intval($idActgpmServ));
        $this->getEntityManager()->remove($actgpmServicio);
        if ($this->getEntityManager()->flush()) {
            $this->setFlash('success', 'El servicio se eliminó correctamente');
        } else {
            $this->setFlash('error', 'No se pudo eliminar el servicio');
        }
        //die('////<pre>' . nl2br(var_export($personal->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'htmlSidebar' => $this->renderView('gpm/Actividad/actividad_sidebar.html.twig', array(
                'actividad' => $actividad
            )),
        )));
    }

    /**
     * @Route("/gpm/actividad/remover", options={"expose"=true}, name="gpm_actividad_remover")
     * @Method({"GET", "POST"})
     */
    public function removerAction(Request $request)
    {
        $idAct = $request->get('id');
        $idActgpmServ = $request->get('idServicio');
        $actividad = $this->actividadManager->find(intval($idAct));
        $proyecto = $actividad->getProyecto();
        $this->getEntityManager()->remove($actividad);
        if ($this->getEntityManager()->flush()) {
            //  $this->setFlash('success', 'La actividad se eliminó correctamente');
        } else {
            //$this->setFlash('error', 'No se pudo eliminar la actividad');
        }
        $progreso = $this->proyectoManager->getProgressbar($proyecto);
        $proyectos = $this->proyectoManager->findAll($actividad->getProyecto());
        //die('////<pre>' . nl2br(var_export($personal->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'tituloSidebar' => $this->renderView('gpm/Proyecto/titulo_sidebar.html.twig', array(
                'proyecto' => $proyecto
            )),
            'htmlSidebar' => $this->renderView('gpm/Actividad/actividades_sidebar.html.twig', array(
                'proyecto' => $proyecto,
                'countActTotales' => $progreso['countActTotales'],
                'countActFinalizadas' => $progreso['countActFinalizadas'],
                'progreso' => $progreso['progreso']
            )),
            'html' => $this->renderView('gpm/Actividad/actividades.html.twig', array(
                'proyecto' => $proyecto
            )),
            'idProyecto' => $proyecto->getId()
        )));
    }

    /**
     * @Route("/gpm/actividad/rquitarpersona", options={"expose"=true}, name="gpm_actividad_quitarpersona")
     * @Method({"GET", "POST"})
     */
    public function quitarpersonalAction(Request $request)
    {
        $idAct = $request->get('id');
        $idPersonal = $request->get('idPersonal');
        $actividad = $this->actividadManager->find(intval($idAct));
        $personal = $this->getEntityManager()->getRepository('App:Personal')->find(intval($idPersonal));
        $this->getEntityManager()->remove($personal);
        $this->getEntityManager()->flush();
        //die('////<pre>' . nl2br(var_export($personal->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('gpm/Actividad/personal.html.twig', array(
                'actividad' => $actividad
            )),
        )));
    }

    /**
     * @Route("/gpm/actividad/getresponsables",options={"expose"=true},  name="gpm_actividad_getresponsables")
     * @Method({"GET", "POST"})
     */
    public function getResponsablesAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find(intval($id));
        $responsables = $this->actividadManager->findAllResponsablesQuery($organizacion, $search);
        $str = $this->getArrayResp($responsables);
        //   die('////<pre>' . nl2br(var_export($str, true)) . '</pre>////');
        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/gpm/actividad/newresponsable", options={"expose"=true}, name="gpm_actividad_newresponsable")
     * @Method({"GET", "POST"})
     */
    public function newajaxAction(Request $request)
    {
        $id = $request->get('id');
        $organizacion = $this->organizacionManager->find($id);
        $tmp = array();
        parse_str($request->get('formResponsable'), $tmp);
        $dataResp = $tmp['reponsable'];
        //die('////<pre>' . nl2br(var_export($dataResp, true)) . '</pre>////');
        $persona = $this->personalManager->create($dataResp);
        $responsable = $this->actividadManager->saveResponsable($organizacion, $persona);

        return new Response(json_encode(array('id' => $responsable->getId(), 'nombre' => $responsable->getPersona()->getNombre())), 200);
    }

    private function getArrayResp($responsables)
    {
        $str = array();
        foreach ($responsables as $cont) {
            $x = $cont->getPersona()->getNombre();
            $str[] = array('id' => $cont->getId(), 'text' => $x);
        }
        return $str;
    }

    /**
     * @Route("/gpm/actividad/getservyref",options={"expose"=true},  name="gpm_actividad_getservyref")
     * @Method({"GET", "POST"})
     */
    public function getServiciosAction(Request $request)
    {
        $id = $request->get('id');
        $actividad = $this->actividadManager->find(intval($id));
        $servicios = $this->getArrayServicios($actividad);
        $referencias = $this->getArrayReferencias($actividad);
        //   die('////<pre>' . nl2br(var_export($str, true)) . '</pre>////');
        return new Response(json_encode(array('servicios' => $servicios, 'referencias' => $referencias)), 200);
    }

    /**
     * @Route("/gpm/actividad/getpersonal",options={"expose"=true},  name="gpm_actividad_getpersonal")
     * @Method({"GET", "POST"})
     */
    public function getPersonalAction(Request $request)
    {
        $id = $request->get('id');
        $actividad = $this->actividadManager->find(intval($id));
        $personal = $this->getArrayPersonal($actividad);
        //   die('////<pre>' . nl2br(var_export($str, true)) . '</pre>////');
        return new Response(json_encode(array('personal' => $personal)), 200);
    }

    private function getArrayServicios($actividad)
    {
        $str = array();
        if ($actividad != null && $actividad->getActgpmServicios() != null) {
            foreach ($actividad->getActgpmServicios() as $serv) {
                $str[] = strval($serv->getServicio()->getId());
            }
        }
        return $str;
    }

    private function getArrayReferencias($actividad)
    {
        $str = array();
        if ($actividad != null && $actividad->getActgpmRefgpm() != null) {
            foreach ($actividad->getActgpmRefgpm() as $ref) {
                $str[] = strval($ref->getReferencia()->getId());
            }
        }
        return $str;
    }

    private function getArrayPersonal($actividad)
    {
        $str = array();
        if ($actividad != null && $actividad->getPersonal() != null) {
            foreach ($actividad->getPersonal() as $personal) {
                $str[] = strval($personal->getPersona()->getId());
            }
        }
        return $str;
    }

    /**
     * @Route("/gpm/actividad/ingresomanual", options={"expose"=true}, name="gpm_actividad_ingresomanual")
     * @Method({"GET", "POST"})
     */
    public function ingresomanualAction(Request $request)
    {
        $tmp = array();
        $actividad = $this->actividadManager->find(intval($request->get('id')));
        parse_str($request->get('form'), $tmp);
        $data = $tmp['ingreso_manual'];

        $actividad = $this->actividadManager->addIngresoManual($actividad, $data);

        foreach ($actividad->getIngresos() as $ingreso) {
            $ingreso->setTiempo($this->utilsManager->minutos2tiempo($ingreso->getTiempo()));
            $ingreso->setDistancia(($ingreso->getDistancia() / 1000) . ' kms');
        }

        return new Response(json_encode(array(
            'estado' => true,
            'id' => $actividad->getId(),
            'html' => $this->renderView('gpm/IngresoManual/ingreso_manual.html.twig', array(
                'actividad' => $actividad
            )),
        )));
    }

    /**
     * @Route("/gpm/actividad/removeringreso", options={"expose"=true}, name="gpm_actividad_removeringreso")
     * @Method({"GET", "POST"})
     */
    public function removeringresoAction(Request $request)
    {
        $id = $request->get('id');
        $ingreso = $this->getEntityManager()->getRepository('App:ActividadManual')->findOneBy(array('id' => intval($id)));
        $actividad = $ingreso->getActividad();
        $this->getEntityManager()->remove($ingreso);
        if ($this->getEntityManager()->flush()) {
            $this->setFlash('success', 'El reporte se eliminó correctamente');
        } else {
            $this->setFlash('error', 'No se pudo eliminar el reporte');
        }
        //die('////<pre>' . nl2br(var_export($personal->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('gpm/IngresoManual/ingreso_manual.html.twig', array(
                'actividad' => $actividad
            )),
        )));
    }
}
