<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Contratista as Contratista;
use App\Model\app\BreadcrumbManager;
use App\Model\app\Router\ContratistaRouter;
use App\Model\app\OrganizacionManager;
use App\Model\app\ContratistaManager;
use App\Model\gpm\PersonalManager;
use App\Form\gpm\MotivoActividadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ContratistaController
 *
 * @author nicolas
 */
class ContratistaController extends AbstractController
{

    private $breadcrumbManager;
    private $contratistaRouter;
    private $organizacionManager;
    private $contratistaManager;
    private $personalManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ContratistaRouter $contratistaRouter,
        OrganizacionManager $organizacionManager,
        ContratistaManager $contratistaManager,
        PersonalManager $personalManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->contratistaRouter = $contratistaRouter;
        $this->contratistaManager = $contratistaManager;
        $this->personalManager = $personalManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Contratista');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/gpm/contratista/{id}/list", name="gpm_contratista_list")
     * @Method({"GET", "POST"}) 
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de contratistas");
        $menu = array(
            1 => array(
                $this->contratistaRouter->btnNew($organizacion),
            )
        );
        return $this->render('gpm/Contratista/list.html.twig', array(
            'menu' => $menu,
            'organizacion' => $organizacion
        ));
    }

    /**
     * @Route("/gpm/contratista/getcontratista",options={"expose"=true},  name="gpm_contratista_get")
     * @Method({"GET", "POST"})
     */
    public function getContratistaAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find(intval($id));
        // die('////<pre>' . nl2br(var_export($proyecto->getId(), true)) . '</pre>////');
        $contratistas = $this->contratistaManager->findAllQuery($organizacion, $search);
        $str = $this->getArrayContratista($contratistas);
        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/gpm/contratista/getpersonas",options={"expose"=true},  name="gpm_persona_get")
     * @Method({"GET", "POST"})
     */
    public function getPersonaAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $contratista = $this->contratistaManager->find(intval($id));
        // die('////<pre>' . nl2br(var_export($proyecto->getId(), true)) . '</pre>////');
        $personas = $this->personalManager->findAllQuery($contratista, $search);
        $str = $this->getArrayContratista($personas);
        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/gpm/contratista/new", options={"expose"=true}, name="gpm_contratista_new")
     * @Method({"GET", "POST"})
     */
    public function newajaxAction(Request $request)
    {
        $id = $request->get('id');
        $organizacion = $this->organizacionManager->find($id);
        $tmp = array();
        parse_str($request->get('formContratista'), $tmp);
        $dataCont = $tmp['contratista'];
        // die('////<pre>' . nl2br(var_export($dataRef, true)) . '</pre>////');
        $contratista = $this->contratistaManager->create($organizacion);
        $contratista->setNombre($dataCont['nombre']);
        $contSave = $this->contratistaManager->save($contratista);
        $html = $this->renderView('gpm/Contratista/contratistas.html.twig', array(
            'organizacion' => $organizacion
        ));
        return new Response(json_encode(array('id' => $contSave->getId(), 'nombre' => $contSave->getNombre(), 'html' => $html)), 200);
    }

    /**
     * @Route("/gpm/contratista/edit", options={"expose"=true}, name="gpm_contratista_edit")
     * @Method({"GET", "POST"})
     */
    public function editajaxAction(Request $request)
    {
        $id = $request->get('id');
        $contratista = $this->contratistaManager->find($id);
        $tmp = array();
        parse_str($request->get('formContratistaEdit'), $tmp);
        // die('////<pre>' . nl2br(var_export($tmp['contratista_edit_'], true)) . '</pre>////');
        $dataCont = $tmp['contratista_edit'];
        $contratista->setNombre($dataCont['nombre']);
        $contSave = $this->contratistaManager->save($contratista);
        $html = $this->renderView('gpm/Contratista/contratistas.html.twig', array(
            'organizacion' => $contratista->getOrganizacion()
        ));
        return new Response(json_encode(array('html' => $html)), 200);
    }

    private function getArrayContratista($contratistas)
    {
        $str = array();
        foreach ($contratistas as $cont) {
            $x = $cont->getNombre();
            $str[] = array('id' => $cont->getId(), 'text' => $x);
        }
        return $str;
    }

    /**
     * @Route("/gpm/contratista/remover", options={"expose"=true}, name="gpm_contratista_remover")
     * @Method({"GET", "POST"})
     */
    public function removerAction(Request $request)
    {
        $id = $request->get('id');
        $contratista = $this->contratistaManager->find(intval($id));
        $organizacion = $contratista->getOrganizacion();
        $this->getEntityManager()->remove($contratista);
        $this->getEntityManager()->flush();
        //die('////<pre>' . nl2br(var_export($personal->getId(), true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('gpm/Contratista/contratistas.html.twig', array(
                'organizacion' => $organizacion
            )),
        )));
    }

    /**
     * @Route("/gpm/contratista/{id}/show", name="gpm_contratista_show")
     * @Method({"GET", "POST"}) 
     */
    public function showAction(Request $request, Contratista $contratista)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "ver Contratista");
        $menu = array(
            1 => array(
                $this->contratistaRouter->btnNewPersona(),
            )
        );
        return $this->render('gpm/Contratista/show.html.twig', array(
            'menu' => $menu,
            'organizacion' => $contratista->getOrganizacion(),
            'contratista' => $contratista
        ));
    }

    /**
     * @Route("/gpm/contratista/newpersonal", name="gpm_contratista_newpersona")
     * @Method({"GET", "POST"})
     */
    public function newpersonaAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formPersona'), $tmp);
        $dataPers = $tmp['persona'];
        $id = $request->get('idCont');
        $contratista = $this->contratistaManager->find(intval($id));
        // die('////<pre>' . nl2br(var_export($dataPers, true)) . '</pre>////');
        $persona = $this->personalManager->create($dataPers, $contratista);
        $personas = $this->personalManager->findAllPersonas($contratista);
        $html = $this->renderView('gpm/Contratista/personal.html.twig', array(
            'contratista' => $contratista
        ));
        return new Response(json_encode(array(
            'html' => $html
        )));
    }

    /**
     * @Route("/gpm/contratista/removerpersona", name="gpm_contratista_removerpersona")
     * @Method({"GET", "POST"})
     */
    public function removerpersonaAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formPersona'), $tmp);
        $id = $request->get('id');
        // die('////<pre>' . nl2br(var_export($dataPers, true)) . '</pre>////');
        $persona = $this->personalManager->findPersona($id);
        $contratista = $persona->getContratista();
        $this->getEntityManager()->remove($persona);
        $this->getEntityManager()->flush();
        $personas = $this->personalManager->findAllPersonas($contratista);
        $html = $this->renderView('gpm/Contratista/personal.html.twig', array(
            'contratista' => $contratista
        ));
        return new Response(json_encode(array(
            'html' => $html
        )));
    }

    /**
     * @Route("/gpm/contratista/personaedit", options={"expose"=true}, name="gpm_contratista_personaedit")
     * @Method({"GET", "POST"})
     */
    public function editpersonaajaxAction(Request $request)
    {
        $id = $request->get('id');
        $persona = $this->personalManager->findPersona(intval($id));
        $tmp = array();
        parse_str($request->get('formPersonaEdit'), $tmp);
        $data = $tmp['persona_edit'];
        $persona->setNombre($data['nombre']);
        $persona->setTelefono($data['telefono']);
        $persona->setCuit($data['cuit']);
        $persona->setDocumento($data['documento']);
        $this->getEntityManager()->persist($persona);
        $this->getEntityManager()->flush();
        $html = $this->renderView('gpm/Contratista/personal.html.twig', array(
            'contratista' => $persona->getContratista()
        ));
        return new Response(json_encode(array('html' => $html)), 200);
    }
}
