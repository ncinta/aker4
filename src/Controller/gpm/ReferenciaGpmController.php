<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Model\gpm\ProyectoManager;
use App\Model\gpm\ActividadManager;
use App\Model\gpm\ReferenciaGpmManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GmapsManager;
use App\Model\app\ContratistaManager;
use App\Model\app\Router\ProyectoRouter;
use App\Form\gpm\ProyectoNewType;
use App\Form\gpm\ActividadNewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ActividadController
 *
 * @author nicolas
 */
class ReferenciaGpmController extends AbstractController
{

    protected $actividadManager = null;
    protected $breadcrumbManager = null;
    protected $contratistaManager = null;
    protected $organizacionManager = null;
    protected $proyectoManager = null;
    protected $referenciaGpmManager = null;
    protected $userloginManager = null;
    protected $gmapsManager = null;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ActividadManager $actividadManager,
        ReferenciaGpmManager $referenciaGpmManager,
        ProyectoManager $proyectoManager,
        OrganizacionManager $organizacionManager,
        ContratistaManager $contratistaManager,
        UserLoginManager $userloginManager,
        GmapsManager $gmapsManager
    ) {
        $this->actividadManager = $actividadManager;
        $this->referenciaGpmManager = $referenciaGpmManager;
        $this->proyectoManager = $proyectoManager;
        $this->contratistaManager = $contratistaManager;
        $this->organizacionManager = $organizacionManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\ReferenciaGpm');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/gpm/referencia/get", options={"expose"=true}, name="gpm_referencia_get")
     * @Method({"GET", "POST"})
     */
    public function getreferenciasAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find($id);
        //recargo los productos y devuelvo
        $referencias = $this->referenciaGpmManager->findAllQuery($organizacion, $search);
        $str = $this->getArrayReferencia($referencias);


        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/gpm/referencia/new", options={"expose"=true}, name="gpm_referencia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $id = $request->get('id');
        $organizacion = $this->organizacionManager->find($id);
        $tmp = array();
        parse_str($request->get('formReferencia'), $tmp);
        $dataRef = $tmp['referencia'];
        $referencia = $this->referenciaGpmManager->create($organizacion);
        $referenciaGpm = $this->referenciaGpmManager->save($referencia, $dataRef);
        $actividad = $this->actividadManager->find(intval($dataRef['actividad']));

        return new Response(json_encode(array(
            'id' => $referenciaGpm->getId(),
            'nombre' => $referenciaGpm->getNombre(),
            'html' => $actividad != null ? $this->renderView('gpm/Actividad/referencias.html.twig', array(
                'actividad' => $actividad
            )) : ''
        )), 200);
    }

    private function getArrayReferencia($referencias)
    {
        $str = array();
        foreach ($referencias as $ref) {
            if ($ref->getUnica() != 1) { //para todas las actividades
                $x = $ref->getNombre();
                $str[] = array('id' => $ref->getId(), 'text' => $x);
            }
        }
        return $str;
    }

    /**
     * @Route("/gpm/referencia/quitar", options={"expose"=true}, name="gpm_referencia_quitar")
     * @Method({"GET", "POST"})
     */
    public function quitarAction(Request $request)
    {
        $id = $request->get('id');
        $idActividad = intval($request->get('actividad'));
        $tmp = array();
        $actividad = null;
        parse_str($request->get('formReferencia'), $tmp);
        $dataRef = $tmp['referencia_quitar'];
        $referencia = $this->getEntityManager()->getRepository('App\Entity\ReferenciaGpm')->findOneBy(array('id' => intval($dataRef['id'])));
        //  die('////<pre>' . nl2br(var_export($idActividad, true)) . '</pre>////');
        $invisibles = $this->gmapsManager->castVisibilidadReferencias(array($referencia), false);
        if ($dataRef['unica'] === '1') {
            $actividad = $this->getEntityManager()->getRepository('App\Entity\ActividadGpm')->findOneBy(array('id' => intval($idActividad)));
            $refAct = $this->getEntityManager()->getRepository('App\Entity\ActgpmRefgpm')->ifExist($actividad->getId(), $referencia->getId());
            $remove = $this->getEntityManager()->remove($refAct);
        }
        $removeRef = $this->getEntityManager()->remove($referencia);
        $this->getEntityManager()->flush();

        return new Response(json_encode(array(
            'html' => $actividad != null ? $this->renderView('gpm/Actividad/referencias.html.twig', array(
                'actividad' => $actividad
            )) : '',
            'invisibles' => $invisibles
        )), 200);
    }

    /**
     * @Route("/gpm/referencia/viewAjax", options={"expose"=true}, name="gpm_referencias_viewAJAX")
     * @Method({"GET", "POST"})
     */
    public function referenciasAjaxAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $invisibles = array();
        $visibles = array();
        $allReferencias = $this->referenciaGpmManager->findAll($organizacion);

        $idAct = intval($request->get('actividad'));
        if ($idAct > 0) {
            $invisibles = $this->gmapsManager->castVisibilidadReferencias($allReferencias, false);
            $actividad = $this->actividadManager->find($idAct);
            $referencias = $this->referenciaGpmManager->findByProyecto($actividad->getProyecto());
            $visibles = $this->gmapsManager->castReferencias($referencias);
            // return new Response(json_encode($resp));
        } elseif ($idAct == 0) {
            $visibles = $this->gmapsManager->castReferencias($allReferencias);
        } else {
            $visibles = $this->gmapsManager->castReferencias($allReferencias);
        }
        //  die('////<pre>' . nl2br(var_export($visibles, true)) . '</pre>////');
        return new Response(json_encode(array('visibles' => $visibles, 'invisibles' => $invisibles)));
    }

    /**
     * Actualiza una referencia. Los datos fueron pasados por ajax.
     * Devuelvo un ajax con la nueva referencia creada para que la vista la 
     * incorpore como una referencia mas.
     *
     * @Route("/gpm/referencia/update/ajax", name="gpm_referencia_update_ajax")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function updateajaxAction(Request $request)
    {
        $form_referencia = array();
        parse_str($request->get('referencia'), $form_referencia);
        $data = $form_referencia['formrr'];
        //        die('////<pre>' . nl2br(var_export($data), true) . '</pre>////');
        unset($form_referencia); //no uso mas el $form_consulta.
        if ($data) {
            $em = $this->getEntityManager();

            $referencia = $this->referenciaGpmManager->find($data['referencia_id']);
            $referencia->setPathIcono($data['pathIcono']);
            $referencia->setColor(isset($data['color']) ? $data['color'] : '#FFFFFF');
            $referencia->setTransparencia(isset($data['transparencia']) ? $data['transparencia'] : 0.5);

            //es un poligono viejo, entonces se tiene que cambiar a poligono nuevo.
            if ($referencia->isOldPoligon()) {
                $referencia->setClase($referencia->getClase());
            }

            if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
                $referencia->setArea(isset($data['area']) ? $data['area'] : 0.0);
                $referencia->setPoligono($data['poligono']);

                //hago esto para traer el punto central del poligono.
                $puntos = $this->gmapsManager->parsePoligono($referencia);
                $referencia->setLatitud($puntos['latitud']);
                $referencia->setLongitud($puntos['longitud']);
            } else {
                $centro = explode(',', substr($data['centro'], 1, strlen($data['centro']) - 2));
                $referencia->setLatitud($centro[0]);
                $referencia->setLongitud($centro[1]);
                $referencia->setRadio($data['radio']);
            }

            $em->persist($referencia);
            $em->flush();

            $newRef = $this->gmapsManager->getDataReferencia($referencia);
            //es el retorno.
            return new Response(json_encode(array(
                'result' => 'ok',
                'id' => $referencia->getId(),
                'referencia' => $newRef,
            )));
        }
    }
}
