<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Model\gpm\ProyectoManager;
use App\Model\gpm\PersonalManager;
use App\Model\app\OrganizacionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ResponsableController
 *
 * @author nicolas
 */
class ResponsableController extends AbstractController
{

    protected $proyectoManager = null;
    protected $personalManager = null;
    protected $organizacionManager = null;

    function __construct(ProyectoManager $proyectoManager, OrganizacionManager $organizacionManager,  PersonalManager $personalManager)
    {
        $this->proyectoManager = $proyectoManager;
        $this->personalManager = $personalManager;
        $this->organizacionManager = $organizacionManager;
    }

    /**
     * @Route("/gpm/responsable/newresponsable", options={"expose"=true}, name="gpm_responsable_newresponsable")
     * @Method({"GET", "POST"})
     */
    public function newajaxAction(Request $request)
    {
        $id = $request->get('id');
        $organizacion = $this->organizacionManager->find($id);
        $tmp = array();
        parse_str($request->get('formResponsable'), $tmp);
        //die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        $dataResp = $tmp['reponsable'];
        $persona = $this->personalManager->create($dataResp);
        $responsable = $this->proyectoManager->saveResponsable($organizacion, $persona);

        return new Response(json_encode(array('id' => $responsable->getId(), 'nombre' => $responsable->getPersona()->getNombre())), 200);
    }

    /**
     * @Route("/gpm/responsable/getresponsables",options={"expose"=true},  name="gpm_responsable_getresponsables")
     * @Method({"GET", "POST"})
     */
    public function getResponsablesAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find(intval($id));
        // die('////<pre>' . nl2br(var_export($proyecto->getId(), true)) . '</pre>////');
        $responsables = $this->proyectoManager->findAllResponsablesQuery($organizacion, $search);
        $str = $this->getArrayResp($responsables);
        return new Response(json_encode(array('results' => $str)), 200);
    }

    private function getArrayResp($responsables)
    {
        $str = array();
        foreach ($responsables as $cont) {
            $x = $cont->getPersona()->getNombre();
            $str[] = array('id' => $cont->getId(), 'text' => $x);
        }
        return $str;
    }
}
