<?php

namespace App\Controller\gpm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Entity\Proyecto as Proyecto;
use App\Entity\FiltroProyecto as FiltroProyecto;
use App\Model\gpm\ProyectoManager;
use App\Model\gpm\PersonalManager;
use App\Model\gpm\ClienteManager;
use App\Model\gpm\ReferenciaGpmManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaFormManager;
use App\Model\app\ContratistaManager;
use App\Model\app\UtilsManager;
use App\Model\app\Router\ProyectoRouter;
use App\Form\gpm\ProyectoNewType;
use App\Form\gpm\ProyectoEditType;
use App\Form\gpm\ReferenciaGpmType;
use App\Form\gpm\ProyectoFilter;
use GMaps\Geocoder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ProyectoController
 *
 * @author nicolas
 */
class ProyectoController extends AbstractController
{

    protected $proyectoManager = null;
    protected $personalManager = null;
    protected $breadcrumbManager = null;
    protected $contratistaManager = null;
    protected $clienteManager = null;
    protected $organizacionManager = null;
    protected $referenciaGpmManager = null;
    protected $userloginManager = null;
    protected $gmapsManager = null;
    protected $excelManager = null;
    protected $utilsManager = null;
    protected $proyectoRouter = null;
    protected $referenciaFormManager = null;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ProyectoManager $proyectoManager,
        OrganizacionManager $organizacionManager,
        UtilsManager $utilsManager,
        ProyectoRouter $proyectoRouter,
        PersonalManager $personalManager,
        ContratistaManager $contratistaManager,
        ClienteManager $clienteManager,
        ExcelManager $excelManager,
        ReferenciaGpmManager $referenciaGpmManager,
        UserLoginManager $userloginManager,
        GmapsManager $gmapsManager,
        ReferenciaFormManager $referenciaFormManager
    ) {
        $this->proyectoManager = $proyectoManager;
        $this->personalManager = $personalManager;
        $this->contratistaManager = $contratistaManager;
        $this->clienteManager = $clienteManager;
        $this->organizacionManager = $organizacionManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->referenciaGpmManager = $referenciaGpmManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->proyectoRouter = $proyectoRouter;
        $this->referenciaFormManager = $referenciaFormManager;
        $this->utilsManager = $utilsManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Proyecto');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/gpm/{id}/index", name="gpm_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Proyectos");

        $countTotales = $countCurso = $countProgramados = $countFinalizados = 0;
        $checkFiltro = false;
        $formReferencia = $this->getReferenciaForm();
        $informe = array();
        $fechaTodos = false;


        $filtro = new FiltroProyecto();
        $filtro->setOrganizacion($organizacion);
        if ($request->getMethod() == 'GET') {
            $now = new \DateTime();
            $desde = (new \DateTime())->modify('-1 month');
            $f = $this->get("session")->get("filtro");  //existe filtro aplicado
            $filtro->setDesde(new \DateTime($this->utilsManager->datetime2sqltimestamp($desde->format('d/m/Y'), false)));
            $filtro->setHasta(new \DateTime($this->utilsManager->datetime2sqltimestamp($now->format('d/m/Y'), true)));
            $filtro->setFechatodos(false);
            $filtro->setEstado(-1);
            $checkFiltro = true;
            //die('////<pre>' . nl2br(var_export($filtro, true)) . '</pre>////');
            if ($f) {
                $filtro->setResponsable($f->getResponsable());
                $filtro->setEstado($f->getEstado());
                $filtro->setContratista($f->getContratista());
                $filtro->setCliente($f->getCliente());
                $checkFiltro = true;
            }
            $this->get("session")->set("filtro", $filtro);  //grabo el filtro nuevo.
        }

        $options = $this->getOptions($organizacion);
        $form = $this->createForm(ProyectoFilter::class, $filtro, $options);
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {  //grabo el filtro en la session
            $fechaTodos = isset($request->get('filtro')['fechatodos']);
            if (!($fechaTodos)) {
                $filtro->setDesde(new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('filtro')['desde'], false)));
                $filtro->setHasta(new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('filtro')['hasta'], true)));
            } else {
                $filtro->setDesde(null);
                $filtro->setHasta(null);
            }
            $this->get("session")->set("filtro", $filtro);  //grabo el filtro nuevo.
            $checkFiltro = true;
        }

        $proyectos = $this->proyectoManager->obtener($filtro);
        $proyecto = $this->proyectoManager->updateEstadoActividades($proyectos);

        foreach ($proyectos as $proyecto) {
            if ($proyecto->getEstado() == 0) {
                $countProgramados++;
            } elseif ($proyecto->getEstado() == 1) {
                $countCurso++;
            } else {
                $countFinalizados++;
            }
            $countTotales++;

            $proyecto->setProgreso($this->proyectoManager->getProgressbar($proyecto)['progreso']);

            $informe[] = $this->getInforme($proyecto);
        }

        $arrFiltro = $this->getArrFiltro($filtro, $fechaTodos);
        $menu = $this->getMenu($organizacion, json_encode($informe, true), json_encode($arrFiltro, true));

        return $this->render('gpm/Proyecto/index.html.twig', array(
            'proyectos' => $proyectos,
            'mapa' => $this->getMapa(),
            'geocoder' => new Geocoder(),
            'iconos' => glob('images/iconos/*.*'),
            'formReferencia' => $formReferencia->createView(),
            'organizacion' => $organizacion,
            'menu' => $menu,
            'countTotales' => $countTotales,
            'countProgramados' => $countProgramados,
            'countCurso' => $countCurso,
            'filter' => $form->createView(),
            'checkFiltro' => $checkFiltro,
            'filtro' => $filtro,
            'fechaTodos' => $fechaTodos,
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($this->userloginManager->getOrganizacion())->createView(),
        ));
    }

    /**
     * @Route("/gpm/proyecto/{informe}/{filtro}/exportar", name="gpm_proyecto_exportar")
     * @Method({"GET", "POST"})
     */
    public function exportarAction(Request $request, $informe = null, $filtro = null)
    {

        if (isset($filtro) && isset($informe)) {
            $filtro = json_decode($filtro, true);
            //  die('////<pre>' . nl2br(var_export($filtro, true)) . '</pre>////');
            $informe = json_decode($informe, true);
            //creo el xls
            $xls = $this->excelManager->create("Proyectos");
            $xls->setHeaderInfo(array(
                'C2' => 'Desde',
                'D2' => $filtro['desde'],
                'C3' => 'Hasta',
                'D3' => $filtro['hasta'],
                'C4' => 'Responsable',
                'D4' => $filtro['responsable'],
                'C5' => 'Cliente',
                'D5' => $filtro['cliente'],
                'C6' => 'Contratista',
                'D6' => $filtro['contratista'],
                'C7' => 'Estado',
                'D7' => $filtro['estado'],
            ));

            $xls->setBar(9, array(
                'A' => array('title' => 'Nombre', 'width' => 20),
                'B' => array('title' => 'Codigo', 'width' => 20),
                'C' => array('title' => 'Inicio', 'width' => 20),
                'D' => array('title' => 'Fin', 'width' => 20),
                'E' => array('title' => 'Estado', 'width' => 20),
                'F' => array('title' => 'Avance', 'width' => 20),
                'G' => array('title' => 'Responsable', 'width' => 20),
                'H' => array('title' => 'Contratista', 'width' => 20),
                'I' => array('title' => 'Cliente', 'width' => 20),
            ));

            $i = 11;
            foreach ($informe as $key => $data) {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $data['nombre']),
                    'B' => array('value' => $data['codigo']),
                    'C' => array('value' => $data['inicio']),
                    'D' => array('value' => $data['fin']),
                    'E' => array('value' => $data['estado']),
                    'F' => array('value' => $data['progreso']),
                    'G' => array('value' => $data['responsable']),
                    'H' => array('value' => $data['contratista']),
                    'I' => array('value' => $data['cliente'])
                ));
                $i++;
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=proyectos.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
    }

    private function getOptions($organizacion)
    {
        $options['responsable'] = $this->proyectoManager->findAllResponsablesQuery($organizacion, '');
        $options['estado'] = array('Todos' => -1, 'Programado' => 0, 'En Curso' => 1, 'Finalizado' => 2);
        $options['contratista'] = $this->contratistaManager->findAllQuery($organizacion, '');
        $options['cliente'] = $this->clienteManager->findAllQuery($organizacion, '');
        return $options;
    }

    private function getInforme($proyecto)
    {
        return array(
            'nombre' => $proyecto->getNombre(),
            'codigo' => $proyecto->getCodigo(),
            'inicio' => $proyecto->getFechaInicio()->format('d-m-Y'),
            'fin' => $proyecto->getFechaFin()->format('d-m-Y'),
            'estado' => $proyecto->getStrEstado(),
            'progreso' => $proyecto->getProgreso() . '%',
            'responsable' => $proyecto->getResponsable()  && $proyecto->getResponsable()->getPersona() ? $proyecto->getResponsable()->getPersona()->getNombre() : '---',
            'contratista' => $proyecto->getContratista() ? $proyecto->getContratista()->getNombre() : '---',
            'cliente' => $proyecto->getCliente() ? $proyecto->getCliente()->getNombre() : '---',
        );
    }

    private function getArrFiltro($filtro, $fechaTodos)
    {
        if ($filtro->getEstado() == 0) {
            $estado = 'Programado';
        } elseif ($filtro->getEstado() == 1) {
            $estado = 'En Curso';
        } else {
            $estado = 'Finalizado';
        }
        return array(
            'desde' => !$fechaTodos ? $filtro->getDesde()->format('d-m-Y') : 'Todos',
            'hasta' => !$fechaTodos ? $filtro->getHasta()->format('d-m-Y') : 'Todos',
            'responsable' => $filtro->getResponsable() ? $this->proyectoManager->findResponsable($filtro->getResponsable())->getPersona()->getNombre() : 'Todos',
            'contratista' => $filtro->getResponsable() ? $this->contratistaManager->find($filtro->getContratista())->getNombre() : 'Todos',
            'cliente' => $filtro->getCliente() ? $this->clienteManager->find($filtro->getCliente())->getNombre() : 'Todos',
            'estado' => $estado,
        );
        //   die('////<pre>' . nl2br(var_export($add , true)) . '</pre>////');
    }

    private function getReferenciaForm()
    {
        $options = array(
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'em' => $this->getEntityManager(),
        );
        return $this->createForm(ReferenciaGpmType::class, null, $options);
    }

    private function getMenu($organizacion, $informe, $filtro)
    {
        $menu = array(
            1 => array(
                $this->proyectoRouter->btnNew($organizacion),
            ),
            2 => array(
                $this->proyectoRouter->btnExportar($informe, $filtro),
            ),
            3 => array(
                $this->proyectoRouter->btnMotivo($organizacion),
            ),
            4 => array(
                $this->proyectoRouter->btnNewReferencia($organizacion),
            ),
            5 => array(
                $this->proyectoRouter->btnContratista($organizacion),
            ),
        );
        return $menu;
    }

    public function getMapa()
    {
        $mapa = $this->gmapsManager->createMap(true);
        $mapa->setSize('100%', '250px');
        $mapa->setIniZoom(10);

        $mapa->setIniLatitud($this->userloginManager->getOrganizacion()->getLatitud() != null ? $this->userloginManager->getOrganizacion()->getLatitud() : -32.890722);
        $mapa->setIniLongitud($this->userloginManager->getOrganizacion()->getLongitud() != null ? $this->userloginManager->getOrganizacion()->getLongitud() : -68.839352);

        return $mapa;
    }


    /**
     * @Route("/gpm/proyecto/{idorg}/new", name="gpm_proyecto_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idorg)
    {
        $em = $this->getEntityManager();
        $organizacion = $this->organizacionManager->find($idorg);

        $options = array(
            'organizacion' => $organizacion,
            'em' => $em,
        );
        $proyecto = $this->proyectoManager->create($organizacion);

        $form = $this->createForm(ProyectoNewType::class, $proyecto, $options);
        $form->handleRequest($request);
        //   die('////<pre>' . nl2br(var_export($form->isSubmitted(), true)) . '</pre>////');
        if ($form->isSubmitted() && $form->isValid()) {
            $save = $this->proyectoManager->save($proyecto);
            $this->setFlash('success', sprintf('Proyecto creado correctamente'));
            $this->breadcrumbManager->pop();

            return $this->redirect($this->generateUrl('gpm_proyecto_show', array('id' => $proyecto->getId())));
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Proyecto");
        return $this->render('gpm/Proyecto/new.html.twig', array(
            'proyecto' => $proyecto,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/gpm/proyecto/{id}/edit", name="gpm_proyecto_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Proyecto $proyecto)
    {
        $em = $this->getEntityManager();

        if (!$proyecto) {
            throw $this->createNotFoundException('Código de Proyecto no encontrado.');
        }
        $options = array(
            'organizacion' => $proyecto->getOrganizacion(),
            'em' => $em,
        );

        $form = $this->createForm(ProyectoEditType::class, $proyecto, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $save = $this->proyectoManager->save($proyecto);
            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($proyecto->getNombre())));
            $this->breadcrumbManager->pop();
            return $this->redirect($this->generateUrl('gpm_proyecto_show', array('id' => $proyecto->getId())));
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Modificar Proyecto");
        return $this->render('gpm/Proyecto/edit.html.twig', array(
            'proyecto' => $proyecto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/gpm/proyecto/{id}/show", name="gpm_proyecto_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Proyecto $proyecto)
    {
        if (!$proyecto) {
            throw $this->createNotFoundException('Código de Proyecto no encontrado.');
        }
        $proy = $this->proyectoManager->updateEstadoActividades($proyecto->getOrganizacion()->getProyectos());
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Ver Proyecto");
        $formReferencia = $this->getReferenciaForm();
        $proyecto->setProgreso($this->proyectoManager->getProgressbar($proyecto)['progreso']);
        return $this->render('gpm/Proyecto/show.html.twig', array(
            'proyecto' => $proyecto,
            'organizacion' => $proyecto->getOrganizacion(),
            'mapa' => $this->getMapa(),
            'geocoder' => new Geocoder(),
            'formReferencia' => $formReferencia->createView(),
            'iconos' => glob('images/iconos/*.*'),
        ));
    }

    /**
     * @Route("/gpm/proyecto/getactividades", options={"expose"=true}, name="gpm_proyecto_getactividades")
     * @Method({"GET", "POST"})
     */
    public function getactividadesAction(Request $request)
    {
        $id = $request->get('id');
        $proyecto = $this->proyectoManager->find(intval($id));
        $progreso = $this->proyectoManager->getProgressbar($proyecto);
        return new Response(json_encode(array(
            'html' => $this->renderView('gpm/Actividad/actividades.html.twig', array(
                'proyecto' => $proyecto
            )),
            'htmlSidebar' => $this->renderView('gpm/Actividad/actividades_sidebar.html.twig', array(
                'proyecto' => $proyecto,
                'countActTotales' => $progreso['countActTotales'],
                'countActFinalizadas' => $progreso['countActFinalizadas'],
                'progreso' => $progreso['progreso']
            )),
        )));
    }

    /**
     * @Route("/gpm/proyecto/getproyecto", options={"expose"=true}, name="gpm_proyecto_getproyecto")
     * @Method({"GET", "POST"})
     */
    public function getproyectosAction(Request $request)
    {
        $id = $request->get('id');
        $proyecto = $this->proyectoManager->find(intval($id));

        $progreso = $this->proyectoManager->getProgressbar($proyecto);

        return new Response(json_encode(array(
            'tituloSidebar' => $this->renderView('gpm/Proyecto/titulo_sidebar.html.twig', array(
                'proyecto' => $proyecto
            )),
            'htmlSidebar' => $this->renderView('gpm/Proyecto/proyecto_sidebar.html.twig', array(
                'proyecto' => $proyecto,
                'countActTotales' => $progreso['countActTotales'],
                'countActFinalizadas' => $progreso['countActFinalizadas'],
                'progreso' => $progreso['progreso']
            )),
        )));
    }

    /**
     * @Route("/gpm/proyecto/delete",options={"expose"=true},  name="gpm_proyecto_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $proyecto = $this->proyectoManager->find(intval($id));
        $organizacion = $proyecto->getOrganizacion();
        //  die('////<pre>' . nl2br(var_export($this->getEntityManager()->remove($proyecto), true)) . '</pre>////');
        $this->getEntityManager()->remove($proyecto);
        $ok = $this->getEntityManager()->flush();
        $proyectos = $this->proyectoManager->findAll($organizacion);
        return new Response(json_encode(array(
            'html' => $this->renderView('gpm/Proyecto/proyectos.html.twig', array(
                'proyectos' => $proyectos,
            )),
            'totalProyectos' => count($proyectos)
        )));
    }
}
