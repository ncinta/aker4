<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\informe\InformeChoferType;
use App\Entity\Organizacion;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\IbuttonManager;
use App\Model\app\HistoricoIbuttonManager;
use App\Model\app\GeocoderManager;
use App\Model\app\ChoferManager;
use App\Entity\ChoferServicioHistorico;

/**
 * Description of InformeChoferFlotaController
 *
 * @author claudio
 */
class InformeChoferFlotaController extends AbstractController
{

    private $moduloChofer;
    private $user;
    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $choferManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $ibuttonManager;
    private $histIbuttonManager;
    private $excelManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        IbuttonManager $ibuttonManager,
        HistoricoIbuttonManager $histIbuttonManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager,
        ChoferManager $choferManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
        $this->referenciaManager = $referenciaManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->histIbuttonManager = $histIbuttonManager;
        $this->choferManager = $choferManager;
    }

    /**
     * @Route("/choferflota/{id}/chofer", name="informe_choferflota",
     *     requirements={
     *         "id": "\d+"
     *     },
     *      methods={"GET", "POST"},
     * )
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function choferAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Choferes x Flota");
        //traigo todo los chofere existentes para esta organizacion        
        $this->loggerManager->setTimeInicial();
        //$arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
        $servicios = $this->servicioManager->findByTipoObjeto($organizacion, [1]);
        //dd($servicios);

        $result = $this->generar($servicios);
        $consulta['destino']  = 1;
        //dd($result);
        $this->loggerManager->logInforme($consulta);
        $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

        switch ($consulta['destino']) {
            case '1': //sale a pantalla.
                return $this->render('informe/Choferflota/pantalla.html.twig', array(
                    'consulta' => $consulta,
                    'result' => $result,
                    'time' => $this->loggerManager->getTimeGeneracion(),
                ));
                break;
            case '5':
                //cambiar el return cuando se implemente salida por excel
                //return $this->redirect($this->generateUrl('apmon_informe_infraccion', array('id' => $id)));
                //return $this->exportarAction($request, 0, $consulta, $result, $organizacion, $this->moduloChofer);
                break;
        }

        dd($result);

        return $this->render('informe/Choferflota/chofer.html.twig', array(
            'id' => $organizacion->getId(),
        ));
    }
  

    private function generar($servicios)
    {
        $data = [];
        foreach ($servicios as $servicio) {
            $dataRep = $dataEnt = null;
            $recepcion = $this->choferManager->findUltimoHistorico($servicio, ChoferServicioHistorico::MOTIVO_RECEPCION);
            if (!is_null($recepcion)) {
                // dd($recepcion);
                $dataRep = [
                    'chofer' => $recepcion->getChofer()->getNombre(),
                    'fecha' => $recepcion->getFecha() ? $recepcion->getFecha()->format('d-m-Y H:i') : '---',
                    'estado' => $recepcion->getStrEstadoTemplate(), 
                    'colorEstado' => $recepcion->getStrBadge(),
                    'idFormulario' => $recepcion->getIdFormulario()
                ];
                if ($recepcion->getFecha()) {
                    $recepOffset = $recepcion->getFecha()->getOffset();
                }
            }
            $entrega = $this->choferManager->findUltimoHistorico($servicio, ChoferServicioHistorico::MOTIVO_ENTREGA);
            if (!is_null($entrega)) {
                //dd($entrega);
                $dataEnt = [
                    'chofer' => $entrega->getChofer()->getNombre(),
                    'fecha' => $entrega->getFecha() ? $entrega->getFecha()->format('d-m-Y H:i') : '---',
                    'estado' => $entrega->getStrEstadoTemplate(),
                    'colorEstado' => $entrega->getStrBadge(),
                    'idFormulario' => $entrega->getIdFormulario()
                ];
            }

            if ($dataRep || $dataEnt) {
                $tiempoUso = '';
                if (!is_null($recepcion) && !is_null($entrega)) {
                    if ($recepcion->getFecha() && $entrega->getFecha()) {
                        // Obtener la diferencia en segundos entre los dos puntos en el tiempo
                        $difference_seconds = abs($recepcion->getFecha()->getTimestamp() - $entrega->getFecha()->getTimestamp());

                        // Convertir la diferencia de segundos a horas
                        //$tiempoUso = round($difference_seconds / 3600, 1);
                        $tiempoUso = $this->utilsManager->segundos2tiempo($difference_seconds);
                    }
                }
                $d = [
                    'servicio' => $servicio,
                    'recepcion' => $dataRep,
                    'entrega' => $dataEnt,
                    'check' => '',
                    'tiempoUso' => $tiempoUso
                ];
                $data[] = $d;
            }
        }
        //dd($data);
        return $data;
    }

    private function detalles($servicios)
    {
        $result = array();

        foreach ($servicios as $servicio) {
            $backend = $this->backendManager->informeChofer($servicio->getId(), $desde, $hasta, $referencias);
            foreach ($backend as $data) {
                $ibutton = $this->ibuttonManager->findByCodigo($data['ibutton']);
                if (is_null($ibutton)) {
                    $ibutton = $this->ibuttonManager->findByDallas($data['ibutton']);
                }
                //dd($ibutton);
                $inicio = new \DateTime($data['inicio']['fecha']);
                $fin = new \DateTime($data['fin']['fecha']);
                if ($ibutton && $ibutton->getChofer() != null && $ibutton->getChofer()->getFechaAsignacion() <= $inicio) {

                    //esta asignado todavia
                    $data['chofer'] = $ibutton->getChofer()->getNombre();
                } elseif ($ibutton) {
                    //estuvo asignado antes, busco en el historico
                    $historico = $this->histIbuttonManager->findByIbuttonFecha($ibutton, $inicio, $fin);
                    if ($historico) {
                        $data['chofer'] = $historico->getChofer()->getNombre();
                    }
                }
                if (!isset($data['chofer'])) { //no tengo ibutton registrado.
                    $data['chofer'] = $data['ibutton'];
                }

                //domicilio
                if (isset($data['inicio']['posicion'])) {
                    $direc = $this->geocoderManager->inverso($data['inicio']['posicion']['latitud'], $data['inicio']['posicion']['longitud']);
                    $data['inicio']['domicilio'] = $direc;
                }

                //referencia
                if (isset($data['inicio']['referencia_id']) && !is_null($data['inicio']['referencia_id'])) {
                    $referencia = $this->referenciaManager->find($data['inicio']['referencia_id']);
                    if (!is_null($referencia)) {
                        $data['inicio']['str_referencia'] = $referencia->getNombre();
                    }
                }

                if (isset($data['fin']['posicion'])) {
                    $direc = $this->geocoderManager->inverso($data['fin']['posicion']['latitud'], $data['fin']['posicion']['longitud']);
                    $data['fin']['domicilio'] = $direc;
                }

                //referencia
                if (isset($data['fin']['referencia_id']) && !is_null($data['fin']['referencia_id'])) {
                    $referencia = $this->referenciaManager->find($data['fin']['referencia_id']);
                    if (!is_null($referencia)) {
                        $data['fin']['str_referencia'] = $referencia->getNombre();
                    }
                }
                $data['distancia'] = intval($data['distancia']) / 1000;  //pasamos a km la distancia 
                $data['duracion'] = $this->utilsManager->segundos2tiempo(intval($data['duracion']));

                $result[$servicio->getNombre()][] = $data;
            }
        }
        return $result;
    }
}
