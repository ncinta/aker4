<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformePrudenciaOldType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GeocoderManager;

class InformePrudenciaController extends AbstractController
{

    const MAX_CALLE = 40;

    private $colores = array(
        "leve" => "#CCFFCC",
        "medio" => "#FFFFCC",
        "alto" => "#FFCCCC"
    );
    private $userloginManager;
    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $excelManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ExcelManager $excelManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/informe/prudencia", name="informe_prudencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PRUDENCIA")
     */
    public function prudenciaAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Prudencia Vial");
        $user = $this->userloginManager->getUser();
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario(),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($user->getOrganizacion())
        );
        $form = $this->createForm(InformePrudenciaOldType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeprudencia');
            if (!is_null($request->get('id'))) {
                $consulta['desde'] = $request->get('desde');
                $consulta['hasta'] = $request->get('hasta');
                $consulta['max_ruta'] = $request->get('max_ruta');
                $consulta['max_avenida'] = $request->get('max_avenida');
                $consulta['servicio'] = $request->get('id');
                $consulta['destino'] = 1;
            }
            //paso la fecha a formato yyyy-mm-dd
            $periodo = array(
                'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
            );
            if ($periodo['hasta'] <= $periodo['desde']) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->prudenciaAction($request);
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //son las mismas referencias para todos los servicios
                $referencias = $this->referenciaManager->findAsociadas();

                //establesco sobre que servicios debo controlar.
                if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                    if ($consulta['servicio'] == '0') {
                        $servicios = $this->servicioManager->findAllByUsuario($user);
                        $consulta['servicionombre'] = 'Toda la Flota';
                    } else {
                        $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                        $consulta['servicionombre'] = 'Grupo ' . $grupo->getNombre();
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                    }
                    foreach ($servicios as $servicio) {
                        $arr = $this->getDataInforme($servicio->getId(), $periodo, $consulta, $referencias);
                        $informe = $arr['informe'];
                        $data[] = array(
                            'servicio' => $servicio,
                            'servicio_nombre' => $servicio->getNombre(),
                            'infracciones' => $arr['infracciones']
                        );
                    }
                } else {
                    //aca se obtiene el informe.
                    $servicio = $this->servicioManager->find($consulta['servicio']);
                    $servicios[] = $servicio;
                    $consulta['servicionombre'] = $servicio->getNombre();

                    //este reporte es para un solo vehiculo, lo muestra desglosado.
                    $arr = $this->getDataInforme(intval($consulta['servicio']), $periodo, $consulta, $referencias);
                    $data[] = array(
                        'servicio' => $servicio, 'servicio_nombre' => $servicio->getNombre(),
                        'infracciones' => $arr['infracciones']
                    );
                    $informe[] = array(
                        'servicio' => $servicio->getNombre(),
                        'data' => $arr['informe'],
                    );
                    $consulta['tipo'] = '0';
                }
            }
            $this->loggerManager->logInforme($consulta);

            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    return $this->render('informe/Prudencia/pantalla.html.twig', array(
                        'consulta' => $consulta,
                        'resumen' => $data,
                        'informe' => $informe,
                        'colores' => $this->colores,
                        'time' => $this->loggerManager->getTimeGeneracion(),
                    ));
                    break;
                case '5': //sale a excel
                    return $this->exportarAction($request, 1, $consulta, $informe, $data);
                    break;
            }
        }

        return $this->render('informe/Prudencia/prudencia.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => null,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }



    public function getDataInforme($servicio_id, $periodo, $consulta, $referencias)
    {
        $infracciones = array(
            "objetivos" => array(
                "descripcion" => "Referencias",
                "leve" => 0,
                "medio" => 0,
                "alto" => 0
            ),
            "rutas" => array(
                "descripcion" => "Rutas y Avenidas",
                "leve" => 0,
                "medio" => 0,
                "alto" => 0
            ),
            "calles" => array(
                "descripcion" => "Calles y Caminos",
                "leve" => 0,
                "medio" => 0,
                "alto" => 0
            )
        );


        $informe = array();
        $tmpHistorial = $this->backendManager->historial(intval($servicio_id), $periodo['desde'], $periodo['hasta'], $referencias, array());
        if (is_null($tmpHistorial))
            exit;
        reset($tmpHistorial);
        while ($trama = current($tmpHistorial)) {
            next($tmpHistorial);

            if (isset($trama['posicion'])) {
                $data['fecha'] = $trama['fecha'];
                $data['velocidad'] = $trama['posicion']['velocidad'];
                $data['lugar'] = '---';

                if (isset($trama['referencia_id'])) {
                    $zona = 'objetivos';
                    $ref = $this->referenciaManager->find($trama['referencia_id']);
                    $data['lugar'] = $ref->getNombre();
                    if ($ref->getVelocidadMaxima() != 0) {
                        $vmax = $ref->getVelocidadMaxima();
                    } else {
                        $vmax = false;
                    }
                } else {
                    $vmax = false;
                }
                if (!$vmax) {
                    $direc = $this->geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                    if ($direc != '---') {
                        $data['lugar'] = $this->utilsManager->simplexml2array($direc);
                        $ldir = strtolower($data['lugar']);
                        if (strpos($ldir, "ruta ") !== false || strpos($ldir, "acceso ") !== false) {
                            $zona = "rutas";
                            $vmax = $consulta['max_ruta'];
                        } else if ($ldir != "" && $ldir != "---") {
                            $zona = "calles";
                            if (
                                strpos($ldir, "avenida ") !== false ||
                                strpos($ldir, "av.") !== false ||
                                strpos($ldir, "av ") !== false
                            ) {
                                $vmax = $consulta['max_avenida'];
                            } else {
                                $vmax = self::MAX_CALLE;
                            }
                        } else { // ni idea, puede ser un camino o ruta provincial quien sabe...
                            $zona = "rutas";
                            $vmax = $consulta['max_ruta'];
                        }
                    } else {
                        $zona = 'rutas';
                        $vmax = $consulta['max_ruta'];
                    }
                }

                if ($data['velocidad'] > $vmax * 1.3) {
                    $riesgo = "alto";
                } else if ($data['velocidad'] > $vmax * 1.15) {
                    $riesgo = "medio";
                } else if ($data['velocidad'] > $vmax * 1.0) {
                    $riesgo = "leve";
                } else {
                    $riesgo = false;
                }

                if ($riesgo) {
                    $data['velocidad_maxima'] = $vmax;
                    $data['zona'] = $zona;
                    $data['riesgo'] = $riesgo;

                    //aca armo el informe.
                    $informe[$zona][] = $data;
                    $infracciones[$zona][$riesgo]++;
                }
                unset($data);
            }
            unset($trama);
        }

        return array('informe' => $informe, 'infracciones' => $infracciones);
    }

    /**
     * @Route("/informe/prudencia/{tipo}/exportar", name="informe_prudencia_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PRUDENCIA")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null, $resumen = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $resumen = json_decode($request->get('resumen'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Prudencia Vial");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
                'C5' => 'Velocidad Máx. Ruta',
                'D5' => $consulta['max_ruta'],
                'C6' => 'Velocidad Máx. Avenida',
                'D6' => $consulta['max_avenida'],
            ));

            //resumen de infracciones
            $xls->setBar(8, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Descripción', 'width' => 20),
                'C' => array('title' => 'Riesgo Leve', 'width' => 20),
                'D' => array('title' => 'Riesgo Medio', 'width' => 20),
                'E' => array('title' => 'Riesgo Grave', 'width' => 20)
            ));
            $i = 9;
            foreach ($resumen as $row) {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $row['servicio_nombre']),
                    'B' => array('value' => $row['infracciones']['objetivos']['descripcion']),
                    'C' => array('value' => $row['infracciones']['objetivos']['leve']),
                    'D' => array('value' => $row['infracciones']['objetivos']['medio']),
                    'E' => array('value' => $row['infracciones']['objetivos']['alto']),
                ));
                $i++;
                $xls->setRowValues($i, array(
                    'B' => array('value' => $row['infracciones']['rutas']['descripcion']),
                    'C' => array('value' => $row['infracciones']['rutas']['leve']),
                    'D' => array('value' => $row['infracciones']['rutas']['medio']),
                    'E' => array('value' => $row['infracciones']['rutas']['alto']),
                ));
                $i++;
                $xls->setRowValues($i, array(
                    'B' => array('value' => $row['infracciones']['calles']['descripcion']),
                    'C' => array('value' => $row['infracciones']['calles']['leve']),
                    'D' => array('value' => $row['infracciones']['calles']['medio']),
                    'E' => array('value' => $row['infracciones']['calles']['alto']),
                ));
                $i++;
            }
            $i++;
            if (count($resumen) == 1) {
                //detalle de infracciones
                $xls->setBar($i++, array(
                    'A' => array('title' => 'Fecha', 'width' => 20),
                    'B' => array('title' => 'Lugar', 'width' => 20),
                    'C' => array('title' => 'Máxima (km/h)', 'width' => 20),
                    'D' => array('title' => 'Registrada (km/h)', 'width' => 20),
                    'E' => array('title' => 'Riesgo', 'width' => 20),
                ));
                foreach ($informe as $servicio) {
                    foreach ($servicio['data'] as $key => $zonas) {
                        foreach ($zonas as $row) {
                            $xls->setRowValues($i, array(
                                'A' => array('value' => $row['fecha']),
                                'B' => array('value' => $row['lugar']),
                                'C' => array('value' => $row['velocidad_maxima']),
                                'D' => array('value' => $row['velocidad']),
                                'E' => array('value' => ucfirst($row['riesgo'])),
                            ));
                            $i++;
                        }
                    }
                }
            }


            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=prudenciavial.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_paso'));
        }
    }
}