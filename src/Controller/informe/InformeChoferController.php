<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\informe\InformeChoferType;
use App\Entity\Organizacion;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\IbuttonManager;
use App\Model\app\HistoricoIbuttonManager;
use App\Model\app\GeocoderManager;

/**
 * Description of InformeChoferController
 *
 * @author nicolas
 */
class InformeChoferController extends AbstractController
{

    private $moduloChofer;
    private $user;
    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $ibuttonManager;
    private $histIbuttonManager;
    private $excelManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        IbuttonManager $ibuttonManager,
        HistoricoIbuttonManager $histIbuttonManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
        $this->referenciaManager = $referenciaManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->histIbuttonManager = $histIbuttonManager;
    }

    /**
     * @Route("/chofer/{id}/chofer", name="informe_chofer",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function choferAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Choferes");
        //traigo todo los chofere existentes para esta organizacion
        $options = array(
            'servicios' => $this->servicioManager->findAll($organizacion),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
        );
        $form = $this->createForm(InformeChoferType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->user = $this->getUser2Organizacion($organizacion->getId());
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('chofer');
            if ($consulta) {
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                $this->moduloChofer = $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_CHOFER');
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_chofer', array('id' => $organizacion->getId())));
                }
                $referencias = $this->referenciaManager->findAsociadas($organizacion);
                $result = $this->detalles($arrServicios, $referencias, $desde, $hasta);
                $this->loggerManager->logInforme($consulta);
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                $consulta['servicionombre'] = $arrServicios['servicionombre'];
            }
            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    return $this->render('informe/Chofer/pantalla.html.twig', array(
                        'consulta' => $consulta,
                        'result' => $result,
                        'moduloChofer' => $this->moduloChofer,
                        'time' => $this->loggerManager->getTimeGeneracion(),
                    ));
                    break;
                case '5':
                    //cambiar el return cuando se implemente salida por excel
                    //return $this->redirect($this->generateUrl('apmon_informe_infraccion', array('id' => $id)));
                    return $this->exportarAction($request, 0, $consulta, $result, $organizacion, $this->moduloChofer);
                    break;
            }
        }
        return $this->render('informe/Chofer/chofer.html.twig', array(
            'form' => $form->createView(),
            'id' => $organizacion->getId(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/informe/chofer/{tipo}/exportar", name="informe_chofer_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null, $moduloChofer)
    {
        if ($tipo === 1) { //viene por pantalla
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
            $moduloChofer = false;
            if (is_array($request->get('moduloChofer'))) {
                $moduloChofer = $request->get('moduloChofer');
            }
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Choferes");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Servicio',
                'D3' => $consulta['servicionombre'],
                'C4' => 'Fecha desde',
                'D4' => $consulta['desde'],
                'C5' => 'Fecha hasta',
                'D5' => $consulta['hasta'],
            ));
            $i = 6;
            //esto es para el resumen totalizado de infracciones.
            $xls->setBar($i, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Chofer', 'width' => 20),
                'C' => array('title' => 'Inicio', 'width' => 20),
                'D' => array('title' => 'Referencia', 'width' => 20),
                'E' => array('title' => 'Dirección', 'width' => 40),
                'F' => array('title' => 'Fin', 'width' => 20),
                'G' => array('title' => 'Referencia', 'width' => 20),
                'H' => array('title' => 'Dirección', 'width' => 40),
                'I' => array('title' => 'Distacia', 'width' => 10),
                'J' => array('title' => 'Duración', 'width' => 10),
            ));
            $i++;
            foreach ($informe as $servicio => $detalle) {   //recorro todo el detalle para un servicio
                foreach ($detalle as $value) {   //recorro todo el detalle para un servicio
                    $xls->setCellValue('A' . $i, $servicio);
                    $xls->setCellValue('B' . $i, $value['chofer']);

                    $fecha = new \DateTime($value['inicio']['fecha']);
                    $fecha->setTimezone(new \DateTimeZone($this->userloginManager->getTimeZone()));
                    $xls->setCellValue('C' . $i, $fecha->format('d/m/Y H:i:s'));
                    // $xls->setCellValue('C' . $i, \PHPExcel_Shared_Date::PHPToExcel(strtotime($value['inicio']['fecha'])), 'dd/mm/yyyy HH:MM:SS');

                    $xls->setCellValue('D' . $i, isset($value['inicio']['str_referencia']) ? $value['inicio']['str_referencia'] : '-----');
                    $xls->setCellValue('E' . $i, $value['inicio']['domicilio']);

                    $fecha = new \DateTime($value['fin']['fecha']);
                    $fecha->setTimezone(new \DateTimeZone($this->userloginManager->getTimeZone()));
                    $xls->setCellValue('F' . $i, $fecha->format('d/m/Y H:i:s'));
                    //$xls->setCellValue('F' . $i, \PHPExcel_Shared_Date::PHPToExcel(strtotime($value['fin']['fecha'])), 'dd/mm/yyyy HH:MM:SS');

                    $xls->setCellValue('G' . $i, isset($value['fin']['str_referencia']) ? $value['fin']['str_referencia'] : '-----');
                    $xls->setCellValue('H' . $i, $value['fin']['domicilio']);
                    $xls->setCellValue('I' . $i, $value['distancia']);
                    $xls->setCellValue('J' . $i, $value['duracion']);
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=choferes.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_chofer', array('id' => $this->userloginManager->getOrganizacion()->getId())));
        }
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    /**
     *
        ### Función: `detalles($arrServicios, $referencias, $desde, $hasta)`

        Esta función genera un informe sobre los servicios, obteniendo datos relacionados con el chofer asignado, la distancia recorrida, duración, y las ubicaciones geográficas del inicio y fin de cada servicio en un período de tiempo específico. El flujo de la función es el siguiente:

        1. **Procesamiento de servicios**: Itera sobre la lista de servicios proporcionados, y para cada uno, consulta el backend para obtener los datos correspondientes al rango de fechas.

        2. **Identificación del chofer**:
        - Intenta localizar el identificador `ibutton` asociado a cada servicio, ya sea a través del código del `ibutton` o de su número `Dallas`.
        - Si se encuentra un `ibutton` y este tiene un chofer asignado cuya fecha de asignación es anterior o igual al inicio del servicio, se asocia el chofer con el servicio.
        - Si el `ibutton` no está asignado actualmente, busca en el historial de asignaciones del `ibutton` y asigna el chofer correspondiente si existió previamente.
        - Si no se encuentra un chofer asociado, el sistema utiliza el identificador del `ibutton` como el nombre del chofer.

        3. **Geocodificación**:
        - Si el servicio contiene información sobre las posiciones de inicio o fin, se utiliza un servicio de geocodificación inversa para obtener la dirección física de dichas posiciones.
        - Si las posiciones incluyen referencias, se obtiene el nombre de la referencia asociada, si está disponible.

        4. **Cálculos adicionales**:
        - Convierte la distancia de metros a kilómetros.
        - Convierte la duración del servicio de segundos a un formato de tiempo legible.

        5. **Resultado**: Devuelve un array con los nombres de los servicios como claves, y para cada uno, una lista de los datos del servicio correspondiente, incluyendo chofer, direcciones, referencias, distancia, y duración.

        ---

        Esta función es útil para generar un reporte detallado de los servicios realizados, incluyendo el chofer responsable, la distancia y duración del trayecto, y las ubicaciones de inicio y fin del servicio.
     */
    private function detalles($arrServicios, $referencias, $desde, $hasta)
    {
        $result = array();

        foreach ($arrServicios['servicios'] as $servicio) {
            $backend = $this->backendManager->informeChofer($servicio->getId(), $desde, $hasta, $referencias);
            foreach ($backend as $data) {
                $ibutton = $this->ibuttonManager->findByCodigo($data['ibutton']);
                if (is_null($ibutton)) {
                    $ibutton = $this->ibuttonManager->findByDallas($data['ibutton']);
                }
                //dd($ibutton);
                $inicio = new \DateTime($data['inicio']['fecha']);
                $fin = new \DateTime($data['fin']['fecha']);
                if ($ibutton && $ibutton->getChofer() != null && $ibutton->getChofer()->getFechaAsignacion() <= $inicio) {

                    //esta asignado todavia
                    $data['chofer'] = $ibutton->getChofer()->getNombre();
                } elseif ($ibutton) {
                    //estuvo asignado antes, busco en el historico
                    $historico = $this->histIbuttonManager->findByIbuttonFecha($ibutton, $inicio, $fin);
                    if ($historico) {
                        $data['chofer'] = $historico->getChofer()->getNombre();
                    }
                }
                if (!isset($data['chofer'])) { //no tengo ibutton registrado.
                    $data['chofer'] = $data['ibutton'];
                }

                //domicilio
                if (isset($data['inicio']['posicion'])) {
                    $direc = $this->geocoderManager->inverso($data['inicio']['posicion']['latitud'], $data['inicio']['posicion']['longitud']);
                    $data['inicio']['domicilio'] = $direc;
                }

                //referencia
                if (isset($data['inicio']['referencia_id']) && !is_null($data['inicio']['referencia_id'])) {
                    $referencia = $this->referenciaManager->find($data['inicio']['referencia_id']);
                    if (!is_null($referencia)) {
                        $data['inicio']['str_referencia'] = $referencia->getNombre();
                    }
                }

                if (isset($data['fin']['posicion'])) {
                    $direc = $this->geocoderManager->inverso($data['fin']['posicion']['latitud'], $data['fin']['posicion']['longitud']);
                    $data['fin']['domicilio'] = $direc;
                }

                //referencia
                if (isset($data['fin']['referencia_id']) && !is_null($data['fin']['referencia_id'])) {
                    $referencia = $this->referenciaManager->find($data['fin']['referencia_id']);
                    if (!is_null($referencia)) {
                        $data['fin']['str_referencia'] = $referencia->getNombre();
                    }
                }
                $data['distancia'] = intval($data['distancia']) / 1000;  //pasamos a km la distancia 
                $data['duracion'] = $this->utilsManager->segundos2tiempo(intval($data['duracion']));

                $result[$servicio->getNombre()][] = $data;
            }
        }
        return $result;
    }
}
