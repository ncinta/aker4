<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeEstadiaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;

class InformeEstadiaController extends AbstractController
{

    private $servicioManager;
    private $informeUtilsManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $excelManager;

    function __construct(
        ServicioManager $servicioManager,
        InformeUtilsManager $informeUtilsManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/informe/estadia", name="informe_estadia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_ESTADIA")
     */
    public function estadiaAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Estadía en Referencias");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(InformeEstadiaType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('informeestadia');
            $this->loggerManager->setTimeInicial();
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_estadia'));
                } elseif (!isset($consulta['tiempo_minimo']) || $consulta['tiempo_minimo'] == 0) {
                    $this->get('session')->getFlashBag()->add('error', 'Debe ingresar un tiempo mínimo para la estadía en la referencia. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_estadia'));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //establesco sobre que referencias debo trabajar.
                    if ($consulta['referencia'] == '-1') {    //considerar todas las referencias
                        $referencias = $this->referenciaManager->findAsociadas();
                    } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
                        $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
                    } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
                        $referencias = array();
                    } else {   //es un id de referencia.
                        $referencias[] = $this->referenciaManager->find($consulta['referencia']);
                    }
                    //obtengo los nombres de las referencias.
                    $castReferencia = array();
                    foreach ($referencias as $referencia) {
                        $castReferencia[$referencia->getId()] = $referencia->getNombre();
                    }

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //ejecuto las consultas.
                    $dataRef = array();
                    $options = array('contacto' => true, 'entrada_2' => false);
                    foreach ($arrServicios['servicios'] as $servicio) {  //informe para cada servicio
                        if ($servicio->getEquipo()) {
                            $data = $this->backendManager->informeEstadiaReferencias($servicio->getId(), $periodo['desde'], $periodo['hasta'], $referencias, array('contacto' => true, 'entrada_2' => false));
                            if ($data) {
                                //agrego los campos que no tengo en el informe.
                                foreach ($data as $i => $value) {   //recorro cada paso por la referencia y proceso.
                                    $value['count'] = 0;
                                    //die('////<pre>' . nl2br(var_export($value, true)) . '</pre>////');
                                    if ($value['duracion'] < $consulta['tiempo_minimo'] * 60) {   //filtro de tiempo
                                        $value = null;   //descarto el dato porque es menos tiempo del pedido.
                                    } else {
                                        $value['tiempo'] = isset($value['duracion']) ? $this->utilsManager->segundos2tiempo($value['duracion']) : '0';
                                        if ($value['fin']) {
                                            $value['fecha_egreso'] = 'todavia en ' . $castReferencia[$value['referencia_id']];
                                        }
                                    }

                                    if ($value != null) {  //tengo datos.
                                        //armo el array con las referencias.
                                        $dataRef[$value['referencia_id']]['servicios'][$servicio->getId()]['pasos'][] = $value;
                                        $dataRef[$value['referencia_id']]['servicios'][$servicio->getId()]['nombre'] = $servicio->getNombre();
                                    }
                                }
                            }
                        }
                    } //fin recorrido de servicios
                    //recorro para hacer los count
                    foreach ($dataRef as $i => $ref) {
                        $acu = 0;
                        $dataRef[$i]['nombre'] = $castReferencia[$i];
                        foreach ($ref['servicios'] as $x => $serv) {
                            $dataRef[$i]['servicios'][$x]['count'] = count($serv['pasos']);
                            $acu = $acu + count($serv['pasos']);
                        }
                        $dataRef[$i]['count'] = $acu;
                    }
                }
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Estadia/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $dataRef,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 0, $consulta, $dataRef);
                        break;
                }
            }
        }
        return $this->render('informe/Estadia/estadia.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    function parse($fecha)
    {
        list($fecha, $hora) = explode(" ", $fecha);
        list($y, $m, $d) = explode("-", $fecha);
        list($h, $n, $s) = explode(":", $hora);
        return mktime($h, $n, $s, $m, $d, $y);
    }

    /**
     * @Route("/informe/estadia/{tipo}/exportar", name="informe_estadia_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_ESTADIA")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Estadía en Referencias");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Referencia', 'width' => 40),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Cantidad', 'width' => 10),
                'D' => array('title' => 'Fecha Ingreso', 'width' => 20),
                'E' => array('title' => 'Fecha Egreso', 'width' => 20),
                'F' => array('title' => 'Duración', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $referencia) {   //esto es para cada referencia
                $xls->setCellValue('A' . $i, $referencia['nombre']);
                foreach ($referencia['servicios'] as $servicio) {
                    $xls->setCellValue('B' . $i, $servicio['nombre']);
                    $xls->setCellValue('C' . $i, $servicio['count']);
                    foreach ($servicio['pasos'] as $paso) {
                        $xls->setRowValues($i, array(
                            'D' => array('value' => $paso['fecha_ingreso']),
                            'E' => array('value' => $paso['fecha_egreso']),
                            'F' => array('value' => $paso['tiempo'], 'format' => '[HH]:MM:SS'),
                        ));
                        $i++;
                    }
                }
            }

            //aca hago el resumen si lo piden
            if ($tipo == 1) {
                $i++;
                $xls->setSubTitle('A' . $i, 'RESUMEN');

                $i++;
                $xls->setBar($i, array(
                    'A' => array('title' => 'Referencia', 'width' => 40),
                    'B' => array('title' => 'Servicio', 'width' => 20),
                    'C' => array('title' => 'Cantidad', 'width' => 20),
                ));
                $i++;

                foreach ($informe as $referencias) {   //esto es para cada referencia
                    //$xls->setCellValue('A' . $i, $servicio['servicio']);
                    foreach ($referencias['servicios'] as $servicio) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $referencias['referencia']),
                            'B' => array('value' => $servicio['servicio']),
                            'C' => array('value' => count($servicio['pasos'])),
                        ));
                        $i++;
                    }
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=referencias.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_estadia'));
        }
    }
}
