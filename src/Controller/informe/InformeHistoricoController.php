<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\informe\InformeHistoricoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\ReferenciaManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\GeocoderManager;
use App\Model\app\UserLoginManager;

class InformeHistoricoController extends AbstractController
{

    private $bread;
    private $excelManager;
    private $utilsManager;
    private $backendManager;
    private $referenciaManager;
    private $tanqueManager;
    private $geocoderManager;
    private $servicioManager;
    private $userlogin;

    public function __construct(
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userlogin,
        BackendManager $backendManager,
        ReferenciaManager $referenciaManager,
        TanqueManager $tanqueManager,
        GeocoderManager $geocoderManager,
        ExcelManager $excelManager
    ) {
        $this->userlogin = $userlogin;
        $this->excelManager = $excelManager;
        $this->utilsManager = $utilsManager;
        $this->backendManager = $backendManager;
        $this->referenciaManager = $referenciaManager;
        $this->tanqueManager = $tanqueManager;
        $this->geocoderManager = $geocoderManager;
        $this->servicioManager = $servicioManager;
        $this->bread = $breadcrumbManager;
    }

    /**
     * @Route("/informe/historico", name="informe_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function historicoAction(Request $request)
    {

        //   die('////<pre>'.nl2br(var_export('$info', true)).'</pre>////');
        $this->bread->push($request->getRequestUri(), "Informe Historico");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $form = $this->createForm(InformeHistoricoType::class, null, $options);
        $form->handleRequest($request);
        if (!is_null($request->get('consulta'))) {
            $step = $request->get('step');
            parse_str($request->get('consulta'), $consulta);
            $consulta = $consulta['informehistorico'];
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);

                if ($step == 0)
                    $sendHead = true;

                $periodo = (array)$this->utilsManager->periodo2array($desde, $hasta);
                $informe = $this->getDataInforme(intval($consulta['servicio']), $periodo[$step], $consulta);
                $step++;
                if ($step >= count($periodo))
                    $step = -1;
                return new Response(json_encode(array(
                    'step' => $step,
                    'consulta' => $consulta,
                    'head' => isset($sendHead) && $sendHead ? $this->renderHistoricoHead($consulta) : null,
                    'informe' => $this->renderHistoricoBody($informe, $consulta),
                )));
            }
        }
        return $this->render('informe/Historico/historico.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function renderHistoricoHead($consulta)
    {
        return $this->renderView('informe/Historico/result_head.html.twig', array(
            'consulta' => $consulta,
        ));
    }

    public function renderHistoricoBody($historico, $consulta)
    {
        //return "hola mundo";
        return $this->renderView('informe/Historico/result_body.html.twig', array(
            'historico' => $historico,
            'consulta' => $consulta,
        ));
    }

    public function getDataInforme($servicio_id, $periodo, $consulta)
    {

        if ($consulta['referencia'] == '0') {  //no considerar referencias
            $referencias = array();
        } elseif ($consulta['referencia'] == '-1') {    //considerar todas las referencias
            $referencias = $this->referenciaManager->findAsociadas();
        } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
            $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
        } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
            $referencias = array();
        } else {   //es un id de referencia.
            $referencias[] = $this->referenciaManager->find($consulta['referencia']);
        }
        $servicio = $this->servicioManager->find($servicio_id);
        if ($servicio->getMedidorCombustible()) {
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }

        $historial = array();
        $tmpHistorial = $this->backendManager->historial(intval($servicio_id), $periodo['desde'], $periodo['hasta'], $referencias, array());

        reset($tmpHistorial);

        while ($trama = current($tmpHistorial)) {
            next($tmpHistorial);

            //aca hago el procesamiento de las tramas...
            $trama['distancia'] = isset($trama['distancia']) ? $trama['distancia'] : '0';
            $trama['tiempo'] = isset($trama['tiempo']) ? $this->utilsManager->segundos2tiempo($trama['tiempo']) : '0';

            //referencia
            if (isset($trama['referencia_id']) && !is_null($trama['referencia_id'])) {
                $referencia = $this->referenciaManager->find($trama['referencia_id']);
                if (!is_null($referencia)) {
                    $trama['referencia_nombre'] = $referencia->getNombre();
                }
                unset($referencia);
            }

            //direccion
            if (isset($consulta['mostrar_direcciones']) && $consulta['mostrar_direcciones'] == '1' && isset($trama['posicion'])) {
                $trama['domicilio'] = '---';
                $direc = $this->geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                $trama['domicilio'] = $direc;

                unset($direc);
            }

            //carcontrol si existe.
            if ($servicio->getMedidorCombustible() && isset($isMedicionesLoad)) {
                if (isset($trama['carcontrol']) && isset($trama['carcontrol']['porcentaje_tanque'])) {
                    if ($isMedicionesLoad) {
                        $medicion = $this->tanqueManager->getLitros($trama['carcontrol']['porcentaje_tanque']);
                        if ($medicion['modo'] == -1) {
                            $trama['carcontrol']['litros_tanque'] = '<' . $medicion['litros'];
                        } elseif ($medicion['modo'] == 1) {
                            $trama['carcontrol']['litros_tanque'] = '>' . $medicion['litros'];
                        } else {
                            $trama['carcontrol']['litros_tanque'] = $medicion['litros'];
                        }
                    }
                }
            }

            //recodifico el motivo de transmision.
            if (isset($trama['motivo'])) {
                $trama['motivo'] = $this->servicioManager->getMotivoTx2Str($trama);
            }

            //aca armo el historial.
            $historial[] = $trama;
            unset($trama);
        }
        return $historial;
    }

    /**
     * @Route("/informe/historico/exportar/excel", name="informe_historico_exportar_excel")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function exportarExcelAction(Request $request)
    {

        $consulta = $request->get('informehistorico');
        //paso la fecha a formato yyyy-mm-dd
        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
        $desde_date = new \DateTime($desde);
        $hasta_date = new \DateTime($hasta);
        $interval = date_diff($desde_date, $hasta_date);

        /* si la diferencia es menor a 5 días sale en excel, de lo contrario por cvs 
          ya que mayor a esta cifra la salida por excel explota */
        if ($interval->days < 5) {
            $servicio = $this->servicioManager->find(intval($consulta['servicio']));
            $xls = $this->excelManager->create("Informe Historico de Posiciones");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $servicio->getNombre(),
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            //titulo del excel
            $xls->setBar(6, array(
                'A' => array('title' => 'Fecha', 'width' => 10),
                'B' => array('title' => 'Hora', 'width' => 10),
                'C' => array('title' => 'Fecha Recepción', 'width' => 20),
                'D' => array('title' => 'Latitud', 'width' => 15),
                'E' => array('title' => 'Longitud', 'width' => 15),
                'F' => array('title' => 'Velocidad', 'width' => 10),
                'G' => array('title' => 'Dirección', 'width' => 10),
                'H' => array('title' => 'Motivo', 'width' => 10),
                'I' => array('title' => 'Contacto', 'width' => 10),
                'J' => array('title' => 'Batería', 'width' => 10),
                'K' => array('title' => 'Bat. Ext.', 'width' => 10),
                'L' => array('title' => 'Bat. Ext. (v)', 'width' => 10),
                'M' => array('title' => 'Entrada 1', 'width' => 10),
                'N' => array('title' => 'Entrada 2', 'width' => 10),
                'O' => array('title' => 'Entrada 3', 'width' => 10),
                'P' => array('title' => 'Odómetro', 'width' => 15),
                'Q' => array('title' => 'Horómetro', 'width' => 10),
                'R' => array('title' => 'Tiempo', 'width' => 10),
                'S' => array('title' => 'Distancia', 'width' => 10),
                'T' => array('title' => 'Referencia', 'width' => 20),
                'U' => array('title' => 'Domicilio', 'width' => 20),
                'V' => array('title' => 'Alimentación', 'width' => 10),
                'W' => array('title' => 'Temperatura', 'width' => 10),
                'X' => array('title' => 'Humedad', 'width' => 10),
                'Y' => array('title' => 'Aceleración', 'width' => 10),
                'Z' => array('title' => 'iButton', 'width' => 10),
                'AA' => array('title' => 'ACK %', 'width' => 10),
                'AB' => array('title' => 'GSM Señal (%)', 'width' => 10),
                'AC' => array('title' => 'GSM Error', 'width' => 10),
                'AD' => array('title' => 'GPS Dim', 'width' => 10),
                'AE' => array('title' => 'GPS Error', 'width' => 10),
            ));
            if (isset($consulta['mostrar_sensores'])) {
                $xls->setBar(6, array(
                    'AF' => array('title' => 'RPM Motor', 'width' => 10),
                    'AG' => array('title' => 'Tanque %', 'width' => 10),
                    'AH' => array('title' => 'Tanque Lts.', 'width' => 10),
                    'AI' => array('title' => 'Temp. Motor', 'width' => 10),
                    'AJ' => array('title' => 'Presión Aceite', 'width' => 10),
                    'AK' => array('title' => 'Presión Aceite (bar)', 'width' => 10),
                    'AL' => array('title' => 'Sensor 1', 'width' => 10),
                    'AM' => array('title' => 'Sensor 2', 'width' => 10),
                    'AN' => array('title' => 'Sensor 3', 'width' => 10),
                    'AO' => array('title' => 'Salida 1', 'width' => 10)
                ));
            }

            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            $periodo = (array)$this->utilsManager->periodo2array($desde, $hasta);
            $i = 7;
            foreach ($periodo as $value) {
                $historial = $this->getDataInforme($servicio->getId(), $value, $consulta);
                foreach ($historial as $key => $trama) {
                    $date = explode(" ", $trama['fecha']);
                    $fecha = new \DateTime($trama['fecha']);
                    //$fecha->setTimezone(new \DateTimeZone($this->userlogin->getTimeZone()));
                    $fechaRec = new \DateTime($trama['fecha_recepcion']);
                    //$fechaRec->setTimezone(new \DateTimeZone($this->userlogin->getTimeZone()));
                    //die('////<pre>'.nl2br(var_export($fecha->format('d/m/Y'), true)).'</pre>////');
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $fecha->format('d/m/Y')),
                        'B' => array('value' => $fecha->format('H:i')),
                        'C' => array('value' => $fechaRec->format('d/m/Y H:i')),
                        'D' => array('value' => isset($trama['posicion']['latitud']) ? $trama['posicion']['latitud'] : ''),
                        'E' => array('value' => isset($trama['posicion']['longitud']) ? $trama['posicion']['longitud'] : ''),
                        'F' => array('value' => isset($trama['posicion']['velocidad']) ? $trama['posicion']['velocidad'] : ''),
                        'G' => array('value' => isset($trama['posicion']['direccion']) ? $trama['posicion']['direccion'] : ''),
                        'H' => array('value' => isset($trama['motivo']) ? $trama['motivo'] : 0),
                        'I' => array('value' => isset($trama['contacto']) && $trama['contacto'] ? 'SI' : 'NO'),
                        'J' => array('value' => isset($trama['bateria_interna']) ? $trama['bateria_interna'] : '0'),
                        'K' => array('value' => isset($trama['bateria_externa']) && $trama['bateria_externa'] != -1 ? $trama['bateria_externa'] : 0),
                        'L' => array('value' => isset($trama['bateria_externa_volts']) && $trama['bateria_externa_volts'] != -1 ? $trama['bateria_externa_volts'] : 0),
                        'M' => array('value' => isset($trama['entrada_1']) && $trama['entrada_1'] ? 'SI' : 'NO'),
                        'N' => array('value' => isset($trama['entrada_2']) && $trama['entrada_2'] ? 'SI' : 'NO'),
                        'O' => array('value' => isset($trama['entrada_3']) && $trama['entrada_3'] ? 'SI' : 'NO'),
                        'P' => array('value' => isset($trama['odometro']) ? round($trama['odometro'] / 1000) : 0),
                        'Q' => array('value' => isset($trama['horometro']) ? $trama['horometro'] : 0),
                        'R' => array('value' => isset($trama['tiempo']) ? $trama['tiempo'] : ''),
                        'S' => array('value' => isset($trama['distancia']) ? $trama['distancia'] : 0),
                        'T' => array('value' => isset($trama['referencia_nombre']) ? $trama['referencia_nombre'] : ''),
                        'U' => array('value' => isset($trama['domicilio']) ? $trama['domicilio'] : ''),
                        'V' => array('value' => isset($trama['alimentacion']) && $trama['alimentacion'] ? 'SI' : 'NO'),
                        'W' => array('value' => isset($trama['temperatura']) ? $trama['temperatura'] : ''),
                        'X' => array('value' => isset($trama['humedad']) ? $trama['humedad'] : ''),
                        'Y' => array('value' => isset($trama['aceleracion']) ? $trama['aceleracion'] : ''),
                        'Z' => array('value' => isset($trama['isbutton']) ? $trama['ibutton'] : ''),
                        'AA' => array('value' => isset($trama['porcentaje_ack']) ? $trama['porcentaje_ack'] : ''),
                        'AB' => array('value' => isset($trama['gsm_csq']) ? $trama['gsm_csq'] : ''),
                        'AC' => array('value' => isset($trama['gsm_error']) ? $trama['gsm_error'] : ''),
                        'AD' => array('value' => isset($trama['gps_dim']) ? $trama['gps_dim'] : ''),
                        'AE' => array('value' => isset($trama['gps_error']) ? $trama['gps_error'] : ''),
                    ));
                    if (isset($consulta['mostrar_sensores'])) {
                        $xls->setRowValues($i, array(
                            'AF' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['rpm_motor'] : ''),
                            'AG' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['porcentaje_tanque'] : ''),
                            'AH' => array('value' => isset($trama['carcontrol']['litros_tanque']) ? $trama['carcontrol']['litros_tanque'] : 0),
                            'AI' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['temperatura_motor'] : ''),
                            'AJ' => array('value' => isset($trama['carcontrol']['sensor_presion_aceite']) && $trama['carcontrol']['sensor_presion_aceite'] ? 'SI' : 'NO'),
                            'AK' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['presion_aceite'] : ''),
                            'AL' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_1'] : ''),
                            'AM' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_2'] : ''),
                            'AN' => array('value' => isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_3'] : ''),
                            'AO' => array('value' => isset($trama['salida_1']) && $trama['salida_1'] ? 'SI' : 'NO'),
                        ));
                    }
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=historico.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            return $this->exportarCSVAction($request);
        }
    }

    /**
     * @Route("/informe/historico/exportar/csv", name="informe_historico_exportar_csv")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function exportarCSVAction(Request $request)
    {

        $consulta = $request->get('informehistorico');
        $servicio = $this->servicioManager->find(intval($consulta['servicio']));

        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
        $periodo = (array)$this->utilsManager->periodo2array($desde, $hasta);

        $title = array();
        $title[] = 'Fecha';
        $title[] = 'Fecha Recepción';
        $title[] = 'Latitud';
        $title[] = 'Longitud';
        $title[] = 'Velocidad';
        $title[] = 'Dirección';
        $title[] = 'Motivo TX';
        $title[] = 'Contacto';
        $title[] = 'Batería';
        $title[] = 'Bat. Ext.';
        $title[] = 'Bat. Ext. (v)';
        $title[] = 'Entrada 1';
        $title[] = 'Entrada 2';
        $title[] = 'Entrada 3';
        $title[] = 'Odómetro';
        $title[] = 'Horómetro';
        $title[] = 'Tiempo';
        $title[] = 'Distancia';
        $title[] = 'Referencia';
        $title[] = 'Domicilio';
        $title[] = 'Alimentacion';
        $title[] = 'Temperatura';
        $title[] = 'Humedad';
        $title[] = 'Aceleracion';
        $title[] = 'iButton';
        $title[] = 'ACK %';
        $title[] = 'GSM Señal';
        $title[] = 'GSM Error';
        $title[] = 'GPS Dim';
        $title[] = 'GPS Error';

        if (isset($consulta['mostrar_sensores'])) {
            $title[] = 'RPM Motor';
            $title[] = '% Tanque';
            $title[] = 'Litros Tanque';
            $title[] = 'Temp. Motor';
            $title[] = 'Sensor Aceite';
            $title[] = 'Presión Aceite';
            $title[] = 'Sensor 1';
            $title[] = 'Sensor 2';
            $title[] = 'Sensor 3';
            $title[] = 'Salida 1';
        }



        $info[] = implode(';', $title);
        foreach ($periodo as $value) {
            $historial = $this->getDataInforme($servicio->getId(), $value, $consulta);

            foreach ($historial as $key => $trama) {
                $line = array();
                $line[] = $trama['fecha'];
                $line[] = $trama['fecha_recepcion'];
                $line[] = isset($trama['posicion']['latitud']) ? $trama['posicion']['latitud'] : '';
                $line[] = isset($trama['posicion']['longitud']) ? $trama['posicion']['longitud'] : '';
                $line[] = isset($trama['posicion']['velocidad']) ? $trama['posicion']['velocidad'] : '';
                $line[] = isset($trama['posicion']['direccion']) ? $trama['posicion']['direccion'] : '';
                $line[] = isset($trama['motivo']) ? $trama['motivo'] : 0;
                $line[] = isset($trama['contacto']) ? 'SI' : 'NO';
                $line[] = isset($trama['bateria_interna']) ? $trama['bateria_interna'] : '---';
                $line[] = isset($trama['bateria_externa']) && $trama['bateria_externa'] != -1 ? $trama['bateria_externa'] : 0;
                $line[] = isset($trama['bateria_externa_volts']) && $trama['bateria_externa_volts'] != -1 ? $trama['bateria_externa_volts'] : 0;
                $line[] = isset($trama['entrada_1']) ? 'SI' : 'NO';
                $line[] = isset($trama['entrada_2']) ? 'SI' : 'NO';
                $line[] = isset($trama['entrada_3']) ? 'SI' : 'NO';
                $line[] = isset($trama['odometro']) ? $trama['odometro'] / 1000 : 0;
                $line[] = isset($trama['horometro']) ? $trama['horometro'] : 0;
                $line[] = isset($trama['tiempo']) ? $trama['tiempo'] : '';
                $line[] = isset($trama['distancia']) ? $trama['distancia'] : 0;
                $line[] = isset($trama['referencia_nombre']) ? $trama['referencia_nombre'] : '---';
                $line[] = isset($trama['domicilio']) ? str_replace(',', ' ', $trama['domicilio']) : '---';

                if (isset($trama['alimentacion']))
                    $line[] = (isset($trama['alimentacion']) && $trama['alimentacion']) ? 'SI' : 'NO';
                if (isset($trama['temperatura']))
                    $line[] = isset($trama['temperatura']) ? $trama['temperatura'] : '';
                if (isset($trama['humedad']))
                    $line[] = isset($trama['humedad']) ? $trama['humedad'] : '';
                if (isset($trama['aceleracion']))
                    $line[] = isset($trama['aceleracion']) ? $trama['aceleracion'] : '';
                if (isset($trama['ibutton']))
                    $line[] = isset($trama['ibutton']) ? $trama['ibutton'] : '';
                if (isset($trama['porcentaje_ack']))
                    $line[] = isset($trama['porcentaje_ack']) ? $trama['porcentaje_ack'] : '';
                if (isset($trama['gsm_csq']))
                    $line[] = isset($trama['gsm_csq']) ? $trama['gsm_csq'] : '';
                if (isset($trama['gsm_error']))
                    $line[] = isset($trama['gsm_error']) ? $trama['gsm_error'] : '';
                if (isset($trama['gps_dim']))
                    $line[] = isset($trama['gps_dim']) ? $trama['gps_dim'] : '';
                if (isset($trama['gps_error']))
                    $line[] = isset($trama['gps_error']) ? $trama['gps_error'] : '';
                if (isset($consulta['mostrar_sensores'])) {
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['rpm_motor'] : '';
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['porcentaje_tanque'] : '';
                    $line[] = isset($trama['carcontrol']['litros_tanque']) ? $trama['carcontrol']['litros_tanque'] : 0;
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['temperatura_motor'] : '';
                    $line[] = isset($trama['carcontrol']['sensor_presion_aceite']) && $trama['carcontrol']['sensor_presion_aceite'] ? 'SI' : 'NO';
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['presion_aceite'] : '';
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_1'] : '';
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_2'] : '';
                    $line[] = isset($trama['carcontrol']) ? $trama['carcontrol']['cc_entrada_3'] : '';
                }
                if (isset($consulta['mostrar_sensores'])) {
                    $line[] = isset($trama['salida_1']) ? $trama['salida_1'] : '';
                }
                //                die('////<pre>'.nl2br(var_export($trama, true)).'</pre>////');

                $info[] = implode(';', $line);
            }
        }
        //        die('////<pre>'.nl2br(var_export($info, true)).'</pre>////');
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=historico.csv');
        $response->headers->set('Cache-Control', 'maxage=1');

        $response->setContent(
            $this->render('informe/Historico/export.csv.twig', array(
                'info' => $info,
            ))
        );
        return $response;
    }
}
