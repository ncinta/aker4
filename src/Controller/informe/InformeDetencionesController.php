<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeDetencionesType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\GeocoderManager;

class InformeDetencionesController extends AbstractController
{

    private $user;
    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $excelManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager =  $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/informe/{id}/detenciones", name="informe_detenciones",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DETENCIONES")
     */
    public function detencionesAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe detenciones");

        $this->user = $this->getUser2Organizacion($id);
        //traigo todo los servicios existentes para el usuario
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($this->user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($this->user->getOrganizacion()),
        );

        $form = $this->createForm(InformeDetencionesType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $this->user = $this->getUser2Organizacion($id);

            $consulta = $request->get('informedetenciones');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            $mostrartotal = isset($consulta['mostrar_totales']) ? $consulta['mostrar_totales'] : false;
            $tiempo_minimo = intval($consulta['tiempo_minimo']) * 60;
            $mostrar_direcciones = isset($consulta['mostrar_direcciones']) ? $consulta['mostrar_direcciones'] : false;

            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_detenciones', array('id' => $id)));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //armo los períodos que debo tener en cuenta.
                $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                //obtengo los servicios a incluir en el informe.
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];

                //inicio procesamiento del informe
                $informe = array();
                foreach ($arrServicios['servicios'] as $servicio) {
                    if ($servicio->getEquipo()) {
                        $informe[] = $this->procesarServicio($servicio, $periodo, $tiempo_minimo, $mostrar_direcciones);
                    }
                }

                $this->loggerManager->logInforme($consulta);

                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Detenciones/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '2': //sale a grafico.
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                            $template = 'informe/Detenciones/grafico_flota.html.twig';
                        } else {
                            //obtengo el servicio
                            $template = 'informe/Detenciones/grafico.html.twig';
                        }
                        return $this->render($template, array(
                            'consulta' => $consulta,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                            'dt' => $this->getGraficoInforme($consulta, $informe)
                        ));
                        break;
                    case '3':   //sale al mapa
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                            //esto es un error, porque por mapa no se puede mostrar toda la flota
                            //entonces lo mando a la pantalla por gil....
                            $this->get('session')->getFlashBag()->add('error', 'No se puede mostrar toda la flota en el mapa.');
                            return $this->render('informe/Detenciones/pantalla_flota.html.twig', array(
                                'consulta' => $consulta,
                                'informe' => $informe,
                            ));
                        } else {
                            return $this->render('informe/Detenciones/mapa.html.twig', array(
                                'consulta' => $consulta,
                                'informe' => $informe,
                                'mapa' => $this->crear_mapa($consulta, $informe, $arrServicios),
                            ));
                            //return $this->render_detenciones_mapa($consulta, $informe);
                        }
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 1, $consulta, $informe);
                        break;
                    case '6': //sale a mapa completo
                        $mapa = $this->crear_mapa($consulta, $informe);
                        $this->agregar_recorrido($mapa, $consulta, $arrServicios['servicios']);
                        $this->agregar_referencias($mapa);
                        $info_distancias = $this->getDataInformeDistancias($servicio, $periodo, $consulta);

                        return $this->render('informe/Detenciones/mapa_completo.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'mapa' => $mapa,
                            'informeExt' => $info_distancias,
                        ));
                        break;
                }
            }
        }
        return $this->render('informe/Detenciones/detenciones.html.twig', array(
            'organizacion' => $this->user->getOrganizacion(),
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    private function procesarServicio($servicio, $periodo, $tiempo_minimo, $obtener_direccion)
    {

        $info = $this->getDataInforme($servicio, $periodo, $tiempo_minimo, $obtener_direccion);
        return array(
            'servicio_id' => $servicio->getId(),
            'servicio' => $servicio->getNombre(),
            'count' => count($info),
            'data' => $info,
            'totales' => $this->getTotalesInforme($info)
        );
    }

    public function getTotalesInforme($informe)
    {
        $total = array(
            'segundos_detenido' => 0,
            'tiempo_detenido' => 0,
        );
        foreach ($informe as $value) {
            if (isset($value['segundos_detenido'])) {
                $total['segundos_detenido'] += $value['segundos_detenido'];
            }
        }
        $total['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($total['segundos_detenido']);
        return $total;
    }

    /**
     * @Route("/informe/detenciones/{tipo}/exportar", name="informe_detenciones_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DETENCIONES")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Detenciones");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => 'Toda la flota',
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            //                 die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha Inicio', 'width' => 20),
                'C' => array('title' => 'Fecha Fin', 'width' => 20),
                'D' => array('title' => 'Duración', 'width' => 20),
                'E' => array('title' => 'Referencia', 'width' => 100),
                'F' => array('title' => 'Direccion', 'width' => 100),
            ));
            $i = 7;
            foreach ($informe as $datos) {   //esto es para cada servicio
                $xls->setCellValue('A' . $i, $datos['servicio']);
                foreach ($datos['data'] as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => $value['fecha_ingreso']),
                        'C' => array('value' => $value['fecha_egreso']),
                        'D' => array('value' => $value['duracion'], 'format' => '[HH]:MM:SS'),
                    ));
                    if (isset($value['referencia_nombre'])) {
                        $xls->setRowValues($i, array(
                            'E' => array('value' => isset($value['referencia_nombre']) ? $value['referencia_nombre'] : ''),
                        ));
                    }
                    if (isset($value['domicilio'])) {
                        $xls->setRowValues($i, array(
                            'F' => array('value' => isset($value['domicilio']) ? $value['domicilio'] : ''),
                        ));
                    }
                    $i++;
                }
                if (isset($consulta['mostrar_totales'])) {
                    //se agegan los totales.
                    $totales = $datos['totales'];
                    $xls->setRowTotales($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => 'TOTALES'),
                        'C' => array('value' => ''),
                        'D' => array('value' => $totales['tiempo_detenido'], 'format' => '[h]:mm:ss'),
                    ));
                }
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=detenciones.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_detenciones'));
        }
    }

    public function getGraficoInforme($consulta, $informe)
    {
        $myArray = array();
        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
            foreach ($informe as $key => $value) {
                $myArray[$key]['servicio'] = $value['servicio'];
                $myArray[$key]['detenciones'] = $value['count'];
            }
            return $myArray;
        } else {
            $i = 1; //esto es para un solo servicio
            foreach ($informe as $key => $value) {
                //  die('////<pre>' . nl2br(var_export($value, true)) . '</pre>////');
                if ($value['count'] > 0) {
                    foreach ($value['data'] as $data) {
                        $myArray[$i]['fecha'] = $data['fecha_ingreso'];
                        $myArray[$i]['duracion'] = $data['segundos_detenido'] / 60;
                        $i++;
                    }
                }
            }
            return $myArray;
        }
    }

    private function crear_mapa($consulta, $data)
    {
        $mapa = $this->getMapaInforme($consulta, $data);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $informe = $data[0]['data'];
        $id = 1;
        foreach ($informe as $id => $detencion) {
            $detencion['direccion'] = '0';
            $detencion['velocidad'] = '0';
            $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $detencion, $icon);
            $mark->setTitle($detencion['detencion_id']);

            if ($detencion['referencia_id'] != null) {
                $this->gmapsManager->addMarkerReferencia($mapa, $this->referenciaManager->find($detencion['referencia_id']), true);
            }

            $this->gmapsManager->setFitBounds($mapa, $detencion['latitud'], $detencion['longitud']);
            $id++;
        }

        return $mapa;
    }

    private function agregar_recorrido($mapa, $consulta, $servicios)
    {
        $historial = $this->getHistorial($consulta, $servicios[0]);

        $this->gmapsManager->addPolyline($mapa, 'recorrido', $historial['puntos_polilinea']);
        //puntoA
        $puntoinicio = $this->gmapsManager->createPosicionIcon($mapa, 'puntoA', 'iconA.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'A', $historial['puntoA'], $puntoinicio);
        //puntoB
        $puntofinal = $this->gmapsManager->createPosicionIcon($mapa, 'puntoB', 'iconB.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'B', $historial['puntoB'], $puntofinal);

        return $mapa;
    }

    private function agregar_referencias($mapa)
    {
        //agrego las referencias al mapa
        $referencias = $this->referenciaManager->findAsociadas($this->user->getOrganizacion());
        foreach ($referencias as $referencia) {
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
        }
        $mapa = $this->gmapsManager->setClusterable($mapa, true);
        return $mapa;
    }

    private function getMapaInforme($consulta, $informe)
    {
        $mapa = $this->gmapsManager->createMap();
        return $mapa;
    }

    private function getDataInforme($servicio, $periodo, $tiempo_minimo, $obtener_direccion)
    {

        $referencias = $this->referenciaManager->findAsociadas($this->user->getOrganizacion());

        $informe = array();
        foreach ($periodo as $k => $v) {
            $data = $this->backendManager->informeDetenciones(
                $servicio->getId(),
                $v['desde'],
                $v['hasta'],
                $tiempo_minimo,
                $referencias
            );
            foreach ($data as $key => $value) {
                $data[$key]['detencion_id'] = $key + 1;
                if (isset($value['segundos_detenido'])) {
                    $data[$key]['duracion'] = $this->utilsManager->segundos2tiempo(intval($value['segundos_detenido']));
                } else {
                    $data[$key]['duracion'] = 0;
                }
                //proceso si es en una referencia.
                if (isset($value['referencia_id']) && $value['referencia_id'] != null) {
                    $referencia = $this->referenciaManager->find($value['referencia_id']);
                    if ($referencia) {
                        $data[$key]['referencia_nombre'] = $referencia->getNombre();
                    }
                }
                //obtengo la direccion siempre que se solicite.
                if ($obtener_direccion) {
                    $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                    //die('////<pre>' . nl2br(var_export($direc , true)) . '</pre>////');
                    $data[$key]['domicilio'] = $direc;
                }
                $informe[] = $data[$key];
            }
        }
        return $informe;
    }

    public function getDataInformeDistancias($servicio, $periodo, $consulta)
    {
        $informe = array();
        //pido toda la info al backend.        
        if ($servicio->getUltFechahora() >= $periodo[0]['desde']) {    //evito q se consulte para equipos q no reportan en el periodo
            $informe = $this->backendManager->informeDistancias($servicio->getId(), $periodo[0]['desde'], $periodo[0]['hasta'], array());
        }
        //termino de procesar y agregar nuevos campos calculados al informe.
        $informe['tiempo_activo'] = intval($this->utilsManager->segundos2tiempo($informe['segundos_activo']));
        $informe['tiempo_detenido'] = intval($this->utilsManager->segundos2tiempo($informe['segundos_detenido']));
        $informe['tiempo_movimiento'] = intval($this->utilsManager->segundos2tiempo($informe['segundos_movimiento']));
        $informe['tiempo_motor_encendido'] = intval($this->utilsManager->segundos2tiempo($informe['segundos_motor_encendido']));
        $informe['diferencia_kms'] = abs(intval($informe['km_total']) - intval($informe['kms_calculada']));
        $informe['informar_kms'] = max(intval($informe['km_total']), intval($informe['kms_calculada'])) * 0.05 < intval($informe['diferencia_kms']);
        $informe['km_total'] = 0;
        $informe['km_por_dia'] = 0;
        if (isset($consulta['calcular_distancias']) && $consulta['calcular_distancias'] == "1") {
            $informe['km_total'] = intval($informe['kms_calculada']);
            $informe['km_por_dia'] = intval($informe['kms_por_dia_calculada']);
        }

        return $informe;
    }

    private function getHistorial($consulta, $servicio)
    {
        $opciones = array();
        try {
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            $consHistorial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), $opciones);
        } catch (\Exception $exc) {
            return null;
        }

        $historial = array();
        //$min_lat = $max_lat = $min_lng = $max_lng = false;
        $puntos_polilinea = array();
        $puntoA = null;
        reset($consHistorial);
        while ($trama = current($consHistorial)) {
            next($consHistorial);

            if (isset($trama['posicion'])) {
                $puntos_polilinea[] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                if ($puntoA == null) {
                    $puntoA = array('latitud' => $trama['posicion']['latitud'], 'longitud' => $trama['posicion']['longitud']);
                }
                $ultima = $trama; //es para tener la ultima trama;
                $historial[] = $trama['posicion'];
            }
        }

        unset($consHistorial);

        if (isset($ultima) && !is_null($ultima)) {
            $puntoB = array('latitud' => $ultima['posicion']['latitud'], 'longitud' => $ultima['posicion']['longitud']);
        } else {
            $puntoB = null;
        }

        return array(
            'historial' => $historial,
            'puntos_polilinea' => $puntos_polilinea,
            'puntoA' => $puntoA,
            'puntoB' => $puntoB,
        );
    }
}
