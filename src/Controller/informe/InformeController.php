<?php

namespace App\Controller\informe;

use App\Entity\app\Usuario;
use App\Entity\informe\Inbox;
use App\Entity\informe\prudencia_vial\v1\Excesos;
use App\Entity\informe\prudencia_vial\v1\Servicio;
use App\Entity\Organizacion;
use App\Entity\informe\prudencia_vial\v1\Exceso;
use App\Entity\informe\prudencia_vial\v1\Posicion;
use App\Entity\informe\prudencia_vial\v1\Referencia;
use App\Form\informe\InformeExcesosVelocidadType;
use App\Model\app\BackendManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DominioManager;
use App\Model\app\GmapsManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\Router\InformeRouter;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use App\Model\informe\SchemaManager;
use App\Form\informe\InformePrudenciaType;
use App\Form\informe\InformeResponsabilidadType;
use App\Form\informe\InformeResponsabilidadV2Type;
use App\Model\app\ChoferManager;
use App\Model\app\ExcelManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\informe\exceso_velocidad\v1\ExcesoVelocidadManager;
use App\Model\informe\prudencia_vial\v1\PrudenciaVialManager;
use App\Model\informe\responsabilidad_vial\v1\ResponsabilidadVialManager;
use App\Model\informe\responsabilidad_vial\v2\ResponsabilidadVialManager as ResponsabilidadVialV2Manager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class InformeController extends AbstractController
{

    private $breadcrumbManager;
    private $userlogin;
    private $dominioManager;
    private $notificador;
    private $entityManager;
    private $schemaManager;
    private $informeManager;
    private $gmapsManager;
    private $referenciaManager;
    private $utilsManager;
    private $backendManager;
    private $informeRouter;
    private $servicioManager;
    private $grupoServicioManager;
    private $grupoReferenciaManager;
    private $excelManager;
    private $prudenciaVialManager;
    private $responsabilidadVialManager;
    private $session;
    private $excesoVelocidadManager;
    private $choferManager;
    private $responsabilidadVialV2Manager;


    public function __construct(
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userlogin,
        DominioManager $dominioManager,
        NotificadorAgenteManager $notificador,
        EntityManagerInterface $entityManager,
        SchemaManager $schemaManager,
        InformeManager $informeManager,
        GmapsManager $gmapsManager,
        ReferenciaManager $referenciaManager,
        UtilsManager $utilsManager,
        BackendManager $backendManager,
        InformeRouter $informeRouter,
        ServicioManager $servicioManager,
        GrupoServicioManager $grupoServicioManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        ExcelManager $excelManager,
        PrudenciaVialManager $prudenciaVialManager,
        ResponsabilidadVialManager $responsabilidadVialManager,
        ResponsabilidadVialV2Manager $responsabilidadVialV2Manager,
        SessionInterface $session,
        ExcesoVelocidadManager $excesoVelocidadManager,
        ChoferManager $choferManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userlogin = $userlogin;
        $this->dominioManager = $dominioManager;
        $this->notificador = $notificador;
        $this->entityManager = $entityManager;
        $this->schemaManager = $schemaManager;
        $this->informeManager = $informeManager;
        $this->gmapsManager = $gmapsManager;
        $this->referenciaManager = $referenciaManager;
        $this->utilsManager = $utilsManager;
        $this->backendManager = $backendManager;
        $this->informeRouter = $informeRouter;
        $this->servicioManager = $servicioManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->excelManager = $excelManager;
        $this->prudenciaVialManager = $prudenciaVialManager;
        $this->responsabilidadVialManager = $responsabilidadVialManager;
        $this->session = $session;
        $this->excesoVelocidadManager = $excesoVelocidadManager;
        $this->choferManager = $choferManager;
        $this->responsabilidadVialV2Manager = $responsabilidadVialV2Manager;
    }


    /**
     * @Route("/informe/{id}/homepage", name="informe_home",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function homepageAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->clear($request->getRequestUri(), "Informes", "Inbox");
        $user = $this->userlogin->getUser();
        //  $this->session->clear();
        $inbox = $this->informeManager->getInbox($user);
        $menu[1] = array(
            $this->informeRouter->btnNew($organizacion)
        );
        $informe = new Inbox();
        $informes = $informe->getAllInformes();
        return $this->render('informe/Home/homepage.html.twig', array(
            'inbox' => $inbox,
            'organizacion' => $organizacion,
            'menu' => $menu,
            'informes' => $informes
        ));
    }

    /**
     * @Route("/informe/{id}/new", name="informe_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->clear($request->getRequestUri(), "Informes", "Inbox");
        $user = $this->userlogin->getUser();

        $informe = new Inbox();
        $informes = $informe->getAllInformes();
        return $this->render('informe/Home/new.html.twig', array(
            'organizacion' => $organizacion,
            'informes' => $informes
        ));
    }


    /**
     * @Route("/informe/{id}/show", name="informe_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Inbox $inbox)
    {
        $this->breadcrumbManager->clear($request->getRequestUri(), $inbox->getStrTipo($inbox->getTipo()), " Show");

        $inbox->setVisto(true);
        $inbox = $this->informeManager->save($inbox);

        //datos del formulario que generamos para mostrar en el encabezado del show
        $dataInforme = $inbox->getDataInforme();

        //datos del informe mismo
        $datos = $this->getDataByInforme($inbox);
        $data = $datos['data'];
        // dd($data);
        $notIsEmpty = $this->verifyEmpty($datos);

        //traemos el template correspondiente, de acuerdo al tipo de informe que queremos mostrar
        $template = sprintf('informe/Templates/%s', $inbox->getTemplate($inbox->getTipo()));


        return $this->render($template, array(
            'dataInforme' => $dataInforme,
            'mapa' => $this->gmapsManager->createMap(),
            'inbox' => $inbox,
            'data' => $data,
            'notIsEmpty' => $notIsEmpty,
            'organizacion' => $inbox->getOrganizacion(),
            'isExport' =>  $datos['isExport'], //para mostrar el boton de exportar a excel, hay forms que no exportan y otros si
        ));
    }

    // verifica que tenga datos, sino que no muestre nada
    private function verifyEmpty($datos)
    {
        $mayorCero = false;
        if ($datos['data'] != null && count($datos['data']) > 0) {

            foreach ($datos['data'] as $item) {
                if ($item['total'] > 0) {
                    $mayorCero = true;
                    break;
                }
            }
        }

        return $mayorCero;
    }

    private function getDataByInforme($inbox)
    {
        $data = null;
        $isExport = false;
        $dataInforme = $inbox->getDataInforme();
        $desde = $this->utilsManager->datetime2sqltimestamp($inbox->getFechaDesde());
        $hasta = $this->utilsManager->datetime2sqltimestamp($inbox->getFechaHasta());
        $key = $inbox->getInforme($inbox->getTipo());

        $managers = [
            'prudencia_vial' => $this->prudenciaVialManager,
            'indice_responsabilidad_vial' => $this->responsabilidadVialManager,
            'indice_responsabilidad_vial_v2' => $this->responsabilidadVialV2Manager,
            'exceso_velocidad' => $this->excesoVelocidadManager,
        ];
        $ids = [];

        if (array_key_exists($key, $managers)) {
            if ($key != 'indice_responsabilidad_vial_v2') {
                $ids = $this->servicioManager->getIds($dataInforme['servicio'])['ids'];
            } else {
                $ids = $this->choferManager->getIds($dataInforme['chofer'])['ids'];
            }
            $sessionKey = $key . $inbox->getId();
            $data = $this->session->get($sessionKey);   
           // $data = null;         
            if (!$data) {                
                $data = $managers[$key]->getResumen($inbox->getUid(), $ids, $desde, $hasta);
                $this->session->set($sessionKey, $data);
            }

            $isExport = true; // Avisamos que este informe exporta a excel, que muestre el botón
        }

        return ['data' => $data, 'isExport' => $isExport];
    }

    /**
     * Función que mostrará los datos en el popup cuando
     * @Route("/informe/getdetalle", options={"expose"=true}, name="informe_getdetalle")
     * @Method({"GET", "POST"})
     * 
     */
    public function getDataDetalleInforme(Request $request)
    {
        $tipo = $request->get('tipo');
        $uid = $request->get('uid');

        // Inicializamos las variables
        $html = null;
        $response = null;
        $data = [];

        // Obtenemos el inbox y datos comunes        
        $inbox = $this->informeManager->findByUid($uid);
        $dataInforme = $inbox->getDataInforme();

        $desde = $this->utilsManager->datetime2sqltimestamp($inbox->getFechaDesde());
        $hasta = $this->utilsManager->datetime2sqltimestamp($inbox->getFechaHasta());
        // dd($data);
        // Configuración de gestores y vistas
        $tiposInforme = [
            Inbox::TIPO_PRUDENCIA_VIAL => [
                'manager' => $this->prudenciaVialManager,
                'template' => 'informe/Templates/prudencia_vial/v1/detalle.html.twig'
            ],
            Inbox::TIPO_EXCESO_VELOCIDAD => [
                'manager' => $this->excesoVelocidadManager,
                'template' => 'informe/Templates/exceso_velocidad/v1/detalle.html.twig'
            ],
            Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL => [
                'manager' => $this->responsabilidadVialManager,
                'template' => 'informe/Templates/responsabilidad_vial/v1/detalle.html.twig'
            ],
            Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2 => [
                'manager' => $this->responsabilidadVialV2Manager,
                'template' => 'informe/Templates/responsabilidad_vial/v2/detalle.html.twig'
            ]
        ];

        // Si el tipo de informe existe en la configuración
        if (array_key_exists($tipo, $tiposInforme)) {
            $key = $inbox->getInforme($inbox->getTipo());
            $manager = $tiposInforme[$tipo]['manager'];
            $template = $tiposInforme[$tipo]['template'];
            if ($key != 'indice_responsabilidad_vial_v2') {
                $ids = $this->servicioManager->getIds($dataInforme['servicio'])['ids'];
                $servicio_id = $request->get('servicio');
                $data = $manager->getDetalle($inbox->getUid(), $servicio_id, $desde, $hasta);
            } else {
                $ids = $this->choferManager->getIds($dataInforme['chofer'])['ids'];
                $chofer_id = intval($request->get('chofer'));
                $data = $manager->getDetalle($inbox->getUid(), $chofer_id, $desde, $hasta);
            }
            // Obtener los datos del informe
            $referenciasHidden = $manager->getReferenciasHidden($inbox->getUid(), $ids, $desde, $hasta);
            // Renderizar la vista
            $html = $this->renderView($template, ['data' => $data['data']]);


            // Construir la respuesta
            $response = [
                'excesos' => $data['data']['posiciones'],
                'referenciasShow' => $data['referenciasShow'],
                'referenciasHidden' => $referenciasHidden,
            ];
        }

        // Retornar la respuesta en formato JSON
        return new Response(
            json_encode([
                'html' => $html,
                'response' => $response
            ])
        );
    }

    /**
     * Función que devuelve el form del informe correspondiente que se eligió
     * @Route("/informe/getform", options={"expose"=true}, name="informe_getform")
     * @Method({"GET", "POST"})
     */
    public function getFormInforme(Request $request)
    {
        $tipo = intval($request->get('informe_tipo'));
        $organizacion = $this->userlogin->getOrganizacion();
        $form = null;
        $html = null;
        switch ($tipo) {
            case Inbox::TIPO_PRUDENCIA_VIAL:
                $form = $this->createForm(InformePrudenciaType::class, null, $this->getOptsServAndRef($organizacion));
                $html = $this->renderView('informe/Formularios/prudencia_vial/v1/prudencia_vial.html.twig', array('form' => $form->createView()));
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL:
                $form = $this->createForm(InformeResponsabilidadType::class, null, $this->getOptsServAndRef($organizacion));
                $html = $this->renderView('informe/Formularios/responsabilidad_vial/v1/responsabilidad_vial.html.twig', array('form' => $form->createView()));
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2:
                $form = $this->createForm(InformeResponsabilidadV2Type::class, null, $this->getOptsChoferAndRef($organizacion));
                $html = $this->renderView('informe/Formularios/responsabilidad_vial/v2/responsabilidad_vial.html.twig', array('form' => $form->createView()));
                break;
            case Inbox::TIPO_EXCESO_VELOCIDAD:
                $form = $this->createForm(InformeExcesosVelocidadType::class, null, $this->getOptsServAndRef($organizacion));
                $html = $this->renderView('informe/Formularios/exceso_velocidad/v1/exceso_velocidad.html.twig', array('form' => $form->createView()));
                break;
            default:
                $form = null;
                break;
        }

        return new Response(
            json_encode(array(
                'html' => $html
            ))
        );
    }

    private function getOptsServAndRef($organizacion)
    {
        return array(
            'servicios' => $this->servicioManager->findAllByUsuario(),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
            'referencias' => $this->referenciaManager->findAsociadas($organizacion),
            'gruposRef' => $this->grupoReferenciaManager->findAllByOrganizacion($organizacion),
        );
    }

    private function getOptsChoferAndRef($organizacion)
    {
        return array(
            'choferes' => $this->choferManager->findAll($organizacion),
            'referencias' => $this->referenciaManager->findAsociadas($organizacion),
            'gruposRef' => $this->grupoReferenciaManager->findAllByOrganizacion($organizacion),
        );
    }

    /**
     * Función que toma los datos del form y se los envía a la API de informes para generar el informe
     * @Route("/informe/sendform", name="informe_sendform")
     * @Method({"GET", "POST"})
     */
    public function sendFormInforme(Request $request)
    {
        $tmp = null;
        parse_str($request->get('form'), $tmp);
        $organizacion = $this->userlogin->getOrganizacion();
        $usuario = $this->userlogin->getUser();
        $tipo = $request->get('tipo');

        // Configuración de los diferentes tipos de informes
        $tiposInforme = [
            Inbox::TIPO_PRUDENCIA_VIAL => [
                'keyForm' => 'informeprudencia',
                'camposRequeridos' => ['asunto', 'desde', 'hasta', 'max_ruta', 'max_avenida']
            ],
            Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL => [
                'keyForm' => 'informeresponsabilidad',
                'camposRequeridos' => ['asunto', 'desde', 'hasta', 'max_referencia', 'max_resto']
            ],
            Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2 => [
                'keyForm' => 'informeresponsabilidadv2',
                'camposRequeridos' => ['asunto', 'desde', 'hasta', 'max_referencia', 'max_resto']
            ],
            Inbox::TIPO_EXCESO_VELOCIDAD => [
                'keyForm' => 'informeexcesosvelocidad',
                'camposRequeridos' => ['asunto', 'desde', 'hasta', 'velocidad_maxima']
            ]
        ];

        // Validar el tipo de informe
        if (!isset($tiposInforme[$tipo])) {
            return new Response(
                json_encode(['error' => 'Tipo de informe no válido']),
                400
            );
        }
        // Obtener los datos según el tipo de informe
        $config = $tiposInforme[$tipo];
        $data = $tmp[$config['keyForm']];

        // Validar campos requeridos
        $missingFields = $this->validarCamposRequeridos($data, $config['camposRequeridos']);
        if ($missingFields) {
            return new Response(
                json_encode(['error' => 'Faltan campos: ' . implode(', ', $missingFields)]),
                500
            );
        }

        // Agregar datos comunes
        $data['usuario'] = $usuario;
        $data['organizacion'] = $organizacion;

        // Enviar informe
        $informe = $this->informeManager->sendInforme($data, $tipo); //envía y crea el informe

        return new Response(
            json_encode(['id' => $organizacion->getId()])
        );
    }

    /**
     * Función auxiliar para validar si los campos requeridos están presentes y no están vacíos.
     */
    private function validarCamposRequeridos(array $data, array $camposRequeridos)
    {
        $camposFalta = [];
        foreach ($camposRequeridos as $field) {
            if (empty($data[$field])) {
                $camposFalta[] = $field;
            }
        }
        return $camposFalta;
    }

    private function response($usuario)
    {
        $inbox = $this->informeManager->getInbox($usuario);
        $html = $this->renderView('informe/Home/content_informes.html.twig', array('inbox' => $inbox));
        return new Response(
            json_encode(array(
                'html' => $html
            ))
        );
    }

    /**
     * Función que chequea el estado del informe en la api de informes
     * @Route("/informe/checkestado", options={"expose"=true}, name="informe_checkestado")
     * @Method({"GET", "POST"})
     */
    public function checkEstadoAction(Request $request)
    {
        $form = null;
        $html = null;
        $informe = null;
        $uid = $request->get('uid');
        $inbox = $this->informeManager->findByUid($uid);
        $organizacion = $this->userlogin->getOrganizacion();
        switch ($inbox->getTipo()) {
            case Inbox::TIPO_PRUDENCIA_VIAL:
                $informe = $this->prudenciaVialManager->getInformeByUid($uid);
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL:
                $informe = $this->responsabilidadVialManager->getInformeByUid($uid, 1);
                break;
            case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2:
                $informe = $this->responsabilidadVialV2Manager->getInformeByUid($uid, 2);
                break;
            case Inbox::TIPO_EXCESO_VELOCIDAD:
                $informe = $this->excesoVelocidadManager->getInformeByUid($uid);
                break;

            default:

                break;
        }

        if ($informe != null) {
            if ($informe->getEstado() == 'finalizado') {
                $inbox->setEstado(Inbox::ESTADO_FINALIZADO);
            } else {
                $inbox->setEstado(Inbox::ESTADO_GENERANDO);
            }
            $inbox = $this->informeManager->save($inbox);
            if ($inbox != null) {
                $htmlSpan = $this->renderView('informe/Home/span-status.html.twig', array('informe' => $inbox));
                $htmlHref = $this->renderView('informe/Home/asunto-href.html.twig', array('informe' => $inbox));
                return  new Response(
                    json_encode(array(
                        'htmlSpan' => $htmlSpan,
                        'htmlHref' => $htmlHref,
                    ))
                );
            }
        }
        return new Response(
            json_encode(array(
                'status' => false,
            ))
        );
    }


    /**
     * @Route("/informe/export", options={"expose"=true}, name="informe_export")
     * @Method({"POST"})
     */
    public function exportAction(Request $request)
    {

        if ($request->isMethod('POST')) {
            $uid = $request->get('uid');

            $inbox = $this->informeManager->findByUid($uid);
            $this->schemaManager->changeSchema($inbox->getSchema($inbox->getTipo()));
            // Obtener el EntityManager de la conexión 'informes'
            $this->schemaManager->changeSchema(); //volvemos al public            
            $dataInforme = $inbox->getDataInforme();
            // Verificar si los datos se han decodificado correctamente
            $key = $inbox->getInforme($inbox->getTipo());
            $sessionKey = $key . $inbox->getId();
            if ($dataInforme !== null && $uid !== null) {
                $nombre = $inbox->getStrTipo($inbox->getTipo());
                switch ($inbox->getTipo()) {
                    case Inbox::TIPO_PRUDENCIA_VIAL:
                        $informe = $this->prudenciaVialManager->getInformeByUid($inbox->getUid()); //envía y crea el informe 
                        $datos = $this->getDataByInforme($inbox, $informe);
                        $data = $datos['data'];
                        $isExport = $datos['isExport'];
                        $xls = $this->prudenciaVialManager->getExcelPrudenciaVial($data, $dataInforme);
                        break;
                    case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL:
                        // Recuperar el array desde la sesión
                        $data = $this->session->get($sessionKey);

                        if ($dataInforme === null) {
                            // Manejar el caso en que no haya datos en la sesión
                            throw $this->createNotFoundException('No se encontraron datos para exportar.');
                        }
                        $xls = $this->responsabilidadVialManager->getExcelResponsabilidadVial($data, $dataInforme);
                        $nombre = 'responsabilidad_vial';
                        break;
                    case Inbox::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2:
                        // Recuperar el array desde la sesión
                        $data = $this->session->get($sessionKey);

                        if ($dataInforme === null) {
                            // Manejar el caso en que no haya datos en la sesión
                            throw $this->createNotFoundException('No se encontraron datos para exportar.');
                        }
                        $xls = $this->responsabilidadVialV2Manager->getExcelResponsabilidadVial($data, $dataInforme);
                        $nombre = 'responsabilidad_vial';
                        break;
                    case Inbox::TIPO_EXCESO_VELOCIDAD:
                        // Recuperar el array desde la sesión
                        $data = $this->session->get($sessionKey);

                        if ($dataInforme === null) {
                            // Manejar el caso en que no haya datos en la sesión
                            throw $this->createNotFoundException('No se encontraron datos para exportar.');
                        }
                        $xls = $this->excesoVelocidadManager->getExcelExcesoVelocidad($data, $dataInforme);
                        $nombre = 'exceso_velocidad';
                        break;

                    default:

                        break;
                }
            } else {
                return new Response('Invalid JSON data: ' . json_last_error_msg(), Response::HTTP_BAD_REQUEST);
            }
        }

        $response = $xls->getResponse();
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . $nombre . '.xls');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Pragma', 'public');

        return $response;
    }
}
