<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeResponsabilidadOldType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ChoferManager;
use App\Model\app\GrupoReferenciaManager;

/**
 * Description of InformeResponsabilidadVial
 *
 * @author nicolas
 */
class InformeResponsabilidadVialController extends AbstractController
{

    const COMIENZO = 'B';

    //velocidades km/h maximas por default
    const MAX_LIVIANOS = 110;
    const MAX_PESADOS = 80;
    const MAX_RUTAS = 110;
    const MAX_AVENIDA = 60;
    const MAX_CALLE = 40;

    //indices
    const MIN_IR_ALTO = 1000;
    const MIN_IR_MEDIO = 500;
    const IR_ALTO = 2;
    const IR_MEDIO = 1.5;

    //motivos de tx
    const MOTIVOTX_ACELERACION = 23;     //23
    const MOTIVOTX_FRENADA = 24;        //24
    const MOTIVOTX_GIRO = 135;           //135

    const referencias = 1;
    const otros = 2;

    //const calles = 2;
    private $moduloChofer;
    private $encabezado = array(
        '1' => 'Referencias',
        '2' => 'Fuera Ref.',
    );
    private $colores = array(
        "nada" => "#FFFFFF",
        "leve" => "#CCFFCC",
        "medio" => "#FFFFCC",
        "alto" => "#FFCCCC"
    );
    private $titulos = array(
        'Aceleración', 'Freno', 'Giro', 'Total K.m.', 'Hs. Encendido', 'Hs. Detenido', 'I.R. Total'
    );
    private $servicioManager;
    private $grupoReferencia;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $excelManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $choferManager;
    private $userloginManager;
    private $organizacionManager;

    function __construct(
        ServicioManager $servicioManager,
        GrupoReferenciaManager $grupoReferencia,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ExcelManager $excelManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        ChoferManager $choferManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->grupoReferencia = $grupoReferencia;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->choferManager = $choferManager;
        $this->userloginManager = $userloginManager;
        $this->organizacionManager = $organizacionManager;
    }

    /**
     * @Route("/informe/responsabilidad", name="informe_responsabilidad")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PRUDENCIA")
     */
    public function responsabilidadAction(Request $request)
    {

        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();

        $form = $this->createForm(InformeResponsabilidadOldType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->loggerManager->setTimeInicial();
            $organizacion = $this->userloginManager->getOrganizacion();
            $this->moduloChofer = $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_CHOFER');
            $consulta = $request->get('informeresponsabilidad');
            $data = array();
            $informe = null;
            //paso la fecha a formato yyyy-mm-dd
            $periodo = array(
                'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
            );
            if ($periodo['hasta'] <= $periodo['desde']) {
                $this->get('session')->setFlash('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_responsabilidad'));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                //aca se obtiene el informe.
                if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                    $varios = true;
                    if ($consulta['servicio'] == '0') {
                        $servicios = $this->servicioManager->findAllByUsuario();
                        $consulta['servicionombre'] = 'toda la flota';
                    } else {
                        $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                        $consulta['servicionombre'] = 'grupo "' . $grupo->getNombre() . '"';
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                    }
                } else {
                    $servicios = array($this->servicioManager->find($consulta['servicio']));
                    $consulta['servicionombre'] = $servicios[0]->getNombre();
                    $varios = false;
                }
                if ($consulta['referencia'] == -1) {
                    $referencia = 'Todas las referencias';
                } elseif (substr($consulta['referencia'], 0, 1) == '*') {
                    $referencia = 'Grupo ' . $this->grupoReferencia->find(substr($consulta['referencia'], 1, strlen($consulta['referencia'])))->getNombre();
                } else {
                    $referencia = $this->referenciaManager->find($consulta['referencia'])->getNombre();
                }
                $consulta['referencianombre'] = $referencia;

                //saco las referencias para todos los servicios ya que son las mismas
                $referencias = $this->getReferencias($consulta);
                $this->cache = array();
                foreach ($servicios as $servicio) {
                    if ($servicio->getEquipo()) {
                        $informe = $this->getDataInforme($servicio, $periodo, $consulta, $referencias);

                        if ($informe['total'] > 0) {
                            $data[] = array(
                                'servicio' => $servicio->getNombre(),
                                'infracciones' => $informe['infracciones'],
                                'freno' => $informe['freno'],
                                'IRTOTAL' => $informe['IRTOTAL'],
                                'aceleracion' => $informe['aceleracion'],
                                'giro' => $informe['giro'],
                                'totales' => $this->getDataTotales($servicio, $periodo, $referencias),
                                'detalle' => $varios ? null : $informe['informe'],
                            );
                        }
                    }
                }

                $this->loggerManager->logInforme($consulta);
            }

            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) === 'g') {
                        $template = 'informe/Responsabilidad/pantalla.html.twig';
                    } else {
                        $template = 'informe/Responsabilidad/pantalla_unitario.html.twig';
                    }
                    return $this->render($template, array(
                        'consulta' => $consulta,
                        'informe' => $data,
                        'colores' => $this->colores,
                        'encabezado' => $this->encabezado,
                        'moduloChofer' => $this->moduloChofer,
                        'time' => $this->loggerManager->getTimeGeneracion(),
                    ));
                    break;
                case '5': //sale a excel
                    return $this->exportarAction($request, 1, $consulta, $data, $this->moduloChofer, $this->encabezado);
                    break;
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Indice de responsabilidad Vial");
        return $this->render('informe/Responsabilidad/responsabilidad.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => null,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function getDataInforme($servicio, $periodo, $consulta, $referencias)
    {
        $total = 0;
        $aceleracion = 0;
        $freno = 0;
        $giro = 0;
        $informe = array();
        $infracciones = array(
            self::referencias => array( // otros lugares fuera de las referencias.
                "leve" => 0,
                "medio" => 0,
                "alto" => 0,
                "nada" => 0,
                "IR" => 0
            ),
            self::otros => array( // otros lugares fuera de las referencias.
                "leve" => 0,
                "medio" => 0,
                "alto" => 0,
                "nada" => 0,
                "IR" => 0
            ),
        );
        
        //$referencias = array();
        $fields = array_flip(array('posicion', 'fecha', 'referencia_id', 'motivo', 'ibutton'));
        $tmpHistorial = $this->backendManager->historial(intval($servicio->getId()), $periodo['desde'], $periodo['hasta'], $referencias, array('fields' => $fields));
        if (is_null($tmpHistorial))
            exit;
        reset($tmpHistorial);

        while ($trama = current($tmpHistorial)) {
            next($tmpHistorial);
            if (isset($trama['posicion'])) {
                $data['fecha'] = $trama['fecha'];
                $data['velocidad'] = $trama['posicion']['velocidad'];
                $data['lugar'] = '---';
                $data['chofer'] = '';
                if ($this->moduloChofer) {
                    //chofer
                    if (isset($trama['ibutton']) && $trama['ibutton'] != 0) {
                        $chofer = $this->choferManager->findByDallas($trama['ibutton']);
                        if ($chofer) {
                            $data['chofer'] = $chofer->getNombre();
                        } else {
                            $data['chofer'] = $trama['ibutton'];
                        }
                    }
                }

                if (isset($trama['referencia_id'])) {
                    $ref = $this->referenciaManager->find($trama['referencia_id']);
                    $zona = self::referencias;
                    // cargamos el array con tipo de referencia o yacimiento
                    if (!isset($infracciones[$zona])) {
                        $infracciones[$zona] = array(
                            'leve' => 0,
                            'medio' => 0,
                            'alto' => 0,
                            'IR' => 0,
                        );
                    }
                    $data['lugar'] = $ref->getNombre();
                    // se toma la minima entre la ref y la del formulario
                    if (intval($ref->getVelocidadMaxima()) == 0) {     //la ref no tiene velocidad max
                        $vmax = intval($consulta['max_referencia']);
                    } elseif (intval($consulta['max_referencia']) == 0) {   //la consulta viene sin vel max
                        $vmax = intval($ref->getVelocidadMaxima());
                    } else {     //se toma la minima entre la vel max de la ref y la consulta.
                        $vmax = min(intval($ref->getVelocidadMaxima()), intval($consulta['max_referencia']));
                    }
                } else {
                    $zona = self::otros;
                    $vmax = intval($consulta['max_resto']);
                }


                if ($data['velocidad'] > $vmax * 1.16) {
                    $riesgo = "alto";
                } else if ($data['velocidad'] > $vmax * 1.08) {
                    $riesgo = "medio";
                } else if ($data['velocidad'] > $vmax * 1.01) {
                    $riesgo = "leve";
                } else {
                    $riesgo = false;
                }

                if ($riesgo) {
                    $data['riesgo'] = $riesgo;
                    $data['velocidad_maxima'] = $vmax;

                    $informe[$zona][] = $data;
                    $infracciones[$zona][$riesgo]++;
                    $total++;
                }
                unset($data);
            }
            unset($trama);
        }


        $infrac = $this->calcularIR($infracciones);
        ksort($infrac['infracciones']);

        return array(
            'infracciones' => $infrac['infracciones'],
            'IRTOTAL' => $infrac['IRTOTAL'],
            'freno' => $freno,
            'aceleracion' => $aceleracion,
            'giro' => $giro,
            'total' => $total,
            'informe' => $informe,
        );
    }

    public function calcularIR($infracciones)
    {
        $total = 0;
        foreach ($infracciones as $key => $zona) {
            $IR = $zona['leve'] + ($zona['medio'] * self::IR_MEDIO) + ($zona['alto'] * self::IR_ALTO);
            $zona['IR'] = array('IR' => round($IR), 'color' => $this->colorIR($IR));
            $infracciones[$key] = $zona;
            $total += $IR;
        }
        $IRTOTAL = array('IRTOTAL' => round($total), 'color' => $this->colorIR($total));
        return array('infracciones' => $infracciones, 'IRTOTAL' => $IRTOTAL);
    }

    public function colorIR($IR)
    {
        $colores = array("leve" => "#70CD00", "medio" => "#DAB001", "alto" => "#B80000");

        if ($IR < self::MIN_IR_MEDIO) {
            $color = $colores['leve'];
        } elseif ($IR > self::MIN_IR_MEDIO && $IR < self::MIN_IR_ALTO) {
            $color = $colores['medio'];
        } else {
            $color = $colores['alto'];
        }
        return $color;
    }

    public function getReferencias($consulta)
    {
        if ($consulta['referencia'] == '0') {  //no considerar referencias
            $referencias = array();
        } elseif ($consulta['referencia'] == '-1') { //considerar todas las referencias
            $referencias = $this->referenciaManager->findAsociadas();
        } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
            $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
        } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
            $referencias = array();
        } else {   //es un id de referencia.
            $referencias[] = $this->referenciaManager->find(
                $consulta['referencia']
            );
        }
        return $referencias;
    }

    public function getDataTotales($servicio, $periodo, $referencias)
    {
        $informe = $this->backendManager->informeDistancias(
            $servicio->getId(),
            $periodo['desde'],
            $periodo['hasta'],   
            $referencias         
        );
        //dd($informe);
        $data = array(
            'km_total' => round(intval($informe['km_total'])),
            'motor_encendido' => $this->utilsManager->segundos2tiempo($informe['segundos_movimiento']),
            'motor_detenido' => $this->utilsManager->segundos2tiempo(
                $informe['segundos_detenido']
            )
        );
        return $data;
    }

    /**
     * @Route("/informe/responsabilidad/{tipo}/exportar", name="informe_responsabilidad_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_REFERENCIA")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null, $encabezado = null)
    {
        $i = 6;
        $ord = ord(self::COMIENZO);
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $encabezado = json_decode($request->get('encabezado'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Responsabilidad Vial");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
                'C5' => 'Referencia',
                'D5' => $consulta['referencianombre'],
            ));

            //esto es el encabezado
            $i++;
            $xls->setBar($i, array('A' => array('title' => 'Servicio', 'width' => 25)));
            foreach ($encabezado as $value) {
                $xls->setBar($i, array(
                    chr($ord) => array('title' => $value . ' leve', 'width' => 10),
                    chr($ord + 1) => array('title' => $value . ' medio', 'width' => 10),
                    chr($ord + 2) => array('title' => $value . ' alto', 'width' => 10),
                    chr($ord + 3) => array('title' => 'I.R.', 'width' => 10),
                ));
                $ord = $ord + 4;
            }
            foreach ($this->titulos as $titulo) {
                $xls->setBar($i, array(
                    chr($ord) => array('title' => $titulo, 'width' => 10)
                ));
                $ord++;
            }

            //este es el cuerpo del resumen
            $i++;
            $ord = ord(self::COMIENZO);
            foreach ($informe as $arr) {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $arr['servicio']),
                ));
                foreach ($encabezado as $j => $data) {
                    $value = $arr['infracciones'][$j];
                    if (!is_null($value)) {
                        if (isset($value['leve']) || isset($value['medio']) || isset($value['alto'])) {
                            $xls->setRowValues($i, array(
                                chr($ord) => array('value' => $value['leve']),
                                chr($ord + 1) => array('value' => $value['medio']),
                                chr($ord + 2) => array('value' => $value['alto']),
                                chr($ord + 3) => array('value' => $value['IR']['IR']),
                            ));
                        }
                    }
                    $ord = $ord + 4;
                }
                $i++;
                $ord = ord(self::COMIENZO);

                //detalle de infracciones
                if (isset($arr['detalle'])) {
                    //esto es para el detalle de las infracciones
                    $i++;
                    //esto es para el resumen totalizado de infracciones.
                    $xls->setBar($i, array(
                        'A' => array('title' => 'Servicio', 'width' => 20),
                        'B' => array('title' => 'Fecha', 'width' => 20),
                        'C' => array('title' => 'Chofer', 'width' => 20),
                        'D' => array('title' => 'Lugar / Referencia', 'width' => 20),
                        'E' => array('title' => 'Riesgo', 'width' => 20),
                        'F' => array('title' => 'Veloc. Max.', 'width' => 20),
                        'G' => array('title' => 'Velocidad', 'width' => 10),
                    ));
                    $i++;
                    foreach ($arr['detalle'] as $detalle) {   //recorro todo el detalle para un servicio
                        foreach ($detalle as $value) {   //recorro todo el detalle para un servicio
                            $fecha = new \DateTime($value['fecha']);
                            $fecha->setTimezone(new \DateTimeZone($this->userloginManager->getTimeZone()));

                            $xls->setCellValue('A' . $i, $arr['servicio']);
                            $xls->setCellValue('B' . $i, $fecha->format('d/m/Y H:i:s'));
                            $xls->setCellValue('C' . $i, isset($value['chofer']) ? $value['chofer'] : '---');
                            $xls->setCellValue('D' . $i, $value['lugar']);
                            $xls->setCellValue('E' . $i, $value['riesgo']);
                            $xls->setCellValue('F' . $i, $value['velocidad_maxima']);
                            $xls->setCellValue('G' . $i, $value['velocidad']);
                            $i++;
                        }
                    }
                }
            }


            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=responsabilidadvial.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_responsabilidad'));
        }
    }
}
