<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use GMaps\Geocoder;
use App\Form\informe\InformeExcesosVelocidadOldType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\GeocoderManager;

class InformeExcesosVelocidadController extends AbstractController
{

    protected $userloginManager;
    protected $servicioManager;
    protected $loggerManager;
    protected $utilsManager;
    protected $breadcrumbManager;
    protected $informeUtilsManager;
    protected $backendManager;
    protected $grupoServicioManager;
    protected $excelManager;
    protected $referenciaManager;
    protected $gmapsManager;
    protected $geocoderManager;
    private $referenciaId = array();

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ExcelManager $excelManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->excelManager = $excelManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/informe/excesosvelocidad", name="informe_excesosvelocidad")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_EXCESO_VELOCIDAD")
     */
    public function excesosvelocidadAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Exceso Velocidad");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $form = $this->createForm(InformeExcesosVelocidadOldType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeexcesosvelocidad');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_excesosvelocidad'));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //armo los períodos que debo tener en cuenta.
                    $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //obtengo las referencias que necesito para todo el informe
                    if ($consulta['referencia'] == '0') {  //se pide por referencia
                        $referencias = array();
                    } elseif ($consulta['referencia'] == '-1') {
                        $referencias = $this->referenciaManager->findAllVisibles();
                    } else {
                        $referencias = $this->referenciaManager->findAllByGrupo($consulta['referencia']);
                    }

                    //armo el array con los nombres de las [id, ref.nombre] para tenerlos en memoria luego y no tener que hacer dobre consulta.
                    foreach ($referencias as $referencia) {
                        $this->referenciaId[$referencia->getId()] = $referencia->getNombre();
                    }

                    //aca se obtiene el informe.
                    $informe = array();
                    foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                        if ($servicio->getEquipo()) {
                            $informe[] = $this->procesarServicio($servicio, $periodo, $consulta, $referencias);
                        }
                    }
                }

                $this->loggerManager->logInforme($consulta);

                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/ExcesosVelocidad/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;

                    case '2': //sale a grafico la cantidad.
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) === 'g') {
                            return $this->render(
                                'informe/ExcesosVelocidad/grafico_flota.html.twig',
                                array(
                                    'consulta' => $consulta,
                                    'dt' => $this->getGraficoInforme($consulta, $informe)
                                )
                            );
                        } else {
                            //obtengo el servicio
                            $consulta['servicionombre'] = $this->servicioManager->find(intval($consulta['servicio']))->getnombre();
                            return $this->render(
                                'informe/ExcesosVelocidad/grafico.html.twig',
                                array(
                                    'consulta' => $consulta,
                                    'dt' => $this->getGraficoInforme($consulta, $informe)
                                )
                            );
                        }
                        break;
                    case '3':
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) === 'g') {
                            //esto es un error, porque por mapa no se puede mostrar toda la flota
                            //entonces lo mando a la pantalla por gil....
                            $this->get('session')->getFlashBag()->add('error', 'No se puede mostrar toda la flota en el mapa.');
                            return $this->render('informe/ExcesosVelocidad/pantalla_flota.html.twig', array(
                                'consulta' => $consulta,
                                'informe' => $informe,
                            ));
                        } else {
                            $consulta['servicionombre'] = $this->servicioManager->find(intval($consulta['servicio']))->getnombre();
                            return $this->render_excesovelocidad_mapa($consulta, $informe[0]);
                        }
                        break;
                    case '5': //sale a excel
                        //       die('////<pre>'.nl2br(var_export('$informe', true)).'</pre>////');
                        //dd($consulta);
                        return $this->exportarAction($request, 1, $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/ExcesosVelocidad/excesosvelocidad.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOEXCESOS'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta, $referencias)
    {
        $data = array(
            'servicio' => $servicio->getNombre(),
            'data' => null,
            'count' => 0
        );

        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);

        if ($servicio->getUltFechahora() >= $desde) {
            $info = $this->getDataInforme($servicio, $periodo, $consulta, $referencias);
            $data['data'] = $info;
            $data['count'] = count($info);
        }
        return $data;
    }

    public function getDataInforme($servicio, $periodo, $consulta, $referencias)
    {
        $informe = array();
        foreach ($periodo as $key => $value) {
            $result = $this->backendManager->informeExcesosVelocidad(
                $servicio->getId(),
                $value['desde'],
                $value['hasta'],
                intval($consulta['velocidad_maxima']),
                $referencias
            );
            if ($result) {
                $informe = array_merge($informe, $result);
            }
        }

        foreach ($informe as $key => $value) {
            //$informe[$key]['fecha'] = $this->utilsManager->fecha_a_local($informe[$key]['fecha']);
            $informe[$key]['duracion'] = $this->utilsManager->segundos2tiempo($value['duracion']);
            if ($consulta['mostrar_direcciones']) {
                $informe[$key]['domicilio'] = '---';
                $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                $informe[$key]['domicilio'] = $direc;
            }
            //referencia
            if (isset($informe[$key]['referencia_id']) && !is_null($informe[$key]['referencia_id'])) {
                $informe[$key]['referencia'] = [
                    'id' => $informe[$key]['referencia_id'], 
                    'nombre' => $this->referenciaId[$value['referencia_id']]
                ];
            }
        }
       // dd($informe);
        return $informe;
    }

    /**
     * @Route("/informe/excesosvelocidad/{tipo}/exportar", name="informe_excesosvelocidad_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_EXCESO_VELOCIDAD")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Excesos de Velocidades");
            //dd($consulta);
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 20),
                'C' => array('title' => 'Máx. Permitida (km/h)', 'width' => 20),
                'D' => array('title' => 'Registrada (km/h)', 'width' => 20),
                'E' => array('title' => 'Duración', 'width' => 20),
                'F' => array('title' => 'Referencia', 'width' => 100),
                'G' => array('title' => 'Domicilio', 'width' => 100),
            ));

            $xls->setCellValue('D2', $consulta['servicionombre']);
            $totales = json_decode($request->get('totales'), true);

            $i = 7;
            foreach ($informe as $datos) {   //esto es para cada servicio
                //die('////<pre>'.nl2br(var_export($datos['data'], true)).'</pre>////');
                foreach ($datos['data'] as $key => $value) {
                    $date = explode(" ", $value['fecha']);
                    $xls->setCellValue('A' . $i, $datos['servicio']);
                    $xls->setRowValues($i, array(
                        'B' => array('value' => (($value['fecha']))),
                        'C' => array('value' => intval($value['velocidad_maxima'])),
                        'D' => array('value' => intval($value['velocidad_registrada'])),
                        'E' => array('value' => $value['duracion']),
                    ));
                    if (isset($value['referencia'])) {
                        $xls->setRowValues($i, array(
                            'F' => array('value' => isset($value['referencia']['nombre']) ? $value['referencia']['nombre'] : ''),
                        ));
                    }
                    if (isset($value['domicilio'])) {
                        $xls->setRowValues($i, array(
                            'G' => array('value' => isset($value['domicilio']) ? $value['domicilio'] : ''),
                        ));
                    }
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=velocidades.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_excesosvelocidad'));
        }
    }

    private function render_excesovelocidad_mapa($consulta, $informe)
    {
        //obtengo el historial de equipos segun la fecha de consutla.
        $mapa = $this->getMapaInforme($consulta, $informe, $this->referenciaManager, $this->utilsManager, $this->backendManager, $this->gmapsManager);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        //configuro los puntos donde se produjeron los excesos de velocidad.
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $id = 0;
        if (count($informe) > 0) {
            //die('////<pre>'.nl2br(var_export($informe, true)).'</pre>////');
            foreach ($informe['data'] as $id => $exceso) {
                $txt = $exceso['velocidad_registrada'] . ' km/h';
                $exceso['direccion'] = '0';
                $exceso['velocidad'] = $exceso['velocidad_registrada'];
                $exceso['duracion'] = $this->utilsManager->segundos2tiempo(intval($exceso['duracion']));
                //paso a fecha/hora del usuario.

                $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $exceso, $icon);
                $mark->setTitle($txt);

                if ($min_lat === false) {
                    $min_lat = $max_lat = $exceso['latitud'];
                    $min_lng = $max_lng = $exceso['longitud'];
                } else {
                    $min_lat = min($min_lat, $exceso['latitud']);
                    $max_lat = max($max_lat, $exceso['latitud']);
                    $min_lng = min($min_lng, $exceso['longitud']);
                    $max_lng = min($max_lng, $exceso['longitud']);
                }
            }

            $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
        }
        return $this->render('informe/ExcesosVelocidad/mapa.html.twig', array(
            'consulta' => $consulta,
            'informe' => $informe,
            'mapa' => $mapa,
        ));
    }

    private function getMapaInforme($consulta, $informe)
    {
        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
        $periodo = (array)$this->utilsManager->periodo2array($desde, $hasta);

        $mapa = $this->gmapsManager->createMap(true);

        //meto las referencias en el mapa.
        $referencias = $this->referenciaManager->findAsociadas();
        foreach ($referencias as $referencia) {
            //agrego las referencias
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, true);
        }

        $puntos_polilinea = array();
        foreach ($periodo as $value) {
            $consulta_historial = $this->backendManager->historial(intval($consulta['servicio']), $value['desde'], $value['hasta']);
            reset($consulta_historial);
            while ($trama = current($consulta_historial)) {
                next($consulta_historial);
                if (isset($trama['posicion'])) {
                    $puntos_polilinea[] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                }
            }
        }
        $this->gmapsManager->addPolyline($mapa, 'recorrido', $puntos_polilinea);

        return $mapa;
    }



    public function getGraficoInforme($consulta, $informe)
    {
        $myArray = array();

        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) === 'g') {  //es para un grupo
            foreach ($informe as $key => $value) {
                $myArray[$key]['servicio'] = $value['servicio'];
                $myArray[$key]['excesos'] = $value['count'];
            }
            return $myArray;
        } else {   //esto es para un solo servicio
            //es el tmp agrupado por día
            $tmp = array();
            foreach ($informe[0]['data'] as $value) {
                $i = isset($tmp[substr($value['fecha'], 0, 10)]) ? $tmp[substr($value['fecha'], 0, 10)] : 0;
                $tmp[substr($value['fecha'], 0, 10)] = $i + 1;
            }
            foreach ($tmp as $key => $value) {
                $myArray[$key]['fecha'] = $key;
                $myArray[$key]['excesos'] = $value;
            }
            unset($tmp);

            return $myArray;
        }
    }
}
