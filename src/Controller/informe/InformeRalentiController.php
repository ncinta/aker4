<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeRalentiType;
//libreria de graficos.
use SaadTazi\GChartBundle\DataTable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GeocoderManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;

class InformeRalentiController extends AbstractController
{

    private $servicioManager;
    private $geocoderManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $excelManager;
    private $userlogin;
    private $gmapsManager;

    function __construct(
        ServicioManager $servicioManager,
        GeocoderManager $geocoderManager,
        GmapsManager $gmapsManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        UserLoginManager $userlogin,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager,
        ExcelManager $excelManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->geocoderManager = $geocoderManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->excelManager = $excelManager;
        $this->userlogin = $userlogin;
        $this->gmapsManager = $gmapsManager;
    }

    /**
     * @Route("/informe/ralenti", name="informe_ralenti")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_RALENTI")
     */
    public function ralentiAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de ralentí");

        //traigo todo los servicios existentes.
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(InformeRalentiType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeralenti');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);

            //paso la fecha a formato yyyy-mm-dd
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->ralentiAction($request);
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //armo los períodos que debo tener en cuenta.
                $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                //obtengo los servicios a incluir en el informe.
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];

                //aca se obtiene el informe.
                $informe = array();
                foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                    if ($servicio->getEquipo()) {
                        $informe[] = $this->procesarServicio($servicio, $periodo, $consulta);
                    }
                }
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Ralenti/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));

                        break;
                    case '2': //sale a grafico.
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                            $template = 'informe/Ralenti/grafico_flota.html.twig';
                        } else {
                            $template = 'informe/Ralenti/grafico.html.twig';
                        }
                        return $this->render(
                            $template,
                            array(
                                'consulta' => $consulta,
                                'dt' => $this->getGraficoInforme($consulta, $informe),
                                'time' => $this->loggerManager->getTimeGeneracion(),
                            )
                        );
                        break;
                    case '3':  //sale al mapa
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                            //esto es un error, porque por mapa no se puede mostrar toda la flota
                            //entonces lo mando a la pantalla por gil....
                            $this->get('session')->getFlashBag()->add('error', 'No se puede mostrar toda la flota en el mapa.');
                            return $this->render('informe/Ralenti/pantalla_flota.html.twig', array(
                                'consulta' => $consulta,
                                'informe' => $informe,
                            ));
                        } else {
                            return $this->render_detenciones_mapa($consulta, $informe);
                        }
                        break;
                }
            }
        }
        return $this->render('informe/Ralenti/ralenti.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta)
    {
        $info = $this->getDataInforme($servicio, $periodo, $consulta['tiempo_minimo'] * 60, $consulta['mostrar_direcciones']);
        return array(
            'servicio' => $servicio->getNombre(),
            'count' => count($info),
            'data' => $info,
            'totales' => $this->getTotalesInforme($info), //los totales...
        );
    }

    /**
     * @Route("/informe/ralenti/{tipo}/exportar", name="informe_ralenti_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_RALENTI")
     */
    public function exportarAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Detenciones en Ralenti");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //es para un solo servicio.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Fecha Inicio', 'width' => 20),
                    'B' => array('title' => 'Fecha Fin', 'width' => 20),
                    'C' => array('title' => 'Duración', 'width' => 20),
                    'D' => array('title' => 'Referencia', 'width' => 100),
                    'E' => array('title' => 'Direccion', 'width' => 100),
                ));
                $xls->setCellValue('D2', $consulta['servicionombre']);

                $totales = json_decode($request->get('totales'), true);
                $i = 7;
                foreach ($informe as $key => $value) {
                    $fecha = new \DateTime($value['tiempo_ralenti']);
                    $fecha->setTimezone(new \DateTimeZone($this->userlogin->getTimeZone()));
                    //$xls->setCellValue('C' . $i, $fecha->format('d/m/Y H:i:s'));

                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['fecha_inicio']),
                        'B' => array('value' => $value['fecha_fin']),
                        'C' => array('value' => $fecha->format('H:i:s')),
                    ));
                    if (isset($value['referencia_nombre'])) {
                        $xls->setRowValues($i, array(
                            'D' => array('value' => isset($value['referencia_nombre']) ? $value['referencia_nombre'] : ''),
                        ));
                    }
                    if (isset($value['domicilio'])) {
                        $xls->setRowValues($i, array(
                            'E' => array('value' => isset($value['domicilio']) ? $value['domicilio'] : ''),
                        ));
                    }
                    $i++;
                }
            } else {   //es para la flota.
                //                 die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Fecha Inicio', 'width' => 20),
                    'C' => array('title' => 'Fecha Fin', 'width' => 20),
                    'D' => array('title' => 'Duración', 'width' => 20),
                    'E' => array('title' => 'Referencia', 'width' => 30),
                    'F' => array('title' => 'Direccion', 'width' => 100),
                ));
                $i = 7;
                foreach ($informe as $datos) {   //esto es para cada servicio
                    $xls->setCellValue('A' . $i, $datos['servicio']);
                    foreach ($datos['data'] as $key => $value) {
                        $xls->setRowValues($i, array(
                            'B' => array('value' => $value['fecha_inicio']),
                            'C' => array('value' => $value['fecha_fin']),
                            'D' => array('value' => $value['tiempo_ralenti']),
                        ));
                        if (isset($value['referencia_nombre'])) {
                            $xls->setRowValues($i, array(
                                'E' => array('value' => isset($value['referencia_nombre']) ? $value['referencia_nombre'] : ''),
                            ));
                        }
                        if (isset($value['domicilio'])) {
                            $xls->setRowValues($i, array(
                                'F' => array('value' => isset($value['domicilio']) ? $value['domicilio'] : ''),
                            ));
                        }
                        $i++;
                    }
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=ralenti.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_ralenti'));
        }
    }

    public function getGraficoInforme($consulta, $informe)
    {
        $myArray = array();
        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
            foreach ($informe as $key => $value) {
                $myArray[$key]['servicio'] = $value['servicio'];
                $myArray[$key]['detenciones'] = $value['count'];
            }
            return $myArray;
        } else {   //esto es para un solo servicio
            foreach ($informe as $key => $value) {
                $myArray[$key]['fecha'] = substr($value['desde'], 8, 2) . '-' . substr($value['desde'], 5, 2);
                $myArray[$key]['detenciones'] = $value['count'];
            }
            return $myArray;
        }
    }

    private function render_detenciones_mapa($consulta, $informe)
    {
        $mapa = $this->getMapaInforme($consulta, $informe);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        foreach ($informe as $id => $detencion) {
            if ($detencion['count'] > 0) {
                foreach ($detencion['data'] as $data) {
                    $data['direccion'] = '0';
                    $data['velocidad'] = '0';
                    $data['tiempo_ralenti'] = isset($data['duracion']) ? $this->utilsManager->segundos2tiempo(intval($data['duracion'])) : 0;
                    $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $data, $icon);
                    $mark->setTitle($data['detencion_id']);

                    if ($data['referencia_id'] != null) {
                        $this->gmapsManager->addMarkerReferencia($mapa, $this->referenciaManager->find($data['referencia_id']), true);
                    }

                    if ($min_lat === false) {
                        $min_lat = $max_lat = $data['latitud'];
                        $min_lng = $max_lng = $data['longitud'];
                    } else {
                        $min_lat = min($min_lat, $data['latitud']);
                        $max_lat = max($max_lat, $data['latitud']);
                        $min_lng = min($min_lng, $data['longitud']);
                        $max_lng = min($max_lng, $data['longitud']);
                    }
                    $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
                }
                return $this->render('informe/Ralenti/mapa.html.twig', array(
                    'consulta' => $consulta,
                    'informe' => $informe,
                    'mapa' => $mapa,
                ));
            } else {
                $this->get('session')->getFlashBag()->add('error', 'No hay Detenciones');
                return $this->redirect($this->generateUrl('informe_ralenti'));
            }
        }
    }

    private function getMapaInforme($consulta, $informe)
    {
        $mapa = $this->gmapsManager->createMap();
        return $mapa;
    }

    private function getDataInforme($servicio, $periodo, $tiempo_minimo, $obtener_direccion)
    {

        $referencias = $this->referenciaManager->findAsociadas();
        $informe = array();
        foreach ($periodo as $key => $value) {
            $data = $this->backendManager->informeRalenti($servicio->getId(), $value['desde'], $value['hasta'], $tiempo_minimo, $referencias);
            if ($data) {
                $informe = array_merge($informe, $data);
            }
            unset($data);
        }
        $id = 0;
        foreach ($informe as $key => $value) {
            $informe[$key]['detencion_id'] = $key + 1;
            $informe[$key]['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo(intval($value['duracion']));
            //proceso si es en una referencia.
            if ($value['referencia_id'] != null) {
                $referencia = $this->referenciaManager->find($value['referencia_id']);
                if ($referencia) {
                    $informe[$key]['referencia_nombre'] = $referencia->getNombre();
                }
            }
            //obtengo la direccion siempre que se solicite.
            if ($obtener_direccion) {
                $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                $informe[$key]['domicilio'] = $direc;
            }
        }
        return $informe;
    }

    public function getTotalesInforme($informe)
    {
        $total = array(
            'tiempo_ralenti' => 0,
            'duracion' => 0,
        );
        foreach ($informe as $value) {
            $total['duracion'] += $value['duracion'];
        }
        $total['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo(intval($total['duracion']));
        return $total;
    }
}
