<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InfraccionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\ChoferManager;
use App\Model\app\GeocoderManager;

/**
 *
 * @author nicolas
 */
class InformeInfraccionController extends AbstractController
{

    const MOTIVOTX_ACELERACION = 23;     //23
    const MOTIVOTX_FRENADA = 24;        //24
    const MOTIVOTX_GIRO = 135;           //135

    private $arrMotivos = array(self::MOTIVOTX_ACELERACION, self::MOTIVOTX_FRENADA, self::MOTIVOTX_GIRO);
    private $user;
    private $moduloChofer;
    private $userloginManager;
    private $servicioManager;
    private $geocoderManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $excelManager;
    private $choferManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        GeocoderManager $geocoderManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ExcelManager $excelManager,
        ChoferManager $choferManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->geocoderManager = $geocoderManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->choferManager = $choferManager;
    }

    /**
     * @Route("/informe/{id}/infraccion", name="informe_infraccion",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_INFRACCION")
     */
    public function infraccionAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Infracciones");
        $this->user = $this->getUser2Organizacion($id);
        //traigo todo los servicios existentes para el usuario
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($this->user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($this->user->getOrganizacion()),
        );
        $form = $this->createForm(InfraccionType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->user = $this->getUser2Organizacion($id);
            $this->moduloChofer = $this->organizacionManager->isModuloEnabled($this->user->getOrganizacion(), 'CLIENTE_CHOFER');

            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('infraccion');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_infraccion', array('id' => $id)));
                }
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];
                $informe['detalles'] = $this->armarPorTrama($arrServicios['servicios'], $desde, $hasta);
                $totales = $this->armarTotales($informe['detalles'], $consulta['servicionombre']);
                $informe['totales'] = $totales['totales'];
                $informe['total'] = $totales['total'];
                $this->loggerManager->logInforme($consulta);
            }

            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    return $this->render('informe/Infraccion/pantalla.html.twig', array(
                        'consulta' => $consulta,
                        'result' => $informe,
                        'total_total' => array(),
                        'time' => $this->loggerManager->getTimeGeneracion(),
                        'moduloChofer' => $this->moduloChofer,
                    ));
                    break;
                case '5':
                    //cambiar el return cuando se implemente salida por excel
                    return $this->exportarAction($request, 0, $consulta, $informe, $this->moduloChofer);
                    break;
            }
        }
        return $this->render('informe/Infraccion/infraccion.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function armarTotales($informe, $servicioNombre)
    {
        $arr_totales = $total = array();
        $aceleracion_total = 0;
        $freno_total = 0;
        $giro_total = 0;
        foreach ($informe as $clave => $valor) {
            $aceleracion = 0;
            $freno = 0;
            $giro = 0;
            foreach ($valor as $data) {
                if ($data['motivo'] == self::MOTIVOTX_ACELERACION) {  // este motivo hay que modificar 23
                    $aceleracion++;
                } elseif ($data['motivo'] == self::MOTIVOTX_FRENADA) { // este motivo hay que modificar 24
                    $freno++;
                } elseif ($data['motivo'] == self::MOTIVOTX_GIRO) { // este motivo hay que modificar 135
                    $giro++;
                }
            }

            $aceleracion_total += $aceleracion;
            $freno_total += $freno;
            $giro_total += $giro;

            $arr_totales[$clave] = array(
                'aceleracion' => $aceleracion,
                'freno' => $freno,
                'giro' => $giro
            );
        }

        if (substr($servicioNombre, 0, 1) === 'g' || substr($servicioNombre, 0, 1) === 't') {
            $total = array(
                'servicio' => $servicioNombre,
                'aceleracion' => $aceleracion_total,
                'freno' => $freno_total,
                'giro' => $giro_total
            );
        }
        return array('totales' => $arr_totales, 'total' => $total);
    }

    public function armarPorTrama($servicios, $desde, $hasta)
    {
        $infracciones = 0;
        $return = array();
        $motivo = ' --- ';
        $referencias = $this->referenciaManager->findAsociadas($this->user->getOrganizacion());
        foreach ($servicios as $servicio) {
            if ($servicio->getEquipo()) {
                $result = $this->backendManager->informeEventos($servicio->getId(), $desde, $hasta, $referencias, $this->arrMotivos);
                //  die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
                if ($result != null) {
                    foreach ($result as $value) {
                        $ref = null;
                        $domicilio = null;
                        $chofer = null;

                        //domicilio
                        if (isset($value['posicion'])) {
                            $direc = $this->geocoderManager->inverso($value['posicion']['latitud'], $value['posicion']['longitud']);
                            $domicilio = $direc;
                        }

                        //referencia
                        if (isset($value['referencia_id']) && !is_null($value['referencia_id'])) {
                            $referencia = $this->referenciaManager->find($value['referencia_id']);
                            if (!is_null($referencia)) {
                                $ref = $referencia->getNombre();
                            }
                        }

                        if ($this->moduloChofer) {
                            //chofer
                            if (isset($value['ibutton']) && $value['ibutton'] != 0 && $value['ibutton'] != null) {
                                $chofer = $this->choferManager->findByDallas($value['ibutton']);
                                $chofer = $chofer ? $chofer->getNombre() : 'n/n';
                            }
                        }

                        //motivo
                        if ($value['motivo'] == self::MOTIVOTX_ACELERACION) {
                            $motivo = 'Aceleración';
                        } elseif ($value['motivo'] == self::MOTIVOTX_FRENADA) {
                            $motivo = 'Frenada';
                        } elseif ($value['motivo'] == self::MOTIVOTX_GIRO) {
                            $motivo = 'Giro';
                        } else {
                            $motivo = '';
                        }

                        $return[$servicio->getNombre()][] = array(
                            'fecha' => $value['fecha'],
                            'posicion' => isset($value['posicion']) ? $value['posicion'] : null, //arr con lat, long, vel
                            'motivo' => $value['motivo'],
                            'motivostr' => $motivo,
                            'domicilio' => $domicilio,
                            'referencia' => $ref,
                            'chofer' => $chofer
                        );

                        $infracciones++;
                    }
                }
            }
        }
        //die('////<pre>' . nl2br(var_export($return, true)) . '</pre>////');
        return $return;
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    /**
     * @Route("/informe/infraccion/{tipo}/exportar", name="informe_infraccion_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_INFRACCION")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        $moduloChofer = false;
        if (is_array($request->get('moduloChofer'))) {
            $moduloChofer = $request->get('moduloChofer');
        }

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Infracciones");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            $i = 7;

            //esto es para el resumen totalizado de infracciones.
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Aceleración', 'width' => 10),
                'C' => array('title' => 'Frenada', 'width' => 10),
                'D' => array('title' => 'Giro', 'width' => 10),
            ));
            foreach ($informe['totales'] as $servicio => $tot) {
                $xls->setCellValue('A' . $i, $servicio);
                $xls->setCellValue('B' . $i, $tot['aceleracion']);
                $xls->setCellValue('C' . $i, $tot['freno']);
                $xls->setCellValue('D' . $i, $tot['giro']);

                $i++;
            }

            //esto es para el detalle de las infracciones
            $i++;
            //esto es para el resumen totalizado de infracciones.
            $xls->setBar($i, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 20),
                'C' => array('title' => 'Chofer', 'width' => 20),
                'D' => array('title' => 'Referencia', 'width' => 20),
                'E' => array('title' => 'Acción', 'width' => 20),
                'F' => array('title' => 'Dirección', 'width' => 20),
                'G' => array('title' => 'Velocidad', 'width' => 10),
            ));
            $i++;
            foreach ($informe['detalles'] as $servicio => $detalle) {   //recorro todo el detalle para un servicio
                foreach ($detalle as $value) {   //recorro todo el detalle para un servicio
                    $xls->setCellValue('A' . $i, $servicio);
                    $fecha = new \DateTime($value['fecha']);
                    $fecha->setTimezone(new \DateTimeZone($this->userloginManager->getTimeZone()));
                    $xls->setCellValue('C' . $i, $fecha->format('d/m/Y H:i:s'));

                    //$xls->setCellValue('B' . $i, \PHPExcel_Shared_Date::PHPToExcel(strtotime($value['fecha'])), 'dd/mm/yyyy HH:MM:SS');
                    $xls->setCellValue('C' . $i, $value['chofer']);
                    $xls->setCellValue('D' . $i, $value['referencia']);
                    $xls->setCellValue('E' . $i, $value['domicilio']);
                    $xls->setCellValue('F' . $i, $value['motivostr']);
                    $xls->setCellValue('G' . $i, $value['posicion']['velocidad']);
                    $i++;
                }
            }


            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=infracciones.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_infraccion', array('id' => $this->user->getOrganizacion()->getId())));
        }
    }
}
