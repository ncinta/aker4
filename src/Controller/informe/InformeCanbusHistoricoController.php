<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\informe\CanbusHistoricoType;
use App\Entity\Organizacion;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;

class InformeCanbusHistoricoController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $excelManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        ExcelManager $excelManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
    }

    function parse($fecha)
    {
        list($fecha, $hora) = explode(" ", $fecha);
        list($y, $m, $d) = explode("-", $fecha);
        list($h, $n, $s) = explode(":", $hora);
        return mktime($h, $n, $s, $m, $d, $y);
    }

    /**
     * @Route("/informe/{id}/canbus/historico", name="informe_canbus_historico",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CANBUS")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de historico de Canbus");

        // Obtener el usuario actual
        $user = $this->userloginManager->getUser();
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }

        $choiceCan = array();
        foreach ($this->variablesCan as $key => $var) {
            $choiceCan[$var['nombre']] = $key;
        } 
        ksort($choiceCan);
        // Crear formulario        
        $form = $this->createForm(CanbusHistoricoType::class, null, [
            'servicios' => $this->servicioManager->findByCanbus($organizacion),
            'variablesCan' => $choiceCan
        ]);

        return $this->render('informe/CanbusHistorico/index.html.twig', array(
            'form' => $form->createView(),
            'informe' => null,
            'organizacion' => $organizacion,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
            'variables' => $choiceCan
        ));
    }


    /**
     * @Route("/informe/canbus/generar",options={"expose"=true},  name="informe_canbus_generar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CANBUS")
     */
    public function generarAction(Request $request)
    {
        //traigo el usuario master o el userlogin dependiendo de donde este.
        $organizacion = $this->userloginManager->getOrganizacion();

        $tmp = array();
        parse_str($request->get('formCanbus'), $tmp);
        $consulta = $tmp['informecanbushistorico'];        
        if (!$consulta) {
            return new Response(json_encode(array(
                'status' => 'error', 'html' => 'Error',
            )), 404);
        }

        $informe = $this->buildInforme($consulta);
        //  die('////<pre>' . nl2br(var_export($info, true)) . '</pre>////');

        switch ($consulta['destino']) {
            case '1':   //pantalla
                $html =  $this->renderView('informe/CanbusHistorico/pantalla.html.twig', array(
                    'organizacion' => $organizacion,
                    'informe' => $informe,
                ));
                break;

            case '2':   //grafico
                $html =  $this->renderView('informe/CanbusHistorico/graficos.html.twig', array(
                    'organizacion' => $organizacion,
                    'dt' => $informe,
                ));
                // dd($informe);
                break;
        }

        return new Response(json_encode(array(
            'status' => 'ok',
            'destino' => $consulta['destino'],
            'html' => $html,
            'informe' => $informe,
        )), 200);
    }

    private function buildInforme($consulta)
    {
        // Validar fechas
        $periodo = array(
            'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
            'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
        );
        if ($periodo['hasta'] <= $periodo['desde']) {
            $this->addFlash('error', 'Se han ingresado mal las fechas. Verifique por favor.');
            $this->breadcrumbManager->pop();
            return $this->redirectToRoute('informe_canbus_historico', ['id' => $this->userloginManager->getOrganizacion()->getId()]);
        }


        // Procesar consulta     
        $informe = null;
        $servicio = $this->servicioManager->findById($consulta['servicio']);

        if ($servicio->getUltFechahora() < $periodo['desde']) {
            return null; // Evitar consultar equipos que no reportan en el periodo
        }

        switch ($consulta['destino']) {
            case '1':   //pantalla                
                $data = $this->getDataInforme($servicio, $periodo);
                $informe[$servicio->getNombre()] = $data;
                $informe['servicionombre'] = $servicio->getNombre();
                break;
            case '2':   //grafico                
                $data = $this->getDataInforme($servicio, $periodo);               
                foreach (array_values($consulta['variables']) as $var) {
                    $arr[$var] = $this->variablesCan[$var];
                }
              //  dd($arr);                
                $informe = $this->armarDataGrafico($data, $arr);
                // dd($informe);

                break;
        }
        return $informe;
    }

    private $variablesCan  = [
        'velMotorRPM' => ['nombre' => 'RPM de Motor', 'color' => '#CAD459', 'medida' => 'RPM'],
        'tempMotor' => ['nombre' => 'Temperatura de Motor', 'color' => '#47C24A', 'medida' => 'grados'],
        'velInstantanea' => ['nombre' => 'Velocidad', 'color' => '#45D3DE', 'medida' => 'kms/h'],
        'nivelCombustible' => ['nombre' => 'Combustible', 'color' => '#6645DE', 'medida' => '%'],
        'acelerador' => ['nombre' => 'Acelerador', 'color' => '#66AADE', 'medida' => '%'],
        'pedalFreno' => ['nombre' => 'Pedal Freno', 'color' => '#66AADE', 'medida' => '%'],
        'tempAnticongelante' => ['nombre' => 'Temp. Anticongelante', 'color' => '#66AADE', 'medida' => 'grados'],
        'consumoInstantaneo' => ['nombre' => 'Consumo Instantaneo', 'color' => '#66AADE', 'medida' => 'lts'],
        'sensorCombustible' => ['nombre' => 'Sensor Combustible', 'color' => '#66AADE', 'medida' => 'on/off'],
        'contacto' => ['nombre' => 'Contacto', 'color' => '#66AADE', 'medida' => 'on/off'],
        'presionAceite' => ['nombre' => 'Presión Aceite', 'color' => '#66AADE', 'medida' => 'kPa'],
    ];

    private function armarDataGrafico($informe, $variables)
    {       
        $data = [];
        foreach ($variables as $codename => $value) {    //para cada variable pedida            
            $myArray = [];
            $i = 0;
            foreach ($informe as $d) {
               // dd($d[$variable]);
                //$fecha = $this->parseFecha($d['fecha']);
                $dt = new \DateTime($d['fecha']);
                $myArray[] = ['fecha' => $dt->format('Y-m-d H:i:s'), 'valor' => isset($d[$codename]) ? $d[$codename] : null];
                $i++;
            }
           // dd($myArray);
            $data[] = [
                'variable' => $codename,
                'vAxis' => $value['medida'],
                'nombre' => $value['nombre'],
                'color' => $value['color'],
                'data' => $myArray,
            ];
        }
       // dd($data);
        return $data;
    }

    private function parseFecha($fecha)
    {
        $dt = new \DateTime($fecha);
        $arrFecha = [
            'd' => $dt->format('d'),
            'm' => $dt->format('m'),
            'Y' => $dt->format('Y'),
            'H' => $dt->format('H'),
            'i' => $dt->format('i'),
            's' => $dt->format('s'),
            'str' => $dt->format('d/m H:i:s'),
        ];
        return $arrFecha;
    }
    private function getDataInforme($servicio, $periodo)
    {
        $tmpHistorial = $this->backendManager->historial($servicio->getId(), $periodo['desde'], $periodo['hasta'], array(), array());
        $h = [];

        foreach ($tmpHistorial as $trama) {
            // if (isset($trama['canbusData'])) { dd($trama['canbusData']); }
            $data = null;

            if (isset($trama['canbusData'])) {
                $data['fecha'] = $trama['fecha'];
                $data['odometro'] = $trama['canbusData']['odometro'] ?? '---';
                $data['odolitro'] = $trama['canbusData']['odolitro'] ?? '---';
                $data['velMotorRPM'] = $trama['canbusData']['velMotorRPM'] ?? 0;
                $data['velInstantanea'] = $trama['canbusData']['velInstantanea'] ?? 0;
                $data['presionAceite'] = $trama['canbusData']['presionAceite'] ?? 0;
                $data['nivelCombustible'] = intval($this->servicioManager->getPorcentajeTanque($servicio, intval($trama['canbusData']['nivelCombustible'] ?? 0)));
                $data['tempMotor'] = $trama['canbusData']['tempMotor'] ?? 0;
                $data['acelerador'] = $trama['canbusData']['acelerador'] ?? 0;
                $data['pedalFreno'] = $trama['canbusData']['pedalFreno'] ?? 0;
                $data['tempAnticongelante'] = $trama['canbusData']['tempAnticongelante'] ?? 0;
                $data['consumoInstantaneo'] = $trama['canbusData']['consumoInstantaneo'] ?? 0;
                $data['sensorCombustible'] = $trama['canbusData']['sensorCombustible'] ?? 0;
                $data['contacto'] = $trama['canbusData']['contacto'] ?? 0;

                $odolAnt = $trama['canbusData']['odolitro'] ?? 0;
                $odolAcu = 0;
                $data['odolitroAcumulado'] = $odolAcu + ($trama['canbusData']['odolitro'] ?? 0 - $odolAnt);

                $odomAnt = $trama['canbusData']['odometro'] ?? 0;
                $odomAcu = 0;
                $data['odometroAcumulado'] = $odomAcu + ($trama['canbusData']['odometro'] ?? 0 - $odomAnt);

                try {
                    if ($data['odolitroAcumulado'] != 0 && $data['odometroAcumulado'] != 0) {
                        $data['rendimiento'] = ($data['odometroAcumulado'] / $data['odolitroAcumulado']);
                    } else {
                        $data['rendimiento'] = 0;
                    }
                } catch (\Exception $exc) {
                    $data['rendimiento'] = 0;
                }
            }

            if (!is_null($data)) {
                $h[] = $data;
            }
        }
        //  $informe[$servicio->getNombre()] = $h;
        unset($tmpHistorial);


        //$informe['servicionombre'] = $arrServicios['servicionombre'];

        return $h;
    }

    /**
     * @Route("/informe/canbus/historico/exportar", name="informe_canbus_historico_exportar",
     *     methods={"GET", "POST"}
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CANBUS")
     */
    public function exportarAction(Request $request,  $tipo = null, $id = null)
    {
        // die('////<pre>' . nl2br(var_export($request, true)) . '</pre>////');
        if (is_array($request->get('informe'))) {
            $informe = $request->get('informe');
        } else {
            $informe = json_decode($request->get('informe'), true);
        }
        if (is_array($request->get('consulta'))) {
            $consulta = $request->get('consulta');
        } else {
            $consulta = json_decode($request->get('consulta'), true);
        }

        if (isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Histórico de Canbus");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 20),
                'C' => array('title' => 'Tipo', 'width' => 20),
                'D' => array('title' => 'Odometro', 'width' => 20),
                'E' => array('title' => 'Odometro Acum.', 'width' => 20),
                'F' => array('title' => 'Odolitro', 'width' => 20),
                'G' => array('title' => 'Odolitro Acum.', 'width' => 20),
                'H' => array('title' => 'Rendimiento', 'width' => 20),
                'I' => array('title' => 'Rpm', 'width' => 20),
                'J' => array('title' => 'Velocidad', 'width' => 20),
                'K' => array('title' => 'Presion Aceite', 'width' => 20),
                'L' => array('title' => 'Tanque', 'width' => 20),
                'M' => array('title' => 'Temp. Motor', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $nombre => $tramas) {   //esto es para cada referencia
                $xls->setCellValue('A' . $i, $nombre);
                foreach ($tramas as $trama) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $nombre),
                        'B' => array('value' => $trama['fecha']),
                        'C' => array('value' => $trama['tipo']),
                        'D' => array('value' => $trama['odometro'], 'format' => '0.0'),
                        'E' => array('value' => $trama['odometroAcumulado'], 'format' => '0.0'),
                        'F' => array('value' => $trama['odolitro'], 'format' => '0.0'),
                        'G' => array('value' => $trama['odolitroAcumulado'], 'format' => '0.0'),
                        'H' => array('value' => $trama['rendimiento'], 'format' => '0.00'),
                        'I' => array('value' => isset($trama['velMotorRPM']) ? $trama['velMotorRPM'] : ''),
                        'J' => array('value' => isset($trama['velInstantanea']) ? $trama['velInstantanea'] : ''),
                        'K' => array('value' => isset($trama['presionAceite']) ? $trama['presionAceite'] : ''),
                        'L' => array('value' => isset($trama['nivelCombustible']) ? $trama['nivelCombustible'] : ''),
                        'M' => array('value' => isset($trama['tempMotor']) ? $trama['tempMotor'] : ''),
                    ));
                    $i++;
                }
            }

            //aca hago el resumen si lo piden
            if ($tipo == 1) {
                $i++;
                $xls->setSubTitle('A' . $i, 'RESUMEN');

                $i++;
                $xls->setBar($i, array(
                    'A' => array('title' => 'Referencia', 'width' => 40),
                    'B' => array('title' => 'Servicio', 'width' => 20),
                    'C' => array('title' => 'Cantidad', 'width' => 20),
                ));
                $i++;

                foreach ($informe as $referencias) {   //esto es para cada referencia
                    //$xls->setCellValue('A' . $i, $servicio['servicio']);
                    foreach ($referencias['servicios'] as $servicio) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $referencias['referencia']),
                            'B' => array('value' => $servicio['servicio']),
                            'C' => array('value' => count($servicio['pasos'])),
                        ));
                        $i++;
                    }
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=canbus_historial.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_canbus_historico', array('id' => $request->get('id'))));
        }
    }
}
