<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeVelocidadesType;
use App\Entity\Organizacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\GrupoReferenciaManager;

class InformeVelocidadesController extends AbstractController
{

    private $servicioManager;
    private $grupoReferencia;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $excelManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $userloginManager;
    private $organizacionManager;

    function __construct(
        ServicioManager $servicioManager,
        GrupoReferenciaManager $grupoReferencia,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ExcelManager $excelManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->grupoReferencia = $grupoReferencia;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->userloginManager = $userloginManager;
        $this->organizacionManager = $organizacionManager;
    }

    /**
     * @Route("/informe/{id}/velocidades", name="informe_velocidades",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_VELOCIDADES")
     */
    public function velocidadesAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Tiempos y Velocidades");

        $user = $this->getUser2Organizacion($organizacion->getId());
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
        );

        $form = $this->createForm(InformeVelocidadesType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informevelocidades');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                $mostrartotal = isset($consulta['mostrar_totales']) ? $consulta['mostrar_totales'] : false;
                $desglosar = isset($consulta['mostrar_desglosado']) ? $consulta['mostrar_desglosado'] : false;

                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //armo los períodos que debo tener en cuenta.
                    $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], $desglosar);

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $user);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //inicio procesamiento del informe
                    $informe = array();
                    foreach ($arrServicios['servicios'] as $servicio) {
                        if ($servicio->getEquipo()) {
                            $informe[] = $this->procesarServicio($servicio, $periodo, $consulta, $mostrartotal);
                        }
                    }
                }

                $this->loggerManager->logInforme($consulta);
                //aca se redirige al destino correspondiente.
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Velocidades/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'organizacion' => $user->getOrganizacion(),
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '2':
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) === 'g') {
                            return $this->render(
                                'informe/Velocidades/grafico_flota.html.twig',
                                array(
                                    'consulta' => $consulta,
                                    'organizacion' => $user->getOrganizacion(),
                                    'time' => $this->loggerManager->getTimeGeneracion(),
                                    'dt' => $this->getGraficoInforme($consulta, $informe)
                                )
                            );
                        } else {
                            //obtengo el servicio
                            $consulta['servicionombre'] = $this->servicioManager->find(intval($consulta['servicio']))->getnombre();
                            return $this->render(
                                'informe/Velocidades/grafico.html.twig',
                                array(
                                    'consulta' => $consulta,
                                    'organizacion' => $user->getOrganizacion(),
                                    'time' => $this->loggerManager->getTimeGeneracion(),
                                    'dt' => $this->getGraficoInforme($consulta, $informe)
                                )
                            );
                        }
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 1,  $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/Velocidades/velocidades.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $user->getOrganizacion(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOTIEMPOS'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta, $mostrartotal)
    {
        $info = $this->getDataInforme($servicio, $periodo, $consulta);
        return array(
            'servicio' => $servicio->getNombre(),
            'data' => $info,
            'totales' => $this->getTotalesInforme($info), //los totales...
        );
    }

    /**
     * @Route("/informe/velocidades/{tipo}/exportar", name="velocidades_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_VELOCIDADES")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {

            //creo el xls
            $xls = $this->excelManager->create("Informe de Tiempos y Velocidades");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));


            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Primer Reporte', 'width' => 20),
                'C' => array('title' => 'Último Reporte', 'width' => 20),
                'D' => array('title' => 'Tiempo activo (horas)', 'width' => 20),
                'E' => array('title' => 'Tiempo detenido (horas)', 'width' => 25),
                'F' => array('title' => 'Tiempo en movimineto (horas)', 'width' => 20),
                'G' => array('title' => 'Tiempo motor encendido (horas)', 'width' => 20),
                'H' => array('title' => 'Velocidad promedio', 'width' => 15),
                'I' => array('title' => 'Velocidad máxima', 'width' => 15),
                'J' => array('title' => 'Horóm. Inicial', 'width' => 15),
                'K' => array('title' => 'Horóm. Final', 'width' => 15),
                'L' => array('title' => 'Diferencia Horómetro', 'width' => 15),
            ));
            $i = 7;
            foreach ($informe as $datos) {   //esto es para cada servicio
                foreach ($datos['data'] as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => $value['desde']),
                        'C' => array('value' => $value['hasta']),
                        'D' => array('value' => $value['tiempo_activo'], 'format' => '[H]:MM:SS'),
                        'E' => array('value' => $value['tiempo_detenido'], 'format' => '[H]:MM:SS'),
                        'F' => array('value' => $value['tiempo_movimiento'], 'format' => '[H]:MM:SS'),
                        'G' => array('value' => $value['tiempo_motor_encendido'], 'format' => '[H]:MM:SS'),
                        'H' => array('value' => intval($value['velocidad_promedio']), 'format' => '0.00'),
                        'I' => array('value' => intval($value['velocidad_maxima']), 'format' => '0.00'),
                        'J' => array('value' => $value['horometro_inicial']),
                        'K' => array('value' => $value['horometro_final']),
                        'L' => array('value' => $value['horometro_diff']),
                    ));
                    $i++;
                }
                //se agregan los totales
                if (isset($consulta['mostrar_totales'])) {
                    $xls->setRowTotales($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => ''),
                        'C' => array('value' => ''),
                        'D' => array('value' => $datos['totales']['tiempo_activo'], 'format' => '[h]:mm:ss'),
                        'E' => array('value' => $datos['totales']['tiempo_detenido'], 'format' => '[h]:mm:ss'),
                        'F' => array('value' => $datos['totales']['tiempo_movimiento'], 'format' => '[h]:mm:ss'),
                        'G' => array('value' => $datos['totales']['tiempo_motor_encendido'], 'format' => '[h]:mm:ss'),
                        'H' => array('value' => intval($datos['totales']['velocidad_promedio']), 'format' => '0.00'),
                        'I' => array('value' => intval($datos['totales']['velocidad_maxima']), 'format' => '0.00'),
                        'J' => array('value' => intval($datos['totales']['velocidad_maxima']), 'format' => '0.00'),
                        'I' => array('value' => intval($datos['totales']['velocidad_maxima']), 'format' => '0.00'),
                        'J' => array('value' => ''),
                        'K' => array('value' => ''),
                        'L' => array('value' => ''),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=velocidades.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_velocidades'));
        }
    }

    public function getDataInforme($servicio, $periodo, $consulta)
    {
        $informe = array();
        foreach ($periodo as $key => $value) {
            $informe[] = $this->backendManager->informeDistancias(
                $servicio->getId(),
                $value['desde'],
                $value['hasta'],
                $this->referenciaManager->findAllVisibles()
            );
        }
        foreach ($informe as $key => $value) {
            $informe[$key]['tiempo_activo'] = $this->utilsManager->segundos2tiempo($informe[$key]['segundos_activo']);
            $informe[$key]['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($informe[$key]['segundos_detenido']);
            $informe[$key]['tiempo_movimiento'] = $this->utilsManager->segundos2tiempo($informe[$key]['segundos_movimiento']);
            $informe[$key]['tiempo_motor_encendido'] = $this->utilsManager->segundos2tiempo($informe[$key]['segundos_motor_encendido']);
            $informe[$key]['horometro_inicial'] = $this->utilsManager->minutos2tiempo($value['horometro_inicial']);
            $informe[$key]['horometro_final'] = $this->utilsManager->minutos2tiempo($value['horometro_final']);
            $informe[$key]['horometro_diff'] = $this->utilsManager->minutos2tiempo(intval($value['horometro_final']) - intval($value['horometro_inicial']));
        }

        return $informe;
    }

    public function getGraficoInforme($consulta, $informe)
    {
        $myArray = array();
        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //esto es para la flota.
            foreach ($informe as $key => $value) {
                $myArray[$key]['servicio'] = $value['servicio'];
                $myArray[$key]['tiempo_activo'] = intval($this->utilsManager->tiempo2horas($value['data'][0]['tiempo_activo']));
                $myArray[$key]['tiempo_detenido'] = intval($this->utilsManager->tiempo2horas($value['data'][0]['tiempo_detenido']));
                $myArray[$key]['tiempo_movimiento'] = intval($this->utilsManager->tiempo2horas($value['data'][0]['tiempo_movimiento']));
                $myArray[$key]['tiempo_motor_encendido'] = intval($this->utilsManager->tiempo2horas($value['data'][0]['tiempo_motor_encendido']));
                $myArray[$key]['velocidad_promedio'] = intval($value['data'][0]['velocidad_promedio']);
                $myArray[$key]['velocidad_maxima'] = intval($value['data'][0]['velocidad_maxima']);
            }
            return $myArray;
        } else {   //para un solo servicio.
            foreach ($informe[0]['data'] as $key => $value) {
                $myArray[$key]['fecha'] = substr($value['desde'], 8, 2) . '-' . substr($value['desde'], 5, 2);
                $myArray[$key]['tiempo_activo'] = intval($this->utilsManager->tiempo2horas($value['tiempo_activo']));
                $myArray[$key]['tiempo_detenido'] = intval($this->utilsManager->tiempo2horas($value['tiempo_detenido']));
                $myArray[$key]['tiempo_movimiento'] = intval($this->utilsManager->tiempo2horas($value['tiempo_movimiento']));
                $myArray[$key]['tiempo_motor_encendido'] = intval($this->utilsManager->tiempo2horas($value['tiempo_motor_encendido']));
                $myArray[$key]['velocidad_promedio'] = intval($value['velocidad_promedio']);
                $myArray[$key]['velocidad_maxima'] = intval($value['velocidad_maxima']);
            }
            return $myArray;
        }
    }

    public function getTotalesInforme($informe)
    {
        $total = array(
            'segundos_activo' => 0,
            'segundos_detenido' => 0,
            'segundos_movimiento' => 0,
            'segundos_motor_encendido' => 0,
            'tiempo_activo' => 0,
            'tiempo_detenido' => 0,
            'tiempo_movimiento' => 0,
            'tiempo_motor_encendido' => 0,
            'velocidad_promedio' => 0,
            'velocidad_maxima' => 0,
        );
        $countVP = $countVM = 0;
        foreach ($informe as $value) {
            if (isset($value['segundos_detenido'])) {
                $total['segundos_activo'] += intval($value['segundos_activo']);
                $total['segundos_detenido'] += intval($value['segundos_detenido']);
                $total['segundos_movimiento'] += intval($value['segundos_movimiento']);
                $total['segundos_motor_encendido'] += intval($value['segundos_motor_encendido']);

                if (intval($value['velocidad_promedio']) != 0) {
                    $total['velocidad_promedio'] += intval($value['velocidad_promedio']);
                    $countVP++;
                }
                if (intval($value['velocidad_maxima']) != 0) {
                    $total['velocidad_maxima'] += intval($value['velocidad_maxima']);
                    $countVM++;
                }
            }
        }
        $total['tiempo_activo'] = intval($this->utilsManager->segundos2tiempo($total['segundos_activo']));
        $total['tiempo_detenido'] = intval($this->utilsManager->segundos2tiempo($total['segundos_detenido']));
        $total['tiempo_movimiento'] = intval($this->utilsManager->segundos2tiempo($total['segundos_movimiento']));
        $total['tiempo_motor_encendido'] = intval($this->utilsManager->segundos2tiempo($total['segundos_motor_encendido']));
        $total['velocidad_promedio'] = intval($total['velocidad_promedio'] / ($countVP == 0 ? 1 : $countVP));  // count($informe);
        $total['velocidad_maxima'] = intval($total['velocidad_maxima'] / ($countVM == 0 ? 1 : $countVM));    //count($informe);
        return $total;
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }
}
