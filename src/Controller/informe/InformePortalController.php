<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\PortalPorEquipoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;

class InformePortalController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $excelManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ExcelManager $excelManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/informe/portal", name="informe_portal")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PORTALES")
     */
    public function portalAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Portal");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $form = $this->createForm(PortalPorEquipoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('portalporequipo');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->portalAction($request);
                } else {
                    //controlo que haya portales asignados.
                    $ref = (array) $this->referenciaManager->findAsociadas();
                    if (count($ref) == 0) {
                        $this->get('session')->getFlashBag()->add('error', 'No tiene asignado referencias. Verifique por favor.');
                        $this->breadcrumbManager->pop();
                        return $this->portalAction($request);
                    }
                    unset($ref);

                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //armo los períodos que debo tener en cuenta.
                    $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //aca se obtiene el informe.
                    $informe = array();
                    $total_flota = 0;
                    foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                        $info = $this->procesarServicio($servicio, $periodo, $consulta);
                        if ($info != null) {
                            $total_flota += $info['total'];    //obtengo el total de la flota
                            $informe[] = $info;
                        }
                    }
                }
            } else {
                $informe = NULL;
                $total_flota = NULL;
            }

            $this->loggerManager->logInforme($consulta);
            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {
                        return $this->render('informe/Portal/pantalla_flota.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'total' => $total_flota,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                    } else {
                        return $this->render('informe/Portal/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'total' => $total_flota,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                    }
                    break;
                case '5': //sale a excel
                    return $this->exportarAction(
                        $request,
                        $consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g' ? 1 : 0,
                        $consulta,
                        $informe
                    );
                    break;
            }
        }
        return $this->render('informe/Portal/portal.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOPORTICOS'),
        ));
    }

    private $portales = null; //funciona como cache de portales.
    private $portales_map = null; //funciona como cache de precios de portales.

    private function procesarServicio($servicio, $periodo, $consulta)
    {
        if (null == $servicio->getVehiculo() || null == $servicio->getVehiculo()->getTipovehiculo()) { //es null porque no tiene portico asociado
            return null;
        }
        $tVehiculo = $servicio->getVehiculo()->getTipovehiculo()->getId();
        //obtengo los portales
        if (is_null($this->portales) || !key_exists($tVehiculo, $this->portales)) {   //no existe el portal en la cache...
            //armo la estructura cache de portales
            $this->portales[$tVehiculo] = $this->getEntityManager()
                ->getRepository('App:PrecioPortal')
                ->findByTipoVehiculo(0, $servicio->getVehiculo()->getTipovehiculo());

            //armo la estructura cache de precios
            foreach ($this->portales[$tVehiculo] as $portal) {
                $this->portales_map[$tVehiculo][$portal->getId()] = array(
                    'nombre' => $portal->getReferencia()->getNombre(),
                    'precio' => intval($portal->getPrecio())
                );
            }
        }
        $consulta_informe = $this->backendManager->informePortales(
            $servicio->getId(),
            $periodo[0]['desde'],
            $periodo[0]['hasta'],
            $this->portales[$tVehiculo]
        );

        $total = 0;
        $cantidad = 0;
        //while ($entrada = $consulta_informe->fetch()) {
        foreach ($consulta_informe as $i => $entrada) {
            $cantidad++;
            $total += $this->portales_map[$tVehiculo][$entrada["precioportal_id"]]['precio'];

            $consulta_informe[$i]["precio"] = $this->portales_map[$tVehiculo][$entrada["precioportal_id"]]['precio'];
            $consulta_informe[$i]["portal"] = $this->portales_map[$tVehiculo][$entrada["precioportal_id"]]['nombre'];

            unset($consulta_informe[$i]['referencia']);
        }
        return array(
            'servicio' => $servicio->getNombre(),
            'tipovehiculo' => $servicio->getVehiculo()->getTipovehiculo()->getTipo(),
            'cantidad' => $cantidad,
            'total' => $total,
            'portal' => $consulta_informe
        );
    }

    /**
     * @Route("/informe/portal/{tipo}/exportar", name="informe_portal_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PORTALES")
     */
    public function exportarAction(Request $request,  $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        $total = json_decode($request->get('total'), true);
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Control de pórticos de Autopistas");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //un solo equipo
                $xls->setBar(6, array(
                    'A' => array('title' => 'Fecha', 'width' => 20),
                    'B' => array('title' => 'Portal', 'width' => 30),
                    'C' => array('title' => 'Precio ($)', 'width' => 20),
                    'D' => array('title' => 'Velocidad (km/h)', 'width' => 20),
                ));
                $xls->setCellValue('D2', $consulta['servicionombre']);
                $i = 7;

                foreach ($informe[0]['portal'] as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['fecha']),
                        'B' => array('value' => $value['portal']),
                        'C' => array('value' => $value['precio'], 'format' => '#.00'),
                        'D' => array('value' => $value['velocidad'], 'format' => '0'),
                    ));
                    $i++;
                }
            } else {   //es para la flota.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Tarifa', 'width' => 20),
                    'C' => array('title' => 'Cantidad', 'width' => 20),
                    'D' => array('title' => 'Total', 'width' => 20),
                ));
                $i = 7;
                foreach ($informe as $value) {   //esto es para cada servicio
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['servicio']),
                        'B' => array('value' => $value['tipovehiculo']),
                        'C' => array('value' => $value['cantidad'], 'format' => '0'),
                        'D' => array('value' => $value['total'], 'format' => '#0.00'),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=precioportal.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_portal'));
        }
    }
}
