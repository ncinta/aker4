<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\GraficoThType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\UserLoginManager;

class GraficothController extends AbstractController
{

    private $tempMax = 0;
    private $tempMin = 0;
    private $breadcrumbManager;
    private $servicioManager;
    private $grupoServicioManager;
    private $utilsManager;
    private $backendManager;
    private $userloginManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        GrupoServicioManager $grupoServicioManager,
        UtilsManager $utilsManager,
        BackendManager $backendManager,
        UserLoginManager $userloginManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->servicioManager = $servicioManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->utilsManager = $utilsManager;
        $this->userloginManager = $userloginManager;
    }

    /**
     * @Route("/informe/grafico/th", name="informe_grafico_sensor_th",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_GRAFICO_SENSOR_TH")
     */
    public function graficothAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Gráfico de Sensores");
        $servs = $this->servicioManager->findAllByUsuario();
        $servicios = array();
        foreach ($servs as $value) {
            if (count($value->getSensores()) > 0) {
                $servicios[] = $value;
            }
        }
        $options = array(
            'servicios' => $servicios,
            'grupos' => $this->grupoServicioManager->findAsociadas(),
        );
        $form = $this->createForm(GraficoThType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('graficoth');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            }

            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_grafico_sensor_th'));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                //obtengo el formato de la fechas a mostrar.
                $periodo = array('desde' => $desde, 'hasta' => $hasta);

                $servicio = $this->servicioManager->find(intval($consulta['servicio']));
                $consulta['servicionombre'] = !is_null($servicio) ? $servicio->getnombre() : 'n/n';

                //temperaturas por default
                $this->tempMax = 30; //$consulta['maximo'];
                $this->tempMin = 10;  // $consulta['minimo'];

                $data = $this->getDataInforme($servicio, $periodo, $consulta);

                //aca se redirige al destino correspondiente.
                return $this->render(
                    'informe/GraficoTh/grafico.html.twig',
                    array(
                        'consulta' => $consulta,
                        'dt' => $data,
                        'informe' => $data,
                    )
                );
            }
        }



        return $this->render('informe/GraficoTh/graficoth.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => null,
        ));
    }

    public function getDataInforme($servicio, $periodo)
    {
        $informe = array(
            'datos'=> array(),
            'tabla'=> array(),
            'titulos'=> array()
        );
        $titles = array('fecha');
        $tmpHistorial = $this->backendManager->historial($servicio->getId(), $periodo['desde'], $periodo['hasta'], array(), array());
        reset($tmpHistorial);

        while ($trama = current($tmpHistorial)) {
            next($tmpHistorial);

            if (isset($trama['sensores'])) {   //esto es para los seriados.
                foreach ($trama['sensores'] as $sens) {
                    if (isset($sens['id_sensor']) && isset($sens['medicion_sensor'])) {
                        $tramaSensor[$sens['id_sensor']] = $sens['medicion_sensor'];
                    }
                }
            }

            $data = null;
            $fecha = new \DateTime($trama['fecha'], new \DateTimeZone($this->userloginManager->getUser()->getTimezone()));
            $data['fecha'] = $fecha->format('d/m, H:i');
            foreach ($servicio->getSensores() as $sensor) {
                if ($sensor->getModeloSensor()->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                    if (isset($tramaSensor[$sensor->getSerie()])) {
                        $data[$sensor->getNombre()] = $tramaSensor[$sensor->getSerie()];
                    }
                } else {  //no es seriado
                    $campo = $sensor->getModeloSensor()->getCampoTrama();
                    if (isset($trama[$campo])) {
                        $data[$sensor->getNombre()] = $trama[$campo];
                    }
                }
                $titles[$sensor->getId()] = $sensor->getNombre();
            }

            $informe['datos'][] = array_values($data);
            $informe['tabla'][] = $data;
            $informe['titulos'] = array_values($titles);
        }

        return $informe;
    }
}
