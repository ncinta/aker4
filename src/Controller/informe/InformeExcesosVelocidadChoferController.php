<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeExcesosVelocidadChoferType;
use App\Entity\Organizacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\IbuttonManager;
use App\Model\app\ChoferManager;
use App\Model\app\GeocoderManager;

class InformeExcesosVelocidadChoferController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $grupoServicioManager;
    private $excelManager;
    private $geocoderManager;
    private $referenciaManager;
    private $ibuttonManager;
    private $choferManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager,
        ReferenciaManager $referenciaManager,
        IbuttonManager $ibuttonManager,
        ChoferManager $choferManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
        $this->referenciaManager = $referenciaManager;
        $this->choferManager = $choferManager;
    }

    /**
     * @Route("/informe/excesovelocidad/{id}/chofer", name="informe_excesosvelocidad_chofer",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function excesosvelocidadchoferAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Exceso Velocidad por Chofer");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $options['choferes'] = $this->choferManager->findAll($organizacion);
        $form = $this->createForm(InformeExcesosVelocidadChoferType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            //aca se obtiene el informe.
            $informe = array();
            $excesos = array();
            $chofer = null;
            $ibutton = null;
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeexcesosvelocidad');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->setFlash('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_excesosvelocidad_chofer', array('id' => $organizacion->getId())));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                    //armo los períodos que debo tener en cuenta.
                    $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);
                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];
                    $chofer = (intval($consulta['chofer'])) == 0 ? null : $this->choferManager->find($consulta['chofer']);
                    $consulta['chofernombre'] = !is_null($chofer) ? $chofer->getNombre() : 'Todos';
                    foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                        $excesos[] = $this->procesarServicio($servicio, $periodo, $consulta);
                    }
                }
                foreach ($excesos as $exceso) {
                    foreach ($exceso['data'] as $data) {  //recorro los excesos
                        $ibutton = $this->ibuttonManager->findByCodigo($data['ibutton']);    //obtengo el ibutton del exceso
                        if (!is_null($ibutton)) {                            //existe el ibutton
                            $choferS = is_null($ibutton->getChofer()) ? $ibutton->getCodigo() : $ibutton->getChofer()->getNombre();  //obtengo el chofer asignado
                            $exceso['ibutton'] = $choferS;                            //no se que hace esto                            
                            if ($chofer == null) { //cuando consulta por toda la flota de choferes                                
                                // hacer la estructura se conserver segun lo que necesite el template
                                if (!isset($informe[$choferS])) {
                                    $informe[$choferS] = ['servicio' => $exceso['servicio'], 'count' => 0, 'ibutton' => $choferS];
                                }
                                $informe[$choferS]['data'][] = $data;
                                $informe[$choferS]['count'] = count($informe[$choferS]['data']);
                            } else {   //consulto por choferes puntuales
                                if (!is_null($chofer->getIbutton())) {   //el chofer tiene asiganso un ibutton                                   
                                    if ($ibutton->getCodigo() == $chofer->getIbutton()->getCodigo()) { //solamente uno                                        
                                        if (!isset($informe[$choferS])) {
                                            $informe[$choferS] = ['servicio' => $exceso['servicio'], 'count' => 0, 'ibutton' => $choferS];
                                        }
                                        $informe[$choferS]['data'][] = $data;
                                        $informe[$choferS]['count'] = count($informe[$choferS]['data']);
                                    }
                                }
                            }
                        }
                    }
                }
                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
               // dd($informe);

                $this->loggerManager->logInforme($consulta);

                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/ExcesoVelocidadChofer/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;

                    case '5': //sale a excel
                        return $this->exportarAction($request,  1, $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/ExcesoVelocidadChofer/excesosvelocidad.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOEXCESOS'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta)
    {
        $info = $this->getDataInforme($servicio, $periodo, $consulta);
        return array(
            'servicio' => $servicio->getNombre(),
            'data' => $info,
            'count' => count($info)
        );
    }

    /**
     * @Route("/informe/excesovelocidad/{tipo}/chofer/exportar", name="informe_excesosvelocidad_chofer_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Excesos de Velocidades");

            $xls->setHeaderInfo(array(
                'C2' => 'Chofer',
                'D2' => $consulta['chofernombre'],
                'C3' => 'Servicio',
                'D3' => $consulta['servicionombre'],
                'C4' => 'Fecha desde',
                'D4' => $consulta['desde'],
                'C5' => 'Fecha hasta',
                'D5' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Chofer', 'width' => 20),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Fecha', 'width' => 20),
                'D' => array('title' => 'Máx. Permitida (km/h)', 'width' => 20),
                'E' => array('title' => 'Registrada (km/h)', 'width' => 20),
                'F' => array('title' => 'Duración', 'width' => 20),
            ));
            if ($consulta['referencia'] != 0) {
                $xls->setBar(6, array(
                    'G' => array('title' => 'Referencia', 'width' => 100)
                ));
            }
            if ($consulta['mostrar_direcciones']) {
                $xls->setBar(6, array(
                    'H' => array('title' => 'Domicilio', 'width' => 100),
                ));
            }
            $i = 7;
            foreach ($informe as $datos) {   //esto es para cada servicio
                foreach ($datos['data'] as $value) {
                    // die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');
                    $xls->setCellValue('A' . $i, $datos['ibutton']);
                    $xls->setCellValue('B' . $i, $datos['servicio']);
                    $xls->setRowValues($i, array(
                        'C' => array('value' => $value['fecha']),
                        'D' => array('value' => intval($value['velocidad_maxima']), 'format' => '0.00'),
                        'E' => array('value' => intval($value['velocidad_registrada']), 'format' => '0.00'),
                        'F' => array('value' => $value['duracion']),
                    ));
                    if ($consulta['referencia'] != 0) {
                        $xls->setRowValues($i, array(
                            'G' => array('value' => (!is_null($value['referencia_id'])) ?
                                $this->referenciaManager->find($value['referencia_id'])->getNombre() : '---')
                        ));
                    }
                    if ($consulta['mostrar_direcciones']) {
                        $xls->setRowValues($i, array(
                            'H' => array('value' => (!is_null($value['domicilio'])) ? $value['domicilio'] : '---',)
                        ));
                    }
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=velocidades.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_excesovelocidad_chofer'));
        }
    }

    public function getDataInforme($servicio, $periodo, $consulta)
    {
        if ($consulta['referencia'] == '0') {  //se pide por referencia
            $referencias = array();
        } elseif ($consulta['referencia'] == '-1') {
            $referencias = $this->referenciaManager->findAllVisibles();
        } else {
            $referencias = $this->referenciaManager->findAllByGrupo($consulta['referencia']);
        }

        $informe = array();
        foreach ($periodo as $key => $value) {
            $result = $this->backendManager->informeExcesosVelocidad(
                $servicio->getId(),
                $value['desde'],
                $value['hasta'],
                intval($consulta['velocidad_maxima']),
                $referencias
            );
            if ($result) {
                $informe = array_merge($informe, $result);
            }
        }
        //     die('////<pre>'.nl2br(var_export($servicio->getId(), true)).'</pre>////');
        foreach ($informe as $key => $value) {
            $informe[$key]['duracion'] = $this->utilsManager->segundos2tiempo($value['duracion']);
            if ($consulta['mostrar_direcciones']) {
                $informe[$key]['domicilio'] = '---';
                $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                $informe[$key]['domicilio'] = $direc;
            }
            if (!is_null($value['referencia_id'])) {
                $referencia = $this->referenciaManager->find($value['referencia_id']);
                if (!is_null($referencia)) {
                    $informe[$key]['referencia']['nombre'] = $referencia->getNombre();
                }
            }
        }
        return $informe;
    }
}
