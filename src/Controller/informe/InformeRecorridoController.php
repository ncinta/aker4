<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeRecorridoReferenciaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;

/**
 * Description of InformeRecorridoController
 *
 * @author nicolas cinta
 */
class InformeRecorridoController extends AbstractController
{

    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $userloginManager;
    private $gmapsManager;
    protected $colores = array(
        1 => '#000000',
        2 => '#000080',
        3 => '#FF0000',
        4 => '#800080',
        5 => '#CF5300',
        6 => '#000080',
        7 => '#FF00FF',
        8 => '#004603',
        9 => '#3b0067',
        10 => '#006469',
    );

    function __construct(
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager,
        UserLoginManager $userloginManager,
        GmapsManager $gmapsManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
    }

    /**
     * @Route("/informe/recorridoreferencia", name="informe_recorrido_referencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_RECORRIDO_REFERENCIA")
     */
    public function recorridoReferenciaAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Recorrido");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(InformeRecorridoReferenciaType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informerecorrido');
            $organizacion = $this->userloginManager->getOrganizacion();
            $arrServicios = array();
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                $mostrar_direcciones = isset($consulta['mostrar_direcciones']) ? $consulta['mostrar_direcciones'] : false;
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->recorridoReferenciaAction($request);
                } else {
                    $referencia = $this->referenciaManager->find(intval($consulta['referencia']));
                    $periodo = $this->informeUtilsManager->armarPeriodo($periodo['desde'], $periodo['hasta'], $consulta['destino'], false);
                    //obtengo los servicios a incluir en el informe.
                    if ($consulta['servicio'] == 0 || substr($consulta['servicio'], 0, 1) == 'g') {
                        if ($consulta['servicio'] == 0) {
                            $arrServicios = $this->servicioManager->findAllByOrganizacion($organizacion);
                            $consulta['servicionombre'] = 'Toda la Flota';
                        } else {
                            $grupo = $this->grupoServicioManager->find(substr($consulta['servicio'], 1));
                            foreach ($grupo->getServicios() as $servicio) {
                                $arrServicios[] = $servicio;
                            }
                            $consulta['servicionombre'] = 'Grupo ' . $grupo->getNombre();
                        }
                    } else {
                        $servicio = $this->servicioManager->find($consulta['servicio']);
                        $arrServicios[] = $servicio;
                        $consulta['servicionombre'] = $servicio->getNombre();
                    }
                }
            }
            $consulta['referencianombre'] = $referencia->getNombre();
            $this->loggerManager->logInforme($consulta);
            switch ($consulta['destino']) {
                case '6': //sale a mapa completo
                    $mapa = $this->crear_mapa($consulta, $referencia);
                    $informe = $this->agregar_recorrido($mapa, $consulta, $arrServicios);
                    $this->agregar_referencias($mapa, $referencia);
                    return $this->render('informe/Recorrido/mapa_completo.html.twig', array(
                        'consulta' => $consulta,
                        'mapa' => $mapa,
                        'informe' => $informe,
                        'ocultarpanelsur' => true,
                    ));
                    break;
            }
        }
        return $this->render('informe/Recorrido/recorrido.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function agregar_recorrido($mapa, $consulta, $servicios)
    {
        $arr_servicios = array();
        $arr_polilineas = array();
        foreach ($servicios as $servicio) {
            $i = 1;
            $historial = $this->getHistorial($consulta, $servicio);
            foreach ($historial as $key => $dia) {
                $this->gmapsManager->addRecorrido($mapa, $key, $dia['puntos_polilinea'], $this->colores[$i]);
                $arr_polilineas[] = $key;
                $i++;
            }
            if (count($historial)) {
                $arr_servicios[$servicio->getNombre()] = $historial;
            }
        }
        $inf_bottom = array('servicios' => $arr_servicios, 'polilineas' => json_encode($arr_polilineas));

        return $inf_bottom;
    }

    private function agregar_referencias($mapa, $referencia)
    {
        //agrego las referencias al mapa
        $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, false, true, 0.3);
        $mapa = $this->gmapsManager->setClusterable($mapa, true);
        return $mapa;
    }

    private function getMapaInforme($consulta)
    {
        $mapa = $this->gmapsManager->createMap();
        return $mapa;
    }

    private function crear_mapa($consulta, $referencia)
    {
        $mapa = $this->getMapaInforme($consulta);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $this->gmapsManager->setFitBounds($mapa, $referencia->getLatitud(), $referencia->getLongitud());



        return $mapa;
    }

    private function getHistorial($consulta, $servicio)
    {
        $opciones = array('opcion' => 'con_referencia');
        try {
            $referencia = $this->referenciaManager->find($consulta['referencia']);
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            $consHistorial = $this->backendManager->recorrido($servicio->getId(), $desde, $hasta, array($referencia), $opciones);
        } catch (\Exception $exc) {
            return null;
        }
        $historial = array();
        $puntoA = null;
        if (!is_null($consHistorial)) {
            reset($consHistorial);
            while ($trama = current($consHistorial)) {
                next($consHistorial);
                $dia = new \DateTime($trama['fecha']);

                if (isset($trama['referencia_id']) && $trama['referencia_id'] == $referencia->getId()) {
                    if (isset($trama['posicion'])) {
                        $key = $servicio->getId() . $dia->format('Ymd');
                        $historial[$key]['fecha'] = $dia->format('d-m-Y');
                        $historial[$key]['puntos_polilinea'][] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                        if ($puntoA == null) {
                            $puntoA = $trama;
                        }
                    }
                }
            }
        }
        unset($consHistorial);

        return $historial;
    }
}
