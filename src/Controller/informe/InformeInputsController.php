<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Form\informe\InformeInputsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\GeocoderManager;

class InformeInputsController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $excelManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager
    ) {
        $this->utilsManager = $utilsManager;
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/informe/inputs", name="informe_inputs")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INPUTS")
     */
    public function inputsAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Inputs");
        $servs = $this->servicioManager->findAllByUsuario();
        $servicios = array();
        foreach ($servs as $value) {
            if ($value->getEntradasDigitales()) {
                $servicios[] = $value;
            }
        }
        $options['servicios'] = $servicios;
        $form = $this->createForm(InformeInputsType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeinputs');
            if ($consulta) {
                if (!isset($consulta['mostrar_direcciones']))
                    $consulta['mostrar_direcciones'] = false;
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_inputs'));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                    //aca se obtiene el informe.
                    $servicio = $this->servicioManager->find($consulta['servicio']);
                    $consulta['servicionombre'] = $servicio->getNombre();
                    //este reporte es para un solo vehiculo, lo muestra desglosado.
                    $data = $this->getDataInforme(intval($consulta['servicio']), $periodo, $consulta);
                    $informe[] = array(
                        'servicio' => $servicio->getNombre(),
                        'data' => $data,
                        'totales' => $this->getTotalesInforme($data)
                    );
                    $consulta['tipo'] = '0';
                }
                // die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Inputs/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '3': //sale a mapa.
                        return $this->render_mapa($consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/Inputs/inputs.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => null,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function getDataInforme($servicio_id, $periodo, $consulta)
    {

        $referencias = $this->referenciaManager->findAsociadas();
        $servicio = $this->servicioManager->find($servicio_id);
        $options = array();
        foreach ($servicio->getInputs() as $input) {
            $options[$input->getModeloSensor()->getCampoTrama()] = true;
        }
        //$options = array('entrada_2' => true);
        $informe = $this->backendManager->informeInputs(intval($servicio_id), $periodo['desde'], $periodo['hasta'], $referencias, $options);
        foreach ($informe as $i => $trama) {
            //aca hago el procesamiento de las tramas...
            $informe[$i]['tiempo'] = isset($trama['segundos_activo']) ? $this->utilsManager->segundos2tiempo($trama['segundos_activo']) : '0';
            //distancia
            if (isset($trama['distancia']) && $trama['distancia'] != 0)
                $informe[$i]['distancia'] = $trama['distancia'] / 1000;

            //sensores
            foreach ($servicio->getInputs() as $input) {
                $campo = $input->getModeloSensor()->getCampoTrama();
                if (isset($trama[$campo]))
                    $informe[$i]['inputs'][$input->getNombre()] = $trama[$campo];
            }
            //referencia / direccion in
            if (isset($trama['referencia_in']) && !is_null($trama['referencia_in'])) {
                $referencia = $this->referenciaManager->find($trama['referencia_in']);
                if (!is_null($referencia))
                    $informe[$i]['referencia_in'] = $referencia->getNombre();

                unset($referencia);
            } else {
                if (isset($consulta['mostrar_direcciones']) && $consulta['mostrar_direcciones']) {
                    $direc = $this->geocoderManager->inverso($trama['posicion_in']['latitud'], $trama['posicion_in']['longitud']);
                    $informe[$i]['referencia_in'] = $direc;
                }
            }

            //referencia / direccion out
            if (isset($trama['referencia_out']) && !is_null($trama['referencia_out'])) {
                $referencia = $this->referenciaManager->find($trama['referencia_out']);
                if (!is_null($referencia))
                    $informe[$i]['referencia_out'] = $referencia->getNombre();
                unset($referencia);
            } else {
                if (isset($consulta['mostrar_direcciones']) && $consulta['mostrar_direcciones']) {
                    if ($trama['posicion_in']['latitud'] != $trama['posicion_out']['latitud'] && $trama['posicion_in']['longitud'] != $trama['posicion_out']['longitud']) {
                        $direc = $this->geocoderManager->inverso($trama['posicion_out']['latitud'], $trama['posicion_out']['longitud']);
                        if ($direc != '---')
                            $informe[$i]['referencia_out'] = $direc;
                    } else {
                        if (isset($informe[$i]['referencia_in']))
                            $informe[$i]['referencia_out'] = $informe[$i]['referencia_in'];
                    }
                }
            }
        }
        return $informe;
    }

    public function getTotalesInforme($informe)
    {
        $total = array(
            'segundos_activo' => 0,
            'tiempo_activo' => 0,
            'distancia' => 0,
            'km_por_dia' => 0,
        );
        foreach ($informe as $value) {
            if (isset($value['segundos_activo'])) {
                $total['segundos_activo'] += $value['segundos_activo'];
            }
            if (isset($value['distancia'])) {
                $total['km_por_dia'] += $value['distancia'];
            }
        }
        //        die('////<pre>'.nl2br(var_export($informe, true)).'</pre>////');
        $total['tiempo_activo'] = $this->utilsManager->segundos2tiempo(intval($total['segundos_activo']));
        return $total;
    }

    /**
     * @Route("/informe/inputs/{tipo}/exportar", name="informe_inputs_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INPUTS")
     */
    public function exportarAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Activación de Entradas Digitales");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            //             die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
            $xls->setBar(6, array(
                'A' => array('title' => 'Entrada', 'width' => 20),
                'B' => array('title' => 'Fecha Inicio', 'width' => 20),
                'C' => array('title' => 'Referencia Inicio', 'width' => 80),
                'D' => array('title' => 'Fecha Fin', 'width' => 20),
                'E' => array('title' => 'Referencia Fin', 'width' => 80),
                'F' => array('title' => 'Duración', 'width' => 20),
                'G' => array('title' => 'Distancia', 'width' => 20),
            ));
            $xls->setCellValue('D2', $consulta['servicionombre']);
            $i = 7;
            $servicio = $informe[0]['data'];
            foreach ($servicio as $key => $row) {
                //                die('////<pre>' . nl2br(var_export(array_keys($row['inputs']), true)) . '</pre>////');
                $xls->setRowValues($i, array(
                    'A' => array('value' => implode(',', array_keys($row['inputs']))),
                    'B' => array('value' => $row['fecha_ingreso']),
                    'C' => array('value' => $row['referencia_in']),
                    'D' => array('value' => $row['fecha_egreso']),
                    'E' => array('value' => $row['referencia_out']),
                    'F' => array('value' => $row['tiempo'], 'format' => '[HH]:MM:SS'),
                    'G' => array('value' => $row['distancia']),
                ));
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=entradasdigitales.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_inputs'));
        }
    }

    private function render_mapa($consulta, $informe)
    {
        $mapa = $this->getMapaInforme($consulta, $informe);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $informe = $informe[0]['data'];
        foreach ($informe as $id => $data) {
            $label = 'Id: ' . $id;
            $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $data['posicion_in'], $icon, $label);
            $arr[] = sprintf('<b>ID: %d</b>', $id);
            $arr[] = sprintf('<b>Fecha Ingreso:</b> %s', $data['fecha_ingreso']);
            $arr[] = sprintf('<b>Posicion:</b> %s, %s', $data['posicion_in']['latitud'], $data['posicion_in']['longitud']);

            $mark->setInformacion(implode('<br>', $arr));

            if ($min_lat === false) {
                $min_lat = $max_lat = $data['posicion_in']['latitud'];
                $min_lng = $max_lng = $data['posicion_in']['longitud'];
            } else {
                $min_lat = min($min_lat, $data['posicion_in']['latitud']);
                $max_lat = max($max_lat, $data['posicion_in']['latitud']);
                $min_lng = min($min_lng, $data['posicion_in']['longitud']);
                $max_lng = min($max_lng, $data['posicion_in']['longitud']);
            }
        }
        $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
        return $this->render('informe/Inputs/mapa.html.twig', array(
            'consulta' => $consulta,
            'informe' => $informe,
            'mapa' => $mapa,
        ));
    }

    private function getMapaInforme($consulta, $informe)
    {
        $mapa = $this->gmapsManager->createMap(true);

        //meto las referencias en el mapa.
        $referencias = $this->referenciaManager->findAsociadas();
        foreach ($referencias as $referencia) {
            //agrego las referencias
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, true);
        }

        return $mapa;
    }
}
