<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\IndiceMantenimientoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Organizacion as Organizacion;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\MantenimientoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\ServicioManager;
use App\Model\app\CentroCostoManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\IndiceManager;

/**
 * Description of IndiceMantenimientoController
 *
 * @author nicolas
 */
class IndiceMantenimientoController extends AbstractController
{

    protected $breadcrumbManager;
    protected $excelManager;
    protected $userloginManager;
    protected $mantenimientoManager;
    protected $loggerManager;
    protected $utilsManager;
    protected $servicioManager;
    protected $centrocostoManager;
    protected $gruposervicioManager;
    protected $indiceManager;
    protected $servicio_filtro = array();

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ExcelManager $excelManager,
        UserLoginManager $userloginManager,
        MantenimientoManager $mantenimientoManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        ServicioManager $servicioManager,
        CentroCostoManager $centrocostoManager,
        GrupoServicioManager $gruposervicioManager,
        IndiceManager $indiceManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->excelManager = $excelManager;
        $this->userloginManager = $userloginManager;
        $this->mantenimientoManager = $mantenimientoManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->servicioManager = $servicioManager;
        $this->centrocostoManager = $centrocostoManager;
        $this->gruposervicioManager = $gruposervicioManager;
        $this->indiceManager = $indiceManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/informe/mantenimiento/{id}/indice", name="informe_mantenimiento_indice",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function indiceAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Indice de Mantenimiento");

        $form = $this->createForm(IndiceMantenimientoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));

        $consulta = $request->get('form');

        if ($consulta) {
            $tmp = array();
            parse_str($request->get('form'), $tmp);
            $data = $tmp['indice'];
            $this->loggerManager->setTimeInicial();
            $desde = $this->utilsManager->datetime2sqltimestamp('01/' . $data['desde'], true);
            $hasta = $this->utilsManager->datetime2sqltimestamp(
                date("t-m-Y", strtotime(str_replace('/', '-', '01/' . $data['hasta']))),
                false
            ); // traigo el ultimo dia del mes 
            //die('////<pre>' . nl2br(var_export($data['servicio'], true)) . '</pre>////');
            $servicio_str = isset($data['servicio']) ? $data['servicio'] : null;
            $cc_str = isset($data['centro_costo']) ? $data['centro_costo'] : null;
            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
            } else {
                $informe = $this->armarInforme($servicio_str, $cc_str, $desde, $hasta);
                $filtro = array(
                    'servicio' => $informe['filtro'],
                    'desde' => $data['desde'],
                    'hasta' => $data['hasta']
                );

                return new Response(json_encode(array(
                    'html' => $this->renderView('informe/IndiceMantenimiento/content.html.twig', array(
                        'indices' => $informe
                    )),
                    'indices' => $informe
                )), 200);
            }
        }

        $this->loggerManager->logInforme($consulta);
        $servicio_str = '0';
        $desde = $this->utilsManager->datetime2sqltimestamp('01/' . (new \DateTime())->modify('-30 days')->format('m/Y'), true);
        $hasta = $this->utilsManager->datetime2sqltimestamp(
            date("t-m-Y", strtotime(str_replace('/', '-', '01/' . (new \DateTime())->format('m/Y')))),
            false
        ); //ultimo dia del mes
        $informe = $this->armarInforme($servicio_str, null, $desde, $hasta);

        return $this->render('informe/IndiceMantenimiento/pantalla.html.twig', array(
            'indices' => $informe,
            'indices_json' => json_encode($informe),
            'desde' => (new \DateTime())->modify('-30 days')->format('m/Y'),
            'hasta' => (new \DateTime())->format('m/Y'),
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'time' => $this->loggerManager->getTimeGeneracion(),
        ));
    }

    private function armarInforme($servicios_pedido, $centroscosto, $desde, $hasta)
    {
        // die('////<pre>' . nl2br(var_export($servicios, true)) . '</pre>////');
        $indices = array();

        $filtro = array(
            'desde' => (new \DateTime($desde))->format('d/m/Y'),
            'hasta' => (new \DateTime($hasta))->format('d/m/Y'),
            'servicio' => ''
        );

        if ($centroscosto == null) { //buscar por grupo o servicios
            if ($servicios_pedido == '0') {  //informe para toda la flota
                $servicios = $this->servicioManager->findAllByOrganizacion($this->userloginManager->getOrganizacion());
                $servicio_filtro[] = 'Toda la flota';
                $indices = $this->indiceManager->getIndicesServicios($servicios, $desde, $hasta);
            } elseif (substr($servicios_pedido, 0, 1) === 'g') {   //es un grupo de servicios
                $grupo = $this->gruposervicioManager->find(intval(substr($servicios_pedido, 1)));
                $servicio_filtro[] = 'grupo "' . $grupo->getNombre() . '"';
                $indices = $this->indiceManager->getIndicesGs($grupo, $desde, $hasta);
            } else {    //este reporte es para un solo vehiculo, lo muestra desglosado.
                $servicio = $this->servicioManager->find(intval($servicios_pedido));
                if ($servicio) {
                    $servicio_filtro[] = $servicio->getNombre();
                } else {
                    $servicio_filtro[] = 'error';
                }
                
                $indices = $this->indiceManager->getIndicesServicios(array($servicio), $desde, $hasta);
            }
        } else { //busca por cc
            $centros = array();
           // die('////<pre>' . nl2br(var_export($centroscosto, true)) . '</pre>////');
            foreach ($centroscosto as $cc) {
                if ($cc != '0') {
                    $centro = $this->centrocostoManager->find(intval(substr($cc, 1)));
                    $centros[] = $centro;
                    $servicio_filtro[] = 'centro de costo "' . $centro->getNombre() . '"';
                } else {
                    $servicio_filtro[] = 'Todos los centro de costo';
                    $organizacion = $this->userloginManager->getOrganizacion();
                    $centros = $this->centrocostoManager->findAllByOrganizacion($organizacion);
                }
            }
            $indices = $this->indiceManager->getIndicesCc($centros, $desde, $hasta);
        }
        $filtro['servicio'] = implode(", ", $servicio_filtro);

        return array('indices' => $this->indiceManager->ordenarIndices($indices), 'filtro' => $filtro);
    }
}
