<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Form\informe\InformeMantenimientoType;
use App\Form\informe\InformeHistoricoMantenimientoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\TareaMantManager;

class InformeMantenimientoController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $excelManager;
    private $tareaMantManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ExcelManager $excelManager,
        TareaMantManager $tareaMantManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->excelManager = $excelManager;
        $this->tareaMantManager = $tareaMantManager;
    }

    /**
     * @Route("/informe/{id}/mantenimiento", name="informe_mantenimiento",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function indexAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Tareas Manteniemiento");
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
        );

        $form = $this->createForm(InformeMantenimientoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('informemant');
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $organizacion = $this->organizacionManager->find($id);
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //establesco sobre que servicios debo controlar.
                if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                    if ($consulta['servicio'] == '0') {
                        $user = null;
                        if ($organizacion != $this->userloginManager->getOrganizacion()) {
                            $user = $organizacion->getUsuarioMaster();
                        }
                        $servicios = $this->servicioManager->findAllByUsuario($user);
                    } else {
                        $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                    }
                } else {    //es para un solo vehiculo
                    $servicios = array($this->servicioManager->find($consulta['servicio']));
                }

                //ejecuto las consultas.
                $informe = $this->tareaMantManager->procesarStatusMantenimiento($servicios);

                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');

                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Mantenimiento/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 0, $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/Mantenimiento/index.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/informe/mantenimiento/{tipo}/exportar", name="informe_mantenimiento_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Tareas de Mantenimiento");

            //encabezado...
            $now = new \DateTime('', new \DateTimeZone($this->userloginManager->getTimezone()));
            $xls->setHeaderInfo(array(
                'C2' => 'Informe generado el ' . $now->format('d/m/Y H:i:s'),
            ));
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Tipo', 'width' => 20),
                'C' => array('title' => 'Odometro', 'width' => 10),
                'D' => array('title' => 'Horometro', 'width' => 10),
                'E' => array('title' => 'Mantenimiento', 'width' => 30),
                'F' => array('title' => 'Estado', 'width' => 30),
            ));
            $i = 7;
            foreach (array_reverse($informe) as $tareas) {
                if ($tareas) {
                    foreach ($tareas as $tarea) {
                        //   die('////<pre>'.nl2br(var_export($tarea, true)).'</pre>////');
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $tarea['servicio']),
                            'B' => array('value' => $tarea['tipo_vehiculo']),
                            'C' => array('value' => $tarea['odometro'], 'format' => '0.0'),
                            'D' => array('value' => $tarea['horometro'], 'format' => '0.0'),
                            'E' => array('value' => $tarea['nombre']),
                            'F' => array('value' => $tarea['status']['message']),
                        ));
                        $i++;
                    }
                }
            }



            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=mantenimiento.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_mantenimiento'));
        }
    }

    function parse($fecha)
    {
        list($fecha, $hora) = explode(" ", $fecha);
        list($y, $m, $d) = explode("-", $fecha);
        list($h, $n, $s) = explode(":", $hora);
        return mktime($h, $n, $s, $m, $d, $y);
    }

    /**
     * @Route("/mantenimiento/{id}/status", name="informe_status_mantenimiento",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function statusAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Tareas Manteniemiento por Servicio");
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
        );
        $form = $this->createForm(InformeMantenimientoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('informemant');
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $organizacion = $this->organizacionManager->find($id);
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //establesco sobre que servicios debo controlar.
                if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                    if ($consulta['servicio'] == '0') {
                        $user = null;
                        if ($organizacion != $this->userloginManager->getOrganizacion()) {
                            $user = $organizacion->getUsuarioMaster();
                        }
                        $servicios = $this->servicioManager->findAllByUsuario($user);
                    } else {
                        $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                    }
                } else {    //es para un solo vehiculo
                    $servicios = array($this->servicioManager->find($consulta['servicio']));
                }

                //ejecuto las consultas.
                $informe = $this->tareaMantManager->procesarStatusMantenimientoPorServicio($servicios, $consulta);


                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Mantenimiento/pantallastatus.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarstatusAction(
                            $request,
                            0,
                            $consulta,
                            $informe
                        );
                        break;
                }
            }
        }
        return $this->render('informe/Mantenimiento/status.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/informe/mantenimiento/status/{tipo}/exportar", name="informe_status_mantenimiento_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function exportarstatusAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {

        if ($informe == null) {
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Tareas de Mantenimiento por Servicio");

            //encabezado...
            $now = new \DateTime('', new \DateTimeZone($this->userloginManager->getTimezone()));
            $xls->setHeaderInfo(array(
                'C2' => 'Informe generado el ' . $now->format('d/m/Y H:i:s'),
            ));
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Tipo', 'width' => 20),
                'C' => array('title' => 'Odometro', 'width' => 10),
                'D' => array('title' => 'Horometro', 'width' => 10),
                'E' => array('title' => 'Ult. Reporte', 'width' => 10),
                'F' => array('title' => 'Mantenimiento', 'width' => 30),
                'G' => array('title' => 'Ultimo', 'width' => 30),
                'H' => array('title' => 'Estado', 'width' => 30),
            ));
            $i = 7;
            foreach ($informe as $servicio) {
                $ultFechaHora = '';
                $serv = $servicio['servicio'];

                if ($serv['ult_fechahora'] != null) {
                    if (is_array($request->get('informe'))) {
                        //die('////<pre>' . nl2br(var_export($serv, true)) . '</pre>////');
                        $date = $serv['ult_fechahora'];
                    } else {
                        $date = new \DateTime($serv['ult_fechahora']['date']);
                    }
                    $ultFechaHora = $date->format('d-m-Y H:i:s');
                }

                if ($servicio['mantenimientos'] != null) {
                    foreach ($servicio['mantenimientos'] as $tarea) {
                        if ($servicio['mantenimientos'] != null) {
                            $nomTarea = $tarea['nombre'];
                            $staTarea = $tarea['status']['message'];
                            if ($tarea['ultimo'] != null) {
                                $date = new \DateTime($this->utilsManager->datetime2sqltimestamp($tarea['ultimo'], false));
                                $ultTarea = $date->format('d-m-Y');
                            } else {
                                $ultTarea = '';
                            }
                        } else {
                            $nomTarea = '';
                            $staTarea = '';
                            $ultTarea = '';
                        }
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $serv['nombre']),
                            'B' => array('value' => $serv['tipo_vehiculo']),
                            'C' => array('value' => $serv['odometro'], 'format' => '0.0'),
                            'D' => array('value' => $serv['horometro'], 'format' => '0.0'),
                            'E' => array('value' => $ultFechaHora),
                            'F' => array('value' => $nomTarea),
                            'G' => array('value' => $ultTarea),
                            'H' => array('value' => $staTarea),
                        ));
                        $i++;
                    }
                } else {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $serv['nombre']),
                        'B' => array('value' => $serv['tipo_vehiculo']),
                        'C' => array('value' => $serv['odometro'], 'format' => '0.0'),
                        'D' => array('value' => $serv['horometro'], 'format' => '0.0'),
                        'E' => array('value' => $ultFechaHora),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=mantenimiento.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_mantenimiento'));
        }
    }

    /**
     * @Route("/informe/{id}/mantenimiento/historico", name="informe_historial_mant",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function historicoAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Historico de Manteniemiento");
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($user),
            'grupos' => $this->grupoServicioManager->findAllByUsuario($user),
        );
        $form = $this->createForm(InformeHistoricoMantenimientoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('informemant');
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $organizacion = $this->organizacionManager->find($id);
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                //establesco sobre que servicios debo controlar.
                if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                    if ($consulta['servicio'] == '0') {
                        $consulta['servicionombre'] = 'Toda la flota';
                        $user = null;
                        if ($organizacion != $this->userloginManager->getOrganizacion()) {
                            $user = $organizacion->getUsuarioMaster();
                        }
                        $servicios = $this->servicioManager->findAllByUsuario($user);
                    } else {
                        $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                        $consulta['servicionombre'] = $grupo->getNombre();
                    }
                } else {    //es para un solo vehiculo
                    $servicios = array($this->servicioManager->find($consulta['servicio']));
                    $consulta['servicionombre'] = $servicios[0]->getNombre();
                }
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                //ejecuto las consultas.
                $incluir_ok = isset($consulta['incluir_ok']) ? true : false;
                $informe = $this->procesarHistorico($servicios, $incluir_ok, $desde, $hasta);
                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/HistoricoMantenimiento/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarhistoricoAction($request, 0, $consulta, $informe);
                        break;
                }
            }
        }

        return $this->render('informe/HistoricoMantenimiento/historico.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function procesarHistorico($servicios, $incluir_ok, $desde, $hasta)
    {
        $arr_servicio = array();
        foreach ($servicios as $servicio) {
            $arr_tarea = array();
            $tareas = $this->tareaMantManager->findHistoricoByFecha($servicio, $desde, $hasta);
            foreach ($tareas as $tarea) {
                $status = null;
                $sm = $tarea->getServicioMantenimiento();
                //die('////<pre>' . nl2br(var_export(count($tareas), true)) . '</pre>////');
                if (!is_null($sm)) {
                    $status = $this->tareaMantManager->analizeMantenimiento($sm);
                }
                $add = true;
                if ($status['code'] == 1) {
                    $add = isset($incluir_ok) && $incluir_ok;
                }
                if ($add) {
                    $arr_tarea[] = array('tarea' => $tarea, 'status' => $status);
                    $arr_servicio[$servicio->getId()] = array(
                        'servicio' => $servicio,
                        'tareas' => $arr_tarea
                    );
                }
            }
        }
        return $arr_servicio;
    }

    /**
     * @Route("/informe/mantenimiento/{tipo}/exportarhistorico", name="informe_mantenimiento_exportarhistorico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANTENIMIENTO")
     */
    public function exportarhistoricoAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe historico de mantenimiento");
            //encabezado...
            $now = new \DateTime('', new \DateTimeZone($this->userloginManager->getTimezone()));
            $xls->setHeaderInfo(array(
                'C2' => 'Informe generado el ' . $now->format('d/m/Y H:i:s'),
                'C3' => 'Servicio',
                'D3' => $consulta['servicionombre'],
                'C4' => 'Desde',
                'D4' => $consulta['desde'],
                'C5' => 'Hasta',
                'D5' => $consulta['hasta'],
            ));
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 20),
                'C' => array('title' => 'Mantenimiento', 'width' => 50),
                'D' => array('title' => 'Tipo', 'width' => 20),
                'E' => array('title' => 'Taller', 'width' => 30),
                'F' => array('title' => 'Costo', 'width' => 30),
            ));
            $i = 7;
            foreach ($informe as $data) {
                foreach ($data['tareas'] as $value) {
                    $fecha = $value['tarea']->getFecha();
                    $tipo = is_null($value['tarea']->getServicio()) ? 'Periodica' : 'Eventual';
                    $taller = is_null($value['tarea']->getTaller()) ? ' --- ' : $value['tarea']->getTaller()->getNombre();
                    $direccion = is_null($value['tarea']->getTaller()) ? ' --- ' : $value['tarea']->getTaller()->getDireccion();
                    $status = is_null($value['status']) ? ' --- ' : $value['status']['message'];
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $data['servicio']->getNombre()),
                        'B' => array('value' => $fecha)
                    ));
                    if (!is_null($value['tarea']->getServiciomantenimiento())) {
                        $xls->setRowValues(
                            $i,
                            array(
                                'C' => array('value' => $value['tarea']->getServicioMantenimiento()->getMantenimiento()->getNombre() . ' (' . $value['tarea']->getObservacion() . ')')
                            )
                        );
                    } else {
                        $xls->setRowValues($i, array(
                            'C' => array('value' => $value['tarea']->getObservacion())
                        ));
                    }
                    $xls->setRowValues($i, array(
                        'D' => array('value' => $tipo),
                        'E' => array('value' => $taller),
                        'F' => array('value' => intval($value['tarea']->getCosto())),
                    ));

                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=mantenimiento.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
    }
}
