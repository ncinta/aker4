<?php

namespace App\Controller\informe;

use App\Form\informe\InformeCanbusResumenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organizacion;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;

class InformeCanbusResumenController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $excelManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        ExcelManager $excelManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
    }

    function parse($fecha)
    {
        list($fecha, $hora) = explode(" ", $fecha);
        list($y, $m, $d) = explode("-", $fecha);
        list($h, $n, $s) = explode(":", $hora);
        return mktime($h, $n, $s, $m, $d, $y);
    }

    /**
     * @Route("/informe/{id}/canbus/resumen", name="informe_canbus_resumen",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CANBUS")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Viajes Canbus");
        $user = $this->userloginManager->getUser();
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }

        //obtengo los servicios que tienen canbus solamente.
        $servicios = array();
        $servs = $this->servicioManager->findAllByUsuario($user);
        foreach ($servs as $value) {
            if ($value->getCanbus()) {
                $servicios[] = $value;
            }
        }
        $options['servicios'] = $servicios;
        $form = $this->createForm(InformeCanbusResumenType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informecanbusresumen');
            //die('////<pre>'.nl2br(var_export($consulta, true)).'</pre>////');
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_canbus_resumen', array('id' => $organizacion->getId())));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];
                    //aca selecciono solamente los servicios con canbus.
                    $servicios = array();
                    foreach ($arrServicios['servicios'] as $servicio) {
                        if ($servicio->getCanbus()) {
                            $servicios[] = $servicio;
                        }
                    }


                    //ejecuto las consultas.
                    $referencias = array();
                    foreach ($servicios as $servicio) {  //informe para cada servicio
                        if ($servicio->getUltFechahora() < $periodo['desde'])     //evito q se consulte para equipos q no reportan en el periodo
                            continue;

                        $tmpHistorial = $this->backendManager->historial($servicio->getId(), $periodo['desde'], $periodo['hasta'], $referencias, array());
                        //die('////<pre>' . nl2br(var_export($tmpHistorial, true)) . '</pre>////');
                        if (is_array($tmpHistorial)) {
                            reset($tmpHistorial);
                            $viaje = array();
                            while ($trama = current($tmpHistorial)) {
                                next($tmpHistorial);
                                $data = null;
                                if (isset($trama['canbusData']) || isset($trama['canbusInicioTrayecto']) || isset($trama['canbusFinTrayecto'])) {

                                    if (isset($trama['canbusData']) && !isset($fecha_inicio)) {
                                        $fecha_inicio = $trama['fecha'];
                                    }
                                    if (isset($trama['canbusInicioTrayecto'])) {
                                        $fecha_inicio = $trama['fecha'];
                                    }
                                    if (isset($trama['canbusFinTrayecto'])) {
                                        $tramaFin = $trama['canbusFinTrayecto'];
                                        $data['viaje'] = count($viaje) + 1;
                                        $data['tipo'] = 'Fin';
                                        $data['fecha_inicio'] = $fecha_inicio;
                                        unset($fecha_inicio);
                                        $data['fecha_fin'] = $trama['fecha'];
                                        $data['tiempo_segundos'] = $tramaFin['duracionTray'];
                                        $data['tiempo'] = $this->utilsManager->segundos2tiempo($tramaFin['duracionTray']);
                                        $data['distancia'] = $tramaFin['distanciaTray'];
                                        $data['consumo'] = $tramaFin['consumoTray'];
                                        $data['max_velocidad'] = $tramaFin['velMaxTray'];
                                        $data['max_rpm'] = $tramaFin['maxRPMTray'];
                                        $data['marcha_segundos'] = $tramaFin['tiempoMotorMarcha'];
                                        $data['marcha_tiempo'] = $this->utilsManager->segundos2tiempo($tramaFin['tiempoMotorMarcha']);
                                        $data['marcha_consumo'] = $tramaFin['consumoMarcha'];

                                        $data['ralenti_segundos'] = $tramaFin['tiempoRalenti'];
                                        $data['ralenti_tiempo'] = $this->utilsManager->segundos2tiempo($tramaFin['tiempoRalenti']);
                                        $data['ralenti_consumo'] = $tramaFin['consumoRalenti'];

                                        $data['crucero_segundos'] = $tramaFin['tiempoCrucero'];
                                        $data['crucero_tiempo'] = $this->utilsManager->segundos2tiempo($tramaFin['tiempoCrucero']);
                                        $data['crucero_distancia'] = $tramaFin['distanciaCrucero'];
                                        $data['crucero_consumo'] = $tramaFin['consumoCrucero'];
                                        $data['crucero_cantidad'] = $tramaFin['cantUsoCrucero'];

                                        $data['carga_motor'] = $tramaFin['cargaMotor'];
                                        $data['cant_freno'] = $tramaFin['cantUsoFreno'];
                                        $data['cant_retarder'] = $tramaFin['cantUsoRetarder'];
                                        $data['cant_embrague'] = $tramaFin['cantUsoEmbrague'];

                                        $odometro = $tramaFin['odometroFin'] - $tramaFin['odometroInicio'];
                                        $odolitro = $tramaFin['odolitroFin'] - $tramaFin['odolitroInicio'];
                                        if ($odometro > 0 && $odolitro > 0) {
                                            $data['rendimiento'] = ($odometro / $odolitro);
                                        } else {
                                            $data['rendimiento'] = 0;
                                        }
                                        $data['color_rendim'] = "#FFFFFF";
                                    }
                                }
                                if ($data != null)
                                    $viaje[] = $data;
                            }
                            unset($tmpHistorial);
                            $informe[$servicio->getNombre()] = array(
                                'data' => $viaje,
                                'totales' => $this->getTotalesInforme($viaje, $this->utilsManager),
                            );
                        }
                    }
                }
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/CanbusResumen/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 0, $consulta, $informe);
                        break;
                }
            }
        }

        return $this->render('informe/CanbusResumen/index.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    public function getTotalesInforme($info, $utils)
    {
        $total = array(
            'tiempo' => 0,
            'distancia' => 0,
            'consumo' => 0,
            'max_velocidad' => 0,
            'max_rpm' => 0,
            'marcha_tiempo' => 0,
            'marcha_consumo' => 0,
            'ralenti_tiempo' => 0,
            'ralenti_consumo' => 0,
            'crucero_tiempo' => 0,
            'crucero_distancia' => 0,
            'crucero_consumo' => 0,
            'crucero_cantidad' => 0,
            'cant_freno' => 0,
            'cant_retarder' => 0,
            'cant_embrague' => 0,
            'rendimiento' => 0,
            'carga_motor' => 0,
        );

        $cant_rend = $cant_carga = 0;
        $total['viaje'] = count($info);
        if (count($info) > 0) {
            foreach ($info as $value) {

                //            $data['fecha_inicio'] = $fecha_inicio;
                //            $data['fecha_fin'] = $trama['fecha'];
                $total['tiempo'] += intval($value['tiempo_segundos']);
                $total['distancia'] += intval($value['distancia']);
                $total['consumo'] += intval($value['consumo']);

                if ($total['max_velocidad'] < $value['max_velocidad']) {
                    $total['max_velocidad'] = $value['max_velocidad'];
                }
                if ($total['max_rpm'] < $value['max_rpm']) {
                    $total['max_rpm'] = $value['max_rpm'];
                }

                $total['marcha_tiempo'] += intval($value['marcha_segundos']);
                $total['marcha_consumo'] += intval($value['marcha_consumo']);

                $total['ralenti_tiempo'] += intval($value['ralenti_segundos']);
                $total['ralenti_consumo'] += intval($value['ralenti_consumo']);

                $total['crucero_tiempo'] += intval($value['crucero_segundos']);
                $total['crucero_distancia'] += intval($value['crucero_distancia']);
                $total['crucero_consumo'] += intval($value['crucero_consumo']);
                $total['crucero_cantidad'] += intval($value['crucero_cantidad']);

                if ($value['carga_motor'] != 0) {
                    $cant_carga++;
                }
                $total['carga_motor'] += intval($value['carga_motor']);
                $total['cant_freno'] += intval($value['cant_freno']);
                $total['cant_retarder'] += intval($value['cant_retarder']);
                $total['cant_embrague'] += intval($value['cant_embrague']);

                if ($value['rendimiento'] != 0) {
                    $cant_rend++;
                }
                $total['rendimiento'] += intval($value['rendimiento']);
            }
            if ($cant_rend > 0)
                $total['rendimiento'] = $total['rendimiento'] / $cant_rend;
            if ($cant_carga > 0)
                $total['carga_motor'] = $total['carga_motor'] / $cant_carga;

            $total['tiempo'] = $this->utilsManager->segundos2tiempo($total['tiempo']);
            $total['marcha_tiempo'] = $this->utilsManager->segundos2tiempo($total['marcha_tiempo']);
            $total['ralenti_tiempo'] = $this->utilsManager->segundos2tiempo($total['ralenti_tiempo']);
            $total['crucero_tiempo'] = $this->utilsManager->segundos2tiempo($total['crucero_tiempo']);
        }
        return $total;
    }

    /**
     * @Route("/informe/canbus/resumen/{tipo}/exportar", name="informe_canbus_resumen_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CANBUS")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            if (is_array($request->get('informe'))) {
                $informe = $request->get('informe');
            } else {
                $informe = json_decode($request->get('informe'), true);
            }
            if (is_array($request->get('consulta'))) {
                $consulta = $request->get('consulta');
            } else {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        if (isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Viaje de Canbus");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 25),
                'B' => array('title' => 'Viaje', 'width' => 10),
                'C' => array('title' => 'Inicio', 'width' => 20),
                'D' => array('title' => 'Fin', 'width' => 20),
                'E' => array('title' => 'Tiempo (hs)', 'width' => 10),
                'F' => array('title' => 'Distancia (kms)', 'width' => 10),
                'G' => array('title' => 'Combustible (lts)', 'width' => 10),
                'H' => array('title' => 'Rendim (kms/lts)', 'width' => 8),
                'I' => array('title' => 'Vel. Max.', 'width' => 10),
                'J' => array('title' => 'RPM Max.', 'width' => 10),
                'K' => array('title' => 'Marcha Tiempo', 'width' => 10),
                'L' => array('title' => 'Marcha Litros', 'width' => 10),
                'M' => array('title' => 'Ralenti Tiempo', 'width' => 10),
                'N' => array('title' => 'Ralenti Litros', 'width' => 10),
                'O' => array('title' => 'Crucero Tiempo', 'width' => 10),
                'P' => array('title' => 'Crucero Litros', 'width' => 10),
                'Q' => array('title' => 'Crucero Kms', 'width' => 10),
                'R' => array('title' => 'Crucero Cant.', 'width' => 10),
                'S' => array('title' => 'Carga Motor', 'width' => 10),
                'T' => array('title' => 'Uso Freno', 'width' => 10),
                'U' => array('title' => 'Uso Retarder', 'width' => 10),
                'V' => array('title' => 'Uso Embrague', 'width' => 10),
            ));
            $i = 7;
            foreach ($informe as $nombre => $tramas) {   //esto es para cada referencia
                //die('////<pre>' . nl2br(var_export($tramas, true)) . '</pre>////');
                $xls->setCellValue('A' . $i, $nombre);
                if (isset($tramas['data'])) {
                    foreach ($tramas['data'] as $trama) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $nombre),
                            'B' => array('value' => $trama['viaje']),
                            'C' => array('value' => $trama['fecha_inicio']),
                            'D' => array('value' => $trama['fecha_fin']),
                            'E' => array('value' => $trama['tiempo']),
                            'F' => array('value' => $trama['distancia'], 'format' => '0.0'),
                            'G' => array('value' => $trama['consumo'], 'format' => '0.0'),
                            'H' => array('value' => $trama['rendimiento'], 'format' => '0.0'),
                            'I' => array('value' => $trama['max_velocidad'], 'format' => '0.0'),
                            'J' => array('value' => $trama['max_rpm'], 'format' => '0.0'),
                            'K' => array('value' => $trama['marcha_tiempo']),
                            'L' => array('value' => $trama['marcha_consumo'], 'format' => '0.0'),
                            'M' => array('value' => $trama['ralenti_tiempo']),
                            'N' => array('value' => $trama['ralenti_consumo'], 'format' => '0.0'),
                            'O' => array('value' => $trama['crucero_tiempo']),
                            'P' => array('value' => $trama['crucero_consumo'], 'format' => '0.0'),
                            'Q' => array('value' => $trama['crucero_distancia'], 'format' => '0.0'),
                            'R' => array('value' => $trama['crucero_cantidad']),
                            'S' => array('value' => $trama['carga_motor']),
                            'T' => array('value' => $trama['cant_freno']),
                            'U' => array('value' => $trama['cant_retarder']),
                            'V' => array('value' => $trama['cant_embrague']),
                        ));
                        $i++;
                    }
                }

                if (isset($consulta['mostrar_totales'])) {
                    $i = $i++;
                    $total = $tramas['totales'];
                    $xls->setRowTotales($i, array(
                        'A' => array('value' => $nombre),
                        'B' => array('value' => ''),
                        'C' => array('value' => ''),
                        'D' => array('value' => ''),
                        'B' => array('value' => $total['viaje']),
                        'E' => array('value' => $total['tiempo']),
                        'F' => array('value' => $total['distancia'], 'format' => '0.0'),
                        'G' => array('value' => $total['consumo'], 'format' => '0.0'),
                        'H' => array('value' => $total['rendimiento'], 'format' => '0.0'),
                        'I' => array('value' => $total['max_velocidad'], 'format' => '0.0'),
                        'J' => array('value' => $total['max_rpm'], 'format' => '0.0'),
                        'K' => array('value' => $total['marcha_tiempo']),
                        'L' => array('value' => $total['marcha_consumo'], 'format' => '0.0'),
                        'M' => array('value' => $total['ralenti_tiempo']),
                        'N' => array('value' => $total['ralenti_consumo'], 'format' => '0.0'),
                        'O' => array('value' => $total['crucero_tiempo']),
                        'P' => array('value' => $total['crucero_consumo'], 'format' => '0.0'),
                        'Q' => array('value' => $total['crucero_distancia'], 'format' => '0.0'),
                        'R' => array('value' => $total['crucero_cantidad']),
                        'S' => array('value' => $total['carga_motor'], 'format' => '0.0'),
                        'T' => array('value' => $total['cant_freno']),
                        'U' => array('value' => $total['cant_retarder']),
                        'V' => array('value' => $total['cant_embrague']),
                    ));
                    $i++;
                }
            }


            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=canbus_resumen.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_paso'));
        }
    }
}
