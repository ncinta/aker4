<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GMaps\Geocoder;
use App\Form\informe\InformePasoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;

class InformePasoController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $excelManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $grupoServicioManager;
    private $referenciaManager;

    private $referenciaId = array();

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        ExcelManager $excelManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->excelManager = $excelManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
    }

    /**
     * @Route("/informe/paso", name="informe_paso")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PASO_REFERENCIA")
     */
    public function pasoAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Paso");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(InformePasoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informepaso');
            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->pasoAction($request);
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //obtengo las referencias a controlar
                    if ($consulta['referencia'] == '0') {  //no considerar referencias
                        $referencias = array();
                    } elseif ($consulta['referencia'] == '-1') {    //considerar todas las referencias
                        $referencias = $this->referenciaManager->findAsociadas();
                    } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
                        $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
                    } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
                        $referencias = array();
                    } else {   //es un id de referencia.
                        $referencias[] = $this->referenciaManager->find($consulta['referencia']);
                    }

                    //armo el array con los nombres de las [id, ref.nombre] para tenerlos en memoria luego y no tener que hacer dobre consulta.
                    foreach ($referencias as $referencia) {
                        $this->referenciaId[$referencia->getId()] = $referencia->getNombre();
                    }


                    //aca se obtiene el informe.
                    $informe = array();
                    foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                        if ($servicio->getEquipo()) { //solo los habilitados
                            $informe[] = $this->procesarServicio($servicio, $periodo, $consulta, $referencias);
                        }
                    }
                }

                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Paso/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 1, $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/Paso/paso.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOPASO'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta, $referencias)
    {
        $info = $this->getDataInforme($servicio, $periodo, $referencias);

        return array(
            'servicio' => $servicio->getNombre(),
            'data' => $info,
            'count' => !is_null($info) ? count($info) : 0,
        );
    }

    public function getDataInforme($servicio, $periodo, $referencias)
    {
        $informe = $this->backendManager->informePasoReferencias($servicio->getId(), $periodo['desde'], $periodo['hasta'], $referencias);

        if ($informe) {
            foreach ($informe as $i => $trama) {
                //aca hago el procesamiento de las tramas...
                //$informe[$i]['fecha_ingreso'] = $this->utilsManager->fecha_a_local($informe[$i]['fecha_ingreso']);
                //$informe[$i]['fecha_egreso'] = $this->utilsManager->fecha_a_local($informe[$i]['fecha_egreso']);

                $informe[$i]['tiempo'] = isset($trama['duracion']) ? $this->utilsManager->segundos2tiempo($trama['duracion']) : '0';
                $informe[$i]['tiempo_anterior'] = isset($trama['duracion_anterior']) ? $this->utilsManager->segundos2tiempo($trama['duracion_anterior']) : '0';
                if (isset($trama['duracion_anterior']) && $trama['distancia_anterior'] != 0 && isset($trama['distancia_anterior'])) {
                    $mtsseg = (intval($trama['distancia_anterior'])) / (intval($trama['duracion_anterior']));
                    $informe[$i]['vel_prom'] = ($mtsseg / 1000) * 3600;
                } else {
                    $informe[$i]['vel_prom'] = 0;
                }

                //referencia
                if (isset($trama['referencia_id']) && !is_null($trama['referencia_id'])) {
                    $informe[$i]['referencia_nombre'] = $this->referenciaId[$trama['referencia_id']];
                }
            }
        }

        return $informe;
    }

    /**
     * @Route("/informe/paso/{tipo}/exportar", name="informe_paso_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PASO_REFERENCIA")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe Paso por Referencias");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Fecha Ingreso', 'width' => 20),
                'C' => array('title' => 'Fecha Egreso', 'width' => 20),
                'D' => array('title' => 'Referencia', 'width' => 80),
                'E' => array('title' => 'Duración', 'width' => 20),
                'F' => array('title' => 'Distancia', 'width' => 20),
                'G' => array('title' => 'Duración a Ant.', 'width' => 20),
                'H' => array('title' => 'Distancia a Ant.', 'width' => 20),
                'I' => array('title' => 'Vel. Prom. (km/h)', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $servicio) {   //esto es para cada servicio
                //$xls->setCellValue('A' . $i, $servicio['servicio']);
                if (isset($servicio['data'])) {
                    foreach ($servicio['data'] as $key => $row) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $servicio['servicio']),
                            'B' => array('value' => $row['fecha_ingreso']),
                            'C' => array('value' => $row['fecha_egreso']),
                            'E' => array('value' => $row['tiempo'], 'format' => '[HH]:MM:SS'),
                            'F' => array('value' => $row['distancia']),
                            'G' => array('value' => $row['tiempo_anterior'], 'format' => '[HH]:MM:SS'),
                            'H' => array('value' => $row['distancia_anterior']),
                            'I' => array('value' => $row['vel_prom'], 'format' => '#0.00'),
                        ));
                        if (isset($row['referencia_nombre'])) {
                            $xls->setRowValues($i, array(
                                'D' => array('value' => isset($row['referencia_nombre']) ? $row['referencia_nombre'] : ''),
                            ));
                        }
                        $i++;
                    }
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=pasoreferencias.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_paso'));
        }
    }
}
