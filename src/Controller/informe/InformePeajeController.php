<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\PortalPorEquipoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;

class InformePeajeController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $excelManager;
    private $grupoServicioManager;
    private $referenciaManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ExcelManager $excelManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/informe/peaje", name="informe_peaje")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_PEAJES")
     */
    public function peajeAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Peaje");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(PortalPorEquipoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $consulta = $request->get('portalporequipo');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->peajeAction($request);
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                    //aca se obtiene el informe.
                    if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //para toda la flota
                        if ($consulta['servicio'] == '0') {
                            $servicios = $this->servicioManager->findAllByUsuario();
                            $consulta['servicionombre'] = 'toda la flota';
                        } else {
                            $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
                            $consulta['servicionombre'] = 'grupo "' . $grupo->getNombre() . '"';
                            $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                        }

                        //controlo que haya refernecias asignadas.
                        $ref = (array) $this->referenciaManager->findAsociadas();
                        if (count($ref) == 0) {
                            $this->get('session')->getFlashBag()->add('error', 'No tiene asignado referencias. Verifique por favor.');
                            $this->breadcrumbManager->pop();
                            return $this->redirect($this->generateUrl('informe_peaje'));
                        }
                        unset($ref);

                        $informe = array();
                        $total_flota = 0;
                        foreach ($servicios as $servicio) {
                            $total = 0;
                            if ($servicio->getVehiculo() != null && $servicio->getVehiculo()->getTipoVehiculo() != null) {
                                //obtengo todos los precios de los portales para el servicio.
                                $portales = $this->getEntityManager()
                                    ->getRepository('App:PrecioPortal')
                                    ->findByTipoVehiculo(1, $servicio->getVehiculo()->getTipoVehiculo());
                                $consulta_informe = $this->backendManager->informePortales($servicio->getId(), $desde, $hasta, $portales);
                                $portales_map = array();
                                foreach ($portales as $portal) {
                                    $portales_map[$portal->getId()] = $portal;
                                }
                                $cantidad = 0;
                                //while ($entrada = $consulta_informe->fetch()) {
                                foreach ($consulta_informe as $entrada) {
                                    $portal = $portales_map[$entrada["precioportal_id"]];
                                    $cantidad++;
                                    $total += intval($portal->getPrecio());
                                }
                                $informe[] = array(
                                    'servicio' => $servicio->getNombre(),
                                    'tipovehiculo' => $servicio->getVehiculo()->getTipovehiculo()->getTipo(),
                                    'cantidad' => $cantidad,
                                    'total' => $total,
                                );
                            } else {
                                $informe[] = array(
                                    'servicio' => $servicio->getNombre(),
                                    'tipovehiculo' => '---',
                                    'cantidad' => 0,
                                    'total' => 0,
                                );
                            }
                            $total_flota += $total;
                        }
                    } else {   //esto es para un solo equipo
                        $servicio = $this->servicioManager->find($consulta['servicio']);
                        $consulta['servicionombre'] = $servicio->getNombre();
                        if ($servicio->getVehiculo() != null && $servicio->getVehiculo()->getTipoVehiculo() != null) {
                            //obtengo los portales para este vehiculo
                            $portales = $this->getEntityManager()
                                ->getRepository('App:PrecioPortal')
                                ->findByTipoVehiculo(1, $servicio->getVehiculo()->getTipovehiculo());

                            $consulta_informe = $this->backendManager->informePortales(intval($consulta['servicio']), $desde, $hasta, $portales);
                            $informe = array();
                            $portales_map = array();
                            foreach ($portales as $portal) {
                                $portales_map[$portal->getId()] = $portal;
                            }
                            $total = 0;
                            foreach ($consulta_informe as $entrada) {
                                $portal = $portales_map[$entrada["precioportal_id"]];
                                $entrada["portal"] = $portal->getReferencia()->getNombre();
                                $entrada["precio"] = $portal->getPrecio();
                                $entrada["tipovehiculo"] = $servicio->getVehiculo()->getTipovehiculo()->getTipo();
                                $informe[] = $entrada;
                                $total += intval($entrada["precio"]);
                            }
                        } else {
                            $informe = array();
                            $total = null;
                        }
                    }
                }
            } else {
                $informe = NULL;
                $total = NULL;
            }

            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {
                        return $this->render('informe/Peaje/pantalla_flota.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'total' => $total_flota
                        ));
                    } else {
                        return $this->render('informe/Peaje/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'total' => $total
                        ));
                    }
                    break;
                case '5': //sale a excel
                    return $this->exportarAction(
                        $request,
                        $consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g' ? 1 : 0,
                        $consulta,
                        $informe
                    );

                    break;
            }
        }
        return $this->render('informe/Peaje/peaje.html.twig', array(
            'form' => $form->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFOPEAJES'),
        ));
    }

    /**
     * @Route("/informe/peaje/{tipo}/exportar", name="informe_peaje_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        $total = json_decode($request->get('total'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Control de peajes de rutas");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //es para un solo servicio.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Fecha', 'width' => 20),
                    'B' => array('title' => 'Portal', 'width' => 30),
                    'C' => array('title' => 'Precio ($)', 'width' => 20),
                    'D' => array('title' => 'Velocidad (km/h)', 'width' => 20),
                ));
                $xls->setCellValue('D2', $consulta['servicionombre']);
                $i = 7;

                foreach ($informe as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['fecha']),
                        'B' => array('value' => $value['portal']),
                        'C' => array('value' => $value['precio'], 'format' => '#.00'),
                        'D' => array('value' => $value['velocidad'], 'format' => '0'),
                    ));
                    $i++;
                }
            } else {   //es para la flota.
                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Tarifa', 'width' => 20),
                    'C' => array('title' => 'Cantidad', 'width' => 20),
                    'D' => array('title' => 'Total', 'width' => 20),
                ));
                $i = 7;
                foreach ($informe as $value) {   //esto es para cada servicio
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['servicio']),
                        'B' => array('value' => $value['tipovehiculo']),
                        'C' => array('value' => $value['cantidad'], 'format' => '0'),
                        'D' => array('value' => $value['total'], 'format' => '#0.00'),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=preciopeajes.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_peaje'));
        }
    }
}
