<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeManejoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\IbuttonManager;
use App\Model\app\HistoricoIbuttonManager;
use App\Model\app\ChoferManager;
use App\Model\app\GeocoderManager;

/**
 * Description of InformeManejoController
 *
 * @author nicolas
 */
class InformeManejoController extends AbstractController
{

    private $moduloChofer;
    private $user;
    protected $userloginManager;
    protected $servicioManager;
    protected $loggerManager;
    protected $utilsManager;
    protected $breadcrumbManager;
    protected $informeUtilsManager;
    protected $backendManager;
    protected $organizacionManager;
    protected $grupoServicioManager;
    protected $referenciaManager;
    protected $ibuttonManager;
    protected $histIbuttonManager;
    protected $choferManager;
    protected $excelManager;
    protected $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        IbuttonManager $ibuttonManager,
        HistoricoIbuttonManager $histIbuttonManager,
        ChoferManager $choferManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->histIbuttonManager = $histIbuttonManager;
        $this->choferManager = $choferManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/manejo/{id}/manejo", name="informe_manejo",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANEJO")
     */
    public function manejoAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de manejo");
        //traigo todo los chofere existentes para esta organizacion
        $organizacion = $this->organizacionManager->find($id);
        $options = array(
            'choferes' => $this->choferManager->findAll($organizacion),
            'servicios' => $this->servicioManager->findAll($organizacion),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($organizacion),
        );

        $form = $this->createForm(InformeManejoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->user = $this->getUser2Organizacion($id, $this->userloginManager, $this->organizacionManager);
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('manejo');

            if ($consulta) {
                $choferes = null;
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                if ($consulta['chofer'] != 0) {
                    $chofer = $this->choferManager->findById($consulta['chofer']);
                    $choferes = array($chofer);
                    $consulta['chofernombre'] = $chofer->getNombre();
                } else {
                    $choferes = $this->choferManager->findAll($organizacion);
                    $consulta['chofernombre'] = 'Todos los Choferes';
                }
                $consulta['servicionombre'] = $arrServicios['servicionombre'];
                $this->moduloChofer = $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_CHOFER');
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                if ($hasta <= $desde) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_manejo', array('id' => $id)));
                }
                $referencias = $this->referenciaManager->findAsociadas($organizacion);
                $result = $this->detalles($choferes, $arrServicios, $referencias, $desde, $hasta);

                $resultTotal = $this->totales($result);
                $this->loggerManager->logInforme($consulta);
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
            }
            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    return $this->render('informe/Manejo/pantalla.html.twig', array(
                        'consulta' => $consulta,
                        'result' => $resultTotal,
                        'moduloChofer' => $this->moduloChofer,
                        'time' => $this->loggerManager->getTimeGeneracion(),
                    ));
                    break;
                case '5':
                    return $this->exportarAction($request, $organizacion, $resultTotal, $consulta);
                    break;
            }
        }
        return $this->render('informe/Manejo/manejo.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/informe/manejo/{tipo}/exportar", name="informe_manejo_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_MANEJO")
     */
    public function exportarAction(Request $request, $organizacion, $informe, $consulta)
    {
     

        if ($informe != null) {
            $xls = $this->excelManager->create("Informe de Manejo");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Chofer',
                'D3' => $consulta['chofernombre'],
                'C4' => 'Servicio',
                'D4' => $consulta['servicionombre'],
                'C5' => 'Fecha desde',
                'D5' => $consulta['desde'],
                'C6' => 'Fecha hasta',
                'D6' => $consulta['hasta'],
            ));
            $i = 7;
            //esto es para el resumen totalizado de infracciones.
            $xls->setBar($i, array(
                'A' => array('title' => 'Chofer', 'width' => 20),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Inicio', 'width' => 20),
                'D' => array('title' => 'Referencia', 'width' => 20),
                'E' => array('title' => 'Dirección', 'width' => 40),
                'F' => array('title' => 'Fin', 'width' => 20),
                'G' => array('title' => 'Referencia', 'width' => 20),
                'H' => array('title' => 'Dirección', 'width' => 40),
                'I' => array('title' => 'Distacia', 'width' => 10),
                'J' => array('title' => 'Duración', 'width' => 10),
            ));
            $i++;
            foreach ($informe as $chofer => $detalle) {   //recorro todo el detalle para un servicio
                foreach ($detalle as $key => $value) {   //recorro todo el detalle para un servicio
                    if (!is_null($value['servicio']) && $key != 'totales') {
                        $xls->setCellValue('A' . $i, $chofer);
                        $xls->setCellValue('B' . $i, $value['servicio']);
                        $xls->setCellValue('C' . $i, $value['inicio']['fecha']);
                        $xls->setCellValue('D' . $i, isset($value['inicio']['str_referencia']) ? $value['inicio']['str_referencia'] : '-----');
                        $xls->setCellValue('E' . $i, $value['inicio']['domicilio']);
                        $xls->setCellValue('F' . $i, $value['fin']['fecha']);
                        $xls->setCellValue('G' . $i, isset($value['fin']['str_referencia']) ? $value['fin']['str_referencia'] : '-----');
                        $xls->setCellValue('H' . $i, $value['fin']['domicilio']);
                        $xls->setCellValue('I' . $i, $value['distancia_simple']);
                        $xls->setCellValue('J' . $i, $value['duracion_simple']);
                        $i++;
                    }
                }
                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                $xls->setCellValue('H' . $i, 'TOTAL ===>');
                $xls->setCellValue('I' . $i, $detalle['totales']['distancia_total']);
                $xls->setCellValue('J' . $i, $detalle['totales']['duracion_total']);
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=choferes.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_manejo', array('id' => $organizacion->getId())));
        }
    }

    private function getUser2Organizacion($id, $userloginManager, $organizacionManager)
    {
        $user = $userloginManager->getUser();
        $organizacion = $organizacionManager->find($id);
        if ($organizacion != $userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    /**
     * 
        ### Función: `detalles($choferes, $arrServicios, $referencias, $desde, $hasta)`

        Esta función genera un informe detallado sobre los servicios realizados por los choferes en un período de tiempo especificado. El proceso incluye:

        1. **Identificación de choferes**: Para cada chofer en la lista, se obtiene el identificador `ibutton`, si está disponible.
        
        2. **Procesamiento de servicios**: Para cada servicio en la lista de servicios proporcionada, se consulta el backend para obtener los datos relacionados con dicho servicio para el chofer en el rango de fechas especificado.
        
        3. **Geocodificación**:
        - Si los datos del servicio incluyen posiciones de inicio o fin, se obtiene la dirección correspondiente utilizando un servicio de geocodificación inversa.
        - Si existen referencias asociadas a la posición de inicio o fin, se recupera el nombre de la referencia.

        4. **Cálculos adicionales**:
        - La distancia se convierte de metros a kilómetros.
        - La duración del servicio se convierte de segundos a un formato de tiempo legible.

        5. **Validación de histórico**:
        - Se verifica si el chofer estuvo activo durante el período del servicio comparando los datos históricos del `ibutton`.
        - Si hay una coincidencia entre el chofer y el servicio, los datos se almacenan en el resultado final.

        6. **Resultado**: Devuelve un array con los choferes como claves, y para cada uno, una lista de los servicios correspondientes, incluyendo datos de distancia, duración, direcciones, y referencias.

        ---

        Esta función es útil para generar reportes detallados de las actividades de los choferes en un período de tiempo determinado, permitiendo realizar un análisis exhaustivo de los servicios realizados.
     */
    private function detalles($choferes, $arrServicios, $referencias, $desde, $hasta)
    {
        //dd($choferes);
        $result = array();
        $ibuttons = array();
        foreach ($choferes as $chofer) {
            $ibuttons[] = $chofer->getIbutton() ? $chofer->getIbutton()->getDallas() : null;
            foreach ($arrServicios['servicios'] as $servicio) {                
                $backend = $this->backendManager->informeChofer($servicio->getId(), $desde, $hasta, $referencias, $ibuttons);                             
                foreach ($backend as $data) {                  
                   // dd($data); 
                    $data['servicio'] = $servicio->getNombre();
                    
                    //domicilio
                    if (isset($data['inicio']['posicion'])) {
                        $direc = $this->geocoderManager->inverso($data['inicio']['posicion']['latitud'], $data['inicio']['posicion']['longitud']);
                        $data['inicio']['domicilio'] = $direc;
                    }
                    
                    //referencia
                    if (isset($data['inicio']['referencia_id']) && !is_null($data['inicio']['referencia_id'])) {
                        $referencia = $this->referenciaManager->find($data['inicio']['referencia_id']);
                        if (!is_null($referencia)) {
                            $data['inicio']['str_referencia'] = $referencia->getNombre();
                        }
                    }
                    
                    if (isset($data['fin']['posicion'])) {
                        $direc = $this->geocoderManager->inverso($data['fin']['posicion']['latitud'], $data['fin']['posicion']['longitud']);
                        $data['fin']['domicilio'] = $direc;
                    }
                    
                    //referencia
                    if (isset($data['fin']['referencia_id']) && !is_null($data['fin']['referencia_id'])) {
                        $referencia = $this->referenciaManager->find($data['fin']['referencia_id']);
                        if (!is_null($referencia)) {
                            $data['fin']['str_referencia'] = $referencia->getNombre();
                        }
                    }
                    $data['distancia_simple'] = round(($data['distancia'] / 1000), 2);  //pasamos a km la distancia 
                    $data['duracion_simple'] = $this->utilsManager->segundos2tiempo(intval($data['duracion']));
                    
                    $ibutton = $this->ibuttonManager->findByDallas($data['ibutton']);
                    $inicio = new \DateTime($data['inicio']['fecha']);
                    $fin = new \DateTime($data['fin']['fecha']);
                    $historico = $this->histIbuttonManager->findByIbuttonChoferFecha($ibutton, $chofer, $inicio, $fin);
                    if (!is_null($historico)) {
                        $result[$chofer->getNombre()][] = $data;
                    } else {
                        $chof = $this->choferManager->findByIbuttonFecha($ibutton, $inicio);
                        if (!is_null($chof) && $chof->getId() == $chofer->getId()) {
                            $result[$chofer->getNombre()][] = $data;
                        }
                    }
                }
            }
        }
        return $result;
    }
    
    public function totales($result)
    {
        $servicio = null;
        foreach ($result as $id => $chofer) {
            $distanciaTotal = 0;
            $duracionTotal = 0;
            foreach ($chofer as $data) {
                $distanciaTotal += $data['distancia'];
                $duracionTotal += $data['duracion'];
                $servicio = $data['servicio'];
            }
            $result[$id]['totales'] = array(
                'distancia_total' => round(($distanciaTotal / 1000), 2),
                'duracion_total' => $this->utilsManager->segundos2tiempo(intval($duracionTotal)),
                'servicio' => $servicio
            );
        }
        
       // die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
        return $result;
    }
}
