<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeDistanciasType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;

class InformeDistanciasController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $excelManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        ExcelManager $excelManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/informe/{id}/distancias", name="informe_distancias",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function distanciasAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Distancias");

        //traigo el usuario master o el userlogin dependiendo de donde este.
        $user = $this->getUser2Organizacion($id);

        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($user),
            'grupos' => $this->grupoServicioManager->findAsociadas($user),
            'calcular_distancias' => $this->userloginManager->isGranted('ROLE_APMON_INFORME_DISTANCIA_PUNTOS'),
        );

        $form = $this->createForm(InformeDistanciasType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();

            $consulta = $request->get('informedistancias');
            if ($consulta) {
                $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
                $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
                $mostrartotal = isset($consulta['mostrar_totales']) ? $consulta['mostrar_totales'] : false;
                $desglosar = isset($consulta['mostrar_desglosado']) ? $consulta['mostrar_desglosado'] : false;
            } else {
                $informe = false;
            }

            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_distancias', array('id' => $id)));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                //armo los períodos que debo tener en cuenta.
                $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], $desglosar);

                //traigo el usuario master o el userlogin dependiendo de donde este.
                $user = $this->getUser2Organizacion($id);

                //obtengo los servicios a incluir en el informe.
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $user);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];

                //aca se obtiene el informe.
                $informe = array();
                foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                    if ($servicio->getEquipo()) {
                        $informe[] = $this->procesarServicio($servicio, $periodo, $consulta, $mostrartotal);
                    }
                }
                // die('////<pre>' . nl2br(var_export(json_encode($informe,true), true)) . '</pre>////');
                //aca se redirige al destino correspondiente.
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Distancias/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'organizacion' => $user->getOrganizacion(),
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '2':  //sale a grafico.
                        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {
                            $template = 'informe/Distancias/grafico_flota.html.twig';
                        } else {
                            //obtengo el servicio
                            $consulta['servicionombre'] = $this->servicioManager->find(intval($consulta['servicio']))->getnombre();
                            $template = 'informe/Distancias/grafico.html.twig';
                        }
                        //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                        return $this->render(
                            $template,
                            array(
                                'consulta' => $consulta,
                                'organizacion' => $user->getOrganizacion(),
                                'time' => $this->loggerManager->getTimeGeneracion(),
                                'dt' => $this->getGraficoInforme($consulta, $informe)
                            )
                        );
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 1, $consulta, $informe);
                        break;
                }
            }
        }
        return $this->render('informe/Distancias/distancias.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $user->getOrganizacion(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_INFODISTANCIAS'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta, $mostrartotal)
    {
        $info = $this->getDataInforme($servicio, $periodo, $consulta);
        return array(
            'servicio' => $servicio->getNombre(),
            'data' => $info,
            'totales' => $this->getTotalesInforme($info, $mostrartotal), //los totales...
        );
    }

    /**
     * @Route("/informe/distancias/{tipo}/exportar", name="informe_distancias_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_DISTANCIAS")
     */
    public function exportarAction(Request $request,  $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Distancias Recorridas");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));


            //se hace la barra principal
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Primer Reporte', 'width' => 20),
                'C' => array('title' => 'Segundo Reporte', 'width' => 20),
                'D' => array('title' => 'Tiempo activo (horas)', 'width' => 20),
                'E' => array('title' => 'Distancia recorrida (kms)', 'width' => 25),
                'F' => array('title' => 'Promedio diario (kms)', 'width' => 20),
                'G' => array('title' => 'Horóm. Inicial', 'width' => 15),
                'H' => array('title' => 'Horóm. Final', 'width' => 15),
                'I' => array('title' => 'Diferencia Horómetro', 'width' => 15),
            ));
            $i = 7;

            foreach ($informe as $datos) {   //esto es para cada servicio
                //$xls->setCellValue('A' . $i, $datos['servicio']);
                //die('////<pre>' . nl2br(var_export(json_encode($informe), true)) . '</pre>////');
                foreach ($datos['data'] as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => $value['desde']),
                        'C' => array('value' => $value['hasta']),
                        'D' => array('value' => $value['tiempo_activo'], 'format' => '[h]:mm:ss'),
                        'E' => array('value' => intval($value['km_total']), 'format' => '0.00'),
                        'F' => array('value' => intval($value['km_por_dia']), 'format' => '0.00'),
                        'G' => array('value' => $value['horometro_inicial']),
                        'H' => array('value' => $value['horometro_final']),
                        'I' => array('value' => $value['horometro_diff']),
                    ));
                    $i++;
                }
                if (isset($consulta['mostrar_totales'])) {
                    //aca exporto el total para el vehiculo
                    $xls->setRowTotales($i, array(
                        'A' => array('value' => $datos['servicio']),
                        'B' => array('value' => ''),
                        'C' => array('value' => ''),
                        'D' => array('value' => $datos['totales']['tiempo_activo'], 'format' => '[h]:mm:ss'),
                        'E' => array('value' => intval($datos['totales']['km_total']), 'format' => '0.00'),
                        'F' => array('value' => intval($datos['totales']['km_por_dia']), 'format' => '0.00'),
                        'G' => array('value' => ''),
                        'H' => array('value' => ''),
                        'I' => array('value' => ''),
                    ));
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=distancias.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_distancias'));
        }
    }

    /**
     * Obitene la información desde el historial del servicio para la consulta
     * y período dado. Además calcula los campos restantes que surgen a partir 
     * de los que tiene el historial.
     * @param Servicio $servicio
     * @param array $periodo
     * @param array $consulta
     * @return array informe para el servicio y período seleccionado segun consulta 
     */
    public function getDataInforme($servicio, $periodo, $consulta)
    {
        $informe = array();
        //pido toda la info al backend.
        foreach ($periodo as $key => $value) {
            if ($servicio->getUltFechahora() >= $value['desde']) {    //evito q se consulte para equipos q no reportan en el periodo
                $informe[] = $this->backendManager->informeDistancias($servicio->getId(), $value['desde'], $value['hasta'], $this->referenciaManager->findAllVisibles());
            }
        }
        //termino de procesar y agregar nuevos campos calculados al informe.
        foreach ($informe as $key => $value) {
            $informe[$key]['tiempo_activo'] = $this->utilsManager->segundos2tiempo(intval($informe[$key]['segundos_activo']));
            $informe[$key]['diferencia_kms'] = abs(intval($informe[$key]['km_total']) - intval($informe[$key]['kms_calculada']));
            $informe[$key]['informar_kms'] = max(intval($informe[$key]['km_total']), intval($informe[$key]['kms_calculada'])) * 0.05 < $informe[$key]['diferencia_kms'];
            // $informe[$key]['km_total'] = 0;
            // $informe[$key]['km_por_dia'] = 0;
            if (isset($consulta['calcular_distancias']) && $consulta['calcular_distancias'] == "1") {
                $informe[$key]['km_total'] = intval($informe[$key]['kms_calculada']);
                $informe[$key]['km_por_dia'] = intval($informe[$key]['kms_por_dia_calculada']);
            }
            $informe[$key]['horometro_inicial'] = $this->utilsManager->minutos2tiempo(intval($value['horometro_inicial']));
            $informe[$key]['horometro_final'] = $this->utilsManager->minutos2tiempo(intval($value['horometro_final']));
            $informe[$key]['horometro_diff'] = $this->utilsManager->minutos2tiempo(intval($value['horometro_final']) - intval($value['horometro_inicial']));
        }

        return $informe;
    }

    public function getGraficoInforme($consulta, $informe)
    {
        $myArray = array();
        if ($consulta['servicio'] == '0' || substr($consulta['servicio'], 0, 1) == 'g') {  //esto es para la flota.
            foreach ($informe as $key => $servicio) {
                //die('////<pre>' . nl2br(var_export($servicio, true)) . '</pre>////');
                if (isset($servicio['data'][0])) {
                    $value = $servicio['data'][0];
                    $myArray[$key]['servicio'] = $servicio['servicio'];
                    $myArray[$key]['tiempo_activo'] = isset($value['tiempo_activo']) ? $this->utilsManager->tiempo2horas($value['tiempo_activo']) : 0;
                    $myArray[$key]['km_total'] = isset($value['km_total']) ? $value['km_total'] : 0;
                    $myArray[$key]['km_por_dia'] = isset($value['km_por_dia']) ? $value['km_por_dia'] : 0;
                }
            }
            return $myArray;
        } else {  //esto es para un solo equipo
            foreach ($informe[0]['data'] as $key => $value) {
                $myArray[$key]['fecha'] = substr($value['desde'], 8, 2) . '-' . substr($value['desde'], 5, 2);
                $myArray[$key]['tiempo_activo'] = $this->utilsManager->tiempo2horas($value['tiempo_activo']);
                $myArray[$key]['km_total'] = $value['km_total'];
            }

            return $myArray;
        }
    }

    public function getTotalesInforme($informe, $mostrartotal)
    {
        $total = array(
            'segundos_activo' => 0,
            'tiempo_activo' => 0,
            'km_total' => 0,
            'km_por_dia' => 0,
        );
        if ($informe && $mostrartotal) {
            foreach ($informe as $value) {
                $total['segundos_activo'] += intval($value['segundos_activo']);
                $total['km_total'] += intval($value['km_total']);
                $total['km_por_dia'] += intval($value['km_por_dia']);
            }
            $total['tiempo_activo'] = $this->utilsManager->segundos2tiempo(intval($total['segundos_activo']));
        }
        return $total;
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }
}
