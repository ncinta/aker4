<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeRalentiChoferType;
use App\Entity\Organizacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ChoferManager;
use App\Model\app\IbuttonManager;
use App\Model\app\GeocoderManager;


class InformeRalentiChoferController extends AbstractController
{

    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $geocoderManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $choferManager;
    private $ibuttonManager;
    private $excelManager;

    function __construct(
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        GeocoderManager $geocoderManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager,
        ChoferManager $choferManager,
        IbuttonManager $ibuttonManager,
        ExcelManager $excelManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->geocoderManager = $geocoderManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->choferManager = $choferManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/informe/ralenti/{id}/chofer", name="informe_ralenti_chofer",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function ralentiAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de ralentí por chofer");
        //traigo todo los servicios existentes.
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $options['choferes'] = $this->choferManager->findAll($organizacion);
        $form = $this->createForm(InformeRalentiChoferType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informeralenti');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);

            //paso la fecha a formato yyyy-mm-dd
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            if ($hasta <= $desde) {
                $this->get('session')->setFlash('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //armo los períodos que debo tener en cuenta.
                $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                //obtengo los servicios a incluir en el informe.
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];
                $chofer = (intval($consulta['chofer'])) == 0 ? null : $this->choferManager->find($consulta['chofer']);
                $consulta['chofernombre'] = !is_null($chofer) ? $chofer->getNombre() : 'Todos';
                //aca se obtiene el informe.
                $informe = array();
                foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                    $ralentis[] = $this->procesarServicio($servicio, $periodo, $consulta);
                }
                foreach ($ralentis as $ralenti) {
                    foreach ($ralenti['data'] as $data) {  //recorro los excesos
                        $ibutton = $this->ibuttonManager->findByCodigo($data['ibutton']);
                        if (!is_null($ibutton)) {
                            $choferS = is_null($ibutton->getChofer()) ? $ibutton->getCodigo() : $ibutton->getChofer()->getNombre();
                            $data['servicio'] = $servicio->getNombre();
                            $ralenti['ibutton'] = $choferS;
                            if ($chofer == null) { //toda la flota de choferes
                                $informe[$choferS] = $ralenti;
                            } else {
                                if ($ibutton->getCodigo() == $chofer->getIbutton()->getCodigo()) { //solamente uno
                                    $informe[$choferS] = $ralenti;
                                }
                            }
                        }
                    }
                }
                $this->loggerManager->logInforme($consulta);

                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/RalentiChofer/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));

                        break;
                }
            }
        }

        return $this->render('informe/RalentiChofer/ralenti.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function procesarServicio($servicio, $periodo, $consulta)
    {
        $info = $this->getDataInforme($servicio, $periodo, $consulta['tiempo_minimo'] * 60, $consulta['mostrar_direcciones']);
        return array(
            'servicio' => $servicio->getNombre(),
            'count' => count($info),
            'data' => $info,
            'totales' => $this->getTotalesInforme($info), //los totales...
        );
    }

    /**
     * @Route("/informe/ralenti/{tipo}/chofer/exportar", name="informe_ralenti_chofer_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CHOFER")
     */
    public function exportarAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Detenciones en Ralenti");

            $xls->setHeaderInfo(array(
                'C2' => 'Chofer',
                'D2' => $consulta['chofernombre'],
                'C3' => 'Servicio',
                'D3' => $consulta['servicionombre'],
                'C4' => 'Fecha desde',
                'D4' => $consulta['desde'],
                'C5' => 'Fecha hasta',
                'D5' => $consulta['hasta'],
            ));

            $xls->setBar(7, array(
                'A' => array('title' => 'Chofer', 'width' => 20),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Fecha Inicio', 'width' => 20),
                'D' => array('title' => 'Fecha Fin', 'width' => 20),
                'E' => array('title' => 'Duración', 'width' => 20),
                'F' => array('title' => 'Referencia', 'width' => 40),
                'G' => array('title' => 'Direccion', 'width' => 80),
            ));

            $totales = json_decode($request->get('totales'), true);
            $i = 8;
            foreach ($informe as $datos) {
                foreach ($datos['data'] as $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['ibutton']),
                        'B' => array('value' => $datos['servicio']),
                        'C' => array('value' => $value['fecha_inicio']),
                        'D' => array('value' => $value['fecha_fin']),
                        'E' => array('value' => $value['tiempo_ralenti']),
                    ));
                    if (isset($value['referencia_nombre'])) {
                        $xls->setRowValues($i, array(
                            'F' => array('value' => isset($value['referencia_nombre']) ? $value['referencia_nombre'] : ''),
                        ));
                    }
                    if (isset($value['domicilio'])) {
                        $xls->setRowValues($i, array(
                            'G' => array('value' => isset($value['domicilio']) ? $value['domicilio'] : ''),
                        ));
                    }
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=ralenti.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_ralenti_chofer'));
        }
    }

    private function getDataInforme($servicio, $periodo, $tiempo_minimo, $obtener_direccion)
    {

        $referencias = $this->referenciaManager->findAsociadas();
        $informe = array();
        foreach ($periodo as $key => $value) {
            $data = $this->backendManager->informeRalenti($servicio->getId(), $value['desde'], $value['hasta'], $tiempo_minimo, $referencias);
            if ($data) {
                $informe = array_merge($informe, $data);
            }
            unset($data);
        }
        $id = 0;
        foreach ($informe as $key => $value) {
            $informe[$key]['detencion_id'] = $key + 1;
            $informe[$key]['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo($value['duracion']);
            //proceso si es en una referencia.
            if ($value['referencia_id'] != null) {
                $referencia = $this->referenciaManager->find($value['referencia_id']);
                if ($referencia) {
                    $informe[$key]['referencia_nombre'] = $referencia->getNombre();
                }
            }
            //obtengo la direccion siempre que se solicite.
            if ($obtener_direccion) {
                $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                $informe[$key]['domicilio'] = $this->utilsManager->simplexml2array($direc);
            }
        }
        return $informe;
    }

    public function getTotalesInforme($informe)
    {
        $total = array(
            'tiempo_ralenti' => 0,
            'duracion' => 0,
        );
        foreach ($informe as $value) {
            $total['duracion'] += $value['duracion'];
        }
        $total['tiempo_ralenti'] = $this->utilsManager->segundos2tiempo($total['duracion']);
        return $total;
    }
}
