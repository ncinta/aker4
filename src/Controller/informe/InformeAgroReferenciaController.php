<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\informe\AgroReferenciaType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;

class InformeAgroReferenciaController extends AbstractController
{

    private $user;
    private $userloginManager;
    private $organizacionManager;
    private $breadcrumbManager;
    private $servicioManager;
    private $referenciaManager;
    private $grupoServicioManager;
    private $utilsManager;
    private $excelManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $backendManager;

    function __construct(
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager,
        ExcelManager $excelManager,
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoServicioManager $grupoServicioManager,
        UtilsManager $utilsManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->organizacionManager = $organizacionManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->utilsManager = $utilsManager;
        $this->excelManager = $excelManager;
        $this->loggerManager = $loggerManager;
        $this->backendManager = $backendManager;
        $this->informeUtilsManager = $informeUtilsManager;
    }

    /**
     * @Route("/agro/info/{id}/referencia", name="informe_agro_referencia",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_AGRO_INFORME_REFERENCIA")
     */
    public function referenciaAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Trabajo en Referencia");

        //traigo el usuario master o el userlogin dependiendo de donde este.
        $this->user = $this->getUser2Organizacion($id);

        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($this->user),
            'referencias' => $this->referenciaManager->findAsociadas($this->user),
            'grupos_referencias' => $this->referenciaManager->findAllGrupos($this->user),
            'grupos' => $this->grupoServicioManager->findAsociadas($this->user)
        );


        $form = $this->createForm(AgroReferenciaType::class, null, $options);

        return $this->render('informe/AgroReferencia/referencia.html.twig', array(
            'organizacion' => $this->user->getOrganizacion(),
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/agro/info/{id}/referencia/generar", name="informe_agro_referencia_generar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_AGRO_INFORME_REFERENCIA")
     */
    public function generarAction(Request $request, $id)
    {

        $this->loggerManager->setTimeInicial();
        $countVP = 0;
        $countVM = 0;
        //traigo el usuario master o el userlogin dependiendo de donde este.
        $this->user = $this->getUser2Organizacion($id);
        $consulta = $request->get('informeref');

        if ($consulta) {
            //paso la fecha a formato yyyy-mm-dd
            $periodo = array(
                'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
            );
            if ($periodo['hasta'] <= $periodo['desde']) {
                $this->setFlash('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_agro_referencia_generar', array('id' => $id)));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //establesco sobre que referencias debo trabajar.
                if ($consulta['referencia'] == '-1') {    //considerar todas las referencias
                    $referencias = $this->referenciaManager->findAsociadas();
                } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
                    $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
                } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
                    $referencias = array();
                } else {   //es un id de referencia.
                    $referencias[] = $this->referenciaManager->find($consulta['referencia']);
                }

                //casteo las referencias solo para tener el nombre e id.
                $castRef = array();
                foreach ($referencias as $ref) {
                    $castRef[$ref->getId()] = $ref->getNombre();
                }

                $mostrartotal = isset($consulta['mostrar_totales']) ? true : false;
                $informe = array();
                //obtengo los servicios a incluir en el informe.
                $arrServ = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                $consulta['servicionombre'] = $arrServ['servicionombre'];

                //PASO 1: Obtengo los datos de los servicios y los ordeno por referencia.
                $arrServicios = array();
                $arrTotal = array();
                foreach ($arrServ['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                    if ($servicio->getUltFechahora() >= $periodo['desde']) {    //evito q se consulte para equipos q no reportan en el periodo
                        $dataRef = array();
                        $tOut = $consulta['melga'] * 60;
                        $ancho = isset($consulta['ancho']) ? floatval(str_replace(',', '.', $consulta['ancho'])) : 0;
                        $consulta['tolerancia'] = $consulta['melga'] . ' min';

                        ///obtengo los datos del servicio.
                        $dataTmp = $this->backendManager->informePasoReferencias($servicio->getId(), $periodo['desde'], $periodo['hasta'], $referencias);
                        //$dataTmp = $this->t;

                        if (count($dataTmp) > 0) {
                            //ordeno las tramas por referencias.
                            $dataRef = array();
                            foreach ($dataTmp as $i => $pos) {
                                //borro campos que no uso.
                                unset($pos['latitud']);
                                unset($pos['longitud']);

                                $dataRef[$pos['referencia_id']][] = $pos;
                            }
                            //en dataRef tengo por referencia las i/o ordenadas
                            unset($dataTmp);

                            foreach ($dataRef as $i => $ref) {   //esto lo hago para cada referencia ya que las tengo ordenadas
                                //debo recorrer las referencias juntando en arrPasos las que tienen el tout menor.
                                $arrPasos = array();
                                //die('////<pre>' . nl2br(var_export($ref, true)) . '</pre>////');
                                foreach ($ref as $x => $pos) {        //esto es para cada entrada/salida
                                    unset($pos['distancia']);
                                    unset($pos['duracion']);
                                    unset($pos['tiempo']);
                                    unset($pos['segundos']);

                                    $cc = count($arrPasos);

                                    if ($pos['fecha_ingreso'] != $pos['fecha_egreso']) {
                                        if ($x == 0) {  //primer paso
                                            $ant = 0;
                                            $arrPasos[] = array('fecha_ingreso' => $pos['fecha_ingreso'], 'fecha_egreso' => $pos['fecha_egreso']);
                                        } else {
                                            if ($cc > 0) {
                                                //   die('////<pre>' . nl2br(var_export($cc, true)) . '</pre>////');
                                                //                                            $ini = new \DateTime($arrPasos[$cc - 1]['fecha_ingreso']);
                                                $fin = new \DateTime($arrPasos[$cc - 1]['fecha_egreso']);
                                                $fpos = new \DateTime($pos['fecha_ingreso']);
                                                $dif = $fpos->diff($fin)->format('%h') * 3600 + $fpos->diff($fin)->format('%i') * 60 + $fpos->diff($fin)->format('%s');
                                                if ($dif <= $tOut) {   //el tiempo de entrada es menor a tOut
                                                    //debo acoplar con el anterior.
                                                    $arrPasos[$cc - 1]['fecha_egreso'] = $pos['fecha_egreso'];
                                                } else {
                                                    $arrPasos[] = array('fecha_ingreso' => $pos['fecha_ingreso'], 'fecha_egreso' => $pos['fecha_egreso']);
                                                }
                                                $ant = count($arrPasos) - 1;
                                            }
                                        }
                                    }
                                }

                                //totalizo
                                $total = array(
                                    'segundos_activo' => 0,
                                    'segundos_detenido' => 0,
                                    'segundos_movimiento' => 0,
                                    'segundos_motor_encendido' => 0,
                                    'tiempo_activo' => 0,
                                    'tiempo_detenido' => 0,
                                    'tiempo_movimiento' => 0,
                                    'tiempo_motor_encendido' => 0,
                                    'velocidad_promedio' => 0,
                                    'velocidad_maxima' => 0,
                                    'km_total' => 0.0,
                                    'km_por_dia' => 0,
                                    'superficie' => 0,
                                    'rendimiento' => 0,
                                );

                                //obtengo datos del informe de distancias.
                                foreach ($arrPasos as $j => $p) {
                                    $info = $this->backendManager->informeDistancias($servicio->getId(), $p['fecha_ingreso'], $p['fecha_egreso'], array());
                                    //die('////<pre>' . nl2br(var_export($info, true)) . '</pre>////');
                                    $arrPasos[$j]['informe'] = $info;

                                    //----
                                    $arrPasos[$j]['tiempo_activo'] = $this->utilsManager->segundos2tiempo($info['segundos_activo']);
                                    $arrPasos[$j]['km_total'] = $info['km_total'];
                                    $arrPasos[$j]['km_por_dia'] = $info['km_por_dia'];
                                    //   die('////<pre>' . nl2br(var_export($info['kms_calculada'], true)) . '</pre>////');

                                    if (intval($info['km_total']) && intval($info['kms_calculada'])) {
                                        $arrPasos[$j]['diferencia_kms'] = abs(floatval($info['km_total']) - floatval($info['kms_calculada']));
                                    } else {
                                        $arrPasos[$j]['diferencia_kms'] = 0;
                                    }
                                    if ($ancho && $info['km_por_dia'] && $info['segundos_movimiento']) {
                                        $arrPasos[$j]['superficie'] = (floatval($ancho) * floatval($info['km_por_dia']) * 1000) / 10000;
                                        $arrPasos[$j]['rendimiento'] = floatval($arrPasos[$j]['superficie']) / (floatval($info['segundos_movimiento']) / 3600);
                                    } else {
                                        $arrPasos[$j]['superficie'] = 0;
                                        $arrPasos[$j]['rendimiento'] = 0;
                                    }

                                    $arrPasos[$j]['horometro_inicial'] = $this->utilsManager->minutos2tiempo(intval($info['horometro_inicial']));
                                    $arrPasos[$j]['horometro_final'] = $this->utilsManager->minutos2tiempo(intval($info['horometro_final']));
                                    $arrPasos[$j]['horometro_diff'] = $this->utilsManager->minutos2tiempo(intval($info['horometro_final']) - intval($info['horometro_inicial']));

                                    $arrPasos[$j]['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($info['segundos_detenido']);
                                    $arrPasos[$j]['tiempo_movimiento'] = $this->utilsManager->segundos2tiempo($info['segundos_movimiento']);
                                    $arrPasos[$j]['tiempo_motor_encendido'] = $this->utilsManager->segundos2tiempo($info['segundos_motor_encendido']);
                                    $arrPasos[$j]['velocidad_promedio'] = $info['velocidad_promedio'];
                                    $arrPasos[$j]['velocidad_maxima'] = $info['velocidad_maxima'];

                                    //totalizo.

                                    $total['superficie'] += floatval($arrPasos[$j]['superficie']);
                                    $total['rendimiento'] += $arrPasos[$j]['rendimiento'];
                                    $total['segundos_activo'] += floatval($info['segundos_activo']);

                                    $total['km_total'] += floatval($arrPasos[$j]['km_total']);
                                    $total['km_por_dia'] += floatval($info['km_por_dia']);
                                    $total['segundos_detenido'] += $info['segundos_detenido'];
                                    $total['segundos_movimiento'] += $info['segundos_movimiento'];
                                    $total['segundos_motor_encendido'] += $info['segundos_motor_encendido'];
                                    if (floatval($info['velocidad_promedio']) != 0) {
                                        $total['velocidad_promedio'] += $info['velocidad_promedio'];
                                        $countVP++;
                                    }
                                    if (floatval($info['velocidad_maxima']) != 0) {
                                        $total['velocidad_maxima'] = ($info['velocidad_maxima'] > $total['velocidad_maxima']) ? $info['velocidad_maxima'] : $total['velocidad_maxima'];
                                        $countVM++;
                                    }
                                }
                                $total['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($total['segundos_detenido']);
                                $total['tiempo_movimiento'] = $this->utilsManager->segundos2tiempo($total['segundos_movimiento']);
                                $total['tiempo_motor_encendido'] = $this->utilsManager->segundos2tiempo($total['segundos_motor_encendido']);
                                $total['velocidad_promedio'] = $total['velocidad_promedio'] / ($countVP == 0 ? 1 : $countVP);  // count($informe);
                                $total['rendimiento'] = $total['rendimiento'] / ($countVP == 0 ? 1 : $countVP);  // count($informe);

                                $arrServicios[$i][] = array(
                                    'nombre' => $servicio->getNombre(),
                                    'id' => $servicio->getId(),
                                    'total_paso' => count($arrPasos),
                                    'pasos' => $arrPasos
                                );
                                $arrTotal[$i][$servicio->getId()] = $total;
                            }
                        } //endif count
                    }  //endif
                } //endfor servicios
                //PASO 2: Para cada referencia obtengo los servicios que pasaron por ella.

                foreach ($arrServicios as $i => $r) {
                    $informe[] = array(
                        'nombre' => $castRef[$i],
                        'id' => $i,
                        'total_servicios' => count($r),
                        'servicios' => $r,
                    );
                }
            }


            $template = 'informe/AgroReferencia/pantalla.html.twig';

            switch ($consulta['destino']) {
                case '1': //sale a pantalla.
                    return $this->render($template, array(
                        'organizacion' => $this->user->getOrganizacion(),
                        'consulta' => $consulta,
                        'referencias' => $castRef,
                        'informe' => $informe,
                        'total' => $arrTotal,
                        'time' => $this->loggerManager->getTimeGeneracion(),
                    ));
                    break;
            }
        }
    }

    /**
     * @Route("/agro/info/referencia/{tipo}/{id}/exportar", name="informe_agro_referencia_exportar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_AGRO_INFORME_REFERENCIA")
     */
    function exportartotalAction(Request $request, $tipo = null, $id = null)
    {
        $informe = json_decode($request->get('informe'), true);
        $total = json_decode($request->get('total'), true);
        $consulta = json_decode($request->get('consulta'), true);
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Trabajo en Referencia");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
                'C5' => 'Torelancia (mins)',
                'D5' => $consulta['tolerancia'],
                'C6' => 'Ancho trabajo (mts)',
                'D6' => $consulta['ancho'],
            ));

            $xls->setBar(8, array(
                'A' => array('title' => 'Referencia', 'width' => 30),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Ingreso', 'width' => 20),
                'D' => array('title' => 'Egreso', 'width' => 20),
                'E' => array('title' => 'Kms Recorrido', 'width' => 20),
                'F' => array('title' => 'Superficie', 'width' => 20),
                'G' => array('title' => 'Rendimiento', 'width' => 20),
                'H' => array('title' => 'Tpo Detenido (hs)', 'width' => 20),
                'I' => array('title' => 'Tpo Movimiento (hs)', 'width' => 20),
                'J' => array('title' => 'Tpo Motor Enc. (hs)', 'width' => 20),
                'K' => array('title' => 'Veloc Prom (km/h)', 'width' => 20),
                'L' => array('title' => 'Veloc Max (km/h)', 'width' => 20),
            ));
            $i = 9;
            foreach ($informe as $referencia) {
                foreach ($referencia['servicios'] as $servicio) {
                    foreach ($servicio['pasos'] as $paso) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $referencia['nombre']),
                            'B' => array('value' => $servicio['nombre']),
                            'C' => array('value' => $paso['fecha_ingreso']),
                            'D' => array('value' => $paso['fecha_egreso']),
                            'E' => array('value' => $paso['km_total']),
                            'F' => array('value' => $paso['superficie']),
                            'G' => array('value' => $paso['rendimiento']),
                            'H' => array('value' => $paso['tiempo_detenido'], 'format' => '[HH]:MM:SS'),
                            'I' => array('value' => $paso['tiempo_movimiento'], 'format' => '[HH]:MM:SS'),
                            'J' => array('value' => $paso['tiempo_motor_encendido'], 'format' => '[HH]:MM:SS'),
                            'K' => array('value' => $paso['velocidad_promedio']),
                            'L' => array('value' => $paso['velocidad_maxima']),
                        ));
                        $i++;
                    }
                    $xls->setRowValues($i, array(
                        'A' => array('value' => ''),
                        'B' => array('value' => 'TOTALES'),
                        'C' => array('value' => ''),
                        'D' => array('value' => ''),
                        'E' => array('value' => $total[$referencia['id']][$servicio['id']]['km_total']),
                        'F' => array('value' => $total[$referencia['id']][$servicio['id']]['superficie']),
                        'G' => array('value' => $total[$referencia['id']][$servicio['id']]['rendimiento']),
                        'H' => array('value' => $total[$referencia['id']][$servicio['id']]['tiempo_detenido'], 'format' => '[HH]:MM:SS'),
                        'I' => array('value' => $total[$referencia['id']][$servicio['id']]['tiempo_movimiento'], 'format' => '[HH]:MM:SS'),
                        'J' => array('value' => $total[$referencia['id']][$servicio['id']]['tiempo_motor_encendido'], 'format' => '[HH]:MM:SS'),
                        'K' => array('value' => $total[$referencia['id']][$servicio['id']]['velocidad_promedio']),
                        'L' => array('value' => $total[$referencia['id']][$servicio['id']]['velocidad_maxima']),
                    ));
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=trabajo_referencia.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_agro_referencia', array('id' => $id)));
        }
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
