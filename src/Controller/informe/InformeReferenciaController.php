<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeReferenciaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;;

class InformeReferenciaController extends AbstractController
{

    private $servicioManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $backendManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $loggerManager;
    private $informeUtilsManager;
    private $excelManager;

    function __construct(
        ServicioManager $servicioManager,
        ExcelManager $excelManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        LoggerManager $loggerManager,
        InformeUtilsManager $informeUtilsManager
    ) {

        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->backendManager = $backendManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->loggerManager = $loggerManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/informe/referencia", name="informe_referencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_REFERENCIA")
     */
    public function pasoAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Paso");
        $options['servicios'] = $this->servicioManager->findAllByUsuario();
        $options['referencias'] = $this->referenciaManager->findAsociadas();
        $options['grupos_referencias'] = $this->referenciaManager->findAllGrupos();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();

        $form = $this->createForm(InformeReferenciaType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informepaso');

            if ($consulta) {
                //paso la fecha a formato yyyy-mm-dd
                $periodo = array(
                    'desde' => $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    'hasta' => $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );
                if ($periodo['hasta'] <= $periodo['desde']) {
                    $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->generateUrl('informe_referencia'));
                } else {
                    $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                    //establesco sobre que referencias debo trabajar.
                    if ($consulta['referencia'] == '-1') {    //considerar todas las referencias
                        $referencias = $this->referenciaManager->findAsociadas();
                    } elseif ($consulta['referencia'][0] == '*') {   //es un grupo de referencia.
                        $referencias = $this->referenciaManager->findAllByGrupo(substr($consulta['referencia'], 1));
                    } elseif ($consulta['referencia'] == '-') {   //es cualquier cagada...
                        $referencias = array();
                    } else {   //es un id de referencia.
                        $referencias[] = $this->referenciaManager->find($consulta['referencia']);
                    }

                    //obtengo los servicios a incluir en el informe.
                    $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio']);
                    $consulta['servicionombre'] = $arrServicios['servicionombre'];

                    //PASO 1: Obtengo los datos de los servicios y los ordeno por referencia.
                    $data = array();
                    foreach ($arrServicios['servicios'] as $servicio) {  //recorro los servicios asignados al grupo.
                        if ($servicio->getUltFechahora() >= $periodo['desde']) {    //evito q se consulte para equipos q no reportan en el periodo
                            $dataTmp = $this->backendManager->informePasoReferencias($servicio->getId(), $periodo['desde'], $periodo['hasta'], $referencias);
                            foreach ($dataTmp as $pos) {
                                $pos['tiempo'] = isset($pos['duracion']) ? $this->utilsManager->segundos2tiempo($pos['duracion']) : '0';
                                $data[$pos['referencia_id']][$servicio->getId()]['servicio'] = $servicio->getNombre();
                                unset($pos['latitud']);
                                unset($pos['longitud']);
                                unset($pos['duracion']);
                                unset($pos['distancia_anterior']);
                                unset($pos['duracion_anterior']);
                                $data[$pos['referencia_id']][$servicio->getId()]['pasos'][] = $pos;
                            }
                            unset($dataTmp);
                        }
                    }
                    //PASO 2: Para cada referencia obtengo los servicios que pasaron por ella.
                    $informe = array();
                    foreach ($referencias as $referencia) {  //para cada referencia pedida.
                        $tmpServ = null;
                        $count_pasos = 0;
                        if (key_exists($referencia->getId(), $data)) {
                            $tmpServ = $data[$referencia->getId()];
                            foreach ($tmpServ as $i => $value) {
                                $tmpServ[$i]['count_pasos'] = count($value['pasos']);
                                $count_pasos = $count_pasos + count($value['pasos']);
                            }
                            //die('////<pre>' . nl2br(var_export($count_pasos, true)) . '</pre>////');

                            $informe[] = array(
                                'referencia' => $referencia->getNombre(),
                                'servicios' => $tmpServ,
                                'count_servicios' => count($tmpServ),
                                'count_pasos' => $count_pasos,
                            );
                        }
                    }
                }
                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Referencia/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '2': //sale a grafico.
                        break;
                }
            }
        }
        return $this->render('informe/Referencia/referencia.html.twig', array(
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     * @Route("/informe/referencia/{tipo}/exportar", name="informe_referencia_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_REFERENCIA")
     */
    public function exportarAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            $xls = $this->excelManager->create("Informe de Referencias");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Referencia', 'width' => 40),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Fecha Ingreso', 'width' => 20),
                'D' => array('title' => 'Fecha Egreso', 'width' => 20),
                'E' => array('title' => 'Duración', 'width' => 20),
                'F' => array('title' => 'Distancia', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $referencias) {   //esto es para cada referencia
                //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                //$xls->setCellValue('A' . $i, $servicio['servicio']);
                if ($referencias['servicios'] != null) {
                    foreach ($referencias['servicios'] as $servicio) {
                        foreach ($servicio['pasos'] as $paso) {
                            $xls->setRowValues($i, array(
                                'A' => array('value' => $referencias['referencia']),
                                'B' => array('value' => $servicio['servicio']),
                                'C' => array('value' => $paso['fecha_ingreso']),
                                'D' => array('value' => $paso['fecha_egreso']),
                                'E' => array('value' => $paso['tiempo'], 'format' => '[HH]:MM:SS'),
                                'F' => array('value' => $paso['distancia']),
                            ));
                            $i++;
                        }
                    }
                }
            }

            //aca hago el resumen si lo piden
            if ($tipo == 1) {
                $i++;
                $xls->setSubTitle('A' . $i, 'RESUMEN');

                $i++;
                $xls->setBar($i, array(
                    'A' => array('title' => 'Referencia', 'width' => 40),
                    'B' => array('title' => 'Servicio', 'width' => 20),
                    'C' => array('title' => 'Cantidad', 'width' => 20),
                ));
                $i++;

                foreach ($informe as $referencias) {   //esto es para cada referencia
                    //$xls->setCellValue('A' . $i, $servicio['servicio']);
                    if ($referencias['servicios'] != null) {
                        foreach ($referencias['servicios'] as $servicio) {
                            $xls->setRowValues($i, array(
                                'A' => array('value' => $referencias['referencia']),
                                'B' => array('value' => $servicio['servicio']),
                                'C' => array('value' => count($servicio['pasos'])),
                            ));
                            $i++;
                        }
                    }
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=referencias.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_referencia'));
        }
    }
}
