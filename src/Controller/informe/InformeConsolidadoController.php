<?php

namespace App\Controller\informe;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\informe\InformeConsolidadoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\LoggerManager;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\BackendManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\GeocoderManager;

/**
 * Description of InformeConsolidadoController
 *
 * @author nicolas
 */
class InformeConsolidadoController extends AbstractController
{

    private $user;
    private $userloginManager;
    private $servicioManager;
    private $loggerManager;
    private $utilsManager;
    private $breadcrumbManager;
    private $informeUtilsManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoServicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $excelManager;
    private $geocoderManager;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        InformeUtilsManager $informeUtilsManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoServicioManager $grupoServicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ExcelManager $excelManager,
        GeocoderManager $geocoderManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/informe/{id}/consolidado", name="informe_consolidado",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CONSOLIDADO")
     */
    public function consolidadoAction(Request $request, $id)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe consolidado");
        $this->user = $this->getUser2Organizacion($id);
        //traigo todo los servicios existentes para el usuario
        $options = array(
            'servicios' => $this->servicioManager->findAllByUsuario($this->user),
            'grupos' => $this->grupoServicioManager->findAllByOrganizacion($this->user->getOrganizacion()),
        );
        $form = $this->createForm(InformeConsolidadoType::class, null, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $this->user = $this->getUser2Organizacion($id);
            $consulta = $request->get('informeconsolidado');
            $consulta['mostrar_direcciones'] = isset($consulta['mostrar_direcciones']);
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            //        $mostrartotal = isset($consulta['mostrar_totales']) ? $consulta['mostrar_totales'] : false;
            $tiempo_minimo = $consulta['tiempo_minimo'] * 60;
            $mostrar_direcciones = isset($consulta['mostrar_direcciones']) ? $consulta['mostrar_direcciones'] : false;
            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('informe_consolidado', array('id' => $id)));
            } else {
                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");

                //armo los períodos que debo tener en cuenta.
                $periodo = $this->informeUtilsManager->armarPeriodo($desde, $hasta, $consulta['destino'], false);

                //obtengo los servicios a incluir en el informe.
                $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $this->user);
                $consulta['servicionombre'] = $arrServicios['servicionombre'];

                //inicio procesamiento del informe
                $informe = array();
                foreach ($arrServicios['servicios'] as $servicio) {
                    if ($servicio->getEquipo()) {
                        $informe[] = array(
                            'servicio' => $servicio,
                            'servicioNombre' => $servicio->getNombre(),
                            'info_gral' => $this->getDataInformeDistancias($servicio, $periodo),
                            'info_detenciones' => $this->getDataInforme(
                                $servicio,
                                $periodo,
                                $tiempo_minimo,
                                $mostrar_direcciones
                            )
                        );
                    }
                }

                $this->loggerManager->logInforme($consulta);
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('informe/Consolidado/pantalla.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'time' => $this->loggerManager->getTimeGeneracion(),
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, 1, $consulta, $informe);
                        break;

                    case '6': //sale a mapa completo
                        $mapa = $this->crear_mapa($consulta, $informe);
                        $this->agregar_recorrido($mapa, $consulta, $arrServicios['servicios']);
                        $this->agregar_referencias($mapa, $this->user);
                        $info_distancias = $this->getDataInformeDistancias($servicio, $periodo);

                        return $this->render('informe/Consolidado/mapa_completo.html.twig', array(
                            'consulta' => $consulta,
                            'informe' => $informe,
                            'mapa' => $mapa,
                            'informeExt' => $info_distancias,
                        ));
                        break;
                }
            }
        }

        return $this->render('informe/Consolidado/consolidado.html.twig', array(
            'organizacion' => $this->user->getOrganizacion(),
            'form' => $form->createView(),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }

    public function getDataInformeDistancias($servicio, $periodo)
    {
        $informe = array();
        //pido toda la info al backend.        
        if ($servicio->getUltFechahora() >= $periodo[0]['desde']) {    //evito q se consulte para equipos q no reportan en el periodo
            $informe = $this->backendManager->informeDistancias($servicio->getId(), $periodo[0]['desde'], $periodo[0]['hasta'], array());
        }
        if (count($informe) > 0) {
            //termino de procesar y agregar nuevos campos calculados al informe.
            $informe['tiempo_activo'] = $this->utilsManager->segundos2tiempo($informe['segundos_activo']);
            $informe['tiempo_detenido'] = $this->utilsManager->segundos2tiempo($informe['segundos_detenido']);
            $informe['tiempo_movimiento'] = $this->utilsManager->segundos2tiempo($informe['segundos_movimiento']);
            $informe['tiempo_motor_encendido'] = $this->utilsManager->segundos2tiempo($informe['segundos_motor_encendido']);
            $informe['diferencia_kms'] = number_format(abs(intval($informe['km_total']) - intval($informe['kms_calculada'])), 2, '.', ',');
            $informe['km_total'] = 0;
            $informe['km_por_dia'] = 0;
        }

        return $informe;
    }

    private function getDataInforme($servicio, $periodo, $tiempo_minimo, $obtener_direccion)
    {

        $referencias = $this->referenciaManager->findAsociadas($this->user->getOrganizacion());
        $informe = array();
        foreach ($periodo as $k => $v) {
            $data = $this->backendManager->informeDetenciones(
                $servicio->getId(),
                $v['desde'],
                $v['hasta'],
                $tiempo_minimo,
                $referencias
            );
            foreach ($data as $key => $value) {
                $data[$key]['detencion_id'] = $key + 1;
                if (isset($value['segundos_detenido'])) {
                    $data[$key]['duracion'] = $this->utilsManager->segundos2tiempo($value['segundos_detenido']);
                } else {
                    $data[$key]['duracion'] = 0;
                }
                //proceso si es en una referencia.
                if (isset($value['referencia_id']) && $value['referencia_id'] != null) {
                    $referencia = $this->referenciaManager->find($value['referencia_id']);
                    if ($referencia) {
                        $data[$key]['referencia_nombre'] = $referencia->getNombre();
                    }
                }
                //obtengo la direccion siempre que se solicite.
                if ($obtener_direccion) {
                    $direc = $this->geocoderManager->inverso($value['latitud'], $value['longitud']);
                    $data[$key]['domicilio'] = $direc;
                }
                $informe[] = $data[$key];
            }
        }
        return $informe;
    }

    /**
     * @Route("/informe/consolidado/{tipo}/exportar", name="informe_consolidado_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_INFORME_CONSOLIDADO")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        //die('////<pre>' . nl2br(var_export($tipo, true)) . '</pre>////');
        if (intval($tipo) === 1) {
            if ($informe == null) {
                $informe = json_decode($request->get('informe'), true);
            }
            if ($consulta == null) {
                $consulta = json_decode($request->get('consulta'), true);
            }
        }
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe Consolidado");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            $xls->setBar(10, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Desde', 'width' => 20),
                'C' => array('title' => 'Hasta', 'width' => 20),
                'D' => array('title' => 'Activo', 'width' => 12),
                'E' => array('title' => 'Detenido', 'width' => 12),
                'F' => array('title' => 'Movimiento', 'width' => 12),
                'G' => array('title' => 'Ralenti', 'width' => 12),
                'H' => array('title' => 'Kms Total', 'width' => 12),
                'I' => array('title' => 'Kms Diarios', 'width' => 12),
                'J' => array('title' => 'Vel. Max.', 'width' => 12),
                'K' => array('title' => 'Vel. Prom.', 'width' => 12),
                'L' => array('title' => 'Ingreso Detención', 'width' => 20),
                'M' => array('title' => 'Egreso Detención', 'width' => 20),
                'N' => array('title' => 'Duración', 'width' => 20),
                'O' => array('title' => 'Referencia', 'width' => 20),
            ));
            $i = 11;
            foreach ($informe as $data) {
                if (count($data['info_detenciones']) > 0) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $data['servicioNombre']),
                        'B' => array('value' => $data['info_gral']['desde']),
                        'C' => array('value' => $data['info_gral']['hasta']),
                        'D' => array('value' => $data['info_gral']['tiempo_activo'], 'format' => '[HH]:MM:SS'),
                        'E' => array('value' => $data['info_gral']['tiempo_detenido'], 'format' => '[HH]:MM:SS'),
                        'F' => array('value' => $data['info_gral']['tiempo_movimiento'], 'format' => '[HH]:MM:SS'),
                        'G' => array('value' => $data['info_gral']['tiempo_motor_encendido'], 'format' => '[HH]:MM:SS'),
                        'H' => array('value' => $data['info_gral']['kms_calculada']),
                        'I' => array('value' => $data['info_gral']['kms_por_dia_calculada']),
                        'J' => array('value' => $data['info_gral']['velocidad_maxima']),
                        'K' => array('value' => $data['info_gral']['velocidad_promedio']),
                    ));

                    foreach ($data['info_detenciones'] as $detencion) {
                        $xls->setRowValues($i, array(
                            'L' => array('value' => $detencion['fecha_ingreso']),
                            'M' => array('value' => $detencion['fecha_egreso']),
                            'N' => array('value' => $detencion['duracion'])
                        ));
                        if (isset($detencion['referencia_nombre'])) {
                            $xls->setRowValues($i, array(
                                'O' => array('value' => $detencion['referencia_nombre'])
                            ));
                        } else {
                            if ($consulta['mostrar_direcciones']) {
                                $xls->setRowValues($i, array(
                                    'O' => array('value' => isset($detencion['domicilio']) ? $detencion['domicilio'] : '---')
                                ));
                            }
                        }
                        $i++;
                    }
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=consolidado.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('informe_consolidado', array('id' => $this->userloginManager->getOrganizacion()->getId())));
        }
    }

    private function crear_mapa($consulta, $data)
    {


        $mapa = $this->getMapaInforme($consulta, $data);
        $icon = $this->gmapsManager->createPosicionIcon($mapa, 'posicion', 'ballred.png');
        $informe = $data[0]['info_detenciones'];
        $id = 1;
        foreach ($informe as $id => $detencion) {
            $detencion['direccion'] = '0';
            $detencion['velocidad'] = '0';
            $mark = $this->gmapsManager->addMarkerPosicion($mapa, $id, $detencion, $icon);
            $mark->setTitle($detencion['detencion_id']);

            if ($detencion['referencia_id'] != null) {
                $this->gmapsManager->addMarkerReferencia($mapa, $this->referenciaManager->find($detencion['referencia_id']), true);
            }

            $this->gmapsManager->setFitBounds($mapa, $detencion['latitud'], $detencion['longitud']);
            $id++;
        }

        return $mapa;
    }

    private function agregar_recorrido($mapa, $consulta, $servicios)
    {
        $historial = $this->getHistorial($consulta, $servicios[0]);
        $this->gmapsManager->addPolyline($mapa, 'recorrido', $historial['puntos_polilinea']);

        //puntoA
        $puntoinicio = $this->gmapsManager->createPosicionIcon($mapa, 'puntoA', 'iconA.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'A', $historial['puntoA'], $puntoinicio);
        //puntoB
        $puntofinal = $this->gmapsManager->createPosicionIcon($mapa, 'puntoB', 'iconB.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'B', $historial['puntoB'], $puntofinal);

        return $mapa;
    }

    private function agregar_referencias($mapa)
    {
        //agrego las referencias al mapa
        $referencias = $this->referenciaManager->findAsociadas($this->user->getOrganizacion());
        foreach ($referencias as $referencia) {
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
        }
        $mapa = $this->gmapsManager->setClusterable($mapa, true);
        return $mapa;
    }

    private function getMapaInforme($consulta, $informe)
    {
        $mapa = $this->gmapsManager->createMap();
        return $mapa;
    }

    private function getHistorial($consulta, $servicio)
    {
        $opciones = array();
        try {
            //$this->container->get('logger')->info('ApmonBundle:Historial -> ' . var_export($consulta, true));
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
            $consHistorial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), $opciones);
        } catch (\Exception $exc) {
            return null;
        }

        $historial = array();
        //$min_lat = $max_lat = $min_lng = $max_lng = false;
        $puntos_polilinea = array();
        $puntoA = null;
        reset($consHistorial);
        while ($trama = current($consHistorial)) {
            next($consHistorial);

            if (isset($trama['posicion'])) {
                $puntos_polilinea[] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                if ($puntoA == null) {
                    $puntoA = array('latitud' => $trama['posicion']['latitud'], 'longitud' => $trama['posicion']['longitud']);
                }
                $ultima = $trama; //es para tener la ultima trama;
                $historial[] = $trama['posicion'];
            }
        }

        unset($consHistorial);

        if (isset($ultima) && !is_null($ultima)) {
            $puntoB = array('latitud' => $ultima['posicion']['latitud'], 'longitud' => $ultima['posicion']['longitud']);
        } else {
            $puntoB = null;
        }

        return array(
            'historial' => $historial,
            'puntos_polilinea' => $puntos_polilinea,
            'puntoA' => $puntoA,
            'puntoB' => $puntoB,
        );
    }
}
