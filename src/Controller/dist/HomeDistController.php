<?php

namespace App\Controller\dist;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\SearchManager;
use App\Model\app\ServiceManager;
use App\Model\app\DominioManager;

class HomeDistController extends AbstractController
{
    private function getSecurityContext()
    {
        return $this->get('security.token_storage');
    }

    private $service;
    private $dominioManager;

    public function __construct(ServiceManager $service, DominioManager $dominioManager)
    {
        $this->service = $service;
        $this->dominioManager = $dominioManager;
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/dist/homepage", name="dist_homepage")
     * @Method({"GET", "POST"})
     */
    public function homepageAction(Request $request, BreadcrumbManager $breadcrumbManager, UserLoginManager $userloginManager)
    {

        $breadcrumbManager->clear($request->getRequestUri(), "Inicio");
        $user = $userloginManager->getUser();
        //die('////<pre>' . nl2br(var_export($user, true)) . '</pre>////');
        //aca armo los menu.r
        $menuMapas = $this->getMenuMapas($user, $userloginManager);
        $menuFlota = $this->getMenuFlota($user, $userloginManager);
        $menuConfig = $this->getMenuConfig($user, $userloginManager);
        $menuInformes = $this->getMenuInformes($user, $userloginManager);

        //aca los grabo en la session.
        $session = $request->getSession();
        $session->set('menuFlota', $menuFlota);
        $session->set('menuConfig', $menuConfig);
        $session->set('menuInformes', $menuInformes);
        
        $this->setIcono2Sesion($request);

        return $this->render('dist/Home/homepage.html.twig', array(
            'menuFlota' => $menuFlota,
            'menuConfig' => $menuConfig,
            'menuInformes' => $menuInformes,
            'menuMapas' => $menuMapas,
            'status' => $this->service->consulServicios(),
            'dash' => $this->service->obtenerDashboard($user),
        ));
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/search", name="search")
     * @Method({"GET", "POST"})
     */
    public function searchAction(Request $request, SearchManager $searchManager, OrganizacionManager $organizacionManager)
    {
        $servicios = $equipos = $chips = $refs = $usuarios = null;
        $permisos = array();
        $value = $request->get('data');
        $checkServicios = $request->get('checkServicios') == 'true';
        $checkEquipos = $request->get('checkEquipos') == 'true';
        $checkChips = $request->get('checkChips') == 'true';
        $checkReferencias = $request->get('checkReferencias') == 'true';
        $checkUsuarios = $request->get('checkUsuarios') == 'true';

        //esto es para saber si hay que buscar en todos.
        $checkAll = !($checkServicios || $checkEquipos || $checkChips || $checkReferencias || $checkUsuarios);

        if ($checkAll || $checkServicios) {
            $arr1 = $searchManager->ServicioByNombre($value);
            $arr2 = $searchManager->ServicioByPatente($value);
            if (is_null($arr1)) {
                $merge = $arr2;
            } else {
                if (!is_null($arr2)) {
                    $merge = array_merge($arr1, $arr2);
                } else {
                    $merge = $arr1;
                }
            }
            if (!is_null($merge)) {
                $merge = array_unique($merge);
                $servicios = $searchManager->getServicios((array) $merge);
            }
            if (!is_null($servicios)) {
                foreach ($servicios as $servicio) {
                    $permisos['servicios'][$servicio->getId()] = $organizacionManager->inTreeOrganizacion(null, $servicio->getOrganizacion());
                }
            }
        }

        if ($checkAll || $checkEquipos) {
            $equipos = $searchManager->equipos($value);
            foreach ($equipos as $equipo) {
                $permisos['equipos'][$equipo->getId()] = $organizacionManager->inTreeOrganizacion(null, $equipo->getOrganizacion());
            }
        }
        if ($checkAll || $checkChips) {
            $chips = $searchManager->chips($value);
            foreach ($chips as $chip) {
                $permisos['chips'][$chip->getId()] = $organizacionManager->inTreeOrganizacion(null, $chip->getOrganizacion());
            }
        }
        if ($checkAll || $checkReferencias) {
            $refs = $searchManager->referencias($value);
            foreach ($refs as $ref) {
                $permisos['referencias'][$ref->getId()] = $organizacionManager->inTreeOrganizacion(null, $ref->getPropietario());
            }
        }
        if ($checkAll || $checkUsuarios) {
            $usuarios = $searchManager->usuarios($value);
            foreach ($usuarios as $usr) {
                $permisos['usuarios'][$usr->getId()] = $organizacionManager->inTreeOrganizacion(null, $usr->getOrganizacion());
            }
        }
        //        die('////<pre>' . nl2br(var_export(count($refs), true)) . '</pre>////');
        $html = $this->renderView("dist/Home/search_result.html.twig", array(
            'usuarios' => $usuarios,
            'referencias' => $refs,
            'chips' => $chips,
            'equipos' => $equipos,
            'servicios' => $servicios,
            'permisos' => $permisos,
        ));
        $html = str_replace($value, '<b>' . $value . '</b>', $html);
        return new Response($html, 200);
    }

    public function acordeonAction(UserLoginManager $userloginManager)
    {
        $user = $userloginManager->getUser();
        return $this->render('dist/Home/test_acordeon.html.twig', array(
            'menuFlota' => $this->getMenuFlota($user, $userloginManager),
            'menuConfig' => $this->getMenuConfig($user, $userloginManager),
        ));
    }

    private function getMenuMapas($user, $userlogManager)
    {
        $menu = array();
        if ($userlogManager->isGranted('ROLE_MONITOR_VER')) {
            $menu[] = $this->menu('Monitor de Eventos', $this->generateUrl('dist_monitor_index', array('id' => $user->getOrganizacion()->getId())), '');
        }
        return $menu;
    }

    private function getMenuFlota($user, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_CLIENTE_VER')) {
            $menu[] = $this->menu('Mis Clientes', $this->generateUrl('distribuidor_cliente_list', array('id' => $user->getOrganizacion()->getId())), '');
        }
        if ($userloginManager->isGranted('ROLE_DISTRIBUIDOR_VER')) {
            $menu[] = $this->menu('Mis Distribuidores', $this->generateUrl('dist_list'), '');
        }

        if ($userloginManager->isGranted('ROLE_FLOTA_ADMIN')) {
            //$menu[] = $this->menu('Administración de Flotas', $this->generateUrl('servicio_list', array('id' => 0)), '');
            $menu[] = $this->menu('Bitácora Gral.', $this->generateUrl('sauron_bitacora_list', array('id' => $user->getOrganizacion()->getId())), '');
            $menu[] = $this->menu('Bitácora de Servicios', $this->generateUrl('sauron_bitacoraservicio_list', array('id' => $user->getOrganizacion()->getId())), '');
        }
        if ($userloginManager->isGranted('ROLE_EQUIPO_VER')) {
            $menu[] = $this->menu('Depósito de Equipos', $this->generateUrl('sauron_equipo_list'), '');
            if ($userloginManager->isGranted('ROLE_FLOTA_ADMIN')) {
                $menu[] = $this->menu('Bitácora de Equipos', $this->generateUrl('sauron_bitacoraequipo_list', array('id' => $user->getOrganizacion()->getId())), '');
            }
        }
        if ($userloginManager->isGranted('ROLE_CHIP_VER')) {
            $menu[] = $this->menu('Depósito de Chips', $this->generateUrl('dist_chip_list'), '');
        }
        if ($userloginManager->isGranted('ROLE_HELPDESK_SHELL')) {
            $menu[] = $this->menu('Ubicación de Servicio por Patente', $this->generateUrl('sauron_mapa_patente'), '');
        }
        //ksort($menu);
        return $menu;
    }

    private function getMenuInformes($user, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_DIST_INFORME_REPORTES')) {
            $menu[] = $this->menu('Reporte de Facturación', $this->generateUrl('dist_informe_facturacion', array(
                'id' => $user->getOrganizacion()->getId(),
            )), '');
            $menu[] = $this->menu('Cantidad de Reportes de Servicios', $this->generateUrl('dist_informe_reportes', array(
                'id' => $user->getOrganizacion()->getId(),
            )), '');
            $menu[] = $this->menu('Equipos en Stock', $this->generateUrl('sauron_equipo_form_stock', array(
                'id' => $user->getOrganizacion()->getId(),
            )), '');
        }
        if ($userloginManager->isGranted('ROLE_DIST_INFORME_SINREPORTES')) {
            $menu[] = $this->menu('Servicios que no Reportan', $this->generateUrl('dist_informe_sinreportes'), '');
        }
        if ($userloginManager->isModuloEnabled('MANAGER_FABRICA')) {
            $menu[] = $this->menu('Todos los Equipos del Sistema', $this->generateUrl('sauron_equipo_list_all'), '');
        }

        ksort($menu);
        return $menu;
    }

    private function getMenuConfig($user, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_CLIENTE_VER')) {
            $menu[] = $this->menu('Mis Clientes', $this->generateUrl('distribuidor_cliente_list', array('id' => $user->getOrganizacion()->getId())), '');
        }
        if ($userloginManager->isGranted('ROLE_DISTRIBUIDOR_VER')) {
            $menu[] = $this->menu('Mis Distribuidores', $this->generateUrl('dist_list'), '');
        }
        if ($userloginManager->isGranted('ROLE_REFERENCIA_VER')) {
            $menu[] = $this->menu('Mis Referencias', $this->generateUrl('referencia_list', array('id' => $user->getOrganizacion()->getId())), '');
        }
        if ($userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_VER')) {
            $menu[] = $this->menu('Mis Grupos de Referencias', $this->generateUrl('sauron_grupo_referencia_list', array(
                'idorg' => $user->getOrganizacion()->getId(),
            )), '');
        }
        if ($userloginManager->isGranted('ROLE_SAURON_CUENTA_ADMIN')) {
            $menu[] = $this->menu('Mi Cuenta', $this->generateUrl('distribuidor_show', array(
                'id' => $user->getOrganizacion()->getId(),
                'tab' => 'main'
            )), '');
        }


        if (
            $userloginManager->isModuloEnabled('MANAGER_PORTAL') &&
            $userloginManager->isGranted('ROLE_ADMIN_PORTAL')
        ) {
            $menu[] = $this->menu('Administración de Portales', $this->generateUrl('sauron_portal_list', array(
                'idref' => 0,
            )), '');
        }
        if ($userloginManager->isGranted('ROLE_ROOT')) {
            $menu[] = $this->menu('Panel de Control General', $this->generateUrl('panelcontrol'), '');
        }
        //die('////<pre>'.nl2br(var_export($menu, true)).'</pre>////');
        return $menu;
    }

    private function menu($label, $url, $title)
    {
        return array('label' => $label, 'url' => $url, 'title' => $title,);
    }

    private function setIcono2Sesion(Request $request)
    {
        $dominio = $this->dominioManager->find($request->getHost());
        if ($dominio) {
            $logo = [
                'nombre' => $dominio->getOrganizacion()->getNombre(),
                'mensaje' => $dominio->getMensaje(),
                'imagen' => 'images/logos/' . $dominio->getPathlogo(),
            ];
            $i = explode('.', $dominio->getPathlogo());
            $icono = 'images/ico/' . $i[0] . '.ico';
        } else {
            $icono = 'security.ico';
        }
        $request->getSession()->set('icono', $icono);
        // dd($logo);
    }

}
