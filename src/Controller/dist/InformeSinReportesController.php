<?php

namespace App\Controller\dist;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\dist\InformeSinReportesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ExcelManager;
use App\Model\app\UtilsManager;

class InformeSinReportesController extends AbstractController
{

    /**
     * @Route("/informe/sinreportes", name="dist_informe_sinreportes")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_SINREPORTES")
     */
    public function formAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Informe de falta de reportes");
        //traigo todo los servicios existentes.
        $options['organizaciones'] = $organizacionManager->getTreeOrganizaciones(null, true);
        $form = $this->createForm(InformeSinReportesType::class, null, $options);
        return $this->render('dist/InformeSinReportes/form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/informe/sinreportes/generar", name="dist_informe_sinreportes_generar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_SINREPORTES")
     */
    public function generarAction(
        Request $request,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager
    ) {

        $tini = time() + microtime(true);
        $consulta = $request->get('informesinreportes');

        if ($consulta['organizacion'] == 0) {
            $rootOrg = $userloginManager->getOrganizacion();
        } else {
            $rootOrg = $organizacionManager->find(intval($consulta['organizacion']));
        }

        //se ejecuta la recoleccion de datos.
        $informe = $this->getDataDistribuidor($rootOrg, ($consulta['dias'] + 1) * 24, $organizacionManager, $servicioManager);

        switch ($consulta['destino']) {
            case '1': //sale a pantalla.
                return $this->render('dist/InformeSinReportes/pantalla.html.twig', array(
                    'consulta' => $consulta,
                    'informe' => $informe,
                    'tiempo_run' => (time() + microtime(true)) - $tini,
                ));
                break;
            case '2': //sale a grafico.
                break;
        }
    }

    /**
     * Devuelve los datos del los servicios del cliente que no reportan
     * @param  Organizacion $org
     * @return array $informe
     */
    private function getDataCliente($org, $horas, $servicioManager)
    {
        $serv = null;
        $servicios = $servicioManager->findSinReporte($org, null, $horas);
        foreach ($servicios as $servicio) {

            $st_ultreporte = $servicioManager->getStatusUltReporte($servicio);

            $serv[] = array(
                'id' => $servicio->getId(),
                'st_ultreporte' => $st_ultreporte,
                'ult_fechahora' => $servicio->getUltFechahora(),
                'servicio' => $servicio,
            );
        }
        if ($serv == null) {
            return null;
        } else {
            return array('cliente' => $org->getNombre(), 'servicios' => $serv);
        }
    }

    /**
     * Devuelve la estructura del distribuidor.
     * @param Organizacion $org
     * @return array $infor
     */
    private function getDataDistribuidor($org, $horas, $organizacionManager, $servicioManager)
    {
        $dists = $clies = null;
        $orgs = $organizacionManager->findAll($org, array('nombre' => 'ASC'));
        foreach ($orgs as $child) {
            if ($child->getTipoOrganizacion() == 1) {
                $dists[] = $this->getDataDistribuidor($child, $horas, $organizacionManager, $servicioManager);
            } else {   //es un cliente
                $c = $this->getDataCliente($child, $horas, $servicioManager);
                if (!is_null($c)) {
                    $clies[] = $c;
                }
            }
        }
        return array(
            'distribuidor' => $org->getNombre(),
            'distribuidores' => $dists,
            'clientes' => $clies,
        );
    }

    /**
     * @Route("/informe/sinreportes/exportar", name="dist_informe_sinreportes_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_SINREPORTES")
     */
    public function exportarAction(
        Request $request,
        ExcelManager $excelManager,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager
    ) {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        if (isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $excelManager->create("Informe de Servicios Sin Reporte");

            if ($consulta['organizacion'] == 0) {
                $str = 'Todas las organizaciones';
            } else {
                $org = $organizacionManager->find($consulta['organizacion']);
                $str = $org->getNombre();
            }
            $xls->setHeaderInfo(array(
                'C3' => 'Organización',
                'D3' => $str,
            ));

            //aca hago la conversion de la estructura a lo que a mi me conviene
            $data = $this->getDataExcel($informe, $servicioManager, $utilsManager);

            $xls->setBar(6, array(
                'A' => array('title' => 'Distribuidor', 'width' => 20),
                'B' => array('title' => 'Cliente', 'width' => 20),
                'C' => array('title' => 'Servicio', 'width' => 20),
                'D' => array('title' => 'Equipo', 'width' => 20),
                'E' => array('title' => 'Modelo', 'width' => 20),
                'F' => array('title' => 'Ult. Transm.', 'width' => 20),
                'G' => array('title' => 'Días s/Trans.', 'width' => 20),
                'H' => array('title' => 'Bateria (%)', 'width' => 20),
                'I' => array('title' => 'Velocidad (km/h)', 'width' => 20),
                'J' => array('title' => 'Válido', 'width' => 20),
            ));
            $i = 7;
            foreach ($data as $value) {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $value['distribuidor']),
                    'B' => array('value' => $value['cliente']),
                    'C' => array('value' => $value['servicio']),
                    'D' => array('value' => $value['equipo']),
                    'E' => array('value' => $value['modelo']),
                    'F' => array('value' => $value['fecha']),
                    'G' => array('value' => $value['dias']),
                    'H' => array('value' => $value['bateria']),
                    'I' => array('value' => $value['velocidad']),
                    'J' => array('value' => $value['valido']),
                ));
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=serviciossinreporte.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('dist_informe_sinreportes'));
        }
    }

    private function getDataExcel($informe, $servicioManager, $utilsManager)
    {
        $xls = null;
        //incluyo los clientes
        $tmp = $this->getProcClientes($informe['clientes'], $servicioManager, $utilsManager);
        foreach ($tmp as $value) {
            $xls[] = $value;
        }
        //incluyo los distribuidres
        if (!is_null($informe['distribuidores'])) {
            foreach ($informe['distribuidores'] as $dists) {
                $tmp = $this->getProcDist($dists, $servicioManager, $utilsManager);
                foreach ($tmp as $value) {
                    $xls[] = $value;
                }
            }
        }
        //        die('////<pre>' . nl2br(var_export($xls, true)) . '</pre>////');
        return $xls;
    }

    private function getProcClientes($clientes, $servicioManager, $utilsManager)
    {
        $data = array();
        if (!is_null($clientes)) {
            foreach ($clientes as $cliente) {
                foreach ($cliente['servicios'] as $serv) {
                    $servicio = $servicioManager->find($serv['id']);
                    $data[] = array(
                        'distribuidor' => $servicio->getOrganizacion()->getOrganizacionPadre()->getNombre(),
                        'cliente' => $cliente['cliente'],
                        'servicio' => $servicio->getNombre(),
                        'equipo' => is_null($servicio->getEquipo()) ? null : $servicio->getEquipo()->getMdmid(),
                        'modelo' => is_null($servicio->getEquipo()) ? null : $servicio->getEquipo()->getModelo()->getNombre(),
                        'fecha' => $utilsManager->fechaUTC2Local($servicio->getUltFechaHora(), 'd/m/Y H:i:s'),
                        'dias' => $serv['st_ultreporte'],
                        'bateria' => $servicio->getUltBateria(),
                        'velocidad' => $servicio->getUltVelocidad(),
                        'valido' => $servicio->getUltValido() ? 'SI' : 'NO',
                    );
                }
            }
        }

        return $data;
    }

    private function getProcDist($distrib, $servicioManager, $utilsManager)
    {
        $data = array();
        //incluyo los clientes
        $tmp = $this->getProcClientes($distrib['clientes'], $servicioManager, $utilsManager);
        foreach ($tmp as $value) {
            $data[] = $value;
        }
        //incluyo los distribuidres
        if (!is_null($distrib['distribuidores'])) {
            foreach ($distrib['distribuidores'] as $dists) {
                $tmp = $this->getProcDist($dists, $servicioManager, $utilsManager);
                foreach ($tmp as $value) {
                    $data[] = $value;
                }
            }
        }
        return $data;
    }
}
