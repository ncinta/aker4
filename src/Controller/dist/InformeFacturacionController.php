<?php

namespace App\Controller\dist;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\dist\InformeFacturacionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\BackendManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;

class InformeFacturacionController extends AbstractController
{

    private $userloginManager;
    private $bread;
    private $organizacionManager;
    private $utils;
    private $servicioManager;
    private $excelManager;
    private $backedManager;

    function __construct(
        UserLoginManager $userlogin,
        BreadcrumbManager $breadcrumb,
        OrganizacionManager $organizacion,
        UtilsManager $utils,
        ServicioManager $servicioManager,
        ExcelManager $excelManager,
        BackendManager $backend
    ) {
        $this->userloginManager = $userlogin;
        $this->bread = $breadcrumb;
        $this->organizacionManager = $organizacion;
        $this->utils = $utils;
        $this->servicioManager = $servicioManager;
        $this->excelManager = $excelManager;
        $this->backedManager = $backend;
    }

    /**
     * Lista todos los reportes
     * @Route("/informe/{id}/facturacion", name="dist_informe_facturacion",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function formAction(Request $request, $id)
    {

        if ($id == $this->userloginManager->getOrganizacion()->getId()) {
            $this->bread->push($request->getRequestUri(), "Informe de Facturación");
        }

        if (isset($id) && !is_null($id) && (string) $id[0] == '*') {
            $id = str_replace('*', '', $id);
        }
        //traigo todo los servicios existentes.        
        $options['root'] = $this->organizacionManager->find($id);
        $options['distribuidores'] = $this->organizacionManager->findAllDistribuidores($options['root']);
        $options['clientes'] = $this->organizacionManager->findAllClientes($options['root']);

        $form = $this->createForm(InformeFacturacionType::class, null, $options);
        return $this->render('dist/InformeFacturacion/form.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $options['root'],
        ));
    }

    /**
     * Lista todos los reportes
     * @Route("/informe/id/facturacion", name="dist_informe_facturacion_changedistribuidor",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function changedistribuidoAction(Request $request, $id)
    {
        //        return $this->forward('App\Controller\dist\InformeReportesController::form', array(
        //                    'id' => $id,
        //        ));
    }

    /**
     * Lista todos los reportes
     * @Route("/informe/facturacion/generar", name="dist_informe_facturacion_generar",
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function generarAction(Request $request)
    {

        $this->bread->push($request->getRequestUri(), "Resultado");
        $consulta = $request->get('informefacturacion');

        //paso la fecha a formato yyyy-mm-dd
        $fecha = $this->utils->datetime2sqltimestamp($consulta['desde'], false);
        $fecha = date("Y-m-d", strtotime($fecha));
        $consulta['sqldesde'] = $fecha . ' 00:00:00';

        //debo sumar un dia a la fecha desde
        $fecha = $this->utils->datetime2sqltimestamp($consulta['hasta'], true);
        $fecha = date("Y-m-d", strtotime("$fecha + 1 days"));
        $consulta['sqlhasta'] = $fecha . ' 00:00:00';

        if ($consulta['distribuidor'] != '' && $consulta['cliente'] == '') { //algo tengo en el distribuidor
            if ($consulta['distribuidor'][0] == '*') { ///estoy pidiendo todo los distribuidores
                $consulta['distribuidor'] = str_replace('*', '', $consulta['distribuidor']);
            }
            $distri = $this->organizacionManager->find(intval($consulta['distribuidor']));
            $tmpOrg = $this->organizacionManager->getTreeOrganizaciones($distri);
            foreach ($tmpOrg as $org) {
                if ($org->getTipoOrganizacion() == 2) {  //solo dejo pasar los clientes que son los que tienen servicios.
                    $organizaciones[] = $org;
                }
            }
        }

        if ($consulta['cliente'] != '') {
            if ($consulta['cliente'][0] == '*') { ///estoy pidiendo todo los clientes del distribuidor
                $consulta['cliente'] = str_replace('*', '', $consulta['cliente']);
                $distri = $this->organizacionManager->find(intval($consulta['cliente']));
                $organizaciones = $this->organizacionManager->findAllClientes($distri);
            } else {  //estoy pidiendo por un cliente puntual
                $organizaciones[] = $this->organizacionManager->find(intval($consulta['cliente']));
            }
        }

        //convierto las organizaciones en array;
        $ordIds = array();
        foreach ($organizaciones as $organizacion) {
            $orgIds[] = $organizacion->getId();
        }

        return $this->render('dist/InformeFacturacion/result.html.twig', array(
            'consulta' => $consulta,
            'consulta_json' => json_encode($consulta),
            'organizaciones' => $orgIds,
        ));
    }

    /**
     * @Route("/informe/facturacion/obtener", name="dist_informe_facturacion_obtener")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function obtenerAction(Request $request)
    {
        $fecha_desde = $request->get('fecha_desde');
        $fecha_hasta = $request->get('fecha_hasta');
        $idOrg = $request->get('id');

        $organizacion = $this->organizacionManager->find($idOrg);
        $dataServicios = $this->obtenerDatos($organizacion, $fecha_desde, $fecha_hasta);
        $data = array(
            'organizacion' => ($organizacion->getOrganizacionPadre() != null ? $organizacion->getOrganizacionPadre() . ' : ' : '') . $organizacion->getNombre(),
            'servicios' => $dataServicios,
        );

        $html = $this->renderView('dist/InformeFacturacion/result_body.html.twig', array(
            'servicios' => $dataServicios,
            'organizacion' => $organizacion,
        ));
        return new Response(json_encode(array(
            'id' => $idOrg,
            'html' => $html,
            'data' => json_encode($data),
        )), 200);
    }

    /**
     * Inicializo $data con los valores por default que si o si debo mostrar en el reporte.
     * @param type $servicio
     * @return type array
     */
    private function initData($servicio)
    {
        if ($servicio->getVehiculo()) {
            $patente = $servicio->getVehiculo()->getPatente();
        } else {
            $patente = '';
        }
        if ($servicio->getEquipo()) {
            $equipo = sprintf('%s (%s)', $servicio->getEquipo()->getMdmid(), $servicio->getEquipo()->getModelo()->getNombre());
        } else {
            $equipo = '---';
        }
        //TODO: Esto deberia cambiarse con el status de facturacion en un futuro.
        if ($servicio->getEquipo() && $servicio->getEquipo()->getNumeroSerie() != '') {  //porque hay anotaciones en el nro de serie
            $nota = $servicio->getEquipo()->getNumeroSerie();
        } else {
            $nota = '';
        }
        return array(
            'servicio_id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'estado' => $servicio->getStrEstado(),
            'estado_id' => $servicio->getEstado(),
            'fecha_alta' => $servicio->getFechaAlta(),
            'fecha_baja' => $servicio->getFechaBaja(),
            'patente' => $patente,
            'equipo' => $equipo,
            'nota' => $nota,
            'data' => array(
                'fecha_inicio' => null,
                'fecha_fin' => $servicio->getUltFechahora(),
                'reportes' => '---'
            ),
            'cant_dias' => '---',
            'total_dias' => '---',
            'efectividad' => '---',
        );
    }

    private function IncluirServicio($servicio)
    {
        return $servicio->getEstado() >= 1 && $servicio->getEstado() < 9;
    }

    private function obtenerDatos($organizacion, $fecha_desde, $fecha_hasta)
    {
        $dataServicios = array();
        $ids = array();
        $servicios = $this->servicioManager->findAll($organizacion);
        foreach ($servicios as $servicioTmp) {       //recorro todos los rervicios de la organizacion
            $servicio = null;
            //controlo si debo incluir el servicio o no en la consulta

            if ($this->IncluirServicio($servicioTmp)) {   //el servicio esta para incluirse
                $servicio = $servicioTmp;   //incluyo el servicio

                if ($servicio->getEstado() == 4 && $servicioTmp->getUltFechahora() < date_create($fecha_desde)) {
                    //esta dado de baja y reporto antes de la fecha del periodo
                    $servicio = null;
                }
            } else {   //servicio deshabilitado
                //servicios deshabilitados pero con reporte despues de la fecha de consulta -> reportaron en el periodo
                if ($servicioTmp->getUltFechahora() >= date_create($fecha_desde)) {
                    $servicio = $servicioTmp;
                }
            }

            if (!is_null($servicio)) {
                //inicializo $data con los datos basicos
                $data = $this->initData($servicio);
                //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');

                //solo proceso los servicios que reportaron y que reportaron despues de la fecha de inicio
                if (!is_null($servicio->getUltFechahora()) && $servicio->getUltFechahora() >= date_create($fecha_desde)) {


                    //obtengo la info desde el backend.
                    $dHistorial = $this->backedManager->informeReportes($servicio->getId() * 1, $fecha_desde, $fecha_hasta, array('contar_reportes' => false));

                    //reproceso la info obtenida desde el backend para que deje la data lista.
                    $data = $this->servicioManager->informeFacturacion($servicio, $fecha_desde, $fecha_hasta, $dHistorial, $data);
                }
                //voy acumulando en $dataServicios todos los reportes individuales de los servicios.
                $dataServicios[] = $data;
            }
        }
        //  die('////<pre>' . nl2br(var_export($dataServicios, true)) . '</pre>////');
        return $dataServicios;
    }

    /**
     * @Route("/informe/facturacion/exportar", name="dist_informe_facturacion_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function exportarAction(Request $request)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('data'), true);
        if (isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Reportes de Servicios");

            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Organización', 'width' => 20),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Estado', 'width' => 20),
                'D' => array('title' => 'Equipo', 'width' => 20),
                'E' => array('title' => '', 'width' => 20),  //nota
                'F' => array('title' => 'Fecha Alta', 'width' => 20),
                'G' => array('title' => 'Fecha Baja', 'width' => 20),
                'H' => array('title' => 'Fecha Inicio', 'width' => 20),
                'I' => array('title' => 'Fecha Fin', 'width' => 20),
                'J' => array('title' => 'Cant. Días', 'width' => 20),
                'K' => array('title' => 'Total Días', 'width' => 20),
                'L' => array('title' => 'Efectividad (%)', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $datos) {   //esto es para cada servicio
                foreach ($datos['servicios'] as $key => $value) {
                    if (isset($value['data']['fecha_fin']['date'])) {
                        $fFin = date_create($value['data']['fecha_fin']['date'])->format('Y-m-d H:i');
                    } else {
                        //die('////<pre>' . nl2br(var_export(date_create($value['data']['fecha_fin'])->format('Y'), true)) . '</pre>////');
                        $fFin = date_create($value['data']['fecha_fin'])->format('Y-m-d H:i');
                    }
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['organizacion']),
                        'B' => array('value' => $value['nombre']),
                        'C' => array('value' => $value['estado']),
                        'D' => array('value' => $value['equipo']),
                        'E' => array('value' => $value['nota']),
                        'F' => array('value' => is_null($value['fecha_alta']) ? '---' : date_create($value['fecha_alta']['date'])->format('Y-m-d H:i')),
                        'G' => array('value' => is_null($value['fecha_baja']) ? '---' : date_create($value['fecha_baja']['date'])->format('Y-m-d H:i')),
                        'H' => array('value' => is_null($value['data']['fecha_inicio']) ? '---' : date_create($value['data']['fecha_inicio'])->format('Y-m-d H:i')),
                        'I' => array('value' => is_null($value['data']['fecha_fin']) ? '---' : $fFin),
                        'J' => array('value' => $value['cant_dias']),
                        'K' => array('value' => $value['total_dias']),
                        'L' => array('value' => $value['efectividad']),
                    ));
                    $i++;
                }
                //die('////<pre>' . nl2br(var_export($x, true)) . '</pre>////');
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=reportesservicios.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('dist_informe_reportes', array(
                'id' => $this->userloginManager->getOrganizacion()->getId(),
            )));
        }
    }
}
