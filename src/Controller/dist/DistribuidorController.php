<?php

namespace App\Controller\dist;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Organizacion;
use App\Form\dist\DistribuidorNewType;
use App\Form\dist\DistribuidorEditType;
use App\Form\dist\AsociarModuloType;
use GMaps\Geocoder;
use GMaps\Map;
use GMaps\Marker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UsuarioManager;
use App\Model\app\ServicioManager;
use App\Model\app\ChipManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\PaisManager;
use App\Model\app\EquipoManager;
use App\Model\app\Router\DistribuidorRouter;
use App\Model\app\Router\DominioRouter;


class DistribuidorController extends AbstractController
{

    private function getLogos()
    {
        return glob('images/logos/*.*');
    }


    /**
     * Lista todos los distribuidores del sistema.
     * @Route("/dist/root/list", name="dist_root_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listRootAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Dist. Root");

        $roots = $organizacionManager->findDistribuidoresRoot();

        //el usuario logueado no tiene organizacion
        return $this->render('dist/Distribuidor/list.html.twig', array(
            'distribuidores' => $roots
        ));
    }

    /**
     * Lista todos los distribuidores del sistema.
     * @Route("/dist/list", name="dist_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DISTRIBUIDOR_VER")
     */
    public function listAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        DistribuidorRouter $distribuidorRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Distribuidores");

        $organizacion = $userloginManager->getOrganizacion();
        $distribuidores = $organizacionManager->findAllDistribuidores($organizacion);

        return $this->render('dist/Distribuidor/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion, $distribuidorRouter),
            'distribuidores' => $distribuidores
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, $distribuidorRouter)
    {
        //agrega un distribuidor en una distribuicion, por defecto esta es la del usuaio logueado.
        $menu = array(
            1 => array(
                $distribuidorRouter->btnNew($organizacion),
            ),
        );
        return $distribuidorRouter->toCalypso($menu);
    }

    /**
     * @Route("/dist/{id}/show/{tab}", name="distribuidor_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DISTRIBUIDOR_VER")
     */
    public function showAction(
        Request $request,
        Organizacion $distribuidor,
        $tab,
        UsuarioManager $usuarioManager,
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        ChipManager $chipManager,
        ReferenciaManager $referenciaManager,
        EquipoManager $equipoManager,
        OrganizacionManager $organizacionManager,
        DistribuidorRouter $distribuidorRouter,
        DominioRouter $dominioRouter,
        UserLoginManager $userloginManager
    ) {

        if ($tab == 'main') {
            if ($request->getSession()->get('idorg') == $distribuidor->getId() || $request->getSession()->get('idorg') == null) {
                $breadcrumbManager->pop();
            }
        }
        $deleteForm = $this->createDeleteForm($distribuidor->getId());
        $request->getSession()->set('idorg', $distribuidor->getId());

        $menu = $this->getShowMenu($distribuidor, $distribuidorRouter, $dominioRouter);
        $param = array(
            'organizacion' => $distribuidor,
            'delete_form' => $deleteForm->createView(),
            'tab' => $tab,
            'menu' => $menu,
        );
        $breadcrumbManager->push($request->getRequestUri(), sprintf('%s (%s)', $distribuidor->getNombre(), $tab));
        //die('////<pre>'.nl2br(var_export($tab, true)).'</pre>////');
        switch ($tab) {
            case 'usuarios':
                $param['usuarios'] = $usuarioManager->findAll($distribuidor);
                $param['menu']['Agregar Usuario'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('usuario_new', array(
                        'idorg' => $distribuidor->getId()
                    ))
                );
                break;
            case 'flota':
                $param['servicios'] = $servicioManager->findAll($distribuidor);
                break;
            case 'chips':
                $param['chips'] = $chipManager->findAll($distribuidor);
                $param['menu']['Agregar Chips'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('dist_chip_new', array(
                        'id' => $distribuidor->getId()
                    ))
                );
                break;
            case 'referencias':
                //traigo todas las referencias del dist.
                $param['referencias'] = $referenciaManager->findAsociadas($distribuidor);
                $param['menu']['Agregar Referencia'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('referencia_new', array(
                        'id' => $distribuidor->getId()
                    ))
                );
                $param['menu']['Categorias'] = array(
                    'imagen' => 'icon-tasks',
                    'url' => $this->generateUrl('sauron_categoria_list', array(
                        'idorg' => $distribuidor->getId()
                    ))
                );
                $param['menu']['Grupos'] = array(
                    'imagen' => 'icon-th-large icon-blue',
                    'url' => $this->generateUrl('sauron_grupo_referencia_list', array(
                        'idorg' => $distribuidor->getId()
                    ))
                );
                $param['menu']['Compartir'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("compartir") id=btnCompartir style=display:none',
                    'url' => '#compartir',
                );
                $param['menu']['Copiar'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("copiar") id=btnCopiar style=display:none',
                    'url' => '#copiar',
                );
                $param['menu']['Mover'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("mover") id=btnMover style=display:none',
                    'url' => '#mover',
                );
                break;
            case 'equipos':
                $param['equipos'] = $equipoManager->findAll($distribuidor);

                $param['menu']['Agregar Equipo'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('sauron_equipo_new', array(
                        'idorg' => $distribuidor->getId()
                    ))
                );
                break;
            case 'distribuidores':
                $param['distribuidores'] = $organizacionManager->findAllDistribuidores($distribuidor);

                if ($userloginManager->isGranted('ROLE_ORGANIZACION_AGREGAR')) {
                    $param['menu']['Agregar Distribuidor'] = array(
                        'imagen' => 'icon-plus-sign icon-blue',
                        'url' => $this->generateUrl('distribuidor_new', array(
                            'id' => $distribuidor->getId()
                        ))
                    );
                }
                break;
            case 'clientes':
                $param['menu']['Agregar Cliente'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('distribuidor_cliente_new', array(
                        'id' => $distribuidor->getId()
                    ))
                );

                $param['clientes'] = $organizacionManager->findAllClientes($distribuidor);
                break;

            default:
                break;
        }
        return $this->render('dist/Distribuidor/show.html.twig', $param);
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $distribuidor 
     * @return array $menu
     */
    private function getShowMenu($distribuidor, $distribuidorRouter, $dominioRouter)
    {
        $menu = array(
            1 => array(
                $distribuidorRouter->btnEdit($distribuidor),
                $distribuidorRouter->btnDelete($distribuidor),
                $dominioRouter->btnList($distribuidor),
                $distribuidorRouter->btnAsociarModulo($distribuidor),
            ),
        );
        return $distribuidorRouter->toCalypso($menu);
    }

    /**
     * @Route("/dist/{id}/new", name="distribuidor_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $padre,
        OrganizacionManager $organizacionManager,
        PaisManager $paisManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $distribuidor = $organizacionManager->create($padre, 1); //crea el distribuidor
        if (!$distribuidor) {
            throw $this->createNotFoundException('Código de Distribuidor no accesible.');
        }
        $options['paises'] = $paisManager->getArrayPaises();
        $form = $this->createForm(DistribuidorNewType::class, $distribuidor, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
               // die('////<pre>' . nl2br(var_export($request->get('distribuidor'), true)) . '</pre>////');
                $breadcrumbManager->pop();
                if ($organizacionManager->save($distribuidor, null, null, 1)) {

                    $this->setFlash('success', sprintf('Se ha creado el distribuidor <b>%s</b> en el sistema. Configure los módulos de programa habilitados.', strtoupper($distribuidor)));
                    return $this->redirect($this->generateUrl('distribuidor_asociar_modulo', array(
                        'id' => $distribuidor->getId(), 'aux' => '1'
                    )));
                } else {
                    $error = 'No se pudo crear el cliente. Avise a su Distribuidor.';
                }
            } else {
                $error = 'Los datos no son válidos';
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Distribuidor");
        return $this->render('dist/Distribuidor/new.html.twig', array(
            'distribuidor' => $distribuidor,
            'form' => $form->createView(),
            'mapa' => $this->createMap($organizacionManager->find($padre->getId())),
            'geocoder' => new Geocoder(),
            'logos' => $this->getLogos(),
        ));
    }

    private function createMap($organizacion)
    {
        //mapa para ubicar a la organizacion
        $mapa = new Map();
        $mapa->setSize('80%', '250px');
        $mapa->setDebug(true);
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($organizacion->getLatitud());
        $mapa->setIniLongitud($organizacion->getLongitud());
        $mapa->setIncludeJQuery(true);

        $marcador = new Marker('punto_edicion', $organizacion->getLatitud(), $organizacion->getLongitud(), $organizacion);
        $marcador->setDraggable(true); // habilita la posibilidad de mover un marcador
        $marcador->setRaiseOnDrag(true); // permite que el marcador se "levante" del suelo al arrastrarlo

        $marcador->setClickListener('clickMarcador');
        $marcador->setDragListener('dragMarcador');
        $marcador->setDragendListener('finDragMarcador');
        $mapa->addMarker($marcador);
        return $mapa;
    }

    /**
     * @Route("/dist/{id}/edit", name="distribuidor_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_EDITAR")
     */
    public function editAction(
        Request $request,
        Organizacion $distribuidor,
        PaisManager $paisManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {
        $options['paises'] = $paisManager->getArrayPaises();
        $form = $this->createForm(DistribuidorEditType::class, $distribuidor, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                if ($organizacionManager->save($distribuidor, null, null, 3)) {
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados !!!', strtoupper($distribuidor)));
                    return $this->redirect($breadcrumbManager->getVolver());
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar Distribuidor");
        return $this->render('dist/Distribuidor/edit.html.twig', array(
            'distribuidor' => $distribuidor,
            'form' => $form->createView(),
            'mapa' => $this->createMap($distribuidor),
            'geocoder' => new Geocoder(),
        ));
    }

    /**
     * Elimina un distribuidor del sistema
     * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
     * @Route("/dist/{id}/delete", name="distribuidor_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $form = $this->createDeleteForm($organizacion->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getDoctrine()->getEntityManager();
            $organizacion = $organizacionManager->find($organizacion->getId());
            if (!$organizacion) {
                throw $this->createNotFoundException('Imposible encontrar la organizacion para borrar.');
            }

            $ok = true;
            if (count($organizacion->getEquiposPropios()) > 0) {
                $str = sprintf('No se ha podido eliminar la organizacion <b>%s</b>. Tiene equipos propios asociados.', $organizacion->getNombre());
                $this->setFlash('success', $str);
                $ok = false;
            }
            if ($ok) {
                try {
                    //borrado de servicios eliminados.
                    if ($organizacion->getServicios() != null) {
                        foreach ($organizacion->getServicios() as $servicio) {
                            $em->remove($servicio);
                        }
                    }
                    //borrando contacto
                    if ($organizacion->getContactos() != null) {
                        foreach ($organizacion->getContactos() as $contacto) {
                            $em->remove($contacto);
                        }
                    }

                    if ($organizacion->getGrupoReferencias() != null) {
                        //borrando gruposreferencia
                        foreach ($organizacion->getGrupoReferencias() as $gr) {
                            $em->remove($gr);
                        }
                    }
                    $str = sprintf('Se ha eliminado la organizacion <b>%s</b>', $organizacion->getNombre());
                    $organizacionManager->delete($organizacion);
                    $this->setFlash('success', $str);
                } catch (\Exception $exc) {
                    $str = sprintf('No se ha podido eliminar la organizacion <b>%s</b>', $organizacion->getNombre());
                    $this->setFlash('success', $str);
                }
            }
        }

        return $this->redirect($breadcrumbManager->getVolver());
        //return $this->redirect($return);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/dist/{id}/asocmodulo", name="distribuidor_asociar_modulo",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_MODULO")
     */
    public function asociarmoduloAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $modulosOld = null;
        $em =  $this->getDoctrine()->getManager();

        foreach ($organizacion->getModulos() as $modulo) {
            $modulosOld[] = $modulo->getCodename();
        }

        $form = $this->createForm(AsociarModuloType::class, $organizacion);

        //se traen todos los modulos activos que tiene la organizacion.
        $modulos = $em->getRepository('App:Organizacion')->getModulosActivos($organizacion->getOrganizacionPadre());

        // verificamos si el formulario enviado es valido
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $org_modulos = $request->get('organizacionasociarmodulo');
            foreach ($org_modulos['modulos'] as $id) {
                $modulo = $em->getRepository('App:Modulo')->find($id);
                $modulos[] = $modulo->getCodename();
            }
            if ($modulosOld != null) {
                $diff_add = array_diff($modulos, $modulosOld);
                $diff_remove = array_diff($modulosOld, $modulos);
            } else {
                $diff_add = $modulos;
                $diff_remove = null;
            }

            $breadcrumbManager->pop();
            $organizacionManager->save($organizacion, $diff_add, $diff_remove, 2);

            $this->setFlash('success', sprintf('Los modulos de <b>%s</b> han sido actualizados', strtoupper($organizacion)));

            return $this->redirect($breadcrumbManager->getVolver());
        }


        $breadcrumbManager->push($request->getRequestUri(), "Asociar Modulo");
        return $this->render('dist/Modulo/asociar_modulo.html.twig', array(
            'organizacion' => $organizacion,
            'modulos' => $modulos,
            'form' => $form->createView(),
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
