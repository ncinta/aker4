<?php

namespace App\Controller\dist;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\dist\InformeReportesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\BackendManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;

class InformeReportesController extends AbstractController
{

    /**
     * Lista todos los reportes
     * @Route("/informe/{id}/reportes", name="dist_informe_reportes",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function formAction(
        Request $request,
        $id,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        if ($id == $userloginManager->getOrganizacion()->getId()) {
            $breadcrumbManager->push($request->getRequestUri(), "Informe de reportes");
        }

        if (isset($id) && !is_null($id) && (string) $id[0] == '*') {
            $id = str_replace('*', '', $id);
        }
        //traigo todo los servicios existentes.        
        $options['root'] = $organizacionManager->find($id);
        $options['distribuidores'] = $organizacionManager->findAllDistribuidores($options['root']);
        $options['clientes'] = $organizacionManager->findAllClientes($options['root']);

        $form = $this->createForm(InformeReportesType::class, null, $options);
        return $this->render('dist/InformeReportes/form.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $options['root'],
        ));
    }

    /**
     * Lista todos los reportes
     * @Route("/informe/id/reportes", name="dist_informe_reportes_changedistribuidor",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function changedistribuidoAction(Request $request, $id)
    {
        //        return $this->forward('App\Controller\dist\InformeReportesController::form', array(
        //                    'id' => $id,
        //        ));
    }

    /**
     * Lista todos los reportes
     * @Route("/informe/reportes/generar", name="dist_informe_reportes_generar",
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function generarAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        UtilsManager $utilsManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Resultado");
        $consulta = $request->get('informereportes');

        //paso la fecha a formato yyyy-mm-dd
        $fecha = $utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $fecha = date("Y-m-d", strtotime($fecha));
        $consulta['sqldesde'] = $fecha . ' 00:00:00';

        //debo sumar un dia a la fecha desde
        $fecha = $utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
        $fecha = date("Y-m-d", strtotime("$fecha + 1 days"));
        $consulta['sqlhasta'] = $fecha . ' 00:00:00';

        if ($consulta['distribuidor'] != '' && $consulta['cliente'] == '') { //algo tengo en el distribuidor
            if ($consulta['distribuidor'][0] == '*') { ///estoy pidiendo todo los distribuidores
                $consulta['distribuidor'] = str_replace('*', '', $consulta['distribuidor']);
            }
            $distri = $organizacionManager->find(intval($consulta['distribuidor']));
            $tmpOrg = $organizacionManager->getTreeOrganizaciones($distri);
            foreach ($tmpOrg as $org) {
                if ($org->getTipoOrganizacion() == 2) {  //solo dejo pasar los clientes que son los que tienen servicios.
                    $organizaciones[] = $org;
                }
            }
        }

        if ($consulta['cliente'] != '') {
            if ($consulta['cliente'][0] == '*') { ///estoy pidiendo todo los clientes del distribuidor
                $consulta['cliente'] = str_replace('*', '', $consulta['cliente']);
                $distri = $organizacionManager->find(intval($consulta['cliente']));
                $organizaciones = $organizacionManager->findAllClientes($distri);
            } else {  //estoy pidiendo por un cliente puntual
                $organizaciones[] = $organizacionManager->find(intval($consulta['cliente']));
            }
        }

        //convierto las organizaciones en array;
        $ordIds = array();
        foreach ($organizaciones as $organizacion) {
            $orgIds[] = $organizacion->getId();
        }

        return $this->render('dist/InformeReportes/result.html.twig', array(
            'consulta' => $consulta,
            'consulta_json' => json_encode($consulta),
            'organizaciones' => $orgIds,
        ));
    }

    /**
     * @Route("/informe/reportes/obtener", name="dist_informe_reportes_obtener")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function obtenerAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        BackendManager $backendManager,
        UtilsManager $utilsManager
    ) {
        $fecha_desde = $request->get('fecha_desde');
        $fecha_hasta = $request->get('fecha_hasta');
        $idOrg = $request->get('id');

        $organizacion = $organizacionManager->find($idOrg);
        $dataServicios = $this->obtenerDatos($organizacion, $fecha_desde, $fecha_hasta, $servicioManager, $backendManager, $utilsManager);
        $data = array(
            'organizacion' => ($organizacion->getOrganizacionPadre() != null ? $organizacion->getOrganizacionPadre() . ' : ' : '') . $organizacion->getNombre(),
            'servicios' => $dataServicios,
        );

        $html = $this->renderView('dist/InformeReportes/result_body.html.twig', array(
            'servicios' => $dataServicios,
            'organizacion' => $organizacion,
        ));
        return new Response(json_encode(array(
            'id' => $idOrg,
            'html' => $html,
            'data' => json_encode($data),
        )), 200);
    }

    private function obtenerDatos($organizacion, $fecha_desde, $fecha_hasta, $servicioManager, $backendManager, $utilsManager)
    {
        $dataServicios = array();
        $servicios = $servicioManager->findAll($organizacion);
        foreach ($servicios as $servicio) {       //recorro todos los rervicios de la organizacion
            if (!is_null($servicio->getUltFechahora()) && $servicio->getUltFechahora() >= date_create($fecha_desde)) {
                //obtengo la info desde el backend.
                $dataH = $backendManager->informeReportes($servicio->getId(), $fecha_desde, $fecha_hasta, array('contar_reportes' => true));

                //reproceso la info obtenida desde el backend para que deje la data lista.
                $data = $servicioManager->informeReportes($servicio, $fecha_desde, $fecha_hasta, $dataH);
            } else {    //entro solo el ult remporte es anterior a la fecha de consulta
                $data = array(
                    'servicio_id' => $servicio->getId(),
                    'nombre' => $servicio->getNombre(),
                    'estado' => $servicio->getStrEstado(),
                    'estado_id' => $servicio->getEstado(),
                    'patente' => '',    //ojo con esto porque puede servir el dato
                    'equipo' => $servicio->getEquipo() != null ? $servicio->getEquipo() : '',
                    'tipo_servicio' => $servicio->getTipoServicio()->getNombre(),
                    'data' => array(
                        'fecha_inicio' => null,
                        'fecha_fin' => $servicio->getUltFechahora(),
                        'reportes' => '---'
                    ),
                    'cant_dias' => '---',
                    'total_dias' => '---',
                    'efectividad' => '---',
                );
            }
            if ($data) {
                //                if ($data['data']['reportes'] == 0) {
                //                    $data['data']['fecha_inicio'] = null;
                //                    $data['data']['fecha_fin'] = is_null($servicio->getUltFechahora()) ? null : $utilsManager->fechaUTC2local($servicio->getUltFechahora(), 'd-m-Y H:i:s');
                //                }
                $dataServicios[] = $data;
            }
        }
        return $dataServicios;
    }

    /**
     * @Route("/informe/reportes/exportar", name="dist_informe_reportes_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DIST_INFORME_REPORTES")
     */
    public function exportarAction(Request $request, ExcelManager $excelManager, UserLoginManager $userloginManager)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('data'), true);
        if (isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $excelManager->create("Informe de Reportes de Servicios");

            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Organización', 'width' => 20),
                'B' => array('title' => 'Servicio', 'width' => 20),
                'C' => array('title' => 'Tipo', 'width' => 20),
                'D' => array('title' => 'Estado', 'width' => 20),
                'E' => array('title' => 'Equipo', 'width' => 20),
                'F' => array('title' => 'Fecha Inicio', 'width' => 20),
                'G' => array('title' => 'Fecha Fin', 'width' => 20),
                'H' => array('title' => 'Reportes', 'width' => 20),
                'I' => array('title' => 'Cant. Días', 'width' => 20),
                'J' => array('title' => 'Total Días', 'width' => 20),
                'K' => array('title' => 'Efectividad (%)', 'width' => 20),
            ));
            $i = 7;
            //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
            foreach ($informe as $datos) {   //esto es para cada servicio
                foreach ($datos['servicios'] as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['organizacion']),
                        'B' => array('value' => $value['nombre']),
                        'C' => array('value' => $value['tipo_servicio']),
                        'D' => array('value' => $value['estado']),
                        'E' => array('value' => $value['equipo']),
                        'F' => array('value' => is_null($value['data']['fecha_inicio']) ? '---' : $value['data']['fecha_inicio']),
                        'G' => array('value' => is_null($value['data']['fecha_fin']) ? '---' : $value['data']['fecha_fin']),
                        'H' => array('value' => $value['data']['reportes']),
                        'I' => array('value' => $value['cant_dias']),
                        'J' => array('value' => $value['total_dias']),
                        'K' => array('value' => $value['efectividad']),
                    ));
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=reportesservicios.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('dist_informe_reportes', array(
                'id' => $userloginManager->getOrganizacion()->getId(),
            )));
        }
    }
}
