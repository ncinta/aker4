<?php

namespace App\Controller\dist;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Entity\Chip;
use App\Entity\Organizacion;
use App\Form\dist\ChipType;
use App\Form\dist\ChipTransferType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ChipManager;

class ChipController extends AbstractController
{

    /**
     * Lista todos los chips de la organizacion
     * @Route("/dist/chip/list", name="dist_chip_list")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CHIP_VER")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager, ChipManager $chipManager, UserLoginManager $userloginManager)
    {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Chips");

        $chips = $chipManager->findMisChips();

        return $this->render('dist/Chip/list.html.twig', array(
            'user' => $userloginManager->getUser(),
            'menu' => $this->getListMenu($userloginManager->getOrganizacion(), $userloginManager),
            'chips' => $chips,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, $userloginManager)
    {
        $menu = array();
        //agrega un chip en una distribuicion
        if ($userloginManager->isGranted('ROLE_CHIP_AGREGAR')) {
            $menu['Agregar Chip'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('dist_chip_new', array('id' => $organizacion->getId()))
            );
        }
        return $menu;
    }

    /**
     * muestra los datos del chip
     * @Route("/dist/chip/{id}/show", name="dist_chip_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CHIP_VER")
     */
    public function showAction(Request $request, Chip $chip, BreadcrumbManager $breadcrumbManager, UserLoginManager $userloginManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), $chip->getImei());
        $deleteForm = $this->createDeleteForm($chip->getId());

        return $this->render('dist/Chip/show.html.twig', array(
            'user' => $userloginManager->getUser(),
            'menu' => $this->getShowMenu($chip, $userloginManager),
            'chip' => $chip,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $chip 
     * @return array $menu
     */
    private function getShowMenu($chip, $userlogin)
    {
        $userOrg = $userlogin->getOrganizacion();
        $menu = array();
        if ($userlogin->isGranted('ROLE_CHIP_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('dist_chip_edit', array(
                    'id' => $chip->getId()
                ))
            );
        }
        if ($userlogin->isGranted('ROLE_CHIP_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * crear chip nuevo
     * @Route("/dist/chip/{id}/new", name="dist_chip_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @IsGranted("ROLE_CHIP_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $propietario,
        ChipManager $chipManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        $chip = $chipManager->createChip($propietario);

        $options['arrayStrEstados'] = $chip->getArrayStrEstado();
        $form = $this->createForm(ChipType::class, $chip, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $breadcrumbManager->pop();

            if ($chipManager->save($chip)) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado el chip <b>%s</b> en el sistema.', strtoupper($chip))
                );
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para crear el chip <b>%s</b> en el sistema.', strtoupper($chip)));
            }
        }


        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Chip");
        return $this->render('dist/Chip/new.html.twig', array(
            'chip' => $chip,
            'form' => $form->createView()
        ));
    }

    /**
     * modificar chip 
     * @Route("/chip/{id}/edit", name="dist_chip_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CHIP_EDITAR")
     */
    public function editAction(Request $request, Chip $chip, ChipManager $chipManager, BreadcrumbManager $breadcrumbManager)
    {

        $options['arrayStrEstados'] = $chip->getArrayStrEstado();
        $form = $this->createForm(ChipType::class, $chip, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            if ($chipManager->save($chip)) {
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($chip)));
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }


        $breadcrumbManager->push($request->getRequestUri(), "Editar Chip");
        return $this->render('dist/Chip/edit.html.twig', array(
            'chip' => $chip,
            'form' => $form->createView(),
        ));
    }

    /**
     * 
     * @Route("/chip/{id}/transfer", name="dist_chip_transfer",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CHIP_TRANSFERIR")
     */
    public function transferAction(
        Request $request,
        Chip $chip,
        ChipManager $chipManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager
    ) {
        $breadcrumbManager->push($request->getRequestUri(), "Transferir Chip");
        $orgLogin = $userloginManager->getOrganizacion();
        if ($request->getMethod() == 'POST') {
            $breadcrumbManager->pop();
            $tipo_transfer = $request->get('tipo_tranferencia');
            $new_org = $organizacionManager->find($request->get('new_organizacion'));
            $chipManager->tranferir($chip, $new_org, $tipo_transfer);
            $this->setFlash('success', sprintf('Se ha transferido el chip <b>%s</b> a la Organizacion <b>%s</b>', strtoupper($chip), strtoupper($new_org->getNombre())));
            return $this->redirect($breadcrumbManager->getVolver());
        }

        $organizaciones = $organizacionManager->getTreeOrganizaciones($orgLogin);
        return $this->render('dist/Chip/transferir.html.twig', array(
            'user' => $userloginManager->getUser(),
            'organizaciones' => $organizaciones,
            'organizacion' => $chip->getOrganizacion(),
            'chip' => $chip,
        ));
    }

    /**
     * Elimina a un chip del sistema
     */

    /**
     * @Route("/chip/{id}/delete", name="dist_chip_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CHIP_ELIMINAR")
     */
    public function deleteAction(Request $request, Chip $chip, ChipManager $chipManager, BreadcrumbManager $breadcrumbManager)
    {

        $form = $this->createDeleteForm($chip->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $breadcrumbManager->pop();
            $chipManager->deleteById($chip->getId());
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
