<?php

namespace App\Controller\dist;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Organizacion;
use App\Entity\Dominio;
use App\Form\dist\DominioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DominioManager;
use App\Model\app\Router\DominioRouter;

class DominioController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Lista todos los dominios del distribuidor
     * @Route("/dominio/{id}/list", name="dominio_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DOMINIO_VER")
     */
    public function listAction(Request $request, Organizacion $distribuidor, BreadcrumbManager $breadcrumbManager, DominioRouter $dominioRouter)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Listado de Dominios");
        $menu = array(
            1 => array(
                $dominioRouter->btnNew($distribuidor),
            ),
        );

        return $this->render('dist/Dominio/list.html.twig', array(
            'menu' => $dominioRouter->toCalypso($menu),
            'distribuidor' => $distribuidor,
            'dominios' => $distribuidor->getDominios()
        ));
    }

    private function getLogos()
    {
        return glob('images/logos/*.*');
    }

    /**
     * @Route("/dominio/{id}/new", name="dominio_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DOMINIO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $distribuidor, BreadcrumbManager $breadcrumbManager)
    {
        $dominio = new Dominio();
        $dominio->setOrganizacion($distribuidor);

        $form = $this->createForm(DominioType::class, $dominio);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                $em = $this->getEntityManager();
                $em->persist($dominio);
                $em->flush();

                $this->setFlash('success', sprintf('Se ha creado el nuevo dominio <b>%s</b>', strtoupper($dominio->getDominio())));

                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Dominio");
        return $this->render('dist/Dominio/new.html.twig', array(
            'distribuidor' => $distribuidor,
            'form' => $form->createView(),
            'logos' => $this->getLogos(),
        ));
    }

    /**
     * @Route("/dominio/{id}/edit", name="dominio_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DOMINIO_EDITAR")
     */
    public function editAction(Request $request, Dominio $dominio, BreadcrumbManager $breadcrumbManager)
    {
        $form = $this->createForm(DominioType::class, $dominio);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                $em = $this->getEntityManager();
                $em->persist($dominio);
                $em->flush();
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($dominio)));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', sprintf('Los datos no son correctos'));
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Editar Dominio");
        return $this->render('dist/Dominio/edit.html.twig', array(
            'dominio' => $dominio,
            'form' => $form->createView(),
            'logos' => $this->getLogos(),
        ));
    }

    /**
     * @Route("/dominio/{id}/delete", name="dominio_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_DOMINIO_ELIMINAR")
     */
    public function deleteAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, DominioManager $dominioManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Eliminar");
        if ($dominioManager->delete($id)) {
            $this->setFlash('success', 'El dominio ha sido eliminado.');
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
