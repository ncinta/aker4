<?php

namespace App\Controller\dist;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\Organizacion;
use App\Entity\MotivoActividadGpm;
use App\Form\dist\ClienteNewType;
use App\Form\dist\ClienteEditType;
use App\Form\gpm\MotivoActividadType;
use GMaps\Geocoder;
use GMaps\Map;
use GMaps\Marker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\EquipoManager;
use App\Model\app\Router\ClienteRouter;
use App\Model\app\Router\DistribuidorRouter;
use App\Model\app\ServicioManager;
use App\Model\app\ChipManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UsuarioManager;
use App\Model\app\PaisManager;
use App\Model\gpm\ConfigGpmManager;

class ClienteController extends AbstractController
{

    private function getLogos()
    {
        return glob('images/logos/*.*');
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Lista todos los clientes del sistema.
     * @Route("/dist/cliente/{id}/list", name="distribuidor_cliente_list")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CLIENTE_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        OrganizacionManager $organizacionManager,
        BreadcrumbManager $breadcrumbManager,
        ClienteRouter $clienteRouter,
        DistribuidorRouter $distribuidorRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Clientes");

        $clientes = $organizacionManager->findAllClientes($organizacion);

        return $this->render('dist/Cliente/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion, $clienteRouter, $distribuidorRouter),
            'clientes' => $clientes
        ));
    }

    /**
     * @Route("/dist/cliente/{id}/proyectoshow", name="cliente_proyecto_show")
     * @Method({"GET", "POST"}) 
     */
    public function showproyectoAction(Request $request, Organizacion $organizacion, BreadcrumbManager $breadcrumbManager)
    {

        $breadcrumbManager->push($request->getRequestUri(), "Configuración de proyecto");
        return $this->render('dist/ProyectosGpm/show.html.twig', array(
            'menu' => array('Modificar Configuración' => array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('cliente_proyecto_edit', array(
                    'id' => $organizacion->getId()
                ))
            )),
            'organizacion' => $organizacion
        ));
    }

    /**
     * @Route("/dist/cliente/{id}/proyectoedit", name="cliente_proyecto_edit")
     * @Method({"GET", "POST"}) 
     */
    public function editproyectoAction(Request $request, Organizacion $organizacion, BreadcrumbManager $breadcrumbManager, ConfigGpmManager $configgpmManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Modificar Configuración de proyecto");

        $config = $configgpmManager->create($organizacion);
        if ($request->getMethod() == 'POST') {
            $configuraciones = $config->getArrayConfig();
            if ($request->get('form_configuraciones') != null) {
                foreach ($request->get('form_configuraciones') as $key => $value) {
                    $configuraciones[$key]['value'] = true;
                }
            } else {
                foreach ($configuraciones as $key => $value) {
                    $configuraciones[$key]['value'] = false;
                }
            }
            $config->setOrganizacion($organizacion);
            $config->setVista_actividad($configuraciones);
            $config = $configgpmManager->save($config);

            return $this->redirect($this->generateUrl('cliente_proyecto_show', array(
                'id' => $organizacion->getId()
            )));
            // die('////<pre>' . nl2br(var_export($configuraciones, true)) . '</pre>////');
        }
        return $this->render('dist/ProyectosGpm/edit.html.twig', array(
            'config' => $config,
            'organizacion' => $organizacion
        ));
    }


    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, $clienteRouter, $distribuidorRouter)
    {
        $menu = array(
            1 => array(
                $clienteRouter->btnNew($organizacion),
            ),
        );
        return $distribuidorRouter->toCalypso($menu);
    }

    private function getCliente($id, $organizacionManager)
    {
        $cliente = $organizacionManager->find($id);
        if (!$cliente) {
            throw $this->createNotFoundException('Código de Cliente no accesible.');
        }
        return $cliente;
    }

    /**
     * @Route("/dist/cliente/{id}/show/{tab}", name="distribuidor_cliente_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_CLIENTE_VER")
     */
    public function showAction(
        Request $request,
        Organizacion $cliente,
        $tab,
        EquipoManager $equipoManager,
        BreadcrumbManager $breadcrumbManager,
        ClienteRouter $clienteRouter,
        DistribuidorRouter $distribuidorRouter,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        UserLoginManager $userloginManager,
        ChipManager $chipManager,
        ReferenciaManager $referenciaManager,
        OrganizacionManager $organizacionManager
    ) {

        if ($tab == 'main') {
            $breadcrumbManager->push($request->getRequestUri(), sprintf('%s (%s)', $cliente->getNombre(), $tab));
        } else {
            $breadcrumbManager->push($request->getRequestUri(), sprintf('%s', $tab));
        }

        $deleteForm = $this->createDeleteForm($cliente->getId());
        $request->getSession()->set('idorg', $cliente->getId());
        $param = array(
            'organizacion' => $cliente,
            'delete_form' => $deleteForm->createView(),
            'tab' => $tab,
            'panel' => $this->getPanelMenu($cliente, $userloginManager, $organizacionManager)
        );
        //die('////<pre>'.nl2br(var_export($tab, true)).'</pre>////');
        switch ($tab) {
            case 'usuarios':
                $param['usuarios'] = $usuarioManager->findAll($cliente);
                $param['menu']['Agregar Usuario'] = array(
                    'imagen' => 'icon-plus icon-blue',
                    'url' => $this->generateUrl('usuario_new', array(
                        'idorg' => $cliente->getId()
                    ))
                );
                break;
            case 'flota':
                $param['servicios'] = $servicioManager->findAll($cliente);
                if ($userloginManager->isGranted('ROLE_APMON_FLOTA_ADMIN')) {
                    $param['menu']['Servicio'] = array(
                        'imagen' => 'icon-plus icon-blue',
                        'url' => $this->generateUrl('app_servicio_new', array('idequipo' => 0, 'cliente' => $cliente->getId()))
                    );
                }
                $param['menu']['Equipo'] = array(
                    'imagen' => 'icon-plus icon-blue',
                    'url' => $this->generateUrl('sauron_equipo_new', array(
                        'idorg' => $cliente->getId()
                    ))
                );
                $param['menu']['Asignar a Usuario'] = array(
                    'imagen' => '',
                    'extra' => sprintf("onclick=batchasignarusuario(%s) id=btnAsignarUsuario style=display:none", $cliente->getId()),
                    'url' => '#btnAsignarUsuario',
                );
                if ($userloginManager->isGranted('ROLE_SECURITY')) {
                    $param['menu']['Pedir Datos'] = $this->getBtnPedirDatos();
                }

                break;
            case 'chips':
                //traigo todo los chips del dist.
                $param['chips'] = $chipManager->findAll($cliente);
                $param['user'] = $userloginManager->getUser();
                //die('////<pre>'.nl2br(var_export($tab, true)).'</pre>////');
                $param['menu']['Agregar Chips'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('dist_chip_new', array(
                        'id' => $cliente->getId()
                    ))
                );

                break;
            case 'combustible':
                $param['servicios'] = $servicioManager->findAll($cliente);

                break;
            case 'referencias':
                $param['referencias'] = $referenciaManager->findAsociadas($cliente);

                $param['menu']['Agregar Referencias'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('referencia_new', array(
                        'id' => $cliente->getId()
                    ))
                );
                $param['menu']['Categorias'] = array(
                    'imagen' => 'icon-tasks icon-blue',
                    'url' => $this->generateUrl('sauron_categoria_list', array(
                        'idorg' => $cliente->getId()
                    ))
                );
                $param['menu']['Grupos'] = array(
                    'imagen' => 'icon-th-large icon-blue',
                    'url' => $this->generateUrl('sauron_grupo_referencia_list', array(
                        'idorg' => $cliente->getId()
                    ))
                );
                $param['menu']['Compartir'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("compartir") id=btnCompartir style=display:none',
                    'url' => '#compartir',
                );
                $param['menu']['Copiar'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("copiar") id=btnCopiar style=display:none',
                    'url' => '#copiar',
                );
                $param['menu']['Mover'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("mover") id=btnMover style=display:none',
                    'url' => '#mover',
                );
                break;
            case 'deposito':
                $param['equipos'] = $equipoManager->findAll($cliente);
                $param['equiposdep'] = $equipoManager->findAllEnDeposito($cliente);
                $param['servicios_disponibles'] = count((array)$servicioManager->findAllServiciosSinAsignar($cliente)) > 0;

                $param['menu']['Agregar Equipo'] = array(
                    'imagen' => 'icon-plus-sign icon-blue',
                    'url' => $this->generateUrl('sauron_equipo_new', array(
                        'idorg' => $cliente->getId()
                    ))
                );
                break;

            default:
                $param['menu'] = $this->getShowMenu($cliente, $clienteRouter, $distribuidorRouter);
                $param['panel'] = $this->getPanelMenu($cliente, $userloginManager, $organizacionManager);
                break;
        }
        return $this->render('dist/Cliente/show.html.twig', $param);
    }

    public function getPanelMenu($cliente, $userloginManager, $organizacionManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            $menu['Configurar Módulos'] = array(
                'url' => $this->generateUrl('distribuidor_asociar_modulo', array('id' => $cliente->getId())),
                'descripcion' => 'Módulos de aplicaciones configurados en el cliente.',
            );
        }

        if ($userloginManager->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            $menu['Informes'] = array(
                'url' => $this->generateUrl('dist_cliente_informes', array('id' => $cliente->getId())),
                'descripcion' => 'Pantalla para acceder a los informes que visualiza el cliente.',
            );
        }

        if ($organizacionManager->isModuloEnabled($cliente, 'CLIENTE_EVENTOS')) {
            //administracion de eventos
            if ($userloginManager->isGranted('ROLE_EVENTO_ADMIN')) {
                $menu['Contactos'] = array(
                    'url' => $this->generateUrl('main_contacto_list', array('id' => $cliente->getId())),
                    'descripcion' => 'Accede a los contactos del cliente.'
                );
                $menu['Eventos'] = array(
                    'url' => $this->generateUrl('main_evento_list', array('id' => $cliente->getId())),
                    'descripcion' => 'Configuración de los eventos del clientes'
                );
                $menu['Históricos de Eventos'] = array(
                    'url' => $this->generateUrl('app_eventohistorico_list', array(
                        'id' => $cliente->getId()
                    )),
                    'descripcion' => 'Accede a los eventos históricos generados en el cliente'
                );
                //$menu['Historico de Pánicos'] = array(
                //    'url' => $this->generateUrl('main_panico_historico', array('id' => $cliente->getId())),
                //    'descripcion' => 'Accede a los pánicos históricos generados en el cliente.'
                //);
            }
        }
        if ($organizacionManager->isModuloEnabled($cliente, 'CLIENTE_CHOFER')) {
            $menu['Choferes'] = array(
                'url' => $this->generateUrl('apmon_chofer_list', array('idorg' => $cliente->getId())),
                'descripcion' => 'Accede a los choferes registrados del cliente.'
            );
            $menu['Depósito de Ibutton'] = array(
                'url' => $this->generateUrl('apmon_ibutton_list', array('idorg' => $cliente->getId())),
                'descripcion' => 'Depósito de ibutton registrado en el cliente.'
            );
        }
        if ($organizacionManager->isModuloEnabled($cliente, 'CLIENTE_MANTENIMIENTO')) {
            $menu['Menú de Mantenimiento'] = array(
                'url' => $this->generateUrl('tarea_mant_list', array('idOrg' => $cliente->getId())),
                'descripcion' => 'Accede al menú de mantenimiento de servicios.'
            );
        }
        if ($userloginManager->isGranted('ROLE_FLOTA_ADMIN')) {
            $menu['Bitácora de Servicios'] = array(
                'url' => $this->generateUrl('sauron_bitacoraservicio_list', array('id' => $cliente->getId())),
                'descripcion' => 'Accede la bitácora de los servicios del cliente.'
            );
            $menu['Bitácora de Equipos'] = array(
                'url' => $this->generateUrl('sauron_bitacoraequipo_list', array('id' => $cliente->getId())),
                'descripcion' => 'Accede la bitácora de los equipos del cliente.'
            );
        }
        if (
            $userloginManager->isGranted('ROLE_COMBUSTIBLE_IMPORTACION') &&
            $userloginManager->isModuloEnabled($cliente, 'MANAGER_COMBUSTIBLE')
        ) {
            $menu['Importar Combustible desde Excel'] = array(
                'url' => $this->generateUrl('fuel_import_index', array('id' => $cliente->getId())),
                'descripcion' => 'Importa las cargas de combustible para el clientes desde un archivo excel.',
            );
        }

        if ($organizacionManager->isModuloEnabled($cliente, 'CLIENTE_PROYECTO')) {
            $menu['Proyecto'] = array(
                'url' => $this->generateUrl('cliente_proyecto_show', array('id' => $cliente->getId())),
                'descripcion' => 'Configura los campos que se van a utilizar en las actividades de los proyectos.',
            );
        }

        return $menu;
    }

    private function getBtnPedirDatos()
    {
        return array(
            'imagen' => '',
            'extra' => 'onclick=batchgetdata("getdata") id=btnGetData style=display:none',
            'url' => '#getdata',
        );
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $cliente 
     * @return array $menu
     */
    private function getShowMenu($cliente, $clienteRouter, $distribuidorRouter)
    {
        $menu = array(
            1 => array(
                $clienteRouter->btnEdit($cliente),
                $clienteRouter->btnDelete($cliente),
            ),
        );
        // die('////<pre>'.nl2br(var_export($menu, true)).'</pre>////');
        return $distribuidorRouter->toCalypso($menu);
    }

    /**
     * @Route("/dist/cliente/{id}/new", name="distribuidor_cliente_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ORGANIZACION_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $distribuidor,
        OrganizacionManager $organizacionManager,
        PaisManager $paisManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        $cliente = $organizacionManager->create($distribuidor, 2); //creo como cliente.
        if (!$cliente) {
            throw $this->createNotFoundException('Código de Cliente no accesible.');
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Cliente");
        $options['calypso.pais'] = $paisManager->getArrayPaises();
        $form = $this->createForm(ClienteNewType::class, $cliente, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                if ($organizacionManager->save($cliente)) {
                    $this->setFlash('success', sprintf('Se ha creado el cliente <b>%s</b> en el sistema. Configure los módulos de programa habilitados.', strtoupper($cliente)));

                    return $this->redirect($this->generateUrl('distribuidor_asociar_modulo', array('id' => $cliente->getId())));
                } else {
                    $error = 'No se pudo crear el cliente. Avise a su Distribuidor.';
                }
            } else {
                $error = 'Los datos no son válidos';
            }
        }
        return $this->render('dist/Cliente/new.html.twig', array(
            'cliente' => $cliente,
            'form' => $form->createView(),
            'mapa' => $this->createMap($organizacionManager->find($distribuidor->getId())),
            'geocoder' => new Geocoder(),
            'logos' => $this->getLogos(),
        ));
    }

    private function createMap($organizacion)
    {
        //mapa para ubicar a la organizacion
        $mapa = new Map();
        $mapa->setSize('80%', '250px');
        $mapa->setDebug(true);
        if ($organizacion->getLatitud() == 0) {
            $mapa->setIniZoom(2);
        } else {
            $mapa->setIniZoom(16);
        }
        $mapa->setIniLatitud($organizacion->getLatitud());
        $mapa->setIniLongitud($organizacion->getLongitud());
        $mapa->setIncludeJQuery(true);

        $marcador = new Marker('punto_edicion', $organizacion->getLatitud(), $organizacion->getLongitud(), $organizacion);
        $marcador->setDraggable(true); // habilita la posibilidad de mover un marcador
        $marcador->setRaiseOnDrag(true); // permite que el marcador se "levante" del suelo al arrastrarlo

        $marcador->setClickListener('clickMarcador');
        $marcador->setDragListener('dragMarcador');
        $marcador->setDragendListener('finDragMarcador');
        $mapa->addMarker($marcador);
        return $mapa;
    }

    /**
     * @Route("/dist/cliente/{id}/edit", name="distribuidor_cliente_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @IsGranted("ROLE_ORGANIZACION_EDITAR")
     */
    public function editAction(
        Request $request,
        Organizacion $cliente,
        BreadcrumbManager $breadcrumbManager,
        PaisManager $paisManager,
        OrganizacionManager $organizacionManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Editar Cliente");

        $options['calypso.pais'] = $paisManager->getArrayPaises();
        $form = $this->createForm(ClienteEditType::class, $cliente, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($organizacionManager->save($cliente)) {
                    $breadcrumbManager->pop();
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($cliente)));
                    return $this->redirect($breadcrumbManager->getVolver());
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        return $this->render('dist/Cliente/edit.html.twig', array(
            'cliente' => $cliente,
            'form' => $form->createView(),
            'mapa' => $this->createMap($cliente),
            'geocoder' => new Geocoder(),
            'logos' => $this->getLogos(),
        ));
    }

    /**
     * @IsGranted("ROLE_ORGANIZACION_EDITAR")
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/cliente/{id}/informes", name="dist_cliente_informes",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     */
    public function informesAction(Request $request, $id, OrganizacionManager $organizacionManager)
    {

        $cliente = $this->getCliente($id, $organizacionManager);
        $menu = array();
        if ($organizacionManager->isModuloEnabled($cliente, 'CLIENTE_MANTENIMIENTO')) {
            $menu[] = array(
                'label' => 'Tareas de Mantenimiento',
                'url' => $this->generateUrl('informe_mantenimiento', array('id' => $cliente->getId())),
                'title' => 'Informe de estados de tareas de mantenimiento vehicular sobre los equipos del cliente.',
            );
            $menu[] = array(
                'label' => 'Estado de Mantenimientos',
                'url' => $this->generateUrl('informe_status_mantenimiento', array('id' => $cliente->getId())),
                'title' => 'Informe de estados de los mantenimientos para la flota del cliente',
            );
        }
        $menu[] = array(
            'label' => 'Canbus Historico',
            'url' => $this->generateUrl('informe_canbus_historico', array('id' => $cliente->getId())),
            'title' => 'Informe de historial de tramas con canbus para los sevicios que lo tienen habilitado. Muestra todas las tramas que tengan información de Canbus.',
        );
        /*$menu[] = array(
            'label' => 'Canbus Resumido',
            'url' => $this->generateUrl('informe_canbus_resumen', array('id' => $cliente->getId())),
            'title' => 'Informe resumido de viajes con canbus para los sevicios que lo tienen habilitado.',
        );*/
        $menu[] = array(
            'label' => 'Detenciones',
            'url' => $this->generateUrl('informe_detenciones', array('id' => $cliente->getId())),
            'title' => 'Informe de detenciones de servicios.',
        );
        $menu[] = array(
            'label' => 'Frenadas, Aceleraciones y Giros',
            'url' => $this->generateUrl('informe_infraccion', array('id' => $cliente->getId())),
            'title' => 'Informe de infracciones tales como aceleración y disminución de velocidad bruscas.',
        );
        $menu[] = array(
            'label' => 'Tiempos y Velocidades',
            'url' => $this->generateUrl('informe_velocidades', array('id' => $cliente->getId())),
            'title' => 'Informe de tiempos y velocidades máximas alcanzadas por lo vechiculos. También reporta horómetro.',
        );
        $menu[] = array(
            'label' => 'Distancias recorridas',
            'url' => $this->generateUrl('informe_distancias', array('id' => $cliente->getId())),
            'title' => 'Informe de distancias recorridas por los vechiculos. También reporta horómetro.',
        );
        return $this->render('dist/Cliente/informes.html.twig', array(
            'listmenu' => $menu,
            'cliente' => $cliente,
        ));
    }

    /**
     * Elimina un distribuidor del sistema
     * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
     * @Route("/dist/cliente/{id}/delete", name="distribuidor_cliente_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @IsGranted("ROLE_ORGANIZACION_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager
    ) {

        $form = $this->createDeleteForm($organizacion->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getDoctrine()->getManager();
            if (!$organizacion) {
                throw $this->createNotFoundException('Imposible encontrar la organizacion para borrar.');
            }

            $ok = true;
            if (count((array)$organizacion->getEquiposPropios()) > 0) {
                $str = sprintf('No se ha podido eliminar la organizacion <b>%s</b>. Tiene equipos propios asociados.', $organizacion->getNombre());
                $this->setFlash('success', $str);
                $ok = false;
            }
            if ($ok) {
                try {
                    //borrado de servicios eliminados.
                    if ($organizacion->getServicios() != null) {
                        foreach ($organizacion->getServicios() as $servicio) {
                            $em->remove($servicio);
                        }
                    }
                    //borrando contacto
                    if ($organizacion->getContactos() != null) {
                        foreach ($organizacion->getContactos() as $contacto) {
                            $em->remove($contacto);
                        }
                    }

                    if ($organizacion->getGrupoReferencias() != null) {
                        //borrando gruposreferencia
                        foreach ($organizacion->getGrupoReferencias() as $gr) {
                            $em->remove($gr);
                        }
                    }
                    $str = sprintf('Se ha eliminado la organizacion <b>%s</b>', $organizacion->getNombre());
                    $organizacionManager->delete($organizacion);
                    $this->setFlash('success', $str);
                    return $this->redirect($this->generateUrl('dist_homepage'));
                } catch (\Exception $exc) {
                    $str = sprintf('No se ha podido eliminar la organizacion <b>%s</b>', $organizacion->getNombre());
                    $this->setFlash('success', $str);
                    return $this->redirect($this->generateUrl('dist_homepage'));
                }
            } else {
                $str = sprintf('No se ha podido eliminar la organizacion. Posee equipos propios <b>%s</b>', $organizacion->getNombre());
                $this->setFlash('error', $str);
                return $this->redirect($this->generateUrl('dist_homepage'));
            }
        }
    }
}
