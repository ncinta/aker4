<?php

namespace App\Controller\dist;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\dist\InformeReportesType;
use GMaps\Geocoder;
use App\Form\app\PanicoRegistroType;
use App\Form\dist\EventoFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\ReferenciaManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\GmapsManager;
use App\Model\app\EventoManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\GeocoderManager;

class MonitorController extends AbstractController
{

    /**
     * Lista todos los registro de la bitacora de servicios
     * @Route("/monitor/{id}/index", name="dist_monitor_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function indexAction(
        Request $request,
        $id,
        OrganizacionManager $organizacionManager,
        EventoManager $eventoManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        EventoHistorialManager $eventoHistManager
    ) {

        $organizacion = $organizacionManager->find($id);
        $childs = $organizacionManager->getTreeClientes($organizacion);
        $options = array('eventos' => $eventoManager->findAllByClientes($childs));
        $evento = 0;
        $atendido = 0;
        $eventos = null;
        $datos = null;
        $f_hasta = date('d-m-Y', time());
        $f_desde = date('d-m-Y', strtotime('-1 week'));
        if (!is_null($request->get('f_desde')) && !is_null($request->get('f_hasta'))) {
            $fecha_desde = $request->get('f_desde');
            $fecha_hasta = $request->get('f_hasta');
            $evento = $request->get('evento_form');
            $atendido = $request->get('atendido');
            // $eventos = $this->returnEventos($f_desde, $f_hasta, $evento, $childs, $atendido);
        } else {
            $breadcrumbManager->push($request->getRequestUri(), "Monitor");
            $fecha_desde = $utilsManager->datetime2sqltimestamp($f_desde, true);
            $fecha_hasta = $utilsManager->datetime2sqltimestamp($f_hasta, false);
        }
        if ($request->getMethod() == 'POST') {
            $id = $request->get('id');
            if (!is_null($request->get('bitacoraform'))) { //viene por formulario
                $form = $request->get('bitacoraform');
                $f_desde = $form['fecha_desde'];
                $f_hasta = $form['fecha_hasta'];
                $fecha_desde = $utilsManager->datetime2sqltimestamp($f_desde, true);
                $fecha_hasta = $utilsManager->datetime2sqltimestamp($f_hasta, false);
                $evento = intval($form['evento']);
                $atendido = (isset($form['atender']) ? 1 : 0);
                $eventos = $this->returnEventos($fecha_desde, $fecha_hasta, $evento, $childs, $atendido, $eventoHistManager);
            } else {  //viene por ajax
                $evento = $request->get('evento');
                $atendido = (isset($form['atender']) ? 1 : 0);
                $eventos = $this->returnEventos($fecha_desde, $fecha_hasta, $evento, $childs, $atendido, $eventoHistManager);
                //   die('////<pre>'.nl2br(var_export(count($eventos), true)).'</pre>////');
                $html = $this->renderView("dist/Monitor/tabla.html.twig", array(
                    'eventos' => $eventos,
                    'atendido' => $atendido,
                    'f_desde' => $f_desde,
                    'f_hasta' => $f_hasta,
                    'evento_form' => $evento,
                    'id' => $id,
                    'idOrg' => $organizacion->getId(),
                ));
                return new Response($html, 200);
            }
        }

        return $this->render('dist/Monitor/index.html.twig', array(
            'f_desde' => $f_desde,
            'f_hasta' => $f_hasta,
            'evento_form' => $evento,
            'atendido' => $atendido,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'eventos' => $eventos,
            'id' => $id,
            'idOrg' => $organizacion->getId(),
            'form' => $form = $this->createForm(EventoFormType::class, null, $options)->createView()
        ));
    }

    private function returnEventos($fecha_desde, $fecha_hasta, $evento, $childs, $atendido, $eventoHistManager)
    {
        $datos = $this->getEventos($fecha_desde, $fecha_hasta, $evento, $childs, $atendido, $eventoHistManager);
        $eventos = $this->getArrayEventos($datos);
        return $eventos;
    }

    private function getEventos($fecha_desde, $fecha_hasta, $evento, $childs, $atendido, $eventoHistManager)
    {
        $eventos = $eventoHistManager->findAllByClientes($childs, $fecha_desde, $fecha_hasta, $evento, $atendido);
        return $eventos;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->setFlash($action, $value);
    }

    protected function getArrayEventos($datos)
    {
        $eventos = array();
        $fecha = new \DateTime();
        foreach ($datos as $value) {
            $isNew = $value->getFecha() != null ? $value->getFecha() > $fecha : null;
            $eventos[] = array(
                'alarma' => $value,
                'isNew' => $isNew,
                'prioridad' => $value->getEvento()->getPrioridad(),
                'fecha' => $value->getCreatedAt(),
                'operador' => $value->getEjecutor(),
                'color' => $value->getEvento()->getColor(),
                'tipoEvento' => $value->getEvento()->getNombre(),
                'descripcion' => 'ok',
            );
        }
        return $eventos;
    }

    /**
     * @Route("/monitor/{id}/{idOrg}/{f_desde}/{f_hasta}/{evento}/{atendido}/eventoatender", name="dist_monitor_eventoatender",
     *     requirements={
     *         "id": "\d+",
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function eventoatenderAction(
        Request $request,
        $id,
        $idOrg,
        $f_desde,
        $f_hasta,
        $evento,
        $atendido,
        OrganizacionManager $organizacionManager,
        EventoManager $eventoManager,
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        EventoHistorialManager $eventoHistManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        GeocoderManager $geocoderManager
    ) {

        $eventohist = $eventoHistManager->findById($id);
        $breadcrumbManager->push($request->getRequestUri(), 'Atender(' . $eventohist->getEvento()->getNombre() . ')');
        if (!$eventohist) {
            throw $this->createNotFoundException('Código de evento no encontrado');
        }
        $form = $this->createForm(PanicoRegistroType::class, $eventohist);

        if ($request->getMethod() == 'POST') {
            $eventohist->setRespuesta($request->get('panico')['respuesta']);
            if ($eventoHistManager->registrar($eventohist)) {
                $breadcrumbManager->pop();
            }
            $organizacion = $organizacionManager->find($idOrg);
            //die('////<pre>'.nl2br(var_export($atendido, true)).'</pre>////');
            return $this->redirect($this->generateUrl('dist_monitor_index', array(
                'id' => $organizacion->getId(),
                'f_desde' => $f_desde,
                'f_hasta' => $f_hasta,
                'evento_form' => $evento,
                'atendido' => $atendido,
            )));
        }
        $servicio = $eventohist->getServicio();
        $referencias = $referenciaManager->findAsociadas($servicio->getOrganizacion());
        $registros = $this->getRegistro($request, $servicio, $referencias, $utilsManager, $servicioManager, $geocoderManager); //traigo los registros del servicio para mostrar en mapa
        //aca creo el mapa para mostrar el panicso
        $mapa = $gmapsManager->createMap(true);
        $mapa->setSize('100%', '240px');
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($servicio->getUltLatitud());
        $mapa->setIniLongitud($servicio->getUltLongitud());
        $gmapsManager->addMarkerServicio($mapa, $servicio, true);

        //agrego las referencias al mapa        
        foreach ($referencias as $referencia) {
            $gmapsManager->addMarkerReferencia($mapa, $referencia, true);
        }
        $dataEvento = json_decode($eventohist->getData(), true);
        if (isset($dataEvento['inicio'])) { //si tengo clave inicio tengo lat/long los demas no lo traen
            $gmapsManager->addMarkerPosicion($mapa, $eventohist->getId() . '_inicio', $dataEvento['inicio'], null, 'INICIO');
            $gmapsManager->addMarkerPosicion($mapa, $eventohist->getId() . '_fin', $dataEvento['fin'], null, 'FIN');
        }
        $registros['mapa'] = $mapa;
        return $this->render('dist/Monitor/show_evento.html.twig', array(
            'servicio' => $servicio,
            'servicio' => $servicio,
            'eventoHist' => $eventohist,
            'evento' => $dataEvento,
            'idOrg' => $idOrg,
            'mapa' => $mapa,
            'datos' => $registros['datos'],
            'form' => $form->createView(),
            'f_desde' => $f_desde,
            'f_hasta' => $f_hasta,
            'evento_form' => $evento,
            'atendido' => $atendido,
        ));
    }

    public function getRegistro(Request $request, $servicio, $referencias, $utilsManager, $servicioManager, $geocoderManager)
    {
        $datos = $extras = array();
        //rescato la ultima trama
        $trama = json_decode($servicio->getUltTrama(), true);
        if (!is_null($trama)) {
            $datos['Fecha Evento'] = $utilsManager->fechaUTC2local(date_create($trama['fecha']), 'd/m/Y H:i:s');
            if (isset($trama['posicion'])) {
                $datos['Lat/Lon'] = $trama['posicion']['latitud'] . ', ' . $trama['posicion']['longitud'];
                $datos['Rumbo'] = $trama['posicion']['direccion'] . '°';
                $datos['Velocidad'] = $trama['posicion']['velocidad'] . ' km/h';
            } else {
                $datos['Lat/Lon'] = '---';
                $datos['Rumbo'] = '---';
                $datos['Velocidad'] = '---';
            }

            if (isset($trama['posicion'])) {
                //hago la cercania.            
                $extras['cercania'] = null;
                if ($referencias) {
                    $cercanas = $servicioManager->buscarCercaniaPosicion(
                        $trama['posicion']['latitud'],
                        $trama['posicion']['longitud'],
                        $referencias,
                        1
                    );
                    $extras['cercania'] = is_null($cercanas[0]) ? '---' : $cercanas[0]['leyenda'];
                }
                //obtengo la dirección.
                $direc = $geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                $extras['domicilio'] = $direc;
            }
        }
        return array(
            'datos' => $datos,
            'extras' => $extras,
        );
    }

    /**
     * @Route("/monitor/refreshmapa", name="dist_monitor_refreshmapa",
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function refreshmapaAction(Request $request, EventoHistorialManager $eventoHistManager, GmapsManager $gmapsManager)
    {
        $id = $request->get('id');
        $eventoHist = $eventoHistManager->findById($id);
        $servicio = $eventoHist->getServicio();
        $data = array(
            'id_sc' => 'servicio_' . $servicio->getId(),
            'latitud' => $servicio->getUltLatitud(),
            'longitud' => $servicio->getUltLongitud(),
            'texto' => $servicio->getUltVelocidad() . ' km/h',
            'visible' => true,
            'new_icono' => $gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
        );

        return new Response(json_encode(array('data' => $data)));
    }
}
