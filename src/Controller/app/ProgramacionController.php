<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\ProgramacionManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Entity\Servicio;
use App\Model\app\GeocoderManager;

class ProgramacionController extends AbstractController
{

    private $programacionManager;
    private $servicioManager;
    private $breadcrumbManager;
    private $userloginManager;
    private $gmapsManager;
    private $referenciaManager;
    private $geocoderManager;

    function __construct(
        ProgramacionManager $programacionManager,
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UserloginManager $userloginManager,
        GmapsManager $gmapsManager,
        ReferenciaManager $referenciaManager,
        GeocoderManager $geocoderManager
    ) {
        $this->programacionManager = $programacionManager;
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
        $this->referenciaManager = $referenciaManager;
        $this->geocoderManager = $geocoderManager;
    }

    /**
     * @Route("/app/prg/{id}/config/default", name="main_programacion_default",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("{ROLE_SAURON_PROGRAMACION, ROLE_PRG_VARIABLES_CLIENTE, ROLE_PRG_TELECOMANDOS_CLIENTE}")
     */
    public function defaultAction(Request $request, Servicio $servicio)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $this->programacionManager->setDefault($servicio);

        $this->setFlash('success', 'Se agregaron los valores por defecto, ahora puede configurar cada uno.');

        return $this->redirect($this->generateUrl('main_programacion_edit', array(
            'id' => $servicio->getId(),
            'default' => true
        )));
    }

    /**
     * @Route("app/programacion/{id}/{default}/edit", name="main_programacion_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("{ROLE_SAURON_PROGRAMACION, ROLE_PRG_VARIABLES_CLIENTE, ROLE_PRG_TELECOMANDOS_CLIENTE}") 
     */
    public function editAction(Request $request, Servicio $servicio, $default)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
            $this->programacionManager->setDefault($servicio);

            //esto es para armar el formulario
            $parametros = $this->servicioManager->getProgramacionVars($servicio);
            $form = $this->createPrgForm($servicio, $parametros, is_null($default) ? false : $default);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->breadcrumbManager->pop();
                $data = $form->getData();
                //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');

                $prog2Run = null;

                foreach ($data as $key => $value) {
                    $param = $this->programacionManager->setValue($key, $value);   //grabo el valor
                    if ($param) {   //se hizo la grabacion de la variable, por lo que debo ejecutarla luego.
                        if ($this->programacionManager->isChild($param)) {   //es variable hija.
                            $parmPadre = $this->programacionManager->getParametroPadre($param->getVariable()->getVariablePadre(), $servicio);   //grabo el valor
                            $prog2Run[$parmPadre->getId()] = $parmPadre;
                        } else {
                            $prog2Run[$param->getId()] = $param;
                        }
                    }
                }

                foreach ($prog2Run as $prog) {
                    //inicio la programacion remota del equipo.
                    if ($this->programacionManager->initConfig($servicio)) {   //inicializo las progremotas.
                        //recorro todos los datos del form y lo cargo en la prog remota temporal.
                        $this->programacionManager->setConfig($prog);

                        //armo la programación remota.
                        $idProg = $this->programacionManager->executeConfig();
                        if ($idProg !== false) {
                            $this->setFlash('success', 'La configuración se ha realizado correctamente.');
                            $res = $this->programacionManager->runProgramacion($idProg);
                            if ($res < 0) {
                                $this->setFlash('error', sprintf('No se ha realizado la programación remota. Avise a su distribuidor, Error: %d', $res));
                            } elseif ($res > 0) {
                                $this->setFlash('success', 'La programación remota ha sido procesada correctamente.');
                            } else {
                            }
                        } else {
                            $this->setFlash('error', 'No se ha realizado el cambio de la configuración.');
                        }
                    } else {
                        $this->setFlash('error', 'No tiene equipo asociado, se guarda para programar luego.');
                    }
                }

                return $this->redirect($this->breadcrumbManager->getVolver());
            }

            $this->breadcrumbManager->push($request->getRequestUri(), "Reprogramación");
            return $this->render('app/Programacion/edit.html.twig', array(
                'servicio' => $servicio,
                'parametros' => $parametros,
                'form' => $form->createView(),
            ));
        } else {  //el servicio no esta habilitado. Avisar y volver.
            $this->setFlash('error', 'El servicio no está habilitado. No se puede reprogramar');
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    private function createPrgForm($servicio, $parametros, $default = false)
    {
        return $this->programacionManager->createPrgForm($servicio, $parametros, $default = false);
    }

    /**
     * @Route("/app/prg/{id}/corte", name="main_programacion_corte",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CORTEMOTOR")
     */
    public function corteAction(Request $request, Servicio $servicio)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {
            return $this->render('app/Programacion/corte.html.twig', array(
                'servicio' => $servicio,
                'form' => $this->createCorteForm($servicio->getId())->createView(),
                'extras' => $this->createExtras($servicio),
                'mapa' => $this->createMapa($servicio),
            ));
        } else {  //el servicio no esta habilitado. Avisar y volver.
            $this->setFlash('error', 'El servicio no está habilitado. No se puede cortar el motor');
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * @Route("/app/prg/cortar", name="main_programacion_cortar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CORTEMOTOR")
     */
    public function cortarAction(Request $request)
    {
        $bita = array();
        $id = $request->get('id');    //toma el id del servicio
        $servicio = $this->servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $cortar = $request->get('cortar');   //si es corte o rehabilitacion.
        //inicio la programacion remota del equipo.
        if ($this->programacionManager->initConfig($servicio)) {   //inicializo las progremotas.
            $idCom = $this->programacionManager->setCorteMotor($servicio, $cortar);
            if ($idCom < 0) {
                $bita = $this->addBita($bita, 'Error interno del Manager. Avisa a su distribuidor.');
            } elseif ($idCom > 0) {
                $bita = $this->addBita($bita, 'Generación de comando correcta.');
                $idProg = $this->programacionManager->executeConfig();
                $resRun = $this->programacionManager->runProgramacion($idProg);
                if ($resRun < 0) {
                    $bita = $this->addBita($bita, sprintf('No se ha realizado la programación remota. Avise a su distribuidor, Error: %d', $idCom));
                } elseif ($resRun > 0) {
                    $bita = $this->addBita($bita, 'La programación ha sido procesada correctamente.');
                } else {
                    $bita = $this->addBita($bita, 'No se ha podido procesar la petición, intente nuevamente mas tarde.');
                }
            } else {
                $bita = $this->addBita($bita, 'error.');
            }
        } else {
            $bita = $this->addBita($bita, 'Este servicio no tiene equipo asociado.');
        }

        if (!isset($resRun))
            $resRun = -10;
        $status = ($idCom == 0 && $resRun > 0) ? 0 : $resRun;
        return new Response(json_encode(
            array(
                'id' => $idProg,
                'idCom' => $idCom,
                'status' => $status,
                'log' => implode('', $bita)
            )
        ), 200);
    }

    /**
     * @Route("/app/prg/{id}/cerrojo", name="main_programacion_cerrojo",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CERROJO")
     */
    public function cerrojoAction(Request $request, Servicio $servicio)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        return $this->render('app/Programacion/accionarcerrojo.html.twig', array(
            'servicio' => $servicio,
            'form' => $this->createCorteForm($servicio->getId())->createView(),
            'extras' => $this->createExtras($servicio),
            'mapa' => $this->createMapa($servicio),
        ));
    }

    /**
     * @Route("/app/prg/accionarcerrojo", name="main_programacion_accionarcerrojo")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CERROJO")
     */
    public function accionarcerrojoAction(Request $request)
    {
        $bita = array();
        $id = $request->get('id');    //toma el id del servicio
        $servicio = $this->servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $accion = $request->get('cortar');   //si es apertura o cierre del cerrojo.
        //inicio la programacion remota del equipo.
        if ($this->programacionManager->initConfig($servicio)) {   //inicializo las progremotas.
            $idCom = $this->programacionManager->setCerrojo($servicio, $accion);
            if ($idCom < 0) {
                $bita = $this->addBita($bita, 'Error interno del Manager. Avisa a su distribuidor.');
            } elseif ($idCom > 0) {
                $bita = $this->addBita($bita, 'Generación de comando correcta.');
                $idProg = $this->programacionManager->executeConfig();
                $resRun = $this->programacionManager->runProgramacion($idProg);
                if ($resRun < 0) {
                    $bita = $this->addBita($bita, sprintf('No se ha realizado la programación remota. Avise a su distribuidor, Error: %d', $idCom));
                } elseif ($resRun > 0) {
                    $bita = $this->addBita($bita, 'La programación ha sido procesada correctamente.');
                } else {
                    $bita = $this->addBita($bita, 'No se ha podido procesar la petición, intente nuevamente mas tarde.');
                }
            } else {
                $bita = $this->addBita($bita, 'error.');
            }
        } else {
            $bita = $this->addBita($bita, 'Este servicio no tiene equipo asociado.');
        }

        if (!isset($resRun))
            $resRun = -10;
        $status = ($idCom == 0 && $resRun > 0) ? 0 : $resRun;
        return new Response(json_encode(
            array(
                'id' => $idProg,
                'idCom' => $idCom,
                'status' => $status,
                'log' => implode('', $bita)
            )
        ), 200);
    }

    /**
     * @Route("/app/prg/cortar/wait", name="main_programacion_cortar_wait")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CORTEMOTOR")
     */
    public function cortarwaitAction(Request $request)
    {
        $id = $request->get('id');
        $idCom = $request->get('idCom');
        //die('////<pre>' . nl2br(var_export($idCom, true)) . '</pre>////');
        $st = $this->programacionManager->getStatusProg($id, $idCom);
        //$st = $this->get('calypso.programacion')->getStatusProgramacion($id);
        if ($st && $st >= \App\Entity\Programacion::TERMINADAOK) {
            $now = new \DateTime('', new \DateTimeZone('UTC'));
            $now->setTimezone(new \DateTimeZone($this->userloginManager->getTimezone()));
            $result = sprintf('Comando realizado con éxito a las %s', $now->format('H:i:s'));
        } elseif ($st && $st == \App\Entity\Programacion::TERMINADAPARCIAL) {
            $result = 'Comando realizado parcialmente';
        } elseif ($st && $st == \App\Entity\Programacion::RECHAZADA) {
            $result = 'Comando fallido. Reintente';
        } else {
            $result = '*';
        }
        return new Response($result, 200);
    }

    /**
     * @Route("app/programacion/refresh", name="main_programacion_refresh")
     * @Method({"GET", "POST"})     
     */
    public function refreshAction(Request $request)
    {
        $id = $request->get('id');
        $servicio = $this->servicioManager->find($id);
        $html = $response = $this->renderView(
            'app/Servicio/show_config_content.html.twig',
            array(
                'parametros' => $this->servicioManager->getParams($servicio),
            )
        );
        return new Response($html);
    }

    /**
     * @Route("/app/prg/corte/status", name="main_programacion_corte_status")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CORTEMOTOR")
     */
    public function cortestatusAction(Request $request)
    {
        $id = $request->get('id');
        $servicio = $this->servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $respuesta = array(
            'data' => array(
                'id_sc' => 'servicio_' . $servicio->getId(),
                'latitud' => $servicio->getUltLatitud(),
                'longitud' => $servicio->getUltLongitud(),
                'texto' => $servicio->getUltVelocidad() . ' km/h',
                'visible' => true,
                'new_icono' => $this->gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            ),
            'html' => $this->renderView('app/Programacion/corte_status.html.twig', array(
                'servicio' => $servicio,
                'extras' => $this->createExtras($servicio),
            )),
        );
        return new Response(json_encode($respuesta), 200);
    }

    /**
     * @Route("/app/prg/cerrojo/status", name="main_programacion_cerrojo_status")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_VAR_CERROJO")
     */
    public function cerrojostatusAction(Request $request)
    {
        $id = $request->get('id');
        $servicio = $this->servicioManager->find($id);
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $respuesta = array(
            'data' => array(
                'id_sc' => 'servicio_' . $servicio->getId(),
                'latitud' => $servicio->getUltLatitud(),
                'longitud' => $servicio->getUltLongitud(),
                'texto' => $servicio->getUltVelocidad() . ' km/h',
                'visible' => true,
                'new_icono' => $this->gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            ),
            'html' => $this->renderView('app/Programacion/cerrojo_status.html.twig', array(
                'servicio' => $servicio,
                'extras' => $this->createExtras($servicio),
            )),
        );
        return new Response(json_encode($respuesta), 200);
    }

    private function createMapa($servicio)
    {
        $mapa = $this->gmapsManager->createMap(true);
        $mapa->setSize('100%', '240px');
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($servicio->getUltLatitud());
        $mapa->setIniLongitud($servicio->getUltLongitud());
        $this->gmapsManager->addMarkerServicio($mapa, $servicio, true);
        return $mapa;
    }

    private function createExtras($servicio)
    {
        //hago la cercania.
        $extras = array();
        if (is_null($servicio->getUltLatitud()) || is_null($servicio->getUltLongitud())) {
            $extras['cercania'] = '---';
            $extras['domicilio'] = '---';
        } else {
            $referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
            if ($referencias) {
                $cercanas = $this->servicioManager->buscarCercaniaPosicion($servicio->getUltLatitud(), $servicio->getUltLongitud(), $referencias, 1);
                $extras['cercania'] = is_null($cercanas[0]) ? '---' : $cercanas[0]['leyenda'];
            }

            $direc = $this->geocoderManager->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());
            $extras['domicilio'] = $direc;
        }
        if (is_null($servicio->getUltFechahora())) {  //nunca transmitio.
            $extras['ultFechahora'] = '---';
        } else {
            $ultFecha = new \DateTime($servicio->getUltFechahora()->format('d-m-Y H:i:s'), new \DateTimeZone('UTC'));
            $ultFecha->setTimezone(new \DateTimeZone($this->userloginManager->getUser()->getTimezone()));
            $extras['ultFechahora'] = $ultFecha->format('d-m-Y H:i:s');
        }
        $extras['st_ultreporte'] = $this->servicioManager->getStatusUltReporte($servicio, true);

        $now = new \DateTime(date('d-m-Y H:i:s', time()), new \DateTimeZone('UTC'));
        $now->setTimezone(new \DateTimeZone($this->userloginManager->getUser()->getTimezone()));
        $extras['now'] = $now->format('d-m-Y H:i:s');

        $ult_trama = json_decode($servicio->getUltTrama(), true);
        //$extras['estado'] = $ult_trama['salida_1'] ? 'Cerrado' : 'Abierto';
        if ($servicio->getCorteCerrojo() === true) {
            $extras['estado_cerrojo'] = $this->servicioManager->getStatusCerrojo($servicio, $ult_trama);
        }
        return $extras;
    }

    private function createCorteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    public function addBita($bitacora, $msj)
    {
        $bita[] = $msj;
        return $bita;
    }
}
