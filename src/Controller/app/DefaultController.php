<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\UsuarioManager;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;

class DefaultController extends AbstractController
{

    private $usuarioManager;
    private $userlogin;
    private $breadcrumbManager;

    function __construct(UsuarioManager $usuarioManager, BreadcrumbManager $breadcrumbManager, UserLoginManager $userlogin)
    {
        $this->usuarioManager = $usuarioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userlogin = $userlogin;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getSecurityContext()
    {
        //return $this->container->get('security.token_storage');
        return $this->container->get('security.authorization_checker');
    }

    /**
     * @Route("/", name="main_default")
     * @Method({"GET"})
     */
    public function preindexAction(Request $request)
    {
        //* @IsGranted("ROLE_USER")
        //$user = $this->container->get('security.token_storage')->getToken()->getUser();
        $user = $this->getUser();
        if ($user) {
            $this->usuarioManager->setLogin($request, $user);
            return $this->indexAction($request, $user);
        }
        //no tengo usuario me voy al login
        return $this->redirectToRoute('security_login');
    }

    public function indexAction(Request $request, $user)
    {
        // die('////<pre>' . nl2br(var_export($this->userlogin->isModuloEnabled('CLIENTE_MONITOREO'), true)) . '</pre>////');
        $pilaRetorno = null;
        $volver = null;
        $id = null;

        $session = $request->getSession();
        $session->set('pilaRetorno', $pilaRetorno);
        $session->set('volver', $volver);
        $session->set('idorg', $id);
        $session->set('moduloMonitor', $this->userlogin->isModuloEnabled('CLIENTE_MONITOREO'));

        $this->breadcrumbManager->clear($request->getRequestUri(), "Main");

        if ($user->getOrganizacion()->getTipoOrganizacion() === 1) {
            return $this->redirect($this->generateUrl('dist_homepage'));
        } else {
            // si es un cliente ==> determinar cant de aplicaciones.
            $aplic = $this->getEntityManager()->getRepository('App:Organizacion')
                ->getAplicaciones($user->getOrganizacion());

            if (count($aplic) === 1) {
                if (!is_null($user->getRedirigirLogin()) && $user->getRedirigirLogin() != 0) {  //regirigir a ver mapa
                    return $this->redirect($this->generateUrl('apmon_homepage', array(
                        'redirect' => $user->getRedirigirLogin()
                    )));
                } else {
                    if ($aplic[0]->getRoute() != null) {
                        /* hay una sola aplicacion, ir a esa por default */
                        return $this->redirect($this->generateUrl($aplic[0]->getRoute()));
                    } else {
                        //la aplicacion no tiene route por lo que hay ir al homepage
                        return $this->redirect($this->generateUrl('apmon_homepage'));
                    }
                }
            } else {
                /* como hay muchas aplicaciones entonces mostrar a cual se quiere ir  */
                /* TODO: por ahora se manda al apmon, pero debe mostrarse todas a las que puede ir */
                return $this->redirect($this->generateUrl('apmon_homepage'));
            }
        }
        //}
    }

    protected function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!\is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }
        //die('////<pre>' . nl2br(var_export($user->getUsername(), true)) . '</pre>////');

        return $user;
    }
}
