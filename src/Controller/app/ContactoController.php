<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\app\ContactoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ContactoManager;
use App\Entity\Organizacion;
use App\Entity\Contacto;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\Router\ContactoRouter;
use App\Model\monitor\EmpresaManager;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\TransporteManager;
use Symfony\Component\HttpFoundation\Response;

class ContactoController extends AbstractController
{

    private $breadcrumbManager;
    private $contactoManager;
    private $userloginManager;
    private $organizacionManager;
    private $notificadorAgenteManager;
    private $contactoRouter;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        ContactoManager $contactoManager,
        UserLoginManager $userloginManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        ContactoRouter $contactoRouter
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->contactoManager = $contactoManager;
        $this->userloginManager = $userloginManager;
        $this->organizacionManager = $organizacionManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->contactoRouter = $contactoRouter;
    }

    /**
     *
     * @Route("/app/contacto/{id}/list", name="main_contacto_list",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Contactos");
        $contactos = $this->contactoManager->findAll($organizacion);

        return $this->render('app/Contacto/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'contactos' => $contactos,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        
        $menu[1] = array($this->contactoRouter->btnNew($organizacion));
        return $menu;
    }

    /**
     *
     * @Route("/app/contacto/{id}/show", name="main_contacto_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_VER")
     */
    public function showAction(Request $request, Contacto $contacto)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $contacto->getNombre());
        $deleteForm = $this->createDeleteForm($contacto->getId());

        return $this->render('app/Contacto/show.html.twig', array(
            'menu' => $this->getShowMenu($contacto),
            'contacto' => $contacto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $contacto 
     * @return array $menu
     */
    private function getShowMenu($contacto){
        $menu[1] = array($this->contactoRouter->btnEdit($contacto));
        $menu[2] = array($this->contactoRouter->btnDelete());
        return $menu;
    }

    /**
     *
     * @Route("/app/contacto/{id}/new", name="main_contacto_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {

        $contacto = $this->contactoManager->create($organizacion);
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Contacto");

        $options['config_sms'] = $this->userloginManager->isModuloEnabled('CLIENTE_SMS') && $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_SMS');
        $form = $this->createForm(ContactoType::class, $contacto, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($contacto->getEmail() == '' && $contacto->getCelular() == '') {
                $this->get('session')->getFlashBag()->add('error', 'El telefono y email no pueden ser vacios simultaneamente');
            } else {
                //die('////<pre>'.nl2br(var_export('$data', true)).'</pre>////');
                $contacto = $this->contactoManager->save($contacto);
                $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado el contacto <b>%s</b> en el sistema.', strtoupper($contacto))
                );
            }

            return $this->redirect($this->breadcrumbManager->getVolver());
        }


        return $this->render('app/Contacto/new.html.twig', array(
            'contacto' => $contacto,
            'form' => $form->createView()
        ));
    }

    /**
     *
     * @Route("/app/contacto/{id}/edit", name="main_contacto_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_EDITAR")
     */
    public function editAction(Request $request, Contacto $contacto)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Contacto");
        if (!$contacto) {
            throw $this->createNotFoundException('Código de Contacto no encontrado.');
        }
        $options['config_sms'] = $this->userloginManager->isModuloEnabled('CLIENTE_SMS') && $this->organizacionManager->isModuloEnabled($contacto->getOrganizacion(), 'CLIENTE_SMS');
        $form = $this->createForm(ContactoType::class, $contacto, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($contacto->getEmail() == '' && $contacto->getCelular() == '') {
                $this->get('session')->getFlashBag()->add('error', 'El telefono y email no pueden ser vacios simultaneamente');
            } else {
                $this->breadcrumbManager->pop();
                $contacto = $this->contactoManager->save($contacto);
                $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($contacto)));
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }



        return $this->render('app/Contacto/edit.html.twig', array(
            'contacto' => $contacto,
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Route("/app/contacto/{id}/delete", name="main_contacto_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_ELIMINAR")
     */
    public function deleteAction(Request $request, Contacto $contacto)
    {

        $form = $this->createDeleteForm($contacto->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $cont = new Contacto();
            $cont->setId($contacto->getId());
            $cont->setUpdatedAt($contacto->getUpdatedAt());
            $cont->setCreatedAt($contacto->getCreatedAt());
            $this->breadcrumbManager->pop();
            $this->contactoManager->delete($contacto->getId());
            $notificar = $this->notificadorAgenteManager->notificar($cont, 2);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/monitor/contacto/edit", name="monitor_contacto_edit")
     * @Method({"GET", "POST"})
     */
    public function editcontactoAction(
        Request $request,
        LogisticaManager $logisticaManager,
        TransporteManager $transporteManager,
        EmpresaManager $empresaManager
    ) {
        $id = intval($request->get('id'));
        $contacto = $this->contactoManager->find($id);
        $idEntity = intval($request->get('idEntity'));
        if ($request->get('entity') == 'logistica') {
            $entity = $logisticaManager->find($idEntity);
            $contacto->setLogistica($entity);
        } elseif ($request->get('entity') == 'transporte') {
            $entity = $transporteManager->find($idEntity);
            $contacto->setTransporte($entity);
        } elseif ($request->get('entity') == 'empresa') { //empresa
            $entity = $empresaManager->find($idEntity);
            $contacto->setEmpresa($entity);
        } else {
        }

        $tmp = array();
        parse_str($request->get('form'), $tmp);
      //  die('////<pre>'.nl2br(var_export($tmp, true)).'</pre>////');
        if ($request->getMethod() == 'POST' && isset($tmp['contacto_edit'])) {
            $data = $tmp['contacto_edit'];
            $contacto = $this->contactoManager->setData($contacto, $data);

            if ($contacto = $this->contactoManager->save($contacto)) {
                $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                    'contactos' => $entity->getContactos(),
                    'showItinerario' => false
                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

        /**
     * @Route("/monitor/contacto/delete", name="monitor_contacto_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_AGREGAR")
     */
    public function deletecontactoAction(Request $request)
    {
        $id = intval($request->get('id'));
        $contacto = $this->contactoManager->find($id);

        $cont = new Contacto(); //creo un contacto pasajero para que pueda notificar el que se borró
        $cont->setId($contacto->getId());
        $cont->setUpdatedAt($contacto->getUpdatedAt());
        $cont->setCreatedAt($contacto->getCreatedAt());

        if (!$contacto) {
            throw $this->createNotFoundException('Contacto no encontrado.');
        }
        if ($request->get('entity') == 'logistica') {
            $entity = $contacto->getLogistica();
        } elseif ($request->get('entity') == 'transporte') {
            $entity = $contacto->getTransporte();
        } else { //empresa
            $entity = $contacto->getEmpresa();
        }
        
       if ($this->contactoManager->delete($contacto)) {
            $notificar = $this->notificadorAgenteManager->notificar($cont, 2);
            $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                'contactos' => $entity->getContactos(),

            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
