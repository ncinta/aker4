<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\app\RubroType;
use App\Form\app\InformeRubroType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ExcelManager;
use App\Model\app\LoggerManager;
use App\Model\app\RubroManager;
use App\Model\app\UtilsManager;
use App\Entity\Organizacion;
use App\Entity\Rubro;

/**
 * Description of TallerController
 *
 * @author nicolas
 */
class RubroController extends AbstractController
{

    private $breadcrumbManager;
    private $rubroManager;
    private $userloginManager;
    private $loggerManager;
    private $utilsManager;
    private $excelManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        RubroManager $rubroManager,
        UserLoginManager $userloginManager,
        LoggerManager $loggerManager,
        UtilsManager $utilsManager,
        ExcelManager $excelManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->rubroManager = $rubroManager;
        $this->userloginManager = $userloginManager;
        $this->loggerManager = $loggerManager;
        $this->utilsManager = $utilsManager;
        $this->excelManager = $excelManager;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($id)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_RUBRO_AGREGAR')) {
            $menu['Agregar Rubro'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('app_rubro_new', array('id' => $id))
            );
        }
        $menu['Informe'] = array(
            'imagen' => 'icon-print icon-blue',
            'url' => $this->generateUrl('app_rubro_informe', array('id' => $id))
        );
        return $menu;
    }

    private function getShowMenu($rubro)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_RUBRO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('app_rubro_edit', array('id' => $rubro->getId()))
            );
            if ($this->userloginManager->isGranted('ROLE_RUBRO_ELIMINAR')) {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
        return $menu;
    }

    /**
     *
     * @Route("/app/rubro/{id}/list", name="app_rubro_list",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Rubros");

        return $this->render('app/Rubro/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion->getId()),
            'rubros' => $this->rubroManager->find($organizacion),
        ));
    }

    /**
     *
     * @Route("/app/rubro/{id}/new", name="app_rubro_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Rubro");

        $rubro = $this->rubroManager->create($organizacion);
        $form = $this->createForm(RubroType::class, $rubro);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $rubro = $this->rubroManager->save($rubro);
            $this->breadcrumbManager->pop();
            $this->setFlash('success', sprintf('El taller externo se ha creado con éxito'));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        return $this->render('app/Rubro/new.html.twig', array(
            'organizacion' => $organizacion,
            'rubro' => $rubro,
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Route("/app/rubro/{id}/edit", name="app_rubro_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_EDITAR")
     */
    public function editAction(Request $request, Rubro $rubro)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Taller");
        $form = $this->createForm(RubroType::class, $rubro);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $rubro = $this->rubroManager->save($rubro);
            $this->setFlash('success', sprintf('El rubro se ha modificado con éxito'));
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        return $this->render('app/Rubro/edit.html.twig', array(
            'rubro' => $rubro,
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Route("/app/rubro/{id}/show", name="app_rubro_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_VER")
     */
    public function showAction(Request $request, Rubro $rubro)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Ver Taller");

        return $this->render('app/Rubro/show.html.twig', array(
            'rubro' => $rubro,
            'menu' => $this->getShowMenu($rubro),
            'delete_form' => $this->createDeleteForm($rubro->getId())->createView(),
        ));
    }

    /**
     *
     * @Route("/app/rubro/{id}/delete", name="app_rubro_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_ELIMINAR")
     */
    public function deleteAction(Request $request, Rubro $rubro)
    {

        $form = $this->createDeleteForm($rubro->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->rubroManager->deleteById($rubro->getId())) {
                $this->setFlash('success', 'Rubro eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el rubro del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     *
     * @Route("/app/rubro/{id}/informe", name="app_rubro_informe",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_VER")
     */
    public function informeAction(Request $request, Organizacion $organizacion)
    {

        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getDoctrine()->getManager();
        $form = $this->createForm(InformeRubroType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->loggerManager->setTimeInicial();
            $consulta = $request->get('informe');

            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);

            if ($hasta <= $desde) {
                $this->get('session')->getFlashBag()->add('error', 'Se han ingresado mal las fechas. Verifique por favor.');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $rubro = null;
                if (strlen($consulta['rubro']) > 0) {
                    $rubro = intval($consulta['rubro']);
                }
                $cCosto = null;
                if (strlen($consulta['centroCosto']) > 0) {
                    $cCosto = intval($consulta['centroCosto']);
                }
                $informe = $this->rubroManager->informe($organizacion, $desde, $hasta, $rubro, $cCosto);

                $this->breadcrumbManager->push($request->getRequestUri(), "Resultado");
                //aca se redirige al destino correspondiente.
                switch ($consulta['destino']) {
                    case '1': //sale a pantalla.
                        return $this->render('app/Rubro/informe_result.html.twig', array(
                            'total' => $informe['total'],
                            'informe' => $informe['informe'],
                            'consulta' => $consulta,
                            'organizacion' => $organizacion,
                        ));
                        break;
                    case '2':
                        return $this->render('app/Rubro/grafico.html.twig', array(
                            'consulta' => $consulta,
                            'organizacion' => $organizacion->getId(),
                            'dt' => $this->getGraficoInforme($consulta, $informe['total'])
                        ));
                        break;
                    case '5': //sale a excel
                        return $this->exportarAction($request, $organizacion->getId(), $informe['informe'], $informe['total'], $consulta);
                        break;
                }
            }
        }

        return $this->render('app/Rubro/informe_form.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
        ));
    }

    /**
     * @Route("/app/rubro/{id}/exportar", name="app_rubro_informe_exportar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_RUBRO_VER")
     */
    public function exportarAction(Request $request, $id, $informe = null, $total = null, $consulta = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
            $total = json_decode($request->get('total'), true);
        }
        if (isset($id) && isset($consulta) && isset($informe)) {

            //creo el xls
            $xls = $this->excelManager->create("Informe Rubros");
            $xls->setHeaderInfo(array(
                //                'C2' => 'Rubro',
                //                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));


            $xls->setBar(6, array(
                'A' => array('title' => 'Rubro', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 10),
                'C' => array('title' => 'Servicio', 'width' => 20),
                'D' => array('title' => 'Orden', 'width' => 10),
                'E' => array('title' => 'Interno', 'width' => 10),
                'F' => array('title' => 'Externo', 'width' => 10),
                'G' => array('title' => 'Centro Costo', 'width' => 20),
                'H' => array('title' => 'Cantidad', 'width' => 10),
                'I' => array('title' => 'Producto', 'width' => 40),
                'J' => array('title' => 'Neto', 'width' => 10),
                'K' => array('title' => 'Total', 'width' => 10),
            ));
            $i = 7;

            //  die('////<pre>' . nl2br(var_export($total, true)) . '</pre>////');
            foreach ($informe as $key => $datos) {   //esto es para cada rubro
                foreach ($datos as $value) {
                    if (is_array($value['fecha'])) {
                        $f = date_create($value['fecha']['date']);
                    } else {
                        $f = $value['fecha'];
                    }

                    $xls->setRowValues($i, array(
                        'A' => array('value' => $key),
                        'B' => array('value' => $f->format('d-m-Y')),
                        'C' => array('value' => $value['servicio']),
                        'D' => array('value' => $value['id']),
                        'E' => array('value' => $value['nro_interno']),
                        'F' => array('value' => $value['nro_externo']),
                        'G' => array('value' => $value['centroCosto']),
                        'H' => array('value' => intval($value['cantidad']), 'format' => '0.00'),
                        'I' => array('value' => $value['producto']),
                        'J' => array('value' => intval($value['neto']), 'format' => '0.00'),
                        'K' => array('value' => intval($value['total']), 'format' => '0.00'),
                    ));
                    $i++;
                }
                //se agregan los totales
                $xls->setRowTotales($i, array(
                    'A' => array('value' => $key),
                    'B' => array('value' => 'TOTAL'),
                    'H' => array('value' => intval($total[$key]['cantidad']), 'format' => '0.00'),
                    'J' => array('value' => intval($total[$key]['costoNeto']), 'format' => '0.00'),
                    'K' => array('value' => intval($total[$key]['costoTotal']), 'format' => '0.00'),
                ));
                $i++;
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rubros.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('app_rubro_list'));
        }
    }

    public function getGraficoInforme($consulta, $total)
    {
        $myArray = array();
        $gtotal = 0;
        //die('////<pre>' . nl2br(var_export($total, true)) . '</pre>////');
        foreach ($total as $key => $value) {
            $gtotal += $value['costoTotal'];
        }
        foreach ($total as $key => $value) {
            //die('////<pre>' . nl2br(var_export($key, true)) . '</pre>////');            
            $myArray[] = array("name" => $key, "y" => round((intval($value['costoTotal']) * 100) / $gtotal));
        }
        //------------
        //die('////<pre>' . nl2br(var_export(json_encode($myArray), true)) . '</pre>////');
        return json_encode($myArray, true);
    }
}
