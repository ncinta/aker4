<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\dist\EventoHistorialType;
use App\Form\app\PanicoRegistroType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Organizacion as Organizacion;
use App\Entity\EventoHistorial;
use App\Model\app\BreadcrumbManager;
use App\Model\app\EventoManager;
use App\Model\app\PanicoManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\GmapsManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ExcelManager;
use App\Model\app\UserLoginManager;

class PanicoController extends AbstractController
{

    private $eventoManager;
    private $breadcrumbManager;
    private $servicioManager;
    private $utilsManager;
    private $panicoManager;
    private $organizacionManager;
    private $excelManager;
    private $userloginManager;
    private $gmapsManager;
    private $referenciaManager;
    private $eventoHistorialManager;

    function __construct(
        EventoManager $eventoManager,
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        PanicoManager $panicoManager,
        OrganizacionManager $organizacionManager,
        ExcelManager $excelManager,
        UserLoginManager $userloginManager,
        GmapsManager $gmapsManager,
        ReferenciaManager $referenciaManager,
        EventoHistorialManager $eventoHistorialManager
    ) {

        $this->eventoManager = $eventoManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->utilsManager = $utilsManager;
        $this->panicoManager = $panicoManager;
        $this->organizacionManager = $organizacionManager;
        $this->excelManager = $excelManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
        $this->referenciaManager = $referenciaManager;
        $this->eventoHistorialManager = $eventoHistorialManager;
    }

    /**
     * @Route("/app/panico/{id}/historico", name="main_panico_historico",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function historicoAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Historico de Panico");
        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());
        $servicio = 0;
        $ok = true;
        $panicos = null;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('historial');
            $ok = $this->utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
            }
            $servicio = $form['servicio'];
        }
        if ($ok) {
            $fdesde = $this->utilsManager->datetime2sqltimestamp($fecha_desde . ' 00:00:00');
            $fhasta = $this->utilsManager->datetime2sqltimestamp($fecha_hasta . ' 23:59:59');
            $panicos = $this->panicoManager->listHistorial($organizacion, $fdesde, $fhasta, $servicio);
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
        }

        $options['eventos'] = $this->eventoManager->findAll($organizacion, array('nombre' => 'ASC'));
        $options['servicios'] = $this->servicioManager->findAllByOrganizacion($organizacion);
        $options['servicio_select'] = $servicio;
        $options['evento_select'] = 0;
        $form = $this->createForm(EventoHistorialType::class, null, $options);

        $tmp = null;
        foreach ($panicos as $p) {
            $tmp[] = array(
                'evento' => $p->getEvento()->getNombre(),
                'id' => $p->getId(),
                'servicio' => $p->getServicio()->getNombre(),
                'fecha' => $p->getFecha(),
                'fechaVista' => $p->getFechaVista(),
                'respuesta' => $p->getRespuesta(),
                'titulo' => $p->getTitulo(),
            );
        }
        //        die('////<pre>'.nl2br(var_export($tmp, true)).'</pre>////');

        return $this->render('app/PanicoHistorico/list.html.twig', array(
            'organizacion' => $organizacion,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'menu' => $this->getListMenu($organizacion, $fecha_desde, $fecha_hasta, $servicio, count($panicos) > 0),
            'panicos' => $tmp,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/app/panico/{servicio_id}/{desde}/{hasta}/{organizacion_id}/export", name="main_panico_export")
     * @Method({"GET", "POST"})
     */
    public function exportAction(Request $request, $servicio_id, $desde, $hasta, $organizacion_id)
    {

        $fecha_desde = \DateTime::createFromFormat('dmY', $desde);  //reconstruyo y obtengo la fecha desde
        $fecha_hasta = \DateTime::createFromFormat('dmY', $hasta);  //reconstruyo y obtengo la fecha hasta
        //obtengo el servico para sacar la oganizacion
        $organizacion = $this->organizacionManager->find($organizacion_id);

        if ($servicio_id == 0) {
            $nombre = 'Todos los servicios';
        } else {
            $nombre = $this->servicioManager->find($servicio_id)->getNombre();
        }

        //obtengo los panicos para ser exportados.
        $panicos = $this->panicoManager->listHistorial(
            $organizacion,
            $fecha_desde->format('Y-m-d') . ' 00:00:00',
            $fecha_hasta->format('Y-m-d') . ' 23:59:59',
            $servicio_id
        );

        if (!is_null($panicos)) {
            $xls = $this->excelManager->create("Listado de Pánicos");
            //encabezado...
            $xls->setHeaderInfo(array(
                'C3' => 'Fecha desde',
                'D3' => $fecha_desde->format('d/m/Y'),
                'C4' => 'Fecha hasta',
                'D4' => $fecha_hasta->format('d/m/Y'),
                'C5' => 'Servicio',
                'D5' => $nombre,
            ));

            //title 
            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 40),
                'B' => array('title' => 'Fecha', 'width' => 20),
                'C' => array('title' => 'Mensaje', 'width' => 40),
                'D' => array('title' => 'Dirección', 'width' => 40),
                'E' => array('title' => 'Respuesta', 'width' => 40),
                'F' => array('title' => 'Visto', 'width' => 20),
                'G' => array('title' => 'Operador', 'width' => 20),
            ));

            //cuerpo
            $i = 7;
            foreach ($panicos as $panico) {
                $fpanico = $panico->getFecha();
                $fpanico->setTimezone(new \DateTimeZone($this->userloginManager->getTimezone()));
                $fvista = $panico->getFecha();
                $fvista->setTimezone(new \DateTimeZone($this->userloginManager->getTimezone()));

                $data = $panico->getData();

                $xls->setRowValues($i, array(
                    'A' => array('value' => $panico->getServicio()->getNombre()),
                    'B' => array('value' => $fpanico->format('d-m-Y H:i:s')),
                    'C' => array('value' => $panico->getTitulo()),
                    'D' => array('value' => isset($data['domicilio']) ? $data['domicilio'] : '---'),
                    'E' => array('value' => $panico->getRespuesta()),
                    'F' => array('value' => $fvista->format('d-m-Y H:i:s')),
                    'G' => array('value' => !is_null($panico->getEjecutor()) ? $panico->getEjecutor()->getNombre() : '---'),
                ));
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=panicos.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->breadcrumbManager->getVolver());
            //return $this->redirect($this->generateUrl('apmon_informe_paso'));
        }
    }

    /**
     * @Route("/app/panico/{id}/pendientes", name="main_panico_pendientes",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("{ROLE_EVENTO_HISTORICO_VER, ROLE_APMON_PANICO_HISTORICO, ROLE_SAURON_PANICO_HISTORICO}")
     */
    public function pendientesAction(Request $request, Organizacion $organizacion)
    {

        $servicios = $this->servicioManager->findAllByOrganizacion($organizacion);

        $panicos = $this->panicoManager->listPendientes($servicios);
        $tmp = null;
        foreach ($panicos as $p) {
            $tmp[] = array(
                'evento' => $p->getEvento()->getNombre(),
                'id' => $p->getId(),
                'servicio' => $p->getServicio()->getNombre(),
                'fecha' => $p->getFecha(),
                'fechaVista' => $p->getFechaVista(),
                'respuesta' => $p->getRespuesta(),
                'titulo' => $p->getTitulo(),
            );
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Pánicos sin atender");

        return $this->render('app/PanicoHistorico/pendientes.html.twig', array(
            //'menu' => $this->getListMenu(),
            'panicos' => $tmp,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu($organizacion, $fecha_desde, $fecha_hasta, $servicio_id, $showExportBtn)
    {
        $menu = array();
        if ($showExportBtn) {
            $fdesde = \DateTime::createFromFormat('d/m/Y', $fecha_desde);
            $fhasta = \DateTime::createFromFormat('d/m/Y', $fecha_hasta);
            $menu['Exportar'] = array(
                'imagen' => 'icon-download-alt icon-blue',
                'url' => $this->generateUrl('main_panico_export', array(
                    'organizacion_id' => $organizacion->getId(),
                    'desde' => $fdesde->format('dmY'),
                    'hasta' => $fhasta->format('dmY'),
                    'servicio_id' => $servicio_id
                ))
            );
        }
        $menu['Marcar como Vistos'] = array(
            'imagen' => '',
            'extra' => 'onclick=batch("visto") id=btnVisto style=display:none',
            'url' => '#visto',
        );
        return $menu;
    }

    /**
     * @Route("/app/panico/{id}/show", name="main_panico_historico_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET"},
     * )     
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER", statusCode=404, message="No tiene permismos para esta página")
     */
    public function showAction(Request $request, EventoHistorial $panico)
    {

        if (!$panico) {
            throw $this->createNotFoundException('Código de Pánico no encontrado.');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), 'Mostrar Panico');
        $deleteForm = $this->createDeleteForm($panico->getId());

        $data = $panico->getData();
        if (isset($data['latitud']) && isset($data['longitud'])) {
            //aca creo el mapa para mostrar el panicso
            $mapa = $this->gmapsManager->createMap(true);
            $mapa->setSize('100%', '240px');
            $mapa->setIniZoom(16);
            $mapa->setIniLatitud($data['latitud']);
            $mapa->setIniLongitud($data['longitud']);

            //agrego el punto del panico
            $ptoPanico = $this->gmapsManager->createPosicionIcon($mapa, 'panico', 'ballred.png');
            $this->gmapsManager->addMarkerPosicion($mapa, 'Panico', $data, $ptoPanico);

            //agrego las referencias al mapa
            $referencias = $this->referenciaManager->findAsociadas();
            foreach ($referencias as $referencia) {
                $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
            }
            $cercanas = $this->servicioManager->buscarCercaniaPosicion($data['latitud'], $data['longitud'], $referencias, 5);
        }
        //Este es el menu que se muestra en el show del evento.
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_SAURON_PANICO_REGISTRAR') || $this->userloginManager->isGranted('ROLE_APMON_PANICO_REGISTRAR')) {
            if (is_null($panico->getFechaVista())) {
                $menu['Registrar'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl('main_panico_registrar', array('id' => $panico->getId())),
                );
            }
        }
        if ($this->userloginManager->isGranted('ROLE_SAURON_PANICO_ELIMINAR') || $this->userloginManager->isGranted('ROLE_APMON_PANICO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $this->render('app/PanicoHistorico/show.html.twig', array(
            'menu' => $menu,
            'panico' => array(
                'id' => $panico->getId(),
                'servicio' => $panico->getServicio(),
                'fechaPanico' => $panico->getFecha(),
                'fechaVista' => $panico->getFechaVista(),
                'respuesta' => $panico->getRespuesta(),
                'titulo' => $panico->getTitulo(),
                'mensaje' => isset($data['mensaje']) ? $data['mensaje'] : null,
                'domicilio' => isset($data['domicilio']) ? $data['domicilio'] : null,
                'ultLatitud' => isset($data['latitud']) ? $data['latitud'] : null,
                'ultLongitud' => isset($data['longitud']) ? $data['longitud'] : null,
                'ejecutor' => $panico->getEjecutor(),
            ),
            'delete_form' => $deleteForm->createView(),
            'mapa' => isset($mapa) ? $mapa : null,
            'cercana' => isset($cercanas) ? $cercanas : null,
        ));
    }

    /**
     * @Route("/app/panico/{id}/registrar", name="main_panico_registrar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function registrarAction(Request $request, EventoHistorial $panico)
    {

        if (!$panico) {
            throw $this->createNotFoundException('Código de Panico no encontrado.');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), 'Registrar Panico');

        $form = $this->createForm(PanicoRegistroType::class, $panico);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->eventoHistorialManager->registrar($panico);
                $this->setFlash('success', 'Se ha registrado la respuesta del pánico. Gracias.');

                $volver = $this->breadcrumbManager->getVolver();
                if (is_null($volver)) {
                    $volver = $this->generateUrl('main_panico_historico_show', array('id' => $panico->getId()));
                }
                return $this->redirect($volver);
            }
        }

        $data = $panico->getData();
        if (isset($data['latitud']) && isset($data['longitud'])) {
            //aca creo el mapa para mostrar el panicso
            $mapa = $this->gmapsManager->createMap(true);
            $mapa->setSize('100%', '240px');
            $mapa->setIniZoom(16);
            $mapa->setIniLatitud($data['latitud']);
            $mapa->setIniLongitud($data['longitud']);

            //agrego el punto del panico
            $ptoPanico = $this->gmapsManager->createPosicionIcon($mapa, 'panico', 'ballred.png');
            $this->gmapsManager->addMarkerPosicion($mapa, 'Panico', $data, $ptoPanico);

            //agrego las referencias al mapa
            $referencias = $this->referenciaManager->findAsociadas();
            foreach ($referencias as $referencia) {
                $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
            }
            $cercanas = $this->servicioManager->buscarCercaniaPosicion($data['latitud'], $data['longitud'], $referencias, 5);
        }

        return $this->render('app/PanicoHistorico/registrar.html.twig', array(
            'panico' => array(
                'id' => $panico->getId(),
                'servicio' => $panico->getServicio(),
                'fechaPanico' => $panico->getFecha(),
                'fechaVista' => $panico->getFechaVista(),
                'respuesta' => $panico->getRespuesta(),
                'titulo' => $panico->getTitulo(),
                'mensaje' => isset($data['mensaje']) ? $data['mensaje'] : null,
                'domicilio' => isset($data['domicilio']) ? $data['domicilio'] : null,
                'ultLatitud' => isset($data['latitud']) ? $data['latitud'] : null,
                'ultLongitud' => isset($data['longitud']) ? $data['longitud'] : null,
                'ejecutor' => $panico->getEjecutor(),
            ),
            'mapa' => isset($mapa) ? $mapa : null,
            'cercana' => isset($cercanas) ? $cercanas : null,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/app/panico/query", name="main_panico_queryAJAX")
     * @Method({"GET", "POST"})
     */
    public function queryAjaxAction(Request $request)
    {
        $nuevos = 0;
        $servicios = $this->servicioManager->findAllByUsuario();
        $countPanicos = $this->panicoManager->countNews($servicios);
        if ($countPanicos != 0) {
            $nuevos = $countPanicos - $request->getSession()->get('panicos');
        }
        if ($nuevos != 0 || $countPanicos != 0) {     //solo entra cuando tiene panicos por atender o nuevos.
            $html = $this->renderView(
                'app/Template/alerta_panico.html.twig',
                array(
                    'cantidad' => $countPanicos,
                    'panicos' => $this->panicoManager->findNew($servicios, 5),
                )
            );
        } else {
            $html = null;
        }

        $this->getRequest()->getSession()->set('panicos', $countPanicos);

        return new Response(json_encode(array(
            'cantidad' => $countPanicos,
            'nuevas' => $nuevos,
            'html' => $html,
        )));
    }

    /**
     * @Route("/app/panico/{id}/delete", name="main_panico_historico_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("{ROLE_APMON_PANICO_ELIMINAR}")
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->panicoManager->delete($id);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    protected function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
