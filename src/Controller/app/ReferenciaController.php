<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Form\app\ReferenciaType;
use App\Entity\Referencia;
use App\Entity\Organizacion;
use GMaps\Geocoder;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\GmapsManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\NotificadorAgenteManager;

/**
 * Description of ReferenciaController
 *
 */
class ReferenciaController extends AbstractController
{

    private $utils;
    private $bread;
    private $organizacionManager;
    private $userloginManager;
    private $gmapsManager;
    private $referenciaManager;
    private $notificadorAgenteManager;

    public function __construct(
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        OrganizacionManager $organizacionManager,
        UserLoginManager $userloginManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager
    ) {
        $this->utils = $utilsManager;
        $this->bread = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->userloginManager = $userloginManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/referencia/{id}/list", name="referencia_list",
     * requirements={
     *         "id": "\d+"
     *     }),
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->bread->push($request->getRequestUri(), "Listado de Referencias");

        $referencias = $this->referenciaManager->findAsociadas($organizacion);

        return $this->render('app/Referencia/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'referencias' => $referencias,
            'userorg' => $organizacion,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_AGREGAR')) {
            $menu['Agregar Referencia'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('referencia_new', array(
                    'id' => $organizacion->getId()
                ))
            );

            $menu['Importar Kml'] = array(
                'imagen' => 'fa fa-file-picture-o',
                'url' => $this->generateUrl('referencia_kml_importacion', array(
                    'id' => $organizacion->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_CATEGORIA_ADMIN')) {
            $menu['Categorias'] = array(
                'imagen' => 'icon-tasks icon-blue',
                'url' => $this->generateUrl('apmon_categoria_list')
            );
        }
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_ADMIN')) {
            $menu['Grupos'] = array(
                'imagen' => 'icon-th-large icon-blue',
                'url' => $this->generateUrl('apmon_grupo_referencia_list')
            );
        }

        if ($organizacion->getTipoOrganizacion() == 1) {
            if ($this->userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_ADMIN')) {
                $menu['Grupos'] = array(
                    'imagen' => 'icon-th-large icon-blue',
                    'url' => $this->generateUrl('sauron_grupo_referencia_list', array(
                        'idorg' => $organizacion->getId(),
                    ))
                );
            }
            if ($this->userloginManager->isGranted('ROLE_REFERENCIA_COMPARTIR')) {
                $menu['Compartir'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("compartir") id=btnCompartir style=display:none',
                    'url' => '#compartir',
                );
            }
            if ($this->userloginManager->isGranted('ROLE_REFERENCIA_COPIAR')) {
                $menu['Copiar'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("copiar") id=btnCopiar style=display:none',
                    'url' => '#copiar',
                );
            }
            if ($this->userloginManager->isGranted('ROLE_REFERENCIA_MOVER')) {
                $menu['Mover'] = array(
                    'imagen' => '',
                    'extra' => 'onclick=batch("mover") id=btnMover style=display:none',
                    'url' => '#mover',
                );
            }
        }
        return $menu;
    }

    /**
     * @Route("/referencia/{id}/show", name="referencia_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_VER") 
     */
    public function showAction(Request $request, Referencia $referencia)
    {

        $this->bread->push($request->getRequestUri(), $referencia->getNombre());

        $mapa = $this->gmapsManager->createMap();
        $mapa->setSize('100%', '250px');
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($referencia->getLatitud());
        $mapa->setIniLongitud($referencia->getLongitud());
        $mapa->setIncludeJQuery(false);

        $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
        $deleteForm = $this->createDeleteForm($referencia->getId());

        return $this->render('app/Referencia/show.html.twig', array(
            'menu' => $this->getShowMenu($referencia),
            'referencia' => $referencia,
            'delete_form' => $deleteForm->createView(),
            'mapa' => $mapa,
            'precios' => $this->referenciaManager->getPreciosPortales($referencia),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $referencia 
     * @return array $menu
     */
    private function getShowMenu($referencia)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_EDITAR')) {  //puedo editar
            if ($this->userloginManager->getOrganizacion()->getTipoOrganizacion() == 2) {  //cliente
                if ($this->userloginManager->getOrganizacion() == $referencia->getPropietario()) {
                    $menu['Editar'] = array(
                        'imagen' => 'icon-edit icon-blue',
                        'url' => $this->generateUrl('referencia_edit', array(
                            'id' => $referencia->getId()
                        ))
                    );
                }
            } else {  //distribuidor
                $menu['Editar'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl('referencia_edit', array(
                        'id' => $referencia->getId()
                    ))
                );
            }
        }
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_ELIMINAR')) {
            if ($this->userloginManager->getOrganizacion()->getTipoOrganizacion() == 2) {  //cliente
                if ($this->userloginManager->getOrganizacion() == $referencia->getPropietario()) {  //es el dueño
                    $menu['Eliminar'] = array(
                        'imagen' => 'icon-minus icon-blue',
                        'url' => '#modalDelete',
                        'extra' => 'data-toggle=modal',
                    );
                }
            } else {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
        return $menu;
    }

    /**
     * @Route("/referencia/{id}/new", name="referencia_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_AGREGAR") 
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        $referencia = $this->referenciaManager->create($organizacion);
        $options = array(
            'propietario' => $referencia->getPropietario(),
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'em' => $this->getDoctrine()->getManager(),
        );
        $form = $this->createForm(ReferenciaType::class, $referencia, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->referenciaManager->save($referencia)) {
                $notificar = $this->notificadorAgenteManager->notificar($referencia, 1);
                $this->bread->pop();
                $this->setFlash('success', sprintf('La referencia "%s" han sido creada', strtoupper($referencia->getNombre())));
                return $this->redirect($this->generateUrl('referencia_show', array('id' => $referencia->getId())));
            } else {
                $this->setFlash('error', 'Error en la grabación de la referencia.');
            }
        }
        $this->bread->push($request->getRequestUri(), "Nueva Referencia");
        return $this->render('app/Referencia/new.html.twig', array(
            'referencia' => $referencia,
            'form' => $form->createView(),
            'mapa' => $this->getMapa($referencia),
            'geocoder' => new Geocoder(),
            'iconos' => glob('images/iconos/*.*'),
        ));
    }

    public function getMapa($referencia)
    {
        $mapa = $this->gmapsManager->createMap(false);
        $mapa->setSize('80%', '250px');
        $mapa->setIniZoom(16);
        if (is_null($referencia->getLatitud()) || $referencia->getLatitud() == 0) {
            $referencia->setLatitud($referencia->getPropietario()->getLatitud());
            $referencia->setLongitud($referencia->getPropietario()->getLongitud());
        }
        $mapa->setIniLongitud($referencia->getPropietario()->getLongitud());
        $mapa->setIniLatitud($referencia->getLatitud());
        $mapa->setIniLongitud($referencia->getLongitud());
        $marcador = $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, false, false);
        $marcador->setDraggable(true); // habilita la posibilidad de mover un marcador
        $marcador->setRaiseOnDrag(true); // permite que el marcador se "levante" del suelo al arrastrarlo
        $marcador->setClickListener('clickMarcador');
        $marcador->setDragListener('dragMarcador');
        $marcador->setDragendListener('finDragMarcador');

        return $mapa;
    }

    /**
     * @Route("/referencia/{id}/edit", name="referencia_edit",
     * requirements={
     *         "id": "\d+"
     *     }),
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_EDITAR")
     */
    public function editAction(Request $request, Referencia $referencia)
    {

        $options = array(
            'propietario' => $referencia->getPropietario(),
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'em' => $this->getDoctrine()->getManager(),
        );
        $form = $this->createForm(ReferenciaType::class, $referencia, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();
            if ($this->referenciaManager->save($referencia)) {
                $notificar = $this->notificadorAgenteManager->notificar($referencia, 1);
                $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($referencia->getNombre())));
                return $this->redirect($this->bread->getVolver());
            } else {
                $this->setFlash('error', 'Error en la grabación de Referencia.');
            }
        }

        $this->bread->push($request->getRequestUri(), "Editar Referencia");

        //llego aca porque algo mal salio y debo generar nuevamente el mapa para la vista.
        $mapa = $this->gmapsManager->createMap(false);
        $mapa->setSize('100%', '250px');
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($referencia->getLatitud());
        $mapa->setIniLongitud($referencia->getLongitud());

        $marcador = $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, false, false);
        $marcador->setDraggable(true); // habilita la posibilidad de mover un marcador
        $marcador->setRaiseOnDrag(true); // permite que el marcador se "levante" del suelo al arrastrarlo
        $marcador->setClickListener('clickMarcador');
        $marcador->setDragListener('dragMarcador');
        $marcador->setDragendListener('finDragMarcador');

        $geocoder = new Geocoder();
        $geocoder->setDebug(true);

        //die('////<pre>' . nl2br(var_export($referencia->getSalida(), true)) . '</pre>////');
        return $this->render('app/Referencia/edit.html.twig', array(
            'referencia' => $referencia,
            'form' => $form->createView(),
            'mapa' => $mapa,
            'geocoder' => $geocoder,
            'iconos' => glob('images/iconos/*.*'),
        ));
    }

    /**
     * @Route("/referencia/{id}/delete", name="referencia_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_ELIMINAR") 
     */
    public function deleteAction(Request $request, Referencia $referencia)
    {
        $id = $referencia->getId();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->bread->pop();
            $ref = new Referencia();
            $ref->setId($id);
            $ref->setUpdatedAt($referencia->getUpdatedAt());
            $ref->setCreatedAt($referencia->getCreatedAt());
            $this->referenciaManager->delete($id);
            $notificar = $this->notificadorAgenteManager->notificar($ref, 2);
        }

        return $this->redirect($this->bread->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Crea un referencia. Los datos fueron pasados por ajax.
     * Devuelvo un ajax con la nueva referencia creada para que la vista la 
     * incorpore como una referencia mas.
     * 
     * @Route("/referencia/create/ajax", name="main_referencia_create_ajax")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function createajaxAction(Request $request)
    {
        $status = true;
        $newRef = null;
        $form_referencia = array();
        parse_str($request->get('referencia'), $form_referencia);
        $data = isset($form_referencia['formcr']) ? $form_referencia['formcr'] : null;

        unset($form_referencia); //no uso mas el $form_consulta.
        if ($data) {
            $organizacion = $this->organizacionManager->find($data['organizacion']);
            $referencia = $this->referenciaManager->create($organizacion);
            $referencia->setNombre($data['nombre']);
            $referencia->setPathIcono($data['pathIcono']);
            $referencia->setVisibilidad(true);
            $referencia->setEntrada(true);
            $referencia->setSalida(true);
            $referencia->setColor(isset($data['color']) ? $data['color'] : '#ffffff');
            $referencia->setTransparencia(isset($data['transparencia']) ? $data['transparencia'] : 0.5);

            $referencia->setClase($data['clase']);

            if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
                $referencia->setArea(isset($data['area']) ? $data['area'] : 0.0);
                $referencia->setPoligono($data['poligono']);

                //hago esto para traer el punto central del poligono.
                $puntos = $this->gmapsManager->parsePoligono($referencia);
                $referencia->setLatitud($puntos['latitud']);
                $referencia->setLongitud($puntos['longitud']);
            } else {
                try {
                    $centro = explode(',', substr($data['centro'], 1, strlen($data['centro']) - 2));
                    if (count($centro) >= 2) {
                        $referencia->setLatitud($centro[0]);
                        $referencia->setLongitud($centro[1]);
                        $referencia->setRadio($data['radio']);
                    } else {
                        $status = 'Falta Latitud y Longitud, posicionese sobre el mapa.';
                    }
                } catch (\Exception $ex) {
                    $status = 'Falta Latitud y Longitud, posicionese sobre el mapa.';
                }
            }

            if (isset($data['tipoReferencia']) && $data['tipoReferencia'] != '') {
                $em = $this->getEntityManager();
                $tRef = $em->getRepository('App:TipoReferencia')->find($data['tipoReferencia']);
                $referencia->setTipoReferencia($tRef);
            } else {
                $status = 'Falta tipo de referencia';
            }

            if (isset($data['categoria']) && $data['categoria'] != '') {
                $em = $this->getEntityManager();
                $categoria = $em->getRepository('App:Categoria')->findOneBy(array('id' => intval($data['categoria'])));
                $referencia->setCategoria($categoria);
            }

            if (isset($data['identificador']) && $data['identificador'] != '') {
                $referencia->setCodigoExterno($data['identificador']);
            }
            if ($status === true) {
                if (!$this->referenciaManager->check($referencia)) { //verificar poligono
                    $referencia = $this->referenciaManager->fix($referencia); //redibujar poligono
                } else {
                    $referencia->setStatus(1); //verificada
                }
                $this->referenciaManager->save($referencia);
                $newRef = $this->gmapsManager->getDataReferencia($referencia);
                $notificar = $this->notificadorAgenteManager->notificar($referencia, 1);
            } else {
                $newRef = null;
            }
            //es el retorno.
        }
        return new Response(json_encode(array(
            'result' => $status,
            'referencia' => $newRef,
        )));
    }

    /**
     * Actualiza una referencia. Los datos fueron pasados por ajax.
     * Devuelvo un ajax con la nueva referencia creada para que la vista la 
     * incorpore como una referencia mas.
     *
     * @Route("/referencia/update/ajax", name="main_referencia_update_ajax")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function updateajaxAction(Request $request)
    {
        $form_referencia = array();
        parse_str($request->get('referencia'), $form_referencia);
        $data = $form_referencia['formrr'];
        unset($form_referencia); //no uso mas el $form_consulta.
        if ($data) {
            $em = $this->getEntityManager();

            $referencia = $this->referenciaManager->find($data['referencia_id']);
            $referencia->setPathIcono($data['pathIcono']);
            $referencia->setColor(isset($data['color']) ? $data['color'] : '#FFFFFF');
            $referencia->setTransparencia(isset($data['transparencia']) ? $data['transparencia'] : 0.5);

            //es un poligono viejo, entonces se tiene que cambiar a poligono nuevo.
            if ($referencia->isOldPoligon()) {
                $referencia->setClase($referencia->getClase());
            }

            if ($referencia->getClase() == $referencia::CLASE_POLIGONO) {
                $referencia->setArea(isset($data['area']) ? $data['area'] : 0.0);
                $referencia->setPoligono($data['poligono']);

                //hago esto para traer el punto central del poligono.
                $puntos = $this->gmapsManager->parsePoligono($referencia);
                $referencia->setLatitud($puntos['latitud']);
                $referencia->setLongitud($puntos['longitud']);
            } else {
                $centro = explode(',', substr($data['centro'], 1, strlen($data['centro']) - 2));
                //die('////<pre>' . nl2br(var_export($centro), true) . '</pre>////');
                $referencia->setLatitud($centro[0]);
                $referencia->setLongitud($centro[1]);
                $referencia->setRadio($data['radio']);
            }
            if (!$this->referenciaManager->check($referencia)) { //verificar poligono
                $referencia = $this->referenciaManager->fix($referencia); //redibujar poligono
            } else {
                $referencia->setStatus(1); //verificada
            }
            $this->referenciaManager->save($referencia);

            $newRef = $this->gmapsManager->getDataReferencia($referencia);
            $notificar = $this->notificadorAgenteManager->notificar($referencia, 1);
            //es el retorno.
            return new Response(json_encode(array(
                'result' => 'ok',
                'referencia' => $newRef,
            )));
        }
    }

    /**
     * @Route("/mapa/referencia/info", name="main_referencia_info_ajax")
     * @Method({"GET", "POST"})
     */
    public function infoajaxAction(Request $request)
    {
        $id = explode('r', $request->get('id_sc'));
        $referencia = $this->referenciaManager->find($id[1]);
        $html = '<h3>' . $referencia->getNombre() . '</h3></br>';
        //$html .= '<h4>' . $referencia->getId() . '</h4></br>';
        $html .= 'Organización: <b>' . $referencia->getPropietario()->getNombre() . '</b></br>';
        $html .= 'Clase: <b>' . $referencia->getStrClase() . '</b></br>';

        if ($referencia->getClase() == $referencia::CLASE_RADIAL) {
            $html .= 'Radio: <b>' . $referencia->getRadio() . ' mts.</b></br>';
        }
        $html .= 'Categoria: <b>' . $referencia->getCategoria() . '</b></br>';
        if (!is_null($referencia->getStatus())) {
            $html .= 'Estado: <b>' . $referencia->getStrStatus() . '</b></br>';
        } else {
            $html .= 'Estado: <b>' . 'No Checkeada' . '</b></br>';
        }
        if (!is_null($referencia->getTipoReferencia())) {
            $html .= 'Tipo Referencia: <b>' . $referencia->getTipoReferencia() . '</b></br>';
        }
        $html .= 'Lat/Lon: <b>' . $referencia->getLatitud() . ', ' . $referencia->getlongitud() . '</b></br>';
        if ($referencia->getArea() != null || $referencia->getArea() != 0) {
            $html .= sprintf('Area: <b>%10.2f m2 (%10.2f ha)</b></br>', $referencia->getArea(), $referencia->getArea() / 10000);
        }
        $parm = array(
            $referencia->getId(),
            $referencia->getLatitud(),
            $referencia->getLongitud(),
        );
        //tengo que redirigir a apmon o sauron segun corresponda al usuario logueado.
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_EDITAR')) {
            if ($this->userloginManager->getOrganizacion()->getTipoOrganizacion() == 1) {
                $redibujar = true;
                $path_edit = $this->generateUrl('referencia_edit', array('id' => $referencia->getId()));
            } else {
                if ($this->userloginManager->getOrganizacion() == $referencia->getPropietario()) {
                    $redibujar = true;
                    $path_edit = $this->generateUrl('referencia_edit', array('id' => $referencia->getId()));
                }
            }
            if (isset($redibujar) && $redibujar) {
                $html .= '<a href="#" onclick="redibujarReferencia(' . implode(',', $parm) . ')" title="Permite cambiar el tamaño del dibujo, color y transparencia."><i class="icon-pencil"></i>Redibujar</a></br>';
            }
            if (isset($path_edit)) {
                $html .= '<a href="' . $path_edit . '" title="Cambia los datos de la referencia asociada." ><i class="icon-map-marker"></i>Editar</a>';
            }
        }
        return new Response($html);
    }

    /**
     * @Route("/mapa/referencia/search", name="main_referencia_search_ajax")
     * @Method({"GET", "POST"})
     */
    public function searchajaxAction(Request $request)
    {
        $query = $request->get('query');
        if (is_null($query)) {
            $referencias = $this->referenciaManager->findAsociadas();
        } else {
            $referencias = $this->referenciaManager->findByNombreAndUsuario(strtolower($query), $this->userloginManager->getUser());
        }
        if (is_null($referencias)) {
            $html = '<em>No hay resultados para la busqueda.</em>';
        } else {
            $html = $this->renderView('apmon/Mapa/dialog-referencia-ubicar-result.html.twig', array(
                'referencias' => $referencias
            ));
        }
        //        die('////<pre>' . nl2br(var_export($html, true)) . '</pre>////');
        return new Response(json_encode(array('referencias' => $html)));
    }

    /**
     * @Route("/mapa/referencia/griiconos", name="main_referencia_obtenergrillaiconos_ajax")
     * @Method({"GET", "POST"})
     */
    public function obtenergrillaiconosajaxAction()
    {
        return new Response(
            $this->renderView(
                'apmon/Mapa/dialog-iconos-grilla.html.twig',
                array(
                    'iconos' => glob('images/iconos/*.*')
                )
            )
        );
    }

    /**
     * @Route("/mapa/referencia/marker", name="main_referencia_marker")
     * @Method({"GET", "POST"})
     */
    public function markerajaxAction(Request $request)
    {
        $id = $request->get('id');
        if ($id) {
            $referencias[] = $this->referenciaManager->find($id);
            $respuesta = $this->gmapsManager->castReferencias($referencias);
        }
        //        die('////<pre>' . nl2br(var_export($html, true)) . '</pre>////');
        return new Response(json_encode($respuesta));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/sauron/referencia/batch", name="referencia_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_SHARE")
     */
    public function batchAction(Request $request)
    {
        $checks = $request->get('checks');
        $accion = $request->get('accion');
        if ($request->getMethod() == 'POST') {
            $this->bread->pop();

            $organizacion = $this->organizacionManager->find($request->get('new_organizacion'));
            $result = true;
            foreach ($checks as $key => $value) {
                if ($accion == 'copiar') {
                    $result = $result && $this->referenciaManager->copiar($value, $organizacion);
                } elseif ($accion == 'mover') {
                    $result = $result && $this->referenciaManager->mover($value, $organizacion);
                } elseif ($accion == 'compartir') {
                    $result = $result && $this->referenciaManager->compartir($value, $organizacion);
                } else {
                    $this->setFlash('error', 'Operacion no soportada');
                    return $this->redirect($this->bread->getVolver());
                }
            }

            if ($result == true) {
                $this->setFlash('success', sprintf('Se ha realizado la operacion de <b>%s</b> para las referencias hacia la Organizacion <b>%s</b>', strtoupper($accion), strtoupper($organizacion->getNombre())));
            } else {
                $this->setFlash('error', 'Se ha producido un error en la copia de las referencias');
            }
            return $this->redirect($this->bread->getVolver());
        } else {
            $this->bread->push($request->getRequestUri(), "Copiar Referencia");
            $checks = explode(' ', $request->get('checks'));
            $referencias = array();
            foreach ($checks as $id) {
                $referencias[] = array(
                    'id' => $id,
                    'nombre' => $this->referenciaManager->find($id)->getNombre()
                );
            }
            return $this->render("app/Referencia/transferir_batch.html.twig", array(
                'organizaciones' => $this->organizacionManager->getTreeOrganizaciones(),
                'organizacion' => $this->userloginManager->getOrganizacion(),
                'funcion' => $accion,
                'checks' => $checks,
                'referencias' => $referencias,
            ));
        }
    }

    /**
     * @Route("/referencia/{idref}/{idorg}/remover", name="referencia_remover",
     *     requirements={
     *         "idref": "\d+",
     *         "idorg": "\d+"
     *     })
     * @ParamConverter("referencia", options={"mapping":{"idref" = "id"}})
     * @ParamConverter("organizacion", options={"mapping":{"idorg" = "id"}})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_SHARE")
     */
    public function removerAction(Request $request, Referencia $referencia, Organizacion $organizacion)
    {
        $grupo = $this->referenciaManager->remover($referencia, $organizacion);
        $notificar = $this->notificadorAgenteManager->notificar($referencia, 1); // es un update porque no se elimina totalmente
        if (!$grupo) {
            $this->setFlash('error', 'Se ha producido un error al remover el grupo');
        } else {
            $this->setFlash('success', 'Se ha removido el grupo');
        }
        return $this->redirect($this->bread->getVolver());
    }

    /**
     * @Route("/app/referencia/kml/{id}/importacion", name="referencia_kml_importacion",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_AGREGAR")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {
        $this->bread->pushPorto($request->getRequestUri(), "Importación de Kml");
        $save = true;
        if ($request->getMethod() == 'POST') {
            $archivo = $request->files->get('import_archivo');
            $destino = $this->getParameter('kernel.project_dir') . '/web/uploads/import';
            $nombreArchivo = md5(uniqid()) . '.' . $archivo->getClientOriginalExtension();
            $archivo->move($destino, $nombreArchivo);    
            $referencias = $this->referenciaManager->saveKml($destino . '/' . $nombreArchivo);
            if ($referencias) {
                $mapa = $this->gmapsManager->createMap(true);
                $mapa->setSize('100%', '300px');
                $mapa->setIniZoom(16);
                $mapa->setIniLatitud($referencias[0]->getLatitud());
                $mapa->setIniLongitud($referencias[0]->getLongitud());
               foreach ($referencias as $referencia) {
                    $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
                }
             
                $this->setFlash('success', 'Referencias creadas con éxito');
                return $this->render('app/Referencia/importacion_exito.html.twig', array(
                    'mapa'=> $mapa,
                    'organizacion'=> $organizacion,
                    'referencias'=> $referencias
                ));
            }
            $this->setFlash('error', 'Se ha producido un error al crear referencias. Verifique');
            return $this->redirect($this->bread->getVolver());
        }
        return $this->render('app/Referencia/importacion.html.twig', array(
            'organizacion' => $organizacion,

        ));
    }
}
