<?php

namespace App\Controller\app;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Form\app\ServicioCambioOdometroType;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\BitacoraServicio;
use App\Form\sauron\EquiposEnDepositoType;
use App\Form\app\ServicioNewType;
use App\Form\app\ServicioEditType;
use App\Form\app\ServicioBajaType;
use App\Form\app\OdometroHorometroType;
use App\Form\app\FlotaType;
use App\Form\app\ServicioChoferType;
use App\Model\app\ExcelManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\SensorManager;
use App\Model\app\EquipoManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UsuarioManager;
use App\Model\app\UtilsManager;
use App\Model\app\EventoManager;
use App\Model\app\TareaMantManager;
use App\Model\app\GmapsManager;
use App\Model\app\BackendManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ProgramacionManager;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\BitacoraManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\GeocoderManager;
use App\Model\app\TipoServicioManager;
use App\Model\crm\PeticionManager;
use App\Model\crm\Router\PeticionRouter;
use App\Model\app\PrestacionManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\monitor\EmpresaManager;
use App\Model\monitor\TransporteManager;
use App\Model\monitor\SatelitalManager;
use App\Model\app\Router\ServicioRouter;
use App\Model\app\Router\EquipoRouter;
use App\Model\app\Router\CombustibleRouter;
use App\Model\fuel\TanqueRouter;
use App\Model\app\Router\OrdenTrabajoRouter;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\Router\MantenimientoRouter;
use App\Model\app\ChoferManager;
use App\Model\app\IndiceManager;
use App\Entity\Servicio;
use App\Entity\Organizacion;
use App\Event\ServicioEvent;
use App\Form\apmon\ChoferServicioFilter;
use Knp\Snappy\Pdf;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class ServicioController extends AbstractController
{

    private $utilsManager;
    private $bread;
    private $eventoManager;
    private $organizacionManager;
    private $userlogin;
    private $referenciaManager;
    private $gmapsManager;
    private $servicioManager;
    private $geocoderManager;
    private $prgManager;
    private $bitacoraManager;
    private $bitacoraGralManager;
    private $combustibleManager;
    private $tareaMantManager;
    private $sensorManager;
    private $equipoManager;
    private $servicioRouter;
    private $equipoRouter;
    private $combustibleRouter;
    private $tanqueRouter;
    private $mantRouter;
    private $usuarioManager;
    private $transporteManager;
    private $empresaManager;
    private $satelitalManager;
    private $ordenTrabajoRouter;
    private $ordenTrabajoManager;
    private $peticionManager;
    private $prestacionManager;
    private $backendManager;
    private $excelManager;
    private $tipoServicioManager;
    private $notificadorAgenteManager;
    private $choferManager;
    private $indiceManager;
    private $moduloMantenimiento;
    private $moduloMonitor;
    private $moduloCentroCosto;
    private $peticionRouter;
    private $eventDispatcher;

    public function __construct(
        UtilsManager $utilsManager,
        BreadcrumbManager $breadcrumbManager,
        EventoManager $eventoManager,
        OrganizacionManager $organizacionManager,
        UserLoginManager $userloginManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        GeocoderManager $geocoderManager,
        ProgramacionManager $prgManager,
        BitacoraServicioManager $bitacoraManager,
        CombustibleManager $combustibleManager,
        TareaMantManager $tmantManager,
        SensorManager $sensorManager,
        EquipoManager $equipoManager,
        ServicioRouter $servicioRouter,
        EquipoRouter $equipoRouter,
        CombustibleRouter $fuelRouter,
        BackendManager $backendManager,
        TanqueRouter $tanqueRouter,
        MantenimientoRouter $mantRouter,
        UsuarioManager $usuarioManager,
        ExcelManager $excelManager,
        TransporteManager $transporteManager,
        EmpresaManager $empresaManager,
        SatelitalManager $satelitalManager,
        OrdenTrabajoManager $otManager,
        OrdenTrabajoRouter $otRouter,
        PeticionManager $peticionManager,
        PrestacionManager $prestacionManager,
        PeticionRouter $peticionRouter,
        TipoServicioManager $tipoServicioManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        BitacoraManager $bitacoraGralManager,
        ChoferManager $choferManager,
        IndiceManager $indiceManager,
        EventDispatcherInterface $eventDispatcher,
    ) {
        $this->prestacionManager = $prestacionManager;
        $this->peticionRouter = $peticionRouter;
        $this->peticionManager = $peticionManager;
        $this->ordenTrabajoManager = $otManager;
        $this->ordenTrabajoRouter = $otRouter;
        $this->transporteManager = $transporteManager;
        $this->empresaManager = $empresaManager;
        $this->satelitalManager = $satelitalManager;
        $this->usuarioManager = $usuarioManager;
        $this->mantRouter = $mantRouter;
        $this->tanqueRouter = $tanqueRouter;
        $this->combustibleRouter = $fuelRouter;
        $this->equipoRouter = $equipoRouter;
        $this->servicioRouter = $servicioRouter;
        $this->equipoManager = $equipoManager;
        $this->sensorManager = $sensorManager;
        $this->tareaMantManager = $tmantManager;
        $this->combustibleManager = $combustibleManager;
        $this->prgManager = $prgManager;
        $this->bitacoraManager = $bitacoraManager;
        $this->utilsManager = $utilsManager;
        $this->bread = $breadcrumbManager;
        $this->eventoManager = $eventoManager;
        $this->organizacionManager = $organizacionManager;
        $this->userlogin = $userloginManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->servicioManager = $servicioManager;
        $this->geocoderManager = $geocoderManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->choferManager = $choferManager;
        $this->bitacoraGralManager = $bitacoraGralManager;
        $this->indiceManager = $indiceManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/servicio/{id}/list", name="servicio_list",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER") 
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {
        if ($this->userlogin->isDistribuidor()) {
            $response = $this->listDistribuidorAction($request, $organizacion);
        } else {
            $response = $this->listClienteAction($request, $organizacion);
        }
        return $response;
    }

    /**
     * @Route("/servicio/{id}/list/cliente", name="servicio_list_cliente",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN") 
     */
    public function listClienteAction(Request $request, Organizacion $organizacion)
    {
        $menu[1] = array(
            $this->servicioRouter->btnNew(),
            $this->equipoRouter->btnNew($organizacion),
        );
        $this->moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
        $this->moduloCentroCosto = $this->userlogin->isGranted('ROLE_APMON_PRESTACION_ADMIN');
        $this->moduloMantenimiento = $this->userlogin->isGranted('ROLE_APMON_MANT_ADMIN');

        $options = array(
            'empresas' => $this->empresaManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'satelitales' => $this->satelitalManager->findAllByOrganizacion($organizacion),
            'claseServicio' => $this->tipoServicioManager->findAll($organizacion),
            'moduloMonitor' => $this->moduloMonitor,
            'arrayStrTipoObjetos' => SERVICIO::TIPOOBJETOS,
            'arrayStrEstado' => SERVICIO::ESTADO
        );
        $consulta = array(
            'tipoServicio' => -1,
            'claseServicio' => 0,
            'transporte' => 0,
            'satelital' => 0,
            'empresa' => 0
        );
        $servicios = $this->servicioManager->findAllByUsuario(); //los necesito a todos para los índices
        $servicios_flota = array();
        $indices = array();
        if ($this->moduloMantenimiento) {
            $indices = $this->getIndices($servicios);
        }
        foreach ($servicios as $key => $servicio) {
            if ($servicio->getEstado() != 0 && $servicio->getEstado() != 4) { //qe estén habil y no dados de baja
                //  die('////<pre>' . nl2br(var_export($servicio->getNombre(), true)) . '</pre>////');
                $servicios_flota[] = $servicio;
            }
        }

        $formFlota = $this->createForm(FlotaType::class, null, $options);
        $this->bread->pushPorto($request->getRequestUri(), "Ver Flota");
        return $this->render('app/Servicio/list_cliente.html.twig', array(
            'menu' => $menu,
            'consulta' => $consulta,
            'servicios' => $servicios_flota,
            'indices' => $indices,
            'formFlota' => $formFlota->createView(),
            'filtro' => 'filtro:Default',
            'moduloMonitor' => $this->moduloMonitor,
            'moduloCentroCosto' => $this->moduloCentroCosto,
            'moduloMantenimiento' => $this->moduloMantenimiento
        ));
    }

    private function getIndices($servicios)
    {        
        $indices = array();
        foreach ($servicios as $servicio) {
            $indices[$servicio->getId()] = null;
            /*$indice = $this->indiceManager->getLastInsertServicio($servicio);
            if ($indice) {
                $indices[$servicio->getId()] = array(
                    'mantenimiento_rn' => $indice->getMantenimientoNuevo() > 0 ? round($indice->getMantenimientoRealizado() / $indice->getMantenimientoNuevo(), 3) : 0,
                    'mantenimiento_rp' => $indice->getMantenimientoPendiente() > 0 ? round($indice->getMantenimientoRealizado() / $indice->getMantenimientoPendiente(), 3) : 0,
                    'peticion_rn' => $indice->getPeticionNueva() > 0 ? round($indice->getPeticionRealizada() / $indice->getPeticionNueva(), 3) : 0,
                    'peticion_rp' => $indice->getPeticionPendiente() > 0 ? round($indice->getPeticionRealizada() / $indice->getPeticionPendiente(), 3) : 0,
                );
            } else {
                $indices[$servicio->getId()] = null;
            }*/
        }
        return $indices;
    }

    /**
     * @Route("/servicio/{id}/list/odometro", name="servicio_list_odometro",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN") 
     */
    public function listOdometroAction(Request $request, Organizacion $organizacion)
    {
        $serviciosAll = $this->servicioManager->findAllByUsuario();
        $teoria = array();
        $servicios = null;
        foreach ($serviciosAll as $servicio) {  //recorro todos los equipos y saco solo los deshabilitados.
            if ($servicio->getEstado() == 0) {   //solo para los vehiculos deshabilitados
                $servicios[] = $servicio;
                if ($servicio->getFechaUltCambio()) {  //solo puedo calcular si tengo fecha.                        
                    $horometro = $this->obtenerValores($servicio);
                    $teoria[$servicio->getId()] = array(
                        'total' => $this->formatHorometro($horometro['total']),
                        'diferencia' => $this->formatHorometro($horometro['diff']),
                        'prestacion' => $this->prestacionManager->findActivo($servicio),
                        'ult_registro' =>  $this->formatUltRegistro($servicio)

                    );
                }
            }
        }

        $this->bread->push($request->getRequestUri(), "Servicio Sin Equipos");
        return $this->render('app/Servicio/list_sinequipo.html.twig', array(
            'menu' => null,
            'servicios' => $servicios,
            'data' => $teoria,
            'form' => $this->createForm(OdometroHorometroType::class)->createView(),
        ));
    }

    private function formatUltRegistro($servicio)
    {
        $ultOdometro =  $this->bitacoraManager->obtenerUltByFecha($servicio, BitacoraServicio::EVENTO_CAMBIO_ODOMETRO);
        $ultHorometro = $this->bitacoraManager->obtenerUltByFecha($servicio, BitacoraServicio::EVENTO_CAMBIO_HOROMETRO);
        $str = '';
        if ($ultOdometro) {
            $str .= '<b>Odómetro:</b><label>' . ' ' . $this->bitacoraManager->parse($ultOdometro) . ',Ejecutor:  <b>' . $ultOdometro->getEjecutor()->getNombre() . '</b></label>';
        }
        if ($ultHorometro) {
            $str .= "<br> " . '<b>Horometro:</b><label>' . ' ' . $this->bitacoraManager->parse($ultHorometro) . ', Ejecutor:  <b>' . $ultHorometro->getEjecutor()->getNombre() . '</b></label>';
        }
        return $str;
    }

    /**
     * @Route("/servicio/{id}/list/distribuidor", name="servicio_list_distribuidor",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER") 
     */
    public function listDistribuidorAction(Request $request, Organizacion $organizacion)
    {
        //grabo el idorg en la session para que se perdurre en los refresh
        $request->getSession()->set('sauron_mapa_organizacion', $organizacion->getId());

        $this->bread->push($request->getRequestUri(), 'Flota: ' . $organizacion->getNombre());

        //esta es la organizacion que debo traer la flota
        $servicios = $this->servicioManager->findAll($organizacion, array('nombre', 'ASC'));

        //genero datos adicionales a los que vienen en $servicios.
        $status = array();
        foreach ($servicios as $s) {
            if ($s->getUltfechahora()) {
                $intervalo = date_diff(date_create(), date_create($s->getUltfechahora()->format('Y-m-d H:i:s')));
                $intervalo = $intervalo->format('%d');
            } else {
                $intervalo = -1;
            }
            $status[$s->getId()] = array(
                'st_ultreporte' => $intervalo,
            );
        }

        //Aca armo el menu de opciones que se muestra a la derecha del listado
        $menu = array(
            1 => array(
                $this->servicioRouter->btnFlotaEnMapa($organizacion),
                $this->equipoRouter->btnNew($organizacion),
            )
        );

        return $this->render('app/Servicio/list.html.twig', array(
            'menu' => $this->servicioRouter->toCalypso($menu),
            'status' => $status,
            'organizacion' => $organizacion,
            'organizaciones' => $this->organizacionManager->getTreeOrganizaciones(null, true),
            'servicios' => $servicios,
        ));
    }

    /**
     * @Route("/servicio/{id}/show/{tab}", name="servicio_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER") 
     */
    public function showAction(Request $request, Servicio $servicio, $tab)
    {
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        $desde = (new \DateTime())->modify('-30 days');
        $hasta = new \DateTime();

        $param = array(
            'servicio' => $servicio,
            'json_servicio' => json_encode(array('id' => $servicio->getId(), 'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : null)),
            'tab' => is_null($tab) ? 'main' : $tab,
            'refresh' => false,
            'delete_form' => $this->createDeleteForm($servicio->getId())->createView(),
            'isPortalEnabled' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_PORTAL'),
            'modulomantenimiento' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_MANTENIMIENTO'),
            'modulocombustible' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'MANAGER_COMBUSTIBLE'),            
        );

        $menu[1] = $this->getShowMenu($servicio);
        //aca hago las cosas para cada tab segun se necesite.
        $this->bread->pushPorto($request->getRequestUri(), sprintf('%s (%s)', substr($servicio->getNombre(), 0, 10), $tab));
        switch ($tab) {
            case 'main':
                if ($this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER')) {
                    $menu[6] = array($this->servicioRouter->btnExporQr($servicio));
                }
                break;
            case 'registro':
                //$param['menu'] = $this->getShowMenu($servicio);
                //aca creo el mapa para mostrar el panicso
                $mapa = $this->gmapsManager->createMap(true);
                $mapa->setSize('100%', '240px');
                $mapa->setIniZoom(16);
                $mapa->setIniLatitud($servicio->getUltLatitud());
                $mapa->setIniLongitud($servicio->getUltLongitud());
                $this->gmapsManager->addMarkerServicio($mapa, $servicio, true);
                //agrego las referencias al mapa        
                $referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
                foreach ($referencias as $referencia) {
                    $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true);
                }
                $param['mapa'] = $mapa;
                $datos = $this->getRegistro($servicio, $referencias);
                $param['datos'] = $datos['datos'];
                $param['extras'] = $datos['extras'];
                break;
            case 'config':
                if ($servicio->getEquipo() != null) {
                    $param['cantidad_variables'] = count($this->prgManager->getOnlyVariables($servicio->getEquipo()->getModelo()));
                    $param['parametros'] = $this->servicioManager->getParams($servicio);
                } else {
                    $param['cantidad_variables'] = null;
                    $param['parametros'] = null;
                }
                break;
            case 'sensor':
                $menu[2] = array($this->servicioRouter->btnSensor($servicio));
                $param['sensores'] = $servicio->getSensores();
                break;
            case 'bitacora':
                $registro = $this->bitacoraManager->getRegistros($servicio);
                $param['bitacora'] = array();
                foreach ($registro as $value) {
                    $param['bitacora'][] = array(
                        'registro' => $value,
                        'descripcion' => $this->bitacoraManager->parse($value),
                    );
                }
                break;
            case 'evento':
                $eventos = $this->eventoManager->findAll($servicio->getOrganizacion());
                $param['eventos'] = array();
                foreach ($eventos as $evento) {
                    $param['eventos'][] = array(
                        'evento' => $evento,
                        'inServicio' => $this->eventoManager->inServicio($servicio, $evento),
                    );
                }
                break;
            case 'tanque':
                $mediciones = $this->servicioManager->getMedicionestanque($servicio);
                if ($this->userlogin->isGranted('ROLE_FLOTA_EDITAR')) {
                    $menu[3] = array(
                        $this->tanqueRouter->btnEdit($servicio),
                        $this->tanqueRouter->btnCopyFrom($servicio, $mediciones),
                        $this->tanqueRouter->btnCopyTo($servicio, $mediciones),
                    );
                }
                $param['tanque'] = $mediciones;
                break;
            case 'combustible':
                $menu[4] = array(
                    $this->combustibleRouter->btnNewCarga($servicio),
                );
                $param['cargas'] = $this->combustibleManager->findAllCargas($servicio);
                break;
            case 'mante':
                $mantenimientos = $this->tareaMantManager->findByServicio($servicio);
                $statusmant = array();
                foreach ($mantenimientos as $tarea) {
                    $statusmant[$tarea->getId()] = $this->tareaMantManager->analizeMantenimiento($tarea);
                }
                //dd($statusmant);

                $menu[5] = array(
                    $this->mantRouter->btnNewTarea($servicio),
                    $this->ordenTrabajoRouter->btnNewServicio($servicio)
                );

                $costoTotal = 0;
                $ordenes = $this->ordenTrabajoManager->findByServicio($servicio);
                foreach ($ordenes as $orden) {
                    $costoTotal += $orden->getCostoTotal();
                }
                $peticiones = $this->peticionManager->findByServicio($servicio);
                $nuevas = $cerradas = $encurso = array();
                foreach ($peticiones as $peticion) {
                    if ($peticion->getEstado()->getPosicion() == 1) { //abierta
                        $nuevas[] = $peticion;
                    } else if ($peticion->getEstado()->getPosicion() == 2 || $peticion->getEstado()->getPosicion() == 3) { // en curso o en pausa
                        $encurso[] = $peticion;
                    } elseif ($peticion->getEstado()->getPosicion() == 10) { //cerrada
                        $cerradas[] = $peticion;
                    }
                }

                $consulta['incluir_ok'] = true;

                $param['mantenimientos'] = $mantenimientos;
                $param['statusmant'] = $statusmant;
                $param['consulta'] = null;
                $param['organizacion'] = $servicio->getOrganizacion();
                $param['tareas'] = $this->tareaMantManager->procesarStatusMantenimientoPorServicio(array($servicio), $consulta);
                $param['eventuales'] = $this->tareaMantManager->findHistByServicio($servicio);
                $param['costoTotal'] = $costoTotal;
                $param['nuevas'] = $nuevas;
                $param['encurso'] = $encurso;
                $param['cerradas'] = $cerradas;
                $param['peticiones'] = $peticiones;
                $param['ordenes'] = $this->ordenTrabajoManager->toArray($ordenes);
                // dd($statusmant);
                break;
            case 'prestacion':   //centro de costo                
                $arrayStatus = array();
                $now = new \DateTime();
                $prestaciones = $this->prestacionManager->findByServicio($servicio->getId());
                foreach ($prestaciones as $prestacion) {
                    //if ($prestacion->getInicio_prestacion() <= $now && $now <= $prestacion->getFin_prestacion()) {
                    if (is_null($prestacion->getFin_prestacion())) {
                        $arrayStatus[$prestacion->getId()] = 'Ahora';
                    } elseif ($prestacion->getInicio_prestacion() > $now) {
                        $arrayStatus[$prestacion->getId()] = 'Futuro';
                    } elseif ($prestacion->getFin_prestacion() != null) {
                        $arrayStatus[$prestacion->getId()] = 'Terminado';
                    } else {
                        $arrayStatus[$prestacion->getId()] = '---';
                    }
                }
                $param['prestaciones'] = $prestaciones;
                $param['status'] = $arrayStatus;

                break;
            case 'peticion':
                $menu[5] = array(
                    $this->mantRouter->btnNewTarea($servicio),
                    $this->ordenTrabajoRouter->btnNewServicio($servicio),
                    $this->peticionRouter->btnNew($servicio),
                );
                $param['peticiones'] = $this->servicioManager->findPeticiones($servicio);
                break;
            case 'inputs':
                $menu[5] = array(
                    $this->servicioRouter->btnInputs($servicio)
                );
                $param['inputs'] = $servicio->getInputs();
                break;
            case 'itinerario':
                $itinerarios = $servicio->getItinerarios();
                $param['itinerarios'] = $itinerarios;
                break;
            case 'chofer':
                $param['historico'] = $this->choferManager->historicoFilter($servicio->getOrganizacion(), $desde, $hasta, null, $servicio);
                $param['formHistoricoFilter'] = $this->createForm(ChoferServicioFilter::class, null, array('organizacion' => $servicio->getOrganizacion(),'em' => $this->getEntityManager()))->createView();
                break;
            default:
                //$param['menu'] = $this->getShowMenu($servicio, $userloginManager, $organizacionManager),
                break;
        }
        $param['menu'] = $menu;
        return $this->render('app/Servicio/show.html.twig', $param);
    }

    public function getShowMenu($servicio)
    {
        $menu = array(
            $this->servicioRouter->btnEdit($servicio),
            $this->servicioRouter->btnBaja($servicio),
            $this->servicioRouter->btnRetirarEquipo($servicio),
            $this->servicioRouter->btnAsignarEquipo($servicio),
            $this->servicioRouter->btnConfigOdometro($servicio),
            $this->servicioRouter->btnHistorico($servicio),
            $this->servicioRouter->btnProgramar($servicio),
            $this->servicioRouter->btnCorteMotor($servicio),
            $this->servicioRouter->btnCerrojo($servicio),
            $this->servicioRouter->btnDatosExtra($servicio),
            //$this->combustibleRouter->btnNewCarga($servicio);
        );

        if ($servicio->getEstado() == $servicio::ESTADO_HABILITADO) {

            //solo se pone para el cerrojo
            //                if ($servicio->getCorteCerrojo() && $userlogin->isGranted('ROLE_VAR_CERROJO')) {
            //                    $menu['Manipulación Cerrojo'] = array(
            //                        'imagen' => '',
            //                        'url' => $this->generateUrl('main_programacion_cerrojo', array(
            //                            'id' => $servicio->getId(),
            //                    )));
            //                }
        } else {
        }

        //die('////<pre>' . nl2br(var_export($menu, true)) . '</pre>////');


        return $menu;
    }

    /**
     *
     * @Route("/servicio/{idequipo}/new", name="servicio_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_AGREGAR") 
     */
    public function newAction(Request $request, $idequipo)
    {
        $equipo = null;
        $organizacion = !is_null($request->get('cliente')) ? $this->organizacionManager->find(intval($request->get('cliente'))) : $this->userlogin->getOrganizacion();
        // die('////<pre>'.nl2br(var_export($organizacion->getNombre(), true)).'</pre>////');
        $arr = array();
        if ($idequipo != 0) {
            $equipo = $this->equipoManager->find($idequipo);
            if (!$equipo) {
                throw $this->createNotFoundException('Equipo no encontrado.');
            }
        }

        $this->moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
        //creo el servicio
        $servicio = $this->servicioManager->create($equipo, $organizacion);
        foreach ($servicio->getArrayStrTipoObjetos() as $key => $value) {
            $arr[$value] = $key;
        }
        $options = array(
            'arrayStrTipoObjetos' => $arr,
            'em' => $this->getEntityManager(),
            'organizacion' => $organizacion,
            //'tipoObjeto' => $servicio->getTipoObjeto(),
            'accesorios' => $this->equipoManager->getAccesoriosModelo($servicio->getEquipo()),
            'inputs' => $this->sensorManager->getSensoresDigitales($servicio->getEquipo()),
            'arrayStrTipoObjetos' => $servicio->getArrayStrTipoObjetos(),
            //'required_patente' => $OrganizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_SHELL'),
            'required_portal' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_PORTAL'),
            'arrayStrMedidorCombustible' => $servicio->getArrayStrMedidorCombustible(),
            'monitor' => $this->moduloMonitor,
        );
        //creo el formulario
        $form = $this->createForm(ServicioNewType::class, $servicio, $options);
        $form->handleRequest($request);
        // die('////<pre>'.nl2br(var_export('asd', true)).'</pre>////');
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();

            if ($this->servicioManager->save($servicio)) {
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);  //1 UPDATE, 2 DELETE
                //el servicio q se esta creando es un vehiculo
                if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO) {
                    $this->setFlash('success', sprintf('El servicio <b>%s</b> ha sido agregado a la flota. Complete los datos del vehiculo.', strtoupper($servicio)));
                    return $this->redirect($this->generateUrl('apmon_vehiculo_new', array(
                        'idservicio' => $servicio->getId()
                    )));
                } else {  //es cualquier tipo de servicio.
                    $this->setFlash('success', sprintf('Se ha creado el servicio "%s" en la flota.', strtoupper($servicio)));

                    return $this->redirect($this->generateUrl('servicio_show', array(
                        'id' => $servicio->getId(),
                        'tab' => 'main'
                    )));
                }
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Nuevo Servicio");

        return $this->render('app/Servicio/new.html.twig', array(
            'servicio' => $servicio,
            'form' => $form->createView(),
            'moduloMonitor' => $this->moduloMonitor
        ));
    }

    /**
     * @Route("/servicio/{id}/edit", name="servicio_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function editAction(Request $request, Servicio $servicio)
    {
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        if (is_null($servicio->getFechaAlta())) {
            $fecha = new \DateTime('', new \DateTimeZone($this->userlogin->getTimezone()));
            $fecha = date_format($fecha, 'd-m-Y H:i');
            $servicio->setFechaAlta($fecha);
        } else {
            $fecha = $servicio->getFechaAlta()->format('d-m-Y H:i');
            $servicio->setFechaAlta(date($fecha));
            $old_tipoObjeto = $servicio->getTipoObjeto();   //guardo el tipo de objeto anterior.
        }
        if ($servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO && !is_null($servicio->getVehiculo())) {
            $old_Odometro = $servicio->getVehiculo()->getOdometro();
        }
        $options = array(
            'arrayStrTipoObjetos' => $servicio->getArrayStrTipoObjetos(),
            'organizacion' => $servicio->getOrganizacion(),
            'tipoObjeto' => $servicio->getTipoObjeto(),
            'inputs' => $this->sensorManager->getSensoresDigitales($servicio->getEquipo()),
            'accesorios' => $this->equipoManager->getAccesoriosModelo($servicio->getEquipo()),
            'arrayStrTipoObjetos' => $servicio->getArrayStrTipoObjetos(),
            'required_patente' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_SHELL'),
            'required_portal' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_PORTAL'),
            'arrayStrMedidorCombustible' => $servicio->getArrayStrMedidorCombustible(),
            'monitor' => $this->userlogin->isGranted('ROLE_ITINERARIO_VER')
        );
        $form = $this->createForm(ServicioEditType::class, $servicio, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->servicioManager->save($servicio)) {
                // se cambio de persona -> vehiculo por lo que se deben cargar los datos del vehiculo
                if ($old_tipoObjeto == $servicio::TIPOOBJETO_PERSONA && $servicio->getTipoObjeto() == $servicio::TIPOOBJETO_VEHICULO) {
                    $this->setFlash('success', sprintf('Los datos de <b>%s</b> han cambiado. Complete los datos del vehiculo.', strtoupper($servicio)));
                    return $this->redirect($this->generateUrl('sauron_vehiculo_new', array(
                        'idservicio' => $servicio->getId()
                    )));
                }
                if (isset($old_Odometro) && $old_Odometro !== $servicio->getVehiculo()->getOdometro()) {
                    //                    die('////<pre>'.nl2br(var_export($servicio->getVehiculo()->getOdometro(), true)).'</pre>////');
                    $this->servicioManager->cambiarOdometro($servicio, $servicio->getVehiculo()->getOdometro());
                }
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($servicio)));
                $this->bread->pop();
                return $this->redirect($this->bread->getVolver());
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Editar Servicio");
        return $this->render('app/Servicio/edit.html.twig', array(
            'servicio' => $servicio,
            'form' => $form->createView(),
            'moduloMonitor' => $this->userlogin->isGranted('ROLE_ITINERARIO_VER'),
        ));
    }

    /**
     * @Route("/servicio/{id}/baja", name="servicio_baja",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_BAJA") 
     */
    public function bajaAction(Request $request, Servicio $servicio)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        if (is_null($servicio->getFechaBaja())) {
            $fecha = new \DateTime('', new \DateTimeZone($this->userlogin->getTimezone()));
            $fecha = date_format($fecha, 'd-m-Y H:i');
            $servicio->setFechaBaja($fecha);
        } else {
            $fecha = $servicio->getFechaBaja()->format('d-m-Y H:i');
            $servicio->setFechaBaja(date($fecha));
        }

        $form = $this->createForm(ServicioBajaType::class, $servicio);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();

            if ($this->servicioManager->baja($servicio)) {
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
                $this->setFlash('success', sprintf('La baja de <b>%s</b> han sido procesada', strtoupper($servicio)));

                return $this->redirect($this->bread->getVolver());
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Baja de Servicio");
        return $this->render('app/Servicio/baja.html.twig', array(
            'servicio' => $servicio,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/servicio/{id}/asignar/equipo", name="servicio_asignar_equipo")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_ASIGNAR") 
     */
    public function asignarequipoAction(Request $request, Servicio $servicio)
    {
        $qbEquipos = $this->equipoManager->findAllEnDeposito($servicio->getOrganizacion());
        if ($qbEquipos != null) {
            $options['queryResult'] = $qbEquipos;
            $options['servicio'] = $servicio;
            $form = $this->createForm(EquiposEnDepositoType::class, null, $options);
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data = $form->getData();
                    $equipo = $this->equipoManager->find($data['equipo']);   //busco el equipo seleccionado
                    if ($this->equipoManager->asignarServicio($equipo, $servicio)) {
                        $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
                        $this->setFlash('success', 'El equipo ha sido asociacido al servicio seleccionado');
                        return $this->redirect($this->bread->getVolver());
                    } else {
                        $this->setFlash('error', 'Error al asociar el servicio al equipo');
                    }
                }
            }

            $this->bread->pushPorto($request->getRequestUri(), "Asignar Equipo");
            return $this->render("app/Servicio/asignarequipo.html.twig", array(
                'form' => $form->createView(),
                'servicio' => $servicio
            ));
        } else {
            $this->setFlash('error', sprintf('No existen servicios disponibles para asociar al equipo.'));
            return $this->redirect($this->bread->getVolver());
        }
    }

    /**
     * @IsGranted("ROLE_FLOTA_VER")
     */
    public function getRegistro($servicio, $referencias)
    {
        $datos = $extras = array();
        //rescato la ultima trama
        $trama = json_decode($servicio->getUltTrama(), true);
        if (!is_null($trama)) {
            $datos['Fecha Evento'] = $this->utilsManager->fechaUTC2local(date_create($trama['fecha']), 'd/m/Y H:i:s');
            if (isset($trama['posicion'])) {
                $datos['Lat/Lon'] = $trama['posicion']['latitud'] . ', ' . $trama['posicion']['longitud'];
                $datos['Rumbo'] = $trama['posicion']['direccion'] . '°';
                $datos['Velocidad'] = $trama['posicion']['velocidad'] . ' km/h';
            } else {
                $datos['Lat/Lon'] = '---';
                $datos['Rumbo'] = '---';
                $datos['Velocidad'] = '---';
            }
            if (isset($trama['contacto']))
                $datos['Contacto'] = $trama['contacto'] ? 'SI' : 'NO';
            $datos['Motivo Tx'] = $this->servicioManager->getMotivoTx2Str($trama);
            if (isset($trama['motivo_original'])) {
                $datos['Motivo Original'] = $trama['motivo_original'];
            }
            if (isset($trama['entrada_1']))
                $datos['Input 1'] = isset($trama['entrada_1']) && $trama['entrada_1'] == true ? 'SI' : 'NO';
            if (isset($trama['entrada_2']))
                $datos['Input 2'] = isset($trama['entrada_2']) && $trama['entrada_2'] == true ? 'SI' : 'NO';
            if (isset($trama['entrada_3']))
                $datos['Input 3'] = isset($trama['entrada_3']) && $trama['entrada_3'] == true ? 'SI' : 'NO';
            if (isset($trama['salida_1']))
                $datos['Output 1'] = $trama['salida_1'] ? 'SI' : 'NO';
            if ($this->userlogin->isGranted('ROLE_SECURITY')) {
                if (isset($trama['ip_origen']) && isset($trama['puerto_origen'])) {
                    //$datos['IP Origen'] = $trama['ip_origen'] . ':' . $trama['puerto_origen'];
                    $datos['IP Origen'] = $servicio->getUltIp() . ':' . $servicio->getUltPuerto();
                }
                if (isset($trama['ip_capturador'])) {
                    $datos['IP Capturador'] = $trama['ip_capturador'];
                }
            }
            if (isset($trama['bateria_interna']))
                $datos['Batería Int.'] = $trama['bateria_interna'] . '%';
            if (isset($trama['bateria_general']))
                $datos['Batería Gral.'] = $trama['bateria_general'] . '%';
            if (isset($trama['bateria_externa']))
                $datos['Batería Ext.'] = $trama['bateria_externa'] . '%';
            if (isset($trama['bateria_externa_volts']))
                $datos['Volt Ext.'] = $trama['bateria_externa_volts'] . 'v';
            if (isset($trama['odometro']))
                $datos['Odómetro'] = round((intval($trama['odometro'])) / 1000, 2) . ' kms';
            if (isset($trama['odometro_gps']))
                $datos['Odómetro GPS'] = round((intval($trama['odometro_gps'])) / 1000, 2) . ' kms';
            if (isset($trama['horometro']))
                $datos['Horómetro'] = $this->utilsManager->minutos2tiempo($trama['horometro']);
            if (isset($trama['alimentacion']))
                $datos['Alimentación'] = $trama['alimentacion'] ? 'SI' : 'NO';
            if (isset($trama['temperatura']))
                $datos['Temperatura'] = $trama['temperatura'];
            if (isset($trama['humedad']))
                $datos['Humedad'] = $trama['humedad'];
            if (isset($trama['aceleracion']))
                $datos['Aceleración'] = $trama['aceleracion'];
            if (isset($trama['ibutton']))
                $datos['iButton'] = $trama['ibutton'];
            if (isset($trama['porcentaje_ack']))
                $datos['ACK %'] = $trama['porcentaje_ack'] . '%';
            if (isset($trama['gsm_csq']))
                $datos['GSM Señal'] = $trama['gsm_csq'];
            if (isset($trama['gsm_error']))
                $datos['GSM Error'] = $trama['gsm_error'];
            if (isset($trama['gps_dim']))
                $datos['GPS Dim'] = $trama['gps_dim'];
            if (isset($trama['gps_error']))
                $datos['GPS Error'] = $trama['gps_error'];
            //die('////<pre>' . nl2br(var_export($trama, true)) . '</pre>////');

            $datos['Fecha Recepción'] = $this->utilsManager->fechaUTC2local(date_create($trama['fecha_recepcion']), 'd/m/Y H:i:s');
            $datos['Fecha Guardado'] = $this->utilsManager->fechaUTC2local(date_create($trama['fecha_guardado']), 'd/m/Y H:i:s');
            if (isset($trama['carfinder'])) {
                $datos['Firmware'] = isset($trama['carfinder']['version_firmware']) ? $trama['carfinder']['version_firmware'] : '---';
            }
            if (isset($trama['carcontrol'])) {
                $carcontrol = $trama['carcontrol'];
                //aca van los datos del carfinder.
                if (isset($carcontrol['rpm_motor']))
                    $datos['RPM Motor'] = $carcontrol['rpm_motor'] . ' rpm';
                if (isset($carcontrol['porcentaje_tanque'])) {
                    $datos['Tanque'] = $carcontrol['porcentaje_tanque'] . '% ';
                }
                if (isset($carcontrol['temperatura_motor']))
                    $datos['Temp. Motor'] = $carcontrol['temperatura_motor'] . '°C';
                if (isset($carcontrol['sensor_presion_aceite']))
                    $datos['Sensor Presión Aceite'] = $carcontrol['sensor_presion_aceite'] ? 'SI' : 'NO';
                if (isset($carcontrol['presion_aceite']))
                    $datos['Presión Aceite'] = $carcontrol['presion_aceite'] . '%';
                if (isset($carcontrol['cc_entrada_1']))
                    $datos['Sensor 1'] = $carcontrol['cc_entrada_1'];
                if (isset($carcontrol['cc_entrada_2']))
                    $datos['Sensor 2'] = $carcontrol['cc_entrada_2'];
                if (isset($carcontrol['cc_entrada_3']))
                    $datos['Sensor 3'] = $carcontrol['cc_entrada_3'];
            }

            if (isset($trama['posicion'])) {
                //hago la cercania.            
                $extras['cercania'] = null;
                if ($referencias) {
                    $cercanas = $this->servicioManager->buscarCercaniaPosicion($trama['posicion']['latitud'], $trama['posicion']['longitud'], $referencias, 1);
                    $extras['cercania'] = is_null($cercanas[0]) ? '---' : $cercanas[0]['leyenda'];
                }
                //obtengo la dirección.
                //die('////<pre>'.nl2br(var_export('$direc', true)).'</pre>////');
                $extras['domicilio'] = $this->geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
            }
        }

        return array(
            'datos' => $datos,
            'extras' => $extras,
        );
    }

    /**
     * @Route("/servicio/{id}/cambioodometro", name="servicio_cambio_odometro",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function cambioodometroAction(Request $request, Servicio $servicio)
    {
        //die('////<pre>' . nl2br(var_export($servicio->getEquipo()->getModelo()->getTipoProgramacion(), true)) . '</pre>////');
        $options['servicio'] = $servicio;
        $form = $this->createForm(ServicioCambioOdometroType::class, null, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if (isset($data['fecha'])) {   //aca proceso los kms desde que se tomo hasta ahora.
                $desde = $this->utilsManager->datetime2sqltimestamp($data['fecha'], false);
                $hasta = new \DateTime();
                $hasta = $hasta->format('Y-m-d H:i:s');
                $informe = $this->backendManager->informeDistancias($servicio->getId(), $desde, $hasta, array());
                //die('/////<pre>' . nl2br(var_export($informe['km_total'] . ' | ' . $informe['kms_calculada'], true)) . '</pre>////');
                if (isset($informe['km_total']) && $informe['km_total'] != '---' && isset($informe['kms_calculada']) && $informe['kms_calculada'] != '---') { //hay informe de distancia
                    try {
                        $diffKms = abs($informe['km_total'] - $informe['kms_calculada']);
                        if (max($informe['km_total'], $informe['kms_calculada']) * 0.05 < $diffKms) {
                            $data['odometro'] = $data['odometro'] + round($informe['km_total']);
                        } else {
                            $data['odometro'] = $data['odometro'] + round($informe['kms_calculada']);
                        }
                        //die('/////<pre>' . nl2br(var_export($data['odometro'] . ' | ' . $informe['kms_calculada'], true)) . '</pre>////');
                    } catch (\Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                }
                // le sumamos el horometro que ya tenía  con el que posee el tablero para que quede igual
                if ($data['horas'] != null && isset($informe['segundos_motor_encendido'])) {
                    //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                    $minutos = ($data['horas'] * 60) + $data['minutos'] + round($informe['segundos_motor_encendido'] / 60);
                    $prog = $this->servicioManager->cambiarHorometro($servicio, $minutos);
                }
            }
            if ($this->servicioManager->cambiarOdometro($servicio, $data['odometro'])) {
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
                $this->bread->pop();
                $this->setFlash('success', sprintf('Se ha cambiado el odometro correctamente del servicio %s', strtoupper($servicio)));
                return $this->redirect($this->bread->getVolver());
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Cambio Odómetro");
        return $this->render('app/Servicio/cambioodometro.html.twig', array(
            'servicio' => $servicio,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/servicio/asignareventos", name="servicio_asignar_eventos")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function asignareventosAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $servicio = $this->servicioManager->findById($request->get('servicio'));
            $evento = $this->eventoManager->findById($request->get('evento'));
            $eventServicio = new ServicioEvent($servicio, $evento);
            if ($request->get('accion') == 1) {
                //es para vincular
                $servicio = $this->eventoManager->saveServicioEv($servicio, $evento);
                $this->eventDispatcher->dispatch($eventServicio, ServicioEvent::EVENTO_ENABLED);
            } else {
                //devincular
                $servicio = $this->eventoManager->removeServicioEv($servicio, $evento);
                $this->eventDispatcher->dispatch($eventServicio, ServicioEvent::EVENTO_DISABLED);
            }
            $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
            return new Response($evento->getId());
        }
    }

    /**
     * @Route("/servicio/asignareventosall", name="servicio_asignar_eventosall")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function asignareventosallAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $servicio = $this->servicioManager->findById($request->get('id'));
            $eventos = $this->eventoManager->findAll($servicio->getOrganizacion());
            if ($request->get('accion') == 1) { //es para vincular
                $repetidora = $this->eventoManager->saveServicioEvs($servicio, $eventos);
            } else { //devincular
                $repetidora = $this->eventoManager->removeServicioEvs($servicio, $eventos);
            }
            $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
            return new Response();
        }
    }

    /**
     * @Route("/servicio/batch", name="servicio_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_USUARIO_ASIGNAR") 
     */
    public function batchasignarusuarioAction(Request $request)
    {
        $organizacion = $this->organizacionManager->find($request->get('idorg'));
        if (!$organizacion) {
            throw $this->createNotFoundException('Código de organizacion no encontrado.');
        }
        $checks = $request->get('checks');
        if ($request->getMethod() == 'POST') {
            //aca entra por post con los datos para asociarlos al
            $this->bread->pop();

            $usuario = $this->usuarioManager->find($request->get('usuario'));
            $result = true;
            foreach ($checks as $key => $idServicio) {
                $result = $result && $this->servicioManager->asignarUsuario($idServicio, $usuario);
            }

            if ($result == true) {
                $this->setFlash('success', 'Se ha realizado la operacion de asignación');
            } else {
                $this->setFlash('success', 'Se ha realizado la operacion de asignación, pero con advertencias de asignación.');
            }

            return $this->redirect($this->bread->getVolver());
        } else {
            $this->bread->pushPorto($request->getRequestUri(), "Asignar Usuario");
            $checks = explode(' ', $request->get('checks'));
            $servicios = array();
            foreach ($checks as $id) {
                $servicios[] = array(
                    'id' => $id,
                    'nombre' => $this->servicioManager->find($id)->getNombre()
                );
            }
            return $this->render("app/Servicio/asignar_usuario_batch.html.twig", array(
                'usuarios' => $this->usuarioManager->findAll($organizacion, array('nombre' => 'ASC')),
                'organizacion' => $organizacion,
                'checks' => $checks,
                'servicios' => $servicios,
            ));
        }
    }

    /**
     * @Route("/servicio/refresh", name="servicio_refresh",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_VER") 
     */
    public function refreshAction(Request $request)
    {

        $id = $request->get('id');

        $servicio = $this->servicioManager->find($id);
        $referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        $datos = $this->getRegistro($servicio, $referencias);

        $data = array(
            'id_sc' => 'servicio_' . $servicio->getId(),
            'latitud' => $servicio->getUltLatitud(),
            'longitud' => $servicio->getUltLongitud(),
            'texto' => $servicio->getUltVelocidad() . ' km/h',
            'visible' => true,
            'new_icono' => $this->gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            'domicilio' => $datos['extras']['domicilio'],
            'cercania' => isset($datos['extras']['cercania']) ? $datos['extras']['cercania'] : null,
        );
        $html = $response = trim($this->renderView('app/Servicio/show_data.html.twig', array(
            'servicio' => $servicio,
            'datos' => $datos['datos'],
            'refresh' => false,
        )));

        return new Response(json_encode(array('data' => $data, 'html' => $html)));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/servicio/{id}/delete", name="servicio_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_ELIMINAR") 
     */
    public function deleteAction(Request $request, Servicio $servicio)
    {
        $form = $this->createDeleteForm($servicio->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $serv = new Servicio();
            $serv->setId($servicio->getId());
            $serv->setUpdatedAt($servicio->getUpdatedAt());
            $serv->setCreatedAt($servicio->getCreatedAt());
            //ahora elimino el servicio
            $this->servicioManager->delete($servicio);
            $notificar = $this->notificadorAgenteManager->notificar($serv, 2);

            $this->setFlash('success', 'Servicio eliminado del sistema.');
            $this->bread->pop();
        }
        return $this->redirect($this->bread->getVolver());
    }

    /**
     *
     * @Route("/servicio/filtrar", name="servicio_filtrar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN") 
     */
    public function filtrarAction(Request $request)
    {
        $tmp = array();
        $itinerarios = array();
        parse_str($request->get('formFlota'), $tmp);
        $form = $tmp['flota'];
        $user = $this->userlogin->getUser();
        $empresa = null;
        $transporte = null;
        $satelital = null;
        $tipoServicio = null;
        $claseServicio = null;
        $indices = array();
        $consulta = array(
            'tipoServicio' => -1,
            'claseServicio' => 0,
            'transporte' => 0,
            'satelital' => 0,
            'empresa' => 0,
            'estado' => -1
        );
        if ($form) {
            if ((isset($form['tipoServicio']) && $form['tipoServicio']) > -1) {
                //dd(SERVICIO::TIPOOBJETOS[$form['tipoServicio']]);
                $tipoServicio = $form['tipoServicio'] * 1;
                $strTipo = SERVICIO::TIPOOBJETOS[$form['tipoServicio']];
                $consulta['tipoServicio'] = $tipoServicio;
            } else {
                $consulta['tipoServicio'] = -1;
                $tipoServicio = null;
                $strTipo = 'Todos Tipos';
            }
            if (isset($form['claseServicio']) && ($form['claseServicio']) > 0) {
                $claseServicio = $this->tipoServicioManager->findById(intval($form['claseServicio']));
                $strClase = $claseServicio->getNombre();
                $consulta['claseServicio'] = $claseServicio->getId();
            } else {
                $claseServicio = null;
                $strClase = 'Toda Clase';
            }
            $estado = isset($form['estado']) ? $form['estado'] : null;
            $this->moduloCentroCosto = $this->userlogin->isGranted('ROLE_APMON_PRESTACION_ADMIN');
            $this->moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
            $this->moduloMantenimiento = $this->userlogin->isGranted('ROLE_APMON_MANT_ADMIN');

            if ($this->moduloMonitor) {
                if (($form['transporte']) > 0) {
                    $transporte = $this->transporteManager->find($form['transporte']);
                    $strTransporte = $transporte->getNombre();
                    $consulta['transporte'] = $transporte->getId();
                } else {
                    $transporte = null;
                    $strTransporte = 'Todos Transportes';
                }
                if (($form['satelital']) > 0) {
                    $satelital = $this->satelitalManager->find($form['satelital']);
                    $strSatelital = $satelital->getNombre();
                    $consulta['satelital'] = $satelital->getId();
                } else {
                    $satelital = null;
                    $strSatelital = 'Todos Satelitales';
                }

                if (intval($form['empresa']) > 0) {
                    $empresa = $this->empresaManager->find(intval($form['empresa']));
                    $strEmpresa = $empresa->getNombre();
                    $consulta['empresa'] = $empresa->getId();
                    //  die('////<pre>' . nl2br(var_export($consulta, true)) . '</pre>////');
                } else {
                    $empresa = null;
                    $strEmpresa = 'Todas Empresas';
                }
                $filtro = sprintf('<b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> ', $strClase, $strTipo, $strTransporte, $strSatelital, $strEmpresa);
            } else {
                $filtro = sprintf('<b>%s</b> | <b>%s</b> ', $strClase, $strTipo);
            }
            $servicios = $this->servicioManager->filtrar(
                $user->getOrganizacion(),
                $claseServicio,
                $tipoServicio,
                $estado,
                $transporte,
                $satelital,
                $empresa
            );  //parche
        }
        if ($this->moduloMantenimiento) {
            $indices = $this->getIndices($servicios);
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Servicio/list_content.html.twig', array(
                'servicios' => $servicios,
                'moduloMonitor' => $this->moduloMonitor,
                'moduloCentroCosto' => $this->moduloCentroCosto,
                'moduloMantenimiento' => $this->moduloMantenimiento,
                'indices' => $indices,
                'consulta' => $consulta
            )),
            'itinerarios' => json_encode($itinerarios),
            'filtro' => $filtro,
        )));
    }

    /**
     *
     * @Route("/servicio/exportar", name="servicio_exportar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function exportarAction(Request $request)
    {

        $consulta = json_decode($request->get('consulta'), true);
        //  die('////<pre>' . nl2br(var_export($consulta, true)) . '</pre>////');
        $organizacion = $this->userlogin->getOrganizacion();
        $moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
        $moduloMantenimiento = $this->userlogin->isGranted('ROLE_APMON_MANT_ADMIN');
        $moduloCentroCosto = $this->userlogin->isGranted('ROLE_APMON_PRESTACION_ADMIN');
        $transporte = null;
        $satelital = null;
        $empresa = null;
        $strEmpresa = null;
        $strSatelital = null;
        $strTransporte = null;
        if (!is_null($consulta)) {
            $tipoServicio = $consulta['tipoServicio'] == -1 ? null : $consulta['tipoServicio'];
            if ($consulta['tipoServicio'] == -1) {
                $strTipoServicio = 'Todos';
            } elseif ($consulta['tipoServicio'] == 0) {
                $strTipoServicio = 'Persona';
            } else {
                $strTipoServicio = 'Vehículo';
            }

            $claseServicio = $consulta['claseServicio'] == 0 ? null : $this->tipoServicioManager->findById(intval($consulta['claseServicio']));
            $strClaseServicio = $consulta['claseServicio'] == 0 ? 'Todos' : $claseServicio->getNombre();

            $xls = $this->excelManager->create("Informe de Flota");
            $xls->setHeaderInfo(array(
                'C2' => 'Tipo de Servicio',
                'D2' => $strTipoServicio,
                'C3' => 'Clase de Servicio',
                'D3' => $strClaseServicio,
            ));
            if ($moduloMonitor) {
                $transporte = $consulta['transporte'] == 0 ? null : $this->transporteManager->find($consulta['transporte']);
                $strTransporte = $consulta['transporte'] == 0 ? 'Todos' : $transporte->getNombre();

                $satelital = $consulta['satelital'] == 0 ? null : $this->satelitalManager->find($consulta['satelital']);
                $strSatelital = $consulta['satelital'] == 0 ? 'Todos' : $satelital->getNombre();

                $empresa = $consulta['empresa'] == 0 ? null : $this->empresaManager->find(intval($consulta['empresa']));
                $strEmpresa = $consulta['empresa'] == 0 ? 'Todos' : $empresa->getNombre();

                $xls->setHeaderInfo(array(
                    'C4' => 'Empresa',
                    'D4' => $strEmpresa,
                    'C5' => 'Transporte',
                    'D5' => $strTransporte,
                    'C6' => 'Satelital',
                    'D6' => $strSatelital,
                ));
            }
            $servicios = $this->servicioManager->filtrar(
                $organizacion,
                $claseServicio,
                $tipoServicio,
                $transporte,
                $satelital,
                $empresa
            );  //parche


            $xls->setBar(8, array(
                'A' => array('title' => 'Estado', 'width' => 20),
                'B' => array('title' => 'Nombre', 'width' => 20),
                'C' => array('title' => 'Equipo', 'width' => 20),
                'D' => array('title' => 'Ult. Transmisión', 'width' => 20),
            ));
            if ($moduloMantenimiento && $moduloMonitor) {
                $xls->setBar(8, array(
                    'E' => array('title' => 'Indice Manten. R/N', 'width' => 20),
                    'F' => array('title' => 'Indice Manten. R/P', 'width' => 20),
                    'G' => array('title' => 'Indice P.R. R/N', 'width' => 20),
                    'H' => array('title' => 'Indice P.R. R/P', 'width' => 20),
                    'I' => array('title' => '', 'width' => 20),
                    'J' => array('title' => 'Empresa', 'width' => 20),
                    'K' => array('title' => 'Transporte', 'width' => 20),
                    'L' => array('title' => 'Satelital', 'width' => 20),
                ));
                if ($moduloCentroCosto) {
                    $xls->setBar(8, array(
                        'M' => array('title' => 'Centro de Costo', 'width' => 20),
                    ));
                }
            } else {
                if ($moduloMantenimiento && !$moduloMonitor) {
                    $xls->setBar(8, array(
                        'E' => array('title' => 'Indice Manten. R/N', 'width' => 20),
                        'F' => array('title' => 'Indice Manten. R/P', 'width' => 20),
                        'G' => array('title' => 'Indice P.R. R/N', 'width' => 20),
                        'H' => array('title' => 'Indice P.R. R/P', 'width' => 20),
                    ));
                }
                if ($moduloMonitor && !$moduloMantenimiento) {
                    $xls->setBar(8, array(
                        'E' => array('title' => '', 'width' => 20),
                        'F' => array('title' => 'Empresa', 'width' => 20),
                        'G' => array('title' => 'Transporte', 'width' => 20),
                        'H' => array('title' => 'Satelital', 'width' => 20),
                    ));
                }

                if ($moduloCentroCosto) {
                    $xls->setBar(8, array(
                        'I' => array('title' => 'Centro de Costo', 'width' => 20),
                    ));
                }
            }
            $indices = array();
            if ($moduloMantenimiento) {
                $indices = $this->getIndices($servicios);
            }

            $i = 9;
            // die('////<pre>'.nl2br(var_export($informe, true)).'</pre>////');
            foreach ($servicios as $servicio) {   //esto es para cada servicio
                if ($servicio->getEstado() != 9) {
                    $empresas = array();
                    foreach ($servicio->getItinerarios() as $itinerario) {
                        if ($itinerario->getEmpresa() !== null) {
                            $empresas[$itinerario->getEmpresa()->getId()] = $itinerario->getEmpresa()->getNombre();
                        }
                    }
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $servicio->getStrestado()),
                        'B' => array('value' => $servicio->getNombre()),
                        'C' => array('value' => !is_null($servicio->getEquipo()) ? $servicio->getEquipo()->getMdmid() : '---'),
                        'D' => array('value' => !is_null($servicio->getUltFechaHora()) ? $servicio->getUltFechaHora()->format('d/m/Y H:i') : '---'),
                    ));
                    if ($moduloMantenimiento && $moduloMonitor) {
                        $xls->setBar(8, array(
                            'E' => array('value' => $indices[$servicio->getId()]['mantenimiento_rn']),
                            'F' => array('value' => $indices[$servicio->getId()]['mantenimiento_rp']),
                            'G' => array('value' => $indices[$servicio->getId()]['peticion_rn']),
                            'H' => array('value' => $indices[$servicio->getId()]['peticion_rp']),
                            'I' => array('value' => 'CUSTODIO'),
                            'J' => array('value' => implode(",", $empresas)),
                            'K' => array('value' => !is_null($servicio->getTransporte()) ? $servicio->getTransporte()->getNombre() : '---'),
                            'L' => array('value' => !is_null($servicio->getSatelital()) ? $servicio->getSatelital()->getNombre() : '---'),
                        ));
                        if ($moduloCentroCosto) {
                            $xls->setBar(8, array(
                                'M' => array('value' => !is_null($servicio->getCentrocosto()) ? $servicio->getCentrocosto()->getNombre() : '---'),
                            ));
                        }
                    } else {
                        if ($moduloMantenimiento && !$moduloMonitor) {
                            $xls->setRowValues($i, array(
                                'E' => array('value' => array_key_exists($servicio->getId(), $indices) ? $indices[$servicio->getId()]['mantenimiento_rn'] : 0),
                                'F' => array('value' => array_key_exists($servicio->getId(), $indices)  ? $indices[$servicio->getId()]['mantenimiento_rp'] : 0),
                                'G' => array('value' => array_key_exists($servicio->getId(), $indices)  ? $indices[$servicio->getId()]['peticion_rn'] : 0),
                                'H' => array('value' => array_key_exists($servicio->getId(), $indices) ? $indices[$servicio->getId()]['peticion_rp'] : 0)
                            ));
                        }
                        if ($moduloMonitor && !$moduloMantenimiento) {
                            if ($servicio->getIsCustodio()) {
                                $xls->setRowValues($i, array(
                                    'E' => array('value' => 'CUSTODIO')
                                ));
                            }
                            $xls->setRowValues($i, array(
                                'F' => array('value' => implode(",", $empresas)),
                                'G' => array('value' => !is_null($servicio->getTransporte()) ? $servicio->getTransporte()->getNombre() : '---'),
                                'H' => array('value' => !is_null($servicio->getSatelital()) ? $servicio->getSatelital()->getNombre() : '---'),
                            ));
                        }
                        if ($moduloCentroCosto) {
                            $xls->setRowValues($i, array(
                                'I' => array('value' => !is_null($servicio->getCentrocosto()) ? $servicio->getCentrocosto()->getNombre() : '---'),
                            ));
                        }
                    }
                    $i++;
                }
            }



            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=flota.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('organizacion_list', array('id' => $organizacion->getId(), 'tab' => 'main')));
        }
    }

    private function obtenerValores($servicio)
    {
        if ($servicio->getCentrocosto()) {   //entro si tengo centro de costo asignado
            $horasUso = $this->prestacionManager->contarTiempoUso($servicio, $servicio->getFechaUltCambio(), new \Datetime());
            $total = $servicio->getUltHorometro() + $horasUso;
            // die('////<pre>' . nl2br(var_export($servicio->getNombre(), true)) . '</pre>////');
            return array('total' => intval($total), 'diff' => $horasUso);
        } else {
            $now = new \DateTime();  //ahora.
            $diff = date_diff($now, $servicio->getFechaUltCambio(), true);
            $diff2 = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i;
            $total = $servicio->getUltHorometro() + $diff2;
            return array('total' => intval($total), 'diff' => $diff2);
        }
        //$total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;
    }

    private function formatHorometro($horometro)
    {
        $horas = floor($horometro / 60);
        $minutos2 = $horometro % 60;
        //aca solo formateo el 0 antes del nro.
        if ($horas < 10)
            $horas = '0' . $horas;
        if ($minutos2 < 10)
            $minutos2 = '0' . $minutos2;

        return sprintf('%s:%s', $horas, $minutos2);
    }

    /**
     * @Route("/servicio/addvalores", name="servicio_addvalores")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN") 
     */
    public function addvaloresAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formValores'), $tmp);
        $id = $request->get("id");
        $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($tmp['valores']['fecha'], false), new \DateTimeZone($this->userlogin->getTimezone()));
        //die('////<pre>' . nl2br(var_export($tmp['valores']['horometro_hora'], true)) . '</pre>////');
        $odometro = intval($tmp['valores']['odometro']) * 1000;
        $horometro_hora = intval($tmp['valores']['horometro_hora']);
        $horometro_minuto = intval($tmp['valores']['horometro_minuto']);
        $horometro = ($horometro_hora * 60) + $horometro_minuto;
        $servicio = $this->servicioManager->updateValores($id, $horometro, $fecha, $odometro);
        $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
        return new Response(json_encode(array(
            'fecha' => $this->formatUltRegistro($servicio),
            'odometro' => is_null($servicio->getUltOdometro()) ? 0 : number_format(($servicio->getUltOdometro() / 1000), 2, ',', '.'),
            'horometro' => $servicio->formatHorometro(),
            'id' => $servicio->getId(),
        )));
    }

    /**
     * @Route("/servicio/actvalores", name="servicio_actvalores")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN") 
     */
    public function actvaloresAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formValores'), $tmp);
        $id = $request->get("id");

        $servicio = $this->servicioManager->find(intval($id));
        $now = new \DateTime();  //ahora.
        $horometro = $servicio->getUltHorometro() + $this->obtenerValores($servicio, $this->prestacionManager);
        $servicio = $this->servicioManager->updateValores($id, $horometro, $now, null);
        $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);

        return new Response(json_encode(array(
            'fecha' => is_null($servicio->getFechaUltCambio()) ? '---' : $servicio->getFechaUltCambio()->format('d-m-Y H:i'),
            'odometro' => is_null($servicio->getUltOdometro()) ? 0 : $servicio->getUltOdometro() / 1000,
            'horometro' => $servicio->formatHorometro(),
            'mins' => $this->formatHorometro($this->obtenerValores($servicio, $this->prestacionManager)),
            'id' => $servicio->getId(),
        )));
    }

    /**
     * @Route("/servicio/{id}/asignarchofer", name="servicio_asignar_chofer",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function asignarchoferction(Request $request, Servicio $servicio)
    {
        if (!$servicio) {
            throw $this->createNotFoundException('Código de servicio no encontrado.');
        }
        $options['choferes'] = $this->choferManager->findAll($servicio->getOrganizacion());
        $form = $this->createForm(ServicioChoferType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();
            $chofer = $this->choferManager->find(intval($request->get('servicio_chofer')['chofer']));
            if ($chofer != null) {
                $chofer->setServicio($servicio);
                //die('////<pre>' . nl2br(var_export($chofer->getId(), true)) . '</pre>////');
                if ($this->choferManager->save($chofer)) {
                    $ejecutor = $this->userlogin->getUser();
                    $bitacora = $this->bitacoraGralManager->choferAsignarServicio($ejecutor, $chofer, $servicio);
                    $this->setFlash('success', sprintf('El chofer se ha asignado con éxito'));

                    return $this->redirect($this->bread->getVolver());
                }
            }
        }
        $this->bread->pushPorto($request->getRequestUri(), "Asignar Servicio");
        return $this->render('app/Servicio/asignar_chofer.html.twig', array(
            'servicio' => $servicio,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/servicio/{id}/exportqr", name="servicio_exportqr",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     */
    public function exportqrAction(Request $request, Servicio $servicio, Pdf $snappy)
    {
        //      die('////<pre>' . nl2br(var_export('$html', true)) . '</pre>////');
        $filename = sprintf('servicio%d.pdf', $servicio->getId());
        $html = $this->renderView('app/Servicio/exportqr.html.twig', array(
            'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : null,
            'json_servicio' => json_encode(array('id' => $servicio->getId(), 'patente' => $servicio->getVehiculo() ? $servicio->getVehiculo()->getPatente() : null)),
        ));
        //return new Response($html);
        return new Response(
            $snappy->getOutputFromHtml($html, array(
                //'orientation' => 'Landscape',
                'default-header' => false
            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $filename,
                'charset' => 'UTF-8',
            )
        );
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
