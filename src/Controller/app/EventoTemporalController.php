<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Form\app\EventoNewType;
use App\Form\app\EventoEditType;
use App\Entity\EventoParametro;
use App\Entity\EventoNotificacion;
use App\Entity\EventoTemporal as EventoTemporal;
use App\Entity\Evento as Evento;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Organizacion as Organizacion;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\EventoManager;
use App\Model\app\EventoTemporalManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ContactoManager;
use App\Model\app\ServicioManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\Router\EventoTemporalRouter;
use App\Model\monitor\EventoItinerarioManager;

class EventoTemporalController extends AbstractController
{

    private $userloginManager;
    private $eventoManager;
    private $breadcrumbManager;
    private $organizacionManager;
    private $tipoEventoManager;
    private $servicioManager;
    private $contactoManager;
    private $grupoReferenciaManager;
    private $eventoHistorialManager;
    private $eventoTemporalManager;
    private $notificadorAgenteManager;
    private $evTemporalRouter;
    protected $eventoItinerarioManager;

    function __construct(
        UserLoginManager $userloginManager,
        TipoEventoManager $tipoEventoManager,
        EventoHistorialManager $eventoHistorialManager,
        EventoTemporalManager $eventoTemporalManager,
        EventoManager $eventoManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        ServicioManager $servicioManager,
        ContactoManager $contactoManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoTemporalRouter $evTemporalRouter,
        EventoItinerarioManager $eventoItinerarioManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->eventoManager = $eventoManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->tipoEventoManager = $tipoEventoManager;
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->contactoManager = $contactoManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoHistorialManager = $eventoHistorialManager;
        $this->eventoTemporalManager = $eventoTemporalManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->evTemporalRouter = $evTemporalRouter;
        $this->eventoItinerarioManager = $eventoItinerarioManager;
    }

    /**
     * Lista todos los eventos
     * @Route("/eventotemporal/list", name="app_eventotemporal_list"), methods={"GET"}     
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER")
     */
    public function listAction(Request $request)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Eventos");

        $data = $this->armarData(
            $this->eventoTemporalManager->findByUser(),
            $this->userloginManager->getUltEventoVisto()
        );

        return $this->render('app/EventoTemporal/list.html.twig', array(
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'menu' => $this->getListMenu(),
            'data' => $data,
            'ultimoIdVisto' => $this->userloginManager->getUltEventoVisto(),
        ));
    }

    private $hayNuevos = false;

    private function armarData($eventos, $lastIdRead)
    {
        $data = [];
        $maxIdRead = $lastIdRead;
        foreach ($eventos as $evento) {
            $data[$evento->getId()] = [
                'evento' => $evento,
                'nuevo' => $evento->getId() > $lastIdRead,
            ];
            if (!$this->hayNuevos && $evento->getId() > $lastIdRead) {
                $this->hayNuevos = $evento->getId() > $lastIdRead;   //es para saber si tengo q generar sonido sobre los nuevos
                $maxIdRead = $evento->getId();
            }
        }
        if ($this->hayNuevos) {
            $this->userloginManager->setUltEventoVisto($maxIdRead); //seteo el ultimo
        }
        return $data;
    }

    /**
     * Lista todos los eventos
     * @Route("/eventotemporal/reload", name="app_eventotemporal_reload"), methods={"GET"}     
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER")
     */
    public function reloadAction(Request $request)
    {
        $lastIdRead = $this->userloginManager->getUltEventoVisto();

        $eventos = $this->eventoTemporalManager->findByUser(-1, array('t.id' => 'DESC'));
        $data = $this->armarData($eventos, $lastIdRead);
        $tabla = $this->renderView('app/EventoTemporal/tablaTemporal.html.twig', array(
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'data' => $data,
            'ultimoIdVisto' => $lastIdRead,
        ));

        //dd($tabla);
        return new Response(json_encode(array(
            'tabla' => $tabla,
            'playsound' => $this->hayNuevos
        )), 200);
    }

    /**
     * registra como visto el evento.
     * @Route("/eventotemporal/grabarvisto", name="app_eventotemporal_grabarvisto"), methods={"POST"}
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER")
     */
    public function grabarvistoAction(Request $request)
    {
        $idEventoTmp = intval($request->get('id'));  //es el id del eventoTemporal.        
        if (!is_null($idEventoTmp) && $idEventoTmp > 0) {
            $eventoTmp = $this->eventoTemporalManager->findById($idEventoTmp);   //obtengo el evento temporal

        }

        return new Response(json_encode([]), 200);
    }

    /**
     * @Route("/eventotemporal/comentar", name="app_eventotemporal_comentar"), methods={"POST"}
     */
    public function registrarAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formComentario'), $tmp);
        //dd($request->get('id'));
        //dd($tmp);   
        $this->hacerComentario(intval($request->get('id')), $tmp['formComentario']['comentario']);

        return new Response(json_encode([]), 200);
    }

    private function hacerVisto($id)
    {
        $eventoTmp = $this->eventoTemporalManager->findById($id);   //obtengo el evento temporal
        if (!is_null($eventoTmp)) {
            if ($eventoTmp->getClase() == 3) {    //evento itinerario
                $evHistorial = $eventoTmp->getEventoHistorial();  //rescato el evento historial

                //grabo el visto en el itinerario                    
                $this->eventoItinerarioManager->setupVisto(
                    $eventoTmp->getEvento()->getHistoricoItinerario(),
                    $this->userloginManager->getUser(),
                    $eventoTmp->getEventoHistorial()
                );
                $this->eventoTemporalManager->deleteAll($evHistorial); //borro las otras notific del evento temporal
            } else {   //evento normal  testing

            }
            //aca se marca como borrado el eventoTemporal
            $this->eventoHistorialManager->setupVistoTemporal($eventoTmp, $this->userloginManager->getUser());
        }
    }

    private function hacerComentario($id, $comentario)
    {
        $eventoTmp = $this->eventoTemporalManager->findById($id);   //obtengo el evento temporal
        if (!is_null($eventoTmp)) {
            if ($eventoTmp->getClase() == 3) {    //evento itinerario
                $evHistItinerario = $eventoTmp->getHistoricoItinerario();
                $this->eventoItinerarioManager->registrar($evHistItinerario, $comentario);
            }

            $evHistorial = $eventoTmp->getEventoHistorial();  //rescato el evento historial

            //grabo la respuesta en el evento regular            
            $this->eventoHistorialManager->registrar($evHistorial, $comentario);

            //borro las otras notific del evento temporal            
            $this->eventoTemporalManager->deleteAll($evHistorial);
        }
        return true;
    }

    /**
     * @Route("/eventotemporal/obtener", name="app_eventotemporal_obtener"), methods={"GET"}
     */
    public function obtenerAction(Request $request)
    {
        $data = [];
        $eventoTmp = $this->eventoTemporalManager->findById(intval($request->get('id')));   //obtengo el evento temporal
        if ($eventoTmp && !is_null($eventoTmp->getData())) {
            $data = $eventoTmp->getData();
            $data = array(
                'evento' => $data['cuerpo']
            );
        }
        return new Response(json_encode($data), 200);
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu()
    {
        $menu[1] = array(
            $this->evTemporalRouter->btnRefresh(),
            $this->evTemporalRouter->btnPauseRefresh()
        );
        //$menu[2] = array($this->evTemporalRouter->btnCheckAll());
        $menu[2] = array($this->evTemporalRouter->btnVisto());

        return $menu;
    }

    /**
     * 
     * @Route("/eventotemporal/batch", name="app_eventotemporal_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_REGISTRAR")
     */
    public function batchAction(Request $request)
    {
        $checks = explode(' ', $request->get('checks'));
        $accion = $request->get('accion');
        if ($checks && $accion) {
            $result = true;
            // dd($checks);
            foreach ($checks as $key => $value) {
                if ($key == 0) continue;  //salta el primero
                if ($accion == 'visto') {
                    $evento = $this->eventoTemporalManager->findById(intval($value));
                    if ($evento) {
                        $this->hacerVisto($evento);
                    }
                }
            }
            if ($result == true) {
                $this->setFlash('success', sprintf('Se ha realizado la operación de <b>%s</b> para los eventos seleccionado', $accion));
                return new Response(json_encode($checks));
            } else {
                $this->setFlash('error', 'Se ha producido un error en la operación');
                return null;
            }
        }
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
