<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\BackendManager;
use App\Model\app\GeocoderManager;

class MapaController extends AbstractController
{

    private $userloginManager;
    private $utilsManager;
    private $servicioManager;
    private $geocoderManager;
    private $referenciaManager;
    private $backendManager;

    function __construct(
        UserLoginManager $userloginManager,
        UtilsManager $utilsManager,
        ReferenciaManager $referenciaManager,
        ServicioManager $servicioManager,
        GeocoderManager $geocoderManager,
        BackendManager $backendManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->utilsManager = $utilsManager;
        $this->servicioManager = $servicioManager;
        $this->geocoderManager = $geocoderManager;
        $this->referenciaManager = $referenciaManager;
        $this->backendManager = $backendManager;
    }

    /**
     * @Route("/mapa/servicio/info", name="main_mapa_servicio_infoAJAX")
     * @Method({"GET", "POST"})
     */
    public function servicioinfoAJAXAction(Request $request)
    {

        $id = explode('_', $request->get('idServ'));
        $servicio = $this->servicioManager->find($id[1]);

        $direc = $this->geocoderManager->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());

        $info = '<h2>' . $servicio->getNombre() . '</h2>';

        //se agrega la patente
       
        if ($servicio->getVehiculo()) {
            $info .= sprintf('<p><label class="label label-info label-xs">%s</label></p>', $servicio->getVehiculo()->getPatente());
        }
       
        if (!is_null($servicio->getGrupos())) {
            $info .= 'Grupos: ';
            foreach ($servicio->getGrupos() as $grp) {
                $info .= sprintf('<label class="label label-default label-xs">%s</label> ', $grp->getNombre());
            }
        }
        if (!is_null($servicio->getCentrocosto())) {
            $info .= sprintf('<p>Centro de Costo: <b>%s</b></p> ', $servicio->getCentrocosto()->getNombre());
        }

        $info .= '<h4>Última transmisión</h4>';
        $info .= '<p>Fecha y hora: <b>' . $this->utilsManager->fechaUTC2local($servicio->getUltFechahora(), 'd/m/Y H:i:s') . '</b></br>';
        //$info .= 'Fecha y hora: <b>' . date_format($servicio->getUltFechahora(), 'd/m/Y H:i:s') . '</b></br>';
        $info .= 'Velocidad: <b>' . $servicio->getUltVelocidad() . ' km/h</b></br>';
        $info .= 'Rumbo: <b>' . $servicio->getUltDireccion() . '° ( ' . $servicio->getStrUltDireccion() . ' ) </b></br>';
        $info .= 'Bateria: <b>' . $servicio->getUltBateria() . ' %</b></br>';
        $info .= 'Dirección: <b>' . $direc . '</b></br>';
        $info .= 'Lat/Lon: <b>' . $servicio->getUltLatitud() . ', ' . $servicio->getUltlongitud() . '</b></br></p>';
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_AGREGAR')) {
            $info .= '<a href="#" onclick="crearReferenciaStep1(' . $servicio->getUltLatitud() . ', ' . $servicio->getUltlongitud() . ');" class="btn btn-default"><i class="icon-screenshot"></i> Crear referencia</a>  ';
        }
        if ($this->userloginManager->isGranted('ROLE_MAPA_HISTORIAL')) {
            $info .= '<a href="' . $this->generateUrl('apmon_historial') . '?id=' . $servicio->getId() . '" target="_blank" class="btn btn-default"> Histórico</a> ';
        }
        if ($this->userloginManager->isGranted('ROLE_MAPA_ULTIMAS_POSICIONES')) {
            $info .= '<a href="' . $this->generateUrl('apmon_posiciones') . '?id=' . $servicio->getId() . '" target="_blank" class="btn btn-default"> Últimas Posiciones</a> ';
        }
        return new Response($info);
    }

    private function row($ley, $dat)
    {
        return '<tr><td>' . $ley . '</td><td>' . $dat . '</td></tr>';
    }

    /**
     * home del cliente
     * @Route("/mapa/servicio/backendinfo", name="main_mapa_backend_infoAJAX")
     * @Method({"GET", "POST"})
     */
    public function serviciobackendinfoAJAXAction(Request $request)
    {

        $consulta = $this->get('session')->get('consulta');
        $servicio = $this->servicioManager->find(intval($consulta['servicio']));
        $oid = $request->get('oid');
        $trama = $this->backendManager->trama($servicio->getId(), $oid);
        $info = '';
        $info .= '<table class="table table-condensed">';
        $info .= '<thead><tr><th colspan="2">Datos Adicionales</th></tr></thead>';
        $info .= '<tbody>';
        if (isset($trama['posicion'])) {
            //obtengo la dirección.
            $direc = $this->geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
            $info .= $this->row('Domicilio', $direc);


            //hago la cercania.
            $referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
            if ($referencias) {
                $cercanas = $this->servicioManager->buscarCercaniaPosicion($trama['posicion']['latitud'], $trama['posicion']['longitud'], $referencias, 1);
                if (is_null($cercanas[0])) {
                    $info .= $this->row('Cercania', '---');
                } else {
                    $info .= $this->row('Cercania', $cercanas[0]['leyenda']);
                }
            }
            //datos de la posicion.
            $info .= $this->row('Lat/Lon', $trama['posicion']['latitud'] . ', ' . $trama['posicion']['longitud']);
            $info .= $this->row('Rumbo', $trama['posicion']['direccion'] . '°');
            $info .= $this->row('Velocidad', $trama['posicion']['velocidad'] . ' kms/h');
        }
        if (isset($trama['bateria_externa']) && $trama['bateria_externa'] != -1)
            $info .= $this->row('Bat. Externa', $trama['bateria_externa'] . '%');
        if (isset($trama['bateria_externa_volts']) && $trama['bateria_externa_volts'] != -1)
            $info .= $this->row('Volt Bat. Externa', $trama['bateria_externa_volts'] . 'v');
        if (isset($trama['bateria_interna']) && $trama['bateria_interna'] != -1)
            $info .= $this->row('Bat. Int.', $trama['bateria_interna'] . '%');
        if (isset($trama['contacto']))
            $info .= $this->row('Contacto', ($trama['contacto'] ? 'SI' : 'NO'));
        if (isset($trama['ibutton']))
            $info .= $this->row('IButton', $trama['ibutton']);

        if (isset($trama['panico']))
            $info .= $this->row('Panico', ($trama['panico'] ? 'SI' : 'NO'));

        //se recorren todos los inputs.
        foreach ($servicio->getInputs() as $input) {
            $options[] = array($input->getModeloSensor()->getCampoTrama() => true);
            $info .= $this->row($input->getNombre(), ($trama[$input->getModeloSensor()->getCampoTrama()] ? 'SI' : 'NO'));
        }

        if (isset($trama['odometro']))
            $info .= $this->row('Odómetro', round((intval($trama['odometro'])) / 1000, 2) . ' kms');
        if (isset($trama['horometro']))
            $info .= $this->row('Horómetro', $this->utilsManager->minutos2tiempo($trama['horometro']));
        if (isset($trama['motivo']))
            $info .= $this->row('Motivo Tx', $this->servicioManager->getMotivoTx2Str($trama));

        if (isset($trama['carcontrol'])) {
            $carcontrol = $trama['carcontrol'];
            //aca van los datos del carfinder.
            if (isset($carcontrol['rpm_motor']))
                $info .= $this->row('RPM Motor', $carcontrol['rpm_motor'] . ' rpm');
            if (isset($carcontrol['porcentaje_tanque'])) {
                $info .= $this->row('Tanque', $carcontrol['porcentaje_tanque'] . '% ');
            }
            if (isset($carcontrol['temperatura_motor']))
                $info .= $this->row('Temp. Motor', $carcontrol['temperatura_motor'] . ' °C');
            if (isset($carcontrol['sensor_presion_aceite']))
                $info .= $this->row('Sensor Presión Aceite', ($carcontrol['sensor_presion_aceite'] ? 'SI' : 'NO'));
            if (isset($carcontrol['presion_aceite']))
                $info .= $this->row('Presión Aceite', $carcontrol['presion_aceite'] . '%');
            if (isset($carcontrol['cc_entrada_1']))
                $info .= $this->row('Sensor 1', $carcontrol['cc_entrada_1']);
            if (isset($carcontrol['cc_entrada_2']))
                $info .= $this->row('Sensor 2', $carcontrol['cc_entrada_2']);
            if (isset($carcontrol['cc_entrada_3']))
                $info .= $this->row('Sensor 3', $carcontrol['cc_entrada_3']);
        }

        if (isset($trama['sensores'])) {   //esto es para los seriados.
            // die('////<pre>' . nl2br(var_export($trama['sensores'], true)) . '</pre>////');
            foreach ($trama['sensores'] as $sens) {
                if (isset($sens['id_sensor']) && isset($sens['medicion_sensor'])) {
                    $tramaSensor[$sens['id_sensor']] = $sens['medicion_sensor'];
                }
            }
            foreach ($servicio->getSensores() as $sensor) {
                $modelo = $sensor->getModeloSensor();
                $valorSensor = '---';
                if ($modelo->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                    if (isset($tramaSensor[$sensor->getSerie()])) {
                        $valorSensor = $tramaSensor[$sensor->getSerie()];
                    }
                } else {  //no es seriado
                    $campo = $sensor->getModeloSensor()->getCampoTrama();
                    if (isset($trama[$campo])) {
                        $valorSensor = $trama[$campo];
                    }
                }
                if (!is_null($valorSensor)) {
                    $info .= $this->row($sensor->getNombre(), sprintf("%s %s", $valorSensor, $sensor->getMedida()));
                }
            }
        }

        if (isset($trama['panico']))
            $info .= $this->row('Panico', ($trama['panico'] ? 'SI' : 'NO'));

        // die('////<pre>' . nl2br(var_export($info, true)) . '</pre>////');
        if (count($servicio->getInputs()) === 0) {  //tiene nada de entradas configuradas.
            //muestro
            if (isset($trama['entrada_1']))
                $info .= $this->row('Entrada 1', ($trama['entrada_1'] ? 'SI' : 'NO'));
            if (isset($trama['entrada_2']))
                $info .= $this->row('Entrada 2', ($trama['entrada_2'] ? 'SI' : 'NO'));
            if (isset($trama['entrada_3']))
                $info .= $this->row('Entrada 3', ($trama['entrada_3'] ? 'SI' : 'NO'));
        }

        if (isset($trama['canbusInicioTrayecto'])) {
            $info .= '<tr><th colspan="2">CanBus inicio viaje</th></tr>';
            $canbus = $trama['canbusInicioTrayecto'];
            if (isset($canbus['odolitro'])) { //todo esto es para el viaje
                $info .= $this->row('Odolito', $canbus['odolitro'] . ' lts');
            }
            if (isset($canbus['odometro'])) { //todo esto es para el viaje
                $info .= $this->row('Odómetro', $canbus['odometro'] . ' kms');
            }
            //die('////<pre>' . nl2br(var_export($trama, true)) . '</pre>////');
        }
        if (isset($trama['canbusFinTrayecto'])) {
            $info .= '<tr><th colspan="2">CanBus fin viaje</th></tr>';
            $canbus = $trama['canbusFinTrayecto'];
            if (isset($canbus['duracionTray'])) { //todo esto es para el viaje
                $info .= $this->row('Viaje', sprintf(
                    "%s | %s kms | %s lts | V.Max: %s km/h | Max. RPM: %s",
                    $this->utilsManager->segundos2tiempo($canbus['duracionTray']),
                    $canbus['distanciaTray'],
                    $canbus['consumoTray'],
                    $canbus['velMaxTray'],
                    $canbus['maxRPMTray']
                ));
            }

            if (isset($canbus['tiempoMotorMarcha'])) {
                $info .= $this->row('Marcha', sprintf("%s | %s lts", $this->utilsManager->segundos2tiempo($canbus['tiempoMotorMarcha']), $canbus['consumoMarcha']));
            }
            if (isset($canbus['tiempoRalenti'])) {
                $info .= $this->row('Ralenti', sprintf("%s | %s lts", $this->utilsManager->segundos2tiempo($canbus['tiempoRalenti']), $canbus['consumoRalenti']));
            }
            if (isset($canbus['tiempoCrucero'])) {
                $info .= $this->row('Crucero', sprintf("%s | %s kms | %s lts", $this->utilsManager->segundos2tiempo($canbus['tiempoCrucero']), $canbus['distanciaCrucero'], $canbus['consumoCrucero']));
            }
        }
        if (isset($trama['canbusData'])) {
            $info .= '<tr><th colspan="2">CanBus en viaje</th></tr>';
            $canbus = $trama['canbusData'];
            //aca van los datos del carfinder.
            if (isset($canbus['velMotorRPM']))
                $info .= $this->row('RPM Motor', $canbus['velMotorRPM'] . ' rpm');
            if (isset($canbus['odolitro']))
                $info .= $this->row('Odolitro', $canbus['odolitro'] . ' lts');
            if (isset($canbus['odometro']))
                $info .= $this->row('Odómetro', $canbus['odometro'] . ' kms');
            if (isset($canbus['velInstantanea']))
                $info .= $this->row('Veloc. Inst.', $canbus['velInstantanea'] . ' kms/hr');
            if (isset($canbus['presionAceite']))
                $info .= $this->row('Presión Aceite', $canbus['presionAceite'] . ' kPa');
            if (isset($canbus['nivelCombustible'])) {
                $porc = $this->servicioManager->getPorcentajeTanque($servicio, intval($canbus['nivelCombustible']));
                $lts = $this->servicioManager->getLtrTanque($servicio, intval($canbus['nivelCombustible']));
                $info .= $this->row('Nivel Tanque', sprintf('%d%% (%d lts)', $porc, $lts));
            }
            if (isset($canbus['tempMotor']))
                $info .= $this->row('Temp. Motor', $canbus['tempMotor'] . ' °C');
        }

        $info .= '</tbody></table>';
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_AGREGAR')) {
            $info .= '<p><a href="#" onclick="crearReferencia()"><i class="icon-screenshot"></i> Crear referencia en este punto</a></p>';
        }
        return new Response($info);
    }
}
