<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Form\app\VehiculoDatoExtraType;
use App\Entity\VehiculoExtra;
use App\Entity\Servicio;

class VehiculoExtraController extends AbstractController
{

    private $breadcrumbManager;
    private $servicioManager;

    function __construct(BreadcrumbManager $breadcrumbManager, ServicioManager $servicioManager)
    {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/app/vehiculoextra/{id}/new", name="main_vehiculoextra_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function newAction(Request $request, Servicio $servicio)
    {

        if (!$servicio) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $datoExtra = new VehiculoExtra();
        $datoExtra->setVehiculo($servicio->getVehiculo());
        $form = $this->createForm(VehiculoDatoExtraType::class, $datoExtra);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->get('vehiculoextra');

            $em = $this->getEntityManager();
            $tipoDato = $em->getRepository('App:TipoDatoExtra')->findOneBy(array('id' => $data['tipoDatoExtra']));
            //die('////<pre>'.nl2br(var_export($data, true)).'</pre>////');
            $datoExtra->setTipoDatoExtra($tipoDato);
            $datoExtra->setDato($data['dato']);
            $datoExtra->setNota($data['nota']);
            $this->servicioManager->saveDatoExtra($datoExtra);
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Dato Extra");
        return $this->render('app/VehiculoExtra/new.html.twig', array(
            //'menu' => $this->getShowMenu($servicio),
            'servicio' => $servicio,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/app/vehiculoextra/{id}/edit", name="main_vehiculoextra_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function editAction(Request $request, VehiculoExtra $datoExtra)
    {
        $em = $this->getEntityManager();

        if (!$datoExtra) {
            throw $this->createNotFoundException('Código de dato no encontrado.');
        }

        $form = $this->createForm(VehiculoDatoExtraType::class, $datoExtra);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->get('vehiculoextra');
            $em = $this->getEntityManager();
            $tipoDato = $em->getRepository('App:TipoDatoExtra')->findOneBy(array('id' => $data['tipoDatoExtra']));
            //die('////<pre>'.nl2br(var_export($data, true)).'</pre>////');
            $datoExtra->setTipoDatoExtra($tipoDato);
            $datoExtra->setDato($data['dato']);
            $datoExtra->setNota($data['nota']);
            $this->servicioManager->saveDatoExtra($datoExtra);
            return $this->breadcrumbManager->getVolver();
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Dato Extra");
        return $this->render('app/VehiculoExtra/edit.html.twig', array(
            //'menu' => $this->getShowMenu($servicio),
            'dato' => $datoExtra,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/app/vehiculoextra/{id}/show", name="main_vehiculoextra_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function showAction(Request $request, VehiculoExtra $dato)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Ver Dato Extra");
        return $this->render('app/VehiculoExtra/show.html.twig', array(
            'dato' => $dato,
            'menu' => $this->getShowMenu($dato),
            'delete_form' => $this->createDeleteForm($dato->getId())->createView(),
        ));
    }

    /**
     * @Route("/app/vehiculoextra/{id}/delete", name="main_vehiculoextra_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_EDITAR") 
     */
    public function deleteAction($id)
    {
        $em = $this->getEntityManager();
        $dato = $em->getRepository('App:VehiculoExtra')->findOneBy(array('id' => $id));
        if (!$dato) {
            throw $this->createNotFoundException('El dato extra no se encuentra.');
        } else {
            $delete = $this->servicioManager->deleteDatoExtra($dato);
            $this->setFlash('success', sprintf('El dato extra se eliminó correctamente'));
            $this->breadcrumbManager->pop();
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    private function getShowMenu($dato)
    {
        $menu = array();
        $menu['Editar'] = array(
            'imagen' => 'icon-editar icon-blue',
            'url' => $this->generateUrl('main_vehiculoextra_edit', array('id' => $dato->getId()))
        );
        $menu['Eliminar'] = array(
            'imagen' => 'icon-minus icon-blue',
            'url' => '#modalDelete',
            'extra' => 'data-toggle=modal',
        );


        return $menu;
    }
}
