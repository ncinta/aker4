<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\app\EventoRegistroType;
use App\Form\app\EventoHistorialType;
use App\Entity\Organizacion;
use App\Entity\EventoHistorial;
use App\Entity\EventoTemporal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\EventoTemporalManager;
use App\Model\app\EventoManager;
use App\Model\app\UtilsManager;
use App\Model\app\Router\EventoHistoricoRouter;

class EventoHistoricoController extends AbstractController
{

    private $eventoHistoricoRouter;
    function __construct(
        EventoHistoricoRouter $eventoHistoricoRouter,
    ) {
        $this->eventoHistoricoRouter = $eventoHistoricoRouter;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * Lista todos los eventos
     * @Route("/eventohistorico/{id}/list", name="app_eventohistorico_list",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        EventoManager $eventoManager,
        EventoHistorialManager $eventoHistorialManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Historico de Eventos");
        $fecha_desde = date('d/m/Y', time());
        $fecha_hasta = date('d/m/Y', time());
        $evento = 0;
        $servicio = 0;
        $ok = true;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('historial');
            $ok = $utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
            }
            $evento = $form['evento'];
            $servicio = $form['servicio'];
        }
        if ($ok) {
            $eventos = $eventoHistorialManager->listAll(
                $organizacion,
                $utilsManager->datetime2sqltimestamp($fecha_desde . ' 00:00:00'),
                $utilsManager->datetime2sqltimestamp($fecha_hasta . ' 23:59:59'),
                $evento,
                $servicio
            );
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
            $eventos = null;
        }
        $options['eventos'] = $eventoManager->findAll($organizacion, array('nombre' => 'ASC'));
        $options['servicios'] = $servicioManager->findAllByOrganizacion($organizacion);
        $options['servicio_select'] = $servicio;
        $options['evento_select'] = $evento;
        $form = $this->createForm(EventoHistorialType::class, null, $options);

        return $this->render('app/EventoHistorico/list.html.twig', array(
            'organizacion' => $organizacion,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'menu' => $this->getListMenu($userloginManager),
            'eventos' => $eventos,
            'form' => $form->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu($userlogin)
    {
        $menu = array();
        if ($userlogin->isGranted('ROLE_EVENTO_REGISTRAR')) {
            $menu['Marcar como Vistos'] = array(
                'imagen' => '',
                'extra' => 'onclick=batch("visto") id=btnVisto style=display:none',
                'url' => '#visto',
            );
        }
        return $menu;
    }


    /**
     * 
     * @Route("/eventohistorico/{id}/registrar", name="app_eventohistorico_registrar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_REGISTRAR")
     */
    public function registrarAction(
        Request $request,
        EventoHistorial $evento,
        EventoHistorialManager $eventoHistManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), 'Registrar Evento');

        $form = $this->createForm(EventoRegistroType::class, $evento);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $eventoHistManager->registrar($evento);
            $this->setFlash('success', 'Se ha registrado la respuesta del evento. Gracias.');
            return $this->redirect($breadcrumbManager->getVolver());
        }

        if (is_array($evento->getData())) {
            $data = $evento->getData();
        } else {
            $data = json_decode($evento->getData());
        }
        return $this->render('dist/EventoHistorico/registrar.html.twig', array(
            'evento' => $evento,
            'data' => $data,
            'form' => $form->createView(),
        ));
    }

    /**
     * 
     * @Route("/eventohistorico/{id}/show", name="app_eventohistorico_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_HISTORICO_VER")
     */
    public function showAction(
        Request $request,
        EventoHistorial $evento,
        EventoHistorialManager $eventoHistorialManager,
        EventoTemporalManager $eventoTemporalManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager
    ) {
        $eventoHistorialManager->setupVisto($evento, $userloginManager->getUser());
        $breadcrumbManager->pushPorto($request->getRequestUri(), 'Mostrar Evento');

        $menu[1] = array(
            $this->eventoHistoricoRouter->btnDelete(),
        );

        if (is_array($evento->getData())) {
            $data = $evento->getData();
        } else {
            $data = json_decode($evento->getData());
        }

        if (
            $evento->getEvento()->getRegistrar() == true && $evento->getFechaVista() == null &&
            $userloginManager->isGranted('ROLE_EVENTO_REGISTRAR')
        ) {

            $menu['Registrar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => $this->generateUrl('main_eventohistorico_registrar', array('id' => $evento->getId()))
            );
        }

        return $this->render('app/EventoHistorico/show.html.twig', array(
            'menu' => $menu,
            'evento' => $evento,
            'data' => $data,
            'delete_form' => $this->createDeleteForm($evento->getId())->createView(),
        ));
    }

    /**
     * 
     * @Route("/eventohistorico/{id}/delete", name="app_eventohistorico_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_HISTORICO_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        EventoHistorial $evento,
        EventoHistorialManager $eventoHistorialManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $form = $this->createDeleteForm($evento->getId());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();
            $eventoHistorialManager->delete($evento->getId());
        }
        return $this->redirect($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * 
     * @Route("/eventohistorico/batch", name="app_eventohistorico_batch")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_REGISTRAR")
     */
    public function batchAction(Request $request, EventoHistorialManager $eventoHistorialManager)
    {
        $org = null;
        $checks = explode(' ', $request->get('checks'));
        $accion = $request->get('accion');
        if ($checks && $accion) {
            $result = true;
            foreach ($checks as $key => $value) {
                if ($accion == 'visto') {
                    $evento = $eventoHistorialManager->findById($value);
                    if (is_null($org)) {
                        $org = $evento->getServicio()->getOrganizacion();
                    }
                    $result = $result && $eventoHistorialManager->registrar($evento);
                }
            }
            if ($result == true) {
                $this->setFlash('success', sprintf('Se ha realizado la operación de <b>%s</b> para los eventos seleccionado', $accion));
                return new Response(json_encode($checks));
            } else {
                $this->setFlash('error', 'Se ha producido un error en la operación');
                return null;
            }
        }
    }
}
