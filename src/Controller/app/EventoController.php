<?php

namespace App\Controller\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Form\app\EventoNewType;
use App\Form\app\EventoEditType;
use App\Entity\EventoParametro;
use App\Entity\EventoNotificacion;
use App\Entity\EventoHistorial;
use App\Entity\Evento as Evento;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Organizacion as Organizacion;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\EventoManager;
use App\Model\app\EventoTemporalManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ContactoManager;
use App\Model\app\ServicioManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\Router\EventoTemporalRouter;
use App\Model\app\Router\EventoRouter;
use App\Model\app\BitacoraManager;
use App\Model\monitor\EventoItinerarioManager;

class EventoController extends AbstractController
{

    private $userloginManager;
    private $eventoManager;
    private $breadcrumbManager;
    private $organizacionManager;
    private $tipoEventoManager;
    private $servicioManager;
    private $contactoManager;
    private $grupoReferenciaManager;
    private $eventoHistorialManager;
    private $eventoTemporalManager;
    private $eventoRouter;
    private $notificadorAgenteManager;
    private $evTemporalRouter;
    protected $bitacoraManager;
    protected $eventoItinerarioManager;

    function __construct(
        UserLoginManager $userloginManager,
        TipoEventoManager $tipoEventoManager,
        EventoHistorialManager $eventoHistorialManager,
        EventoTemporalManager $eventoTemporalManager,
        EventoManager $eventoManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        ServicioManager $servicioManager,
        ContactoManager $contactoManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        EventoTemporalRouter $evTemporalRouter,
        EventoRouter $eventoRouter,
        BitacoraManager $bitacoraManager,
        EventoItinerarioManager $eventoItinerarioManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->eventoManager = $eventoManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->tipoEventoManager = $tipoEventoManager;
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->contactoManager = $contactoManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->eventoHistorialManager = $eventoHistorialManager;
        $this->eventoTemporalManager = $eventoTemporalManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->evTemporalRouter = $evTemporalRouter;
        $this->eventoRouter = $eventoRouter;
        $this->bitacoraManager = $bitacoraManager;
        $this->eventoItinerarioManager = $eventoItinerarioManager;
    }

    /**
     * @Route("/app/evento/{id}/list", name="main_evento_list",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Eventos");

        $menu = array();
        //agrega un evento en una distribuicion

        $menu = array(
            1 => array($this->eventoRouter->btnNew($organizacion)),
        );

        $eventos = $this->eventoManager->findAll($organizacion);

        return $this->render('app/Evento/list.html.twig', array(
            'menu' => $menu,
            'eventos' => $eventos,
        ));
    }


    /**
     * @Route("/app/evento/{id}/new", name="main_evento_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function newAction(Request $request, $id)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Evento");

        $organizacion = $this->organizacionManager->find($id);

        //creo el evento
        $teventos = $this->tipoEventoManager->findAll();
        foreach ($teventos as $ev) {
            if ($ev->getCodename() == 'EV_CODELCO' && $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_CODELCO')) {
                $choiceEv[$ev->getNombre()] = $ev->getId();
            } else {
                $choiceEv[$ev->getNombre()] = $ev->getId();
            }
        }
        $evento = $this->eventoManager->create($organizacion);

        $options['teventos'] = $choiceEv;
        $options['tnotificweb'] = array_flip($evento->getArrayStrNotificacionWeb());
        $form = $this->createForm(EventoNewType::class, $evento, $options);

        return $this->render('app/Evento/new.html.twig', array(
            'evento' => $evento,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/app/evento/create", name="main_evento_create",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function createAction(Request $request)
    {

        $datos = $request->get('datos');
        $organizacion = $this->organizacionManager->find($request->get('organizacion'));  //creo la organizacion.
        $tmp = array();
        parse_str($request->get('formev'), $tmp);
        $datos['evento'] = $tmp['evento'];    //grabo el form en $datos.
        $error = null;
        // die('////<pre>'.nl2br(var_export($datos, true)).'</pre>////');
        if (!is_null($datos)) {
            if ($request->getMethod() == 'POST') {
                //hago las validaciones
                if ($datos['evento']['nombre'] == "") {
                    $error[] = 'Nombre de Evento Vacio';
                }
                //if (!isset($datos['parametros'])) {
                //    $error[] = 'No estan configurado los parámetros del evento.';
                //}
                if (!isset($datos['servicios'])) {
                    $error[] = 'No estan configurado los servicios para monitorear.';
                }

                //llego aca y no tengo errores
                if (is_null($error)) {
                    $evento = $this->eventoManager->create($organizacion);   //creo el evento. 
                    $evento = $this->eventoManager->setAll($datos['evento']);

                    if (isset($datos['parametros'])) {
                        //ahora agrego las condiciones.
                        foreach ($datos['parametros'] as $key => $value) {
                            if (strpos($key, 'VAR_') !== false && ($value !== '')) {
                                $param = $this->eventoManager->addParam($evento, $key, $value);
                            }
                        }
                    }

                    //ahora grabo los servicios.
                    foreach ($datos['servicios'] as $servicio_id) {
                        $evento = $this->eventoManager->addServicio($evento, $this->servicioManager->find($servicio_id));
                    }

                    //ahora grabo los contactos
                    if (isset($datos['notificaciones'])) {
                        foreach ($datos['notificaciones'] as $notif) {
                            $evento = $this->eventoManager->addNotificacion(
                                $evento,
                                $this->contactoManager->find($notif['contacto_id']),
                                $notif['tipo']
                            );
                        }
                    }

                    //grabo el evento y todo lo asociado.
                    $evento->setActivo(true);  //activo el evento.
                    $evento = $this->eventoManager->save($evento);
                    //retorno todo ok. 
                    $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
                    $bitacora = $this->bitacoraManager->updateEvento($this->userloginManager->getUser(), $evento, 1);

                    $this->get('session')->getFlashBag()->add(
                        'success',
                        sprintf('Se ha creado el evento <b>%s</b> en el sistema.', strtoupper($evento))
                    );
                    //devuelvo y redirect
                    return new Response(
                        json_encode(array(
                            'status' => 0,
                            'redirect' => $this->breadcrumbManager->getVolver()
                        ))
                    );
                }
            }
        } else {
            $error[] = 'No se ha configurado los parámetros del evento';
        }
        return new Response(
            json_encode(array(
                'status' => -1,
                'error' => implode(' | ', $error)
            ))
        );
    }

    /**
     * @Route("/app/evento/{id}/edit", name="main_evento_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function editAction(Request $request, $id)
    {

        $evento = $this->eventoManager->findById($id);
        if (!$evento) {
            throw $this->createNotFoundException('Código de Evento no encontrado.');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Evento");
        $options['tnotificweb'] = array_flip($evento->getArrayStrNotificacionWeb());
        $form = $this->createForm(EventoEditType::class, $evento, $options);

        return $this->render('app/Evento/edit.html.twig', array(
            'evento' => $evento,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/app/evento/{id}/update", name="main_evento_update",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function updateAction(Request $request, $id)
    {
        $evento = $this->eventoManager->findById($id);
        if (!$evento) {
            throw $this->createNotFoundException('Código de Evento no encontrado.');
        }
        $options['tnotificweb'] = array_flip($evento->getArrayStrNotificacionWeb());
        $form = $this->createForm(EventoEditType::class, $evento, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->eventoManager->save($evento)) {
                $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
                $bitacora = $this->bitacoraManager->updateEvento($this->userloginManager->getUser(), $evento, 2);
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($evento->getNombre())));
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }
        return $this->render('app/Evento/edit.html.twig', array(
            'evento' => $evento,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/app/evento/viewparam", name="main_evento_viewparam")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function viewParamAction(Request $request)
    {

        $idEvento = $request->get('idEvento');
        $idOrg = $request->get('idOrg');
        $organizacion = $this->organizacionManager->find($idOrg);
        if ($idEvento != null) {
            //entro aca por que tengo un evento que mostrar. Editar....
            $evento = $this->eventoManager->findById($idEvento);
            foreach ($evento->getServicios() as $servicio) {
                $checked[] = $servicio->getId();
            }
        }
        //obtengo el id del evento seleccionado
        $idTEvento = $request->get('idTEvento');
        $tevento = $this->tipoEventoManager->findById($idTEvento);
        if ($tevento) {
            $variables = $tevento->getVariables();

            //levanto los valore por default del evento para pasarlo al form.
            $default['id'] = $tevento->getId();
            if (isset($evento)) {
                foreach ($evento->getParametros() as $param) {
                    if (strtoupper($param->getVariable()->getDatatype()) == 'BOOLEAN') {
                        $default[$param->getVariable()->getCodename()] = $param->getValor() == '1' ? true : false;
                    } else {
                        $default[$param->getVariable()->getCodename()] = $param->getValor();
                    }
                }
            }

            $type = 'Symfony\Component\Form\Extension\Core\Type\FormType';
            $formcond = $this->get('form.factory')->createNamedBuilder('form', $type, $default);
            foreach ($variables as $variable) {
                if (isset($evento)) { //debo buscar el valor grabado.
                    $param = $this->eventoManager->findParam($evento, $variable);
                }

                $codename = $variable->getCodename();
                switch ($codename) {
                    case ($codename === 'VAR_GRUPO_REFERENCIA_IN' || $codename === 'VAR_GRUPO_REFERENCIA_OUT'):
                        //obtengo los grupos de referencias                        
                        $grp = $this->grupoReferenciaManager->findAllByOrganizacion($organizacion);
                        $choice = array();
                        foreach ($grp as $gr) {
                            $choice[$gr->getNombre()] = $gr->getId();
                        }
                        $formcond->add($variable->getCodename(), ChoiceType::class, array(
                            'label' => $variable->getNombre(),
                            'choices' => $choice,
                            'required' => true,
                            'attr' => array(
                                'help' => 'Seleccion un grupo de referencia. Obligatorio.',
                            ),
                        ));
                        break;
                    default:
                        switch (strtoupper($variable->getDatatype())) {
                            case 'STRING':
                                $formcond->add($variable->getCodename(), TextType::class, array(
                                    'label' => $variable->getNombre(),
                                ));
                                break;
                            case 'BOOLEAN':
                                $formcond->add($variable->getCodename(), CheckboxType::class, array(
                                    'label' => $variable->getNombre(),
                                ));
                                break;
                            case 'INTEGER':
                                $formcond->add($variable->getCodename(), IntegerType::class, array(
                                    'label' => $variable->getNombre(),
                                    'attr' => array(
                                        //'value' => isset($param) ? $param->getValor() : null,
                                        'min' => $variable->getCotaMinima(),
                                        'max' => $variable->getCotaMaxima(),
                                        'help' => sprintf('Valores entre %d y %d. En %s', $variable->getCotaMinima(), $variable->getCotaMaxima(), $variable->getUnidadMedida()),
                                    ),
                                    'invalid_message' => 'Valores fuera de rango'
                                ));
                                break;
                            case 'FLOAT':
                                $formcond->add($variable->getCodename(), NumberType::class, array(
                                    'label' => $variable->getNombre(),
                                    'attr' => array(
                                        //'value' => isset($param) ? $param->getValor() : null,
                                        'min' => $variable->getCotaMinima(),
                                        'max' => $variable->getCotaMaxima(),
                                        'help' => sprintf('Valores entre %d y %d. En %s', $variable->getCotaMinima(), $variable->getCotaMaxima(), $variable->getUnidadMedida()),
                                    ),
                                    'invalid_message' => 'Valores fuera de rango'
                                ));
                                break;
                        }
                        break;
                }
            }

            return new Response(json_encode(array(
                'html' => $this->renderView('app/Evento/setupparam.html.twig', array(
                    'evento' => $tevento,
                    'formcond' => $formcond->getForm()->createView(),
                )),
            )));
        } else {
            return new Response(json_encode(array('html' => 'Error')));
        }
    }

    /**
     * @Route("/app/evento/createparam", name="main_evento_createparam")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function createParamAction(Request $request)
    {
        $em = $this->getEntityManager();

        $datos = $request->get('datos');
        $tmp = array();
        parse_str($request->get('formev'), $tmp);
        $datos['evento'] = $tmp['evento'];    //grabo el form en $datos.
        $formParam = array();
        parse_str($request->get('formcond'), $formParam);
        $tmp = null;
        //die('////<pre>' . nl2br(var_export($formParam, true)) . '</pre>////');
        $paramEntity = null;
        foreach ($formParam['form'] as $key => $value) {
            if (strpos($key, 'VAR_') !== false && $value != '') {
                $variable = $em->getRepository('App:VariableEvento')->findOneBy(array('codename' => $key));
                if ($variable) {
                    $datos['parametros'][$key] = $value;
                    $tmp[$key]['variable'] = $variable;
                    $tmp[$key]['valor'] = $value;
                    if ($variable->getCodename() === 'VAR_GRUPO_REFERENCIA_IN' || $variable->getCodename() === 'VAR_GRUPO_REFERENCIA_OUT') {
                        $paramEntity[$variable->getId()]['nombreEntity'] = $this->grupoReferenciaManager->find($value);
                    }
                }
            }
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewparam.html.twig', array(
                'params' => $tmp,
                'paramEntity' => $paramEntity,
            )),
            'datos' => $datos,
        )));
    }

    /**
     * @Route("/app/evento/updateparam", name="main_evento_updateparam")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function updateParamAction(Request $request)
    {

        $idEvento = $request->get('idEvento');
        if ($idEvento != null) {
            //entro aca por que tengo un evento que mostrar. Editar....
            $evento = $this->eventoManager->findById($idEvento);
        }
        $em = $this->getEntityManager();

        $formParam = array();
        parse_str($request->get('formcond'), $formParam);
        $params = null;
        $paramEntity = null;
        foreach ($formParam['form'] as $key => $value) {
            // die('////<pre>' . nl2br(var_export($key, true)) . '</pre>////');
            if (strpos($key, 'VAR_') !== false && $value != '') {
                $variable = $em->getRepository('App:VariableEvento')->findOneBy(array('codename' => $key));
                if ($variable) {
                    $var = new EventoParametro();
                    $var->setEvento($evento);
                    $var->setVariable($variable);
                    $var->setValor($value);
                    $params[] = $var;

                    if ($variable->getCodename() === 'VAR_GRUPO_REFERENCIA_IN' || $variable->getCodename() === 'VAR_GRUPO_REFERENCIA_OUT') {
                        $paramEntity[$variable->getId()]['nombreEntity'] = $this->grupoReferenciaManager->find($value);
                    }
                }
            }
        }
        $this->eventoManager->setParams($evento, $params);
        $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
        $bitacora = $this->bitacoraManager->updateEvento($this->userloginManager->getUser(), $evento, 2, 'paramentros');
        //  die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewparam.html.twig', array(
                'params' => $params,
                'paramEntity' => $paramEntity,
            )),
        )));
    }

    /**
     * @Route("/app/evento/viewservicios", name="main_evento_viewservicios")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function viewServiciosAction(Request $request)
    {

        $idEvento = $request->get('idEvento');
        $idOrg = $request->get('idOrg');
        if ($idEvento != null) {
            //entro aca por que tengo un evento que mostrar. Editar....
            $evento = $this->eventoManager->findById($idEvento);
            foreach ($evento->getServicios() as $servicio) {
                $checked[] = $servicio->getId();
            }
        }
        $organizacion = $this->organizacionManager->find($idOrg);
        //obtengo el id del evento seleccionado
        $servicios = $this->servicioManager->findAllByOrganizacion($organizacion);
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/setupservicios.html.twig', array(
                'servicios' => $servicios,
                'checked' => isset($checked) ? $checked : null,
            )),
        )));
    }

    /**
     * @Route("/app/evento/createservicios", name="main_evento_createservicios")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function createServiciosAction(Request $request)
    {
        $datos = $request->get('datos');
        $tmp = array();
        parse_str($request->get('formev'), $tmp);
        $datos['evento'] = $tmp['evento'];    //grabo el form en $datos.

        $formServ = array();
        parse_str($request->get('formserv'), $formServ);
        $datos['servicios'] = $formServ['checks'];
        $tmp = null;
        foreach ($formServ['checks'] as $id) {
            $tmp[] = $this->servicioManager->find($id);
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewservicios.html.twig', array(
                'servicios' => $tmp,
                'status' => array(),
            )),
            'datos' => $datos,
        )));
    }

    /**
     * @Route("/app/evento/updateservicios", name="main_evento_updateservicios")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function updateServiciosAction(Request $request)
    {

        $idEvento = $request->get('idEvento');
        $evento = $this->eventoManager->findById($idEvento);  //leo el evento

        $formServ = array();
        parse_str($request->get('formserv'), $formServ);

        $tmp = array();
        if (isset($formServ['checks'])) {
            foreach ($formServ['checks'] as $id) {
                $tmp[] = $this->servicioManager->find($id);
            }
            $evento =  $this->eventoManager->setServicios($evento, $tmp);
        } else {
            $evento = $this->eventoManager->setServicios($evento, null);
        }
        $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
        $bitacora = $this->bitacoraManager->updateEvento($this->userloginManager->getUser(), $evento, 2, 'servicios');
        //obtener el json desde api/eventos
        $status = $this->eventoManager->getStatusAgentes($evento);

        //   die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewservicios.html.twig', array(
                'servicios' => $tmp,
                'status' => isset($status['servicios']) ? $status['servicios'] : [],
            )),
        )));
    }

    /**
     * @Route("/app/evento/viewnotificacion", name="main_evento_viewnotificacion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function viewNotificacionAction(Request $request)
    {

        $idEvento = $request->get('idEvento');
        $idOrg = $request->get('idOrg');
        $organizacion = $this->organizacionManager->find($idOrg);
        if ($idEvento != null) {    //entro aca porque estoy editando
            //entro aca por que tengo un evento que mostrar. Editar....
            $evento = $this->eventoManager->findById($idEvento);
            foreach ($evento->getNotificaciones() as $notif) {
                switch ($notif->getTipo()) {
                    case 1:
                        $celular[] = $notif->getContacto()->getId();
                        break;
                    case 2:
                        $email[] = $notif->getContacto()->getId();
                        break;
                    case 3:
                        $noticeme[] = $notif->getContacto()->getId();
                        break;
                    default:
                        break;
                }
            }
        } else {   //estoy creando el form
            $datos = $request->get('datos');
            if (isset($datos) && isset($datos['notificaciones'])) {   //tengo form de datos
                foreach ($datos['notificaciones'] as $value) {
                    switch ($value['tipo']) {
                        case 1:
                            $celular[] = $value['contacto_id'];
                            break;
                        case 2:
                            $email[] = $value['contacto_id'];
                            break;
                        case 3:
                            $noticeme[] = $value['contacto_id'];
                            break;
                        default:
                            break;
                    }
                }
                //                die('////<pre>' . nl2br(var_export($celular, true)) . '</pre>////');
            }
        }

        $contactos = $this->contactoManager->findAll($organizacion);
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/setupnotificacion.html.twig', array(
                'contactos' => $contactos,
                'celular' => isset($celular) ? $celular : null,
                'email' => isset($email) ? $email : null,
                'noticeme' => isset($noticeme) ? $noticeme : null,
            )),
        )));
    }

    /**
     * @Route("/app/evento/createnotificacion", name="main_evento_createnotificacion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_AGREGAR")
     */
    public function createNotificacionAction(Request $request)
    {
        $datos = $request->get('datos');

        $tmp = array();
        parse_str($request->get('formev'), $tmp);
        $datos['evento'] = $tmp['evento'];    //grabo el form en $datos.

        $formNotif = array();
        parse_str($request->get('formnotif'), $formNotif);
        //  die('////<pre>'.nl2br(var_export($formNotif, true)).'</pre>////');
        $datTmp = array();
        $tmp = array();
        if (isset($formNotif['checks_notif'])) {
            foreach ($formNotif['checks_notif'] as $value) {
                $tmp = explode('_', $value);
                $contacto = $this->contactoManager->find(intval($tmp[1]));
                switch ($tmp[0]) {
                    case 'celular':
                        $d = 1;
                        break;
                    case 'email':
                        $d = 2;
                        break;
                    case 'noticeme':
                        $d = 3;
                        break;
                    default:
                        $d = '---';
                        break;
                }
                // $datos['notificaciones'][] = array('contacto' => $contacto->getId(), 'tipo' => $tmp[0]);
                $datTmp[] = array(
                    'contacto_id' => $contacto->getId(),
                    'contacto' => $contacto,
                    'tipo' => $d
                );
            }
        }
        $datos['notificaciones'] = $datTmp;
        //die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewnotificacion.html.twig', array(
                'notificaciones' => $datTmp,
            )),
            'datos' => $datos,
        )));
    }

    /**
     * @Route("/app/evento/updatenotificacion", name="main_evento_updatenotificacion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_EDITAR")
     */
    public function updateNotificacionAction(Request $request)
    {
        $idEvento = $request->get('idEvento');
        $evento = $this->eventoManager->findById($idEvento);  //leo el evento

        $formNotif = array();
        parse_str($request->get('formnotif'), $formNotif);

        $datTmp = array();
        $tmp = array();
        if (isset($formNotif['checks_notif'])) {
            foreach ($formNotif['checks_notif'] as $value) {
                $tmp = explode('_', $value);
                $contacto = $this->contactoManager->find(intval($tmp[1]));
                switch ($tmp[0]) {
                    case 'celular':
                        $tipo = 1;
                        break;
                    case 'email':
                        $tipo = 2;
                        break;
                    case 'noticeme':
                        $tipo = 3;
                        break;
                    default:
                        $tipo = 0;
                        break;
                }
                $notif = new EventoNotificacion();
                $notif->setEvento($evento);
                $notif->setContacto($contacto);
                $notif->setTipo($tipo);
                $datTmp[] = $notif;
            }
            $this->eventoManager->setNotificaciones($evento, $datTmp);
        } else {
            $this->eventoManager->setNotificaciones($evento, null);
        }
        $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
        $bitacora = $this->bitacoraManager->updateEvento($this->userloginManager->getUser(), $evento, 2, 'notificaciones');

        $status = $this->eventoManager->getStatusAgentes($evento);
        $statusContacto = $this->procesarStatusContacto($status);

        return new Response(json_encode(array(
            'html' => $this->renderView('app/Evento/viewnotificacion.html.twig', array(
                'notificaciones' => $datTmp,
                'statusContacto' => $statusContacto
            )),
        )));
    }

    /**
     * @Route("/app/evento/{id}/show", name="main_evento_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_VER")
     */
    public function showAction(Request $request, $id)
    {

        $evento = $this->eventoManager->findById($id);
        if (!$evento) {
            throw $this->createNotFoundException('Código de Evento no accesible.');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), 'Mostrar Evento');
        $deleteForm = $this->createDeleteForm($id);

        //este es el menu que se muestra en el show del evento.
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_EVENTO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('main_evento_edit', array(
                    'id' => $evento->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_EVENTO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        //armo el tmp por datos que no vienen en las entidades.
        $tmp = null;
        foreach ($evento->getParametros() as $param) {
            if ($param->getVariable()->getCodename() === 'VAR_GRUPO_REFERENCIA_IN' || $param->getVariable()->getCodename() === 'VAR_GRUPO_REFERENCIA_OUT') {
                $tmp[$param->getVariable()->getId()]['nombreEntity'] = $this->grupoReferenciaManager->find($param->getValor());
            }
        }

        //obtener el json desde api/eventos
        $status = $this->eventoManager->getStatusAgentes($evento);
        $statusContacto = $this->procesarStatusContacto($status);
        //dd($statusContacto);

        return $this->render('app/Evento/show.html.twig', array(
            'menu' => $menu,
            'evento' => $evento,
            'status' => isset($status['servicios']) ? $status['servicios'] : [],
            'statusContacto' => $statusContacto,
            'paramEntity' => $tmp,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    private function procesarStatusContacto($status)
    {
        $arr = false;
        if (isset($status['contactos'])) {
            foreach ($status['contactos'] as $contacto) {
                //dd($contacto);
                if (isset($contacto['token'])) {
                    $arr['token'][] = $contacto['id'];
                }

                if (isset($contacto['email'])) {
                    $arr['email'][] = $contacto['id'];
                }
            }
        }
        return $arr;
    }

    /**
     * @Route("/app/evento/{id}/delete", name="main_evento_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EVENTO_ELIMINAR")
     */
    public function deleteAction(Request $request, $id)
    {
        $eve = $this->eventoManager->findById($id);
        $organizacion = $this->userloginManager->getOrganizacion();
        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            //debo mandar a notificar antes de borrar
            $notificar = $this->notificadorAgenteManager->notificar($eve, 2);
            //ahora borro el evento
            $this->eventoManager->delete($id);
            $bitacora = $this->bitacoraManager->deleteEvento($this->userloginManager->getUser(), $eve);
        }
        return $this->redirect($this->generateUrl('main_evento_list', array('id' => $organizacion->getId())));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/app/evento/query", name="app_evento_query")
     * @Route("/apmon/evento/query", name="apmon_evento_query")
     * @Route("/evento/query", name="evento_query")
     * @Method({"GET", "POST"})
     */
    public function queryAjaxAction(Request $request)
    {
        //pongo esta linea para que no se activen la consulta por ajax de los eventos.s 
        //return new Response(json_encode(array('eventos' => null)));

        //aca grabo en la session los nuevos.
        $sonido = false;
        $body_popup = '';
        $button_evento = '';
        $button_evento_aker = '';

        //dd($this->userloginManager->getUltEventoVisto());

        $eventosPanico = array();
        $eventosNormal = array();

        //recupero el id del ultimo id visto para hacer la consulta a partir de alli
        $lastIdRead = $this->userloginManager->getUltEventoVisto();  //recupero el ultimo evento mostrado

        //traigo los eventos del temporal solo los nuevos mayores al id leido
        $eventos = $this->eventoTemporalManager->findNuevos($lastIdRead);
        $sonido = false;
        $maxIdRead = 0;
        $data = [];
        if (count($eventos) > 0) { //tengo eventos que procesar.
            //$sonido = $this->isPlaySonido($sonido, $eventos);   //seteo si hay que reproducir sonido o no.    
            //recorro todos los evento nuevos que he traido
            foreach ($eventos as $key => $evento) {
                $data[$evento->getId()] = [
                    'nuevo' => $evento->getId() > $lastIdRead,
                    'evento' => $evento,
                ];
                $sonido = $evento->getEvento()->getSonido() == true && $sonido == false;
                if ($evento->getId() >= $maxIdRead) {
                    $maxIdRead = $evento->getId();
                }
            }
            $this->userloginManager->setUltEventoVisto($maxIdRead);

            $body_popup = $this->renderView('app/Template/body_popup_evento.html.twig', array(
                'cantidad' => count($eventos),
                'data' => $data,
            ));
        }

        //armo el html del button con las notif.
        $cantTotal = $this->eventoTemporalManager->contarByUser();  //cuento total de eventos para el usuario
        $ultEventos = $this->eventoTemporalManager->findByUser(10);  //los ultimos 10 en el temporal.
        $button_evento = $this->renderView('app/Template/alerta_evento.html.twig', array(
            'cantidad' => $cantTotal,
            'eventos' => $ultEventos,
            'organizacion' => $this->userloginManager->getOrganizacion(),
        ));

        $button_evento_aker = $this->renderView('app/Template_aker/alerta_evento.html.twig', array(
            'cantidad' => $cantTotal,
            'eventos' => $ultEventos,
            'organizacion' => $this->userloginManager->getOrganizacion(),
        ));

        $eventos = array(
            'body' => $body_popup,
            'cant_total' => $cantTotal,
            'cant_nuevos' => count($data),
            'button_evento' => isset($button_evento) ? $button_evento : null, //html del boton evento.
            'button_evento_aker' => isset($button_evento_aker) ? $button_evento_aker : null, //html del boton evento.
            'sonido' => $sonido,
        );

        return new Response(json_encode(array('eventos' => $eventos)));
        //return new Response(json_encode(array('eventos' => null)));
    }


    private function isPlaySonido($sonido, $eventos)
    {
        if (!$sonido) {
            foreach ($eventos as $ev) {
                $sonido = $ev->getEvento()->getSonido() == true && $sonido == false;
                if ($sonido)
                    break;
            }
        }
        return $sonido;
    }


    /**
     * @Route("/apmon/evento/setupvisto", name="main_evento_setupvisto")
     * @Method({"GET", "POST"})
     */
    public function setupvistoAction(Request $request)
    {
        $idEventoTmp = intval($request->get('id'));  //es el id del eventoTemporal.        
        if (!is_null($idEventoTmp) && $idEventoTmp > 0) {
            $eventoTmp = $this->eventoTemporalManager->findById($idEventoTmp);

            $this->eventoHistorialManager->setupVistoTemporal($eventoTmp, $this->userloginManager->getUser());

            if ($eventoTmp != null && $eventoTmp->getEvento() != null) {
                if ($eventoTmp->getEvento()->getClase() == 3 && count($eventoTmp->getEvento()->getHistoricoItinerario()) > 0) {
                    $this->eventoItinerarioManager->setupVisto(
                        $eventoTmp->getEvento()->getHistoricoItinerario(),
                        $this->userloginManager->getUser(),
                        $eventoTmp->getEventoHistorial()
                    );
                }
            }
        }
        return $this->queryAjaxAction($request);
    }

    /**
     * @Route("/app/panico/registrarajax", name="main_panico_registrarajax")
     * @Method({"GET", "POST"})
     */
    public function registrarAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formRegistrar'), $tmp);
        $evHistorial = $this->eventoHistorialManager->findById(intval($request->get('id')));
        $evento = $evHistorial->getEvento();
        if (count($evento->getEventoItinerario()) > 0) { // es un evento de itinerario
            foreach ($evento->getEventoItinerario() as $evIt) {
                foreach ($evIt->getHistoricoItinerario() as $evHistorial) {
                    $evHistorial->setRespuesta($tmp['panico']['respuesta']); //es un historicoItinerario el que le mandamos


                }
            }
        } else { //es un evento comun y silvestre
            $evHistorial->setRespuesta($tmp['panico']['respuesta']);
        }
        $this->eventoHistorialManager->registrar($evHistorial, $evHistorial->getRespuesta());

        return $this->queryAjaxAction($request);
    }
}
