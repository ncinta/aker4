<?php

namespace App\Controller\user;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Usuario as Usuario;
use App\Form\user\UsuarioNewType;
use App\Form\user\UsuarioEditType;
use App\Form\user\UsuarioResetType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\PaisManager;
use App\Model\app\UsuarioManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoReferenciaManager;
use App\Form\user\UsuarioApicodeType;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\Router\UsuarioRouter;
use App\Model\monitor\EmpresaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\TransporteManager;
use PDOException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UsuarioController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Usuario');
    }

    /**
     * @Route("/usuario/{id}/show", name="usuario_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_VER")
     */
    public function showAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        UsuarioRouter $usuarioRouter
    ) {

        $usuario = $this->getRepository()->findOneBy(array('id' => $id));

        $breadcrumbManager->push($request->getRequestUri(), !is_null($usuario) ? $usuario->getNombre() : 'NaN');

        if (!$usuario) {
            throw $this->createNotFoundException('Código de Usuario no encontrado.');
        }
        return $this->render('user/Usuario/show.html.twig', array(
            'usuario' => $usuario,
            'menu' => $this->getShowMenu($usuario, $userloginManager, $usuarioRouter),
            'delete_form' => $this->createDeleteForm($id)->createView()
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $usuario
     * @return array $menu
     */
    private function getShowMenu($usuario, $userloginManager, $usuarioRouter)
    {
        return  array(
            1 => array(
                $usuarioRouter->btnEdit($usuario, $userloginManager),
                $usuarioRouter->btnEditPermiso($usuario, $userloginManager),
                $usuarioRouter->btnDelete($userloginManager),

            ),
            2 => array(
                $usuarioRouter->btnChangePassword($usuario, $userloginManager),
                $usuarioRouter->btnResetClave($usuario, $userloginManager),
                $usuarioRouter->btnApicode($usuario, $userloginManager),
            ),
            3 => array(
                $usuarioRouter->btnServicios($usuario, $userloginManager),
                $usuarioRouter->btnReferencias($usuario, $userloginManager),
            )
        );
    }

    /**
     * @Route("/usuario/{idorg}/new", name="usuario_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_AGREGAR")
     */
    public function newAction(
        Request $request,
        $idorg,
        OrganizacionManager $organizacionManager,
        PaisManager $paisManager,
        UsuarioManager $usuarioManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $organizacion = $organizacionManager->find($idorg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Código de Organizacion no encontrado.');
        }

        //se traen todos los modulos activos que tiene la organizacion.
        $modulos = $organizacionManager->getModulosActivos($organizacion);
        $usuario = $usuarioManager->create($organizacion);

        $options['paises'] = $paisManager->getArrayPaises();
        $options['organizacion'] = $organizacion;
        $form = $this->createForm(UsuarioNewType::class, $usuario, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                try {
                    $usuarioManager->save($usuario);
                    $this->setFlash('success', 'El usuario ha sido creado');

                    return $this->redirect($this->generateUrl('usuario_permiso', array(
                        'id' => $usuario->getId()
                    )));
                } catch (PDOException $e) {
                    $this->setFlash('error', sprintf('Error en la creación del usuario. Es posible que ya exista el usuario o email. (%s)', toString($e)));
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), 'Nuevo Usuario');

        return $this->render('user/Usuario/new.html.twig', array(
            'organizacion' => $organizacion, //es la organizacion donde se crea el usuario
            'modulos' => $modulos, //son las aplicaciones de la organizacion
            'usuario' => $usuario, // en esta instancia 
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/usuario/{id}/edit", name="usuario_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_EDITAR")
     */
    public function editAction(
        Request $request,
        $id,
        PaisManager $paisManager,
        UsuarioManager $usuarioManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        $usuario = $usuarioManager->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }
        //se traen todos los modulos activos que tiene la organizacion.
        $em = $this->getEntityManager();
        $modulos = $em->getRepository('App:Organizacion')->getModulosActivos($usuario->getOrganizacion());

        $options['paises'] = $paisManager->getArrayPaises();
        $options['organizacion'] = $usuario->getOrganizacion();
        $form = $this->createForm(UsuarioEditType::class, $usuario, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $breadcrumbManager->pop();

                //se setean los roles segun estan en las tablas de permisos.
                $usuario->setRoles(array());
                if ($usuario->getOrganizacion()->getUsuarioMaster() == $usuario) {
                    if ($usuario->getOrganizacion()->getTipoOrganizacion() == '1') {
                        $usuario->addRole('ROLE_MASTER_DISTRIBUIDOR');
                    } else {
                        $usuario->addRole('ROLE_MASTER_CLIENTE');
                    }
                }
                foreach ($usuario->getPermisos() as $permiso) {
                    $usuario->addRole($permiso->getCodename());
                }

                // se graba el usuario
                $usuarioManager->save($usuario);

                $this->setFlash('success', sprintf('Los datos de %s han sido actualizados', $usuario));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), 'Editar Usuario');
        return $this->render('user/Usuario/edit.html.twig', array(
            'modulos' => $modulos,
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    private function randChr()
    {
        return chr(rand(ord('a'), ord('z')));
    }

    /**
     * @IsGranted("ROLE_USUARIO_EDITAR")
     * @Route("/usuario/{id}/reset", name="usuario_reset",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function resetAction(Request $request, Usuario $usuario, BreadcrumbManager $breadcrumbManager)
    {
        $form = $this->createForm(UsuarioResetType::class, $usuario);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //empiezo el cambio
            $em = $this->getDoctrine()->getManager();
            $pass = strtoupper($this->randChr()) . $this->randChr() . $this->randChr() . $this->randChr() . rand(1111, 9999);
            $usuario->setPlainPassword($pass);
            $usuario->setChangePassword(true);
            //      die('////<pre>'.nl2br(var_export($pass, true)).'</pre>////');        
            //aca se graba el usuario
            $em->persist($usuario);
            $em->flush();
            if ($usuario->getEmail() != null) {
                $email = $usuario->getEmail();
            } else {
                $email = 'cgbrandolin@gmail.com';
            }
            $message = \Swift_Message::newInstance()
                ->setContentType("text/html")
                ->setSubject('Aker: Reset de Clave')
                ->setFrom('send@example.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'user/Usuario/resetpassword.email.twig',
                    array(
                        'usuario' => $usuario,
                        'pass' => $pass
                    )
                ));

            $this->get('mailer')->send($message);


            $breadcrumbManager->pop();
            $this->setFlash('success', sprintf('La clave de %s ha sido reseteada', $usuario));
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), 'Reset Clave');

        return $this->render('user/Usuario/resetpassword.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    /**
     * @IsGranted("ROLE_USUARIO_EDITAR")
     * @Route("/usuario/{id}/apicode", name="usuario_generarapicode",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function generarapicodeAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, UsuarioManager $usuarioManager)
    {

        $usuario = $usuarioManager->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $form = $this->createForm(UsuarioApicodeType::class, $usuario);

        $apicode = $request->get('c');   //recupero el apicode que se envio
        if (is_null($apicode)) {        //debo generar el apicode
            $apicode = chr(rand(65, 90)) . rand(0, 9) . chr(rand(65, 90)) . chr(rand(65, 90)) . rand(0, 9) . rand(0, 9);
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                // se graba el usuario
                $usuario->setApicode($apicode);
                $usuarioManager->save($usuario);


                $this->setFlash('success', sprintf('Los datos de %s han sido actualizados', $usuario));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        return $this->render("user/Usuario/apicode.html.twig", array(
            'usuario' => $usuario,
            'apicode' => $apicode,
            'form' => $form->createView()
        ));
    }

    /**
     * Esta es una action sobrecargada ya que renderiza el template y hace la 
     * grabación de los permisos cuando estos se configuran en el form de la vista.
     * @param integer $id Es el id del usuario para el cual se estan configurando los permisos.
     * @IsGranted("ROLE_USUARIO_EDITAR")
     * @Route("/usuario/{id}/permiso", name="usuario_permiso",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function permisoAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        UsuarioManager $usuarioManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager,
        NotificadorAgenteManager $notificadorAgenteManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), 'Asignar Permisos');
        $roles = null;
        $rolesOld = null;
        $usuario = $usuarioManager->find($id);
        foreach ($usuario->getPermisos() as $permiso) {
            $rolesOld[] = $permiso->getCodename();
        }
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }
        if ($usuario == $userloginManager->getUser()) {
            throw $this->createNotFoundException('Código de usuario inaccesible.');
        }
        $modulos = $organizacionManager->getModulosActivos($usuario->getOrganizacion());

        if ($request->getMethod() == 'POST') {
            $form = $request->get('permiso');

            $breadcrumbManager->pop();
            //borro todos los roles del usuario
            $usuario->setRoles(array('ROLE_USUARIO'));
            $usuario->setPermisos(null);
            //le asigino el role de master en caso de que sea.
            if ($usuario->getOrganizacion()->getUsuarioMaster() == $usuario) {
                if ($usuario->getOrganizacion()->getTipoOrganizacion() == '1') {
                    $permiso = $this->getEntityManager()->getRepository('App:Permiso')->findOneBy(array('codename' => 'ROLE_MASTER_DISTRIBUIDOR'));
                    $usuario->addPermiso($permiso);
                    $usuario->addRole('ROLE_MASTER_DISTRIBUIDOR');
                } else {
                    $permiso = $this->getEntityManager()->getRepository('App:Permiso')->findOneBy(array('codename' => 'ROLE_MASTER_CLIENTE'));
                    $usuario->addPermiso($permiso);
                    $usuario->addRole('ROLE_MASTER_CLIENTE');
                }
            }
            if (!is_null($form)) {
                //agrego los permisos que estan en el form.
                foreach ($form as $codename) {
                    $permiso = $this->getEntityManager()->getRepository('App:Permiso')->findOneBy(array('codename' => $codename));
                    $usuario->addPermiso($permiso);
                    $usuario->addRole($codename);
                    $roles[] = $codename;
                    
                    if($permiso != null && $permiso->getCodename() == 'ROLE_AGRO_ACCESO_REINGENIO'){ //es usuario de reingenio y tiene modulo agro                        
                        $data = $usuario->getData();
                        if($data == null || !array_key_exists('hash_reingenio', $data)){ //si no existe el hash se lo agregamos
                            $hash = $notificadorAgenteManager->getHashReingenio($usuario);
                            $usuario->addAccesoReingenio($hash);
                        }
                        //aca pedir el hash a reingenio para guardar en el usuario

                    }
                }
            }

            // se graba el usuario
            if ($rolesOld == null) {
                $diff_add = $roles;
                $diff_remove = null;
            } else {
                $diff_add = array_diff($roles, $rolesOld);
                $diff_remove = array_diff($rolesOld, $roles);
            }
            $usuarioManager->save($usuario, $diff_add, $diff_remove);
            $this->setFlash('success', sprintf('Los permisos de <b>%s</b> han sido actualizados', $usuario));
            return $this->redirect($breadcrumbManager->getVolver());
        }

        return $this->render("user/Usuario/permiso.html.twig", array(
            'modulos' => $modulos,
            'usuario' => $usuario,
            'tipo' => $usuario->getOrganizacion()->getTipoOrganizacion(),
        ));
    }

    /**
     * @IsGranted("ROLE_USUARIO_SERVICIO_VER")
     * @Route("/usuario/{id}/serviciosasignados", name="usuario_servicios_asignados",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function serviciosasginadosAction($id, UsuarioManager $usuarioManager, ServicioManager $servicioManager)
    {
        $usuario = $usuarioManager->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $disponibles = $servicioManager->findAllSinAsignar2Usuario($usuario);
        $asignados = $servicioManager->findSoloAsignadosUsuario($usuario);

        return $this->render("user/Usuario/serviciosasignados.html.twig", array(
            'usuario' => $usuario,
            'disponibles' => $disponibles,
            'asignados' => $asignados,
        ));
    }

    /**
     * @Route("/usuario/servicio/asignar", name="usuario_servicios_asignar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_SERVICIO_ASOCIAR")
     */
    public function servicioasignarAction(Request $request, UsuarioManager $usuarioManager, ServicioManager $servicioManager)
    {

        $usuario = $usuarioManager->find($request->get('id'));

        $idsServicios = unserialize($request->get('servicios'));
        if ($usuario && count($idsServicios) > 0) {
            foreach ($idsServicios as $key => $value) {
                $servicioManager->asignarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('user/Usuario/show_disponibles.html.twig', array(
                'disponibles' => $servicioManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('user/Usuario/show_asignados.html.twig', array(
                'asignados' => $servicioManager->findSoloAsignadosUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/usuario/servicio/eliminar", name="usuario_servicios_eliminar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_SERVICIO_DESASOCIAR")
     */
    public function servicioeliminarAction(Request $request, UsuarioManager $usuarioManager, ServicioManager $servicioManager)
    {
        $usuario = $usuarioManager->find($request->get('id'));

        $idsServicios = unserialize($request->get('servicios'));
        if ($usuario && count($idsServicios) > 0) {
            foreach ($idsServicios as $key => $value) {
                $servicioManager->eliminarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('user/Usuario/show_disponibles.html.twig', array(
                'disponibles' => $servicioManager
                    ->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('user/Usuario/show_asignados.html.twig', array(
                'asignados' => $servicioManager
                    ->findSoloAsignadosUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/usuario/{id}/referencias/asignadas", name="usuario_referencias_asignadas",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_VER")
     */
    public function referenciasasignadasAction($id, UsuarioManager $usuarioManager, GrupoReferenciaManager $gruporeferenciaManager)
    {
        $usuario = $usuarioManager->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $disponibles = $gruporeferenciaManager->findAllSinAsignar2Usuario($usuario);
        $asignados = $gruporeferenciaManager->findAllByUsuario($usuario);

        return $this->render('user/Usuario/referenciasasignadas.html.twig', array(
            'usuario' => $usuario,
            'disponibles' => $disponibles,
            'asignados' => $asignados,
        ));
    }

    /**
     * @Route("/usuario/referencias/asignar", name="usuario_referencias_asignar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_ASOCIAR")
     */
    public function referenciasasignarAction(Request $request, UsuarioManager $usuarioManager, GrupoReferenciaManager $gruporeferenciaManager)
    {
        $usuario = $usuarioManager->find($request->get('id'));

        $idsGrpRef = unserialize($request->get('referencias'));
        if ($usuario && count($idsGrpRef) > 0) {
            foreach ($idsGrpRef as $key => $value) {
                $gruporeferenciaManager->asignarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('user/Usuario/show_grp_disponibles.html.twig', array(
                'disponibles' => $gruporeferenciaManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('user/Usuario/show_grp_asignados.html.twig', array(
                'asignados' => $gruporeferenciaManager->findAllByUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/usuario/referencias/eliminar", name="usuario_referencias_eliminar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_DESASOCIAR")
     */
    public function referenciaseliminarAction(Request $request, UsuarioManager $usuarioManager, GrupoReferenciaManager $gruporeferenciaManager)
    {
        $usuario = $usuarioManager->find($request->get('id'));

        $idsGrpRef = unserialize($request->get('referencias'));
        if ($usuario && count($idsGrpRef) > 0) {
            foreach ($idsGrpRef as $key => $value) {
                $gruporeferenciaManager->eliminarUsuario($value, $usuario);
            }
        }
        $respuesta = array(
            'disponibles' => $this->renderView('user/Usuario/show_grp_disponibles.html.twig', array(
                'disponibles' => $gruporeferenciaManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('user/Usuario/show_grp_asignados.html.twig', array(
                'asignados' => $gruporeferenciaManager->findAllByUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/usuario/{id}/eliminar", name="usuario_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_ELIMINAR")
     */
    public function deleteAction(Request $request, $id, UsuarioManager $usuarioManager, BreadCrumbManager $breadcrumbManager)
    {
        $em = $this->getEntityManager();

        $usuario = $em->find('App:Usuario', $id);
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }
        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $usuarioManager->delete($usuario);
            $this->setFlash('success','Usuario Eliminado Correctamente');
            $breadcrumbManager->pop();
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }


    /**
     * @Route("/monitor/usuario/edit", name="monitor_usuario_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_AGREGAR")
     */
    public function usuarioAction(
        Request $request,
        UsuarioManager $usuarioManager,
        LogisticaManager $logisticaManager,
        TransporteManager $transporteManager,
        EmpresaManager $empresaManager,
        UserLoginManager $userloginManager,
        ItinerarioManager $itinerarioManager
    ) {
        $id = intval($request->get('id'));
        $usuario = $usuarioManager->find($id);
        $idEntity = intval($request->get('idEntity'));
       
        if ($request->get('entity') == 'logistica') {
            $entity = $logisticaManager->find($idEntity);
            $usuario->setLogistica($entity);           
        } elseif ($request->get('entity') == 'transporte') {
            $entity = $transporteManager->find($idEntity);
            $usuario->setTransporte($entity);         
        } elseif ($request->get('entity') == 'empresa') { //empresa
            $entity = $empresaManager->find($idEntity);
            $usuario->setEmpresa($entity);            
        } elseif ($request->get('entity') == 'itinerario') {
        } else {
            if($id == 0){
                $entity = $userloginManager->getOrganizacion();               
            }else{ //viene por el show de itinerario
                $entity = $itinerarioManager->find($idEntity);
            }
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);
        if ($request->getMethod() == 'POST' && isset($tmp['usuario_edit'])) {
            $data = $tmp['usuario_edit'];
            $usuario = $usuarioManager->setData($usuario, $data);

            if ($usuario = $usuarioManager->save($usuario)) {
                $html = $this->renderView('monitor/Usuario/usuarios.html.twig', array(
                    'usuarios' => $entity->getUsuarios(),
                 
                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/usuario/delete", name="monitor_usuario_delete")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function deleteusuarioAction(Request $request, UsuarioManager $usuarioManager, ItinerarioManager $itinerarioManager)
    {
        $id = intval($request->get('id'));
        $itinerarioId = intval($request->get('itinerario'));
       // dd($itinerarioId);
        $usuario = $usuarioManager->find($id);       
        // die('////<pre>' . nl2br(var_export($request->get('entity'), true)) . '</pre>////');
        if (!$usuario) {
            throw $this->createNotFoundException('Usuario no encontrado.');
        }
        if ($request->get('entity') == 'logistica') {
            $entity = $usuario->getLogistica();
        } elseif ($request->get('entity') == 'transporte') {
            $entity = $usuario->getTransporte();
        } elseif ($request->get('entity') == 'empresa') {
            $entity = $usuario->getEmpresa();
        } else { //itinerario
            $entity = $itinerarioManager->find($itinerarioId);
            //$entity = $usuario->getEmpresa();
        }
        $usuarios = $entity->getUsuarios();
        if ($usuarioManager->delete($usuario)) {
            $html = $this->renderView('monitor/Usuario/usuarios.html.twig', array(
                'usuarios' => $usuarios,
                'showItinerario' => false,
                'itinerario' => $entity
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }




    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
