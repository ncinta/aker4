<?php

namespace App\Controller\user;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Form\user\ProfileType;
use App\Form\user\ProfileChangePasswordType;
use App\Model\app\UserLoginManager;
use App\Model\app\BitacoraManager;
use App\Model\app\PaisManager;
use App\Model\app\BreadcrumbManager;
use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Usuario');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.token_storage');
    }

    /**
     * @Route("/user/profile/show", name="user_profile_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_PROFILE_VER")
     */
    public function showAction(Request $request, BreadcrumbManager $breadcrumbManager, UserLoginManager $userloginManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Mi Perfil");
        $usuario = $userloginManager->getUser();
        return $this->render('user/Profile/show.html.twig', array(
            'menu' => $this->getShowMenu($usuario, $userloginManager),
            'usuario' => $usuario,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $cliente 
     * @return array $menu
     */
    private function getShowMenu($usuario, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_USUARIO_PROFILE_EDITAR')) {
            $menu['Actualizar perfil'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('user_profile_edit')
            );
        }
        if ($userloginManager->isGranted('ROLE_USUARIO_PROFILE_CAMBIARCLAVE')) {
            $menu['Cambiar contraseña'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('user_profile_password_change', array(
                    'id' => $usuario->getId(),
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/user/profile/edit", name="user_profile_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_PROFILE_EDITAR")
     */
    public function editAction(
        Request $request,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        PaisManager $paisManager
    ) {
        $usuario = $userloginManager->getUser();

        $options['paises'] = $paisManager->getArrayPaises();
        $options['usuario'] = $usuario;
        $form = $this->createForm(ProfileType::class, $usuario, $options);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                $em = $this->getEntityManager();
                $em->persist($usuario);
                $em->flush();

                $this->setFlash('success', 'Los datos de tu perfil han sido actualizados');

                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Cambio de Datos");
        return $this->render('user/Profile/edit.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/user/profile/{id}/changepassword", name="user_profile_password_change")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_PROFILE_CAMBIARCLAVE")
     */
    public function changepasswordAction(
        Request $request,
        Usuario $usuario,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        BitacoraManager $bitacoraManager,
        UserPasswordEncoderInterface $encoder
    ) {

        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $form = $this->createForm(ProfileChangePasswordType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                $user = $em->getRepository(Usuario::class)->findOneBy(['username' => $usuario->getUsername()]);
                if (null === $user) {
                    throw $this->createNotFoundException(sprintf('The user with confirmation token <b>%s</b> does not exist', ''));
                }

                $password = $encoder->encodePassword($user, $form->getData()->getPlainPassword());
                $user->setChangePassword(false);
                $user->setPassword($password);
                $em->persist($user);
                $em->flush();
                $bitacoraManager->usuarioChangePassword($userloginManager->getUser(), $user, false, $request);

                $this->setFlash('success', sprintf('Se ha cambiado la clave de <b>%s</b>', $usuario->getNombre()));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), 'Cambiar clave');
        return $this->render('user/Profile/changepassword.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/user/profile/clean", name="user_profile_password_clean")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function cleanAction(Request $request, UserLoginManager $userloginManager)
    {

        $usuario = $userloginManager->getUser();
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $form = $this->createForm(ProfileChangePasswordType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $userManager = $this->get('fos_user.user_manager');
                $user = $userManager->findUserByUsername($usuario->getUsername());
                $user->setChangePassword(false);
                $user->setLastChangePasswordAt(new \DateTime());
                $user->setPlainPassword($form->getData()->getPlainPassword());

                //aca se graba el usuario
                $userManager->updateUser($user);

                $this->setFlash('success', 'Ha cambiado la contraseña en forma correcta.');
                return $this->redirect($this->generateUrl('main_default'));
                //                } else {
                //                    $this->setFlash('error', 'La contraseña no puede ser la misma que ya tenia.');
                //                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        return $this->render('user/Profile/clean.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
