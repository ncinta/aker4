<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EventoReplyController extends AbstractController
{

    protected $cliente;
    protected $container;

    public function __construct(
        HttpClientInterface $cliente,
        ContainerInterface $container
    ) {
        $this->cliente = $cliente;
        $this->container = $container;
    }

    /**
     * @Route("/ws/evento/add2", name="evento_add2")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) //los panicos tambien entran por aca. 
    {
        $data = utf8_encode($request->getContent());

        $status = true;
        if ($data) {
            //$data = '{"key": "key1234","fecha_utc": "2014-09-17 13:29:00","titulo": "CRPJ84 exceso velocidad","evento_id": "26","servicio_id": "205","data": {"cuerpo": "<h3>EXCESO +90 KMS/H</h3><br><b>Vehiculo:</b> CRPJ84<br><b>Tiempo en infracccion:</b> -199016:-39:-25 aprox.<br><b>Referencia:</b> ---<br><br><b>INICIO INFRACCION</b><br><b>Fecha y hora:</b> 30/12/1899 04:00:00<br><b>Velocidad:</b> 0 kms/h<br><b>Ubicación:</b> <br><br><b>FIN INFRACCCION</b><br><b>Fecha y hora:</b> 23/05/2013 17:48:51<br><b>Velocidad:</b> 64 kms/h<br><b>Ubicación:</b> <br><br>ULTIMO REPORTE<br>Fecha y Hora: 23/05/2013 17:48:51<br>Velocidad: 64<br>Dirección: 2º<br>Posición Válida: SI <br>Ubicación: <br>Odometro: 133603 kms<br>"}}';
            $form = json_decode($data, true);
            //dd($data);
            $response = $this->cliente->request(
                'POST',
                'https://app.akercontrol.com/ws/evento/add',
                array(
                    'body' => $data,
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                )
            );

            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                $result = $response->getContent();
                return new Response($result, $statusCode);
            }
            return new Response($response->getContent(), $statusCode);
        } else {
        }        
        return new Response('Error', 500);
    }

    /**
     * @Route("/ws/evento/panico2", name="evento_panico2")
     * @Method({"GET", "POST"})
     */
    public function panicoAction(Request $request) //los panicos tambien entran por aca. 
    {
        $data = utf8_encode($request->getContent());

        $status = true;
        if ($data) {
            //$data = '{"key": "key1234","fecha_utc": "2014-09-17 13:29:00","titulo": "CRPJ84 exceso velocidad","evento_id": "26","servicio_id": "205","data": {"cuerpo": "<h3>EXCESO +90 KMS/H</h3><br><b>Vehiculo:</b> CRPJ84<br><b>Tiempo en infracccion:</b> -199016:-39:-25 aprox.<br><b>Referencia:</b> ---<br><br><b>INICIO INFRACCION</b><br><b>Fecha y hora:</b> 30/12/1899 04:00:00<br><b>Velocidad:</b> 0 kms/h<br><b>Ubicación:</b> <br><br><b>FIN INFRACCCION</b><br><b>Fecha y hora:</b> 23/05/2013 17:48:51<br><b>Velocidad:</b> 64 kms/h<br><b>Ubicación:</b> <br><br>ULTIMO REPORTE<br>Fecha y Hora: 23/05/2013 17:48:51<br>Velocidad: 64<br>Dirección: 2º<br>Posición Válida: SI <br>Ubicación: <br>Odometro: 133603 kms<br>"}}';
            $form = json_decode($data, true);
            //dd($data);
            $response = $this->cliente->request(
                'POST',
                'https://app.akercontrol.com/ws/evento/panico',
                array(
                    'body' => $data,
                    'headers' => array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'),
                )
            );

            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                $result = $response->getContent();
                return new Response($result, $statusCode);
            }
            return new Response($response->getContent(), $statusCode);
        } else {
        }        
        return new Response('Error', 500);
    }
}
