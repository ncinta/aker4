<?php

namespace App\Controller\ws\interno;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\pae\EventoPaeManager;
use App\Model\pae\ServicioPaeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class PaeController extends AbstractController
{
    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $tanqueManager;
    private $eventoManager;
    private $servicioPaeManager;

    private $utils;

    function __construct(
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        EventoPaeManager $eventoManager,
        ServicioPaeManager $servicioPaeManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->utils = $utilsManager;
        $this->eventoManager = $eventoManager;
        $this->servicioPaeManager = $servicioPaeManager;
    }

    /**
     * @Route("/ws/interno/pae", name="webservice_interno_pae", methods={"GET"})
     */
    public function paeAction(Request $request)
    {
        $datos = null;
        $serviciosPae = null;
        $status = self::STATUS_OK;

        $patentesTesting = $request->query->get('patentes');   //solo patentes        
        if ($patentesTesting) {
            $patenteArray = explode(',', $patentesTesting);
            foreach ($patenteArray as $patente) {
                $servicio = $this->servicioManager->findByPatente($patente);
                if ($servicio) {
                    $serviciosPae[] = $servicio;
                }
            }
        } else {            
            $serviciosPae = $this->servicioPaeManager->obtenerPadron();            
        }
          
        if ($this->getParameter('pae_habilitado') && $serviciosPae) { //aca procesa los servicios en los eventos de agentes
            $this->eventoManager->procesar(array_filter($serviciosPae));    //armo y actualizo los eventos de pae segun se necesite
        }

        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }


    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }


}
