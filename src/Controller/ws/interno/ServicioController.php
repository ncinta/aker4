<?php

namespace App\Controller\ws\interno;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\pae\EventoPaeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class ServicioController extends AbstractController
{
    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $tanqueManager;
    private $eventoManager;

    private $utils;

    function __construct(
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        EventoPaeManager $eventoManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->utils = $utilsManager;
        $this->eventoManager = $eventoManager;
    }

    /**
     * @Route("/ws/interno/servicios", name="webservice_interno_servicios_por_patentes", methods={"GET"})
     */
    public function serviciosPorPatentesAction(Request $request)
    {
        $datos = null;        
        $status = true;

        // Obtener el parámetro de consulta 'patentes'
        $patentes = $request->query->get('patentes');
        if ($patentes) {
            // Dividir la cadena separada por comas en un array de patentes
            $patenteArray = explode(',', $patentes);

            // Buscar y devolver servicios específicos para las patentes proporcionadas
            foreach ($patenteArray as $patente) {
                $status = self::STATUS_OK;
                $servicio = $this->servicioManager->findByPatente($patente);                
                if ($status == self::STATUS_OK && !is_null($servicio)) {
                    $datos[$patente] = $this->getDataServicio($servicio);
                } else {
                    $datos[$patente] = null;
                }
            }

        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }


    private function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function parseParamBool($valor)
    {
        $result = false;
        if (!is_null($valor)) {
            if ($valor == '1' || strtolower($valor == 'true')) {
                $result = true;
            } else {
                $result = false;
            }
        }
        return $result;
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }


    private function getDataServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {
        $infoCerca = null;
        if (!is_null($cercania) && $cercania && !is_null($servicio->getEquipo())) {
            if (is_null($this->referencias)) {
                $this->referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
            }
            if ($this->referencias && !is_null($servicio->getUltLatitud()) && !is_null($servicio->getUltLongitud())) {
                $cercanas = $this->servicioManager->buscarCercaniaPosicion($servicio->getUltLatitud(), $servicio->getUltLongitud(), $this->referencias, 1);
                if (is_null($cercanas[0])) {
                    $infoCerca = null;
                } else {
                    $infoCerca = array(
                        'referencia' => $cercanas[0]['id'],
                        'nombre' => $cercanas[0]['nombre'],
                        'texto' => $cercanas[0]['leyenda'],
                        'icono' => !is_null($cercanas[0]['icono']) ? $cercanas[0]['icono'] : null,
                    );
                }
            }
        }


        // $domicilio = null;   ///para ver error en direccion
        //'ult_reporte' => $strFecha,

        $datos = array(
            'id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'ult_reporte' => !is_null($servicio->getUltFechahora()) ? date_format($servicio->getUltFechahora(), 'Y-m-d H:i:s') : null,
            'ult_fecha' => !is_null($servicio->getUltFechahora()) ? $this->utils->UTC2local($servicio->getUltFechahora(), $servicio->getOrganizacion()->getTimeZone(), 'Y-m-d H:i:s') : null,
            'ult_latitud' => $servicio->getUltLatitud(),
            'ult_longitud' => $servicio->getUltLongitud(),
            'ult_odometro' => $servicio->getUltOdometro(),
            'ult_horometro' => $servicio->formatHorometro(),
            'ult_velocidad' => $servicio->getUltVelocidad(),
            'ult_direccion' => $servicio->getUltDireccion(),
            'ult_bateria' => $servicio->getUltBateria(),
            'domicilio' => is_null($domicilio) || !$domicilio ? null : $this->getDomicilio($servicio),
            'cercania' => $infoCerca,
        );
        if (!is_null($getId) && $getId) {
            $datos['id'] = $servicio->getId();
        }
        //agrego los datos del grupo
        $grupos = $this->getGrupos($servicio);
        if ($grupos) {
            $datos['grupos'] = $grupos;
        }

        //agrego la clase del servicio
        $datos['clase_vehiculo'] = array(
            'id' => $servicio->getTipoServicio()->getId(),
            'nombre' => $servicio->getTipoServicio()->getNombre(),
            'url_icono' => $servicio->getTipoServicio()->getWebPath()
        );

        $datos['organizacion'] = [
            'id' => $servicio->getOrganizacion()->getId(),
            'nombre' => $servicio->getOrganizacion()->getNombre(),
        ];

        if (!is_null($servicio->getVehiculo())) {
            $datos['patente'] = $servicio->getVehiculo()->getPatente() != null ? $servicio->getVehiculo()->getPatente() : '---';
            $datos['marca'] = $servicio->getVehiculo()->getMarca() != null ? $servicio->getVehiculo()->getMarca() : '---';
            $datos['modelo'] = $servicio->getVehiculo()->getModelo() != null ? $servicio->getVehiculo()->getModelo() : '---';
            $datos['anioFabricacion'] = $servicio->getVehiculo()->getAnioFabricacion() != null ?  $servicio->getVehiculo()->getAnioFabricacion() : '---';
            //agrego los datos del tipo de vehiculo
            if (!is_null($servicio->getVehiculo()->getTipoVehiculo())) {
                $datos['tipo_vehiculo'] = array(
                    'id' => $servicio->getVehiculo()->getTipoVehiculo()->getId(),
                    'nombre' => $servicio->getVehiculo()->getTipoVehiculo()->getTipo()
                );
            }
        }

        //empiezo a sacar los datos de la ulTrama
        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['canbusData'])) {
            $can = $ultTrama['canbusData'];
            if ($servicio->getCanbus()) {
                $datos['canbus'] = $can;
            }

            if ($servicio->getMedidorCombustible()) {
                $datos['tanque'] = $this->tanqueManager->getTanque($servicio, $ultTrama);
                if (!is_null($servicio->getVehiculo())) {
                    $datos['tanque']['litrosTotalTanque'] = intval($servicio->getVehiculo()->getLitrosTanque());
                }
            }
        }
        if (isset($ultTrama['contacto'])) {
            $datos['ult_contacto'] = $ultTrama['contacto'];
        }
        foreach ($servicio->getSensores() as $sensor) {
            // dd($sensor->getId());
            if ($sensor->getModeloSensor()->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                if ($sensor->getUltFechahora() != null) {
                    $sensores[$sensor->getSerie()] = array(
                        'nombre' => $sensor->getNombre(),
                        'valor' => round($sensor->getUltValor()),
                        'fecha' => !is_null($sensor->getUltFechahora()) ? date_format($sensor->getUltFechahora(), 'Y-m-d H:i:s') : null,
                        'bateria' => $sensor->getUltBateria(),
                    );
                }
            } else {  //no es seriado
                $campo = $sensor->getModeloSensor()->getCampoTrama();
                if (isset($ultTrama[$campo])) {
                    $sensores[$campo] = array(
                        'nombre' => $sensor->getNombre(),
                        'valor' => $ultTrama[$campo],
                        'title' => sprintf('%s (%d a %d %s)', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                    );
                }
            }
        }
        if (isset($sensores)) {
            $datos['sensores'] = $sensores;
        }
        if ($servicio->getEntradasDigitales() === true) {
            foreach ($servicio->getInputs() as $input) {
                $valor = isset($ultTrama[$input->getModeloSensor()->getCampoTrama()]) ? $ultTrama[$input->getModeloSensor()->getCampoTrama()] : false;
                //aca armo la leyenda teneindo en cuenta si hay icono o no.
                $datos['inputs'][$input->getModeloSensor()->getCampoTrama()] = array(
                    'nombre' => $input->getNombre(),
                    'valor' => $valor,
                    'leyenda' => $input->getAbbr() . ': ' . ($valor ? 'ON' : 'OFF'),
                    'icono' => $input->getIcono()
                );
            }
        }

        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');

        return $datos;
    }

    private function getGrupos($servicio)
    {
        $grupos = null;
        if ($servicio->getGrupos()) {
            foreach ($servicio->getGrupos() as $grp) {
                $grupos[] = array(
                    'id' => $grp->getId(),
                    'nombre' => $grp->getNombre()
                );
            }
        }
        return $grupos;
    }
}
