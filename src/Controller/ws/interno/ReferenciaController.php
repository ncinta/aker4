<?php

namespace App\Controller\ws\interno;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Entity\Organizacion as Organizacion;
use App\Model\app\OrganizacionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\ReferenciaManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Referencia;

class ReferenciaController extends AbstractController
{
    private $referenciaManager;
    private $organizacionManager;

    const STATUS_OK = 0;
    const ERROR = 1;
    const ERROR_FALTAN_DATOS = 2;
    const ERROR_REFERENCIA_NO_ENCONTRADA = 3;
    const ERROR_ORGANIZACION = 4;
    const ERROR_APIKEY = 5;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR => 'Error',
        self::ERROR_FALTAN_DATOS => 'Error, faltan datos',
        self::ERROR_REFERENCIA_NO_ENCONTRADA => 'Error, referencia no encontrada',
        self::ERROR_ORGANIZACION => 'Error, organizacion no encontrada',
        self::ERROR_APIKEY => 'Error,apikey inválida',

    );

    function __construct(
        ReferenciaManager $referenciaManager,
        OrganizacionManager $organizacionManager

    ) {
        $this->referenciaManager = $referenciaManager;
        $this->organizacionManager = $organizacionManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/ws/interno/referencia/fix/{cantidad}", name="webservice_interno_referencia_fix"), methods = {"GET"}
     */
    public function refupdateAction(Request $request, $cantidad = null)
    {
        if (is_null($cantidad)) {
            $cantidad = 10;
        }
        $status = 0;
        $info = ['checked_ok' => [], 'verificadas' => [], 'checked_not_poligon' => [], 'radial' => []];
        $respuesta = null;
        $referencias = $this->referenciaManager->findByStatus(0, $cantidad); // se consultan cada mil referencias                
        foreach ($referencias as $referencia) {
            if ($referencia->getClase() == Referencia::CLASE_POLIGONO) {   //es clase poligono
                if (!is_null($referencia->getPoligono()) && $referencia->getPoligono() != '') {  //tiene poligono para controlar
                    if (!$this->referenciaManager->check($referencia)) { //verificar poligono                
                        //dd($referencia->getId());
                        $referencia = $this->referenciaManager->fix($referencia); //redibujar poligono
                        $info['fixed'][] = $referencia->getId();
                    } else {
                        $referencia->setStatus(1); //verificada
                        $info['verificadas'][] = $referencia->getId();
                    }
                    $referencia = $this->referenciaManager->save($referencia);
                } else {
                    //la referencia dice poligono pero no tiene poligono asociado, se pasa a radial.
                    $referencia->setClase(Referencia::CLASE_RADIAL);
                    $referencia->setRadio(50);
                    $referencia->setStatus(1);
                    $referencia = $this->referenciaManager->save($referencia);
                    $info['checked_not_poligon'][] = $referencia->getId();
                }
            } else {
                $info['radial'][] = $referencia->getId();
            }

            sleep(1); //mandamos cada 1 seg para no matar la api
        }
        return $this->generateResponse(self::STATUS_OK, array(
            'code' => self::STATUS_OK,
            'info' => $info,
            'response' => $this->strResponse[self::STATUS_OK],
        ));
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }

    /**
     * En esta funcion podemos pasar por POSTMAN un json con una estructura de las referencias que queremos dar de alta. El json que tenemos que pasar en el body del reques es:
     *
     * [
     *       {
     *           "type": "Feature",
     *           "properties": {
     *               "Name": "AREA=70|ACC=01",
     *               "description": "Área de incumbencia"
     *           },
     *           "geometry": {
     *               "type": "Polygon",
     *               "coordinates": [
     *                   [
     *                       [
     *                           -69.5500001772106,
     *                           -47.2499996237766
     *                       ],
     *                       [
     *                           -69.5500001772106,
     *                           -47.2499996237766
     *                       ]
     *                   ]
     *               ]
     *           }
     *       },
     *   ] 
     * Se pueden enviar varias referencias en el mismo array de json.
     * Para convertir el KML o KML a esta estructura de json se utiliza el servicio -> https://mygeodata.cloud/converter/kml-to-geojson
     * Tener en cuenta que se debe saca los datos innecesarios y solo pasar los objetos de las referencias ya que en el servicio vienen mas datos.
     * @Route(
     *     "/ws/interno/referencia/add/{id}", 
     *     name="webservice_interno_referencia_add",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"POST"}
     * )     
     */
    public function add(Request $request, Organizacion $organizacion = null)
    {
        $status = self::STATUS_OK;
        $info = '';   //guardo la data de las refeencias generadas
        // Verifica si el objeto $organizacion es null
        if (!$organizacion) {
            // throw new NotFoundHttpException('La organización no existe.');
            $status = self::ERROR_ORGANIZACION;
        }

        if ($status == self::STATUS_OK) {
            if ($organizacion) {
                $data = json_decode($request->getContent(), true);
                if ($data) {
                    //tengo todo listo para crear las referencias.
                    foreach ($data as $item) {

                        $nombre = $item['properties']['name'];
                        $descripcion = isset($item['properties']['description']) ? $item['properties']['description'] : '';
                        $clase = $item['geometry']['type'] == 'Polygon' ? 2 : 1;   //clase 2 = poligonal
                        $poligono = $this->convertirCoordenadasAStringInvertido($item['geometry']['coordinates']);
                        $ptoCentral = $this->obtenerPuntoCentral($item['geometry']['coordinates']);                       
                        if ($poligono) {
                            $referencia = $this->referenciaManager->findByNombre($nombre);
                            if (!$referencia) {
                                $referencia = new Referencia();
                            }
                            
                            $referencia->setPropietario($organizacion);
                            $organizacion->addReferencia($referencia);

                            $referencia->setClase($clase);
                            $referencia->setRadio(null);
                            $referencia->setStatus(1);   //checkeada
                            $referencia->setNombre($nombre);
                            $referencia->setCodigoExterno($nombre);
                            $referencia->setDescripcion($descripcion);
                            $referencia->setVisibilidad(true);
                            $referencia->setColor('#C24749');
                            $referencia->setLatitud($ptoCentral['latitud']);
                            $referencia->setLongitud($ptoCentral['longitud']);
                            $referencia->setPoligono($poligono);
                            $referencia->setVelocidadMaxima(60);    //esto es por default que lo cambien a mano segun necesiten
                            $referencia = $this->referenciaManager->save($referencia);
                          //  dd($poligono);
                        }                        
                    }
                } else {
                    $status = self::ERROR_FALTAN_DATOS;
                }
            }
        }
        return $this->generateResponse(self::STATUS_OK, array(
            'code' => $status,
            'info' => $info,
            'response' => $this->strResponse[$status],
        ));
    }

    private function convertirCoordenadasAStringInvertido(array $coordinates)
    {
        $result = [];

        foreach ($coordinates as $coordGroup) {
            foreach ($coordGroup as $coordinate) {
                // Invertir el orden de las coordenadas (latitud, longitud)
                $result[] = $coordinate[1] . ',' . $coordinate[0];
            }
        }

        // Unir todos los pares de coordenadas con un espacio
        return implode(',', $result);
    }

    function obtenerPuntoCentral(array $coordinates) {
        $totalLat = 0;
        $totalLon = 0;
        $count = 0;
    
        // Recorrer todas las coordenadas
        foreach ($coordinates as $coordGroup) {
            foreach ($coordGroup as $coordinate) {
                // Sumar latitud y longitud
                $totalLon += $coordinate[0]; // Longitud
                $totalLat += $coordinate[1]; // Latitud
                $count++;
            }
        }
    
        // Calcular las medias
        $centroLat = $totalLat / $count;
        $centroLon = $totalLon / $count;
    
        return ['latitud' => $centroLat, 'longitud' => $centroLon];
    }
}
