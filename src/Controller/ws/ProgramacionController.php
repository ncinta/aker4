<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\ProgramacionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ProgramacionController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private $apikey = array(
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;

    const STATUS_OK = 0;
    const STATUS_ERRORPOST = -1;
    const ERROR_FALTAN_DATOS =  -2;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::STATUS_ERRORPOST => 'No es un metodo post',
        self::ERROR_FALTAN_DATOS => 'Error, faltan datos',
    );

    /**
     * Esta funcion atiende la respuesta del programador remoto cuando se ejecutan 
     * las programaciones remotas en el equipo.
     */

    /**
     * @Route("/ws/prog/response", name="webservice_prog_response")
     * @Method({"GET", "POST"})
     */
    public function responseAction(Request $request, ProgramacionManager $programacionManager)
    {
        $data = json_decode(utf8_encode($request->getContent()), true);   //obtengo la data
        // die('////<pre>'.nl2br(var_export($data, true)).'</pre>////');
        $idProg = $data['idProgramacion'];
        $idComando = $data['idComando'];
        $estado = $data['status'];
        if ($request->getMethod() != 'POST') {
            $status = self::STATUS_ERRORPOST;
        } else {
            if (is_null($idProg)) {
                $status = self::ERROR_FALTAN_DATOS;
            } else {
                $status = true;
                $programacionManager->responseProgramacion($idProg, $idComando, $estado);
            }
            $status = self::STATUS_OK;
        }
        return new Response(json_encode(array(
            'code' => $status,
            'response' => $this->strResponse[$status],
        )), 200);
    }

    /**
     * Controla que la ip local del server sea la del reporte del servicio.
     * @param type $servicio
     */
    private function enMiAmbitoLan($servicio)
    {
        $ultTrama = json_decode($servicio->getUltTrama(), true);
        if (!is_null($ultTrama)) {
            return isset($ultTrama['ip_capturador']) && in_array($ultTrama['ip_capturador'], $this->container->parameters['calypso.capturadores']);
        }
        return false;
    }

    /**
     * Esta funcion se ejecuta cada X minutos y despacha al programador todas las
     * programaciones pendientes.
     */

    /**
     * @Route("/ws/prog/reply", name="webservice_prog_reply")
     * @Method({"GET", "POST"})
     */
    public function replyAction(ProgramacionManager $programacionManager)
    {
        // die('////<pre>'.nl2br(var_export($this->container->parameters, true)).'</pre>////');
        $em = $this->getEntityManager();
        $servicios = $em->getRepository('App:Servicio')->progPendientes();

        //recorro todos los servicios, seguro que tengo variables que cumplen las condiciones
        $sendOk = $sendFail = 0;
        foreach ($servicios as $servicio) {
            if ($this->enMiAmbitoLan($servicio)) {  //se controla tambien que tenga equipo
                if ($programacionManager->initConfig($servicio)) {
                    foreach ($servicio->getParametros() as $param) {
                        if (!$param->isBlock() && $param->getValorFuturo() != '' && $param->getIntentos() < 10) {
                            $programacionManager->setConfig($param, $param->getValorFuturo(), false);
                        }
                    }
                    $idProg = $programacionManager->executeConfig();
                    if ($idProg !== false) {
                        $res = $programacionManager->runProgramacion($idProg);
                        if ($res > 0) {
                            $sendOk++;
                        } else {
                            $sendFail++;
                        }
                    }
                }
            }
        }
        // die('////<pre>' . nl2br(var_export($idProg, true)) . '</pre>////');

        $status = self::STATUS_OK;

        return new Response(json_encode(array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'sendOK' => $sendOk,
            'sendFail' => $sendFail,
        )), 200);
    }
}
