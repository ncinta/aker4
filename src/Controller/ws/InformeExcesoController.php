<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\BackendWsManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InformeExcesoController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
        '52f215dbeb19934eb4ea1f7edce2e3de8cbc77f0' => 'mavesa',
    );
    private $referencias = null;
    private $backend;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    /**
     * Parametros por request.
     * apiKey: es el api del cliente.
     * desde: fecha en formato Y-m-d H:n:s
     * hasta: fecha en formato Y-m-d H:n:s
     * serv: nombre del servicio
     * servId: id del servicio, serv y servId son excluyentes. servId es el preferido.
     * @return type
     */

    /**
     * @Route("/ws/info/exceso", name="webservice_info_exceso")
     * @Method({"GET", "POST"})
     */
    public function totalesAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager
    ) {
        $apikey = $request->get('apiKey');
        $fdesde = $request->get('desde');
        $fhasta = $request->get('hasta');
        $serv = $request->get('servicio');
        $servId = $request->get('servicioId');

        $status = true;
        $informe = null;
        if (is_null($fdesde) || is_null($fhasta) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $fdesde = new \DateTime($fdesde);
            $fhasta = new \DateTime($fhasta);
            // die('////<pre>' . nl2br(var_export($fhasta, true)) . '</pre>////');
            $organizacion = $organizacionManager->findByApiKey($apikey);  //obtengo la organizacion.
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    $this->setBackend($organizacion);   //seteo el backend.
                    if (!is_null($servId)) {
                        $servicios[] = $servicioManager->find($servId);
                    } elseif (!is_null($serv)) {
                        $servicios[] = $servicioManager->findByNombre($serv);
                    } else {
                        $servicios = $servicioManager->findAllEnabledByOrganizacion($organizacion, false);
                    }

                    //obtengo la información
                    $informe = array();
                    foreach ($servicios as $servicio) {
                        $data = $this->getDataExceso($servicio, $fdesde->format('Y-m-d H:n:s'), $fhasta->format('Y-m-d H:n:s'), $utilsManager);
                        if ($data) {
                            $informe[] = array(
                                'servicio' => $servicio->getNombre(),
                                'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null,
                                'cantidad' => count($data),
                                'data' => $data,
                            );
                        }
                    }
                    //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                    $status = self::STATUS_OK;
                } else {
                    $status = self::ERROR_ORGANIZACION;
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($informe)) {
            $respuesta['data'] = $informe;
        }
        return $this->generateResponse($status, $respuesta);
    }

    public function getDataExceso($servicio, $desde, $hasta, $utilsManager)
    {
        $informe = array();
        $vMax = is_null($servicio->getTipoServicio()->getVelocidadMaxima()) ? 90 : $servicio->getTipoServicio()->getVelocidadMaxima();
        //se toma a 90 por defaul salvo que tenga otro limite en la base de datos.
        $informe = $this->backend->informeExcesosVelocidad($servicio->getId(), $desde, $hasta, $vMax, array());
        if ($informe != null) {
            foreach ($informe as $key => $value) {
                $informe[$key]['tiempo_exceso'] = $utilsManager->segundos2tiempo($value['duracion']);
                unset($informe[$key]['referencia_id']);
            }
        }
        // die('////<pre>'.nl2br(var_export($informe, true)).'</pre>////');


        return $informe;
    }

    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function setBackend($organizacion)
    {
        $usuario = $organizacion->getUsuarioMaster();
        $this->backend = new BackendWsManager($this->getEntityManager(), $organizacion, $this->container->getParameter('calypso.backend_addr'), $this->container->getParameter('calypso.backend_addr_backup'), $usuario);
        return $this->backend;
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }

    /**
     * Parametros por request.
     * apiKey: es el api del cliente.
     * desde: fecha en formato Y-m-d H:n:s
     * hasta: fecha en formato Y-m-d H:n:s
     * patentes: array de patentes
     * grupoID: ID del grupo de referencias sobre la que se quiere generar el control de exceso     
     */
}
