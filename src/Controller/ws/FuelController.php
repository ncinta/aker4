<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CargaCombustible;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\UtilsManager;
use App\Model\app\UsuarioManager;
use App\Model\app\ServicioManager;
use App\Model\app\ChoferManager;
use App\Model\fuel\PuntoCargaManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\BackendWsManager;
use App\Model\app\BitacoraServicioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FuelController extends AbstractController
{

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 7;
    const ERROR_MEDICIONES = 6;
    const ERROR_USUARIONOENCONTRADO = 10;
    const ERROR_SERVICIONOENCONTRADO = 11;
    const ERROR_CHOFERNOENCONTRADO = 12;
    const ERROR_CARGANOENCONTRADO = 13;
    const ERROR_COMBUSTIBLEERRONEO = 14;
    const ERROR_LITROSINVALIDO = 15;
    const ERROR_NO_HAY_DATOS = 16;
    const ERROR_FALTA_FECHA = 17;
    const ERROR_FALTAN_LITROS = 18;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
        self::ERROR_USUARIONOENCONTRADO => 'Usuario no encontrado',
        self::ERROR_SERVICIONOENCONTRADO => 'Servicio no encontrado',
        self::ERROR_CHOFERNOENCONTRADO => 'Chofer no encontrado',
        self::ERROR_CARGANOENCONTRADO => 'Carga no encontrado',
        self::ERROR_COMBUSTIBLEERRONEO => 'Combustible no encontrado',
        self::ERROR_LITROSINVALIDO => 'Litros no encontrados',
        self::ERROR_NO_HAY_DATOS => 'Body vacio',
        self::ERROR_FALTA_FECHA => 'Falta fecha',
        self::ERROR_FALTAN_LITROS => 'Litros erroneos o faltantes'
    );
    private $organizacionManager;
    private $usuarioManager;
    private $servicioManager;
    private $choferManager;
    private $puntoCargaManager;
    private $combustibleManager;
    private $backendWsManager;
    private $utilsManager;
    private $bitacora;

    public function __construct(
        OrganizacionManager $organizacionManager,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        ChoferManager $choferManager,
        PuntoCargaManager $puntoCargaManager,
        CombustibleManager $combustibleManager,
        BackendWsManager $backendwsManager,
        UtilsManager $utilsManager,
        BitacoraServicioManager $bitacora
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->usuarioManager = $usuarioManager;
        $this->servicioManager = $servicioManager;
        $this->choferManager = $choferManager;
        $this->puntoCargaManager = $puntoCargaManager;
        $this->combustibleManager = $combustibleManager;
        $this->backendWsManager = $backendwsManager;
        $this->utilsManager = $utilsManager;
        $this->bitacora = $bitacora;
    }

    /**
     * @Route("/ws/fuel/add/{apikey}", name="webservice_fuel_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request, $apikey)
    {
        $status = self::STATUS_OK;
        $data = utf8_encode($request->getContent());
        $chofer = null;
        $ptoCarga = null;
        $usuario = null;
        $tipoCombustible = null;
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        if (is_null($apikey)) {
            $status = self::ERROR_APIKEY;
        } else {
            if (is_null($data)) {
                $status = self::ERROR_NO_HAY_DATOS;
            } else {
                $organizacion = $this->organizacionManager->findByApiKey($apikey);
                if (!is_null($organizacion)) {
                    // $status = self::STATUS_OK;
                    $data = json_decode($data, true);

                    //chofer
                    if (isset($data['chofer_id'])) {
                        $chofer = $this->choferManager->find(intval($data['chofer_id']));
                        if (!$chofer) {
                            $status = self::ERROR_CHOFERNOENCONTRADO;
                        }
                    } else {
                        //usuario
                        if (isset($data['usuario_id'])) {
                            $usuario = $this->usuarioManager->find(intval($data['usuario_id']));
                            if (!$usuario) {
                                $status = self::ERROR_USUARIONOENCONTRADO;
                            }
                        }
                    }
                    //servicio
                    if (isset($data['servicio_id'])) {
                        $servicio = $this->servicioManager->find(intval($data['servicio_id']));
                        if (!$servicio) {
                            $status = self::ERROR_SERVICIONOENCONTRADO;
                        }
                    } else {
                        $status = self::ERROR_SERVICIONOENCONTRADO;
                    }

                    //punto de carga
                    if (isset($data['puntocarga_id'])) {
                        $ptoCarga = $this->puntoCargaManager->findById(intval($data['puntocarga_id']));
                        if (!$ptoCarga) {
                            $status = self::ERROR_CARGANOENCONTRADO;
                        }
                    } else {
                        $status = self::ERROR_CARGANOENCONTRADO;
                    }
                    //combustible
                    if (isset($data['combustible_id'])) {
                        $tipoCombustible = $this->puntoCargaManager->findTipoCombustible(intval($data['combustible_id']));
                        if (!$tipoCombustible) {
                            $status = self::ERROR_COMBUSTIBLEERRONEO;
                        }
                    } else {
                        $status = self::ERROR_COMBUSTIBLEERRONEO;
                    }

                    //fecha
                    if (!isset($data['fecha'])) {
                        $status = self::ERROR_FALTA_FECHA;
                    }

                    //parse y control de litros
                    if (isset($data['litros'])) {
                        $litros = number_format(intval($data['litros']), 2);
                    } else {
                        $status = self::ERROR_FALTAN_LITROS;
                    }
                    if ($status == self::STATUS_OK) {

                        $afip = $this->getJsonAfip($data);
                        //aca todos los datos estan bien asi que hay que grabarlos
                        $combustible = new CargaCombustible();
                        $combustible->setServicio($servicio);
                        $combustible->setUsuario($usuario);
                        $combustible->setFecha(new \DateTime($data['fecha']));
                        $combustible->setModoIngreso(2); //modo app.
                        $combustible->setEstado(0); //nueva.
                        $combustible->setLitrosCarga(floatval($data['litros']));

                        //debo buscar el precio, basados en el punto de carga y el combustible
                        $precio = $this->puntoCargaManager->findPrecio(intval($data['puntocarga_id']), intval($data['combustible_id']));
                        if ($precio == 0 && isset($data['monto_total'])) {
                            $combustible->setMontoTotal($data['monto_total']);
                        } else {
                            $combustible->setMontoTotal($precio * floatval($data['litros']));
                        }
                        

                        //si trae la latitud se agrega como latitudCarga
                        if (isset($data['latitud'])) {
                            $combustible->setLatitudCarga(floatval($data['latitud']));
                        }
                        if (isset($data['longitud'])) {
                            $combustible->setLongitudCarga(floatval($data['longitud']));
                        }

                        $combustible->setTipoCombustible($tipoCombustible);
                        $combustible->setChofer($chofer);
                        $combustible->setPuntoCarga($ptoCarga);
                        $combustible->setData(count($afip) > 0 ? json_encode(array("afip" => $afip)) : null);

                        if (isset($data['tipo_carga']) && strtoupper($data['tipo_carga']) == strtoupper('Total')) {
                            $combustible->setCargaCompleta(true);
                        } else {
                            $combustible->setCargaCompleta(false);
                        }

                        if (isset($data['guia_despacho'])) {
                            $combustible->setGuiaDespacho($data['guia_despacho']);
                        }

                        //seteo la trama anterior.
                        $trama = $this->backendWsManager->obtenerTramaAnterior($combustible->getServicio()->getId(), $combustible->getFecha());
                        if ($trama) {
                            if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                                $combustible->setOdometro((int) ($trama['odometro'] / 1000)); //odometro del servicio
                            }
                            if (isset($trama['oid'])) {
                                $combustible->setId_trama($trama['oid']);
                            }
                            if (isset($trama['fecha'])) {
                                $combustible->setFechaTrama(new \DateTime($trama['fecha']));
                            }
                            // en latitud/longitud se trae la posicion del vehiculo en el momento de ingreso de la carga
                            if (isset($trama['posicion'])) {
                                $combustible->setLatitud($trama['posicion']['latitud']);
                                $combustible->setLongitud($trama['posicion']['longitud']);
                            }
                        }
                        if (isset($data['odometro']) && !is_null($data['odometro']) && $data['odometro'] != '') {
                            $combustible->setOdometroTablero(intval($data['odometro'])); //odometro de la carga
                        }
                        if (isset($data['horometro']) && !is_null($data['horometro']) && $data['horometro'] != '') {
                            $combustible->setHorometroTablero(intval($data['horometro']));
                        }
                        $this->combustibleManager->add($combustible);
                        $this->bitacora->combustibleCreateApp(isset($data['chofer_id']) ? $chofer : $usuario, $combustible->getServicio(), $combustible);
                    }
                }
            }
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'carga_id' => isset($combustible) ? $combustible->getId() : null,
            'data' => $data
        );
        return $this->generateResponse($status, $respuesta);
    }

    private function getJsonAfip($data)
    {
        $arr = array();
        foreach ($data as $key => $value) {
            if ($key[0] == '_') {
                $arr[substr($key, 1)] = $value;
            }
        }
        return $arr;
        // die('////<pre>' . nl2br(var_export($arr, true)) . '</pre>////');
    }

    /**
     * @Route("/ws/fuel/updatecoord", name="webservice_fuel_updatecoord")
     * @Method({"GET", "POST"})
     */
    public function tramaFuelAction(Request $request)
    {
        $data = array();

        $cargas = $this->combustibleManager->findAllCargas();
        foreach ($cargas as $carga) {
            if (is_null($carga->getFecha_trama()) && is_null($carga->getId_trama()) && is_null($carga->getLatitud()) && is_null($carga->getLongitud())) {
                $trama = $this->backendWsManager->obtenerTramaAnterior($carga->getServicio()->getId(), $carga->getFecha());
                if (isset($trama['_id']) && isset($trama['_id']['$id'])) {
                    $carga->setId_trama($trama['_id']['$id']);
                }

                if (isset($trama['fecha']) && isset($trama['fecha'])) {
                    $carga->setFecha_trama(new \DateTime($trama['fecha']));
                }

                if (isset($trama['posicion'])) {
                    $posicion = $trama['posicion'];
                    if (isset($posicion['latitud']) && !is_null($posicion['latitud'])) {
                        $carga->setLatitud($posicion['latitud']);
                    }
                    if (isset($posicion['longitud']) && !is_null($posicion['longitud'])) {
                        $carga->setLongitud($posicion['longitud']);
                    }
                }

                $this->combustibleManager->update($carga);
            }
        }

        $status = self::STATUS_OK;
        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $data
        );
        return $this->generateResponse($status, $respuesta);
        //  }
    }

    /**
     * ws ats sa
     * @Route("/ws/fuel/cargas/{apikey}", name="webservice_fuel_cargas"), methods={"GET"})
     */
    public function cargaslAction(Request $request, $apikey)
    {
        $status = self::STATUS_OK;
        $respuesta = ['code' => self::STATUS_OK, 'response' => '', 'cargas' => []];
        $dataOut = [];
        $data = utf8_encode($request->getContent());
        if (is_null($apikey)) {
            $status = self::ERROR_APIKEY;
        } else {
            if (is_null($data)) {
                $status = self::ERROR_NO_HAY_DATOS;
            } else {
                $organizacion = $this->organizacionManager->findByApiKey($apikey);
                if (!is_null($organizacion)) {

                    $data = json_decode($data, true);
                    if (!isset($data['fecha_desde']) || !isset($data['fecha_hasta'])) {
                        $status = self::ERROR_FALTA_FECHA;
                    }

                    if ($status == self::STATUS_OK) { //llego aca porque estan todos los datos obligatorios.                        
                        $servicios = isset($data['servicios']) && count($data['servicios']) > 0 ? $data['servicios'] : null;
                        $puntoscarga = isset($data['puntoscarga']) && count($data['puntoscarga']) > 0 ? $data['puntoscarga'] : null;
                        $cargas = $this->combustibleManager->findByServicioPuntoCarga($data['fecha_desde'], $data['fecha_hasta'], $organizacion, $servicios, $puntoscarga);

                        //armando salida.
                        $dataOut = $this->armarData($cargas);
                       /// die('////<pre>' . nl2br(var_export($dataOut, true)) . '</pre>////');
                    }
                }
            }
        }

        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'cargas' => $dataOut
        ));
    }

    private function armarData($cargas)
    {
        $arr_cargas = array();
        foreach ($cargas as $key => $carga) {
            $arr_cargas[$carga->getId()] = array(
                "usuario_id" => $carga->getUsuario() ? $carga->getUsuario()->getId() : null,
                "servicio" => $carga->getServicio() ? array(
                    "id" => $carga->getServicio()->getId(),
                    "nombre" => $carga->getServicio()->getNombre(),
                    "patente" => $carga->getServicio()->getVehiculo() ? $carga->getServicio()->getVehiculo()->getPatente() : null,
                ) : null,
                "chofer" => $carga->getChofer() ? array(
                    'id' => $carga->getChofer()->getId(),
                    'nombre' => $carga->getChofer()->getNombre(),
                    'documento' => $carga->getChofer()->getDocumento(),
                ) : null,
                "puntocarga" => $carga->getPuntoCarga() ? array(
                    'id' => $carga->getPuntoCarga()->getId(),
                    'nombre' => $carga->getPuntoCarga()->getNombre(),
                    'cuit' => $carga->getPuntoCarga()->getCuit(),
                ) : null, 
                "estacion" => $carga->getReferencia() ? array(
                    'id' => $carga->getReferencia()->getId(),
                    'nombre' => $carga->getReferencia()->getNombre(),
                ) : null,
                "combustible" => $carga->getTipoCombustible() ? array(
                    'id' => $carga->getTipoCombustible()->getId(),
                    'nombre' => $carga->getTipoCombustible()->getNombre(),
                ) : null,
                "fecha" => $carga->getFecha()->format('d/m/Y H:i:s'),
                "guia_despacho" => $carga->getGuiaDespacho(),
                "litros" => $carga->getLitrosCarga(),
                "odometro" => $carga->getOdometro(),
                "horometro" => $carga->getHorometroTablero(),
                "latitud" => $carga->getLatitud(),
                "longitud" => $carga->getLongitud(),
                "tipo_carga" => $carga->getCargaCompleta() ? 'Completa' : 'Parcial',
                "modo_ingreso" => $carga->getStrModoIngreso(),
                'data' => $carga->getData() ? json_decode($carga->getData()) : null

            );
        }

        return $arr_cargas;
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }
}
