<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\NotificadorTeknalManager;
use App\Entity\Itinerario as Itinerario;
use Psr\Log\LoggerInterface;

class ReplyTeknalController extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ERROR_DATA = 1;
    const STATUS_ERROR_SINDATA = 2;
    const STATUS_ERROR_SINEVENTOID = 3;
    const STATUS_ERROR_SINSERVICIOID = 4;
    const STATUS_ERROR_SINTITULO = 5;
    const STATUS_ERROR_GRABACION = 6;
    const STATUS_ERROR_FECHA = 7;
    const STATUS_ERROR_SINUID = 8;

    const LOGISTICA_TEKNAL = 19;   //VER QUE ID TIENE

    private $logger;
    private $itinerarioManager;
    private $logisticaManager;
    private $logear = true;
    private $notificadorTeknalManager;

    public function __construct(
        LoggerInterface $logger,
        ItinerarioManager $itinerarioManager,
        NotificadorTeknalManager $notificadorTeknalManager,
        LogisticaManager $logisticaManager
    ) {
        $this->logger = $logger;
        $this->itinerarioManager = $itinerarioManager;
        $this->logisticaManager = $logisticaManager;
        $this->notificadorTeknalManager = $notificadorTeknalManager;
    }

    /**
     * @Route("/ws/reply/teknal/position", name="reply_teknal_position", methods={"GET"})
     */
    public function replyPositionAction(Request $request)
    {
        //obtener los itinerarios de Teknal que esten nuevos o activos.
        $teknal = $this->logisticaManager->find(self::LOGISTICA_TEKNAL);
        $estados = [Itinerario::ESTADO_NUEVO, Itinerario::ESTADO_ENCURSO];
        $itinerarios = $this->itinerarioManager->findAllByLogistica($teknal, $estados);
        $this->logTeknal('Itinerarios -> ' . count($itinerarios));
        //por cada itinerario armar la data
        foreach ($itinerarios as $itinerario) {
            //puedo tener varios servicios para el itinerario por lo que tengo que mandar para cada uno.
            foreach ($itinerario->getServicios() as $ItServicio) {
                if ($ItServicio === null) {
                    $this->logTeknal('Servicio es null para el itinerario ' . $itinerario->getId());
                    continue;
                }
                
                //armo la data para el itinerario en cuestion
                $dataTeknal = $this->toArrayDataTeknal($itinerario, $ItServicio->getServicio());
                if ($dataTeknal === false) {
                    $this->logTeknal('DataTeknal es false para el itinerario ' . $itinerario->getId());
                    continue;
                }
                $jsonTeknal = json_encode($dataTeknal);
                if ($jsonTeknal !== false && $jsonTeknal !== null) {
                    $this->logTeknal('Json -> ' . $jsonTeknal);
                    //enviar la data
                    $notificarTeknal = $this->notificadorTeknalManager->sendData($jsonTeknal);
                    $this->logTeknal('Result -> ' . $notificarTeknal);
                } else {
                    $this->logTeknal('No se pudo armar el json');
                }
            }
        }
        $status = self::STATUS_OK;

        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    private function logTeknal($str)
    {
        if ($this->logear) {
            $this->logger->notice('replyteknal | ' . $str);
        }
    }

    private function toArrayDataTeknal($itinerario, $servicio)
    {
        if ($servicio->getVehiculo() === null) {
            $patente = $servicio->getNombre();
        } else {
            $patente = $servicio->getVehiculo()->getPatente();
        }
        //obtengo y parseo la fecha actual.
        $fecha = new \DateTime();
        if ($servicio->getUltfechahora() != null) {
            $fecha = $servicio->getUltfechahora()->format('Y-m-d') . ' ' . $servicio->getUltfechahora()->format('H:i:s');
            return [
                'Token' => $this->notificadorTeknalManager->obtenerToken(),
                'Patente' => $patente,
                'Latitud' => $servicio->getUltLatitud(),
                'Longitud' => $servicio->getUltLongitud(),
                'Fecha' => $fecha,
                'Cliente' => 'Robinson'
            ];
        } else {
            return false;
        }
    }
}
