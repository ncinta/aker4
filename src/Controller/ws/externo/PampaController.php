<?php

namespace App\Controller\ws\externo;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\EventoManager;

class PampaController extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ACCESS_DENIED = 403;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
    );

    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $tanqueManager;
    private $utils;
    private $referencias;
    private $tipoEventoManager;
    private $eventoManager;


    function __construct(
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        TipoEventoManager $tipoEvento,
        EventoManager $eventoManager,
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->utils = $utilsManager;
        $this->tipoEventoManager = $tipoEvento;
        $this->eventoManager = $eventoManager;
    }

    private $hashes = [
        '127.0.0.1' => 'gt135e5m6EtRAraoRJvUKSfd3453F3fqztM6VD1Qy',
    ];

    private function checkAccess($ip, $hash)
    {
        return true;
        //return strtoupper($this->hashes[$ip]) == strtoupper($hash);
    }

    /**
     * @Route("/ws/pampa/posiciones", name="webservice_externo_pampa_posiciones", methods={"GET"})
     */
    public function posicionesAction(Request $request)
    {
        $datos = [];
        $status = true;

        // Obtener el parámetro de consulta 'hash'
        $hash = $request->query->get('hash');
        $ipAddress = $request->getClientIp();
        if (!$this->checkAccess('127.0.0.1', $hash)) {
            return $this->generateResponseFail(self::STATUS_ACCESS_DENIED);
        }

        //obtener todos los eventos REPLY_PAMPA
        $tipoEvento = $this->tipoEventoManager->findByCodename('REPLY_PAMPA');
        if ($tipoEvento) {
            $eventos = $this->eventoManager->findByTipo($tipoEvento);
            //para cada evento recorrer los servicios
            foreach ($eventos as $evento) {
                foreach ($evento->getServicios() as $servicio) {
                    $datos[] = $this->getDataServicio($servicio);
                }
            }
        }
        $status = self::STATUS_OK;
        return $this->generateResponse($status, $datos);
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }


    private function generateResponseFail($status)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );

        return new Response(null, $status, $headers);
    }


    private function getTipoServicio($servicio)
    {
        if (in_array($servicio->getTipoServicio()->getId(), [4, 8, 11, 16, 19, 20, 24,])) {
            return 'liviano';
        } elseif (in_array($servicio->getTipoServicio()->getId(), [5, 6, 9, 10, 12, 22, 26, 27, 28, 29, 30, 32, 32, 33, 43, 44, 45, 46, 47, 48])) {
            return 'pesado';
        } elseif (in_array($servicio->getTipoServicio()->getId(), [7, 23 ])) {
            return 'transporte pasajeros';
        } else {
            return 'Otro';
        }
    }

    private function getDataServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {

        $patente = null;
        if (!is_null($servicio->getVehiculo()) and !is_null($servicio->getVehiculo()->getPatente())) {
            $patente = $servicio->getVehiculo()->getPatente();
        }
        $datos = array(
            'cpuID' => $servicio->getId(),
            'vehiculo' => $servicio->getNombre(),
            'patente' => $patente,
            'fechaHora' => !is_null($servicio->getUltFechahora()) ? $this->utils->UTC2local($servicio->getUltFechahora(), $servicio->getOrganizacion()->getTimeZone(), 'Y-m-d\TH:i:s') : null,
            'latitud' => $servicio->getUltLatitud(),
            'longitud' => $servicio->getUltLongitud(),
            'velocidad' => $servicio->getUltVelocidad(),
            'direccion' => $servicio->getUltDireccion(),
            'empresaNombre' => $servicio->getOrganizacion()->getNombre(),
            'tipoVehiculo' => $this->getTipoServicio($servicio),
        );
        //empiezo a sacar los datos de la ulTrama
        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['contacto'])) {
            $datos['contacto'] = $ultTrama['contacto'];
            $datos['ralenti'] = $ultTrama['contacto'] && $servicio->getUltVelocidad() == 0;
        }

        $datos['operadorNombre'] = '';
        $datos['llaveOperador'] = '';
        $datos['operadorId'] = 'dni';
        if ($servicio->getChofer()) {
            $datos['operadorNombre'] = $servicio->getChofer()->getNombre();
            $datos['llaveOperador'] = $servicio->getChofer()->getDallas();
            $datos['operadorId'] = $servicio->getChofer()->getDocumento();
        }

        //determinacion del "evento"
        $datos['evento'] = 'Actividad';


        return $datos;
    }
}
