<?php

namespace App\Controller\ws;

use App\Model\app\OrganizacionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\ReferenciaManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Referencia;

class ReferenciaController extends AbstractController
{


    private $referencias = null;
    private $referenciaManager = null;
    private $organizacionManager = null;

    const STATUS_OK = 0;
    const ERROR = 1;
    const ERROR_FALTAN_DATOS = 2;
    const ERROR_REFERENCIA_NO_ENCONTRADA = 3;
    const ERROR_ORGANIZACION = 4;
    const ERROR_APIKEY = 5;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR => 'Error',
        self::ERROR_FALTAN_DATOS => 'Error, faltan datos',
        self::ERROR_REFERENCIA_NO_ENCONTRADA => 'Error, referencia no encontrada',
        self::ERROR_ORGANIZACION => 'Error, organizacion no encontrada',
        self::ERROR_APIKEY => 'Error,apikey inválida',

    );



    function __construct(
        ReferenciaManager $referenciaManager,
        OrganizacionManager $organizacionManager

    ) {
        $this->referenciaManager = $referenciaManager;
        $this->organizacionManager = $organizacionManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/ws/referencias", name="webservice_referencias"), methods={"GET"})     
     */
    public function referenciasAction(Request $request)
    {
        //$apiCodeUsr = $request->headers->get('apicode');
        //dd($apiCodeUsr);        
        $apikey = $request->get('apiKey');
        //fmto = 1 arma el array sin enumaracion, fmto=2 los arma enumerados
        $fmto = is_null($request->get('fmto')) ? false : (intval($request->get('fmto')));
        $status = true;
        $datos = null;

        if (is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    $referencias = $this->referenciaManager->findAllVisibles($organizacion);
                    $datos = array();
                    $i = 0;
                    foreach ($referencias as $referencia) {
                        $status = self::STATUS_OK;
                        if ($status == self::STATUS_OK && !is_null($referencia)) {
                            $data = $this->referenciaManager->dataApiReferencia($referencia);
                            $grupoRef = $this->addGrupoReferencia($referencia);
                            if ($fmto == 2) {
                                $datos[] = array_merge($data, ['grupo_referencia' => $grupoRef]);
                                $i++;
                            } else {  //este es el formato estandart.
                                $datos[$referencia->getId()] = array_merge($data, ['grupo_referencia' => $grupoRef]);
                            }
                        } else {
                            $status = self::ERROR_REFERENCIA_NO_ENCONTRADA;
                        }
                    }
                } else {
                    $status = self::ERROR_ORGANIZACION;
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }


        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        ));
    }

    private function addGrupoReferencia($referencia)
    {
        $grupos = null;
        foreach ($referencia->getGrupos() as $grupo) {
            $grupos[] = array(
                'id' => $grupo->getId(),
                'nombre' => $grupo->getNombre()
            );
        }

        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');

        return $grupos;
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }
}
