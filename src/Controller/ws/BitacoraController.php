<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Bitacora as Bitacora;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BitacoraManager;

class BitacoraController extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ERROR = 1;

    /**
     * @Route("/ws/bitacora/agente/start", name="bitacora_agente_start")
     * @Method({"GET", "POST"})
     */
    public function agentestartAction(Request $request, BitacoraManager $bitacoraManager)
    {
        $data = $request->getContent();
        $status = true;
        if ($data) {
            $form = json_decode($data, true);
            if (!is_null($form)) {
                if ($status === true) {
                    $data = array(
                        'fecha' => $form['fecha_utc'],
                        'remote_ip' => $request->server->get('REMOTE_ADDR'),
                        'server_ip' => $request->server->get('SERVER_ADDR'),
                    );
                    if ($bitacoraManager->webserviceAdd(Bitacora::EVENTO_AGENTE_INICIO, $data)) {
                        $status = self::STATUS_OK;
                    } else {
                        $status = self::STATUS_ERROR;
                    }
                }
            } else {
                $status = self::STATUS_ERROR;
            }
        } else {
            $status = self::STATUS_ERROR;
        }

        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');
        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    /**
     * @Route("/ws/bitacora/agente/stop", name="bitacora_agente_stop")
     * @Method({"GET", "POST"})
     */
    public function agentestopAction(Request $request, BitacoraManager $bitacoraManager)
    {
        $data = array(
            'remote_ip' => $request->server->get('REMOTE_ADDR'),
            'server_ip' => $request->server->get('SERVER_ADDR'),
        );
        if ($bitacoraManager->webserviceAdd(Bitacora::EVENTO_AGENTE_FIN, $data)) {
            $status = self::STATUS_OK;
        } else {
            $status = self::STATUS_ERROR;
        }
        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');
        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }
}
