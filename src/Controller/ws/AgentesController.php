<?php

namespace App\Controller\ws;

use App\Entity\MotivoTx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\EventoManager;
use App\Model\app\OrganizacionManager;

/**
 * Description of AgentesController
 *
 * @author nicolas
 */
class AgentesController extends AbstractController
{

    const STATUS_OK = 0;
    const ERROR_DE_SERVIDOR = 1;
    const SIN_DATOS_ENVIAR = 2;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_DE_SERVIDOR => 'Error de conexión',
        self::SIN_DATOS_ENVIAR => 'Sin datos para enviar',
    );
    private $tipoevManager = null;
    private $notificadorManager = null;
    private $eventoManager = null;
    private $organizacionManager = null;

    public function __construct(
        TipoEventoManager $tipoevManager,
        NotificadorAgenteManager $notificadorManager,
        EventoManager $eventoManager,
        OrganizacionManager $organizacionManager
    ) {
        $this->tipoevManager = $tipoevManager;
        $this->notificadorManager = $notificadorManager;
        $this->eventoManager = $eventoManager;
        $this->organizacionManager = $organizacionManager;
    }

    /**
     * @Route("/ws/agente/generar/evento", name="webservice_agente_evento")
     * @Method({"GET", "POST"})
     */
    public function eventoAction(Request $request)
    {
        //  http://aker.localhost:8085/ws/agente/generar/evento?codename=EV_PANICO
        //  http://aker.localhost:8085/ws/agente/generar/evento?organizacion=178

        $id = $request->get('organizacion') !== null ? intval($request->get('organizacion')) : null;  //ORGANIZACION
        $codename = $request->get('codename') !== null ? $request->get('codename') : null;  //CODENAME

        if ($id) {
            $organizacion = $this->organizacionManager->find($id);
            $eventos = $this->eventoManager->findAll($organizacion);
            //  die('////<pre>' . nl2br(var_export($organizacion->getId(), true)) . '</pre>////');
        } elseif ($codename) {
            $tipoev = $this->tipoevManager->findByCodename($codename);
            $eventos = $this->eventoManager->findByTipo($tipoev);
        } else {
            $eventos = array();
        }

        if (count($eventos) > 0) {
            // dd($eventos);
            foreach ($eventos as $evento) {
                if ($this->notificadorManager->notificar($evento, 1)) { //INSER/UPDATE
                    // return $this->generateResponse(self::STATUS_OK, $this->strResponse[self::STATUS_OK]);
                } else {
                    // return $this->generateResponse(self::ERROR_DE_SERVIDOR, $this->strResponse[self::ERROR_DE_SERVIDOR]);
                }
            }
        } else {
            return $this->generateResponse(self::SIN_DATOS_ENVIAR, $this->strResponse[self::SIN_DATOS_ENVIAR]);
        }
        return $this->generateResponse(self::STATUS_OK, 'EVENTOS -> ' . count($eventos));
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        }
        //  die('////<pre>' . nl2br(var_export(json_encode($struct), true)) . '</pre>////');
        return new Response(json_encode($struct), $st, $headers);
    }

    /**
     * funcion para cargar los motivos tx en la bd. solamente correrlo una vez
     * @Route("/ws/motivotx/load", name="webservice_motivotx_load")
     * @Method({"GET", "POST"})
     */
    public function motivotxAction(Request $request)
    {
        $em = $this->getEntityManager();
        $motivoDefault = $em->getRepository('App:MotivoTx')->findOneBy(array('code' => -1));
        if (!$motivoDefault) {
            $motivoDefault = new MotivoTx();
            $motivoDefault->setCode(-1);
            $motivoDefault->setNombre('???');
            $em->persist($motivoDefault);
        }
        $str = '';
        for ($i = 0; $i < 136; $i++) {
            $isExist = true;
            switch ($i) {
                case 0:
                    $str = "Motivo: 0";
                    break;
                case 1:
                    $str = "N/A";
                    break;
                case 2:
                    $str = "Tx Mov.";
                    break;
                case 3:
                    $str = "Tx Det.";
                    break;
                case 4:
                    $str = "x Dist.";
                    break;
                case 5:
                    $str = "Rumbo";
                    break;
                case 6:
                    $str = "Solicitud de Posicion";
                    break;
                case 7:
                    $str = "Veloc. Inf.";
                    break;
                case 8:
                    $str = "Veloc. Exc.";
                    break;
                case 9:
                    $str = "Dallas";
                    break;
                case 10:
                    $str = "Inp1";
                    break;
                case 11:
                    $str = "Inp2";
                    break;
                case 12:
                    $str = "Inp3";
                    break;
                case 13:
                    $str = "Temp. Baja";
                    break;
                case 14:
                    $str = "Temp. Alta";
                    break;
                case 15:
                    $str = "Hum. Baja";
                    break;
                case 16:
                    $str = "Hum. Alta";
                    break;
                case 17:
                    $str = "Corte Ralenti";
                    break;
                case 18:
                    $str = "Corte SMS";
                    break;
                case 19:
                    $str = "Corte Telecomando";
                    break;
                case 20:
                    $str = "Ingreso Geocerca";
                    break;
                case 21:
                    $str = "Salida Geocerca";
                    break;
                case 22:
                    $str = "Deteccion Stop";
                    break;
                case 23:
                    $str = "Aceleración brusca";
                    break;
                case 24:
                    $str = "Frenada Brusca";
                    break;
                case 25:
                    $str = "Corte Antena GPS";
                    break;
                case 26:
                    $str = "Pedido de trama";
                    break;
                case 27:
                    $str = "Corte alimentacion";
                    break;
                case 28:
                    $str = "Batería Ext. Conectada";
                    break;
                case 29:
                    $str = "Contacto ON";
                    break;
                case 30:
                    $str = "Contacto OFF";
                    break;
                case 31:
                    $str = "Solicitud trama =";
                    break;
                case 32:
                    $str = "CC: Status";
                    break;
                case 33:
                    $str = "CC: RPM Alto";
                    break;
                case 34:
                    $str = "CC: RPM Normalizado";
                    break;
                case 35:
                    $str = "CC: Temp. Alta";
                    break;
                case 36:
                    $str = "CC: Temp. Normalizada";
                    break;
                case 37:
                    $str = "CC: Subida Nivel Tanque";
                    break;
                case 38:
                    $str = "CC: Bajada Nivel Tanque";
                    break;
                case 39:
                    $str = "CC: Inp1 0->1";
                    break;
                case 40:
                    $str = "CC: Inp1 1->0 ";
                    break;
                case 41:
                    $str = "CC: Inp2 0->1";
                    break;
                case 42:
                    $str = "CC: Inp2 1->0";
                    break;
                case 43:
                    $str = "CC: Inp3 0->1";
                    break;
                case 44:
                    $str = "CC: Inp3 1->0";
                    break;
                case 45:
                    $str = "CC: Temp H->L";
                    break;
                case 46:
                    $str = "CC: Temp L->H";
                    break;
                case 47:
                    $str = "CC: Pres. 0->1";
                    break;
                case 48:
                    $str = "CC: Pres. 1->0";
                    break;
                case 49:
                    $str = "De detenido a movimiento";
                    break;
                case 50:
                    $str = "Salida geocerca autonoma";
                    break;
                case 51:
                    $str = "S.O.S";
                    break;
                case 52:
                    $str = "Cumplido tiempo 0";
                    break;
                case 53:
                    $str = "Cumplido tiempo 1";
                    break;
                case 54:
                    $str = "Cumplido tiempo 2";
                    break;
                case 55:
                    $str = "Cumplido tiempo 3";
                    break;
                case 56:
                    $str = "Conexión al server";
                    break;
                case 57:
                    $str = "Batería ppal baja";
                    break;
                case 58:
                    $str = "Estacionamiento";
                    break;
                case 59:
                    $str = "Cronómetro 0";
                    break;
                case 60:
                    $str = "Cronómetro 1";
                    break;
                case 61:
                    $str = "Cronómetro 2";
                    break;
                case 62:
                    $str = "Cronómetro 3";
                    break;
                case 65:
                    $str = "Contador 0";
                    break;
                case 66:
                    $str = "Contador 1";
                    break;
                case 67:
                    $str = "Contador 2";
                    break;
                case 68:
                    $str = "Contador 3";
                    break;
                case 69:
                    $str = "Horómetro";
                    break;
                case 72:
                    $str = "Periódico";
                    break;
                case 76:
                    $str = "Sleep";
                    break;
                case 77:
                    $str = "Display: ACK";
                    break;
                case 78:
                    $str = "Display: Aceptar";
                    break;
                case 79:
                    $str = "Display: Rechazar";
                    break;
                case 80:
                    $str = "Display: Timeout";
                    break;
                case 81:
                    $str = "Display: Conectado";
                    break;
                case 82:
                    $str = "Display: Desconectado";
                    break;
                case 83:
                    $str = "Display: Test";
                    break;
                case 84:
                    $str = "CC: Conexión cc";
                    break;
                case 85:
                    $str = "CC: Desconexión cc";
                    break;
                case 86:
                    $str = "GPS energizado";
                    break;
                case 87:
                    $str = "Reconexión GPRS";
                    break;
                case 88:
                    $str = "Intervalo de transm. en pánico";
                    break;
                case 89:
                    $str = "Config. entrada";
                    break;
                case 90:
                    $str = "Petición servicio";
                    break;
                case 91:
                    $str = "Posic. OK ";
                    break;
                case 92:
                    $str = "Pánico desactivado";
                    break;
                case 93:
                    $str = "Entrada 4 activada";
                    break;
                case 94:
                    $str = "Entrada 4 abierta";
                    break;
                case 95:
                    $str = "G-Sensor detenido";
                    break;
                case 96:
                    $str = "Antirobo alarmado";
                    break;
                case 97:
                    $str = "Fallo de accesorio";
                    break;
                case 98:
                    $str = "Antena GPS Ok";
                    break;
                case 99:
                    $str = "Packet Ok accesorio wireless";
                    break;
                case 100:
                    $str = "Salida 1 activada";
                    break;
                case 101:
                    $str = "Salida 1 desactivada";
                    break;
                case 102:
                    $str = "Salida 2 activada";
                    break;
                case 103:
                    $str = "Salida 2 desactivada";
                    break;
                case 104:
                    $str = "Salida 3 activada";
                    break;
                case 105:
                    $str = "Salida 3 desactivada";
                    break;
                case 106:
                    $str = "Velocidad normalizada";
                    break;
                case 107:
                    $str = "Error batería backup";
                    break;
                case 108:
                    $str = "Ok batería backup";
                    break;
                case 109:
                    $str = "Posic. reenviada por falla 1er intento";
                    break;
                case 110:
                    $str = "Posic. solicitada por sms";
                    break;
                case 111:
                    $str = "Violación del módulo";
                    break;
                case 112:
                    $str = "Límite frente/trasero alcanzado";
                    break;
                case 113:
                    $str = "Límite lateral alcanzado";
                    break;
                case 114:
                    $str = "Límite vertical alcanzado";
                    break;
                case 115:
                    $str = "TRAMA = SMS";
                    break;
                case 116:
                    $str = "MXT apagado";
                    break;
                case 117:
                    $str = "Antirrobo estado normal";
                    break;
                case 118:
                    $str = "Jamming ON";
                    break;
                case 119:
                    $str = "Jamming OFF";
                    break;
                case 120:
                    $str = "Vel. en neutro";
                    break;
                case 121:
                    $str = "Fallo GPS";
                    break;
                case 122:
                    $str = "Distancia alcanzada";
                    break;
                case 123:
                    $str = "Error Aliment. y GPS";
                    break;
                case 124:
                    $str = "Petición por AGPS";
                    break;
                case 130:
                    $str = "Exeso detención con ignición";
                    break;
                case 131:
                    $str = "Movimiento indebido";
                    break;
                case 132:
                    $str = "Habilitación motor por ralenti";
                    break;
                case 133:
                    $str = "Encendido de equipo";
                    break;
                case 134:
                    $str = "S.O.S. off";
                    break;
                case 135:
                    $str = "Giros Bruscos";
                    break;
                default:
                    $isExist = false;   // si i no está en ningun caso que no grabe nada 
            }
            $motivotx = $em->getRepository('App:MotivoTx')->findOneBy(array('code' => $i));
            if (!$motivotx && $isExist) {
                $motivotx = new MotivoTx();
                $motivotx->setNombre($str);
                $motivotx->setCode($i);
                $em->persist($motivotx);
            }
        }
        $em->flush();
        //case -1 : $str = isset($trama["motivo_original"]) ? "Orig: ".$trama["motivo_original"] : '???';
        return $this->generateResponse(self::STATUS_OK, 'OK');
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
