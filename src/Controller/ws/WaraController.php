<?php

namespace App\Controller\ws;

use App\Model\app\EquipoManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\PrestacionManager;
use App\Model\app\UsuarioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\GeocoderManager;
use App\Model\app\UtilsManager;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class WaraController extends AbstractController
{

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;
    const STATUS_ERROR_DATA = 7;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
        self::STATUS_ERROR_DATA => 'Error de data'
    );

    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $tanqueManager;
    private $usuarioManager;
    private $prestacionManager;
    private $notificadorAgenteManager;
    private $geocoderManager;
    private $utils;
    private $equipoManager;
    private $logger;
    private $logear = true;

    function __construct(
        OrganizacionManager $organizacionManager,
        TanqueManager $tanqueManager,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        PrestacionManager $prestacionManager,
        GeocoderManager $geocoderManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UtilsManager $utilsManager,
        EquipoManager $equipoManager,
        LoggerInterface $logger
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->tanqueManager = $tanqueManager;
        $this->usuarioManager = $usuarioManager;
        $this->prestacionManager = $prestacionManager;
        $this->geocoderManager = $geocoderManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->utils = $utilsManager;
        $this->equipoManager = $equipoManager;
        $this->logger = $logger;
    }


    private function obtenerValores($servicio)
    {
        if (!is_null($servicio->getFechaUltCambio())) {
            if ($servicio->getCentrocosto()) {   //entro si tengo centro de costo asignado
                $horasUso = $this->prestacionManager->contarTiempoUso($servicio, $servicio->getFechaUltCambio(), new \Datetime());
                return intval($horasUso);
            } else {
                $now = new \DateTime();  //ahora.
                $diff = date_diff($now, $servicio->getFechaUltCambio(), true);
                return (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i;
            }
            //$total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;
        }
    }

    /**
     * @Route("/ws/wara/getdata", name="servicio_wara_getdata", methods={"GET"})
     */
    public function getDataWaraAction(Request $request)
    {    //obtener el token para todos los envios
        $token =  $this->notificadorAgenteManager->getTokenWara();
        if ($token) {  //tengo token entonces debo enviar, sino no hacer nada.            
            $getData = $this->notificadorAgenteManager->getDataWara($token);
            if (!$getData) {
                $this->logWara('Result -> ' . 'Hay token pero no trae los datos');
            } else {
                $data = json_decode($this->notificadorAgenteManager->getDataWara($token));              
                foreach ($data->assets as $data_servicio) {
                    if (isset($data_servicio->unitNumber)) {
                        $equipo = $this->equipoManager->findByMdmid($data_servicio->unitNumber);
                        if ($equipo != null) {
                            $servicio = $equipo->getServicio();
                            $odometro = intval($data_servicio->odometer) * 1000;

                            $horometro = explode(".", $data_servicio->hourMeter);
                            $horas = isset($horometro[0]) ? $horometro[0] * 60 : 0;
                            // Obtén la parte decimal del número
                            $minutos = isset($horometro[1]) ? $horometro[1] : 0;

                            $horometro = $horas + $minutos;

                            $servicio->setUltOdometro($odometro);
                            $servicio->setUltHorometro($horometro);
                            $save = $this->getEntityManager()->persist($servicio);
                        }
                    }
                }
            }
            $this->getEntityManager()->flush();
            $this->logWara('Result -> ' . 'Datos Almacenados. In your Face Wara!');
            $status = self::STATUS_OK;
        } else {
            $this->logWara('Sin token');
            $status = self::STATUS_ERROR_DATA;
        }

        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function logWara($str)
    {
        if ($this->logear) {
            $this->logger->notice('dataWara | ' . $str);
        }
    }
}