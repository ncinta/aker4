<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\UsuarioManager;
use App\Model\app\TipoVehiculoManager;
use App\Model\app\TipoServicioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class TipoVehiculoController extends AbstractController
{

    private $tipoVehiculoManager;
    private $tipoServicioManager;
    private $usuarioManager;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );


    function __construct(
        TipoVehiculoManager $tipoVehiculoManager, 
        TipoServicioManager $tipoServicioManager, 
        UsuarioManager $usuarioManager)
    {
        $this->tipoVehiculoManager = $tipoVehiculoManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->usuarioManager = $usuarioManager;        
    }


    /**
     * @Route("/ws/tipovehiculo/list/{phone}/{code}", name="webservice_tipovehiculo_list")
     * @Method({"GET", "POST"})
     */
    public function tipoVehiculoAction($phone, $code)
    {
        $datos = null;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usuario = $this->usuarioManager->findByApicode($phone, $code);
            if ($usuario) {
                $tipoVehiculos = $this->tipoVehiculoManager->findAll();               
                foreach ($tipoVehiculos as $tipo) {
                    $datos[] = array(
                        'id' => $tipo->getId(),
                        'nombre' => $tipo->getTipo()
                    );
                }
            }
            $status = is_null($usuario) ? self::ERROR_APIKEY : self::STATUS_OK;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => null
        );

        if (!is_null($datos)) {
            $respuesta['data'] = $datos;
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }

    private function generateResponse($struct)
    {
        return new Response(json_encode($struct), 200, array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        ));
    }

    /**
     * @Route("/ws/tiposervicio/list/{phone}/{code}", name="webservice_tiposervicio_list"), method({"GET"})
     */
    public function tipoServicioAction($phone, $code)
    {
        $datos = null;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usuario = $this->usuarioManager->findByApicode($phone, $code);
            if ($usuario) {
                $tipoServicio = $this->tipoServicioManager->findAll();                
                foreach ($tipoServicio as $tipo) {
                    $datos[] = array(
                        'id' => $tipo->getId(),
                        'nombre' => $tipo->getNombre(),
                        'icono' => $tipo->getPathIcono(),
                        'path' => $tipo->getWebPath()
                    );
                }
            }
            $status = is_null($usuario) ? self::ERROR_APIKEY : self::STATUS_OK;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => null
        );

        if (!is_null($datos)) {
            $respuesta['data'] = $datos;
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }
}
