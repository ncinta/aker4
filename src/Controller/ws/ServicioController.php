<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\PrestacionManager;
use App\Model\app\UsuarioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\GeocoderManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class ServicioController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );
    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $tanqueManager;
    private $usuarioManager;
    private $prestacionManager;
    private $notificadorAgenteManager;
    private $geocoderManager;
    private $utils;

    function __construct(
        OrganizacionManager $organizacionManager,
        TanqueManager $tanqueManager,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        PrestacionManager $prestacionManager,
        GeocoderManager $geocoderManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UtilsManager $utilsManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->tanqueManager = $tanqueManager;
        $this->usuarioManager = $usuarioManager;
        $this->prestacionManager = $prestacionManager;
        $this->geocoderManager = $geocoderManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->utils = $utilsManager;
    }

    /**
     * @Route("/ws/servicio", name="webservice_servicio"), methods={"GET"})
     */
    public function servicioAction(Request $request)
    {
        $patente = $request->get('patente');
        $apikey = $request->get('apiKey');
        $cercania=$this->parseParamBool($request->get('cercania'));
        $domicilio = $this->parseParamBool($request->get('domicilio'));
        $status = true;
        $datos = null;
        if (is_null($patente) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                $servicio = $this->servicioManager->findByPatente($patente);
                if ($status === true && !is_null($servicio)) {
                    $datos = array(
                        $patente => $this->getDataServicio($servicio, $cercania, $domicilio)
                    );
                    $status = self::STATUS_OK;
                } else {
                    $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                }
            } else {
                $status = self::ERROR_APIKEY;
                //die('////<pre>' . nl2br(var_export($apikey, true)) . '</pre>////');
            }
        }

        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }

    /**
     * @Route("/ws/servicios", name="webservice_servicios"), methods={"GET"})
     */
    public function serviciosAction(Request $request)
    {
        $patentes = $request->get('patentes');

        $apikey = $request->get('apiKey');
        $cercania=$this->parseParamBool($request->get('cercania'));
        $domicilio = $this->parseParamBool($request->get('domicilio'));
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $status = true;
        $datos = null;
        if (is_null($patentes) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            //ejemplo de serializacion y urlencode
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                if (strpos($patentes, '\x22') > -1) {
                    $patentes = unserialize(str_replace('\x22', '"', $patentes));
                } elseif ($this->is_json($patentes)) {
                    $patentes = json_decode($patentes);
                } else {
                    $patentes = unserialize(urldecode($patentes));
                }
                //  die('////<pre>' . nl2br(var_export($patentes, true)) . '</pre>////');
                $datos = array();
                foreach ($patentes as $i => $patente) {
                    $status = self::STATUS_OK;
                    $servicio = $this->servicioManager->findByPatente($patente);
                    if ($status == self::STATUS_OK && !is_null($servicio)) {
                        $datos[$patente] = $this->getDataServicio($servicio, $cercania, $domicilio);
                    } else {
                        // $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                    }
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }

        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }

    private function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @Route("/ws/flota", name="webservice_flota"), methods={"GET"})     
     */
    public function flotaAction(Request $request)
    {
        //$apiCodeUsr = $request->headers->get('apicode');
        //dd($apiCodeUsr);        
        $apikey = $request->get('apiKey');
        //die('////<pre>' . nl2br(var_export($apikey, true)) . '</pre>////');
        $cercania=$this->parseParamBool($request->get('cercania'));
        $domicilio = $this->parseParamBool($request->get('domicilio'));
        $getId = false;
        if ($request->get('id')) {
            $getId = $request->get('id');
        }

        //fmto = 1 arma el array sin enumaracion, fmto=2 los arma enumerados
        $fmto = is_null($request->get('fmto')) ? false : (intval($request->get('fmto')));
        $contacto = -1; //estado que no viene por parametro
        if ($request->get('contacto') != null) {
            $contacto = $request->get('contacto') == 'true' ? true : false;
        }

        //die('////<pre>' . nl2br(var_export($contacto, true)) . '</pre>////');
        $grupo_servicio = is_null($request->get('grupo_servicio')) ? '' : $request->get('grupo_servicio');
        $tipo_vehiculo = is_null($request->get('tipo_vehiculo')) ? '' : $request->get('tipo_vehiculo');
        $arr_grupo_servicio  = eval("return " . $grupo_servicio . ";"); //paso el string a array
        $arr_tipo_vehiculo  = eval("return " . $tipo_vehiculo . ";"); //paso el string a array
        $status = true;
        $datos = null;
        if (is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    $servicios = $this->servicioManager->findByTipoAndGrupo($organizacion, -1, $arr_tipo_vehiculo, $arr_grupo_servicio);
                    $datos = array();
                    foreach ($servicios as $servicio) {
                        $status = self::STATUS_OK;
                        if ($status == self::STATUS_OK && !is_null($servicio)) {
                            if ($fmto == 2) {
                                $datos[] = $this->getDataServicio($servicio, $cercania, $domicilio, $getId);
                            } else {  //este es el formato estandart.
                                $datos[$servicio->getNombre()] = $this->getDataServicio($servicio, $cercania, $domicilio, $getId);
                            }
                        } else {
                            $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                        }
                    }
                } else {
                    $status = self::ERROR_ORGANIZACION;
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        );
        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        ));
    }

    private function parseParamBool($valor) {
        $result=false;
        if (!is_null($valor)) {
            if ($valor == '1' || strtolower($valor == 'true')) {
                $result = true;
            } else {
                $result = false;
            }
        } 
        return $result;
    }

    /**
     * @Route("/ws/flota/{phone}/{code}", name="webservice_app_flota"), methods={"GET"}) 
     */
    public function appflotaAction(Request $request, $phone, $code)
    {
        $usr = null;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $this->usuarioManager->findByApicode($phone, $code);
            $status = is_null($usr) ? self::ERROR_APIKEY : self::STATUS_OK;
        }

        $cercania=$this->parseParamBool($request->get('cercania'));
        $domicilio = $this->parseParamBool($request->get('domicilio'));

        $getId = false;
        if ($request->get('id')) {
            $getId = $request->get('id');
        }

        //fmto = 1 arma el array sin enumaracion, fmto=2 los arma enumerados
        $fmto = is_null($request->get('fmto')) ? false : (intval($request->get('fmto')));       
        
        $datos = array();
        if ($usr != null) {
            $servicios = $this->servicioManager->findEnabledByUsuario($usr, null, false);
            // die('////<pre>'.nl2br(var_export(count($servicios), true)).'</pre>////');
            foreach ($servicios as $servicio) {
                $status = self::STATUS_OK;
                if ($status == self::STATUS_OK && !is_null($servicio)) {
                    if ($fmto == 2) {
                        $datos[] = $this->getDataServicio($servicio, $cercania, $domicilio, $getId);                       
                    } else {  //este es el formato estandart.
                        $datos[$servicio->getNombre()] = $this->getDataServicio($servicio, $cercania, $domicilio, $getId);
                    }
                } else {
                    $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                }
            }
        }

        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        ));
        //die('////<pre>'.nl2br(var_export('$request', true)).'</pre>////');
        //return new Response(json_encode($respuesta), 200, $headers);      
        //return $this->generateResponse($status, $struct);
    }

    private function getUrlServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {
        $infoCerca = null;

        $datos = array(
            'icono_servicio' => '/' . $servicio->getTipoServicio()->getWebPath(),
            'icono_posicion' => $this->gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad())
        );

        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['contacto']) && $ultTrama['contacto']) {
            $datos['icono_contacto'] = 'images/key.png';
        }

        //bateria
        if ($servicio->getUltBateria() <= 20) {
            $datos['icono_bateria'] = 'images/bateria5.png';
        } elseif ($servicio->getUltBateria() > 20 and $servicio->getUltBateria() <= 40) {
            $datos['icono_bateria'] = 'images/bateria4.png';
        } elseif ($servicio->getUltBateria() > 40 and $servicio->getUltBateria() <= 60) {
            $datos['icono_bateria'] = 'images/bateria3.png';
        } elseif ($servicio->getUltBateria() > 60 and $servicio->getUltBateria() <= 80) {
            $datos['icono_bateria'] = 'images/bateria2.png';
        } elseif ($servicio->getUltBateria() > 80 and $servicio->getUltBateria() <= 100) {
            $datos['icono_bateria'] = 'images/bateria1.png';
        } else {
            $servicio->getUltBateria();
        }

        return $datos;
    }

    /**
     * @Route("/ws/servicio/{phone}/{code}/{id}", name="webservice_app_servicio"), methods={"GET"})     
     */
    public function appservicioAction(Request $request, $phone, $code, $id)
    {
        $usr = null;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $this->usuarioManager->findByApicode($phone, $code);
            $status = is_null($usr) ? self::ERROR_APIKEY : self::STATUS_OK;
        }
        $datos = array();

        if ($usr != null) {
            $servicio = $this->servicioManager->find($id);
            if ($usr->getOrganizacion()->getId() != $servicio->getOrganizacion()->getId()) {
                $status = self::ERROR_ORGANIZACION;
            } else {
                $status = self::STATUS_OK;
                if ($status == self::STATUS_OK && !is_null($servicio)) {
                    $datos[$servicio->getNombre()] = $this->getDataServicio($servicio, true, true, false);
                    $datos[$servicio->getNombre()]['url'] = $this->getUrlServicio($servicio, true, true, false);
                } else {
                    $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                }
            }
        }

        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        ));
        //die('////<pre>'.nl2br(var_export('$request', true)).'</pre>////');
        //return $this->generateResponse($status, $struct);
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }

    /**
     * @Route("/ws/servicio/tanque", name="webservice_servicio_tanque")
     * @Method({"GET", "POST"})
     */
    public function serviciotanqueAction(Request $request)
    {

        $apikey = $request->get('apiKey');
        $id = $request->get('id');
        $porcentaje = $request->get('porcentaje');
        $status = true;
        $datos = null;
        if (is_null($apikey) || is_null($id) || is_null($porcentaje)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $servicio = $this->servicioManager->find($id);
            if (is_null($servicio)) {
                $status = self::ERROR_PATENTE_NO_ENCONTRADA;
            } else {
                if ($this->tanqueManager->loadMediciones($servicio, $porcentaje)) {
                    $datos = array(
                        'id' => $servicio->getId(),
                        'nombre' => $servicio->getNombre(),
                        'tanque' => $this->tanqueManager->getLitros($porcentaje)
                    );
                    $status = self::STATUS_OK;
                } else {
                    $status = self::ERROR_MEDICIONES;
                }
            }
        }

        return $this->generateResponse($status, array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        ));
    }

    private function getDataServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {
        $infoCerca = null;
        if (!is_null($cercania) && $cercania && !is_null($servicio->getEquipo())) {
            if (is_null($this->referencias)) {
                $this->referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
            }
            if ($this->referencias && !is_null($servicio->getUltLatitud()) && !is_null($servicio->getUltLongitud())) {
                $cercanas = $this->servicioManager->buscarCercaniaPosicion($servicio->getUltLatitud(), $servicio->getUltLongitud(), $this->referencias, 1);
                if (is_null($cercanas[0])) {
                    $infoCerca = null;
                } else {
                    $infoCerca = array(
                        'referencia' => $cercanas[0]['id'],
                        'nombre' => $cercanas[0]['nombre'],
                        'texto' => $cercanas[0]['leyenda'],
                        'icono' => !is_null($cercanas[0]['icono']) ? $cercanas[0]['icono'] : null,
                    );
                }
            }
        }


        // $domicilio = null;   ///para ver error en direccion
        //'ult_reporte' => $strFecha,

        $datos = array(
            'id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'ult_reporte' => !is_null($servicio->getUltFechahora()) ? date_format($servicio->getUltFechahora(), 'Y-m-d H:i:s') : null,
            'ult_fecha' => !is_null($servicio->getUltFechahora()) ? $this->utils->UTC2local($servicio->getUltFechahora(), $servicio->getOrganizacion()->getTimeZone(), 'Y-m-d H:i:s') : null,
            'ult_latitud' => $servicio->getUltLatitud(),
            'ult_longitud' => $servicio->getUltLongitud(),
            'ult_odometro' => $servicio->getUltOdometro(),
            'ult_horometro' => $servicio->formatHorometro(),
            'ult_velocidad' => $servicio->getUltVelocidad(),
            'ult_direccion' => $servicio->getUltDireccion(),
            'ult_bateria' => $servicio->getUltBateria(),
            'domicilio' => is_null($domicilio) || !$domicilio ? null : $this->getDomicilio($servicio),
            'cercania' => $infoCerca,
        );
        if (!is_null($getId) && $getId) {
            $datos['id'] = $servicio->getId();
        }
        //agrego los datos del grupo
        $grupos = $this->getGrupos($servicio);
        if ($grupos) {
            $datos['grupos'] = $grupos;
        }

        //agrego la clase del servicio
        $datos['clase_vehiculo'] = array(
            'id' => $servicio->getTipoServicio()->getId(),
            'nombre' => $servicio->getTipoServicio()->getNombre(),
            'url_icono' => $servicio->getTipoServicio()->getWebPath()
        );

        if (!is_null($servicio->getVehiculo())) {
            $datos['patente'] = $servicio->getVehiculo()->getPatente() != null ? $servicio->getVehiculo()->getPatente() : '---';
            $datos['marca'] = $servicio->getVehiculo()->getMarca() != null ? $servicio->getVehiculo()->getMarca() : '---';
            $datos['modelo'] = $servicio->getVehiculo()->getModelo() != null ? $servicio->getVehiculo()->getModelo() : '---';
            $datos['anioFabricacion'] = $servicio->getVehiculo()->getAnioFabricacion() != null ?  $servicio->getVehiculo()->getAnioFabricacion() : '---';
            //agrego los datos del tipo de vehiculo
            if (!is_null($servicio->getVehiculo()->getTipoVehiculo())) {
                $datos['tipo_vehiculo'] = array(
                    'id' => $servicio->getVehiculo()->getTipoVehiculo()->getId(),
                    'nombre' => $servicio->getVehiculo()->getTipoVehiculo()->getTipo()
                );
            }
        }

        //empiezo a sacar los datos de la ulTrama
        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['canbusData'])) {
            $can = $ultTrama['canbusData'];
            if ($servicio->getCanbus()) {
                $datos['canbus'] = $can;
            }

            if ($servicio->getMedidorCombustible()) {
                $datos['tanque'] = $this->tanqueManager->getTanque($servicio, $ultTrama);
                if (!is_null($servicio->getVehiculo())) {
                    $datos['tanque']['litrosTotalTanque'] = intval($servicio->getVehiculo()->getLitrosTanque());
                }
            }
        }
        if (isset($ultTrama['contacto'])) {
            $datos['ult_contacto'] = $ultTrama['contacto'];
        }
        foreach ($servicio->getSensores() as $sensor) {
            // dd($sensor->getId());
            if ($sensor->getModeloSensor()->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                if ($sensor->getUltFechahora() != null) {
                    $sensores[$sensor->getSerie()] = array(
                        'nombre' => $sensor->getNombre(),
                        'valor' => round($sensor->getUltValor()),
                        'fecha' => !is_null($sensor->getUltFechahora()) ? date_format($sensor->getUltFechahora(), 'Y-m-d H:i:s') : null,
                        'bateria' => $sensor->getUltBateria(),
                    );
                }
            } else {  //no es seriado
                $campo = $sensor->getModeloSensor()->getCampoTrama();
                if (isset($ultTrama[$campo])) {
                    $sensores[$campo] = array(
                        'nombre' => $sensor->getNombre(),
                        'valor' => $ultTrama[$campo],
                        'title' => sprintf('%s (%d a %d %s)', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                    );
                }
            }
        }
        if (isset($sensores)) {
            $datos['sensores'] = $sensores;
        }
        if ($servicio->getEntradasDigitales() === true) {
            foreach ($servicio->getInputs() as $input) {
                $valor = isset($ultTrama[$input->getModeloSensor()->getCampoTrama()]) ? $ultTrama[$input->getModeloSensor()->getCampoTrama()] : false;
                //aca armo la leyenda teneindo en cuenta si hay icono o no.
                $datos['inputs'][$input->getModeloSensor()->getCampoTrama()] = array(
                    'nombre' => $input->getNombre(),
                    'valor' => $valor,
                    'leyenda' => $input->getAbbr() . ': ' . ($valor ? 'ON' : 'OFF'),
                    'icono' => $input->getIcono()
                );
            }
        }

        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');

        return $datos;
    }

    private function getDomicilio($servicio)
    {
        if(is_null($servicio->getUltLatitud()) && is_null($servicio->getUltLongitud())&& is_null($servicio->getEquipo())){
            return '---';
        }
        return $this->geocoderManager->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud(), $servicio->getOrganizacion());
    }

    private function getGrupos($servicio)
    {
        $grupos = null;
        if ($servicio->getGrupos()) {
            foreach ($servicio->getGrupos() as $grp) {
                $grupos[] = array(
                    'id' => $grp->getId(),
                    'nombre' => $grp->getNombre()
                );
            }
        }
        return $grupos;
    }
    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }

    /**
     * @Route("/ws/servicio/actualizarvalores", name="webservice_servicio_actualizarvalores")
     * @Method({"GET", "POST"})
     */
    public function actvaloresAction(Request $request)
    {
        $orgs = array(
            $this->organizacionManager->find(179),
            $this->organizacionManager->find(1457)
        );
        foreach ($orgs as $org) {
            if ($this->organizacionManager->isModuloEnabled($org, 'CLIENTE_PRESTACIONES')) {
                foreach ($org->getServicios() as $servicio) {
                    if ($servicio->getEstado() == 0 && !is_null($servicio->getCentrocosto())) {
                        // die('////<pre>' . nl2br(var_export($servicio->getFechaUltCambio(), true)) . '</pre>////');
                        if (!is_null($servicio->getFechaUltCambio())) {
                            $now = new \DateTime();  //ahora.
                            $horometro = $servicio->getUltHorometro() + $this->obtenerValores($servicio);
                            $servicio = $this->servicioManager->updateValores($servicio->getId(), $horometro, $now, null);
                           // $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);
                        }
                    }
                }
            }
        }

        return new Response(json_encode(array(
            'stastus' => 'ok',
        )));
    }

    private function obtenerValores($servicio)
    {
        if (!is_null($servicio->getFechaUltCambio())) {
            if ($servicio->getCentrocosto()) {   //entro si tengo centro de costo asignado
                $horasUso = $this->prestacionManager->contarTiempoUso($servicio, $servicio->getFechaUltCambio(), new \Datetime());
                return intval($horasUso);
            } else {
                $now = new \DateTime();  //ahora.
                $diff = date_diff($now, $servicio->getFechaUltCambio(), true);
                return (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i;
            }
            //$total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h;
        }
    }



    /**
     * @Route("/ws/getimage", name="ws_servicio_getimage")
     * @Method({"GET", "POST"})
     */
    public function getimage(Request $request): Response
    {

        $projectDir = $this->getParameter('kernel.project_dir');
        $absolutePath = $projectDir . '/public' . $request->get('image');
        // die('////<pre>' . nl2br(var_export($absolutePath, true)) . '</pre>////');
        $headers = array(
            'Content-Type' => 'image/png',
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Credentials', 'true',

        );


        return new Response(file_get_contents($absolutePath), 200, $headers);
    }
}
