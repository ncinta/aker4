<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BackendWsManager;
use App\Model\app\ServicioManager;
use App\Model\app\UsuarioManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HistoricoController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 6;
    const ERROR_FECHA_INVALIDA = 7;
    const ERROR_MEDICIONES = 8;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_FECHA_INVALIDA => 'Fechas invalidas',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    private function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }

    /**
     * @Route("/ws/historico/{phone}/{code}/{id}/{desde}/{hasta}", name="webservice_app_historico")
     * @Method({"GET", "POST"})
     */
    public function apphistoricoAction(
        $phone,
        $code,
        $id,
        $desde,
        $hasta,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        BackendWsManager $backendwsManager,
        UtilsManager $utilsManager
    ) {
        $usr = null;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $usuarioManager->findByApicode($phone, $code);
            $status = is_null($usr) ? self::ERROR_APIKEY : self::STATUS_OK;
        }
        $datos = array();

        if ($usr != null) {
            $servicio = $servicioManager->find($id);
            if ($usr->getOrganizacion()->getId() != $servicio->getOrganizacion()->getId()) {
                $status = self::ERROR_ORGANIZACION;
            } else {
                if ($desde > $hasta || $desde == 'NaN' ||$hasta == 'NaN') {
                    $status = self::ERROR_FECHA_INVALIDA;
                } else {
                    $status = self::STATUS_OK;
                    if (!is_null($servicio)) {
                        $fdesde = new \DateTime(gmdate("d M Y H:i:s", $desde / 1000));
                        $fdesde->setTimezone(new \DateTimeZone($usr->getTimezone()));
                        //se suma una hora por el bug de la aplicacion. que seta min y seg en 0
                        $fhasta = (new \DateTime(gmdate("d M Y H:i:s", intval($hasta) / 1000)))->modify("+1 hour");
                        $fhasta->setTimezone(new \DateTimeZone($usr->getTimezone()));

                        $datos = $this->getHistorial($servicio, $fdesde, $fhasta, $backendwsManager, $utilsManager);
                    } else {
                        $status = self::ERROR_PATENTE_NO_ENCONTRADA;
                    }
                }
            }
        }
        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        );
        return $this->generateResponse($status, $respuesta);

        //die('////<pre>'.nl2br(var_export('$this->getRequest()', true)).'</pre>////');
        return $this->generateResponse($status, $respuesta);
    }

    /**
     * Obtiene el historial de posicione.
     * @param type $id  es la posicion que se quiere consultar dentro de $consulta['periodo']
     * @param type $consulta es la consulta que se pasa con todos los parametros que se quieren mostrar.
     * @param array $opciones  (sin uso)
     * @return type $historial
     */
    private function getHistorial($servicio, $desde, $hasta, $backendwsManager, $utilsManager)
    {
        $opciones = array();
        try {
            $consulta_historial = $backendwsManager->historial(
                $servicio->getId(),
                $desde,
                $hasta,
                array(),
                $opciones
            );
            //die('////<pre>'.nl2br(var_export($consulta_historial, true)).'</pre>////');
        } catch (\Exception $exc) {
            return null;
        }

        $historial = array();
        reset($consulta_historial);
        while ($trama = current($consulta_historial)) {
            next($consulta_historial);

            if (isset($trama['_id'])) {
                $trama['oid'] = $trama['_id']['$id'];
            }
            unset($trama['fecha_recepcion'], $trama['servicio_id'], $trama['fecha_guardado'], $trama['fecha_guardado'], $trama['ip_capturador'], $trama['ip_origen'], $trama['puerto_origen'], $trama['odometro_gps'], $trama['porcentaje_ack'], $trama['gsm_csq'], $trama['gsm_error'], $trama['gps_dim'], $trama['gps_error'], $trama['temperatura'], $trama['humedad'], $trama['aceleracion'], $trama['bateria_general'], $trama['bateria_externa'], $trama['bateria_externa_volts'], $trama['motivo'], $trama['alimentacion'], $trama['oid']);

            //die('////<pre>'.nl2br(var_export($trama, true)).'</pre>////');
            //se inicia el procesamiento de la trama obtenida
            //dd($trama);            
            $trama['fecha_local'] = $utilsManager->UTC2local(new \DateTime($trama['fecha']), $servicio->getOrganizacion()->getTimeZone(),'Y-m-d H:i:s');
            $trama['distancia'] = isset($trama['distancia']) ? $trama['distancia'] : '0';
            $trama['tiempo'] = isset($trama['tiempo']) ? 'T ' . $utilsManager->segundos2tiempo($trama['tiempo']) : '0';
            if (isset($trama['posicion'])) {
                $puntos_polilinea[] = array(
                    $trama['posicion']['latitud'],
                    $trama['posicion']['longitud'],
                    $trama['posicion']['direccion'],
                    $trama['posicion']['velocidad'],
                    isset($trama['oid']) ? $trama['oid'] : null
                );
                $historial[] = $trama;
            }
        }
        unset($consulta_historial);

        //die('////<pre>' . nl2br(var_export($historial, true)) . '</pre>////');
        return $historial;
    }

    private function getUrlServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {
        $infoCerca = null;
        $datos = array(
            'icono_servicio' => '/' . $servicio->getTipoServicio()->getWebPath(),
            'icono_posicion' => $this->get('calypso.gmaps')->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad())
        );
        //die('////<pre>'.nl2br(var_export($datos, true)).'</pre>////');

        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['contacto']) && $ultTrama['contacto']) {
            $datos['icono_contacto'] = 'images/key.png';
        }

        //bateria
        if ($servicio->getUltBateria() <= 20) {
            $datos['icono_bateria'] = 'images/bateria5.png';
        } elseif ($servicio->getUltBateria() > 20 and $servicio->getUltBateria() <= 40) {
            $datos['icono_bateria'] = 'images/bateria4.png';
        } elseif ($servicio->getUltBateria() > 40 and $servicio->getUltBateria() <= 60) {
            $datos['icono_bateria'] = 'images/bateria3.png';
        } elseif ($servicio->getUltBateria() > 60 and $servicio->getUltBateria() <= 80) {
            $datos['icono_bateria'] = 'images/bateria2.png';
        } elseif ($servicio->getUltBateria() > 80 and $servicio->getUltBateria() <= 100) {
            $datos['icono_bateria'] = 'images/bateria1.png';
        } else {
            $servicio->getUltBateria();
        }

        // die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');
        return $datos;
    }
}
