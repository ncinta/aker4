<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\UsuarioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Psr\Log\LoggerInterface;

class UsuarioController extends AbstractController
{

    private $logger;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 7;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    private function log($str)
    {
        $this->logger->notice('USR | ' . $str);
    }

    /**
     * @Route("/ws/login/{phone}/{code}", name="webservice_app_login")
     * @Method({"GET", "POST"})
     */
    public function loginAction($phone, $code, UsuarioManager $usuarioManager)
    {
        $this->log('login -> ' . $phone);

        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $usuarioManager->findByApicode($phone, $code);
            if ($usr) {
                $datos['usuario_id'] = $usr->getId();
                $datos['timezone'] = $usr->getTimezone();
                $datos['apiKey'] = $usr->getOrganizacion()->getApikey();
                $datos['token'] = $usr->getToken();
                $datos['roles'] = $usr->getRoles();
            }
            $status = is_null($usr) ? self::ERROR_APIKEY : self::STATUS_OK;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => null
        );

        if (!is_null($datos)) {
            $respuesta['data'] = $datos;
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }
}
