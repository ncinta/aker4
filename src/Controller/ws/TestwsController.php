<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class TestwsController extends AbstractController
{

    /**
     * Lists all Cliente entities.
     * @Template("SecurTaxmonBundle:Testws:test.html.twig")
     */
    public function testAction()
    {
        //        $client = new \SoapClient('http://taxmonlocal.securityconsultant.com.ar/app_dev.php/ws/demoapi', array(
        //            'location' => 'http://taxmonlocal.securityconsultant.com.ar/app_dev.php/ws/demoapi'
        //        ));
        //        die('////<pre>' . nl2br(var_export($client, true)) . '</pre>////');
        try {
            $ws = 'http://calypsotrunk.securityconsultant.com.ar/app_dev.php/ws/transtel';
            //            $ws = 'http://calypso.securityconsultant.com.ar/ws/transtel';
            $client = new \SoapClient($ws, array('trace' => true));
            $client->__setLocation($ws);

            $headers = array();
            $headers[] = new \SoapHeader($ws, 'api_key', '1234');
            $client->__setSoapHeaders($headers);

            // $time = date('y-m-dTH:i:s', time()); // '2012-04-17T16:50:45';
            $date = new \DateTime(null, new \DateTimeZone('America/Santiago'));

            //            die('////<pre>' . nl2br(var_export($date, true)) . '</pre>////');

            $params = array(
                'codigo_eess' => '3288002',
                'fecha' => str_replace($date->format('P'), '', $date->format('c')),
                'patente' => 'BFVL871234',
                'litros' => 162.20,
                'guia_despacho' => '',
                'precio_total' => 0,
                'codigo_autorizacion' => 'test' . rand(),
            );

            $result = $client->__soapCall('cargaCombustible', $params);
            $result = $this->obj2array($result);
            die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');

            if (is_soap_fault($result)) {
                trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
            }

            return array('string' => $result);
        } catch (Exception $e) {
            echo "Exception: " . $e->getMessage();
        }
    }

    /**
     * @Template("SecurTaxmonBundle:Testws:test.html.twig")
     */
    public function testdistanciaAction()
    {
        try {
            $ws = 'http://calypsolocal.securityconsultant.com.ar/app_dev.php/ws/transtel';
            $client = new \SoapClient($ws, array('trace' => true));
            $client->__setLocation($ws);

            $headers = array();
            $headers[] = new \SoapHeader($ws, 'api_key', '1234');
            $client->__setSoapHeaders($headers);

            $params = array(
                'codigo_eess' => '795',
                'patente' => 'TA3572',
            );

            $result = $client->__soapCall('controlDistancia', $params);
            $result = $this->obj2array($result);
            die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');

            if (is_soap_fault($result)) {
                trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
            }

            return array('string' => $result);
        } catch (Exception $e) {
            echo "Exception: " . $e->getMessage();
        }
    }

    /**
     * @Template("SecurTaxmonBundle:Testws:test.html.twig")
     */
    public function testrendimientoAction()
    {
        try {
            $ws = 'http://calypsolocal.securityconsultant.com.ar/app_dev.php/ws/intranet';
            $client = new \SoapClient($ws, array('trace' => true));
            $client->__setLocation($ws);

            $headers = array();
            $headers[] = new \SoapHeader($ws, 'api_key', '1234');
            $client->__setSoapHeaders($headers);

            $params = array(
                'servicio_id' => '301',
                'carga_id' => '2171',
            );

            $result = $client->__soapCall('informeRendimiento', $params);
            $result = $this->obj2array($result);
            die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');

            if (is_soap_fault($result)) {
                trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
            }

            return array('string' => $result);
        } catch (Exception $e) {
            echo "Exception: " . $e->getMessage();
        }
    }

    /**
     * @Template("SecurTaxmonBundle:Testws:test.html.twig")
     */
    public function testreportesAction()
    {
        try {
            $ws = 'http://calypsotrunk.securityconsultant.com.ar/app_dev.php/ws/intranet';
            $client = new \SoapClient($ws, array('trace' => true));
            $client->__setLocation($ws);

            $headers = array();
            $headers[] = new \SoapHeader($ws, 'api_key', '1234');
            $client->__setSoapHeaders($headers);

            $params = array(
                'servicio_id' => '145',
                'desde' => '2013-07-01 00:00:00',
                'hasta' => '2013-07-05 23:59:59',
            );

            $result = $client->__soapCall('informeReportes', $params);
            $result = $this->obj2array($result);
            die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');

            if (is_soap_fault($result)) {
                trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
            }

            return array('string' => $result);
        } catch (Exception $e) {
            echo "Exception: " . $e->getMessage();
        }
    }

    /**
     * @Template("WsBundle:Testws:testroutine.html.twig")
     */
    public function testroutineAction()
    {
        try {

            $timezone = new \DateTimeZone('UTC');
            $time = date('y-m-dTH:i:s', time()); // '2012-04-17T16:50:45';
            $date = new \DateTime($time, $timezone);

            $params = array(
                'codigo_eess' => '3288002',
                'fecha' => str_replace($date->format('P'), '', $date->format('c')),
                'patente' => 'CTPY75',
                'litros' => 162.20,
                'guia_despacho' => '',
                'precio_total' => 0,
                'codigo_autorizacion' => 'asdassadf',
            );

            return $this->forward('WsBundle:Transtel:cargatest', $params);
        } catch (Exception $e) {
            echo "Exception: " . $e->getMessage();
        }
    }

    /**
     * Lists all Cliente entities.
     *
     * @Template("SecurTaxmonBundle:Testws:test.html.twig")
     */
    public function transtelAction()
    {
        $client = new \SoapClient('https://64.76.162.83:4254');
        //        die('////<pre>' . nl2br(var_export($client->__getFunctions(), true)) . '</pre>////');

        $response = $client->__call('registrarInactividad', array(
            '6dbc4a543fdf7860460677b7142cd356ae21927a07d9f192a1fca193352cb8d9',
            'BBXV32',
            date(),
        ));
        die('////<pre>' . nl2br(var_export($response, true)) . '</pre>////');
    }

    public function testrestfulAction(Request $request)
    {
        $data = $request->getContent();
        $form = json_decode($data, true);
        $resp = array('hola delphi !!!', $form);
        return new Response(json_encode($resp));
    }

    public function testservicioAction()
    {
        //GET /ws/servicios?apiKey=7cc98b8ebd715250270ad1633436df08b1ab0aa4&patentes=a:39:{i:0;s:6:\'BBFH85\';i:1;s:6:\'BBFH93\';i:2;s:6:\'BBFH94\';i:3;s:6:\'BHTY16\';i:4;s:6:\'BSRD62\';i:5;s:6:\'CSWJ40\';i:6;s:6:\'CZVB96\';i:7;s:6:\'DRZT46\';i:8;s:6:\'DRZT50\';i:9;s:6:\'FFVW40\';i:10;s:6:\'FPWB32\';i:11;s:6:\'JA2893\';i:12;s:6:\'JB8190\';i:13;s:6:\'JB8191\';i:14;s:6:\'JB8198\';i:15;s:6:\'JF5381\';i:16;s:6:\'JG1120\';i:17;s:6:\'JJ1483\';i:18;s:6:\'JJ1630\';i:19;s:6:\'JJ7244\';i:20;s:6:\'JK3812\';i:21;s:6:\'JK3813\';i:22;s:6:\'JL4406\';i:23;s:6:\'JL4407\';i:24;s:6:\'JL4408\';i:25;s:6:\'JL4409\';i:26;s:6:\'UG4754\';i:27;s:6:\'UG4755\';i:28;s:6:\'UG4756\';i:29;s:6:\'UU5838\';i:30;s:6:\'UV9518\';i:31;s:6:\'WV7652\';i:32;s:6:\'WV7653\';i:33;s:6:\'YN1506\';i:34;s:6:\'YN1507\';i:35;s:6:\'YN1508\';i:36;s:6:\'YS9614\';i:37;s:6:\'YS9615\';i:38;s:6:\'ZK4970\';} HTTP/1.0" 400 20176 "-" "-"
        $str = 'a:39:{i:0;s:6:\'BBFH85\';i:1;s:6:\'BBFH93\';i:2;s:6:\'BBFH94\';i:3;s:6:\'BHTY16\';i:4;s:6:\'BSRD62\';i:5;s:6:\'CSWJ40\';i:6;s:6:\'CZVB96\';i:7;s:6:\'DRZT46\';i:8;s:6:\'DRZT50\';i:9;s:6:\'FFVW40\';i:10;s:6:\'FPWB32\';i:11;s:6:\'JA2893\';i:12;s:6:\'JB8190\';i:13;s:6:\'JB8191\';i:14;s:6:\'JB8198\';i:15;s:6:\'JF5381\';i:16;s:6:\'JG1120\';i:17;s:6:\'JJ1483\';i:18;s:6:\'JJ1630\';i:19;s:6:\'JJ7244\';i:20;s:6:\'JK3812\';i:21;s:6:\'JK3813\';i:22;s:6:\'JL4406\';i:23;s:6:\'JL4407\';i:24;s:6:\'JL4408\';i:25;s:6:\'JL4409\';i:26;s:6:\'UG4754\';i:27;s:6:\'UG4755\';i:28;s:6:\'UG4756\';i:29;s:6:\'UU5838\';i:30;s:6:\'UV9518\';i:31;s:6:\'WV7652\';i:32;s:6:\'WV7653\';i:33;s:6:\'YN1506\';i:34;s:6:\'YN1507\';i:35;s:6:\'YN1508\';i:36;s:6:\'YS9614\';i:37;s:6:\'YS9615\';i:38;s:6:\'ZK4970\';}';
        $str = array('BBFH85', 'BBFH93', 'BBFH94', 'BHTY16', 'BSRD62', 'CSWJ40', 'CZVB96', 'DRZT46', 'DRZT50', 'FFVW40', 'FPWB32', 'JA2893', 'JB8190', 'JB8191', 'JB8198', 'JF5381', 'JG1120', 'JJ1483', 'JJ1630', 'JJ7244', 'JK3812', 'JK3813', 'JL4406', 'JL4407', 'JL4408', 'JL4409', 'UG4754', 'UG4755', 'UG4756', 'UU5838', 'UV9518', 'WV7652', 'WV7653', 'YN1506', 'YN1507', 'YN1508', 'YS9614', 'YS9615', 'ZK4970');
        $arr = array(
            'patentes' => urlencode(serialize($str)),
            'apiKey' => '7cc98b8ebd715250270ad1633436df08b1ab0aa4',
        );
        $url = 'http://calypsotrunk.securityconsultant.com.ar/app_dev.php/ws/servicios';

        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($arr),
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        die('////<pre>' . nl2br(var_export($result, true)) . '</pre>////');
        // return $this->redirect($this->generateUrl('webservice_servicios', $arr));
    }

    function obj2array($obj)
    {
        $out = array();
        foreach ($obj as $key => $val) {
            switch (true) {
                case is_object($val):
                    $out[$key] = $this->obj2array($val);
                    break;
                case is_array($val):
                    $out[$key] = $this->obj2array($val);
                    break;
                default:
                    $out[$key] = $val;
            }
        }
        return $out;
    }
}
