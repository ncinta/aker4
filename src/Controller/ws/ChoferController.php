<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Model\app\OrganizacionManager;
use App\Model\app\ChoferManager;
use App\Model\app\LicenciaManager;
use App\Model\app\NotificacionChoferManager;
use App\Model\app\NotificadorManager;
use App\Model\app\ServicioManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Psr\Log\LoggerInterface;

/**
 * Description of ChoferController
 *
 * @author nicolas
 */
class ChoferController extends AbstractController
{
    private $logear = true;

    const STATUS_ERROR = -1;
    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_MEDICIONES = 6;
    const ERROR_ORGANIZACION = 7;
    const ERROR_SERVICIONOENCONTRADO = 8;
    const ERROR_CHOFERNOENCONTRADO = 9;
    const ERROR_NO_HAY_DATOS = 10;

    private $strResponse = array(
        self::STATUS_ERROR => 'Error de datos',
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
        self::ERROR_SERVICIONOENCONTRADO => 'Servicio no encontrado',
        self::ERROR_CHOFERNOENCONTRADO => 'Chofer no encontrado',
        self::ERROR_NO_HAY_DATOS => 'No hay datos'
    );
    private $organizacionManager;
    private $choferManager;
    private $licenciaManager;
    private $notifchoferManager;
    protected $notificadorManager;
    protected $servicioManager;
    private $logger;

    function __construct(
        OrganizacionManager $organizacionManager,
        ChoferManager $choferManager,
        LicenciaManager $licenciaManager,
        NotificacionChoferManager $notifchoferManager,
        NotificadorManager $notificadorManager,
        ServicioManager $servicioManager,
        LoggerInterface $logger,
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->choferManager = $choferManager;
        $this->licenciaManager = $licenciaManager;
        $this->notifchoferManager = $notifchoferManager;
        $this->notificadorManager = $notificadorManager;
        $this->servicioManager = $servicioManager;
        $this->logger = $logger;
    }

    private function log($str)
    {
        if ($this->logear) {
            $this->logger->notice('ws/chofer | ' . $str);
        }
    }
    /**
     * @Route("/ws/chofer/licencia", name="webservice_chofer_licencia", methods = {"GET"})
     */
    public function licenciaAction(Request $request)
    {
        $enviar = $request->get('enviar') ? $request->get('enviar') == "1" || $request->get('enviar') == 'true'  : false;
        $response = array();
        $enviosResult = [];
        $now = new \DateTime();
        $dayPorVencer = (new \DateTime())->modify('+45 days');
        $orgs = $this->organizacionManager->findAll(1); // que traiga todas 1 es el id de security
        foreach ($orgs as $org) {
            if ($this->organizacionManager->isModuloEnabled($org, 'CLIENTE_CHOFER')) {
                $grupos = $this->notifchoferManager->findAll($org);
                foreach ($grupos as $grupo) {   //recorro el grupo
                    $contactos = array();
                    $vencidas = array();
                    $porVencer = array();
                    foreach ($grupo->getNotifChof() as $notifChofer) {   //recorro los choferes                        
                        $chofer = $notifChofer->getChofer();
                        foreach ($chofer->getLicencias() as $licencia) {  //recorro las licencias de los choferes
                            $data = array(
                                'nombre' => $chofer->getNombre(),
                                'telefono' => $chofer->getTelefono(),
                                'licenciaNumero' => $licencia->getNumero(),
                                'licenciaCategoria' => $licencia->getCategoria() ? $licencia->getCategoria() : '---',
                                'licenciaVencimiento' => $licencia->getVencimiento(),
                            );
                            if ($licencia->getVencimiento() <= $now) {    //ya se vencio
                                $vencidas[] = $data;
                            } else {
                                // si vence a futuro pero es menor que los dias de limite                                
                                if ($licencia->getVencimiento() <= $dayPorVencer) {
                                    $porVencer[] = $data;
                                }
                            }
                        }
                    }

                    if (count($vencidas) > 0 || count($porVencer) > 0) {
                        $html = $this->renderView('ws/Chofer/content_chofer.html.twig', array('grupo' => $grupo, 'vencidas' => $vencidas, 'porvencer' => $porVencer));
                        foreach ($grupo->getNotifContact() as $notifContact) {
                            if ($notifContact->getContacto()->getEmail()) {
                                $contactos[] = $notifContact->getContacto()->getEmail();
                            }
                        }
                        $titulo = 'Control de Licencia de Choferes';
                        $enviosResult[] = array(
                            $org->getNombre() => $this->send($enviar, $titulo, $html, $contactos)
                        );
                    }
                }
            }
        }
        $status = self::STATUS_OK;
        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $enviosResult
        );
        return $this->generateResponse($status, $respuesta);
    }

    /** 
     * determino si el evento debe enviarse dependiendo el dia de la semana en la que estoy
     */
    private function inFecha($ev)
    {
        // armo un array con los datos que viene en el evento ordenado por dia de la semana
        $data = [
            0 => $ev->getDomingo(),
            1 => $ev->getLunes(),
            2 => $ev->getMartes(),
            3 => $ev->getMiercoles(),
            4 => $ev->getJueves(),
            5 => $ev->getViernes(),
            6 => $ev->getSabado(),
        ];
        return $data[date("w")];  //para el dia de la semana q estoy veo si debo o no enviarlo        
    }

    private function send($enviar, $asunto, $cuerpo, $destinos)
    {
        if ($enviar) {
            //$destinos = ['cgbrandolin@gmail.com'];   //destino de testing
            return $this->notificadorManager->despacharMail($destinos, $asunto, $cuerpo);
        }
        return false;
    }

    /**
     * @Route("/ws/choferes/{apikey}", name="webservice_chofer", methods={"GET"})     
     */
    public function listAction(Request $request, $apikey)
    {
        $status = self::ERROR_FALTAN_DATOS;
        if (is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                $status = self::STATUS_OK;

                foreach ($organizacion->getChoferes() as $chofer) {
                    $choferes[$chofer->getId()] = array(
                        'id' => $chofer->getId(),
                        'nombre' => $chofer->getNombre(),
                    );
                }
            }
        }
        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'choferes' => isset($choferes) ? $choferes : null
        );
        return $this->generateResponse($respuesta);
    }

    /**
     * @Route("/ws/chofer/login/{phone}/{code}", name="webservice_chofer_login", methods={"GET"})     
     */
    public function loginAction($phone, $code)
    {
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $chofer = $this->choferManager->findByApicode($phone, $code);
            if ($chofer) {
                $datos['usuario_id'] = $chofer->getId();
                $datos['organizacion_id'] = $chofer->getOrganizacion()->getId();
                $datos['timezone'] = $chofer->getOrganizacion()->getTimeZone();
                $datos['apiKey'] = $chofer->getOrganizacion()->getApikey();
                if (!is_null($chofer->getServicio())) {
                    $datos['servicio'] = array(
                        'nombre' => $chofer->getServicio()->getNombre(),
                        'id' => $chofer->getServicio()->getId(),
                        'tipo_servicio' => ['id' => $chofer->getServicio()->getTipoServicio()->getId(), 'nombre' => $chofer->getServicio()->getTipoServicio()->getnombre()],
                        'clase_servicio' => ['id' => $chofer->getServicio()->getTipoObjeto(), 'nombre' => $chofer->getServicio()->getStrTipoObjetos()]
                    );
                }
            }
            $status = is_null($chofer) ? self::ERROR_APIKEY : self::STATUS_OK;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => null
        );

        if (!is_null($datos)) {
            $respuesta['data'] = $datos;
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }


    const TIPO_RECEPCION = 0;
    const TIPO_ENTREGA = 1;

    /**
     * @Route("/ws/chofer/vehiculo/{apikey}", name="webservice_chofer_vehiculo", methods={"POST"})     
     */
    public function vehiculoAction(Request $request, $apikey)
    {
        $historicoId = 0;
        $status = self::STATUS_OK;
        // Decodificar el JSON
        $data = json_decode($request->getContent(), true);
        $this->log($request->getContent());

        if (is_null($apikey)) {
            $status = self::ERROR_APIKEY;
        } else {
            $organizacion = $this->organizacionManager->findByApiKey($apikey);

            if ($organizacion) {

                if (empty($data)) {
                    $status = self::ERROR_NO_HAY_DATOS;
                } else {
                    if ($data['servicio_id'] == 0 || $data['chofer_id'] == 0) {
                        $status = self::ERROR_FALTAN_DATOS;
                    } else {
                        if ($data['motivo'] == 0) {
                            $historicoId = $this->setHistorico($data, self::TIPO_RECEPCION);
                        } else {
                            $historicoId = $this->setHistorico($data, self::TIPO_ENTREGA);
                        }
                    }
                }
            }
        }
        $respuesta = [
            'code' => $status,
            'response' => $this->strResponse[$status],
            'id' => $historicoId,
            'data' => $data
        ];
        // dd($respuesta);

        return $this->generateResponse($respuesta);
    }

    /** Graba el formulario y asocia el chofer al vehiculo */
    private function setHistorico($data, $motivo)
    {
        // Inicializar algunas variables        
        $historico = null;
        $chofer = null;
        $servicio = null;

        // Obtener el estado desde 'status' si existe
        $data['estado'] = isset($data['status']) ? intval($data['status']) : null;

        // Validar servicio
        if (isset($data['servicio_id'])) {
            $servicio = $this->servicioManager->find(intval($data['servicio_id']));
            if (!$servicio) {
                return self::STATUS_ERROR;
            }
        } else {
            return self::STATUS_ERROR;
        }

        // Actualizar el servicio del chofer
        // Validar chofer
        if (isset($data['chofer_id'])) {
            $chofer = $this->choferManager->find(intval($data['chofer_id']));
            if (!$chofer) {
                return self::STATUS_ERROR;
            } else {
                if ($motivo == SELF::TIPO_RECEPCION) {
                    if (!is_null($servicio->getChofer()) and $servicio->getChofer() != $chofer) {
                        /** el servicio ya tiene un chofer y se le quiere asignar otro, entonce tengo
                         * que retirar el chover asociado para despues ponerle otro
                         */
                        $this->choferManager->retirarServicio($servicio->getChofer(), $servicio);
                    }
                    if ($chofer->getServicio() != $servicio) {   //solo debo asociarlo cuando es distinto
                        $this->choferManager->asociarServicio($chofer, $servicio);
                    }
                } else {
                    /** debo retirar el servicio cuando se hace la entrega */
                    $this->choferManager->retirarServicio($chofer, $servicio);
                }

                // Agregar historial                
                $historico = $this->choferManager->addHistorico($chofer, $servicio, $motivo, $data);
                if (!$historico) {
                    return self::STATUS_ERROR;
                }
            }
        } else {
            return self::STATUS_ERROR;
        }

        return $historico->getId();
    }

    private function generateResponse($struct)
    {
        return new Response(json_encode($struct), 200, array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        ));
    }
}
