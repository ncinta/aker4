<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\BackendWsManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InformeDistanciaController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;
    private $backend;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    /**
     * @Route("/ws/info/distancia", name="webservice_info_distancia")
     * @Method({"GET", "POST"})
     */
    public function distanciaAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager
    ) {
        $apikey = $request->get('apiKey');
        $fdesde = $request->get('desde');
        $fhasta = $request->get('hasta');
        $serv = $request->get('servicio');
        $servId = $request->get('servicioId');
        //die('////<pre>' . nl2br(var_export($serv, true)) . '</pre>////');
        $status = true;
        $informe = null;
        if (is_null($fdesde) || is_null($fhasta) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $fdesde = new \DateTime($fdesde);
            $fhasta = new \DateTime($fhasta);
            $organizacion = $organizacionManager->findByApiKey($apikey);  //obtengo la organizacion.
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    $this->setBackend($organizacion);   //seteo el backend.
                    if (!is_null($servId)) {
                        $servicios[] = $servicioManager->find($servId);
                    } elseif (!is_null($serv)) {
                        $servicios[] = $servicioManager->findByNombre($serv);
                    } else {
                        $servicios = $servicioManager->findAllEnabledByOrganizacion($organizacion, false);
                    }
                }

                //obtengo la información
                $informe = array();
                foreach ($servicios as $servicio) {
                    $informe[] = array(
                        'servicio' => $servicio->getNombre(),
                        'patente' => !is_null($servicio->getVehiculo()) ? $servicio->getVehiculo()->getPatente() : null,
                        'data' => $this->getDataDistancia($servicio->getId(), $fdesde->format('Y-m-d H:n:s'), $fhasta->format('Y-m-d H:n:s'), $utilsManager),
                    );
                    //die('////<pre>' . nl2br(var_export($apikey, true)) . '</pre>////');
                }
                $status = self::STATUS_OK;
            } else {
                $status = self::ERROR_ORGANIZACION;
            }
        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($informe)) {
            $respuesta['data'] = $informe;
        }
        return $this->generateResponse($status, $respuesta);
    }

    public function getDataDistancia($servicio_id, $desde, $hasta, $utilsManager)
    {
        $informe = $this->backend->informeDistancias(intval($servicio_id), $desde, $hasta, array());
        //$this->container->get('logger')->info('InformeDistanciaController:getDataDistancia: ' . var_export($informe, true));
        if ($informe != null) {
            $informe['tiempo_activo'] = $utilsManager->segundos2tiempo($informe['segundos_activo']);
            $informe['km_total'] = $informe['kms_calculada'];
            $informe['km_por_dia'] = $informe['kms_por_dia_calculada'];
            if ($informe['segundos_motor_encendido'] !== 0) {
                $informe['segundos_ralenti'] = $informe['segundos_motor_encendido'] - $informe['segundos_movimiento'];
                $informe['tiempo_ralenti'] = $utilsManager->segundos2tiempo($informe['segundos_ralenti']);
                $informe['porcentaje_ralenti'] = ($informe['segundos_motor_encendido'] - $informe['segundos_movimiento']) / $informe['segundos_motor_encendido'];

                if ($informe['segundos_activo'] !== 0) {
                    //uso de flota
                    $informe['porcentaje_uso_flota'] = ($informe['segundos_activo'] - $informe['segundos_motor_encendido']) / $informe['segundos_activo'];
                }
            }
        }
        //die('////<pre>'.nl2br(var_export($informe, true)).'</pre>////');

        return $informe;
    }

    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function setBackend($organizacion)
    {
        $usuario = $organizacion->getUsuarioMaster();
        $this->backend = new BackendWsManager($this->getEntityManager(), $organizacion, $this->container->getParameter('calypso.backend_addr'), $this->container->getParameter('calypso.backend_addr_backup'), $usuario);
        return $this->backend;
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }
}
