<?php

namespace App\Controller\ws;

ini_set('max_execution_time', '0');

use App\Entity\CentroCosto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\TareaMantManager;
use App\Model\app\MantenimientoManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\EventoManager;
use App\Model\crm\PeticionManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\IndiceManager;
use App\Model\app\NotificadorManager;
use App\Model\app\TipoEventoManager;
use App\Entity\IndiceServicio;
use App\Entity\GrupoServicio;
use App\Entity\IndiceCc;
use App\Entity\IndiceGs;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MantenimientoController extends AbstractController
{

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    protected $organizacionManager;
    protected $servicioManager;
    protected $tareamantManager;
    protected $ordentrabajoManager;
    protected $peticionManager;
    protected $mantenimientoManager;
    protected $eventoManager;
    protected $combustibleManager;
    protected $indiceManager;
    protected $notificadorManager;
    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    public function __construct(
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager,
        TareaMantManager $tareamantManager,
        OrdenTrabajoManager $ordentrabajoManager,
        PeticionManager $peticionManager,
        MantenimientoManager $mantenimientoManager,
        EventoManager $eventoManager,
        CombustibleManager $combustibleManager,
        IndiceManager $indiceManager,
        TipoEventoManager $tipoEventoManager,
        NotificadorManager $notificadorManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->tareamantManager = $tareamantManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->peticionManager = $peticionManager;
        $this->mantenimientoManager = $mantenimientoManager;
        $this->eventoManager = $eventoManager;
        $this->combustibleManager = $combustibleManager;
        $this->indiceManager = $indiceManager;
        $this->tipoEventoManager = $tipoEventoManager;
        $this->notificadorManager = $notificadorManager;
    }

    /**
     * @Route("/ws/mante/servicio", name="webservice_mante_servicio", methods={"GET"})
     */
    public function statusMantenimientosAction(Request $request)
    {
        $enviar = $request->get('enviar') ? $request->get('enviar') == "1" || $request->get('enviar') == 'true'  : false;
        $org = $request->get('organizacion') ? $request->get('organizacion') : null;

        $html = null;
        $status = true;
        $enviosResult = [];

        $tipoEvento = $this->tipoEventoManager->findByCodename('EV_MANTENIMIENTO');  //obtengo el evento

        //si pase organizacion entonces busco los mantenimientos de esa organizacion solamente
        if ($org) {
            $organizacion = $this->organizacionManager->find($org);
            $eventos = $this->eventoManager->findByTipoOrganizacion($tipoEvento, $organizacion);
        } else {
            $eventos = $tipoEvento->getEventos();
        }

        $servicios = array();
        $f = new \DateTime('', new \DateTimeZone('America/Buenos_Aires'));
        $titulo = sprintf('SOPORTE Aker: Estado de mantenimientos al %s', $f->format('d-m-Y H:i'));
        foreach ($eventos as $evento) {
            if ($this->inFecha($evento)) {
                $servicios = [];
                $dataServicios = $evento->getServicios();
                if ($dataServicios) {
                    //recorro todos los servicios
                    foreach ($dataServicios as $servicio) {
                        if ($servicio && $servicio->getEstado() <= 1) {
                            $servicios[] = $servicio;
                        }
                    }

                    //pido los informes de los servicios
                    $informe = $this->tareamantManager->procesarStatusMantenimiento($servicios);
                    $html = $this->renderView('ws/InformeMantenimiento/content_informe.html.twig', array('informe' => $informe, 'organizacion' => $evento));

                    $enviosResult[] = array(
                        $evento->getOrganizacion()->getNombre() => $this->send($enviar, $titulo, $html, $evento)
                    );
                }
            }
        }
        $status = self::STATUS_OK;

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $enviosResult
        );
        //dd($respuesta);

        return $this->generateResponse($status, $respuesta);
    }

    /** 
     * determino si el evento debe enviarse dependiendo el dia de la semana en la que estoy
     */
    private function inFecha($ev)
    {
        // armo un array con los datos que viene en el evento ordenado por dia de la semana
        $data = [
            0 => $ev->getDomingo(),
            1 => $ev->getLunes(),
            2 => $ev->getMartes(),
            3 => $ev->getMiercoles(),
            4 => $ev->getJueves(),
            5 => $ev->getViernes(),
            6 => $ev->getSabado(),
        ];
        return $data[date("w")];  //para el dia de la semana q estoy veo si debo o no enviarlo        
    }

    private function send($enviar, $asunto, $cuerpo, $evento)
    {
        if ($enviar) {
            $destinos = array();
            foreach ($evento->getNotificaciones() as $notificacion) {
                if ($notificacion->getTipo() == 2 && $notificacion->getContacto()->getEmail() != null) { //es mail
                    $destinos[] = $notificacion->getContacto()->getEmail();
                }
            }
            $destinos[] = 'cbrandolin@securityconsultant.com.ar';   //destino de testing
            return $this->notificadorManager->despacharMail($destinos, $asunto, $cuerpo);
        }
        return false;
    }

    private function generateResponse($status, $struct)
    {
        return new Response(json_encode($struct), $status == self::STATUS_OK ? 200 : 400);
    }

    /**
     * @Route("/ws/mante/indice", name="webservice_mante_indice")
     * @Method({"GET", "POST"})
     */
    public function indiceAction(Request $request)
    {
        $now = new \DateTime();
        //obtener todas las organizacion con mantenimientos
        //$organizaciones = $this->organizacionManager->getByModulo('CLIENTE_MANTENIMIENTO');
        $borrarIndServ = $this->indiceManager->borrarIndices();

        $organizaciones = array($this->organizacionManager->find(179));   //solo segar ???

        foreach ($organizaciones as $organizacion) {
            $moduloPrestacion = $this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_PETICIONES');
            // $servicios = $organizacion->getServicios();
            $servicios = $this->servicioManager->findActivos($organizacion);  //solo los estaso<4

            //genero los indices para los servicios

            $indiceServicios = $this->generarIndiceServicio($servicios, $moduloPrestacion);

            //genero los indices para los centros de costos
            $indiceCc = $this->generarIndiceCc($organizacion, $moduloPrestacion);

            //genero los indices para los grupos de servicios
            $indiceGs = $this->generarIndiceGs($organizacion, $moduloPrestacion);
        }
        $fin = new \DateTime();
        //dd($fin->getTimestamp() - $now->getTimestamp() . ' seg');
        return $this->generateResponse(self::STATUS_OK, $this->strResponse[self::STATUS_OK]);
    }

    private function generarIndiceServicio($servicios, $moduloPrestacion): bool
    {
        //$servicios = array($this->servicioManager->findById(9681));   testing
        $fecha = date("Y-m-d", strtotime('-1 days'));   //la fecha es para todos los servicios igual.


        foreach ($servicios as $servicio) {
            //inicio los array donde van a quedar los datos.
            $arrMante = array(
                'manteNuevos' => 0,
                'mantePendientes' => 0,
                'manteAsignados' => 0,
                'manteRealizados' => 0,
            );
            $arrPet = array(
                'petNuevas' => 0,
                'petPendientes' => 0,
                'petEnCurso' => 0,
                'petDeten' => 0,
                'petRealizada' => 0,
            );
            $arrOt = array(
                'otNuevas' => 0,
                'otEnCurso' => 0,
                'otCerradas' => 0,
            );

            //GENERO INDICES DE MANTENIMIENTO                
            $tmant = $this->tareamantManager->findByServicio($servicio);
            $tareaVencidas = $this->tareamantManager->data($tmant, 3);   //de los mant, trae los vencidos
            $tareaOk = $this->tareamantManager->data($tmant, 1);  //de los mant, trae los oks
            $ok = $tmant && $tareaOk ? count($tareaOk) : 0;

            //obtengo los mantenmientos
            $arrMante['mantePendientes'] = $tmant && $tareaVencidas ? count($tareaVencidas) : 0; //los mantenimientos vencidos
            $arrMante['manteAsignados'] = count($tmant);

            //obtengo las peticiones
            if ($moduloPrestacion) {
                $arrPet['petEnCurso'] = $this->peticionManager->getCountPeticionPorEstado($servicio, 2, false, $fecha);
                $arrPet['petDeten'] = $this->peticionManager->getCountPeticionPorEstado($servicio, 3, false, $fecha);
                $arrPet['petPendientes'] = $this->peticionManager->getCountPeticionPorEstado($servicio, 1, true, $fecha);
            }

            //obtengo las OT
            $arrOt['otEnCurso'] = $this->ordentrabajoManager->getCountOtPorEstado($servicio, $fecha, 1);

            //obtengo el ultimo indiceServicio generado
            $ultimo = $this->indiceManager->getLastInsertServicio($servicio); //traigo el ultimo indice de ese servicio

            //NO ES EL 1ER INDICE            
            if ($ultimo) {
                //MENTENIMIENTOS
                $arrMante['manteNuevos'] = ($arrMante['mantePendientes'] - $ultimo->getMantenimientoPendiente()) > 0 ?
                    $arrMante['mantePendientes'] - $ultimo->getMantenimientoPendiente() : 0;
                $arrMante['manteRealizados'] = ($ultimo->getMantenimientoPendiente() - $arrMante['mantePendientes']) > 0 ?
                    $ultimo->getMantenimientoPendiente() - $arrMante['mantePendientes'] : 0;


                //PETICIONES
                if ($moduloPrestacion) {
                    $arrPet['petNuevas'] = $this->peticionManager->getNuevasPorServicio($servicio);
                    $arrPet['petRealizada'] = $this->peticionManager->getRealizadasPorServicio($servicio);
                }

                // ORDENES DE TRABAJO
                $arrOt['otCerradas'] = $this->ordentrabajoManager->getCerradasPorCloseAt($servicio);
                $arrOt['otNuevas'] = $this->ordentrabajoManager->getNuevasPorCreatedAt($servicio);
            } else { //ES EL 1ER INDICE
                //MANTENIMIENTOS
                $arrMante['manteNuevos'] = 0;
                $arrMante['manteRealizados'] = 0;

                //PETICIONES
                if ($moduloPrestacion) {
                    $arrPet['petNuevas'] = $arrPet['petPendientes'];
                    $arrPet['petRealizada'] = $this->peticionManager->getCountPeticionPorEstado($servicio, 10, false, $fecha);
                }
                // ORDENES DE TRABAJO
                $arrOt['otCerradas'] = $this->ordentrabajoManager->getCountOtPorEstado($servicio, $fecha, 2);
                $arrOt['otNuevas'] = $this->ordentrabajoManager->getCountOtPorEstado($servicio, $fecha);
            }

            //genero y grabo los indices para el servicio.
            $indice = new IndiceServicio();
            $indice->setServicio($servicio);
            $indiceAnterior = $this->indiceManager->grabarIndice($indice, $arrMante, $arrPet, $arrOt);
        }

        //debo grabar todo lo que tengo en el buffer antes de salir. NO BORRAR
        $this->indiceManager->forcePersistIndiceServicio();

        return true;
    }

    private function generarIndiceCc($organizacion, $moduloPrestacion): bool
    {

        foreach ($organizacion->getCentrosCosto() as $centroCosto) {   //recorro todos los centros de costos de la organizacion                
            $this->manejarIndices($centroCosto, $moduloPrestacion);
        }

        return true;
    }

    private function generarIndiceGs($organizacion, $moduloPrestacion): bool
    {

        foreach ($organizacion->getGruposServicio() as $grupoServicio) {  //recorro todos los grupos de servicios de la organizacion                
            $this->manejarIndices($grupoServicio, $moduloPrestacion);
        }

        return true;
    }

    private function manejarIndices($entity, $moduloPrestacion)
    {
        //inicializo estructuras.
        $arrMante = array(
            'manteNuevos' => 0,
            'mantePendientes' => 0,
            'manteAsignados' => 0,
            'manteRealizados' => 0,
        );
        $arrPet = array(
            'petNuevas' => 0,
            'petPendientes' => 0,
            'petEnCurso' => 0,
            'petDeten' => 0,
            'petRealizada' => 0,
        );
        $arrOt = array(
            'otNuevas' => 0,
            'otEnCurso' => 0,
            'otCerradas' => 0,
        );

        //instancio indice segun corresponda para que sean grabados despues      
        if ($entity instanceof CentroCosto) {
            $indice = new IndiceCc();
            $indice->setCentroCosto($entity);
        } elseif ($entity instanceof GrupoServicio) {
            $indice = new IndiceGs();
            $indice->setGrupoServicio($entity);
        } else {
        }
        //GENERO INDICES DE MANTENIMIENTO
        if (count($entity->getServicios()) == 0) {       //grabo el indice cuando no  tiene servicios la entidad.     
            $indiceAnterior = $this->indiceManager->grabarIndice($indice, $arrMante, $arrPet, $arrOt);
        } else {
            foreach ($entity->getServicios() as $servicio) {    //recorro todos los servicios de la entidad CC o GS.
                $indiceServicio = $this->indiceManager->getLastInsertServicio($servicio); //busco el ultimo de servicio

                // GENERRO INDICE DE MANTENIMIENTOS
                $arrMante['mantePendientes'] += $indiceServicio ? $indiceServicio->getMantenimientoPendiente() : 0;
                $arrMante['manteAsignados'] += $indiceServicio ? $indiceServicio->getMantenimientoAsignado() : 0;
                $arrMante['manteRealizados'] += $indiceServicio ? $indiceServicio->getMantenimientoRealizado() : 0;
                $arrMante['manteNuevos'] += $indiceServicio ? $indiceServicio->getMantenimientoNuevo() : 0;

                // GENERO INDICE DE PETICIONES
                if ($moduloPrestacion) {
                    $arrPet['petNuevas'] += $indiceServicio ? $indiceServicio->getPeticionNueva() : 0;
                    $arrPet['petPendientes'] += $indiceServicio ? $indiceServicio->getPeticionPendiente() : 0;
                    $arrPet['petEnCurso'] += $indiceServicio ? $indiceServicio->getPeticionEncurso() : 0;
                    $arrPet['petDeten'] += $indiceServicio ? $indiceServicio->getPeticionDetenida() : 0;
                    $arrPet['petRealizada'] += $indiceServicio ? $indiceServicio->getPeticionRealizada() : 0;
                }

                $arrOt['otNuevas'] += $indiceServicio ? $indiceServicio->getOrdentrabajoNueva() : 0;
                $arrOt['otEnCurso'] += $indiceServicio ? $indiceServicio->getOrdentrabajoEncurso() : 0;
                $arrOt['otCerradas'] += $indiceServicio ? $indiceServicio->getOrdentrabajoRealizada() : 0;
            }
            $this->indiceManager->grabarIndice($indice, $arrMante, $arrPet, $arrOt);
        }
        //debo grabar todo lo que tengo en el buffer antes de salir. NO BORRAR
        $this->indiceManager->forcePersistIndiceServicio();
    }

    private function doctrineFlush(): void
    {
        try {

            $this->getDoctrine()->getManager()->flush(); // hago el flush cuando ya este todo cargado
        } catch (\PDOException $e) {
            echo ('////<pre>' . nl2br(var_export($e, true)) . '</pre>////');
        }
    }
}
