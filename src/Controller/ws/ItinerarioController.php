<?php

namespace App\Controller\ws;

use App\Entity\ItinerarioServicio;
use App\Model\app\NotificadorAgenteManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\OrganizacionManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\app\LoggerManager;


/**
 * Description of ItinerarioController
 *
 * @author nicolas
 */
class ItinerarioController extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ERROR = 1;
    const STATUS_NADA = 2;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::STATUS_NADA => 'NADA PARA HACER',
        self::STATUS_ERROR => 'ERROR',

    );

    /** este ws lo que hace es buscar los itinerarios cerrados y 
     * @Route("/ws/itinerario/checkcierre", name="webservice_checkcierre")
     * @Method({"GET", "POST"})
     */
    public function itinerarioAction(Request $request, NotificadorAgenteManager $notificadorAgenteManager)
    {
        $status = self::STATUS_ERROR;
        $em = $this->getEntityManager();
        $grupos_eliminados = array();
        $eventos_eliminados = array();
        $itinerariosEventos = $this->findAllForEventos();
        $itinerariosGrupoRef = $this->findAllForGrupoRef();
        if (count($itinerariosGrupoRef) > 0 || count($itinerariosEventos) > 0) {
            $status = self::STATUS_OK;

            foreach ($itinerariosEventos as $itinerario) {
                foreach ($itinerario->getEventoItinerario() as $evIt) {
                    if ($evIt->getEvento()) {
                        $evento = $evIt->getEvento();
                        if ($notificadorAgenteManager->notificar($evento, 2)) {
                            foreach ($evento->getHistoricoItinerario() as $historico) {
                                $historico->removeEvento();
                                $em->persist($historico);
                                $em->flush();
                            }
                            //saco el evento de evIt
                            $evIt->removeEvento();
                            $em->persist($evIt);
                            $em->flush();

                            //borro el evento.
                            $em->remove($evento);
                            $em->flush();
                        }
                    }
                }
            }

            foreach ($itinerariosGrupoRef as $itinerario) {
                $grupo = $itinerario->getGrupoReferencia();
                //   die('////<pre>' . nl2br(var_export($itinerario->getCodigo(), true)) . '</pre>////');
                if ($notificadorAgenteManager->notificar($grupo, 2)) {
                    $itinerario->removeGrupoReferencia();
                    $em->persist($itinerario);
                    $em->flush();
                    $grupos_eliminados[] = $grupo->getNombre();
                    $em->remove($grupo);
                    $em->flush();
                }
            }
        } else {
            $status = self::STATUS_NADA;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'grupos_eliminados' => isset($grupos_eliminados) ? $grupos_eliminados : null,
            'eventos_eliminados' => isset($eventos_eliminados) ? $eventos_eliminados : null
        );
        return $this->generateResponse($respuesta);
    }

    private function generateResponse($struct)
    {
        return new Response(json_encode($struct), 200, array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        ));
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    public function findAllForEventos() //traigo todos los itinerarios que tengan eventos y estén cerrados
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.eventoItinerario', 'u')
            ->where('i.estado >= 9'); //cerrado o eliminado
        return $query->getQuery()->getResult();
    }


    public function findAllForServicios() //traigo todos los itinerarios que tengan servicios
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.servTemp', 's');
        return $query->getQuery()->getResult();
    }

    public function findAllForGrupoRef() //traigo todos los itinerarios que tengan grupo de referencia y estén cerrados
    {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('i')
            ->from('App:Itinerario', 'i')
            ->join('i.grupoReferencia', 'u')
            ->where('i.estado >= 9'); //cerrado o eliminado
        return $query->getQuery()->getResult();
    }

    /** script para corregir el pedo de que los servicios no se agregan a los eventos de los itinerarios
     * @Route("/ws/itinerario/checkservicioevento", name="webservice_checkservicioevento")
     * @Method({"GET", "POST"})
     */
    public function servicioeventosAction(
        Request $request,
        NotificadorAgenteManager $notificadorAgenteManager,
        ItinerarioManager $itinerarioManager,
        OrganizacionManager $organizacionManager
    ) {
        // 
        $status = self::STATUS_ERROR;
        $em = $this->getEntityManager();
        $organizacion = $organizacionManager->find(889); //buscamos sadi
        $itinerarios = $itinerarioManager->findAllByOrganizacion($organizacion);
        if (count($itinerarios) > 0) {
            foreach ($itinerarios as $itinerario) {
                if ($itinerario->getEstado() < 9) {

                    foreach ($itinerario->getEventoItinerario() as $evIt) {
                        if ($evIt->getEvento()) {
                            $evento = $evIt->getEvento();
                            //ahora grabo los servicios.
                            if ($itinerario->getServicios()) {
                                foreach ($itinerario->getServicios() as $servicio) {
                                    $evento->addServicio($servicio->getServicio());
                                    $em->persist($evento);
                                }
                                $em->flush();
                            }
                        }
                    }
                    foreach ($itinerario->getEventoItinerario() as $evIt) {
                        if ($evIt->getEvento()) {
                            if ($evIt->getEvento()->getActivo()) {
                                $notificar = $notificadorAgenteManager->notificar($evIt->getEvento(), 1);
                            }
                        }
                    }
                }
            }
            $status = self::STATUS_OK;
        } else {
            $status = self::STATUS_NADA;
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],

        );
        return $this->generateResponse($respuesta);
    }
}
