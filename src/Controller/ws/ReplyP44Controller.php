<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\NotificadorP44Manager;
use App\Entity\Itinerario as Itinerario;
use App\Model\app\GeocoderManager;
use Psr\Log\LoggerInterface;

class ReplyP44Controller extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ERROR_DATA = 1;
    const STATUS_ERROR_SINDATA = 2;
    const STATUS_ERROR_SINEVENTOID = 3;
    const STATUS_ERROR_SINSERVICIOID = 4;
    const STATUS_ERROR_SINTITULO = 5;
    const STATUS_ERROR_GRABACION = 6;
    const STATUS_ERROR_FECHA = 7;
    const STATUS_ERROR_SINUID = 8;

    const LOGISTICA_P44 = 17;   //VER QUE ID TIENE

    private $logger;
    private $itinerarioManager;
    private $logisticaManager;
    private $logear = true;
    private $notificadorP44Manager;
    private $geocoderManager;

    public function __construct(
        LoggerInterface $logger,
        ItinerarioManager $itinerarioManager,
        NotificadorP44Manager $notificadorP44Manager,
        GeocoderManager $geocoderManager,
        LogisticaManager $logisticaManager
    ) {
        $this->geocoderManager = $geocoderManager;
        $this->logger = $logger;
        $this->itinerarioManager = $itinerarioManager;
        $this->logisticaManager = $logisticaManager;
        $this->notificadorP44Manager = $notificadorP44Manager;
    }

    /**
     * @Route("/ws/reply/p44/position", name="reply_p44_position", methods={"GET"})
     */
    public function replyPositionAction(Request $request)
    {
        //obtener el token para todos los envios
        $token =  $this->notificadorP44Manager->obtenerToken();
        $this->logP44('Token -> ' . $token);
        //$token = true;
        if ($token) {  //tengo token entonces debo enviar, sino no hacer nada.

            //obtener los itinerarios de P44 que esten nuevos o activos.
            $P44 = $this->logisticaManager->find(self::LOGISTICA_P44);
            $estados = [Itinerario::ESTADO_NUEVO, Itinerario::ESTADO_ENCURSO];
            $itinerarios = $this->itinerarioManager->findAllByLogistica($P44, $estados);
            $this->logP44('Itinerarios -> ' . count($itinerarios));
            //por cada itinerario armar la data
            foreach ($itinerarios as $itinerario) {
                //puedo tener varios servicios para el itinerario por lo que tengo que mandar para cada uno.
                foreach ($itinerario->getServicios() as $ItServicio) {
                    //armo la data para el itinerario en cuestion
                    $jsonP44 = json_encode($this->toArrayDataP44($itinerario, $ItServicio->getServicio()));
                    // dd($jsonP44);
                    $this->logP44('Json -> ' . $jsonP44);
                    //enviar la data
                    $notificarP44 = $this->notificadorP44Manager->sendData($jsonP44, $token);
                    $this->logP44('Result -> ' . $notificarP44);
                }
            }
            $status = self::STATUS_OK;
        } else {
            $this->logP44('Sin token');
            $status = self::STATUS_ERROR_DATA;
        }

        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    private function logP44($str)
    {
        if ($this->logear) {
            $this->logger->notice('replyP44 | ' . $str);
        }
    }

    private function toArrayDataP44($itinerario, $servicio)
    {
        //obtengo y parseo la fecha actual.
        $fecha = new \DateTime();
        $shipmentIdentifiers = [
            'type' => 'BILL_OF_LADING',
            "value" => $itinerario->getReferencia_externa()
        ];
        return [
            'shipmentIdentifiers' => array($shipmentIdentifiers),
            "latitude" => $servicio->getUltLatitud(),
            "longitude" => $servicio->getUltLongitud(),
            "utcTimestamp" => $servicio->getUltfechahora() != null ? $servicio->getUltfechahora()->format('Y-m-d') . 'T' . $servicio->getUltfechahora()->format('H:i:s') : null,
            'customerId' => $itinerario->getCliente_externo()
        ];
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
