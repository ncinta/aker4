<?php

namespace App\Controller\ws;

use App\Entity\informe\Inbox;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\BackendWsManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\informe\InformeManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InformeController extends AbstractController
{

    private $referencias = null;
    private $backend;

    const STATUS_OK = 0;
    const ERROR_INTERNO = 1;
    const ERROR_FALTAN_DATOS = 2;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_INTERNO => 'Error interno',
        self::ERROR_FALTAN_DATOS=> 'Error, faltan datos uid o estado',

    );

    /**
     * 
       {
        "uid": "2f6a8cef-ae0b-4af7-a731-9ab156f0bdc2",
        "estado": "en-progreso",
        "fecha": "2024-07-14T10:00:00Z",
        "error": null
        }
     */

    /**
     * @Route("/ws/info/setstatus", name="webservice_info_setstatus")
     * @Method({"GET", "POST"})
     */
    public function statusAction(
        Request $request,
        InformeManager $informeManager
    ) {
        $data = json_decode($request->getContent(),true);
        $uid = isset($data['uid']) ? $data['uid'] : null;
        $estado = isset($data['estado']) ? $data['estado'] : null;
        $fecha = isset($data['fecha']) ? $data['fecha'] : null; //no creo que la usemos, seguramente utilicemos el updated_at
        $error = isset($data['error']) ? $data['error'] : null;
        $fecha = new \DateTime($fecha);
        $status =  self::ERROR_FALTAN_DATOS;
        if ($uid == null || $estado == null) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $informe = $informeManager->findByUid($uid);
            if ($informe != null) {
                if ($estado == 'publicado' || $estado == 'en_progreso') {
                    $informe->setEstado(Inbox::ESTADO_EN_PROGRESO);
                } else { //finalizado
                    $informe->setEstado(Inbox::ESTADO_FINALIZADO);
                    if ($error != null) { //finalizó con error
                        $informe->setError($error);
                    }
                }
                $informe = $informeManager->save($informe);
                $status = $informe ? self::STATUS_OK : self::ERROR_INTERNO;
                
            }
        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);

        return $this->generateResponse($status, $respuesta);
    }


    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }


}
