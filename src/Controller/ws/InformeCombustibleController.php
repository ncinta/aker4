<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InformeCombustibleController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;
    private $backend;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    /**
     * @Route("/ws/info/combust/totalcargas", name="webservice_info_combust")
     * @Method({"GET", "POST"})
     */
    public function totalcargasAction(Request $request, OrganizacionManager $organizacionManager, ServicioManager $servicioManager)
    {
        $apikey = $request->get('apiKey');
        $fdesde = $request->get('desde');
        $fhasta = $request->get('hasta');
        $serv = $request->get('servicio');
        $servId = $request->get('servicioId');
        $status = true;
        $informe = null;
        if (is_null($fdesde) || is_null($fhasta) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $fdesde = new \DateTime($fdesde);
            //            die('////<pre>' . nl2br(var_export($fdesde, true)) . '</pre>////');
            $fhasta = new \DateTime($fhasta);
            $organizacion = $organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    if (!is_null($servId)) {
                        $servicios[] = $servicioManager->find($servId);
                    } elseif (!is_null($serv)) {
                        $servicios[] = $servicioManager->findByNombre($serv);
                    } else {
                        $servicios = $servicioManager->findAllEnabledByOrganizacion($organizacion, false);
                    }

                    //obtengo la información
                    $informe = array();
                    foreach ($servicios as $servicio) {
                        if ($servicio->getVehiculo() != null) {
                            $em = $this->getEntityManager();
                            $cargas = $em->getRepository('App:CargaCombustible')
                                ->findAllCargas($servicio, $fdesde->format('Y-m-d H:i:s'), $fhasta->format('Y-m-d H:i:s'));

                            //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                            $totalImp = $totalLts = 0;
                            foreach ($cargas as $carga) {
                                $totalLts = +$carga->getLitrosCarga();
                                $totalImp = +$carga->getMontoTotal();
                            }
                            if ($totalImp !== 0 || $totalLts !== 0) {
                                $informe[] = array(
                                    'id' => $servicio->getId(),
                                    'servicio' => $servicio->getNombre(),
                                    'litros' => $totalLts,
                                    'importe' => $totalImp,
                                );
                            }
                        }
                    }
                    //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                    $status = self::STATUS_OK;
                } else {
                    $status = self::ERROR_ORGANIZACION;
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($informe)) {
            $respuesta['data'] = $informe;
        }
        return $this->generateResponse($status, $respuesta);
    }

    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }
}
