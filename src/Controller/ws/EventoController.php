<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Panico;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\app\EventoManager;
use App\Model\app\UtilsManager;
use App\Model\app\EquipoManager;
use App\Model\app\ServicioManager;
use App\Model\app\PanicoManager;
use App\Model\app\EventoHistorialManager;
use App\Entity\EventoHistorialNotificacion as EventoHistorialNotificacion;
use App\Model\app\GrupoReferenciaManager;
use App\Model\monitor\EventoItinerarioManager;
use App\Model\app\EventoTemporalManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\monitor\NotificadorP44Manager;
use App\Model\app\ReferenciaManager;
use App\Model\monitor\ItinerarioManager;
use Psr\Log\LoggerInterface;
use App\Model\pae\EventoPaeManager;

class EventoController extends AbstractController
{

    const EMPRESA_MYTMC = 33;
    const EMPRESA_MYTMC_PUERTO = 229;

    const LOGISTICA_CHROBINSON = 5;
    const LOGISTICA_P44 = 17;   //VER QUE ID TIENE

    const STATUS_OK = 0;
    const STATUS_ERROR_DATA = 1;
    const STATUS_ERROR_SINDATA = 2;
    const STATUS_ERROR_SINEVENTOID = 3;
    const STATUS_ERROR_SINSERVICIOID = 4;
    const STATUS_ERROR_SINTITULO = 5;
    const STATUS_ERROR_GRABACION = 6;
    const STATUS_ERROR_FECHA = 7;
    const STATUS_ERROR_SINUID = 8;

    private $utils;
    private $eventoManager;
    private $eventoHistorialManager;
    private $servicioManager;
    private $grpRefManager;
    private $eventoItinerarioManager;
    protected $evTemporalManager;
    private $logger;
    private $notificadorAgenteManager;
    private $notificadorP44Manager;
    private $referenciaManager;
    private $itinerarioManager;
    private $eventoPaeManager;
    private $logear = true;

    public function __construct(
        UtilsManager $utilsManager,
        EventoManager $eventoManager,
        ServicioManager $servicioManager,
        GrupoReferenciaManager $grpManager,
        EventoItinerarioManager $evItManager,
        EventoTemporalManager $evTemporalManager,
        EventoHistorialManager $evHistorialManager,
        LoggerInterface $logger,
        NotificadorAgenteManager $notificadorAgenteManager,
        NotificadorP44Manager $notificadorP44Manager,
        ReferenciaManager $referenciaManager,
        ItinerarioManager $itinerarioManager,
        EventoPaeManager $eventoPaeManager
    ) {
        $this->utils = $utilsManager;
        $this->eventoManager = $eventoManager;
        $this->eventoHistorialManager = $evHistorialManager;
        $this->servicioManager = $servicioManager;
        $this->grpRefManager = $grpManager;
        $this->eventoItinerarioManager = $evItManager;
        $this->evTemporalManager = $evTemporalManager;
        $this->logger = $logger;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->notificadorP44Manager = $notificadorP44Manager;
        $this->referenciaManager = $referenciaManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->eventoPaeManager = $eventoPaeManager;
    }

    private function log($str)
    {
        if ($this->logear) {
            $this->logger->notice('ws/evento/add | ' . $str);
        }
    }

    /**
     * @Route("/ws/evento/add", name="evento_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) //los panicos tambien entran por aca. 
    {
        $data = utf8_encode($request->getContent());
        //$data = $request->getContent();
        $status = self::STATUS_OK;
        if ($data) {
            $this->log("data ->" . $data);
            //$data = '{"key": "key1234","fecha_utc": "2014-09-17 13:29:00","titulo": "CRPJ84 exceso velocidad","evento_id": "26","servicio_id": "205","data": {"cuerpo": "<h3>EXCESO +90 KMS/H</h3><br><b>Vehiculo:</b> CRPJ84<br><b>Tiempo en infracccion:</b> -199016:-39:-25 aprox.<br><b>Referencia:</b> ---<br><br><b>INICIO INFRACCION</b><br><b>Fecha y hora:</b> 30/12/1899 04:00:00<br><b>Velocidad:</b> 0 kms/h<br><b>Ubicación:</b> <br><br><b>FIN INFRACCCION</b><br><b>Fecha y hora:</b> 23/05/2013 17:48:51<br><b>Velocidad:</b> 64 kms/h<br><b>Ubicación:</b> <br><br>ULTIMO REPORTE<br>Fecha y Hora: 23/05/2013 17:48:51<br>Velocidad: 64<br>Dirección: 2º<br>Posición Válida: SI <br>Ubicación: <br>Odometro: 133603 kms<br>"}}';
            $form = json_decode($data, true);
            
            if (!is_null($form)) {
                if (isset($form['fecha_utc'])) {
                    $fecha = new \DateTime($form['fecha_utc'], new \DateTimeZone(('UTC')));   //tiene que estar en hora utc                    
                } else {
                    $status = self::STATUS_ERROR_FECHA;
                }
                $referencia_id = null;
                isset($form['evento_id']) ? $evento_id = $form['evento_id'] : $status = self::STATUS_ERROR_SINEVENTOID;
                isset($form['servicio_id']) ? $servicio_id = $form['servicio_id'] : $status = self::STATUS_ERROR_SINSERVICIOID;
                isset($form['titulo']) ? $titulo = $form['titulo'] : $status = self::STATUS_ERROR_SINTITULO;
                isset($form['data']) ? $datos = $form['data'] : $status = self::STATUS_ERROR_SINDATA;

                //agrega el payload a la data del eventohistorico
                if (isset($form['payload'])) {
                    $datos = array_merge($datos, $form['payload']);                    
                } //agrega el payload a la data del eventohistorico

                //agrega la data de la notificacion a la data del eventohistorico
                if (isset($form['notificacion'])) {
                    $datos = array_merge($datos, $form['notificacion']);
                }               

                if ($status == self::STATUS_OK) {
                    $this->log("form: evento_id:" . $evento_id . " servicio_id:" . $servicio_id . " titulo:" . $titulo);

                    //busco evento y servicio
                    $evento = $this->eventoManager->findById(intval($evento_id));
                    // die('////<pre>'.nl2br(var_export($evento, true)).'</pre>////');
                    if (is_null($evento)) {   //controlo que el evento exista
                        $status = self::STATUS_ERROR_SINEVENTOID;
                        $this->log("Evento no encontrado, se paso evento_id:" . $evento_id);
                    } else {

                        if ($evento->getClase() == 3 && !$evento->getActivo()) { //si es evento de itinerario y no está activo que no proceda
                            //  die('////<pre>'.nl2br(var_export('inactivo', true)).'</pre>////');
                            $this->log("Evento inactivo por el usuario o por el estado del itinerario:");
                            $respuesta = array(0 => self::STATUS_OK);
                            return new Response(json_encode($respuesta), 200);
                        }

                        if ($status == self::STATUS_OK) {
                            $servicio = $this->servicioManager->find($servicio_id * 1); //controlo que el servicio exista
                            if (is_null($servicio)) {
                                $status = self::STATUS_ERROR_SINSERVICIOID;
                                $this->log("Servicio no encontrado, se paso servicio_id:" . $servicio_id);
                            }
                        }

                        if ($status == self::STATUS_OK) {  //encontre servicio y evento -> genero las notificaciones
                            $historial = $this->eventoHistorialManager->add($fecha, $evento, $servicio, $titulo, $datos);
                            if ($historial) {
                                $status = self::STATUS_OK;
                                //   $this->log("Grabado en evento ok. eventohistorial_id:" . $historial->getId());
                            } else {
                                $status = self::STATUS_ERROR_GRABACION;
                                $this->log("Grabado Error de Historial");
                            }

                            //aca debo determinar si es un evento itinerario y meterlo en el itinerario correspondiente.                        
                            if ($evento->getClase() == 3) {   //itinerario
                                if (!$evento->getActivo()) { //si no está activo chau que no procese nada
                                    $this->log("Status final->" . $status);
                                    $respuesta = array(0 => $status);
                                    return new Response(json_encode($respuesta), 200);
                                }
                                $evIt = $this->eventoItinerarioManager->findByEventoServicio($evento);  //busco el eventoitinerario
                                if ($evIt) {
                                    //grabo en el historial
                                    $evItHistorial = $this->eventoItinerarioManager->add($fecha, $evento, $servicio, $titulo, $datos);
                                    $itinerario = $evIt->getItinerario();
                                    $logistica = $itinerario->getLogistica();
                                    $tipoEvento = $evIt->getTipoEvento();
                                    //genero la notificacione en pantalla (por default itinerario siempre)
                                    $this->evTemporalManager->add2Itinerario($fecha, $titulo, $servicio, $itinerario, $evItHistorial, $evento, $historial);

                                    if (!is_null($logistica) && $logistica->getId() == self::LOGISTICA_CHROBINSON && $tipoEvento->getCodename() == 'EV_IO_REFERENCIA') { //es chrobinson y es E/S, tenemos que reportar el evento
                                        $this->notificarCHR($datos, $itinerario, $evento, $fecha);
                                    } elseif (!is_null($logistica) && $logistica->getId() == self::LOGISTICA_P44 && $tipoEvento->getCodename() == 'EV_IO_REFERENCIA') { //es p44 y es E/S, tenemos que reportar el evento
                                        // no hacer notificacion por el momento de los I/O de referencias a P44. 
                                        // $this->notificarP44($datos, $itinerario, $evento, $fecha);
                                    }
                                }
                            } else {  //es un evento normal... no de itinerario                           
                                //tengo que armar las notificaciones en caso de que se tenga que mostrar por pantalla
                                if ($evento->getNotificacionWeb() != null && $evento->getNotificacionWeb() != 0) { //hay que notificar. entonces lo agregamos al temporal                                
                                    $this->evTemporalManager->add2evento($fecha, $titulo, $evento, $servicio, $historial);
                                    $this->log("Grabado evTemporal.");
                                }
                            }

                            //aca se notifica si es PAE                            
                            if ($this->eventoPaeManager->isPae($evento)) {                                
                                $this->log("Evento para PAE");
                                if (isset($form['payload'])) {
                                    $resp = $this->eventoPaeManager->procesarNotificacion($evento, $servicio, $fecha, $form['payload']);                                    
                                    $this->eventoPaeManager->grabarRespuesta($evento, $historial, $resp);
                                } else {
                                    $this->log("No hay payload en la data");
                                }
                            }
                        }
                    }
                }
            } else {
                $status = self::STATUS_ERROR_DATA;
                $this->log("Sin data ->" . $data);
            }
        } else {
            $status = self::STATUS_ERROR_SINDATA;
            $this->log("Error data ->" . $data);
        }
        //$this->log("Error data ->" . $data);
        //dd($data);        
        $this->log("Status final->" . $status);
        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }



    private function notificarCHR($datos, $itinerario, $evento, $fecha)
    {
        if (isset($datos['data']['hacia']) || isset($datos['data']['desde'])) { //puede que no venga ninguno de los 2 
            $referencia_id = isset($datos['data']['hacia']) ?
                $datos['data']['hacia'][0]['id'] :
                $datos['data']['desde'][0]['id']; //viene algono de los 2 
        }
        $referencia = $referencia_id ? $this->referenciaManager->find(intval($referencia_id)) : null;
        $poi = $referencia ? $this->itinerarioManager->findPoiByItiRef($itinerario, $referencia) : null;

        $this->logCHR('Send CHR -> itId:' . $itinerario->getId());
        $token =  $this->notificadorAgenteManager->getTokenChr();
        if ($token) { //si no tiene token es en vano enviar
            //  die('////<pre>'.nl2br(var_export($token, true)).'</pre>////');
            $this->logCHR('Token -> ' . $token);
            $jsonCHR = json_encode($this->toArrayDataChr($itinerario, $evento, $poi, $fecha, $datos['mensaje']));
            $this->logCHR('Json -> ' . $jsonCHR);
            $notificarChr = $this->notificadorAgenteManager->sendDataChr($jsonCHR, $token);
            $this->logCHR('Result -> ' . $notificarChr);
        }
    }

    private function notificarP44($datos, $itinerario, $evento, $fecha)
    {

        if (isset($datos['data']['hacia']) || isset($datos['data']['desde'])) { //puede que no venga ninguno de los 2 
            $referencia_id = isset($datos['data']['hacia']) ?
                $datos['data']['hacia'][0]['id'] :
                $datos['data']['desde'][0]['id']; //viene alguno de los 2 
        }
        $referencia = $referencia_id ? $this->referenciaManager->find(intval($referencia_id)) : null;
        $poi = $referencia ? $this->itinerarioManager->findPoiByItiRef($itinerario, $referencia) : null;

        $this->logP44('Send P44 -> itId:' . $itinerario->getId());
        $token =  $this->notificadorP44Manager->obtenerToken();
        //dd($token);
        if ($token) { //si no tiene token es en vano enviar            
            //$this->logP44('Token -> ' . $token);
            //$jsonCHR = json_encode($this->notificadorP44Manager->toArrayData($itinerario, $evento, $poi, $fecha, $datos['mensaje']));
            //$this->logP44('Json -> ' . $jsonCHR);
            //$notificarChr = $this->notificadorP44Manager->sendDataChr($jsonCHR, $token);            
            //$this->logP44('Result -> ' . $result);
        }
        $result = '';
    }

    const EVENTO_ES_ENTRADA = 'EV_IO_REFERENCIA:INGRESO';
    const EVENTO_ES_SALIDA = 'EV_IO_REFERENCIA:SALIDA';

    private function toArrayDataChr($itinerario, $evento, $poi = null, $fecha = null, $mensaje = null)
    {
        $transitType = "Road";
        $location = null;
        $ciudad = '---';
        $pcia = '---';
        $shipmentIdentifier = array(
            "shipmentNumber" => $itinerario->getCodigo(), //codigo de itinerario
        );
        $dateTime = array(
            "eventDateTime" => $fecha ? $fecha->format('Y-m-d') . 'T' . $fecha->format('H:i:s.v') . 'Z' : null
        );
        if ($poi) {
            $stopType = trim($poi->getStopType());
            $this->logCHR('stopType -> ' . $stopType);
            if ($stopType == '') {
                $stopType = 'D';
            }
            if ($mensaje == self::EVENTO_ES_ENTRADA) {  //entrada
                if ($stopType == 'D') {
                    $eventoCode = 'X1';  //llegada a destino (drop)
                } else {
                    $eventoCode = 'X3';  //llegada al origen (pick)
                }
            } elseif ($mensaje == self::EVENTO_ES_SALIDA) {
                if ($stopType == 'D') {
                    $eventoCode = 'D1';    //salida del destino (drop)
                } else {
                    $eventoCode = 'AF';    //salida del origen (pick)
                }
            } else {
                $stopType = 'D';
                $eventoCode = 'X1'; //drop x1 o pick x3
            }
            $referencia = $poi->getReferencia();
            if ($referencia) {
                $location = array(
                    "type" => $stopType, //drop o pick
                    "stopSequenceNumber" => $poi->getOrden(),
                    "name" => $referencia->getNombre(), //nombre de la referencia
                    "locationId" => $referencia->getCodigoExterno(), //WarehoseCode
                    "address" =>  array(
                        "city" => $referencia->getCiudad() ? $referencia->getCiudad() : "", // 
                        "stateProvinceCode" => $referencia->getProvincia() ?  $referencia->getProvincia()->getCode() : "", //BS AS
                        "country" => "AR"
                    )
                );
            }
        }

        return array(
            'eventCode' => $eventoCode,
            'shipmentIdentifier' => $shipmentIdentifier,
            'dateTime' => $dateTime,
            'location' => $location
        );
    }

    /**
     * @Route("/ws/evento/notificar", name="evento_notificar", methods={"POST"})
     */
    public function notificarAction(Request $request)
    {
        $data = utf8_encode($request->getContent());
        //$data = $request->getContent();

        $status = self::STATUS_ERROR_SINDATA;
        if ($data) {
            //$data = '{"key": "key1234","fecha": "2014-09-17 13:29:00","titulo": "CRPJ84 exceso velocidad","evento_id": "26","servicio_id": "205","data": {"cuerpo": "<h3>EXCESO +90 KMS/H</h3><br><b>Vehiculo:</b> CRPJ84<br><b>Tiempo en infracccion:</b> -199016:-39:-25 aprox.<br><b>Referencia:</b> ---<br><br><b>INICIO INFRACCION</b><br><b>Fecha y hora:</b> 30/12/1899 04:00:00<br><b>Velocidad:</b> 0 kms/h<br><b>Ubicación:</b> <br><br><b>FIN INFRACCCION</b><br><b>Fecha y hora:</b> 23/05/2013 17:48:51<br><b>Velocidad:</b> 64 kms/h<br><b>Ubicación:</b> <br><br>ULTIMO REPORTE<br>Fecha y Hora: 23/05/2013 17:48:51<br>Velocidad: 64<br>Dirección: 2º<br>Posición Válida: SI <br>Ubicación: <br>Odometro: 133603 kms<br>"}}';
            $form = json_decode($data, true);
            if (!is_null($form)) {
                if (isset($form['fecha'])) {
                    $fecha = new \DateTime($form['fecha'], new \DateTimeZone(('UTC')));   //tiene que estar en hora utc                    
                } else {
                    $status = self::STATUS_ERROR_FECHA;
                }
                isset($form['evento_id']) ? $evento_id = $form['evento_id'] : $status = self::STATUS_ERROR_SINEVENTOID;
                isset($form['servicio_id']) ? $servicio_id = $form['servicio_id'] : $status = self::STATUS_ERROR_SINSERVICIOID;
                isset($form['uid']) ? $uid = $form['uid'] : $status = self::STATUS_ERROR_SINUID;

                //debo obtener el eventohistorialnotificacion unico para el UID, tipo de evento y contacto en cuestion

                //debo procesar si viene con tokens
                //dd($form);
                if (isset($form['notificacion'])) {
                    // $canal = $form['notificacion']['tipo'];  
                    $evento = $this->eventoManager->findById($evento_id);
                    if (isset($form['notificacion']['tokens'])) {
                        foreach ($form['notificacion']['tokens'] as $token) {
                            $notificaciones = $this->eventoHistorialManager->findNotificacion($uid, $token, EventoHistorialNotificacion::CANAL_PUSH, $evento);
                            // dd($notificacion);
                            if ($notificaciones) {
                                if ($notificaciones) {
                                    foreach ($notificaciones as $notific) {
                                        $this->eventoHistorialManager->updateNotificacion(EventoHistorialNotificacion::ESTADO_ENVIADO, $notific, $fecha);
                                    }
                                }
                            }
                        }
                    }
                    unset($notificaciones);
                    if (isset($form['notificacion']['emails'])) {
                        foreach ($form['notificacion']['emails'] as $email) {
                            $notificaciones = $this->eventoHistorialManager->findNotificacion($uid, $email, EventoHistorialNotificacion::CANAL_EMAIL, $evento);
                            if ($notificaciones) {
                                foreach ($notificaciones as $notific) {
                                    $this->eventoHistorialManager->updateNotificacion(EventoHistorialNotificacion::ESTADO_ENVIADO, $notific, $fecha);
                                }
                            }
                        }
                    }
                }
                $status = self::STATUS_OK;
            }
        }
        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }


    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/ws/evento/{id}/get", name="evento_get",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET"})
     */
    public function getAction(Request $request, $id)
    {
        $evento = $this->eventoManager->findById($id);
        $data = array(
            'id' => $evento->getId(),
            'nombre' => $evento->getNombre(),
            'activo' => $evento->getActivo(),
            'lunes' => $evento->getLunes(),
            'martes' => $evento->getMartes(),
            'miercoles' => $evento->getMiercoles(),
            'jueves' => $evento->getJueves(),
            'viernes' => $evento->getViernes(),
            'sabado' => $evento->getSabado(),
            'domingo' => $evento->getDomingo(),
            'hora_desde' => $evento->getHoraInicio(),
            'hora_hasta' => $evento->getHoraFin(),
            'created_at' => $evento->getCreatedAt(),
            'updated_at' => $evento->getUpdatedAt(),
            'registrar' => $evento->getRegistrar(),
            'organizacion' => array(
                'id' => $evento->getOrganizacion()->getId(),
                'nombre' => $evento->getOrganizacion()->getNombre(),
            ),
            'parametros' => $this->getParametros($evento),
            'servicios' => $this->getServicios($evento),
        );

        return new JsonResponse($data, 200);
        //return new Response($data, 200);
    }

    private function getServicios($ev)
    {
        $serv = null;
        foreach ($ev->getServicios() as $p) {
            if ($p->getEquipo() != null) {
                $serv[] = array(
                    'id' => $p->getId(),
                    'nombre' => $p->getNombre(),
                    'mdmid' => $p->getEquipo()->getMdmid(),
                    'patente' => !is_null($p->getVehiculo()) ? $p->getVehiculo()->getPatente() : '',
                );
            }
        }
        return $serv;
    }

    private function getParametros($ev)
    {
        $param = null;
        foreach ($ev->getParametros() as $p) {
            if ($p->getVariable()->getDataType() == 'ENTITY') {  //es un grp de referencia
                $idRef = $p->getValor();
                $grpR = $this->grpRefManager->find($idRef);
                $grupo['id'] = $grpR->getId();
                $grupo['nombre'] = $grpR->getNombre();
                $ref = null;
                foreach ($grpR->getReferencias() as $value) {
                    $ref[] = array(
                        'id' => $value->getId(),
                        'nombre' => $value->getNombre(),
                    );
                }
                $grupo['referencias'] = $ref;
            }

            $param[] = array(
                'id' => $p->getId(),
                'valor' => $p->getValor(),
                'codename' => $p->getVariable()->getCodename(),
                'tipoVariable' => $p->getVariable()->getDataType(),
                'unidadMedida' => $p->getVariable()->getUnidadMedida(),
                'cotaMinima' => $p->getVariable()->getCotaMinima(),
                'cotaMaxima' => $p->getVariable()->getCotaMaxima(),
                'grupoReferencia' => isset($grupo) ? $grupo : null,
                //'valorDefault' => $p->getVariable()->getValorDefault(),
            );
        }
        return $param;
    }

    //    {
    //	"id": 1,
    //	"organizacion": {
    //		"id": 12,
    //		"nombre": "Algo SA"
    //	},
    //	"tipoevento": {
    //		"id": 12,
    //		"nombre": "Panico",
    //		"codename": "EV_PANICO"
    //	},
    //	"nombre": "panico de autos",
    //	"activo": true,
    //	"lunes": true,
    //	"martes": true,
    //	"miercoles": true,
    //	"jueves": true,
    //	"viernes": true,
    //	"sabado": true,
    //	"domingo": true,
    //	"hora_desde": "hh:mm:ss",
    //	"hora_hasta": "hh:mm:ss",
    //	"created_at": "Y-m-d H:i:s",
    //	"updated_at": "Y-m-d H:i:s",
    //	"registrar": true,
    //	"parametros": [{
    //		"id": 123,
    //		"valor": "cualquier valor del parametro",
    //		"variableevento": {
    //			"id": 1234,
    //			"nombre": "nombre de la variable",
    //			"codename": "VAR_XXXX",
    //			"datatype": "string",
    //			"cotaminima": "5",
    //			"cotamaxima": "100",
    //			"unidadmedida": "kms/h",
    //
    //			"gruporeferencia": {
    //				"id": 1234,
    //				"nombre": "Bases",
    //				"referencias": [{
    //					"id": 3333,
    //					"nombre": "Base 1",
    //					"tipo": 1,
    //					"radio": 50,
    //					"velocidadmaxima": 60,
    //					"poligono": "xxxxx"
    //				}]
    //			}
    //		}
    //	}],
    //	"servicios": [{
    //		"id": 123456,
    //		"nombre": "Fiat fiorino",
    //		"mdmid": "RN:ASDBBE"
    //	}],
    //	"notificacion": [{
    //		"id": 12344,
    //		"tipo": 2,
    //		"nombre": "Juan Perez",
    //		"email": "juanperez@gmail.com",
    //		"celular": "123456788",
    //		"noticeme": "12345675"
    //	}]
    //}

    /**
     * @Route("/ws/evento/list", name="evento_list")
     * @Method({"GET", "POST"})
     */
    public function listAction()
    {
        $em = $this->getEntityManager();
        $eventos = $em->getRepository('App:EventoHistorial')->findAll();

        return array(
            'eventos' => $eventos,
        );
    }


    /**
     * @Route("/ws/evento/panico", name="evento_panico")
     * @Method({"GET", "POST"})
     */
    public function panicoAction(Request $request, EquipoManager $equipoManager, PanicoManager $panicoManager)
    {
        //$data = '{"key": "key1234","fecha": "2013-08-27 15:59:41", "mdmid": "CF:2371:1724", "mensaje": "Panico de ....", "latitud": "-32.3655", "longitud": "-68.33211", "domicilio": "San Martin algo al 500, Mendoza"}';

        $data = utf8_encode($request->getContent());
        $status = true;
        if ($data) {
            // die('////<pre>'.nl2br(var_export(json_decode($data, true), true)).'</pre>////');
            $form = json_decode($data, true);
            if (!is_null($form)) {
                if (isset($form['fecha'])) {
                    $fecha = new \DateTime($form['fecha'], new \DateTimeZone(('UTC')));   //tiene que estar en hora utc
                    $fecha->setTimezone(new \DateTimeZone('UTC'));
                } else {
                    $status = self::STATUS_ERROR_FECHA;
                }
                //validacion de mdmid
                if (isset($form['mdmid'])) {
                    $equipo = $equipoManager->findByMdmid($form['mdmid']);
                    if ($equipo && $equipo->getServicio()) {
                        $servicio = $equipo->getServicio();
                    } else {
                        $status = self::STATUS_ERROR_SINSERVICIOID;
                    }
                    //                    die('////<pre>' . nl2br(var_export($servicio->getNombre(), true)) . '</pre>////');
                } else {
                    $status = self::STATUS_ERROR_SINEVENTOID;
                }
                if ($status === true) {
                    $panico = new Panico();
                    $panico->setServicio($servicio);
                    $panico->setFechaPanico($fecha);
                    $panico->setMensaje(isset($form['mensaje']) ? $form['mensaje'] : '');
                    $panico->setUltLatitud(isset($form['latitud']) ? intval($form['latitud']) : null);
                    $panico->setUltLongitud(isset($form['longitud']) ? intval($form['longitud']) : null);
                    $panico->setDomicilio(isset($form['domicilio']) ? $form['domicilio'] : null);

                    if ($panicoManager->save($panico)) {
                        $status = self::STATUS_OK;
                    } else {
                        $status = self::STATUS_ERROR_GRABACION;
                    }
                }
            } else {
                $status = self::STATUS_ERROR_SINDATA;
            }
        } else {
            $status = self::STATUS_ERROR_DATA;
        }

        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    private function logCHR($str)
    {
        if ($this->logear) {
            $this->logger->notice('CHR | ' . $str);
        }
    }

    private function logP44($str)
    {
        if ($this->logear) {
            $this->logger->notice('P44 | ' . $str);
        }
    }
}

/**
{
	"cuerpo": "<strong>Veh\u00c3\u00adculo<\/strong>: Iveco 154<br><strong>Evento<\/strong>: SERVICIOS PUBLICOS<br><strong>Referencia<\/strong>: PUENTE JUSTO DARACT  Y 147 [52017]<br><strong>Fecha Salida<\/strong>: 03\/07\/2023 10:05:39<br><strong>Fecha Actual<\/strong>: 03\/07\/2023 10:05:42<br>",
	"uid": "cc78e0cd-6e93-480b-8887-d3305ae535c4",
	"mensaje": "EV_IO_REFERENCIA:SALIDA",
	"version": "1.0",
	"data": {
		"desde": [{
			"id": 52017,
			"nombre": "PUENTE JUSTO DARACT  Y 147"
		}],
		"posicion": {
			"direccion": 6,
			"latitud": -33.25509,
			"longitud": -66.33875,
			"velocidad": 67
		}
	},
	"estado": {
		"sin_destinatarios": false,
		"middleware": []
	},
	"emails": [],
	"tokens": [{
		"id": 1567,
		"nombre": "Ovejero Fernando",
		"token": "cgrtEKNOSGmolyHIDxFg9R:APA91bHP8G_vI-UMg-09lbEwbqgkHpN2auGQG458riuqZZRqDzd9_c5AU5RQrjh4MA0yQzLYJt609_Te9oEh89kkwnZcZp-P3wLPPAi1GoEaxVkXuQK2PPmoOiGoxGILIvucV1Z3JTfK"
	}, {
		"id": 1605,
		"nombre": "Haller Eugenio",
		"token": "fHmGR_mcSJmOFbi4l5JN6i:APA91bH0qkEQGmt5mOPgNjTjfEFtxT3pDrNeyJk-W8LFhlaVix2HCaeq5AwD-B6s8mj1T4eVZKy9sx5EyIsTZb1wzTl7B93yXQ7DzOFwFc0SYDOMEEHr_7IsvVhQBLQ95ebmJDp0Bmbg"
	}, {
		"id": 1571,
		"nombre": "DIEZ BRUNO",
		"token": "ebrnQjaxSRee75J8a7_VvE:APA91bHIDBtBFEhmCiRRs989FWKSuj3KRp0SlNnflUi2n8NP4mUBzHH6EnDNvYLw7bO7BPii_xOK5Jzk1-pp3AIyAHiTN9lL5CU6NKZPmnSi4X90_4wNZrtz-UxmzmFIpCuYc8LXvtB8"
	}, {
		"id": 1573,
		"nombre": "GARCIA RUBEN",
		"token": "eLH8bQhWRvyASalxHNhztg:APA91bGlkLxtyptPbHcMBvRB89wR65gvr724UugHj_V4_Z2dPjhrC5azPrMrJkNFR-mgotzhVsiqP97DbyA7Fv8F1C2DdNaQ6tCwHeFOGwxPYulH-l41RFHJ8lAdoFvevJp02hP6tird"
	}]
}
 */
