<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\BackendWsManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class InformePortalController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;
    private $backend;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );

    /**
     * @Route("/ws/info/portico", name="webservice_info_portico")
     * @Method({"GET", "POST"})
     */
    public function porticoAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        ServicioManager $servicioManager
    ) {

        $apikey = $request->get('apiKey');
        $fdesde = $request->get('desde');
        $fhasta = $request->get('hasta');
        $detalle = !is_null($request->get('detalle')) && $request->get('detalle') == 'true';
        $serv = $request->get('servicio');
        $servId = $request->get('servicioId');
        $status = true;
        $informe = null;
        if (is_null($fdesde) || is_null($fhasta) || is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $fdesde = new \DateTime($fdesde);
            $fhasta = new \DateTime($fhasta);
            $organizacion = $organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    $this->setBackend($organizacion);   //seteo el backend.
                    if (!is_null($servId)) {
                        $servicios[] = $servicioManager->find($servId);
                    } elseif (!is_null($serv)) {
                        $servicios[] = $servicioManager->findByNombre($serv);
                    } else {
                        $servicios = $servicioManager->findAllEnabledByOrganizacion($organizacion, false);
                    }

                    //obtengo la información
                    $informe = array();
                    foreach ($servicios as $servicio) {
                        if ($servicio->getVehiculo() != null) {
                            $data = $this->getDataPortico($organizacion, $servicio, $fdesde->format('Y-m-d H:n:s'), $fhasta->format('Y-m-d H:n:s'));
                            //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                            $dataServ['servicio'] = $servicio->getNombre();
                            $dataServ['patente'] = $servicio->getVehiculo()->getPatente();
                            $dataServ['cantidad'] = count($data);
                            $dataServ['precio_total'] = $this->getObtenerTotal($data);

                            if ($detalle) {  ///pone el detalle de los pasos por porticos.
                                $dataServ['data'] = $data;
                            }
                            $informe[] = $dataServ;
                        }
                        //die('////<pre>' . nl2br(var_export($apikey, true)) . '</pre>////');
                    }
                    $status = self::STATUS_OK;
                } else {
                    $status = self::ERROR_ORGNIZACION;
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }
        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($informe)) {
            $respuesta['data'] = $informe;
        }
        return $this->generateResponse($status, $respuesta);
    }

    private function getObtenerTotal($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value['precio'];
        }
        return $total;
    }

    private function getDataPortico($organizacion, $servicio, $fdesde, $fhasta)
    {
        //die('////<pre>' . nl2br(var_export($servicio->getVehiculo()->getTipoVehiculo(), true)) . '</pre>////');
        $data = array();
        if (!is_null($servicio->getVehiculo()->getTipoVehiculo())) {
            $portales = $this->getEntityManager()
                ->getRepository('App:PrecioPortal')
                ->findByTipoVehiculo(0, $servicio->getVehiculo()->getTipoVehiculo());
            $portales_map = array();
            foreach ($portales as $portal) {
                $portales_map[$portal->getId()] = $portal;
            }

            //obtengo el informe
            $data = array();
            $informe = $this->backend->informePortales($servicio->getId(), $fdesde, $fhasta, $portales);
            foreach ($informe as $key => $value) {
                $portal = $portales_map[$value["precioportal_id"]];
                $data[] = array(
                    'fecha' => $value['fecha'],
                    'portal_id' => $value["precioportal_id"],
                    'portal_nombre' => $portal->getReferencia()->getNombre(),
                    'precio' => $portal->getPrecio(),
                );
            }
            unset($informe);
        }
        return $data;
    }

    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function setBackend($organizacion)
    {
        $usuario = $organizacion->getUsuarioMaster();
        $this->backend = new BackendWsManager($this->getEntityManager(), $organizacion, $this->container->getParameter('calypso.backend_addr'), $this->container->getParameter('calypso.backend_addr_backup'), $usuario);
        return $this->backend;
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }
}
