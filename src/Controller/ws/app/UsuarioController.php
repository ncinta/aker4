<?php

namespace App\Controller\ws\app;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\UsuarioManager;
use App\Model\app\ContactoManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\NotificadorAgenteManager;
use Psr\Log\LoggerInterface;

class UsuarioController extends AbstractController
{

    private $logger;

    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_APICODE = 1;
    const ERROR_FALTAN_DATOS = 2;
    const ERROR_USUARIO_NOENCONTRADO = 3;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_APICODE => 'ApiCode Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_USUARIO_NOENCONTRADO => 'Usuario no encontrado',
    );

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }


    /**
     * @Route("/ws/app/login", name="ws_app_login", methods={"GET"})     
     */
    public function loginAction(Request $request, UsuarioManager $usuarioManager)
    {
        $code = trim($request->headers->get('apicode'));
        $phone = trim($request->headers->get('phone'));
        $this->log('login -> ' . $phone);
        $datos = null;
        $status = self::ERROR_FALTAN_DATOS;
        if (is_null($phone) || is_null($code)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $usuarioManager->findByApicode($phone, $code);
            if ($usr) {
                if ($usr->getApicode() != strtoupper($code)) {
                    $status = self::ERROR_APICODE;
                    // die('////<pre>' . nl2br(var_export($status, true)) . '</pre>////');     
                } else {
                    $datos['usuarioId'] = $usr->getId();
                    $datos['nombre'] = $usr->getNombre();
                    $datos['token'] = $usr->getToken();
                    $datos['timeZone'] = $usr->getTimezone();
                    $datos['apiKey'] = $usr->getOrganizacion()->getApikey();
                    $datos['roles'] = $usr->getRoles();
                    $status = self::STATUS_OK;
                }
            } else {
                $status = self::ERROR_USUARIO_NOENCONTRADO;
            }
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        );

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }

    private function log($str)
    {
        $this->logger->notice('APP/USR | ' . $str);
    }

    /**
     * @Route("/ws/app/usuario", name="ws_app_usuario", methods={"POST"})     
     */
    public function updateAction(
        Request $request,
        UsuarioManager $usuarioManager,
        NotificadorAgenteManager $notificador,
        ContactoManager $contactoManager
    ) {
        $apicode = $request->headers->get('apicode');
        $phone = $request->headers->get('phone');
        $data = json_decode($request->getContent(), true);
        $this->log(sprintf('update: phone-> %s data-> %s', $phone, $request->getContent()));
        $datos = null;
        $status = self::ERROR_FALTAN_DATOS;
        $change = false;
        if (is_null($phone) || is_null($apicode) || is_null($data)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            $usr = $usuarioManager->findByApicode($phone, $apicode);
            if ($usr) {
                if (isset($data['token']) && $usr->getToken() != $data['token']) {
                    $usr->setToken($data['token']);
                    $change = true;
                    $usuarioManager->save($usr);
                    $this->log(sprintf('save token: phone-> %s new token-> %s', $phone, $data['token']));
                }

                //debo obtener todos los contacto asociados al usuario por su telefono
                $contactos = $usuarioManager->findContactos($usr->getTelefono());
                foreach ($contactos as $contacto) {
                    if ($contacto->getToken() != $data['token']) {
                        $contacto->setToken($data['token']);
                        $contactoManager->save($contacto);
                        $notificador->notificar($contacto, 1); // emma me dice que no hace falta actualizar eventos,solamente contacto.
                    }
                }
                $status = self::STATUS_OK;
            } else {
                $status = self::ERROR_APICODE;
            }
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => $datos,
        );

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }
}
