<?php

namespace App\Controller\ws\v2;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GmapsManager;
use App\Model\app\PrestacionManager;
use App\Model\app\UsuarioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\NotificadorAgenteManager;
use App\Entity\Organizacion;
use App\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ServicioController extends AbstractController
{

    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $referencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );
    private $organizacionManager;
    private $servicioManager;
    private $referenciaManager;
    private $gmapsManager;
    private $tanqueManager;
    private $usuarioManager;
    private $prestacionManager;
    private $notificadorAgenteManager;

    function __construct(
        OrganizacionManager $organizacionManager,
        TanqueManager $tanqueManager,
        UsuarioManager $usuarioManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        PrestacionManager $prestacionManager,
        NotificadorAgenteManager $notificadorAgenteManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->tanqueManager = $tanqueManager;
        $this->usuarioManager = $usuarioManager;
        $this->prestacionManager = $prestacionManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    /**
     * @Route("/ws/v2/servicios", name="webservice_v2_servicios")
     * @Method({"GET"})
     */
    public function serviciosAction(Request $request)
    {        
        $data = json_decode($request->getContent(), true);
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $arr_patentes = isset($data['patentes']) ? $data['patentes'] : null;   //es un dato obligatorio
        $apikey = isset($data['apiKey']) ? $data['apiKey'] : null;
        $apicode = isset($data['apiCode']) ? $data['apiCode'] : null;
        $phone = isset($data['phone']) ? $data['phone'] : null;
        $cercania = isset($data['cercania']) ? $data['cercania'] : false;
        $domicilio = isset($data['domicilio']) ? $data['domicilio'] : false;

        $status = true;
        $datos = null;
        if (is_null($arr_patentes) && (is_null($apikey) || is_null($apicode))) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {            
            $servicios =  null;
            if ($apikey) {  //trabajo a nivel organizacion                
                $organizacion = $this->organizacionManager->findByApiKey($apikey);
                if (!is_null($organizacion)) {
                    $datos = array();
                    $servicios = $this->obtenerServiciosByPatentes($arr_patentes, $organizacion);
                    $status = self::STATUS_OK;
                }
            } elseif ($apicode) {   //trabajo a nivel usuario                
                $usuario = $this->usuarioManager->findByApicode($phone, $apicode);
                $servicios = $this->obtenerServiciosByPatentes($arr_patentes, $usuario);
                $status = self::STATUS_OK;
            } else {
                $status = self::ERROR_APIKEY;
            }
            //ya tengo los servicios par devolver
            if (!is_null($servicios)) {                            
                foreach ($servicios as $servicio) {
                    $datos[$servicio->getVehiculo()->getPatente()] = $this->getDataServicio($servicio, $cercania, $domicilio);                    
                }
            }
        }

        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }


    private function obtenerServiciosByPatentes($patentes, $entity)
    {
        $servicios = array();
        //armo un array solo con los servicios a buscar
        //aca controlo si pertenece a la organizacion o al usuario
        if ($entity instanceof Organizacion) {
            foreach ($patentes as $patente) {
                $servicio = $this->servicioManager->findByPatente($patente);
                if ($servicio && $servicio->getOrganizacion() == $entity) {
                    $servicios[] = $servicio;
                }
            }
        } elseif ($entity instanceof Usuario) {
            $servTmp = array();
            foreach ($patentes as $patente) {
                $servicio = $this->servicioManager->findByPatente($patente);
                if ($servicio) {
                    $servTmp[$servicio->getId()] = $servicio;
                }
            }

            //obtengo los servicios del usuario
            //$serviciosAll = $this->servicioManager->findSoloAsignadosUsuario($entity);
            $serviciosAll = $this->servicioManager->findAllByUsuario($entity);            
            foreach ($serviciosAll as $servicio) {
                $arrMaster[$servicio->getId()] = $servicio;
            }

            $servicios = array_intersect_key($arrMaster, $servTmp);           
        }

        //die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');

        return $servicios;
    }
    /**
     * @Route("/ws/v2/servicios/all", name="webservice_v2_servicios_all")
     * @Method({"GET"})
     */
    public function serviciosAllAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $apikey = isset($data['apiKey']) ? $data['apiKey'] : null;

        $status = true;
        $datos = null;
        if (is_null($apikey)) {
            $status = self::ERROR_FALTAN_DATOS;
        } else {
            //ejemplo de serializacion y urlencode
            $organizacion = $this->organizacionManager->findByApiKey($apikey);
            if (!is_null($organizacion)) {
                $datos = array();
                $servicios = $this->servicioManager->findAllByOrganizacion($organizacion);
                foreach ($servicios as $servicio) {
                    $status = self::STATUS_OK;
                    $datos[$servicio->getId()] = array(
                        'id' => $servicio->getId(),
                        'nombre' => $servicio->getNombre(),
                        'tipo' => $servicio->getStrTipoObjetos(),
                        'clase' => $servicio->getTipoServicio()->getNombre()
                    );
                    //die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');
                }
            } else {
                $status = self::ERROR_APIKEY;
            }
        }

        $respuesta = array('code' => $status, 'response' => $this->strResponse[$status]);
        if (!is_null($datos))
            $respuesta['data'] = $datos;
        return $this->generateResponse($status, $respuesta);
    }

    private function generateResponse($status, $struct)
    {
        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 400;
        };
        return new Response(json_encode($struct), $st, $headers);
    }



    /**
     * Genera la informacion del servicio que se le pasa por parametro. Esto se usa siempre para todos los mismos ws
     * solo cambia la forma de consultar los ws pero la estruc es siempre la misma
     */
    private function getDataServicio($servicio, $cercania = null, $domicilio = null, $getId = null)
    {
        $infoCerca = null;
        if (!is_null($cercania) && $cercania) {
            if (is_null($this->referencias)) {
                $this->referencias = $this->referenciaManager->findAsociadas($servicio->getOrganizacion());
            }
            if ($this->referencias && !is_null($servicio->getUltLatitud()) && !is_null($servicio->getUltLongitud())) {
                $cercanas = $this->servicioManager->buscarCercaniaPosicion($servicio->getUltLatitud(), $servicio->getUltLongitud(), $this->referencias, 1);
                if (is_null($cercanas[0])) {
                    $infoCerca = null;
                } else {
                    $infoCerca = array(
                        'referencia' => $cercanas[0]['id'],
                        'nombre' => $cercanas[0]['nombre'],
                        'texto' => $cercanas[0]['leyenda'],
                        'icono' => !is_null($cercanas[0]['icono']) ? $cercanas[0]['icono'] : null,
                    );
                }
            }
        }

        // $domicilio = null;   ///para ver error en direccion
        //'ult_reporte' => $strFecha,
        $datos = array(
            'id' => $servicio->getId(),
            'nombre' => $servicio->getNombre(),
            'ult_reporte' => !is_null($servicio->getUltFechahora()) ? date_format($servicio->getUltFechahora(), 'Y-m-d H:i:s') : null,
            'ult_latitud' => $servicio->getUltLatitud(),
            'ult_longitud' => $servicio->getUltLongitud(),
            'ult_odometro' => $servicio->getUltOdometro(),
            'ult_horometro' => $servicio->formatHorometro(),
            'ult_velocidad' => $servicio->getUltVelocidad(),
            'ult_direccion' => $servicio->getUltDireccion(),
            'domicilio' => is_null($domicilio) || !$domicilio ? null : $this->gmapsManager->getDomicilio($servicio->getUltLatitud(), $servicio->getUltLongitud(), $servicio->getOrganizacion()),
            'cercania' => $infoCerca,
        );
        $datos['tipo_servicio'] = ['id' => $servicio->getTipoServicio()->getId(), 'nombre' => $servicio->getTipoServicio()->getnombre()];
        $datos['clase_servicio'] = ['id' => $servicio->getTipoObjeto(), 'nombre' => $servicio->getStrTipoObjetos()];
        if ($getId != null) {
            $datos['id'] = $servicio->getId();
        }
        if (!is_null($servicio->getVehiculo())) {
            if ($servicio->getVehiculo()->getLitrosTanque()) {
                $datos['litrosTanque'] = intval($servicio->getVehiculo()->getLitrosTanque());
            }
        }
        //die('////<pre>' . nl2br(var_export($datos, true)) . '</pre>////');

        if (!is_null($servicio->getVehiculo())) {
            $datos['patente'] = $servicio->getVehiculo()->getPatente() != null ? $servicio->getVehiculo()->getPatente() : '---';
            $datos['marca'] = $servicio->getVehiculo()->getMarca() != null ? $servicio->getVehiculo()->getMarca() : '---';
            $datos['modelo'] = $servicio->getVehiculo()->getModelo() != null ? $servicio->getVehiculo()->getModelo() : '---';
            $datos['anioFabricacion'] = $servicio->getVehiculo()->getAnioFabricacion() != null ?  $servicio->getVehiculo()->getAnioFabricacion() : '---';
            //agrego los datos del tipo de vehiculo
            if (!is_null($servicio->getVehiculo()->getTipoVehiculo())) {
                $datos['tipo_vehiculo'] = array(
                    'id' => $servicio->getVehiculo()->getTipoVehiculo()->getId(),
                    'nombre' => $servicio->getVehiculo()->getTipoVehiculo()->getTipo()
                );
            }
        }


        $ultTrama = json_decode($servicio->getUltTrama(), true);

        if (isset($ultTrama['contacto'])) {
            $datos['ult_contacto'] = $ultTrama['contacto'];
        }

        $datos['sensores'] = $this->servicioManager->getDataSensores($servicio);

        if ($servicio->getEntradasDigitales() === true) {
            $datos['inputs'] = $this->servicioManager->getDataInputs($servicio);
        }

        //die('////<pre>'.nl2br(var_export($form, true)).'</pre>////');

        return $datos;
    }
}
