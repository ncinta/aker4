<?php

namespace App\Controller\ws\v2;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\UsuarioManager;
use App\Model\app\TipoVehiculoManager;
use App\Model\app\TipoServicioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class TipoVehiculoController extends AbstractController
{

    private $tipoVehiculoManager;
    private $tipoServicioManager;
    private $usuarioManager;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_ORGANIZACION = 5;
    const ERROR_MEDICIONES = 6;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
    );


    function __construct(
        TipoVehiculoManager $tipoVehiculoManager,
        TipoServicioManager $tipoServicioManager,
        UsuarioManager $usuarioManager
    ) {
        $this->tipoVehiculoManager = $tipoVehiculoManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->usuarioManager = $usuarioManager;
    }


    /**
     * @Route("/ws/v2/tipovehiculo/list", name="webservice_v2_tipovehiculo_list"), method({"GET"})     
     */
    public function tipoVehiculoAction()
    {
        $datos = null;
        $status =  self::STATUS_OK;
        $tipoServicio = $this->tipoServicioManager->findAll();
        // die('////<pre>' . nl2br(var_export(count($tipoVehiculos), true)) . '</pre>////');
        foreach ($tipoServicio as $tipo) {
            $datos[] = array(
                'id' => $tipo->getId(),
                'nombre' => $tipo->getNombre(),
                'icono' => $tipo->getPathIcono(),
                'path' => $tipo->getWebPath()
            );
        }

        $respuesta = array(
            'code' => $status,
            'response' => $this->strResponse[$status],
            'data' => null
        );

        if (!is_null($datos)) {
            $respuesta['data'] = $datos;
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        );
        if ($status == self::STATUS_OK) {
            $st = 200;
        } else {
            $st = 403;
        };
        return new Response(json_encode($respuesta), $st, $headers);
    }

    private function generateResponse($struct)
    {
        return new Response(json_encode($struct), 200, array(
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*'
        ));
    }

}
