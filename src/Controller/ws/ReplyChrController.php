<?php

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Panico;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\app\UtilsManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\ReferenciaManager;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\app\NotificadorAgenteManager;
use App\Entity\Itinerario as Itinerario;
use App\Model\app\GeocoderManager;
use Psr\Log\LoggerInterface;

class ReplyChrController extends AbstractController
{

    const STATUS_OK = 0;
    const STATUS_ERROR_DATA = 1;
    const STATUS_ERROR_SINDATA = 2;
    const STATUS_ERROR_SINEVENTOID = 3;
    const STATUS_ERROR_SINSERVICIOID = 4;
    const STATUS_ERROR_SINTITULO = 5;
    const STATUS_ERROR_GRABACION = 6;
    const STATUS_ERROR_FECHA = 7;
    const STATUS_ERROR_SINUID = 8;

    const LOGISTICA_CHROBINSON = 5;

    private $utils;
    private $servicioManager;
    private $grpRefManager;
    private $logger;
    private $referenciaManager;
    private $itinerarioManager;
    private $logisticaManager;
    private $logear = true;
    private $notificadorAgenteManager;
    private $geocoderManager;

    public function __construct(
        UtilsManager $utilsManager,
        ServicioManager $servicioManager,
        GrupoReferenciaManager $grpManager,
        LoggerInterface $logger,
        ReferenciaManager $referenciaManager,
        ItinerarioManager $itinerarioManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        GeocoderManager $geocoderManager,
        LogisticaManager $logisticaManager
    ) {
        $this->utils = $utilsManager;
        $this->geocoderManager = $geocoderManager;
        $this->servicioManager = $servicioManager;
        $this->grpRefManager = $grpManager;
        $this->logger = $logger;
        $this->referenciaManager = $referenciaManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->logisticaManager = $logisticaManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    /**
     * @Route("/ws/reply/chr/position", name="reply_chr_position", methods={"GET"})
     */
    public function replyPositionAction(Request $request)
    {

        //obtener el token para todos los envios
        $token =  $this->notificadorAgenteManager->getTokenChr();
        $this->logCHR('Token -> ' . $token);
        //$token = true;
        if ($token) {  //tengo token entonces debo enviar, sino no hacer nada.

            //obtener los itinerarios de chr que esten nuevos o activos.
            $chr = $this->logisticaManager->find(self::LOGISTICA_CHROBINSON);
            $estados = [Itinerario::ESTADO_NUEVO, Itinerario::ESTADO_ENCURSO];
            $itinerarios = $this->itinerarioManager->findAllByLogistica($chr, $estados);            

            //por cada itinerario armar la data
            foreach ($itinerarios as $itinerario) {
                //puedo tener varios servicios para el itinerario por lo que tengo que mandar para cada uno.
                foreach ($itinerario->getServicios() as $ItServicio) {
                    //armo la data para el itinerario en cuestion
                    $jsonCHR = json_encode($this->toArrayDataChr($itinerario, $ItServicio->getServicio()));                    
                    $this->logCHR('Json -> ' . $jsonCHR);
                   //enviar la data
                   $notificarChr = $this->notificadorAgenteManager->sendDataChr($jsonCHR, $token);
                   $this->logCHR('Result -> ' . $notificarChr);
                }
            }
            $status = self::STATUS_OK;
        } else {
            $this->logCHR('Sin token');
            $status = self::STATUS_ERROR_DATA;
        }

        $respuesta = array(0 => $status);
        return new Response(json_encode($respuesta), 200);
    }

    private function logCHR($str)
    {
        if ($this->logear) {
            $this->logger->notice('replyCHR | ' . $str);
        }
    }

    private function toArrayDataChr($itinerario, $servicio)
    {
        //obtengo y parseo la fecha actual.
        $fecha = new \DateTime();
        $ciudad = '---';
        $pcia = '---';
       
        $direc = $this->geocoderManager->inversoRaw($servicio->getUltLatitud(), $servicio->getUltLongitud());       
       // dd($direc);
        if (isset($direc['exito']) && $direc['exito']) {
            
            if ($direc['provided_by'] == 'vmap3') {
                $ciudad = $direc['direccion'];
            } else {
                if (isset($direc['address'])) {
                    if (!isset($direc['address']['city'])) {
                        //dd($direc['address']);
                        $ciudad = isset($direc['address']['village']) ? $direc['address']['village'] : '---';
                    } else {
                        $ciudad = $direc['address']['city'];
                    }
                } else {
                    $ciudad =  '---';
                }
                
                if (isset($direc['address']['state'])) {
                    $p = $this->itinerarioManager->findProvinciaByNombre($direc['address']['state']);
                    if ($p) {
                        $pcia = $p->getCode();
                    }
                }
            }
        }

        return array(
            "transitType" => "Road",
            'eventCode' => 'X6',   //En Route to Delivery Location
            'shipmentIdentifier' => array(
                "shipmentNumber" => $itinerario->getCodigo(), //codigo de itinerario
            ),
            'dateTime' => array(
                "eventDateTime" => $fecha ? $fecha->format('Y-m-d') . 'T' . $fecha->format('H:i:s.v') . 'Z' : null
            ),
            'location' =>  array(
                //"type" => null,    no enviar esto directamente aunque lo pida la doc
                //"stopSequenceNumber" => null, no enviar esto directamente aunque lo pida la doc
                "address" =>  array(
                    "city" => $ciudad,
                    "stateProvinceCode" => $pcia,
                    "country" => "AR",
                    "latitude" => $servicio->getUltLatitud(),
                    "longitude" => $servicio->getUltLongitud(),
                )
            )
        );
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
