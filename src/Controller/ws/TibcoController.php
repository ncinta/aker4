<?php

/**
 * Implementa una serie de webservice para devolver eventos de TIBCO - Chile.
 */

namespace App\Controller\ws;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\app\BackendWsManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\GeocoderManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class TibcoController extends AbstractController
{

    /**
      APIS KEY
      '52f215dbeb19934eb4ea1f7edce2e3de8cbc77f0' => 'mavesa'
      '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
     */
    private $apikey = array(
        '131da627c68d88023a0be5d6783009be0d285377' => 'ciktur',
        '7cc98b8ebd715250270ad1633436df08b1ab0aa4' => 'pumpcontrol',
    );
    private $timezone;      //mantiene la timezone de la organizacion.
    private $backend;     //este es el backend a donde voy a acceder.
    private $dataReferencias = null;

    const STATUS_OK = 0;
    const ERROR_PATENTE_NO_ENCONTRADA = 1;
    const ERROR_VEHICULO_NO_ACCESIBLE = 2;
    const ERROR_FORMATO_FECHA = 3;
    const ERROR_FALTAN_DATOS = 4;
    const ERROR_APIKEY = 5;
    const ERROR_MEDICIONES = 6;
    const ERROR_FALTAN_PATENTES = 7;
    const ERROR_FALLA_REFERENCIAS = 8;
    const ERROR_ORGANIZACION = 9;
    const ERROR_GENERAL = -1;

    private $strResponse = array(
        self::STATUS_OK => 'OK',
        self::ERROR_PATENTE_NO_ENCONTRADA => 'Patente no encontrada',
        self::ERROR_VEHICULO_NO_ACCESIBLE => 'Vehiculo no accesible',
        self::ERROR_FORMATO_FECHA => 'Formato de fecha erroneo',
        self::ERROR_FALTAN_DATOS => 'Datos obligatorios faltantes',
        self::ERROR_APIKEY => 'ApiKey Error',
        self::ERROR_FALTAN_PATENTES => 'Datos de Patentes faltantes',
        self::ERROR_ORGANIZACION => 'Organizacion erronea',
        self::ERROR_MEDICIONES => 'Error Mediciones',
        self::ERROR_FALLA_REFERENCIAS => 'Error Referencias',
        self::ERROR_GENERAL => 'Error general',
    );

    /**
     * WS que devuelve todas las alarmas de exceso de velocidad sacadas desde el
     * historial de posiciones.
     * Devuelve un json con todas las posiciones del equipo segun las especificaciones
     * que se encuentran mas abajo.
     * Parametros:
     * apiKey: es el api del cliente.
     * desde: fecha en formato Y-m-dTH:i:s, sino se manda, se toma el dia de ayer.
     * hasta: fecha en formato Y-m-dTH:i:s, sino se manda, se toma el dia de ayer.
     * patentes: ABC123,CDB233,FGD433
     * idRef: identificador del grupo de referencias que se deben generar, sino se especifica se toman todas las referencias.
     */

    /**
     * @Route("/ws/tibco/alarmas", name="webservice_tibco_alarmas")
     * @Method({"GET", "POST"})
     */
    public function alarmasAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        GeocoderManager $geocoderManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $gruporeferenciaManager
    ) {

        $status = self::STATUS_OK;
        $request = $request;
        $apikey = $request->get('apiKey');
        $fdesde = $request->get('desde');
        $fhasta = $request->get('hasta');
        $patentes = $request->get('patentes');
        //die('////<pre>' . nl2br(var_export(json_decode($patentes), true)) . '</pre>////');
        $idGrupoReferencias = $request->get('idRef');
        $informe = null;
        if (is_null($patentes)) {
            return $this->failResponse(self::ERROR_FALTAN_PATENTES);
        } else {

            $organizacion = $organizacionManager->findByApiKey($apikey);  //obtengo la organizacion.
            if (!is_null($organizacion)) {
                if ($organizacion->getTipoOrganizacion() == 2) {  //solo para clientes)
                    //seteo el backend.
                    $this->setBackend($organizacion);
                    $this->timezone = $organizacion->getTimeZone();

                    //valido la fecha y pongo la correcta para la consulta
                    $fdesde = $this->setFechaValida($fdesde, true);
                    $fhasta = $this->setFechaValida($fhasta, false);

                    //obtengo las referencias del grupo pasado, si no viene se toman todas las referencias del clientes
                    $referencias = $this->getReferenciasGrupo($organizacion, $idGrupoReferencias, $referenciaManager, $gruporeferenciaManager);
                    if (is_null($referencias)) {
                        return $this->failResponse(self::ERROR_FALLA_REFERENCIAS);
                    }
                    foreach ($referencias as $value) {
                        $this->dataReferencias[$value->getId()] = array(
                            'nombre' => $value->getNombre(),
                            'velocidad' => is_null($value->getVelocidadMaxima()) ? 100 : $value->getVelocidadMaxima(), //hardcode
                        );
                    }
                    //die('////<pre>' . nl2br(var_export($this->dataReferencia, true)) . '</pre>////');

                    $arrayPatentes = explode(',', $patentes);   //obtengo las patentes en un array
                    //obtengo la información
                    $informe = array();
                    foreach ($arrayPatentes as $patente) {
                        $servicio = $servicioManager->findByPatente($patente);
                        if ($servicio) {
                            $informe[$servicio->getVehiculo()->getPatente()] = array(
                                'patente' => $servicio->getVehiculo()->getPatente(),
                                'mdmid' => $servicio->getEquipo()->getMdmid(),
                                'proyecto' => null,
                                'contratista' => null,
                                'transportista' => 'ciktur',
                                'info' => $this->getDataAlertas(
                                    $servicio,
                                    $fdesde->format('Y-m-d H:n:s'),
                                    $fhasta->format('Y-m-d H:n:s'),
                                    $referencias,
                                    $servicioManager,
                                    $geocoderManager
                                ),
                            );
                        }
                    }
                    $status = self::STATUS_OK;
                } else {
                    return $this->failResponse(self::ERROR_ORGANIZACION);
                }
            } else {
                return $this->failResponse(self::ERROR_APIKEY);
            }
        }
        //die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
        $respuesta = array('status_code' => $status, 'message' => $this->strResponse[$status]);
        if ($status == self::STATUS_OK && !is_null($informe)) {
            return new Response(json_encode($informe), 200);
        } else {
            return $this->failResponse(self::ERROR_GENERAL);
        }
    }

    /**
     * Devuelve todas las referencias de un grupo, si idGrupo = null devuelve todas
     * las referencias de la organizacion.
     * @param type $organizacion
     * @param type $idGrupo
     * @return Referencias Devuelve todas las referencias asociadas a un grupo
     */
    private function getReferenciasGrupo($organizacion, $idGrupo, $referenciaManager, $gruporeferenciaManager)
    {
        if (is_null($idGrupo)) {
            return $referenciaManager->findAsociadas($organizacion);
        } else {
            $grp = $gruporeferenciaManager->find($idGrupo);
            //el grupo es de otra organizacion
            if (!$grp)
            /** && $grp->getOrganizacion() != $organizacion) */
            {
                return null;
            }
            return $referenciaManager->findAllByGrupo($idGrupo);
        }
    }

    private function getDataAlertas($servicio, $desde, $hasta, $referencias, $servicioManager, $geocoderManager)
    {
        //obtengo la velocidad maxima del servicio segun el tipo de vechiculo.
        $vMax = is_null($servicio->getTipoServicio()->getVelocidadMaxima()) ? 90 : $servicio->getTipoServicio()->getVelocidadMaxima();


        $reduce = array('_id', 'servicio_id', 'porcentaje_ack', 'fecha_recepcion', 'fecha_guardado', 'ip_capturador', 'ip_origen', 'puerto_origen', 'gsm_csq', 'gsm_error', 'gsm_dim', 'humedad', 'temperatura', 'aceleracion', 'gps_dim', 'gps_error');
        $data = $this->backend->historial($servicio->getId(), $desde, $hasta, $referencias);
        $informe = null;
        reset($data);
        // die(nl2br(var_export($data, true)));
        while ($trama = current($data)) {
            next($data);
            if (!isset($trama['posicion'])) { //descarto las tramas raras
                continue;
            }

            //$trama = $this->array_reduce($trama, $reduce);  esta bueno
            //paso a hora local la trama.
            $fechaLoc = new \DateTime($trama['fecha'], new \DateTimeZone($this->timezone));

            $alarma = null;
            $vPermitida = $vMax;
            $referencia = $cercania = $direc = null;

            //detección de bateria externa desconectada
            if ($trama['bateria_externa'] == 0) {
                //alarma por bateria externa desconectada
                $direc = $geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);

                $alarma[] = sprintf('Sabotaje batería externa en %s', $direc);
            }

            if (isset($trama['referencia_id']) && $trama['referencia_id'] != null) { //es una referencia
                $tipoZona = 'GEO';
                if (isset($this->dataReferencias[$trama['referencia_id']])) {
                    $referencia = $this->dataReferencias[intval($trama['referencia_id'])];
                    if ($trama['posicion']['velocidad'] >= min($vMax, $referencia['velocidad'])) {
                        $vPermitida = min($vMax, $referencia['velocidad']);  //velocidad maxima permitida por la que entra la alarma
                        $alarma[] = sprintf('Exceso velocidad de %d kms/h en %s', $vPermitida, $referencia['nombre']);
                        $cercania = sprintf('Dentro de ' . $referencia['nombre']);
                    }
                }
            } else {
                $tipoZona = 'RUTA';
                if ($trama['posicion']['velocidad'] >= $vMax) {
                    $vPermitida = $vMax;  //velocidad maxima permitida por la que entra la alarma
                    //establesco la cercania
                    $puntosTmp = $servicioManager->buscarCercaniaPosicion($trama['posicion']['latitud'], $trama['posicion']['longitud'], $referencias, 1);
                    if (isset($puntosTmp[0])) {
                        $cercania = $puntosTmp[0]['leyenda'];
                    }

                    if ($direc == null) {
                        $direc = $geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                    }

                    $alarma[] = sprintf('Exceso velocidad de %d kms/h en %s', $vPermitida, $direc);
                }
            }

            //solo agrego si hay alarmas
            if ($alarma != null) {
                $informe[] = array(
                    'fecha' => $fechaLoc->format('Y-m-d H:i:s'),
                    'latitud' => $trama['posicion']['latitud'],
                    'longitud' => $trama['posicion']['longitud'],
                    'velocidad' => $trama['posicion']['velocidad'],
                    'direccion' => $trama['posicion']['direccion'],
                    'chofer' => null,
                    'alarma' => implode(' | ', $alarma),
                    'referencia' => is_null($referencia) ? '' : $referencia['nombre'],
                    'cercania' => $cercania,
                    'valorZona' => $vPermitida,
                    'tipoZona' => $tipoZona,
                );
            }
        }
        //die(nl2br(var_export(json_encode($informe), true)));

        return $informe;
    }

    private function array_reduce($data, $reduceKey)
    {
        foreach ($reduceKey as $key) {
            if (key_exists($key, $data))
                unset($data[$key]);
        }
        return $data;
    }

    private function failResponse($status)
    {
        $respuesta = array('status_code' => $status, 'message' => $this->strResponse[$status]);
        //return $this->generateResponse($status, $respuesta);
        return new Response(json_encode($respuesta), 400);
    }

    private function generateResponse($status, $struct)
    {
        if ($status == self::STATUS_OK) {
            return new Response(json_encode($struct), 200);
        } else {
            return new Response(json_encode($struct), 400);
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function setBackend($organizacion)
    {
        $usuario = $organizacion->getUsuarioMaster();
        $this->backend = new BackendWsManager($this->getEntityManager(), $organizacion, $this->container->getParameter('calypso.backend_addr'), $this->container->getParameter('calypso.backend_addr_backup'), $usuario);
        return $this->backend;
    }

    private function checkApi($api)
    {
        return isset($this->apikey[$api]);
    }

    public function setFechaValida($fecha, $inicial = true)
    {
        if (is_null($fecha)) {  //desde ayer a las 00:00
            if ($inicial) {
                $date = new \DateTime(date('Y-m-d 00:00:00'), new \DateTimeZone($this->timezone));
                $date->sub(new \DateInterval('P1D'));
            } else {
                $date = new \DateTime(date('Y-m-d 23:59:59'), new \DateTimeZone($this->timezone));
                $date->sub(new \DateInterval('P1D'));
            }
            return $date;
        } else {
            return new \DateTime($fecha);
        }
    }
}
