<?php

namespace App\Controller\crm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Categoria;
use App\Form\apmon\CategoriaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BreadcrumbManager;

class CategoriaController extends AbstractController
{

    private $breadcrumbManager;

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    function __construct(BreadcrumbManager $breadcrumbManager)
    {
        $this->breadcrumbManager = $breadcrumbManager;
    }

    /**
     * @Route("/categoria/list", name="crm_categoria_list")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Categorias");
        $categorias = $this->getEntityManager()->getRepository('App:Categoria')->findBy(array(), array('nombre' => 'ASC'));
        return $this->render('crm/Categoria/list.html.twig', array(
            'categorias' => $categorias,
            'menu' => $this->getShowMenu()
        ));
    }

    private function getShowMenu()
    {
        $menu = array(
            1 => array(
                array(
                    'label' => 'Categoria',
                    'imagen' => 'fa fa-plus',
                    'url' => $this->generateUrl('crm_categoria_new')
                )
            )
        );
        return $menu;
    }

    /**
     * @Route("/categoria/new", name="crm_categoria_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $categoria = new Categoria();
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEntityManager()->persist($categoria);
            $this->getEntityManager()->flush();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Categoria");
        return $this->render('crm/Categoria/new.html.twig', array(
            'estado' => $categoria,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/categoria/{id}/edit", name="crm_categoria_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Categoria $categoria)
    {
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEntityManager()->persist($categoria);
            $this->getEntityManager()->flush();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Categoria");
        return $this->render('crm/Categoria/edit.html.twig', array(
            'categoria' => $categoria,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/categoria/{id}/delete", name="crm_categoria_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"}) 
     */
    public function deleteAction(Request $request, Categoria $categoria)
    {
        $this->getEntityManager()->remove($categoria);
        $this->getEntityManager()->flush();
        return $this->redirect($this->breadcrumbManager->getVolver());
    }
}
