<?php

namespace App\Controller\crm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Peticion;
use App\Entity\Servicio;
use App\Entity\Organizacion;
use App\Entity\FiltroPeticion;
use App\Entity\GrupoServicio;
use App\Form\crm\PeticionType;
use App\Form\crm\PeticionEditHeadType;
use App\Form\crm\PeticionFilter;
use App\Form\crm\PanelType;
use App\Form\crm\PeticionEditType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UsuarioManager;
use App\Model\app\UtilsManager;
use App\Model\app\ServicioManager;
use App\Model\crm\PeticionManager;
use App\Model\crm\CategoriaManager;
use App\Model\app\CentroCostoManager;
use App\Model\app\GrupoServicioManager;
use App\Model\crm\EstadoManager;
use App\Model\crm\NotificadorManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\MantenimientoManager;
use Knp\Snappy\Pdf;

class PeticionController extends AbstractController
{

    private $cantidad = 5; //cantidad de divisores que quiero para mostrar en el grafico de panel
    private $breadcrumbManager;
    private $userloginManager;
    private $servicioManager;
    private $peticionManager;
    private $usuarioManager;
    private $estadoManager;
    private $notificadorManager;
    private $categoriaManager;
    private $centroCostoManager;
    private $utilsManager;
    private $mantenimientoManager;
    private $ordentrabajoManager;
    private $grupoServicioManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        EstadoManager $estadoManager,
        NotificadorManager $notificadorManager,
        ServicioManager $servicioManager,
        PeticionManager $peticionManager,
        UsuarioManager $usuarioManager,
        CategoriaManager $categoriaManager,
        CentroCostoManager $centroCostoManager,
        UtilsManager $utilsManager,
        MantenimientoManager $mantenimientoManager,
        OrdenTrabajoManager $ordentrabajoManager,
        GrupoServicioManager  $grupoServicioManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->peticionManager = $peticionManager;
        $this->usuarioManager = $usuarioManager;
        $this->estadoManager = $estadoManager;
        $this->notificadorManager = $notificadorManager;
        $this->categoriaManager = $categoriaManager;
        $this->centroCostoManager = $centroCostoManager;
        $this->utilsManager = $utilsManager;
        $this->mantenimientoManager = $mantenimientoManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->grupoServicioManager = $grupoServicioManager;
    }

    /**
     * @Route("/crm/peticion/{idServicio}/new", name="crm_peticion_new",
     *     requirements={
     *         "idServicio": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PETICION_AGREGAR")
     */
    public function newAction(Request $request, $idServicio)
    {
        $id = intval($idServicio);
        $user = $this->userloginManager->getUser();
        $servicio = null;
        $options['usuarios'] = $this->usuarioManager->findAll($user->getOrganizacion());
        // viene por peticion_main... sin servicio
        if ($id == 0) {
            $options['servicios'] = $this->servicioManager->findAllByOrganizacion($user->getOrganizacion());
        } else { //viene por servicio_list.. con servicio
            $servicio = $this->servicioManager->find($id);
            $options['servicios'] = array($servicio);
        }
        //die('////<pre>' . nl2br(var_export($servicio->getNombre(), true)) . '</pre>////');
        $peticion = $this->peticionManager->create($servicio, $user);
        $form = $this->createForm(PeticionType::class, $peticion, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->peticionManager->save($peticion)) {
                $this->setFlash('success', 'El pedido de reparación se creo con éxito');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Pedido");
        return $this->render('crm/Peticion/new.html.twig', array(
            'servicio' => $servicio,
            'organizacion' => $user->getOrganizacion(),
            'idServicio' => $idServicio,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/crm/peticion/head/{id}/edit", name="crm_peticion_head_edit",
     *     requirements={
     *         "idServicio": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PETICION_AGREGAR")
     */
    public function editheadAction(Request $request, Peticion $peticion)
    {
        $user = $this->userloginManager->getUser();
        $options['usuarios'] = $this->usuarioManager->findAll($user->getOrganizacion());
        $options['change_servicio'] = !is_null($peticion->getOrdenTrabajo()) ? false : true;
        $options['servicios'] = $this->servicioManager->findAllByOrganizacion($user->getOrganizacion());
        $form = $this->createForm(PeticionEditHeadType::class, $peticion, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->peticionManager->save($peticion)) {
                $this->setFlash('success', 'El pedido de reparación se modificó con éxito');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Pedido");
        return $this->render('crm/Peticion/edit_head.html.twig', array(
            'organizacion' => $user->getOrganizacion(),
            'peticion' => $peticion,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/crm/peticion/{idServicio}/showpeticiones", name="crm_peticion_showpeticiones",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @ParamConverter("id", class="App:Servicio", options={"id" = "idServicio"})
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function showpeticionesAction(Request $request, Servicio $servicio)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $servicio->getNombre());
        $peticiones = $this->servicioManager->findPeticiones($servicio);
        $menu = $this->getShowMenuPeticiones($servicio);
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nueva Petición");
        return $this->render('crm/Servicio/show_peticiones.html.twig', array(
            'peticiones' => $peticiones,
            'servicio' => $servicio,
            'menu' => $menu,
        ));
    }

    private function getShowMenuPeticiones($servicio)
    {
        $menu[1] = array();
        array_push($menu[1], $this->get('crm.router.peticion')->btnNew($servicio));
        return $menu;
    }

    /**
     * @Route("/crm/peticion/{id}/newsub", name="crm_peticion_newsub")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_AGREGAR")
     */
    public function newsubAction(Request $request, $id)
    {
        $estadoNuevo = $this->estadoManager->findById(1);
        $estadoCerrado = $this->estadoManager->findById(4);
        $peticion = $this->peticionManager->find($id);
        $options['usuarios'] = $this->usuarioManager->findAll($peticion->getServicio()->getOrganizacion());
        $newPet = $this->peticionManager->create($peticion->getServicio(), $this->userloginManager->getUSer());
        //$newPet = clone $peticion;
        $form = $this->createForm(PeticionType::class, $newPet, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->peticionManager->save($newPet)) {
                $newPet = $this->peticionManager->nuevaPeticion($newPet, null, $this->userloginManager->getUSer(), $estadoNuevo, true, $peticion->getServicio());
                $pet = $this->peticionManager->cerrarPeticion($peticion, $estadoCerrado);
                $servicios[] = $newPet->getServicio();
                $organizacion = $peticion->getServicio()->getOrganizacion();
                $mensaje = $this->armarEmail($newPet, $servicios, $organizacion, null);
                $arr_destino[] = array('persona' => $newPet->getAsignadoA()); //asi se formatea para enviar mail
                $asunto = 'SOPORTE Aker: ' . $newPet->getAsunto() . ' [' . $organizacion->getNombre() . ']';
                $notif = $this->notificadorManager->despacharMail($arr_destino, $asunto, $mensaje);
                $this->setFlash('success', 'El pedido de trabajo se creo con éxito');
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('crm_peticion_main', array('servicio' => $newPet->getServicio()->getId())));
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo pedido de Trabajo");
        return $this->render('crm/Peticion/new_subpeticion.html.twig', array(
            'servicio' => $newPet->getServicio(),
            'peticion' => $peticion,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/crm/peticion/{id}/edit", name="crm_peticion_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_AGREGAR")
     */
    public function editAction(Request $request, Peticion $peticion)
    {

        $data = $request->get('form_peticion');

        $servicio = $peticion->getServicio();
        $comentarios = $this->formatearComentarios($peticion);

        //formulario para que completen en el momento
        $options['usuarios'] = $this->usuarioManager->findAll($servicio->getOrganizacion());
        $options['cambiarAsignado'] = $this->userloginManager->isGranted('ROLE_PETICION_REASINAR');
        $form = $this->createForm(PeticionEditType::class, $peticion, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($peticion->getEstado()->getIsCerrado()) {
                $this->peticionManager->setCerrada($peticion, $peticion->getFechaFin());
            }

            $this->peticionManager->addNota($peticion, $data['nota'], $this->userloginManager->getUser());
            $this->peticionManager->save($peticion);

            $servicios[] = $peticion->getServicio();

            // INICIA NOTIFICACION POR MAIL
            $organizacion = $peticion->getServicio()->getOrganizacion();
            $mensaje = $this->armarEmail($peticion, $servicios, $organizacion, $comentarios);
            //return new Response($mensaje);

            $asunto = sprintf('SOPORTE Aker: #%d %s [%s]', $peticion->getId(), $peticion->getAsunto(), $organizacion->getNombre());

            if ($this->userloginManager->getUser()->getId() == $peticion->getAutor()->getId()) {
                if ($peticion->getAutor()->getId() != $peticion->getAsignadoA()->getId()) {
                    $arr_destino[] = array('persona' => $peticion->getAsignadoA()); //asi se formatea para enviar mail
                    $notif = $this->notificadorManager->despacharMail($arr_destino, $asunto, $mensaje);
                }
            } else {
                $arr_destino[] = array('persona' => $peticion->getAsignadoA()); //asi se formatea para enviar mail
                $arr_destino[] = array('persona' => $peticion->getAutor()); //asi se formatea para enviar mail
                $notif = $this->notificadorManager->despacharMail($arr_destino, $asunto, $mensaje);
            }
            $this->setFlash('success', 'La petición se modificó con éxito.');
            $this->breadcrumbManager->pop();

            //aca veo de hacer nueva subpeticion si es necesario.
            if (isset($data['nuevo_hijo']) && $data['nuevo_hijo'] == '1') { //CREAR NUEVA OR
                return $this->newsubAction($request, $peticion);
            } elseif (isset($data['nuevo_hijo']) && $data['nuevo_hijo'] == '2') { //CREAR NUEVA OT
                //  die('////<pre>' . nl2br(var_export('$data', true)) . '</pre>////');
            } elseif (isset($data['nuevo_hijo']) && $data['nuevo_hijo'] == '3') { //DESCARTAR
            } else {
            }
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar #" . $peticion->getId());


        return $this->render('crm/Peticion/edit.html.twig', array(
            'peticion' => $peticion,
            'comentarios' => $comentarios,
            'peticiones' => $this->servicioManager->findPeticiones($servicio),
           // 'notificaciones' => $this->servicioManager->findNotificaciones($servicio), no se pq esta ?
            'menu' => null, //$this->getShowMenu($peticion),
            'formpeticion' => $form->createView(),
            'delete_form' => $this->createDeleteForm($peticion->getId())->createView(),
        ));
    }

    private function formatearComentarios($peticion)
    {
        //genero los comentarios parseados.
        $comentarios = null;
        if (!is_null($peticion->getComentarios())) {
            foreach ($peticion->getComentarios() as $comentario) {
                $comentarios[] = array(
                    'comentario' => $comentario,
                    'cambios' => json_decode($comentario->getCambio(), true),
                );
            }
        }

        return $comentarios;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/crm/peticion/{id}/newmany", name="crm_peticion_newmany",
     *    options={"expose": true},
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_AGREGAR_MULTIPLE")
     */
    public function newmanyAction(Request $request, Organizacion $organizacion)
    {
        $servicios = null;
        if ($request->get('checks')) {
            $ids = explode(',', $request->get('checks'));
            foreach ($ids as $id) {
                $servicios[] = $this->servicioManager->find($id);
            }
        }
        $peticion = $this->peticionManager->createMany($organizacion, $this->userloginManager->getUser());
        $options['usuarios'] = $this->usuarioManager->findAll($this->userloginManager->getOrganizacion());
        $form = $this->createForm(PeticionType::class, $peticion, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($servicios as $servicio) {
                $newPet = clone $peticion;
                $newPet->setServicio($servicio);
                $this->peticionManager->save($newPet);
            }
            $arr_destino[] = array('persona' => $newPet->getAsignadoA()); //asi se formatea para enviar mail
            $mensaje = $this->armarEmail($peticion, $servicios, $organizacion, null);
            $asunto = 'SOPORTE Aker: ' . $newPet->getAsunto() . ' [' . $organizacion->getNombre() . ']';
            $notif = $this->notificadorManager->despacharMail($arr_destino, $asunto, $mensaje);
            unset($newPet);

            $this->setFlash('success', 'Las peticiones se han creado con éxito');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nueva Petición");
        return $this->render('crm/Peticion/newmany.html.twig', array(
            'organizacion' => $organizacion,
            'servicios' => $servicios,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/crm/peticion/main", name="crm_peticion_main")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function mainAction(Request $request)
    {

        $servicioId = $request->get('servicio');
        if ($servicioId) {
            $this->breadcrumbManager->pop();
        }
        $states = null;
        $peticiones = null;
        $byServicio = array();
        $byCateg = array();
        $byEstado = array();
        $byPrioridad = array();
        $byTipo = array();
        $totalServicio = $totalCat = $totalEstado = $totalPrioridad = $totalTipo = 0;

        $organizacion = $this->userloginManager->getOrganizacion();

        $filtro = new FiltroPeticion();
        $filtro->setOrganizacion($organizacion);
        if ($request->getMethod() == 'GET') {
            $now = new \DateTime();
            $desde = (new \DateTime())->modify('-1 month');
            $f = $this->get("session")->get("filtro");  //existe filtro aplicado
            $filtro->setFechatodos(false);
            $filtro->setHasta($now->setTime(23, 59, 59));
            $filtro->setDesde($desde);
            if ($f) {
                $filtro->setAsignadoA($f->getAsignadoA());
                $filtro->setEstado($f->getEstado());
                $filtro->setPrioridad($f->getPrioridad());
                $filtro->setCategoria($f->getCategoria());
                $filtro->setServicio($f->getServicio());
                $filtro->setTipo($f->getTipo());
                $filtro->setCentrocosto($f->getCentroCosto());
            } else {
                $filtro->setAsignadoA($this->userloginManager->getUser()->getId());
                $filtro->setEstado(1);
                //$filtro->setEstado($this->get('crm.estado')->findDefault()->getId());
            }
            // die('////<pre>' . nl2br(var_export($filtro->getHasta(), true)) . 'RR</pre>////');    
            $this->get("session")->set("filtro", $filtro);  //grabo el filtro nuevo.
        }
     
        $options['grupos'] = $this->grupoServicioManager->findAsociadas($this->userloginManager->getUser());
        $options['servicios'] = $this->servicioManager->findAll($organizacion);
        $options['usuarios'] = $this->usuarioManager->findAll($organizacion);
        $options['userlogin'] = $this->userloginManager->getUser();
        $options['categoria'] = $this->categoriaManager->findAll();
        $options['estado'] = $this->estadoManager->findAll();
        $options['centrocosto'] = $this->centroCostoManager->findAllByOrganizacion($organizacion);

        $form = $this->createForm(PeticionFilter::class, $filtro, $options);
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {  //grabo el filtro en la session
            if (!(isset($request->get('filtro')['fechatodos']))) {
                //  die('////<pre>' . nl2br(var_export($request->get('filtro')['fechatodos'], true)) . '</pre>////');
                $filtro->setDesde(new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('filtro')['desde'], true)));
                $filtro->setHasta(new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('filtro')['hasta'], true)));
                $this->get("session")->set("filtro", $filtro);  //grabo el filtro nuevo.
            }
        }        
        $petsAll = $this->peticionManager->obtener($filtro);
        foreach ($petsAll as $pet) {
            //contador por organizacion
            $servicio = $pet->getServicio();
            if (!isset($byServicio[$servicio->getId()])) {
                $byServicio[$servicio->getId()]['total'] = 0;
            }
            $totalServicio = $byServicio[$servicio->getId()]['total'] + 1;
            $byServicio[$servicio->getId()] = array(
                'id' => $servicio->getId(),
                'nombre' => $servicio->getNombre(),
                'total' => $totalServicio
            );
            //contador por categoria
            if (!isset($byCateg[$pet->getCategoria()->getId()])) {
                $byCateg[$pet->getCategoria()->getId()]['total'] = 0;
            }
            $totalCat = $byCateg[$pet->getCategoria()->getId()]['total'] + 1;
            $byCateg[$pet->getCategoria()->getId()] = array(
                'nombre' => $pet->getCategoria()->getNombre(),
                'total' => $totalCat
            );

            //contador por estado
            if (!isset($byEstado[$pet->getEstado()->getId()])) {
                $byEstado[$pet->getEstado()->getId()]['total'] = 0;
            }
            $totalEstado = $byEstado[$pet->getEstado()->getId()]['total'] + 1;
            $byEstado[$pet->getEstado()->getId()] = array(
                'nombre' => $pet->getEstado()->getNombre(),
                'total' => $totalEstado
            );

            //contador por prioridad
            if (!isset($byPrioridad[$pet->getPrioridad()])) {
                $byPrioridad[$pet->getPrioridad()]['total'] = 0;
            }
            $totalPrioridad = $byPrioridad[$pet->getPrioridad()]['total'] + 1;
            $byPrioridad[$pet->getPrioridad()] = array(
                'nombre' => $pet->strPrioridad(),
                'color' => $pet->colorPrioridad(),
                'total' => $totalPrioridad
            );
            ksort($byPrioridad);

            //contador por tipo
            if (!is_null($pet->getTipo())) {
                if (!isset($byTipo[$pet->getTipo()])) {
                    $byTipo[0] = array('nombre' => 'Normal', 'total' => 0); //normal
                    $byTipo[1] = array('nombre' => 'Retrabajo', 'total' => 0); //retrabajo
                }

                $totalTipo = $byTipo[$pet->getTipo()]['total'] + 1;
                $byTipo[$pet->getTipo()] = array(
                    'nombre' => $pet->getTipo() == 1 ? 'Retrabajo' : 'Normal',
                    'total' => $totalTipo
                );
            }
            if ($servicioId) {
                if ($servicio->getId() == $servicioId) {
                    $peticiones[$pet->getEstado()->getId()][] = $pet;
                }
            } else {
                $peticiones[$pet->getEstado()->getId()][] = $pet;
            }
        }

        foreach ($this->estadoManager->findAll() as $estado) {
            $states[$estado->getId()] = array(
                'estado' => $estado,
            );
        }

        //   die('////<pre>' . nl2br(var_export(count($petsAll), true)) . '</pre>////');
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Mis Peticiones");
        return $this->render('crm/Peticion/main.html.twig', array(
            'peticiones' => $peticiones,
            'servicios' => $byServicio,
            'categorias' => $byCateg,
            'estados' => $byEstado,
            'tipos' => $byTipo,
            'prioridad' => $byPrioridad,
            'organizacion' => $organizacion,
            'states' => $states,
            'filter' => $form->createView()
        ));
    }

    /**
     * @Route("/crm/peticion/{id}/export", name="crm_peticion_export",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @IsGranted("ROLE_PETICION_EDITAR")
     */
    public function exportAction(Request $request, Peticion $peticion, Pdf $snappy)
    {

        $filename = sprintf('peticion%d.pdf', $peticion->getId());
        $html = $this->renderView('crm/Peticion/export.html.twig', array(
            'peticion' => $peticion,
            'comentarios' => $this->formatearComentarios($peticion)
        ));
        //return new Response($html);
        return new Response(
            $snappy->getOutputFromHtml($html, array(
                //'orientation' => 'Landscape',
                'default-header' => false
            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $filename,
                'charset' => 'UTF-8',
            )
        );
    }

    /**
     * @Route("/crm/peticion/exportmany", name="crm_peticion_export_many",
     *     options={"expose": true},
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_EDITAR")
     */
    public function exportmanyAction(Request $request, Pdf $snappy)
    {

        $now = new \DateTime();
        $filename = sprintf('peticiones%s.pdf', $now->format('YmdHis'));
        $checks = explode(',', $request->get('checks'));   //obtengo los id's de las peticiones
        $petTmp = $this->peticionManager->findAllByIds($checks);

        $organizaciones = null;
        foreach ($petTmp as $peticion) {
            $organizaciones[$peticion->getServicio()->getOrganizacion()->getNombre()][] = array(
                'peticion' => $peticion,
                'comentarios' => $this->formatearComentarios($peticion)
            );
        }

        $states = null;
        foreach ($this->estadoManager->findAll() as $estado) {
            $states[$estado->getId()] = array(
                'estado' => $estado,
            );
        }

        $html = $this->renderView('crm/Peticion/exportmany.html.twig', array(
            'organizaciones' => $organizaciones,
        ));
        //        return new Response($html);
        return new Response(
            $snappy->getOutputFromHtml($html, array(
                //'orientation' => 'Landscape',
                'default-header' => false
            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $filename,
                'charset' => 'UTF-8',
            )
        );
    }

    /**
     * Cuenta las peticiones abiertas viejas
     * @Route("/crm/peticion/contar", name="crm_peticion_contar")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function contarAction(Request $request)
    {
        $filtro = new FiltroPeticion();
        $now = new \DateTime();
        $filtro->setAsignadoA($this->userloginManager->getUser()->getId());
        $filtro->setEstado(-1);  //abierta
        $filtro->setDesde($now->setTime(0, 0, 0));
        $filtro->setHasta($now->setTime(23, 59, 59));

        $pets = $this->peticionManager->obtener($filtro);

        if (count($pets) > 99) {
            $valor = '99+';
        } else {
            $valor = count($pets);
        }
        $html = $this->renderView('app/Template/btn_peticiones.html.twig', array(
            'nropet' => $valor,
        ));
        return new response($html, 200);
    }

    protected function armarEmail($peticion, $servicios, $comentarios)
    {
        $mensaje = $this->render('crm/Peticion/mail.html.twig', array(
            'comentarios' => $comentarios,
            'peticion' => $peticion,
            'servicios' => $servicios,
        ));
        return $mensaje;
    }


    private function getCantidad($ots)
    {
        $cantidad['correctivo'] = array();
        $cantidad['preventivo'] = array();
        $cantidad['retrabajo'] = array();

        foreach ($ots as $orden) {
            if ($orden->getTipo() == 0) { //correctivo
                $cantidad['correctivo'][] = $orden;
            } elseif ($orden->getTipo() == 1) { //preventivo
                $cantidad['preventivo'][] = $orden;
            } elseif ($orden->getTipo() == 2) {
                $cantidad['retrabajo'][] = $orden;
            }
        }
        return $cantidad;
    }

    private function getAtrasadas($peticiones, $desde, $hasta, $now)
    {
        $diferencia = $hasta->diff($desde);
        $totalDias = abs($diferencia->format('%R%a') * 1); //obtengo la diferencia entre las 2 fechas en dias
        $divisor = $totalDias / $this->cantidad;
        $indicadores = $this->getIndicadores($divisor, $hasta, $now);
        $arrAtrasadas = $this->armarArray($indicadores, $peticiones);
        return $arrAtrasadas;
    }

    private function getIndicadores($divisor, $hasta, $now)
    {
        $indicadores = array();
        for ($i = $this->cantidad - 1; $i >= 0; $i--) {
            $indicadores[$i] = $hasta->sub(new \DateInterval('P' . round($divisor) . 'D'))->format('d-m-Y H:i');
        }
        if ($now) {
            array_push($indicadores, (new \DateTime())->format('d-m-Y H:i'));
        } else {
            array_push($indicadores, $hasta->format('d-m-Y H:i'));
        }
        ksort($indicadores);
        return $indicadores;
    }

    private function armarArray($indic, $peticiones)
    {
        $arrOrden = array();
        $initBarras = $this->initBarras($indic);
        $arr = $this->cargarBarras($indic, $peticiones, $initBarras);
        foreach ($arr as $key => $value) {
            $fecha = $key;
            foreach ($value as $key => $value) {
                $arrOrden[$key][$fecha] = $value;
            }
        }

        return $arrOrden;
    }

    private function getAsignaciones($peticiones)
    {
        $arr = array();
        foreach ($peticiones as $peticion) {
            $arr[] = $peticion->getAsignadoA()->getNombre();
        }
        $indicadores = $this->initBarras($arr);
        foreach ($peticiones as $peticion) {
            if ($peticion->getEstado()->getIsDefault()) { //peticion nueva
                $indicadores[$peticion->getAsignadoA()->getNombre()]['nueva'] = $indicadores[$peticion->getAsignadoA()->getNombre()]['nueva'] + 1;
            } elseif ($peticion->getEstado()->getIsCurso()) { //peticion en curso
                $indicadores[$peticion->getAsignadoA()->getNombre()]['encurso'] = $indicadores[$peticion->getAsignadoA()->getNombre()]['encurso'] + 1;
            } elseif ($peticion->getEstado()->getIsCerrado()) {
                $indicadores[$peticion->getAsignadoA()->getNombre()]['cerrada'] = $indicadores[$peticion->getAsignadoA()->getNombre()]['cerrada'] + 1;
            }
            $indicadores[$peticion->getAsignadoA()->getNombre()]['retrabajo'] = $peticion->getTipo() == 1 ? $indicadores[$peticion->getAsignadoA()->getNombre()]['retrabajo'] + 1 : $indicadores[$peticion->getAsignadoA()->getNombre()]['retrabajo'];
            $indicadores[$peticion->getAsignadoA()->getNombre()]['normal'] = $peticion->getTipo() == 0 ? $indicadores[$peticion->getAsignadoA()->getNombre()]['normal'] + 1 : $indicadores[$peticion->getAsignadoA()->getNombre()]['normal'];
        }
        return $indicadores;
    }

    private function initBarras($indicadores)
    {
        $arr = array();
        foreach ($indicadores as $value) {
            $arr[$value]['nueva'] = 0;
            $arr[$value]['encurso'] = 0;
            $arr[$value]['cerrada'] = 0;
            $arr[$value]['retrabajo'] = 0;
            $arr[$value]['normal'] = 0;
        }
        return $arr;
    }

    private function cargarBarras($indicadores, $peticiones, $arr)
    {
        foreach ($peticiones as $peticion) {
            foreach ($indicadores as $key => $value) {
                $anterior = $key + 1;
                $fechaPost = ($anterior < count($indicadores)) ? new \DateTime($indicadores[$anterior]) : null;
                if ($peticion->getCreated_at() >= new \DateTime($value) && $peticion->getCreated_at() <= $fechaPost) {
                    if ($peticion->getEstado()->getIsDefault()) { //peticion nueva
                        $arr[$value]['nueva'] = $arr[$value]['nueva'] + 1;
                    } elseif ($peticion->getEstado()->getIsCurso()) { //peticion en curso
                        $arr[$value]['encurso'] = $arr[$value]['encurso'] + 1;
                    } elseif ($peticion->getEstado()->getIsCerrado()) {
                        $arr[$value]['cerrada'] = $arr[$value]['cerrada'] + 1;
                    }
                    $arr[$value]['retrabajo'] = $peticion->getTipo() == 1 ? $arr[$value]['retrabajo'] + 1 : $arr[$value]['retrabajo'];
                    $arr[$value]['normal'] = $peticion->getTipo() == 0 ? $arr[$value]['normal'] + 1 : $arr[$value]['normal'];
                }
            }
        }
        return $arr;
    }

    /**
     * @Route("/crm/peticion/{id}/delete", name="crm_peticion_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_PETICION_ELIMINAR")
     */
    public function deleteAction(Request $request, Peticion $peticion)
    {
        $form = $this->createDeleteForm($peticion->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->peticionManager->delete($peticion)) {
                $this->setFlash('success', 'Peido de trabajo eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el pedido del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/crm/peticion/{idOrganizacion}/ajaxpanel", name="crm_peticion_ajaxpanel",
     *     requirements={
     *         "idOrganizacion": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @ParamConverter("id", class="App:Organizacion", options={"id" = "idOrganizacion"})
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function ajaxpanelAction(Request $request, Organizacion $organizacion)
    {

        $fecha_desde = $this->utilsManager->datetime2sqltimestamp($request->get('desde'), false);
        $desde = new \DateTime($fecha_desde, new \DateTimeZone($organizacion->getTimezone()));
        $fecha_hasta = $this->utilsManager->datetime2sqltimestamp($request->get('hasta'), false);
        $hasta = new \DateTime($fecha_hasta, new \DateTimeZone($organizacion->getTimezone()));
        $hasta2 = new \DateTime($fecha_hasta, new \DateTimeZone($organizacion->getTimezone()));
        $ots = $this->ordentrabajoManager->findAll($organizacion, null, $desde, $hasta);
        $peticiones = $this->peticionManager->findAllEstadoByOrg($organizacion, $desde, $hasta);
        $indices = $this->mantenimientoManager->obtenerIndice($organizacion, $desde, $hasta);
        $arrIndice = $this->getUltIndice($indices);
        $petBarra = $this->getAtrasadas($peticiones, $desde, $hasta, true);
        $cantidad = $this->getCantidad($ots);
        $asignaciones = $this->getAsignaciones($peticiones);
        $asignacionesHtml = $this->renderView('crm/Dashboard/asignaciones.html.twig', array(
            'asignaciones' => $asignaciones,
        ));
        $peticionesHtml = $this->renderView('crm/Dashboard/peticiones_panel.html.twig', array(
            'peticiones' => $peticiones, 'desde' => $desde,
            'hasta' => $hasta2
        ));
        //  die('////<pre>' . nl2br(var_export($peticionesHtml, true)) . '</pre>////');
        $ordenesHtml = $this->renderView('crm/Dashboard/ordenestrabajo_panel.html.twig', array(
            'ordenes' => $ots, 'desde' => $desde,
            'hasta' => $hasta2
        ));
        $cantidadHtml = $this->renderView('crm/Dashboard/cantidad_panel.html.twig', array(
            'cantidad' => $cantidad,
        ));
        $indiceHtml = $this->renderView("crm/Dashboard/indice_panel.html.twig", array(
            'arrIndice' => $arrIndice, 'desde' => $desde,
            'hasta' => $hasta2
        ));

        return new Response(json_encode(array(
            'status' => 'OK',
            'indices' => $indices,
            'atrasadas' => $petBarra,
            'petLength' => count($peticiones),
            'otLength' => count($ots),
            'asignacionesHtml' => $asignacionesHtml,
            'ordenesHtml' => $ordenesHtml,
            'cantidadHtml' => $cantidadHtml,
            'peticionesHtml' => $peticionesHtml,
            'indiceHtml' => $indiceHtml
        )));
    }

    /**
     * @Route("/crm/peticion/{id}/cerrar", name="crm_peticion_cerrar",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_PETICION_EDITAR")
     */
    public function cerrarAction(Request $request, Peticion $peticion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Cerrar");
        $fecha_fin = $this->utilsManager->datetime2sqltimestamp($request->get('form_peticion')['fecha_fin'], false);
        $motivoPeticion = $request->get('form_peticion')['motivo'];
        $organizacion = $this->userloginManager->getOrganizacion();
        $fin = new \DateTime($fecha_fin, new \DateTimeZone($organizacion->getTimezone()));

        if ($this->peticionManager->setCerrada($peticion, $fin, $motivoPeticion)) {
            if ($this->peticionManager->save($peticion)) {
                $this->setFlash('success', 'Pedido de trabajo cerrado con exito.');
            }
        } else {
            $this->setFlash('error', 'Error al cerrar Pedido de trabajo.');
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }
}
