<?php

namespace App\Controller\crm;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Estado;
use App\Form\crm\EstadoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BreadcrumbManager;

class EstadoController extends AbstractController
{

    private $breadcrumbManager;

    function __construct(BreadcrumbManager $breadcrumbManager)
    {
        $this->breadcrumbManager = $breadcrumbManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/estado/list", name="crm_estados_list")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Estados");
        $estados = $this->getEntityManager()->getRepository('App:Estado')->findBy(array(), array('posicion' => 'ASC'));
        return $this->render('crm/Estado/list.html.twig', array(
            'estados' => $estados,
            'menu' => $this->getShowMenu()
        ));
    }

    private function getShowMenu()
    {
        $menu = array(
            1 => array(
                array(
                    'label' => 'Estado',
                    'imagen' => 'fa fa-plus',
                    'url' => $this->generateUrl('crm_estado_new')
                )
            )
        );
        return $menu;
    }

    /**
     * @Route("/estado/new", name="crm_estado_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $estado = new Estado();
        $form = $this->createForm(EstadoType::class, $estado);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEntityManager()->persist($estado);
            $this->getEntityManager()->flush();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Estado");
        return $this->render('crm/Estado/new.html.twig', array(
            'estado' => $estado,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/estado/{id}/edit", name="crm_estado_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Estado $estado)
    {
        $form = $this->createForm(EstadoType::class, $estado);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEntityManager()->persist($estado);
            $this->getEntityManager()->flush();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Estado");
        return $this->render('crm/Estado/edit.html.twig', array(
            'estado' => $estado,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/estado/{id}/delete", name="crm_estado_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Estado $estado)
    {
        $this->getEntityManager()->remove($estado);
        $this->getEntityManager()->flush();
        return $this->redirect($this->breadcrumbManager->getVolver());
    }
}
