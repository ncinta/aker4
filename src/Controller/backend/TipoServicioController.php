<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\TipoServicio;
use App\Form\backend\TipoServicioType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class TipoServicioController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\TipoServicio');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * @Route("/tiposervicio/list", name="tiposervicio_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction()
    {

        $tiposservicios = $this->getRepository()->findBy(array(), array('nombre' => 'ASC'));

        //se arma el menu
        $menu = array();
        $menu['Agregar'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('tiposervicio_new')
        );
        //el usuario logueado no tiene organizacion
        return $this->render('backend/TipoServicio/list.html.twig', array(
            'tiposservicios' => $tiposservicios,
            'menu' => $menu
        ));
    }

    /**
     * @Route("/tiposervicio/{id}/show", name="tiposervicio_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, TipoServicio $tiposervicio)
    {
        $menu['Editar'] = array(
            'imagen' => '',
            'url' => $this->generateUrl('tiposervicio_edit', array(
                'id' => $tiposervicio->getId(),
                'return' => 'show'
            ))
        );
        $deleteForm = $this->createDeleteForm($tiposervicio->getId());

        return $this->render('backend/TipoServicio/show.html.twig', array(
            'menu' => $this->getShowMenu($tiposervicio),
            'delete_form' => $deleteForm->createView(),
            'tiposervicio' => $tiposervicio
        ));
    }

    /**
     * @Route("/tiposervicio/new", name="tiposervicio_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request)
    {

        $tiposervicio = new TipoServicio();
        $form = $this->createForm(TipoServicioType::class, $tiposervicio);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();

            $tiposervicio->upload();

            $em->persist($tiposervicio);
            $em->flush();

            $this->setFlash('success', sprintf('Se ha creado el tipo de servicio.'));

            return $this->redirect($this->generateUrl('tiposervicio_list'));
        }

        return $this->render('backend/TipoServicio/new.html.twig', array(
            'tiposervicio' => $tiposervicio,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/tiposervicio/{id}/edit", name="tiposervicio_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, TipoServicio $tiposervicio)
    {
        $em = $this->getEntityManager();
        if (!$tiposervicio) {
            throw $this->createNotFoundException('Código de Tipo de Servicio no encontrado.');
        }
        $form = $this->createForm(TipoServicioType::class, $tiposervicio);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tiposervicio->upload();
            $em->persist($tiposervicio);
            $em->flush();
            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($tiposervicio->getNombre())));
            return $this->redirect($this->generateUrl('tiposervicio_list'));
        }
        return $this->render('backend/TipoServicio/edit.html.twig', array(
            'tiposervicio' => $tiposervicio,
            'form' => $form->createView(),
        ));
    }

    /**
     * Elimina una aplicacion, sus modulos y permisos.
     * @IsGranted("ROLE_ROOT")
     * @Template()
     */
    public function deleteAction($id = null)
    {
        $em = $this->getEntityManager();
        $tiposervicio = $em->find('App:TipoServicio', $id);

        $em->remove($tiposervicio);
        $em->flush();

        $this->setFlash('success', 'El tipo de servicio ha sido eliminada');

        return $this->redirect($this->generateUrl('iposervicio_list'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Setea el flash de mensajes
     * @param type $action  p/ej: succes, error, informations
     * @param type $value   cadena de tecto con el mensaje
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
