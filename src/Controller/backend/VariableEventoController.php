<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\VariableEvento;
use App\Form\backend\VariableEventoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class VariableEventoController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\VariableEvento');
    }

    private function getListMenu()
    {
        $menu = array();
        $menu['Agregar'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('variableevento_new')
        );
        return $menu;
    }

    /**
     * lista todas las variable de eventos disponibles. Solo los ROOT tiene acceso.
     * @Route("/variableevento/list", name="variableevento_list")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $variables = $this->getRepository('App:VariableEvento')->findBy(array(), array('nombre' => 'asc'));
        $breadcrumbManager->push($request->getRequestUri(), "Variables de Evento");
        return $this->render('backend/VariableEvento/list.html.twig', array(
            'variables' => $variables,
            'menu' => $this->getListMenu(),
        ));
    }

    /**
     * @Route("/variableevento/new", name="variableevento_new")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Variable de Evento");

        $variable = new VariableEvento();
        $form = $this->createForm(VariableEventoType::class, $variable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getEntityManager();
            $em->persist($variable);
            $em->flush();
            $this->setFlash(
                'success',
                sprintf('Se ha creado la variable <b>%s</b> en el sistema.', strtoupper($variable->getNombre()))
            );
            return $this->redirect($breadcrumbManager->getVolver());
        } else {
            $this->setFlash('error', 'Hubo problemas para crear la variable');
        }
        return $this->render('backend/VariableEvento/new.html.twig', array(
            'variable' => $variable,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/variableevento/{id}/edit", name="variableevento_edit")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, VariableEvento $variable, BreadcrumbManager $breadcrumbManager)
    {

        $form = $this->createForm(VariableEventoType::class, $variable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getEntityManager();
            $em->persist($variable);
            $em->flush();
            $this->setFlash(
                'success',
                sprintf('Se ha modificado la variable <b>%s</b> en el sistema.', strtoupper($variable->getNombre()))
            );
            return $this->redirect($breadcrumbManager->getVolver());
        } else {
            $this->setFlash('error', 'Hubo problemas para modificar la variable');
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar");
        return $this->render('backend/VariableEvento/edit.html.twig', array(
            'variable' => $variable,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/variableevento/{id}/delete", name="variableevento_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(VariableEvento $variable)
    {
        $em = $this->getEntityManager();
        if (!$variable) {
            throw $this->createNotFoundException('Código de Varaible de Evento no encontrado.');
        }
        $em->remove($variable);
        $em->flush();
        $this->setFlash('success', 'Se ha eliminado la variable');
        return array(
            'variables' => $this->getRepository('App:VariableEvento')->findBy(array(), array('nombre' => 'asc')),
            'menu' => $this->getListMenu(),
        );
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
