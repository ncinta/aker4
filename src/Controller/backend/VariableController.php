<?php

namespace App\Controller\backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Variable;
use App\Form\backend\VariableType;
use App\Form\backend\VariableCompuestaType;
use App\Entity\Modelo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class VariableController extends AbstractController
{

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Variable');
    }

    /**
     * @Route("/variable/{id}/new", name="variable_new")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, Modelo $modelo, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$modelo) {
            throw $this->createNotFoundException('Unable to find Modelo entity.');
        }


        $variable = new Variable();
        $variable->setModelo($modelo);

        $options['tipos'] = $variable->getArrayTipo();
        $form = $this->createForm(VariableType::class, $variable, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($variable);
            $em->flush();
            $breadcrumbManager->pop();
            if ($variable->getTipoVariable() == $variable::TVAR_COMPUESTA) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado la variable <b>%s</b> en el sistema. Como es compuesta debe definir las variables con la que se asocia la creada.', strtoupper($variable->getNombre()))
                );
                return $this->redirect($this->generateUrl('backend_variable_compuesta_edit', array('id' => $variable->getId())));
            } else {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado la variable <b>%s</b> en el sistema.', strtoupper($variable->getNombre()))
                );
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nueva Variable");
        return $this->render('backend/Variable/new.html.twig', array(
            'variable' => $variable,
            'modelo' => $modelo,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/variable/{id}/edit", name="variable_edit", methods={"GET", "POST"})    
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, Variable $variable, BreadcrumbManager $breadcrumbManager)
    {

        $em = $this->getDoctrine()->getManager();
        $options['tipos'] = $variable->getArrayTipo();
        $form = $this->createForm(VariableType::class, $variable, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($variable);
            $em->flush();
            $breadcrumbManager->pop();
            $this->get('session')->getFlashBag()->add(
                'success',
                sprintf('Se ha modificado la variable <b>%s</b> en el sistema.', strtoupper($variable->getNombre()))
            );
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar");
        return $this->render('backend/Variable/edit.html.twig', array(
            'variable' => $variable,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/variable/{id}/delete", name="variable_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(Variable $variable)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$variable) {
            throw $this->createNotFoundException('Código de Varaible de Evento no encontrado.');
        }
        $modelo = $variable->getModelo();
        $em->remove($variable);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Se ha eliminado la variable');
        return $this->redirect($this->generateUrl('backend_modelo_show', array('id' => $modelo->getId())));
    }

    /**
     * @Route("/variable/{id}/editcompuesta", name="variable_editcompuesta")
     * @Method({"GET", "POST"})     
     * @IsGranted("ROLE_ROOT")
     */
    public function editCompuestaAction(Request $request, Variable $varParent, BreadcrumbManager $breascrumbManager)
    {

        $em = $this->getDoctrine()->getManager();
        $modelo = $varParent->getModelo();
        $options['variables'] = $em->getRepository('App:Variable')->getOnlyVariables($modelo);
        $form = $this->createForm(VariableCompuestaType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            foreach ($data['variables'] as $id) {
                $var = $em->getRepository('App:Variable')->find($id);
                if ($var) {
                    $var->setVariablePadre($varParent);
                    $em->persist($var);
                    $em->flush();
                }
            }
            $breascrumbManager->pop();
            $this->get('session')->getFlashBag()->add(
                'success',
                sprintf('Se ha modificado la variable <b>%s</b> en el sistema.', strtoupper($varParent->getNombre()))
            );
            return $this->redirect($breascrumbManager->getVolver());
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Hubo problemas para modificar la variable');
        }
        $breascrumbManager->push($request->getRequestUri(), "Editar V. Compuesta");
        return $this->render('backend/Variable/editCompuesta.html.twig', array(
            'variable' => $varParent,
            'form' => $form->createView()
        ));
    }
}
