<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Capturador;
use App\Model\app\BreadcrumbManager;
use App\Form\backend\CapturadorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CapturadorController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Capturador');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * lista todas los capturadores disponibles. Solo los SUPER_ADMIN tiene acceso.
     * @Route("/capturador/list", name="capturador_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Listado de Capturadores");
        $menu = array();
        $menu['Agregar Capturador'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('capturador_new')
        );
        return $this->render('backend/Capturador/list.html.twig', array(
            'capturadores' => $this->getRepository('App:Capturador')->findBy(array(), array('nombre' => 'asc')),
            'menu' => $menu
        ));
    }

    /**
     * @Route("/capturador/{id}/show", name="capturador_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, Capturador $capturador, BreadcrumbManager $breadcrumbManager)
    {
        $menu = array();
        $menu['Editar'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('capturador_edit', array(
                'id' => $capturador->getId(),
            ))
        );
        $menu['Eliminar'] = array(
            'imagen' => 'icon-minus icon-blue',
            'url' => '#modalDelete',
            'extra' => 'data-toggle=modal',
        );
        $deleteForm = $this->createDeleteForm($capturador->getId());
        $breadcrumbManager->push($request->getRequestUri(), $capturador->getNombre());

        return $this->render('backend/Capturador/show.html.twig', array(
            'menu' => $menu,
            'delete_form' => $deleteForm->createView(),
            'capturador' => $capturador,
        ));
    }

    /**
     * @Route("/capturador/new", name="capturador_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Capturador");
        $capturador = new Capturador();
        $form = $this->createForm(CapturadorType::class, $capturador);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getEntityManager();

            $em->persist($capturador);
            $em->flush();

            $this->setFlash('success', sprintf('Se ha creado el capturador.'));

            return $this->redirect($breadcrumbManager->getVolver());
        }
        return $this->render('backend/Capturador/new.html.twig', array(
            'capturador' => $capturador,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/capturador/{id}/edit", name="capturador_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, Capturador $capturador, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getEntityManager();
        if (!$capturador) {
            throw $this->createNotFoundException('Código de Aplicación no encontrado.');
        }
        $form = $this->createForm(CapturadorType::class, $capturador);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em->persist($capturador);
            $em->flush();

            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($capturador->getNombre())));

            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Editar Capturador");

        return $this->render('backend/Capturador/edit.html.twig', array(
            'capturador' => $capturador,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/capturador/{id}/delete", name="capturador_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ADMIN_DISTRIBUIDOR")
     */
    public function deleteAction(Capturador $capturador)
    {
        $em = $this->getEntityManager();
        $em->remove($capturador);
        $em->flush();

        $this->setFlash('success', 'El Capturador ha sido eliminada');

        return $this->redirect($this->generateUrl('capturador_list'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Setea el flash de mensajes
     * @param type $action  p/ej: succes, error, informations
     * @param type $value   cadena de tecto con el mensaje
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
