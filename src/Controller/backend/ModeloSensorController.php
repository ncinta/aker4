<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\backend\ModeloSensorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BreadcrumbManager;
use App\Entity\ModeloSensor;
use App\Entity\Modelo;

class ModeloSensorController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\ModeloSensor');
    }

    /**
     * @Route("/modelosensor/{id}/new", name="modelosensor_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, Modelo $modelo, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$modelo) {
            throw $this->createNotFoundException('Unable to find Modelo entity.');
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Sensor");

        $sensor = new ModeloSensor();
        $sensor->setModelo($modelo);

        $form = $this->createForm(ModeloSensorType::class, $sensor);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($sensor);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                sprintf('Se ha creado el sensor <b>%s</b> en modelo.', strtoupper($sensor->getNombre()))
            );
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }
        return $this->render('backend/ModeloSensor/new.html.twig', array(
            'sensor' => $sensor,
            'modelo' => $modelo,
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/modelosensor/{id}/edit", name="modelosensor_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, ModeloSensor $sensor, BreadcrumbManager $breadcrumbManager)
    {

        $form = $this->createForm(ModeloSensorType::class, $sensor);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($sensor);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                sprintf('Se ha modificado el sensor <b>%s</b> en el sistema.', strtoupper($sensor->getNombre()))
            );
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar");
        return $this->render('backend/ModeloSensor/edit.html.twig', array(
            'sensor' => $sensor,
            'form' => $form->createView()
        ));
    }



    /**
     * @Route("/modelosensor/{id}/delete", name="modelosensor_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(ModeloSensor $modelo)
    {
        $em = $this->getEntityManager();
        if (!$modelo) {
            throw $this->createNotFoundException('Código de Sensor no encontrado.');
        };
        $em->remove($modelo);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Se ha eliminado el modelo');
        return $this->redirect($this->generateUrl('modelo_show', array('id' => $modelo->getId())));
    }
}
