<?php

namespace App\Controller\backend;

use App\Entity\Metrica;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Modelo;
use App\Form\backend\MetricaType;
use App\Form\backend\ModeloType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\MetricaManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MetricaController extends AbstractController
{
    private $userloginManager;
    private $breadcrumbManager;
    private $organizacionManager;
    private $metricaManager;
    private $utilsManager;

    function __construct(
        UserLoginManager $userlogin,
        BreadcrumbManager $breadcrumb,
        OrganizacionManager $organizacionManager,
        MetricaManager $metricaManager,
        UtilsManager $utilsManager
    ) {
        $this->userloginManager = $userlogin;
        $this->breadcrumbManager = $breadcrumb;
        $this->organizacionManager = $organizacionManager;
        $this->metricaManager = $metricaManager;
        $this->utilsManager = $utilsManager;
    }


    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/metrica/informes", name="metrica_informes")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function metricainformesAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Metrica de informes");
        $organizacion = $this->userloginManager->getOrganizacion();
        $distribuidores = $this->organizacionManager->findAllDistribuidores($organizacion);
        $clientes = $this->organizacionManager->findAllClientes($organizacion);
        $organizaciones = array();

        foreach ($distribuidores as $distribuidor) {
            foreach ($distribuidor->getOrganizaciones() as $org) {
                $organizaciones[] = $org;
            }
        }

        foreach ($clientes as $cliente) {
            $organizaciones[] = $cliente;
        }
        $options['organizaciones'] = $organizaciones;

        $form = $this->createForm(MetricaType::class, null, $options);
        $hasta = (new \DateTime())->format('d-m-Y');

        $desde = new \DateTime();
        $desde->modify('-1 month');
        $desde = $desde->format('d-m-Y');

        $desde = $this->utilsManager->datetime2sqltimestamp($desde, true);
        $hasta = $this->utilsManager->datetime2sqltimestamp($hasta, false);

        $metricas = $this->metricaManager->getMetricas(Metrica::TIPO_INFORME, null, $desde, $hasta);
        $data = $this->metricaManager->getData($metricas);

        $lastMetricas = array_slice($metricas, 0, 100);
        $dataLastMetricas = $this->metricaManager->getDataLastMetricas($lastMetricas);


        //die('////<pre>' . nl2br(var_export(count($metricas), true)) . '</pre>////');


        return $this->render('backend/Metrica/informes/index.html.twig', array(
            'form' => $form->createView(),
            'pieOrganizacion' => json_encode($data['pieOrganizacion']),
            'pieInformes' => json_encode($data['pieInformes']),
            'dataInfoServicios' => $data['dataInfoServicios'],
            'dataInfoDias' => $data['dataInfoDias'],
            'lastMetricas' => $dataLastMetricas
        ));
    }

    /**
     * @Route("/metricas/ajax", name="metricas_ajax")
     * @Method({"GET", "POST"})  
     */
    public function ajaxpanelAction(Request $request)
    {
        $tmp = array();
        $historicos = array();
        $consulta = array();
        parse_str($request->get('formMetricas'), $tmp);
        $form = $tmp['metrica'];

        $desde = $this->utilsManager->datetime2sqltimestamp($form['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($form['hasta'], true);
        $organizacion = $form['organizacion'] == "0" ? null : $this->organizacionManager->find(intval($form['organizacion']));

        $metricas = $this->metricaManager->getMetricas(Metrica::TIPO_INFORME, $organizacion, $desde, $hasta);
        $data = $this->metricaManager->getData($metricas);

        $lastMetricas = array_slice($metricas, 0, 100);
        $dataLastMetricas = $this->metricaManager->getDataLastMetricas($lastMetricas);

        $html = $this->renderView('backend/Metrica/informes/pantalla.html.twig', array(
            'dataInfoServicios' => $data['dataInfoServicios'],
            'dataInfoDias' => $data['dataInfoDias'],
            'lastMetricas' => $dataLastMetricas
        ));

        return new Response(json_encode(array(
            'pieOrganizacion' => json_encode($data['pieOrganizacion']),
            'pieInformes' => json_encode($data['pieInformes']), 'html' => $html
        )), 200);

        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');




    }



    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
