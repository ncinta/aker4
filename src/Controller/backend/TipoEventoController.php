<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TipoEvento;
use App\Form\backend\TipoEventoType;
use App\Form\backend\VariableEventoSelectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\TipoEventoManager;
use App\Model\app\Router\TipoEventoRouter;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class TipoEventoController extends AbstractController
{

    private $tipoEventoRouter;

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\TipoEvento');
    }

    function __construct(TipoEventoRouter $tipoEventoRouter)
    {
        $this->tipoEventoRouter = $tipoEventoRouter;
    }

    private function getListMenu()
    {

        return array(
            1 => array($this->tipoEventoRouter->btnNew()),
        );
    }
    private function getShowMenu($tipoEvento)
    {

        return array(
               1 => array($this->tipoEventoRouter->btnEdit($tipoEvento)),
              2 => array($this->tipoEventoRouter->btnAddVariable()),
        );
    }

    /**
     * @Route("/tipoevento/list", name="tipoevento_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $tiposeventos = $this->getRepository('App:TipoEvento')->findBy(array(), array('nombre' => 'asc'));
        $breadcrumbManager->push($request->getRequestUri(), "Tipos de Evento");
        return $this->render('backend/TipoEvento/list.html.twig', array(
            'tiposeventos' => $tiposeventos,
            'menu' => $this->getListMenu(),
        ));
    }

    /**
     * @Route("/tipoevento/{id}/show", name="tipoevento_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, TipoEvento $tipoEvento, TipoEventoManager $tipoEventoManager)
    {

        // $deleteForm = $this->createDeleteForm($tipoEvento->getId());
        $variables = $tipoEventoManager->findAllVariables();
        foreach($variables as $variable){
            if(!$tipoEvento->hasVariable($variable)){
                   $options['variables'][$variable->getNombre()] = $variable->getId() ;
            }
        }
        return $this->render('backend/TipoEvento/show.html.twig', array(
            'menu' => $this->getShowMenu($tipoEvento),
            'formVariable' => ($this->createForm(VariableEventoSelectType::class,null, $options))->createView(),
            //   'delete_form' => $deleteForm->createView(),
            'tipoEvento' => $tipoEvento
        ));
    }

    /**
     * @Route("/tipoevento/new", name="tipoevento_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager, TipoEventoManager $tipoEventoManager)
    {
        $variables = array();
        $tipoevento = new TipoEvento();
        $options['variables'] = $tipoEventoManager->findAllVariables();
        //  die('////<pre>' . nl2br(var_export($options, true)) . '</pre>////');
        $form = $this->createForm(TipoEventoType::class, $tipoevento, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //    die('////<pre>' . nl2br(var_export($request->get('secur_Backend_variableeventotype'), true)) . '</pre>////');
            if (!is_null($request->get('secur_Backend_variableeventotype')) && isset($request->get('secur_Backend_variableeventotype')['variables'])) {
                foreach ($request->get('secur_Backend_variableeventotype')['variables'] as $variable) {
                    $variables[] = $tipoEventoManager->findVariable(intval($variable));
                }
            }
            $data = $form->getData();

            $ev = new TipoEvento();
            $ev->setNombre($data->getNombre());
            $ev->setCodename($data->getCodename());
            foreach ($variables as $value) {
                $ev->addVariables($value);
            }
            //$evento = $this->get('calypso.tipoevento')->setVariables($tipoevento, $variables);
            $tipoEventoManager->save($ev);
            $breadcrumbManager->pop();

            $this->setFlash(
                'success',
                sprintf('Se ha creado el tipo de evento <b>%s</b> en el sistema.', strtoupper($tipoevento->getNombre()))
            );
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Tipo de Evento");
        return $this->render('backend/TipoEvento/new.html.twig', array(
            'tipoevento' => $tipoevento,
            'form' => $form->createView()
        ));
    }

        /**
     * @Route("/tipoevento/{id}/edit", name="tipoevento_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, TipoEvento $tipoEvento)
    {
        $em = $this->getEntityManager();
        if (!$tipoEvento) {
            throw $this->createNotFoundException('Código de Tipo de Evento no encontrado.');
        }
        $form = $this->createForm(TipoEventoType::class, $tipoEvento);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($tipoEvento);
            $em->flush();
            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($tipoEvento->getNombre())));
            return $this->redirect($this->generateUrl('tipoevento_list'));
        }
        return $this->render('backend/TipoEvento/edit.html.twig', array(
            'tipoEvento' => $tipoEvento,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("/tipoevento/{id}/delete", name="tipoevento_evento",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(TipoEvento $tipo)
    {
        $em = $this->getEntityManager();
        $tipoevento = $em->getRepository('App:TipoEvento')->find($tipo->getId());
        if (!$tipoevento) {
            throw $this->createNotFoundException('Código de Tipo de Evento no encontrado.');
        }
        $em->remove($tipoevento);
        $em->flush();
        $this->setFlash('success', 'Se ha eliminado el tipo de evento');
        return $this->render('backend/TipoEvento/delete.html.twig', array(
            'tiposeventos' => $this->getRepository('App:TipoEvento')->findBy(array(), array('nombre' => 'asc')),
            'menu' => $this->getListMenu(),
        ));
    }


    /**
     * @Route("/tipoevento/deletevar", name="tipoevento_deletevar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deletevarAction(Request $request, TipoEventoManager $tipoEventoManager)
    {
        $em = $this->getEntityManager();
        $id = $request->get('id');  //rescato el id si viene desde otro mapa.
        $idVariable = $request->get('idVariable');  //rescato el id si viene desde otro mapa.
        $tipoEvento = $em->getRepository('App:TipoEvento')->find($id);
        
        if (!$tipoEvento) {
            throw $this->createNotFoundException('Código de Tipo de Evento no encontrado.');
        }
        try  {
            $variable = $tipoEventoManager->findVariable($idVariable);
            $tipoEvento->removeVariable($variable);
            $em->persist($tipoEvento);
            $em->flush();
            return new Response(json_encode(array(
                'status' => 'ok',
                'html' => $this->renderView('backend/TipoEvento/variables.html.twig', array(
                    'tipoEvento' => $tipoEvento,
                )),
            )));
        }catch(Exception $e){

            return new Response(json_encode(array(
                'status' => 'error'
            )));
        }
    }  
    
    /**
     * @Route("/tipoevento/addvar", name="tipoevento_addvar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function addvarAction(Request $request, TipoEventoManager $tipoEventoManager)
    {
        $em = $this->getEntityManager();
        $tmp = array();
        parse_str($request->get('formVariable'), $tmp);
        $variables = $tmp['variables_evento']['variable'];
       // die('////<pre>' . nl2br(var_export($variables, true)) . '</pre>////');
        $id = $request->get('id');  //rescato el id si viene desde otro mapa.
        $tipoEvento = $em->getRepository('App:TipoEvento')->find($id);
        
        if (!$tipoEvento) {
            throw $this->createNotFoundException('Código de Tipo de Evento no encontrado.');
        }
        try  {
            foreach($variables as $idVariable){
               $variable = $tipoEventoManager->findVariable($idVariable);
                $tipoEvento->addVariables($variable);
            }
            $em->persist($tipoEvento);
            $em->flush();
            return new Response(json_encode(array(
                'status' => 'ok',
                'html' => $this->renderView('backend/TipoEvento/variables.html.twig', array(
                    'tipoEvento' => $tipoEvento,
                )),
            )));
        }catch(Exception $e){

            return new Response(json_encode(array(
                'status' => 'error'
            )));
        }
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
