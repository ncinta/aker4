<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Modelo;
use App\Form\backend\ModeloType;
use App\Model\app\BreadcrumbManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ModeloController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/modelo/list", name="modelo_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Listado de Modelos");
        $em = $this->getEntityManager();

        $menu = array();
        $menu['Agregar'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('modelo_new')
        );
        $modelo = $em->getRepository('App:Modelo')->findBy(array(), array('nombre' => 'asc'));

        return $this->render('backend/Modelo/list.html.twig', array(
            'entities' => $modelo,
            'menu' => $menu,
        ));
    }

    /**
     * @Route("/modelo/{id}/show", name="modelo_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, Modelo $entity, BreadcrumbManager $breadcrumbManager)
    {

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Modelo entity.');
        }
        $breadcrumbManager->push($request->getRequestUri(), $entity->getNombre());
        $deleteForm = $this->createDeleteForm($entity->getId());
        return $this->render('backend/Modelo/show.html.twig', array(
            'menu' => $this->getShowMenu($entity),
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    private function getShowMenu($modelo)
    {
        $menu = array();
        $menu['Editar'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('modelo_edit', array(
                'id' => $modelo->getId(),
            ))
        );
        $menu['Agregar Variable'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('variable_new', array(
                'id' => $modelo->getId(),
            ))
        );
        $menu['Agregar Sensores'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('modelosensor_new', array(
                'id' => $modelo->getId(),
            ))
        );
        return $menu;
    }

    /**
     * Displays a form to create a new Modelo entity.
     * @Route("/modelo/new", name="modelo_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Modelo");
        $entity = new Modelo();
        $options['tprog'] = $entity->getArrayTipoProgramacion();
        $form = $this->createForm(ModeloType::class, $entity, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($breadcrumbManager->getVolver());
        }

        return $this->render('backend/Modelo/new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/modelo/{id}/edit", name="modelo_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")           
     */
    public function editAction(Request $request, Modelo $modelo, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Editar Modelo");
        $em = $this->getEntityManager();

        $options['tprog'] = $modelo->getArrayTipoProgramacion();
        $form = $this->createForm(ModeloType::class, $modelo, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em->persist($modelo);
            $em->flush();

            $this->setFlash('success', 'Los datos de ' . $modelo->getNombre() . ' han sido actualizados.');
            return $this->redirect($breadcrumbManager->getVolver());
        }
        return $this->render('backend/Modelo/edit.html.twig', array(
            'entity' => $modelo,
            'form' => $form->createView(),
        ));
    }


    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
