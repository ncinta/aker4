<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\ServiceManager;
class PanelControlController extends AbstractController
{

    private $service;

    public function __construct(ServiceManager $service)
    {        
        $this->service = $service;
    }
    /**
     * @Route("/panelcontrol", name="panelcontrol")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function panelcontrolAction()
    {        
        return $this->render('backend/PanelControl/index.html.twig', array(
            'status' => $this->service->consulServicios()
        ));
    }
}
