<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Capturador;
use App\Entity\MotivoTx;
use App\Model\app\BreadcrumbManager;
use App\Form\backend\CapturadorType;
use App\Form\backend\MotivoTxType;
use App\Model\app\Router\PanelControlRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MotivoTxController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\MotivoTx');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * lista todas los motivos de transmisión disponibles. Solo los SUPER_ADMIN tiene acceso.
     * @Route("/motivotx/list", name="motivotx_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager, PanelControlRouter $panelControlRouter)
    {
        $breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Motivos");
        $menu[] = array(1 => $panelControlRouter->btnNewMotivoTx());
        return $this->render('backend/MotivoTx/list.html.twig', array(
            'motivos' => $this->getRepository('App:MotivoTx')->findBy(array(), array('nombre' => 'asc')),
            'menu' => $menu
        ));
    }

    /**
     * @Route("/motivotx/{id}/show", name="motivotx_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, MotivoTx $motivotx, BreadcrumbManager $breadcrumbManager, PanelControlRouter $panelControlRouter)
    {
        $menu[] = array(1 => $panelControlRouter->btnEditMotivoTx($motivotx),$panelControlRouter->btnDelete());
        $deleteForm = $this->createDeleteForm($motivotx->getId());
        $breadcrumbManager->pushPorto($request->getRequestUri(), $motivotx->getNombre());

        return $this->render('backend/MotivoTx/show.html.twig', array(
            'menu' => $menu,
            'delete_form' => $deleteForm->createView(),
            'motivotx' => $motivotx,
        ));
    }

    /**
     * @Route("/motivotx/new", name="motivotx_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo MotivoTx");
        $motivotx = new MotivoTx();
        $form = $this->createForm(MotivoTxType::class, $motivotx);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getEntityManager();

            $em->persist($motivotx);
            $em->flush();

            $this->setFlash('success', sprintf('Se ha creado el Motivo de transmisión.'));

            return $this->redirect($breadcrumbManager->getVolver());
        }
        return $this->render('backend/MotivoTx/new.html.twig', array(
            'motivotx' => $motivotx,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/motivotx/{id}/edit", name="motivotx_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, MotivoTx $motivotx, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getEntityManager();
        if (!$motivotx) {
            throw $this->createNotFoundException('Código de motivo no encontrado.');
        }
        $form = $this->createForm(MotivoTxType::class, $motivotx);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em->persist($motivotx);
            $em->flush();

            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($motivotx->getNombre())));

            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->pushPorto($request->getRequestUri(), "Editar MotivoTx");

        return $this->render('backend/MotivoTx/edit.html.twig', array(
            'motivotx' => $motivotx,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/motivotx/{id}/delete", name="motivotx_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(MotivoTx $motivotx)
    {
        $em = $this->getEntityManager();
        $em->remove($motivotx);
        $em->flush();

        $this->setFlash('success', 'El motivo de transmisión ha sido eliminado');

        return $this->redirect($this->generateUrl('motivotx_list'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Setea el flash de mensajes
     * @param type $action  p/ej: succes, error, informations
     * @param type $value   cadena de tecto con el mensaje
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
