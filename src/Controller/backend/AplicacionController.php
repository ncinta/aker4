<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Aplicacion;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Form\backend\AplicacionType;

class AplicacionController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Aplicacion');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }


    /**
     * @Route("/app/aplicacion/list", name="aplicacion_list",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAllAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $aplicaciones = $this->getRepository('App:Aplicacion')->findBy(array(), array('nombre' => 'asc'));
        $breadcrumbManager->push($request->getRequestUri(), "Listado de Aplicaciones");
        //el usuario logueado no tiene organizacion
        return $this->render('backend/Aplicacion/list_all.html.twig', array(
            'aplicaciones' => $aplicaciones,
            'menu' => $this->getListMenu()
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu()
    {
        $menu = array();
        $menu['Agregar Aplicacion'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('aplicacion_new')
        );
        return $menu;
    }

    /**
     * @Route("/aplicacion/{id}/show", name="aplicacion_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, Aplicacion $aplicacion, BreadcrumbManager $breadcrumbManager)
    {

        $modulos = $this->getEntityManager()->getRepository('App:Modulo')->getAll($aplicacion);
        $deleteForm = $this->createDeleteForm($aplicacion->getId());
        $breadcrumbManager->push($request->getRequestUri(), $aplicacion->getNombre());
        $menu = array();
        $menu['Agregar Módulo'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('modulo_new', array(
                'id' => $aplicacion->getId()
            ))
        );
        $menu['Editar Aplicación'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('aplicacion_edit', array(
                'id' => $aplicacion->getId(),
            ))
        );
        $menu['Eliminar Aplicación'] = array(
            'imagen' => 'icon-minus icon-blue',
            'url' => '#modalDelete',
            'extra' => 'data-toggle=modal',
        );
        return $this->render('backend/Aplicacion/show.html.twig', array(
            'menu' => $menu,
            'delete_form' => $deleteForm->createView(),
            'aplicacion' => $aplicacion,
            'modulos' => $modulos,
        ));
    }

    /**
     * @Route("/aplicacion/new", name="aplicacion_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $aplicacion = new Aplicacion();
        $form = $this->createForm(AplicacionType::class, $aplicacion);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $em = $this->getEntityManager();

            $aplicacion->upload();

            $em->persist($aplicacion);
            $em->flush();

            $this->setFlash('success', sprintf('Se ha creado la aplicación, prosiga con la creación de los módulos.'));

            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nueva Aplicación");

        return $this->render('backend/Aplicacion/new.html.twig', array(
            'aplicacion' => $aplicacion,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/aplicacion/{id}/edit", name="aplicacion_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, Aplicacion $aplicacion, BreadcrumbManager $breadcrumbManager)
    {
        if (!$aplicacion) {
            throw $this->createNotFoundException('Código de Aplicación no encontrado.');
        }
        $form = $this->createForm(AplicacionType::class, $aplicacion);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $aplicacion->upload();

            $this->getEntityManager()->persist($aplicacion);
            $this->getEntityManager()->flush();

            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($aplicacion->getNombre())));

            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar Aplicación");

        return $this->render('backend/Aplicacion/edit.html.twig', array(
            'aplicacion' => $aplicacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/aplicacion/{id}/delete", name="aplicacion_delete",
     *     requirements={
     *         "id": "\d+"
     *     })
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(Request $request, Aplicacion $aplicacion)
    {
        $em = $this->getEntityManager();
        $em->remove($aplicacion);
        $em->flush();

        $this->setFlash('success', 'El aplicacion ha sido eliminada');

        return $this->redirect($this->generateUrl('aplicacion_list'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Setea el flash de mensajes
     * @param type $action  p/ej: succes, error, informations
     * @param type $value   cadena de tecto con el mensaje
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
