<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Modulo;
use App\Entity\Aplicacion;
use App\Form\backend\ModuloType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;

class ModuloController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Modulo');
    }

    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * @Route("/modulo/{id}/show", name="modulo_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     *  )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function showAction(Request $request, Modulo $modulo, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), $modulo->getNombre());
        $menu = array();
        $menu['Editar'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('modulo_edit', array(
                'id' => $modulo->getId(),
            ))
        );
        $menu['Eliminar'] = array(
            'imagen' => 'icon-minus icon-blue',
            'url' => '#modalDelete',
            'extra' => 'data-toggle=modal',
        );
        $menu['Agregar Permiso'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('permiso_new', array(
                'id' => $modulo->getId()
            ))
        );
        return $this->render('backend/Modulo/show.html.twig', array(
            'modulo' => $modulo,
            'menu' => $menu,
            'delete_form' => $this->createDeleteForm($modulo->getId())->createView(),
        ));
    }

    /**
     * @Route("/modulo/{id}/new", name="modulo_new",     
     *     methods={"GET", "POST"}
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, Aplicacion $aplicacion, BreadcrumbManager $breadcrumbManager)
    {
        // se recupera la Modulo
        $em = $this->getEntityManager();
        if (!$aplicacion) {
            throw $this->createNotFoundException('Código de Aplicación no encontrado.');
        }
        //se crea el objeto modulo con la Modulo asociada
        $modulo = new Modulo();
        $modulo->setAplicacion($aplicacion);

        //se crea el form
        $form = $this->createForm(ModuloType::class, $modulo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($modulo);
            //  die('////<pre>' . nl2br(var_export('asdasd', true)) . '</pre>////');
            $em->flush();
            $breadcrumbManager->pop();

            $this->setFlash('success', sprintf('Se ha creado el modulo "%s" en el sistema.', strtoupper($modulo)));
            return $this->redirect($breadcrumbManager->getVolver());
        }


        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Módulo");
        return $this->render('backend/Modulo/new.html.twig', array(
            'modulo' => $modulo,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/modulo/{id}/edit", name="modulo_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, Modulo $modulo, BreadcrumbManager $breadcrumbManager)
    {
        $em = $this->getEntityManager();
        if (!$modulo) {
            throw $this->createNotFoundException('Código de Módulo no encontrado.');
        }
        $form = $this->createForm(ModuloType::class, $modulo);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //se hace la relacion en forma manual, pero debe hacerse automaticamente.                
                $em->persist($modulo);
                $em->flush();

                $breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($modulo->getNombre())));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Alta permiso");
        return $this->render('backend/Modulo/edit.html.twig', array(
            'modulo' => $modulo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/modulo/{id}/delete", name="modulo_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(BreadcrumbManager $breadcrumb, $id = null, $return = null)
    {
        $em = $this->getEntityManager();

        $modulo = $em->find('App:Modulo', $id);
        $idApp = $modulo->getAplicacion()->getId();

        $em->remove($modulo);
        $em->flush();

        $this->setFlash('success', 'El módulo ha sido eliminado');

        return $this->redirect($breadcrumb->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
