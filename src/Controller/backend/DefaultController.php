<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DefaultController extends Controller
{

    public function panelcontrolAction()
    {
        return $this->container->render('backend/Home/panelcontrol.html.twig');
    }
}
