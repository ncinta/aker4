<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Permiso as Permiso;
use App\Entity\Modulo as Modulo;
use App\Form\backend\PermisoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\SensorManager;

class PermisoController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/permiso/{id}/new", name="permiso_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, Modulo $modulo, BreadcrumbManager $breadcrumbManager)
    {
        $permiso = new Permiso;
        $permiso->setModulo($modulo);
        $form = $this->createForm(PermisoType::class, $permiso);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($permiso);
            $em->flush();
            $breadcrumbManager->pop();
            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($modulo->getNombre())));
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Permiso");
        return $this->render('backend/Permiso/new.html.twig', array(
            'modulo' => $modulo,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/permiso/{id}/edit", name="permiso_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function editAction(Request $request, Permiso $permiso, BreadcrumbManager $breadcrumbManager)
    {
        $form = $this->createForm(PermisoType::class, $permiso);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($permiso);
            $em->flush();

            $breadcrumbManager->pop();
            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($permiso->getCodename())));
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar");
        return $this->render('backend/Permiso/edit.html.twig', array(
            'permiso' => $permiso,
            'form' => $form->createView()
        ));
    }

    /**
     * Elimina a un chip del sistema
     * @IsGranted("ROLE_PERMISO_ELIMINAR")
     */
    public function deleteAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, SensorManager $sensorManager)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $sensorManager->deleteById($id);
        }
        return $this->redirect($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
