<?php

namespace App\Controller\backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TipoCombustible;
use App\Form\backend\TipoCombustibleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\TipoCombustibleManager;

class TipoCombustibleController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\TipoCombustible');
    }

    private function getListMenu()
    {
        $menu = array();
        $menu['Agregar'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('tipocombustible_new')
        );
        return $menu;
    }

    /**
     * @Route("/tipocombustible/list", name="tipocombustible_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function listAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
        $tiposcombustible = $this->getRepository('App:TipoCombustible')->findBy(array(), array('nombre' => 'asc'));
        $breadcrumbManager->push($request->getRequestUri(), "Tipos de Combustible");
        return $this->render('backend/TipoCombustible/list.html.twig', array(
            'tipoCombustible' => $tiposcombustible,
            'menu' => $this->getListMenu(),
        ));
    }

    /**
     * @Route("/tipocombustible/new", name="tipocombustible_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function newAction(Request $request, BreadcrumbManager $breadcrumbManager)
    {
      
        $tipoCombustible = new TipoCombustible();
        
        $form = $this->createForm(TipoCombustibleType::class, $tipoCombustible);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          
            $data = $form->getData();
            $tipoCombustible->setNombre($data->getNombre());
            $this->getEntityManager()->persist($tipoCombustible);
            $this->getEntityManager()->flush();
            $breadcrumbManager->pop();

            $this->setFlash(
                'success',
                sprintf('Se ha creado el tipo de combustible <b>%s</b> en el sistema.', strtoupper($tipoCombustible->getNombre()))
            );
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Tipo de Evento");
        return $this->render('backend/TipoCombustible/new.html.twig', array(
            'tipocombustible' => $tipoCombustible,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/tipocombustible/{id}/delete", name="tipocombustible_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ROOT")
     */
    public function deleteAction(TipoCombustible $tipo)
    {
     
        $this->getEntityManager()->remove($tipo);
        $this->getEntityManager()->flush();
        $this->setFlash('success', 'Se ha eliminado el tipo de combustible');
        return $this->render('backend/TipoCombustible/list.html.twig', array(
            'tipoCombustible' => $this->getRepository('App:TipoCombustible')->findBy(array(), array('nombre' => 'asc')),
            'menu' => $this->getListMenu(),
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
