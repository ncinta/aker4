<?php

namespace App\Controller\fuel;

use App\Entity\CargaCombustible;
use App\Entity\Servicio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleCheckType;
use App\Form\fuel\AddCombustibleType;
use App\Entity\CheckCargaCombustible;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ChoferManager;
use App\Model\fuel\PuntoCargaManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\fuel\VerificaCombustibleManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\Router\VerificaCombustibleRouter;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CombustibleCheckController extends AbstractController
{

    private $session;
    private $breadcrumbManager;
    private $servicioManager;
    private $backendManager;
    private $userLogin;
    private $utilsManager;
    private $veriFuelManager;
    private $veriRouter;
    private $puntoCargaManager;
    private $choferManager;
    private $combustibleManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        UserLoginManager $userLogin,
        VerificaCombustibleManager $veriFuel,
        VerificaCombustibleRouter $veriRouter,
        PuntoCargaManager $puntoCarga,
        ChoferManager $choferManager,
        CombustibleManager $combustibleManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->backendManager = $backendManager;
        $this->utilsManager = $utilsManager;
        $this->userLogin = $userLogin;
        $this->veriFuelManager = $veriFuel;
        $this->veriRouter = $veriRouter;
        $this->puntoCargaManager = $puntoCarga;
        $this->choferManager = $choferManager;
        $this->combustibleManager = $combustibleManager;
    }

    /**
     * @Route("/fuel/check/{id}/index", name="fuel_check_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * 
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Verificador de Cargas");

        //creo y paso el form de Upload
        $formUpload = $this->createForm(CombustibleCheckType::class);
        //creo y paso el form de Add            
        $formAdd = $this->createForm(AddCombustibleType::class, null, array(
            'estaciones' => $this->puntoCargaManager->findAll($organizacion),
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager()
        ));

        $data = $this->loadCargas($organizacion, 0);
        //die('////<pre>' . nl2br(var_export('importado', true)) . '</pre>////');

        return $this->render('fuel/Check/index.html.twig', array(
            'menu' => $this->getMenu($organizacion),
            'data' => null, //$data,
            'organizacion' => $organizacion,
            'formAdd' => $formAdd->createView(),
            'formUpload' => $formUpload->createView(),
            'servicios' => $this->servicioManager->findAllByOrganizacion($organizacion),
            'checkFiltro' => false,
        ));
    }

    /**
     * @Route("/fuel/check/{id}/buscarcarga", name="fuel_check_buscarcarga",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET"},
     *     options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function buscarCargaAction(Request $request, CheckCargaCombustible $carga)
    {
        //dd($carga);
        $fecha = $carga->getFecha()->format('Y-m-d');

        $desde = new \DateTime($fecha . ' 00:00:00');
        $hasta = new \DateTime($fecha . ' 23:59:59');

        $cargas = $this->combustibleManager->findByServicioYfecha($carga->getServicio(), $desde, $hasta);
        //dd(count($cargas));

        $data = $this->renderView('fuel/Check/tablaBuscarCargas.html.twig', array('cargas' => $cargas));

        return new Response(json_encode($data));
    }

    /**
     * @Route("/fuel/check/{carga}/{combustible}/setcarga", name="fuel_check_setcarga", 
     *  requirements={
     *         "carga": "\d+",
     *         "combustible": "\d+", 
     *  },
     *     methods={"POST"},
     *     options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function setCargaAction(Request $request, CheckCargaCombustible $carga, CargaCombustible $combustible)
    {

        $combustible->setEstado(9); //chequeada
        if ($combustible->getData()) {
            $newData = json_encode(array_merge(json_decode($combustible->getData(), true), array('check' => $carga->getDataArchivo())));
        } else {
            $newData = json_encode(array('check' => $carga->getDataArchivo()));
        }
        $combustible->setData($newData);
        $em = $this->getDoctrine()->getManager();
        $em->persist($combustible);   //grabo combustible       
        $carga->setEstado(9); //check
        $carga->setCargaCombustible($combustible);
        $em->persist($carga);
        $em->flush();

        return new Response(json_encode(array('status' => 'ok')));
    }



    /**
     * @Route("/fuel/check/{id}/upload", name="fuel_check_upload",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET","POST"},
     *     options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function uploadAction(Request $request, Organizacion $organizacion)
    {
        $formUpload = $this->createForm(CombustibleCheckType::class);
        $formUpload->handleRequest($request);
        //dd($formUpload->getData());
        if ($formUpload->isSubmitted() && $formUpload->isValid()) {
            $newFile = sprintf("%s_%s.xls", $this->create_url_slug($organizacion->getNombre()), date('YmdHis'));
            $upload = $this->getParameter('kernel.project_dir');
            // die('////<pre>' . nl2br(var_export($upload, true)) . '</pre>////');
            $uploadDir = $upload . "/public/uploads/checkfuel/";
            if ($formUpload['archivo']->getData() === null) {
                $this->setFlash('error', 'No se pudo procesar el archivo');
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
            $formUpload['archivo']->getData()->move($uploadDir, $newFile);
            if (file_exists($uploadDir . '/' . $newFile)) {  //el archivo se subio correctamente.
                $this->session = rand(12345, 65535 * 888);
                $data = $this->procesarPropio($uploadDir . '/' . $newFile, $organizacion);
            }
            return new Response(json_encode('ok'), 200);
        } else {
            $errors = $formUpload->getErrors();
            foreach ($errors as $error) {
                dd($error->getMessage());
            }
        }
        $data = ['mensaje' => 'Formulario invalido'];
        return new Response(json_encode($data), 500);
    }

    private function getMenu($organizacion)
    {
        $menu = array(
            1 => array(
                $this->veriRouter->btnUploadCargas($organizacion),
            ),
            2 => array(
                $this->veriRouter->btnDelete(),
            ),
            'html' => $this->renderView(
                'app/Template_aker/navbar_select.html.twig',
                array('options' => CheckCargaCombustible::ESTADO_CARGA, 'id' => 'select_estado')
            )
        );

        return $menu;
    }

    /**
     * @Route("/fuel/check/{id}/{estado}/reload", name="fuel_check_reload",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET"},
     *     options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function reloadAction(Request $request, Organizacion $organizacion, $estado)
    {
        $data = array();

        //traigo los datos
        $cargas = $this->loadCargas($organizacion, intval($estado));

        foreach ($cargas as $row) {
            if ($row->getServicio() != null) {
                $servicio = $row->getServicio()->getNombre();
            } else {
                $servicio = '<button class="mb-xs mt-xs mr-xs btn btn-xs btn-default" onclick="addServicio(' . $row->getId() . ')">Asociar servicio</button>';
            }
            $dataArchivo = $row->getDataArchivo();

            $data[] = array(
                'ckeck' => $row->getId(),
                'estado' => $this->renderView('fuel/Check/columnaEstado.html.twig', array('estado' => $row->getEstado())),
                'servicio' => $this->renderView('fuel/Check/columnaServicio.html.twig', array('row' => $row)),
                'fecha' => !is_null($row->getFecha()) ? $row->getFecha()->format('d-m-Y H:i:s') : '',
                'patente' => $dataArchivo['patente'],
                'dataImport' => $this->renderView('fuel/Check/columnaDataImportacion.html.twig', array('row' => $row)),
                'dataCarga' => $this->renderView('fuel/Check/columnaDataCarga.html.twig', array('row' => $row)),
                'acciones' => $this->renderView('fuel/Check/columnaAcciones.html.twig', array('row' => $row)),
                'check' => $this->renderView('fuel/Check/columnaCheck.html.twig', array('row' => $row)),
            );
        }

        return new Response(json_encode(array('data' => $data)), 200);
    }

    /**
     * @Route("/fuel/check/deletecargas", name="fuel_check_deletecargas",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"POST"},
     *     options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function deletecargasAction(Request $request)
    {
        parse_str($request->get('formCheck'), $tmp);
        $data = $tmp['formCheck'];

        foreach ($data as $key => $value) {
            $delete = $this->veriFuelManager->deleteById($key);
        }


        return new Response(json_encode(array('status' => 'ok')));
    }

    private function create_url_slug($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }

    private function procesarPropio($file, $organizacion)
    {
        if (!file_exists($file)) {
            exit("No existe el archivo -> " . $file);
        }
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $objPHPExcel = $reader->load($file);

        $data = null;
        $cels = array(
            'patente' => 'H',
            'fecha' => 'A',
            'hora' => 'Z',
            'litros' => 'L',
            'importe' => 'M',
            'guia' => 'J',
            'odometro' => 'I',
            'estacion' => 'C',
            'chofer' => 'F',
            'choferDni' => 'G',
            'producto' => 'K',
            'contrato' => 'B',
            'localidad' => 'D',
            'tarjeta' => 'E',
        );


        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    //patente C
                    $cellPatente = $this->limpiarPatente($worksheet->getCell($cels['patente'] . $i)->getCalculatedValue());

                    //listros E
                    $cellLitros = $this->limpiarLitros($worksheet->getCell($cels['litros'] . $i)->getCalculatedValue());

                    if (!is_null($cellPatente) && !is_null($cellLitros)) {

                        //formateo de la fecha
                        $dataA = $worksheet->getCell($cels['fecha'] . $i)->getCalculatedValue();
                        if(strlen($dataA) == 18){ // (PARCHE) si la hora es menor a 9 no coloca el 0 adelante y falla al crear la fecha.
                            $dataA = substr($dataA,0,11).'0'.substr($dataA,11);
                        }
                        $dataB = $worksheet->getCell($cels['hora'] . $i)->getCalculatedValue();
                       
                        $f = trim($dataA . ' ' . $dataB);
                        
                        if ($worksheet->getCell($cels['fecha'] . $i)->getDataType() == 'n') {
                            $fecha = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($f);
                        } else {
                            $fecha = date_create_from_format($this->extractDateTimeFormat($f), $f);
                        }
                        
                        //   dd( $fecha );
                        if ($fecha) {
                            //$fecha->setTimezone(new \DateTimeZone($organizacion->getTimezone()));
                        } else {
                            $fecha = null;
                        }
                        //dd($fecha);
                        $estacion = $worksheet->getCell($cels['estacion'] . $i)->getCalculatedValue();
                        $codigo = trim(substr($estacion, 0, strpos($estacion, "-") - 1));
                        $data[] = array(
                            'row' => $i,
                            'fecha' => $fecha,
                            'patente' => $cellPatente,
                            'litros' => $cellLitros,
                            'monto' => $worksheet->getCell($cels['importe'] . $i)->getCalculatedValue(),
                            'estacion' => $estacion,
                            'codigo' => $codigo,
                            'estacion' => $worksheet->getCell($cels['estacion'] . $i)->getCalculatedValue(),
                            'guia' => $worksheet->getCell($cels['guia'] . $i)->getCalculatedValue(),
                            'odometro' => $worksheet->getCell($cels['odometro'] . $i)->getCalculatedValue(),
                            'chofer' => $worksheet->getCell($cels['chofer'] . $i)->getCalculatedValue(),
                            'choferDni' => $worksheet->getCell($cels['choferDni'] . $i)->getCalculatedValue(),
                            'producto' => $worksheet->getCell($cels['producto'] . $i)->getCalculatedValue(),
                            'contrato' => $worksheet->getCell($cels['contrato'] . $i)->getCalculatedValue(),
                            'localidad' => $worksheet->getCell($cels['localidad'] . $i)->getCalculatedValue(),
                        );
                    }
                }
            }
            // dd($data);
        }
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.

        return $this->persistirDatosCarga($data, $organizacion);
    }

    private function loadCargas($organizacion, $estado = null)
    {
        $cargas = $this->veriFuelManager->findByOrganizacion($organizacion, $estado); //busco las nuevas
        return $cargas;
    }

    private function persistirDatosCarga($data, $organizacion)
    {
        $importado = null;
        if (!is_null($data)) {
            $em = $this->getDoctrine()->getManager();
            //  dd($data);
            //recorro la data y la cargo en el sistema en la tabla de cargas
            foreach ($data as $row) {
                //si no existe que grabe
                if (!$this->getRepositoryCheck()->findOneBy(array(
                    'fecha' => $row['fecha'],
                    'litrosCarga' => $row['litros'],
                    'guia_despacho' => $row['guia'],
                    'monto_total' => $row['monto']
                ))) {
                    $carga = new CheckCargaCombustible();
                    if (!is_null($row['fecha'])) {
                        $carga->setFecha($row['fecha']);
                    }
                    $carga->setModoIngreso(3); //modo importado.
                    $carga->setEstado(0); //nueva.
                    $carga->setMontoTotal($row['monto']);
                    $carga->setLitrosCarga($row['litros']);
                    $carga->setSession($this->session);
                    $carga->setOdometroTablero($row['odometro']);
                    $carga->setGuiaDespacho($row['guia']);
                    $carga->setLocalidad($row['localidad']);
                    $carga->setContrato($row['contrato']);
                    $carga->setDataArchivo($row);

                    $carga->setOrganizacion($organizacion);
                    $carga->setUsuario($this->userLogin->getUser());

                    //buscar y asociar servicio
                    $servicio = $this->servicioManager->findByPatente($row['patente']);
                    if ($servicio && $servicio->getOrganizacion() == $organizacion) {
                        $carga->setServicio($servicio);
                    }

                    //buscar y asociar punto de carga              
                    $ptoCarga = $this->puntoCargaManager->findByCodigo($row['codigo']);
                    //dd($ptoCarga);
                    if ($ptoCarga) {
                        $carga->setPuntoCarga($ptoCarga);
                    }

                    if ($row['choferDni']) {

                        $chofer = $this->choferManager->findByDocumentoOrg(trim($row['choferDni']), $organizacion);
                        //dd($chofer);
                        if ($chofer) {
                            $carga->setChofer($chofer);
                        }
                    }

                    //buscar por producto
                    if ($row['producto']) {
                        $producto = $this->combustibleManager->findTipoCombustibleByNombre($row['producto']);
                        if ($producto) {
                            $carga->setTipoCombustible($producto);
                        }
                    }


                    //asociar carga segun fecha
                    //dd($row['fecha']);
                    $cargaSis = $em->getRepository('App:CargaCombustible')->findOneBy(array(
                        'fecha' => $row['fecha'],
                        'servicio' => $servicio,
                    ));
                    // dd($cargaSis);
                    if ($cargaSis) {
                        $carga->setCargaCombustible($cargaSis);
                    }
                    //buscar y asociar pto de carga

                    //buscar y asociar chofer

                    $em->persist($carga);
                    $em->flush();
                    $importado[] = $carga;
                }
            }
        }
        return $importado;
    }

    private function getRepositoryTipoCombustible()
    {
        return $this->getEntityManager()->getRepository('App\Entity\TipoCombustible');
    }
    private function getRepositoryCheck()
    {
        return $this->getEntityManager()->getRepository('App\Entity\CheckCargaCombustible');
    }
    //die('////<pre>' . nl2br(var_export($importado, true)) . '</pre>////');

    /**     
     * @Route("/fuel/check/{id}/dataimport", name="fuel_check_dataimport", 
     *  requirements={
     *         "id": "\d+"
     *  },
     *  methods={"GET"},
     *  options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function dataimportAction(Request $request, CheckCargaCombustible $carga)
    {
        //dd($carga);
        $data = [
            'id' => $carga->getId(),
            'fecha' => $carga->getFecha()->format('d-m-Y H:i:s'),
            'litrosCarga' => $carga->getLitrosCarga(),
            'montoTotal' => $carga->getMontoTotal(),
            'guiaDespacho' => $carga->getGuiaDespacho(),
            'puntoCarga' =>  $carga->getPuntoCarga() ? (string) $carga->getPuntoCarga()->getId() : "0",
            'chofer' => (string) $carga->getChofer() ? (string) $carga->getChofer()->getId() : "0",
            'tipoCombustible' => $carga->getTipoCombustible() ? (string) $carga->getTipoCombustible()->getId() : "0",
        ];

        return new Response(json_encode($data));
    }

    /**     
     * @Route("/fuel/check/{carga}/{servicio}/agregarcarga", name="fuel_check_agregarcarga", 
     *  requirements={
     *         "carga": "\d+",
     *         "servicio": "\d+", 
     *  },
     *  methods={"POST"},
     *  options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function agregarcargaAction(Request $request, CheckCargaCombustible $carga, Servicio $servicio)
    {
        parse_str($request->get('data'), $tmp);
        $data = $tmp['addfuel'];


        //si el check ya tiene carga entonces se edita con los datos nuevos
        if (is_null($carga->getCargaCombustible())) {
            $combustible = new CargaCombustible();
        } else {
            $combustible = $carga->getCargaCombustible();
        }
        $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($data['fecha']));
        $combustible->setFecha($fecha);
        $combustible->setServicio($servicio);
        $combustible->setCargaCompleta($data['cargaCompleta'] == "1");
        $combustible->setLitrosCarga((int)$data['litros_carga']);
        $combustible->setMontoTotal((float) (str_replace(',', '.', $data['monto_total'])));
        $combustible->setOdometro((float) ($data['odometroTablero']));
        $combustible->setOdometroTablero((int) ($data['odometroTablero']));
        $combustible->setGuiaDespacho(($data['guia_despacho']));
        $trama = $this->backendManager->obtenerTramaAnterior($combustible->getServicio()->getId(), $combustible->getFecha());
        if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
            $combustible->setOdometro((float) ($trama['odometro'] / 1000));
        }
        if (isset($trama['oid'])) {
            $combustible->setId_trama($trama['oid']);
        }

        //tipo combustible
        if (isset($data['tipoCombustible'])) {
            $tipoCombustible = $this->combustibleManager->findTipoCombustible(intval($data['tipoCombustible']));
            $combustible->setTipoCombustible($tipoCombustible);
        }

        //punto de carga       
        if (isset($data['puntoCarga'])) {
            $referencia = $this->puntoCargaManager->findById(intval($data['puntoCarga']));
            $combustible->setPuntoCarga($referencia);
        }

        //chofer
        if (isset($data['chofer'])) {
            $chofer = $this->choferManager->findById(intval($data['chofer']));
            $combustible->setChofer($chofer);
        }

        //tipo de carga
        $combustible->setModoIngreso(9); //modo por chequer.
        $combustible->setEstado(9); //chequeada

        if ($combustible->getData()) {
            $newData = json_encode(array_merge(json_decode($combustible->getData(), true), array('check' => $carga->getDataArchivo())));
        } else {
            $newData = json_encode(array('check' => $carga->getDataArchivo()));
        }
        $combustible->setData($newData);
        $em = $this->getDoctrine()->getManager();
        $em->persist($combustible);   //grabo combustible       

        $carga->setEstado(9); //check
        $carga->setCargaCombustible($combustible);
        $em->persist($carga);
        $em->flush();


        return new Response(json_encode($data));
    }

    /**     
     * @Route("/fuel/check/{id}/asociarservicio", name="fuel_check_asociarservicio", 
     *  requirements={
     *         "id": "\d+",
     *  },
     *  methods={"POST"},
     *  options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function asociarservicioAction(Request $request, CheckCargaCombustible $carga)
    {
        $id = $request->get('servicioId');
        $servicio = $this->servicioManager->findById(intval($id));
        //dd($servicio->getNombre());
        $carga->setServicio($servicio);
        $em = $this->getDoctrine()->getManager();
        $em->persist($carga);
        $em->flush();


        return new Response(json_encode('ok'));
    }
    /**     
     * @Route("/fuel/check/{carga}/changestatus", name="fuel_check_changestatus", 
     *  requirements={
     *         "carga": "\d+"
     *  },
     *  methods={"POST"},
     *  options={"expose": true}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function changestatusAction(Request $request, CheckCargaCombustible $carga)
    {
        $status = $request->get('status');
        $carga->setEstado($status);
        $em = $this->getDoctrine()->getManager();
        $em->persist($carga);
        $em->flush();
        return new Response(json_encode('ok'));
    }

    function extractDateTimeFormat($string)
    {
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $string))
            return 'Y-m-d';

        if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $string))
            return 'Y-m-d H:i:s';

        //este
        if (preg_match('/^\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2}$/', $string))
            return 'd/m/Y H:i:s';

        if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}$/', $string))
            return 'Y-m-d H:i:s.v';

        if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $string))
            return 'm/d/Y';

        if (preg_match('/^\d{2}\.\d{2}\.\d{4}$/', $string))
            return 'd.m.Y';
    }

    private function limpiarPatente($patente)
    {
        $patente = str_replace(' ', '', $patente);
        $patente = str_replace('-', '', $patente);
        return strtoupper($patente);
    }

    private function limpiarLitros($lts)
    {
        $l = str_replace(',', '.', $lts);
        return (float)$l;
    }



    /**
     * @Route("/fuel/check/initstep2", name="fuel_check_initstep2")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function initstep2Action(Request $request)
    {
        $session = $request->get('sessionid');
        $em = $this->getDoctrine()->getManager();
        $cargas = $em->getRepository('App:CargaCombustible')->findBy(array('session' => $session));
        $ids = array();
        foreach ($cargas as $value) {
            $ids[] = $value->getId();
        }
        return new Response(json_encode($ids, false));
    }

    /**
     * @Route("/fuel/check/calcodometro", name="fuel_check_calcodometro")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function calcodometroAction(Request $request)
    {
        $odometro = null;
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $carga = $em->getRepository('App:CargaCombustible')->find($id);
        if ($carga != null) {
            //obtengo el odometro del servicio
            $trama = $this->backendManager->obtenerTramaAnterior($carga->getServicio()->getId(), $carga->getFecha());
            // die('////<pre>' . nl2br(var_export($trama['fecha'], true)) . '</pre>////');
            if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                $odometro = (int) ($trama['odometro'] / 1000);
            }
            if (isset($trama['oid'])) {
                $carga->setId_trama($trama['oid']);
            }

            if (isset($trama['fecha']) && isset($trama['fecha'])) {
                $carga->setFechaTrama(new \DateTime($trama['fecha']));
            }

            if (isset($trama['posicion'])) {
                $posicion = $trama['posicion'];
                if (isset($posicion['latitud']) && !is_null($posicion['latitud'])) {
                    $carga->setLatitud($posicion['latitud']);
                }
                if (isset($posicion['longitud']) && !is_null($posicion['longitud'])) {
                    $carga->setLongitud($posicion['longitud']);
                }
            }
            unset($trama);
            if ($odometro != null) {
                $carga->setOdometro($odometro);
                $em->persist($carga);
                $em->flush();
            }
        }
        return new Response(json_encode($odometro, false));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
