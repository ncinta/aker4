<?php

namespace App\Controller\fuel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CargaCombustible;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleImportType;
use App\Entity\ImportCombustible;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class CombustibleImportController extends AbstractController
{

    private $session;
    private $breadcrumbManager;
    private $servicioManager;
    private $referenciaManager;
    private $backendManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        BackendManager $backendManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->backendManager = $backendManager;
    }

    /**
     * @Route("/fuel/combimport/{id}/index", name="fuel_import_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Importación de Cargas");
        $form = $this->createForm(CombustibleImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newFile = sprintf("%s_%s_%s.xls", $this->create_url_slug($organizacion->getNombre()), date('YmdHis'), $form['tipo']->getData());
            $upload = $this->getParameter('kernel.project_dir');
            $uploadDir = $upload . "/web/uploads/import/";
            if ($form['archivo']->getData() === null) {
                $this->setFlash('error', 'No se pudo procesar el archivo');
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
            $form['archivo']->getData()->move($uploadDir, $newFile);
            if (file_exists($uploadDir . '/' . $newFile)) {  //el archivo se subio correctamente.
                $this->session = rand(12345, 65535 * 888);
                if ($form['tipo']->getData() == 0) {  //es un archivo shell
                    $data = $this->procesarShell($uploadDir . '/' . $newFile, $organizacion);
                } elseif ($form['tipo']->getData() == 1) { //es un archivo copec
                    $data = $this->procesarCopec($uploadDir . '/' . $newFile, $organizacion);
                } elseif ($form['tipo']->getData() == 2) { //es un archivo propio combustible
                    $data = $this->procesarPropio($uploadDir . '/' . $newFile, $organizacion);
                }
            }
            return $this->render('fuel/Combustible/importado.html.twig', array(
                'data' => $data,
                'session' => $this->session,
                'filename' => $form['archivo']->getData()->getClientOriginalName(),
                'organizacion' => $organizacion,
            ));
        }
        return $this->render('fuel/Combustible/importacion.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView(),
        ));
    }

    private function create_url_slug($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }

    private function procesarShell($file, $organizacion)
    {
        if (!file_exists($file)) {
            exit("No existe el archivo -> " . $file);
        }
        $objPHPExcel = IOFactory::load($file);
        $data = null;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 3) {  //avanzo hasta los datos
                    //die('////<pre>' . nl2br(var_export($i, true)) . '</pre>////');
                    $cellPatente = $worksheet->getCell('D' . $i);
                    $cellLitros = $worksheet->getCell('I' . $i);
                    if (!is_null($cellPatente) && !is_null($cellLitros)) {
                        $c = $worksheet->getCell('H' . $i);
                        $fecha = gmdate('Y-m-d H:i:s', Date::excelToTimestamp($c));
                        $data[] = array(
                            'patente' => $cellPatente->getCalculatedValue(),
                            'litros' => $cellLitros->getCalculatedValue(),
                            'guia' => $worksheet->getCell('E' . $i)->getCalculatedValue(),
                            'fecha' => $fecha,
                            'odometro' => $worksheet->getCell('K' . $i)->getCalculatedValue(),
                            'monto' => $worksheet->getCell('N' . $i)->getCalculatedValue(),
                        );
                    }
                }
            }
        }
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.
        return $this->persistirDatosCarga($data, $organizacion);
    }

    private function procesarCopec($file, $organizacion)
    {
        if (!file_exists($file)) {
            exit("No existe el archivo -> " . $file);
        }
        $objPHPExcel = IOFactory::load($file);
        $data = null;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellPatente = $worksheet->getCell('B' . $i);
                    $cellLitros = $worksheet->getCell('J' . $i);
                    if (!is_null($cellPatente) && !is_null($cellLitros)) {
                        $c = $worksheet->getCell('E' . $i)->getCalculatedValue() + $worksheet->getCell('F' . $i)->getCalculatedValue();
                        $fecha = gmdate('Y-m-d H:i:s', Date::excelToTimestamp($c));

                        $data[] = array(
                            'patente' => $cellPatente->getCalculatedValue(),
                            'litros' => $cellLitros->getCalculatedValue(),
                            'guia' => $worksheet->getCell('H' . $i)->getCalculatedValue(),
                            'fecha' => $fecha,
                            'odometro' => $worksheet->getCell('L' . $i)->getCalculatedValue(),
                            'monto' => $worksheet->getCell('K' . $i)->getCalculatedValue(),
                        );
                    }
                }
            }
        }
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.
        return $this->persistirDatosCarga($data, $organizacion);
    }

    private function procesarPropio($file, $organizacion)
    {
        if (!file_exists($file)) {
            exit("No existe el archivo -> " . $file);
        }
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $objPHPExcel = $reader->load($file);

        $data = null;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellPatente = $worksheet->getCell('C' . $i)->getCalculatedValue();
                    $cellLitros = $worksheet->getCell('D' . $i)->getCalculatedValue();
                    if (!is_null($cellPatente) && !is_null($cellLitros)) {
                        $c = $worksheet->getCell('A' . $i)->getCalculatedValue() + $worksheet->getCell('B' . $i)->getCalculatedValue();
                        // dd($c);
                        $fecha = gmdate('Y-m-d H:i:s', Date::excelToTimestamp($c));

                        $data[] = array(
                            'patente' => preg_replace('/\s+/', '', $cellPatente), //elimina todos los espacios en blanco de la patente
                            'litros' => $cellLitros,
                            'guia' => $worksheet->getCell('G' . $i)->getCalculatedValue(),
                            'fecha' => $fecha,
                            'odometro' => $worksheet->getCell('E' . $i)->getCalculatedValue(),
                            'monto' => $worksheet->getCell('F' . $i)->getCalculatedValue(),
                            'estacion' => $worksheet->getCell('H' . $i)->getCalculatedValue()
                        );
                    }
                }
            }
        }
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.       
        return $this->persistirDatosCarga($data, $organizacion);
    }

    private function persistirDatosCarga($data, $organizacion)
    {
        $importado = null;
        if (!is_null($data)) {

            $em = $this->getDoctrine()->getManager();
            //recorro la data y la cargo en el sistema.
            foreach ($data as $row) {
                //busco el servicio.
                $patente = str_replace('-', '', $row['patente']);
                $servicio = $this->servicioManager->findByPatente($patente);

                if ($servicio && $servicio->getOrganizacion() == $organizacion) {

                    //busco una carga igual existente.
                    $cargTmp = $em->getRepository('App:CargaCombustible')->findOneBy(array(
                        'servicio' => $servicio->getId(),
                        'fecha' => new \DateTime($row['fecha']),
                        'litrosCarga' => $row['litros'],
                        'guia_despacho' => $row['guia']
                    ));

                    if ($cargTmp === null) {
                        $carga = new CargaCombustible();
                        $carga->setServicio($servicio);
                    } else {
                        $carga = $cargTmp;
                        //  die('////<pre>' . nl2br(var_export($carga, true)) . '</pre>////');
                    }
                    $estacion = $this->referenciaManager->findCodigoExterno(trim($row['estacion']));
                    if ($estacion) {
                        
                        $carga->setReferencia($estacion);
                        //asocio el punto de carga en caso de tenerlo.
                        if ($estacion->getPuntoCarga() != null) {
                            //dd($estacion->getPuntoCarga());
                            foreach ($estacion->getPuntoCarga() as $pto){
                                $carga->setPuntoCarga($pto);
                                continue;
                            }
                            
                        }    
                    }
                    
                    $carga->setFecha(new \DateTime($row['fecha']));
                    $carga->setModoIngreso(3); //modo importado.
                    $carga->setEstado(0); //nueva.
                    $carga->setMontoTotal($row['monto']);
                    $carga->setLitrosCarga($row['litros']);
                    $carga->setSession($this->session);
                    $carga->setOdometro($row['odometro']);
                    $carga->setGuiaDespacho($row['guia']);

                    $em->persist($carga);
                    $em->flush();
                    $importado[] = $carga;
                }
                unset($servicio);
            }
        }
        return $importado;
    }

    //die('////<pre>' . nl2br(var_export($importado, true)) . '</pre>////');


    /**
     * Para una sessionid retorna todos los id de las cargas y los devuelve en un json.
     * @return Response json ids
     */

    /**
     * @Route("/fuel/combimport/initstep2", name="fuel_import_initstep2")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function initstep2Action(Request $request)
    {
        $session = $request->get('sessionid');
        $em = $this->getDoctrine()->getManager();
        $cargas = $em->getRepository('App:CargaCombustible')->findBy(array('session' => $session));
        $ids = array();
        foreach ($cargas as $value) {
            $ids[] = $value->getId();
        }
        return new Response(json_encode($ids, false));
    }

    /**
     * @Route("/fuel/combimport/calcodometro", name="fuel_import_calcodometro")
     * @Method({"GET", "POST"}) 
     * @IsGranted("ROLE_COMBUSTIBLE_IMPORTACION")
     */
    public function calcodometroAction(Request $request)
    {
        $odometro = null;
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $carga = $em->getRepository('App:CargaCombustible')->find($id);
        if ($carga != null) {
            //obtengo el odometro del servicio
            $trama = $this->backendManager->obtenerTramaAnterior($carga->getServicio()->getId(), $carga->getFecha());
            // die('////<pre>' . nl2br(var_export($trama['fecha'], true)) . '</pre>////');
            if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                $odometro = (int) ($trama['odometro'] / 1000);
            }
            if (isset($trama['oid'])) {
                $carga->setId_trama($trama['oid']);
            }

            if (isset($trama['fecha']) && isset($trama['fecha'])) {
                $carga->setFechaTrama(new \DateTime($trama['fecha']));
            }

            if (isset($trama['posicion'])) {
                $posicion = $trama['posicion'];
                if (isset($posicion['latitud']) && !is_null($posicion['latitud'])) {
                    $carga->setLatitud($posicion['latitud']);
                }
                if (isset($posicion['longitud']) && !is_null($posicion['longitud'])) {
                    $carga->setLongitud($posicion['longitud']);
                }
            }
            unset($trama);
            if ($odometro != null) {
                $carga->setOdometro($odometro);
                $em->persist($carga);
                $em->flush();
            }
        }
        return new Response(json_encode($odometro, false));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
