<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CargaCombustible;
use App\Entity\Organizacion;
use App\Entity\Servicio;
use App\Form\fuel\CombustibleType;
use App\Form\fuel\FechaHistorialCombustibleType;
//libreria de graficos.
use SaadTazi\GChartBundle\DataTable;
use App\Form\fuel\CombustibleMasivaType;
use App\Form\fuel\CombustibleEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UsuarioManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\ChoferManager;
use App\Model\fuel\CombustibleManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\Router\CombustibleRouter;
use App\Model\app\Router\PuntoCargaRouter;
use App\Model\app\GeocoderManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\PuntoCargaManager;

class CombustibleController extends AbstractController
{

    private $breadcrumbManager;
    private $servicioManager;
    private $combustibleRouter;
    private $puntocargaRouter;
    private $utilsManager;
    private $combustibleManager;
    private $referenciaManager;
    private $backendManager;
    private $usuarioManager;
    private $excelManager;
    private $tanqueManager;
    private $grupoServicioManager;
    private $choferManager;
    private $geocoderManager;
    private $userloginManager;
    private $puntoCargaManager;

    public function __construct(
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        UsuarioManager $usuarioManager,
        CombustibleRouter $combustibleRouter,
        PuntoCargaRouter $puntocargaRouter,
        UtilsManager $utilsManager,
        CombustibleManager $combustibleManager,
        ExcelManager $excelManager,
        ReferenciaManager $referenciaManager,
        BackendManager $backendManager,
        TanqueManager $tanqueManager,
        GrupoServicioManager $grupoServicioManager,
        ChoferManager $choferManager,
        GeocoderManager $geocoderManager,
        UserLoginManager $userloginManager,
        PuntoCargaManager $puntoCargaManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->combustibleRouter = $combustibleRouter;
        $this->puntocargaRouter = $puntocargaRouter;
        $this->utilsManager = $utilsManager;
        $this->combustibleManager = $combustibleManager;
        $this->referenciaManager = $referenciaManager;
        $this->backendManager = $backendManager;
        $this->usuarioManager = $usuarioManager;
        $this->excelManager = $excelManager;
        $this->tanqueManager = $tanqueManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->choferManager = $choferManager;
        $this->geocoderManager = $geocoderManager;
        $this->userloginManager = $userloginManager;
        $this->puntoCargaManager = $puntoCargaManager;
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/index", name="fuel_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Cargas");

        $servicios = $this->servicioManager->findEnabledByUsuario();
        //die('////<pre>'.nl2br(var_export($menu, true)).'</pre>////');
        $menu = array(
            1 => [
                $this->combustibleRouter->btnMasiva($organizacion),
                $this->combustibleRouter->btnImportar($organizacion),
                $this->combustibleRouter->btnChecker($organizacion),
            ],
            2 => [
                $this->combustibleRouter->btnInformeRendimientoCargas($organizacion),
                $this->combustibleRouter->btnInformeRendimiento($organizacion),
                $this->combustibleRouter->btnInformeCarga($organizacion),
                $this->combustibleRouter->btnInformeDespacho($organizacion),
                $this->combustibleRouter->btnGraficoConsumo($organizacion),
            ],
            3 => [
                $this->puntocargaRouter->btnList($organizacion),
            ]
        );
        $data = $this->combustibleManager->generarDashboard($servicios);
        //die('////<pre>' . nl2br(var_export($menu, true)) . '</pre>////');
        return $this->render('fuel/Combustible/index.html.twig', array(
            //'menu' => $this->combustibleRouter->toCalypso($menu),
            'menu' => $menu,
            'servicios' => $servicios,
            'data' => $data,
        ));
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/list", name="fuel_list",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function listAction(Request $request, Servicio $servicio)
    {
        $hasta = null;
        $desde = null;
        $ok = true;
        if ($request->getMethod() == 'POST') {
            $form = $request->get('historial');
            $ok = $this->utilsManager->isValidDesdeHasta('Y/m/d', $form['desde'], $form['hasta']);
            if ($ok) {    //el formato esta correcto
                $desde = $form['desde'];
                $hasta = $form['hasta'];
            }
        }
        if ($ok) {
            $cargas = $this->combustibleManager->findAllCargas($servicio, $desde, $hasta);
        } else {
            // muestro el mensaje de error porque las fechas estan mal.
            $this->setFlash('error', 'Las fechas no son válidas. Verifique.');
            $cargas = null;
        }
        $form = $this->createForm(FechaHistorialCombustibleType::class);
        $filtro = array('desde' => $desde, 'hasta' => $hasta);
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Historial");

        return $this->render('fuel/Combustible/list.html.twig', array(
            'menu' => $this->getListMenu($servicio, $desde, $hasta),
            'form' => $form->createView(),
            'servicio' => $servicio,
            'desde' => $desde,
            'hasta' => $hasta,
            'cargas' => $cargas
        ));
    }

    /**
     * filtra todas las cargas en un periodo de tiempo.
     * @Route("/fuel/{id}/filtrar", name="fuel_filtrar",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function filtrarAction(Request $request, Servicio $servicio)
    {
        $filtro = $request->get('historial');
        $desde = $this->utilsManager->datetime2sqltimestamp($filtro['desde'], false);        
        $hasta = $this->utilsManager->datetime2sqltimestamp($filtro['hasta'], true);
        $cargas = $this->combustibleManager->findAllCargas($servicio, $desde, $hasta);
        $form = $this->createForm(FechaHistorialCombustibleType::class);

        return $this->render('fuel/Combustible/list.html.twig', array(
            'menu' => $this->getListMenu($servicio, $filtro['desde'], $filtro['hasta']),
            'form' => $form->createView(),
            'servicio' => $servicio,
            'desde' => $desde,
            'hasta' => $hasta,
            'cargas' => $cargas
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menund
     */
    private function getListMenu($servicio, $desde, $hasta)
    {
        $menu = array(
            1 => array(
                $this->combustibleRouter->btnNewCarga($servicio),
                $this->combustibleRouter->btnExportar($servicio, $desde, $hasta)
            )
        );
        return $menu;
        //return $this->combustibleRouter->toCalypso($menu);
    }

    /**
     * @Route("/fuel/{id}/show", name="fuel_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function showAction(Request $request, CargaCombustible $carga)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), 'Carga');
        $deleteForm = $this->createDeleteForm($carga->getId());
        return $this->render('fuel/Combustible/show.html.twig', array(
            'carga' => $carga,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/fuel/{id}/new", name="fuel_new",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_AGREGAR")
     */
    public function newAction(Request $request, Servicio $servicio)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nueva Carga");

        $combustible = new CargaCombustible();
        $combustible->setServicio($servicio);
        $options['estaciones'] = $this->referenciaManager->findEstacionesMapa();
        $options['puntoscarga'] = $this->puntoCargaManager->findAll($servicio->getOrganizacion());
        $options['organizacion'] = $this->userloginManager->getOrganizacion();
        $options['em'] = $this->getEntityManager();
        $form = $this->createForm(CombustibleType::class, $combustible, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('secur_combustibletype')['fecha'] . ':00'));
            //  die('////<pre>' . nl2br(var_export($request->get('secur_combustibletype'), true)) . '</pre>////');
            $combustible->setFecha($fecha);
            $combustible->setCargaCompleta($request->get('secur_combustibletype')['cargaCompleta'] === "2"); //si es completa se coloca en true
            $combustible->setMontoTotal(floatval(str_replace(',', '.', $combustible->getMontoTotal())));
            $trama = $this->backendManager->obtenerTramaAnterior($combustible->getServicio()->getId(), $combustible->getFecha());
            if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                $combustible->setOdometro((int) ($trama['odometro'] / 1000));
            }
            if (isset($trama['oid'])) {
                $combustible->setId_trama($trama['oid']);
            }


            $combustible->setModoIngreso(0); //modo manual.
            $combustible->setEstado(0); //nueva.

            if ($combustible->getReferencia() || $combustible->getPuntoCarga()) {
                if ($combustible->getReferencia()) {
                    $combustible->setReferencia($this->referenciaManager->find($request->get('secur_combustibletype')['referencia']));
                }

                if ($combustible->getPuntoCarga()) {
                    $combustible->setPuntoCarga($this->puntoCargaManager->findById($request->get('secur_combustibletype')['puntocarga']));
                }
            } else {
                $this->setFlash('error', sprintf('Debe colocar una referencia.'));
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
            $this->combustibleManager->save($combustible);
            $hasta = clone $fecha->modify('+1 day');
            $desde = clone $fecha->modify('-1 month');
            $filtro['desde'] = $desde->format('d/m/Y H:i');
            $filtro['hasta'] = $hasta->format('d/m/Y H:i');
            $this->setFlash('success', sprintf('La carga de combustible para "%s" ha sido exitosa.', strtoupper($combustible->getServicio())));
            $this->breadcrumbManager->pop();
            $cargas = $this->combustibleManager->findAllCargas($servicio, $desde, $hasta);
            $form = $this->createForm(FechaHistorialCombustibleType::class);

            return $this->render('fuel/Combustible/list.html.twig', array(
                'menu' => $this->getListMenu($servicio, $filtro['desde'], $filtro['hasta']),
                'form' => $form->createView(),
                'servicio' => $servicio,
                'cargas' => $cargas
            ));
        }
        return $this->render('fuel/Combustible/new.html.twig', array(
            'isCanbusData' => $servicio->getCanbus(),
            'combustible' => $combustible,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/fuel/{id}/edit", name="fuel_edit",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_EDITAR")
     */
    public function editAction(Request $request, CargaCombustible $carga)
    {

        $options['estaciones'] = $this->referenciaManager->findEstacionesMapa();
        $options['puntoscarga'] = $this->puntoCargaManager->findAll($this->userloginManager->getOrganizacion());
        $options['organizacion'] = $this->userloginManager->getOrganizacion();
        $options['em'] = $this->getEntityManager();
        $form = $this->createForm(CombustibleEditType::class, $carga, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            //die('////<pre>' . nl2br(var_export($form->getErrors()->getMessage(), true)) . '</pre>////');
            if ($form->isValid()) {
                $trama = $this->backendManager->obtenerTramaAnterior($carga->getServicio()->getId(), $carga->getFecha());
                $this->breadcrumbManager->pop();
                if (is_null($carga->getOdometro())) {
                    if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                        $carga->setOdometro((int) ($trama['odometro'] / 1000));
                    }
                }
                if (isset($trama['oid'])) {
                    $carga->setId_trama($trama['oid']);
                }

                if (isset($trama['fecha']) && isset($trama['fecha'])) {
                    $carga->setFechaTrama(new \DateTime($trama['fecha']));
                }

                if (isset($trama['posicion'])) {
                    $posicion = $trama['posicion'];
                    if (isset($posicion['latitud']) && !is_null($posicion['latitud'])) {
                        $carga->setLatitud($posicion['latitud']);
                    }
                    if (isset($posicion['longitud']) && !is_null($posicion['longitud'])) {
                        $carga->setLongitud($posicion['longitud']);
                    }
                }

                if ($carga->getReferencia() || $carga->getPuntoCarga()) {
                    if ($carga->getReferencia()) {
                        $carga->setReferencia($this->referenciaManager->find($carga->getReferencia()));
                    } else {
                        $carga->setReferencia(null);
                    }
                    if ($carga->getPuntoCarga()) {
                        $carga->setPuntoCarga($this->puntoCargaManager->findById($carga->getPuntoCarga()));
                        //die('////<pre>' . nl2br(var_export($carga->getPuntoCarga()->getNombre(), true)) . '</pre>////');
                    } else {
                        $carga->setPuntoCarga(null);
                    }
                    $this->combustibleManager->update($carga);
                    $this->setFlash('success', 'Los datos de la carga de combustible han sido actualizados.');
                } else {
                    $this->setFlash('error', sprintf('Debe colocar una referencia.'));
                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->breadcrumbManager->getVolver());
                }

                $hasta = clone $carga->getFecha()->modify('+1 month');
                $desde = clone $carga->getFecha()->modify('-1 month');
                $filtro['desde'] = $desde->format('d/m/Y H:i');
                $filtro['hasta'] = $hasta->format('d/m/Y H:i');
                $cargas = $this->combustibleManager->findAllCargas($carga->getServicio(), $desde, $hasta);
                $form = $this->createForm(FechaHistorialCombustibleType::class);

                return $this->render('fuel/Combustible/list.html.twig', array(
                    'menu' => $this->getListMenu($carga->getServicio(), $filtro['desde'], $filtro['hasta']),
                    'form' => $form->createView(),
                    'servicio' => $carga->getServicio(),
                    'cargas' => $cargas
                ));
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Carga");
        return $this->render('fuel/Combustible/edit.html.twig', array(
            'combustible' => $carga,
            'isCanbusData' => $carga->getServicio()->getCanbus(),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/fuel/{id}/delete", name="fuel_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_ELIMINAR")
     */
    public function deleteAction(Request $request, CargaCombustible $carga)
    {

        $this->combustibleManager->delete($carga);
        $this->breadcrumbManager->push($request->getRequestUri(), "Historial");
        $this->setFlash('success', 'Carga eliminada correctamente');
        $this->breadcrumbManager->pop();
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * Para un determinado servicio y una determina carga, se calcula el 
     * rendimiento de la a misma desde una carga anterior.
     * @param type $servicio_id
     * @param type $ult_carga_id
     * @return null
     */
    private function calcularRendimiento($servicio_id, $ult_carga_id)
    {
        //obtengo el servicio
        $servicio = $this->servicioManager->find($servicio_id);

        //obtengo las cargas entre la fecha
        $em_cargas = $this->getEntityManager()->getRepository('App\Entity\CargaCombustible');
        $ultima_carga = $em_cargas->findCarga($ult_carga_id, false);  //busco carga en cuestion.
        //carga anterior a la ultima
        $cargaAnt = $em_cargas->findCargaAnterior($servicio, $ultima_carga, true); //busco la carga anterior a la solicitada
        //carga ultima
        $cargaUlt = $em_cargas->findCarga($ult_carga_id, true);

        if (!$ultima_carga || count($cargaAnt) == 0) {
            return null;    //tiene una solaa carga, entonces salgo
        }

        //armo el array con las cargas.
        $cargas = array(
            '0' => $cargaAnt[0],
            '1' => $cargaUlt[0],
        );

        //armo las fechas donde se consultara el historial +/- 1 dia
        $desde = date("Y-m-d H:i:s", strtotime($cargas[0]['fecha']->format('Y-m-d H:i:s') . "-1 day"));
        $hasta = date("Y-m-d H:i:s", strtotime($cargas[1]['fecha']->format('Y-m-d H:i:s') . "+1 day"));

        $usuario = $this->usuarioManager->findByUsername('security');
        $backend = new BackendManager(
            $this->getEntityManager(),
            $usuario->getOrganizacion(),
            $this->container->get('calypso.backend_addr'),
            $this->userloginManager,
            $this->utilsManager,
            $usuario
        );
        $consulta_consumo = $backend->informeConsumoCombustible(
            $servicio_id,
            $desde,
            $hasta,
            $cargas,
            array()
        );

        return array(
            'servicio_id' => $servicio->getId(),
            'fecha_inicio' => new \DateTime($consulta_consumo[0]['carga1']['fecha']),
            'fecha_fin' => $consulta_consumo[0]['carga2']['fecha'],
            'vin_inicio' => $cargaAnt[0]['codigo_autorizacion'],
            'vin_fin' => $cargaUlt[0]['codigo_autorizacion'],
            'camion' => $servicio->getVehiculo()->getPatente(),
            'distancia_recorrida' => $consulta_consumo[0]['historial']['distancia'] * 1000, //debe estar en mts.
            'rendimiento_teorico' => $consulta_consumo[0]['carga']['rendimiento'] == '---' ? '0' : $consulta_consumo[0]['carga']['rendimiento'],
            'rendimiento_real' => $consulta_consumo[0]['historial']['rendimiento'],
        );
    }

    /**
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function informerendimientoAction($fecha)
    {
        $em_cargas = $this->getEntityManager()->getRepository('App\Entity\CargaCombustible');
        $desde = $fecha . ' 00:00:00';
        $hasta = $fecha . ' 23:59:59';
        $cargas = $em_cargas->findAllCargas(null, $desde, $hasta);
        $resultado = array();
        //die('////<pre>' . nl2br(var_export("aca", true)) . '</pre>////');                    
        foreach ($cargas as $carga) {
            $resultado[] = $this->calcularRendimiento($carga->getServicio()->getId(), $carga->getId());
        }
    }

    /**
     * @Route("/fuel/{idservicio}/listexport", name="fuel_list_export")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_EXPORTAR")
     */
    public function listexportAction(Request $request, $idservicio)
    {
        $servicio = $this->servicioManager->find(array('id' => $idservicio));
        //die('////<pre>' . nl2br(var_export($request, true)) . '</pre>////');
        $desde = null;
        $hasta = null;
        if ($request->get('desde') !== null && $request->get('hasta') !== null) {
            $desde = $this->utilsManager->datetime2sqltimestamp($request->get('desde') . ':00');
            $hasta = $this->utilsManager->datetime2sqltimestamp($request->get('hasta') . ':59');
        }
        //  die('////<pre>' . nl2br(var_export($desde, true)) . '</pre>////');
        $cargas = $this->combustibleManager->findAllCargas($servicio, $desde, $hasta);
        //renderizo el template.
        $result = $this->renderView('fuel/Combustible/export.xls.twig', array(
            'cargas' => $cargas,
            'fecha_desde' => $desde,
            'fecha_hasta' => $hasta,
            'servicio' => $servicio
        ));
        //genero el archivo.
        return new Response($result, 200, array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Expires' => '0',
            'Content-Disposition' => 'attachment; filename=CargasCombustible.xls'
        ));
    }

    /**
     * @Route("/fuel/export", name="fuel_exporta")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        $totales = json_decode($request->get('totales'), true);
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => 'Toda la flota',
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //es para un solo servicio.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Carga 1', 'width' => 20),
                    'B' => array('title' => '', 'width' => 20),
                    'C' => array('title' => '', 'width' => 20),
                    'D' => array('title' => 'Carga 2', 'width' => 20),
                    'E' => array('title' => '', 'width' => 20),
                    'F' => array('title' => '', 'width' => 20),
                    'G' => array('title' => '', 'width' => 20),
                    'H' => array('title' => 'Segun Sistema', 'width' => 20),
                    'I' => array('title' => '', 'width' => 20),
                    'J' => array('title' => '', 'width' => 20),
                ));
                $xls->setBar(7, array(
                    'A' => array('title' => 'Fecha', 'width' => 20),
                    'B' => array('title' => 'Lugar', 'width' => 40),
                    'C' => array('title' => 'S/Sistema', 'width' => 20),
                    'D' => array('title' => 'S/Ticket', 'width' => 20),
                    'E' => array('title' => 'Diferencia', 'width' => 20),
                    'F' => array('title' => 'Fecha', 'width' => 20),
                    'G' => array('title' => 'Lugar', 'width' => 40),
                    'H' => array('title' => 'S/Ticket', 'width' => 20),
                    'I' => array('title' => 'Distancia (km)', 'width' => 20),
                    'J' => array('title' => 'Promedio (km/h)', 'width' => 20),
                    'K' => array('title' => 'Rendimiento (km/lt)', 'width' => 20),
                ));
                $i = 8;
                foreach ($informe as $key => $value) {
                    //die('////<pre>' . nl2br(var_export($value, true)) . '</pre>////');
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['carga1']['fecha']),
                        'B' => array('value' => $value['carga1']['lugar']),
                        'C' => array('value' => isset($value['carga1']['reales']) ? $value['carga1']['reales'] : '---'),
                        'D' => array('value' => isset($value['carga1']['litros']) ? $value['carga1']['litros'] : '---'),
                        'E' => array('value' => isset($value['carga1']['diferencia']) ? $value['carga1']['diferencia'] : '---'),
                        'F' => array('value' => $value['carga2']['fecha']),
                        'G' => array('value' => $value['carga2']['lugar']),
                        'H' => array('value' => $value['carga2']['litros'], 'format' => '0.00'),
                        'I' => array('value' => $value['historial']['distancia'], 'format' => '0.00'),
                        'J' => array('value' => $value['historial']['velocidad_promedio'], 'format' => '0.00'),
                        'K' => array('value' => $value['historial']['rendimiento'], 'format' => '0.00'),
                        'L' => array('value' => isset($value['rendimiento_teorico']) ? $value['rendimiento_teorico'] : '---'),
                    ));
                    $i++;
                }

                $xls->setRowTotales($i, array(
                    'A' => array('value' => ''),
                    'B' => array('value' => 'TOTALES==>'),
                    'C' => array('value' => $totales['totalEquipo'], 'format' => '0.00'),
                    'D' => array('value' => $totales['totalSistema'], 'format' => '0.00'),
                    'E' => array('value' => $totales['totalDiferencia'], 'format' => '0.00'),
                    'F' => array('value' => ''),
                    'G' => array('value' => ''),
                    'H' => array('value' => $totales['litros'], 'format' => '0.00'),
                    'I' => array('value' => $totales['distancia'], 'format' => '0.00'),
                    'J' => array('value' => $totales['velocidad_promedio'], 'format' => '0.00'),
                    'K' => array('value' => $totales['rendimiento'], 'format' => '0.00'),
                ));
            } else {   //es para la flota.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Cargas', 'width' => 20),
                    'C' => array('title' => 'Litros', 'width' => 20),
                    'D' => array('title' => 'Distancia (km)', 'width' => 20),
                    'E' => array('title' => 'Promedio (km/h)', 'width' => 20),
                    'F' => array('title' => 'Rendimiento (km/lt)', 'width' => 20),
                    'G' => array('title' => 'Rendimiento teorico', 'width' => 20),
                ));
                $i = 7;
                //                die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                foreach ($informe as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['servicio_nombre']),
                        'B' => array('value' => $value['cantidad'], 'format' => '0.00'),
                        'C' => array('value' => $value['litros'], 'format' => '0.00'),
                        'D' => array('value' => $value['distancia'], 'format' => '0.00'),
                        'E' => array('value' => $value['velocidad_promedio'], 'format' => '0.00'),
                        'F' => array('value' => $value['rendimiento'], 'format' => '0.00'),
                        'G' => array('value' => $value['rendimiento_teorico'], 'format' => '0.00',),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_portal'));
        }
    }

    /**
     * @Route("/fuel/exportflota", name="fuel_exportarflota")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarflotaAction(Request $request, $tipo = null)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Cargas', 'width' => 20),
                'C' => array('title' => 'Primera Carga', 'width' => 20),
                'D' => array('title' => 'Ultima Carga', 'width' => 20),
                'E' => array('title' => 'Litros', 'width' => 20),
                'F' => array('title' => 'Kilometros', 'width' => 20),
                'G' => array('title' => 'Km/lt', 'width' => 20),
                'H' => array('title' => 'lt/100km', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $value) {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $value['servicio_nombre']),
                    'B' => array('value' => $value['cantidad']),
                    'C' => array('value' => $value['fecha_prim']),
                    'D' => array('value' => $value['fecha_ult']),
                    'E' => array('value' => $value['litros']),
                    'F' => array('value' => $value['distancia']),
                    'G' => array('value' => $value['rendimiento']),
                    'H' => array('value' => $value['ltsporcien']),
                ));
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_informe_portal'));
        }
    }

    public function getGraficoInforme($informe, $consulta)
    {
        if (isset($consulta['total'])) {  //toda la flota o grupo
            $consulta['servicionombre'] = $consulta['servicio'];
            $myArray = array();
            foreach ($informe as $key => $value) {
                if (isset($value['servicio'])) {
                    $myArray[$key]['servicio'] = $value['servicio_nombre'];
                    $myArray[$key]['rendimiento'] = number_format($value['rendimiento'], 2);
                    $myArray[$key]['teorico'] = $value['servicio']->getVehiculo()->getRendimiento();
                    //            $myArray[$key]['kilometros'] = $value['historial']['distancia'] *1;
                }
            }

            $dt = new DataTable\DataTable();
            $dt->addColumn('servicio', 'Servicio', 'string');
            $dt->addColumn('rendimiento', 'Rendimiento', 'number');
            $dt->addColumn('teorico', 'Teorico', 'number');
            //        $dt->addColumn('kilometros', 'Kilómetros', 'number');
            //------------
            $dt->addRows($myArray);
        } else {
            $myArray = array();
            foreach ($informe as $key => $value) {
                $myArray[$key]['fecha'] = $value['carga1']['fecha'];
                $myArray[$key]['velocidad'] = isset($value['historial']['velocidad_promedio']) ? intval($value['historial']['velocidad_promedio']) : null;
                $myArray[$key]['rendimiento'] = isset($value['historial']['rendimiento']) ? intval($value['historial']['rendimiento']) * 10 : null;
                $myArray[$key]['teorico'] = isset($value['rendimiento_teorico']) ? intval($value['rendimiento_teorico']) * 10 : null;
                //            $myArray[$key]['kilometros'] = $value['historial']['distancia'] *1;
            }

            $dt = new DataTable\DataTable();
            $dt->addColumn('fecha', 'Fecha', 'string');
            $dt->addColumn('velocidad', 'Veloc. Prom.', 'number');
            $dt->addColumn('rendimiento', 'Rendimiento (x10)', 'number');
            $dt->addColumn('teorico', 'Teorico (x10)', 'number');
            //        $dt->addColumn('kilometros', 'Kilómetros', 'number');
            //------------
            $dt->addRows($myArray);
        }
        return $dt->toStrictArray();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function filtrarHistorial($historial, $filtrar = true, $canbus = false)
    {
        $consumo = array();
        reset($historial);
        //        die('////<pre>' . nl2br(var_export($historial, true)) . '</pre>////');
        while ($trama = current($historial)) {
            next($historial);

            if (!isset($trama['posicion']))   //descarto las que no tienen posicion.
                continue;

            $tCC = array(
                'fecha' => $trama['fecha'],
                'contacto' => isset($trama['contacto']) ? $trama['contacto'] : false,
                'velocidad' => $trama['posicion']['velocidad'],
                'tanque' => null,
                'real' => isset($trama['carcontrol']) ? intval($trama['carcontrol']['porcentaje_tanque']) : 0,
                'distancia' => isset($trama['distancia']) ? $trama['distancia'] : 0,
                'tiempo' => isset($trama['tiempo']) ? $trama['tiempo'] : 0,
            );

            if ($canbus) {
                if (isset($trama['canbusData']) && $trama['canbusData']['nivelCombustible'] > 0) {
                    $tCC['tanque'] = intval($trama['canbusData']['nivelCombustible']);
                    $tCC['real'] = intval($trama['canbusData']['nivelCombustible']);
                    $filtar = true;
                }
            } else {
                $tCC['tanque'] = ($tCC['real'] >= 1) ? $tCC['real'] : 0;  //limpio las mediciones < 1% porque no son validas
            }

            if ($filtrar) {
                //ahora debo modificar las tramas que tengan valores y obtener una linea sin saltos para arriba.
                if ($tCC['tanque'] != 0) {   //solo para las tramas con datos.                
                    if (!isset($tMin)) { //existe una 
                        $tMin = $tCC;
                    }

                    if ($tMin['tanque'] < $tCC['tanque'] && abs($tMin['tanque'] - $tCC['tanque']) > 10) {  //posible carga de combustible
                        //tengo una carga de combustible mayor de 10%
                        $tMin = $tCC;
                    }
                    //comparo el minimo que tengo con el minimo actual y reemplazo.
                    $tCC['tanque'] = min($tMin['tanque'], $tCC['tanque']);
                    if ($tMin['tanque'] > $tCC['tanque']) {   ///
                        $tMin = $tCC;   //ahora tomo el nuevo minimo.
                    }
                }
            }

            if ($canbus) {
                if ($trama['canbusData']) {
                    $consumo[] = $tCC;
                }
            } else {
                $consumo[] = $tCC;
            }
        }
        return $consumo;
    }

    /**
     * @Route("/fuel/{id}/listexport", name="fuel_findcarga",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_EXPORTAR")
     */
    public function findcargaAction(Request $request, Servicio $servicio)
    {
        // si viene nulo buscamos por el dia actual
        $fecha = $request->get('fecha');
        if (is_null($fecha)) {
            $fecha = date_format(new \DateTime(), 'Y/m/d');
        } else {
            $fecha = date_format(new \DateTime($fecha), 'Y/m/d');
        }

        $fechaDesde = date_format(new \DateTime($fecha . ' 00:00'), 'Y/m/d H:i');
        $fechaHasta = date_format(new \DateTime($fecha . ' 23:59'), 'Y/m/d H:i');
        if ($servicio->getMedidorCombustible()) {   //siempre que tenga mediciones de combustible.
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }

        $historial = $this->backendManager->historial($servicio->getId(), $fechaDesde, $fechaHasta, array(), array());
        $consumo = $this->filtrarHistorial($historial, true, $servicio->getCanbus());
        unset($historial);
        //proceso los datos para armar la grafica del porcentaje

        $cargaArray = array();
        $i = 1;  //$i es el indice del array
        $j = 2;  //$j es el posterior del indice del array
        for ($i = 1; $i < count($consumo); $i++) {
            if ($this->hayCarga($consumo[$i], $consumo[$j])) {  //solo tomo las cargas que van creciendo.
                $cargaArray[$i]['consumo'] = $consumo[$i];
                $cargaArray[$i]['tanquePosterior']['porcentaje'] = $consumo[$j]['tanque'];
                $cargaArray[$i]['tanqueAnterior']['porcentaje'] = $consumo[$i]['tanque'];
                $horaComienzoCarga = new \DateTime($consumo[$i]['fecha']);
                $horaFinCarga = new \DateTime($consumo[$j]['fecha']);
                $cargaArray[$i]['desde'] = $horaComienzoCarga->modify('-1 minutes')->format('d-m-Y H:i:s');
                $cargaArray[$i]['hasta'] = $horaFinCarga->modify('+1 minutes')->format('d-m-Y H:i:s');
                if (isset($isMedicionesLoad) && $isMedicionesLoad) {
                    $cargaArray[$i]['tanquePosterior']['lit'] = $this->tanqueManager->getCapacidad($consumo[$j]['tanque'], $servicio);
                    $cargaArray[$i]['tanqueAnterior']['lit'] = $this->tanqueManager->getCapacidad($consumo[$i]['tanque'], $servicio);
                    $cargaArray[$i]['diferencia'] = $cargaArray[$i]['tanquePosterior']['lit']['litros'] - $cargaArray[$i]['tanqueAnterior']['lit']['litros'];
                }
            }
            $j++;
        }
        //        die('////<pre>' . nl2br(var_export($cargaArray, true)) . '</pre>////');
        $html = $this->renderView("apmon/Combustible/tabla_combustible_canbus.html.twig", array('arr' => $cargaArray));

        return new Response($html);
    }

    private function hayCarga($carga1, $carga2)
    {
        $ok = false;
        $conValores = !is_null($carga1['tanque']) && !is_null($carga1['tanque']);
        if ($conValores && $carga2['tanque'] > $carga1['tanque']) {  //solo tomo las cargas que van creciendo.
            $ok = true;
        }
        return $ok;
    }

    /**
     * @Route("/fuel/getgrupo", name="fuel_getgrupo")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_EXPORTAR")
     */
    public function getgruposAction()
    {
        // die('////<pre>' . nl2br(var_export('$choice', true)) . '</pre>////');
        $choice = array();
        $grupos = $this->grupoServicioManager->findAllByOrganizacion();
        foreach ($grupos as $grupo) {
            $choice['g' . $grupo->getId()] = 'Grp: ' . $grupo->getNombre();
        }
        return new Response(json_encode($choice));
    }

    /**
     * @Route("/fuel/{id}/newmasiva", name="fuel_newmasiva",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function newmasivaAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevas Cargas");
        $options['estaciones'] = $this->referenciaManager->findEstacionesMapa();
        $options['puntoscarga'] = $this->puntoCargaManager->findAll($organizacion);
        $options['servicios'] = $this->servicioManager->findAllByOrganizacion($organizacion);
        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getEntityManager();
        $combustible = new CargaCombustible();

        $form = $this->createForm(CombustibleMasivaType::class, null, $options);

        if ($request->getMethod() == 'POST') {
            $data = $form->getData();
            $servicio = $this->servicioManager->find($request->get('servicio'));
            $combustible->setServicio($servicio);
            $chofer = $this->choferManager->find(intval($request->get('chofer')));
            $combustible->setChofer($chofer);
            $tipoCombustible = $this->combustibleManager->findTipoCombustible(intval($request->get('tipoCombustible')));
            $combustible->setTipoCombustible($tipoCombustible);
            
            //cambio el formato de la fecha/hora
            $sqldate = $this->utilsManager->datetime2sqltimestamp($request->get('fecha'), false);        
            //$sqldate = date('Y-m-d H:i:s', strtotime($request->get('fecha') . ':00'));
            //dd($sqldate);
            $combustible->setFecha($request->get('fecha') === '' ? null : new \DateTime($sqldate));
            $combustible->setGuiaDespacho($request->get('guiaDespacho'));
            $combustible->setLitrosCarga(floatval($request->get('litrosCarga')));
            $combustible->setCargaCompleta($request->get('tipoCarga') === "2"); //si es completa se coloca en true
            $combustible->setDescripcion($request->get('descripcion') !== '' ? $request->get('descripcion') : null); //si es completa se coloca en true
            $combustible->setOdometroTablero($request->get('odometro') == '' ? null : intval($request->get('odometro')));
            $combustible->setHorometroTablero($request->get('horometro') == '' ? null : intval($request->get('horometro')));
            $monto = $request->get('montoTotal');

            if ($monto != 'NaN') {
                $combustible->setMontoTotal(floatval($request->get('montoTotal')));
            }

            $combustible->setModoIngreso(0); //modo manual.
            $combustible->setEstado(0); //nueva.

            if ($request->get('estacion') != '') {
                $ref = $this->referenciaManager->find(intval($request->get('estacion')));
                $combustible->setReferencia($ref);
            }

            //  die('////<pre>' . nl2br(var_export($request->get('puntocarga'), true)) . '</pre>////');
            if ($request->get('puntocarga') != '') {
                $puntocarga = $this->puntoCargaManager->findById(intval($request->get('puntocarga')));
                $combustible->setPuntoCarga($puntocarga);
            }


            $trama = $this->backendManager->obtenerTramaAnterior($servicio->getId(), $combustible->getFecha());
            if (!isset($data['odometro']) || is_null($data['odometro'])) {
                if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                    $combustible->setOdometro((float) ($trama['odometro'] / 1000));
                }
            } else {
                $combustible->setOdometro($request->get('odometro'));
            }

            if (isset($trama['fecha']) && isset($trama['fecha'])) {
                $combustible->setFechaTrama(new \DateTime($trama['fecha']));
            }

            if (isset($trama['oid'])) {
                $combustible->setId_trama($trama['oid']);
            }

            if (isset($trama['posicion'])) {
                $posicion = $trama['posicion'];
                if (isset($posicion['latitud']) && !is_null($posicion['latitud'])) {
                    $combustible->setLatitud($posicion['latitud']);
                }

                if (isset($posicion['longitud']) && !is_null($posicion['longitud'])) {
                    $combustible->setLongitud($posicion['longitud']);
                }
            }

            $direc = $this->geocoderManager->inverso($combustible->getLatitud(), $combustible->getLongitud());

            $return = array(
                'status' => 'OK',
                'latitud' => !is_null($combustible->getLatitud()) ? $combustible->getLatitud() : '---',
                'longitud' => !is_null($combustible->getLongitud()) ? $combustible->getLongitud() : '---',
                'servicio' => $combustible->getServicio()->getNombre(),
                'ubicacion' => $direc,
                'fecha' => !is_null($combustible->getFecha()) ? $combustible->getFecha()->format('d-m-Y H:i') : 'n/n',
                'guiaDespacho' => $combustible->getGuiaDespacho(),
                'litrosCarga' => $combustible->getLitrosCarga(),
                'montoTotal' => $combustible->getMontoTotal() ? $combustible->getMontoTotal() : '---',
                'odometro' => !is_null($combustible->getOdometro()) ? $combustible->getOdometro() : '---',
                'odometroTablero' => $combustible->getOdometroTablero(),
                'horometroTablero' => $combustible->getHorometroTablero() !== null ? $combustible->getHorometroTablero() : '---',
                'estacion' => !is_null($combustible->getReferencia()) ? $combustible->getReferencia()->getNombre() : '---',
                'puntocarga' => !is_null($combustible->getPuntoCarga()) ? $combustible->getPuntoCarga()->getNombre() : '---',
                'tipoCarga' => !is_null($combustible->getCargaCompleta()) ? $combustible->getCargaCompleta() ? 'Completa' : 'Parcial' : 'n/n',
                'descripcion' => !is_null($combustible->getDescripcion()) ? $combustible->getDescripcion() : '---',
                'chofer' => !is_null($combustible->getChofer()) ? $combustible->getChofer()->getNombre() : '---',
                'tipoCombustible' => !is_null($combustible->getTipoCombustible()) ? $combustible->getTipoCombustible()->getNombre() : '---',
            );

            if ($this->combustibleManager->save($combustible)) {
                $return['id'] = $combustible->getId();
                $return['status'] = 'OK';
            } else {
                $return['status'] = 'ERROR';
            }

            return new Response(json_encode($return));
        }

        return $this->render('fuel/Combustible/newmasiva.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView()
        ));
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
