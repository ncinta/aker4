<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleInformeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\GeocoderManager;

class InfoRendimientoCargasExportController extends AbstractController
{

    private $grupoServicioManager;
    private $servicioManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $combustibleManager;
    private $backendManager;
    private $referenciaManager;
    private $tanqueManager;
    private $geocoderManager;
    private $excelManager;

    public function __construct(
        GrupoServicioManager $grupoServicioManager,
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        CombustibleManager $combustibleManager,
        BackendManager $backendManager,
        ReferenciaManager $referenciaManager,
        TanqueManager $tanqueManager,
        GeocoderManager $geocoderManager,
        ExcelManager $excelManager
    ) {
        $this->grupoServicioManager = $grupoServicioManager;
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->combustibleManager = $combustibleManager;
        $this->backendManager = $backendManager;
        $this->referenciaManager = $referenciaManager;
        $this->tanqueManager = $tanqueManager;
        $this->geocoderManager = $geocoderManager;
        $this->excelManager = $excelManager;
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/rendimientocargas/{tipo}/exportar", name="fuel_info_rendimientocargas_exportar",
     *     requirements={
     *         "tipo": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarAction(Request $request, $tipo)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Exportar");
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        $totales = json_decode($request->get('totales'), true);
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => 'Toda la flota',
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //es para un solo servicio.
                $xls->setBar(7, array(
                    'A' => array('title' => 'Carga Inicial', 'width' => 20),
                    'B' => array('title' => 'Lugar', 'width' => 40),
                    'C' => array('title' => 'Carga Final', 'width' => 40),
                    'D' => array('title' => 'Litros', 'width' => 40),
                    'E' => array('title' => $consulta['unidad'] == 1 ? 'Kilometros' : 'Horas', 'width' => 20),
                    'F' => array('title' => $consulta['unidad'] == 1 ? 'Km/Litro' : 'Litros/Horas', 'width' => 20),
                    'G' => array('title' => $consulta['unidad'] == 1 ? 'Litros/100 Kms' : 'Horas/100 Litros', 'width' => 20),
                ));
                $i = 8;
                foreach ($informe as $key => $value) {
                    // die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                    $xls->setRowValues($i, array(
                        'A' => array('value' => isset($value['carga1']['fecha']) ? $value['carga1']['fecha'] : '---', 'format' => '0.00'),
                        'B' => array('value' => isset($value['carga1']['lugar']) ? $value['carga1']['lugar'] : '---', 'format' => '0.00'),
                        'C' => array('value' => isset($value['carga2']['fecha']) ? $value['carga2']['fecha'] : '---', 'format' => '0.00'),
                        'D' => array('value' => isset($value['parcial']['litros']) ? $value['parcial']['litros'] : '---', 'format' => '0.00'),
                        'E' => array('value' => $consulta['unidad'] == 1 ? $value['parcial']['kilometros'] : $value['parcial']['horas'], 'format' => '0.00'),
                        'F' => array('value' => $consulta['unidad'] == 1 ? $value['parcial']['kmLitro'] : $value['parcial']['litrosHora'], 'format' => '0.00'),
                        'G' => array('value' => $consulta['unidad'] == 1 ? $value['parcial']['litrosCienKms'] : $value['parcial']['horasCienLitros'], 'format' => '0.00'),
                    ));
                    $i++;
                }

                //  die('////<pre>' . nl2br(var_export($totales, true)) . '</pre>////');
                $xls->setRowTotales($i, array(
                    'A' => array('value' => ''),
                    'B' => array('value' => ''),
                    'C' => array('value' => 'TOTALES==>'),
                    'D' => array('value' => isset($totales['litros']) ? $totales['litros'] : '---', 'format' => '0.00'),
                    'E' => array('value' => $consulta['unidad'] == 1 ? $totales['kilometros'] : $totales['horas'], 'format' => '0.00'),
                    'F' => array('value' => $consulta['unidad'] == 1 ? $totales['kmLitro'] : $totales['litrosHora'], 'format' => '0.00'),
                    'G' => array('value' => $consulta['unidad'] == 1 ? $totales['litrosCienKms'] : $totales['horasCienLitros'], 'format' => '0.00'),
                ));
            } else {   //es para la flota.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Cargas', 'width' => 20),
                    'C' => array('title' => 'Litros', 'width' => 20),
                    'D' => array('title' => 'Distancia (km)', 'width' => 20),
                    'E' => array('title' => 'Promedio (km/h)', 'width' => 20),
                    'F' => array('title' => 'Rendimiento (km/lt)', 'width' => 20),
                    'G' => array('title' => 'Rendimiento teorico', 'width' => 20),
                ));
                $i = 7;
                //                die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                foreach ($informe as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['servicio_nombre']),
                        'B' => array('value' => $value['cantidad'], 'format' => '0.00'),
                        'C' => array('value' => $value['litros'], 'format' => '0.00'),
                        'D' => array('value' => $value['distancia'], 'format' => '0.00'),
                        'E' => array('value' => $value['velocidad_promedio'], 'format' => '0.00'),
                        'F' => array('value' => $value['rendimiento'], 'format' => '0.00'),
                        'G' => array('value' => $value['rendimiento_teorico'], 'format' => '0.00',),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            $this->breadcrumbManager->pop();
            return $this->breadcrumbManager->getVolver();
        }
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/rendimientocargas/{tipo}/exportarflota", name="fuel_info_rendimientocargas_exportarflota",
     *     requirements={
     *         "tipo": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarflotaAction(Request $request, $tipo)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Exportar");
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            // Crear el archivo XLS
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");

            // Configurar el encabezado del informe
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            
            // Detalle de las cargas del vehículo
            $xls->setBar(6, array(
                'A' => array('title' => 'Vehículo', 'width' => 30),
                'B' => array('title' => 'Carga 1 Fecha', 'width' => 20),
                'C' => array('title' => 'Carga 2 Fecha', 'width' => 20),
                'D' => array('title' => 'Litros', 'width' => 15),
                'E' => array('title' => 'Distancia', 'width' => 20),
                
                'F' => array('title' => 'Kms x Lt', 'width' => 15),
                'G' => array('title' => 'Litros/100 Km', 'width' => 20),

                'H' => array('title' => 'Horas', 'width' => 15),
                'I' => array('title' => 'Lts/hora', 'width' => 20),
                'J' => array('title' => 'Lts/hora c/100', 'width' => 20),

                'K' => array('title' => 'Kms Tablero', 'width' => 20),
                'L' => array('title' => 'Hs Tablero', 'width' => 20),
                'M' => array('title' => 'Kms x Lt Tablero', 'width' => 15),
                'N' => array('title' => 'Lts/100 Km Tablero', 'width' => 20),
                'O' => array('title' => 'Lts/Hora Tablero', 'width' => 20),
                'P' => array('title' => 'Horas/100 Lts Tablero', 'width' => 20),

                'Q' => array('title' => 'Cons. Teorico', 'width' => 20),
            ));
            $i = 7; // Iniciar después del encabezado

            foreach ($informe as $vehiculo => $data) {
             //   dd($data);
                // Resumen del vehículo
                $resumen = $data['resumen'];

                foreach ($data['detalle'] as $carga) {
                    $parcial = $carga['parcial'];
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $vehiculo),
                        'B' => array('value' => $carga['carga1']['fechaCompleta']),
                        'C' => array('value' => $carga['carga2']['fechaCompleta']),
                        'D' => array('value' => $parcial['litros'], 'format' => '0.00'),
                        'E' => array('value' => $parcial['kilometros'], 'format' => '0.00'),

                        'F' => array('value' => $parcial['kmLitro'], 'format' => '0.00'),
                        'G' => array('value' => $parcial['litrosCienKms'], 'format' => '0.00'),

                        'H' => array('value' => $parcial['horas'], 'format' => '0.00'),
                        'I' => array('value' => $parcial['litrosHora'], 'format' => '0.00'),
                        'J' => array('value' => $parcial['horasCienLitros'], 'format' => '0.00'),

                        'K' => array('value' => $parcial['kilometrosTablero'], 'format' => '0.00'),
                        'L' => array('value' => $parcial['horasTablero'], 'format' => '0.00'),
                        'M' => array('value' => $parcial['litrosKmTablero'], 'format' => '0.00'),
                        'N' => array('value' => $parcial['litrosCienKmsTablero'], 'format' => '0.00'),
                        'O' => array('value' => $parcial['litrosHoraTablero'], 'format' => '0.00'),
                        'P' => array('value' => $parcial['litrosCienHorasTablero'], 'format' => '0.00'),
                    
                        'Q' => array('value' => $parcial['consumoTeoricoPorKm'], 'format' => '0.00'),                       
                    ));
                    $i++;
                }
            }

            // Generar y enviar el archivo de respuesta
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');
            $response->headers->set('Cache-Control', 'maxage=1');

            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }


    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/rendimientocargas/{tipo}/exportarflota/detallada", name="fuel_info_rendimientocargas_exportarflota_detallada",
     *     requirements={
     *         "tipo": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarflotadetalladaAction(Request $request, $tipo)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Exportar");
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Cargas', 'width' => 20),
                'C' => array('title' => 'Primera Carga', 'width' => 20),
                'D' => array('title' => 'Ultima Carga', 'width' => 20),
                'E' => array('title' => 'Litros', 'width' => 20),
                'F' => array('title' => 'Rendim. Teorico lts/100km', 'width' => 20),
                'G' => array('title' => 'Rendim. Teorico lts/Hora', 'width' => 20),
                'H' => array('title' => 'Kms Recorridos GPS', 'width' => 20),
                'I' => array('title' => 'Kms Recorridos Tablero', 'width' => 20),
                'J' => array('title' => 'Consumo GPS (l/100km)', 'width' => 20),
                'K' => array('title' => 'Consumo Tablero (l/100km)', 'width' => 20),
                'L' => array('title' => 'Consumo Teórico', 'width' => 20),
                'M' => array('title' => 'Horas ralenti', 'width' => 20),
                'N' => array('title' => 'Consumo teorico ralentí', 'width' => 20),
                'O' => array('title' => 'Total', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $value) {
                //   die('////<pre>' . nl2br(var_export($request, true)) . '</pre>////');
                $xls->setRowValues($i, array(
                    'A' => array('value' => $value['servicio_nombre']),
                    'B' => array('value' => $value['cantidad']),
                    'C' => array('value' => $value['fecha_prim']),
                    'D' => array('value' => $value['fecha_ult']),
                    'E' => array('value' => $value['litros'], 'format' => '0.00'),
                    'F' => array('value' => $value['rendimiento']),
                    'G' => array('value' => $value['rendimientoHora']),
                    'H' => array('value' => $value['distancia'] > 0 ? $value['distancia'] : '---', 'format' => '0.00'),
                    'I' => array('value' => $value['distanciaTablero'] > 0 ? $value['distanciaTablero'] : '---', 'format' => '0.00'),
                    'J' => array('value' => $value['litrosCienKms'], 'format' => '0.00'),
                    'K' => array('value' => $value['distanciaTablero'] > 0 ? $value['litrosCienKmsTablero'] : '---', 'format' => '0.00'),
                    'L' => array('value' => $value['consumoTeoricoPorKm'], 'format' => '0.00'),
                    'M' => array('value' => $value['ralenti'], 'format' => '0.00'),
                    'N' => array('value' => $value['consumoTeoricoRalenti'], 'format' => '0.00'),
                    'O' => array('value' => $value['consumoTotal'], 'format' => '0.00'),
                ));
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuelrendimientocargas{tipo}/exportar/detallada", name="fuel_info_rendimientocargas_exportar_detallada",
     *     requirements={
     *         "tipo": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportardetalladaAction(Request $request, $tipo)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Exportar");
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        $carga0 = json_decode($request->get('carga0'), true);

        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
                'C5' => 'Rendim. Teorico lts/100km',
                'D5' => $consulta['rendimiento'],
                'C6' => 'Rendim. Teorico lts/Hora',
                'D6' => $consulta['rendimientoHora'],
            ));

            $xls->setBar(8, array(
                'A' => array('title' => 'Fecha', 'width' => 20),
                'B' => array('title' => 'Km GPS', 'width' => 20),
                'C' => array('title' => 'Km Tablero', 'width' => 20),
                'D' => array('title' => 'Litros', 'width' => 20),
                'E' => array('title' => 'Kms Recorridos GPS', 'width' => 20),
                'F' => array('title' => 'Kms Recorridos Tablero', 'width' => 20),
                'G' => array('title' => 'Consumo GPS (l/100km)', 'width' => 20),
                'H' => array('title' => 'Consumo Tablero (l/100km)', 'width' => 20),
                'I' => array('title' => 'Consumo Teórico', 'width' => 20),
                'J' => array('title' => 'Horas ralenti', 'width' => 20),
                'K' => array('title' => 'Consumo teorico ralentí', 'width' => 20),
                'L' => array('title' => 'Total', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $value) {
                //   die('////<pre>' . nl2br(var_export($request, true)) . '</pre>////');
                $xls->setRowValues($i, array(
                    'A' => array('value' => isset($value['carga1']['fecha']) ? $value['carga1']['fecha'] : '---', 'format' => '0.00'),
                    'B' => array('value' => $value['carga1']['odometro'], 'format' => '0.00'),
                    'C' => array('value' => $value['carga1']['odometroTablero'] > 0 ? $value['carga1']['odometroTablero'] : '---', 'format' => '0.00'),
                    'D' => array('value' => $value['parcial']['litros'] > 0 ? $value['parcial']['litros'] : '---', 'format' => '0.00'),
                    'E' => array('value' => $value['parcial']['kilometros'] > 0 ? $value['parcial']['kilometros'] : '---', 'format' => '0.00'),
                    'F' => array('value' => $value['carga1']['odometroTablero'] > 0 ? $value['parcial']['kilometrosTablero'] : '---', 'format' => '0.00'),
                    'G' => array('value' => $value['parcial']['litrosCienKms'] > 0 ? $value['parcial']['litrosCienKms'] : '---', 'format' => '0.00'),
                    'H' => array('value' => $value['carga1']['odometroTablero'] > 0 ? $value['parcial']['litrosCienKmsTablero'] : '---', 'format' => '0.00'),
                    'I' => array('value' => $value['parcial']['consumoTeoricoPorKm'] > 0 ? $value['parcial']['consumoTeoricoPorKm'] : '---', 'format' => '0.00'),
                    'J' => array('value' => $value['parcial']['ralenti'] > 0 ? $value['parcial']['ralenti'] : '---', 'format' => '0.00'),
                    'K' => array('value' => $value['parcial']['consumoTeoricoRalenti'] > 0 ? $value['parcial']['consumoTeoricoRalenti'] : '---', 'format' => '0.00'),
                    'L' => array('value' => $value['parcial']['consumoTotal'] > 0 ? $value['parcial']['consumoTotal'] : '---', 'format' => '0.00'),
                ));
                $i++;
            }
            $xls->setRowValues($i, array(
                'A' => array('value' => isset($carga0['fecha']) ? $carga0['fecha'] : '---', 'format' => '0.00'),
                'B' => array('value' => $carga0['odometro'], 'format' => '0.00'),
                'C' => array('value' => $carga0['odometroTablero'] > 0 ? $carga0['odometroTablero'] : '---', 'format' => '0.00'),
                'D' => array('value' => '<== 1ra Carga'),
            ));

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/rendimientocargas/{tipo}/exportarcanbus", name="fuel_info_rendimientocanbus_exportar",
     *     requirements={
     *         "tipo": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function exportarcanbusAction(Request $request, $tipo)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Exportar");
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        $totales = json_decode($request->get('totales'), true);
        if (isset($tipo) && isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Informe de Rendimiento Combustible");

            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => 'Toda la flota',
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));
            if ($tipo == '0') {   //es para un solo servicio.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Carga 1', 'width' => 20),
                    'B' => array('title' => '', 'width' => 20),
                    'C' => array('title' => '', 'width' => 20),
                    'D' => array('title' => 'Carga 2', 'width' => 20),
                    'E' => array('title' => '', 'width' => 20),
                    'F' => array('title' => '', 'width' => 20),
                    'G' => array('title' => '', 'width' => 20),
                    'H' => array('title' => 'Segun Sistema', 'width' => 20),
                    'I' => array('title' => '', 'width' => 20),
                    'J' => array('title' => '', 'width' => 20),
                ));
                $xls->setBar(7, array(
                    'A' => array('title' => 'Fecha', 'width' => 20),
                    'B' => array('title' => 'Lugar', 'width' => 40),
                    'C' => array('title' => 'S/Sistema', 'width' => 20),
                    'D' => array('title' => 'S/Ticket', 'width' => 20),
                    'E' => array('title' => 'Diferencia', 'width' => 20),
                    'F' => array('title' => 'Fecha', 'width' => 20),
                    'G' => array('title' => 'Lugar', 'width' => 40),
                    'H' => array('title' => 'S/Ticket', 'width' => 20),
                    'I' => array('title' => 'Distancia (km)', 'width' => 20),
                    'J' => array('title' => 'Promedio (km/h)', 'width' => 20),
                    'K' => array('title' => 'Rendimiento (km/lt)', 'width' => 20),
                    'L' => array('title' => 'Rend teórico', 'width' => 20),
                    'M' => array('title' => 'Odom. inicial', 'width' => 20),
                    'N' => array('title' => 'Odom. final', 'width' => 20),
                ));
                $i = 8;
                foreach ($informe as $key => $value) {
                    // die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                    $xls->setRowValues($i, array(
                        'A' => array('value' => isset($value['carga1']['fecha']) ? $value['carga1']['fecha'] : '---'),
                        'B' => array('value' => isset($value['carga1']['lugar']) ? $value['carga1']['lugar'] : '---'),
                        'C' => array('value' => isset($value['carga1']['reales']) ? $value['carga1']['reales'] : 0, 'format' => '0.00'),
                        'D' => array('value' => isset($value['carga1']['litros']) ? $value['carga1']['litros'] : '---', 'format' => '0.00'),
                        'E' => array('value' => isset($value['carga1']['diferencia']) ? $value['carga1']['diferencia'] : 0, 'format' => '0.00'),
                        'F' => array('value' => isset($value['carga2']['fecha']) ? $value['carga2']['fecha'] : '---'),
                        'G' => array('value' => isset($value['carga2']['lugar']) ? $value['carga2']['lugar'] : '---'),
                        'H' => array('value' => isset($value['carga2']['litros']) ? $value['carga2']['litros'] : 0, 'format' => '0.00'),
                        'I' => array('value' => isset($value['historial']['distancia']) ? $value['historial']['distancia'] : 0, 'format' => '0.00'),
                        'J' => array('value' => isset($value['historial']['velocidad_promedio']) ? $value['historial']['velocidad_promedio'] : 0, 'format' => '0.00'),
                        'K' => array('value' => isset($value['historial']['rendimiento']) ? $value['historial']['rendimiento'] : 0, 'format' => '0.00'),
                        'L' => array('value' => isset($value['carga1']['rendimiento_teorico']) ? $value['rendimiento_teorico'] : 0, 'format' => '0.00'),
                    ));
                    $i++;
                }

                //  die('////<pre>' . nl2br(var_export($totales, true)) . '</pre>////');
                $xls->setRowTotales($i, array(
                    'A' => array('value' => ''),
                    'B' => array('value' => 'TOTALES==>'),
                    'C' => array('value' => $totales['totalEquipo'], 'format' => '0.00'),
                    'D' => array('value' => $totales['totalSistema'], 'format' => '0.00'),
                    'E' => array('value' => $totales['totalDiferencia'], 'format' => '0.00'),
                    'F' => array('value' => ''),
                    'G' => array('value' => ''),
                    'H' => array('value' => $totales['litros'], 'format' => '0.00'),
                    'I' => array('value' => $totales['distancia'], 'format' => '0.00'),
                    'J' => array('value' => $totales['velocidad_promedio'], 'format' => '0.00'),
                    'K' => array('value' => $totales['rendimiento'], 'format' => '0.00'),
                ));
            } else {   //es para la flota.
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Cargas', 'width' => 20),
                    'C' => array('title' => 'Litros', 'width' => 20),
                    'D' => array('title' => 'Distancia (km)', 'width' => 20),
                    'E' => array('title' => 'Promedio (km/h)', 'width' => 20),
                    'F' => array('title' => 'Rendimiento (km/lt)', 'width' => 20),
                    'G' => array('title' => 'Rendimiento teorico', 'width' => 20),
                ));
                $i = 7;
                //                die('////<pre>' . nl2br(var_export($informe, true)) . '</pre>////');
                foreach ($informe as $key => $value) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $value['servicio_nombre']),
                        'B' => array('value' => $value['cantidad'], 'format' => '0.00'),
                        'C' => array('value' => $value['litros'], 'format' => '0.00'),
                        'D' => array('value' => $value['distancia'], 'format' => '0.00'),
                        'E' => array('value' => $value['velocidad_promedio'], 'format' => '0.00'),
                        'F' => array('value' => $value['rendimiento'], 'format' => '0.00'),
                        'G' => array('value' => $value['rendimiento_teorico'], 'format' => '0.00',),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=rendimientocombustible.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
