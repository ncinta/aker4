<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organizacion;
use App\Entity\PuntoCarga;
use App\Entity\Referencia;
use App\Form\fuel\InformeDespachoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\LoggerManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;
use App\Model\app\BackendManager;
use App\Model\app\UtilsManager;
use App\Model\fuel\PuntoCargaManager;

/**
 * Description of InformeDespachoController
 *
 * @author nicolas
 */
class InformeDespachoController extends AbstractController
{

    protected $user = null;
    protected $loggerManager;
    protected $organizacionManager;
    protected $combustibleManager;
    protected $bread;
    protected $userlogin;
    protected $referenciaManager;
    protected $gmapsManager;
    protected $excelManager;
    protected $backendManager;
    protected $puntocargaManager;
    protected $utilsManager;

    public function __construct(
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacion,
        UserLoginManager $userlogin,
        ReferenciaManager $referenciaManager,
        GmapsManager $gmapsManager,
        UtilsManager $utilsManager,
        CombustibleManager $combustibleManager,
        PuntoCargaManager $puntocargaManager,
        ExcelManager $excelManager,
        BackendManager $backendManager,
        LoggerManager $logger
    ) {
        $this->combustibleManager = $combustibleManager;
        $this->bread = $breadcrumbManager;
        $this->organizacionManager = $organizacion;
        $this->userlogin = $userlogin;
        $this->referenciaManager = $referenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->excelManager = $excelManager;
        $this->backendManager = $backendManager;
        $this->loggerManager = $logger;
        $this->utilsManager = $utilsManager;
        $this->puntocargaManager = $puntocargaManager;
    }

    /**
     * Lista todas los despacho de combustible
     * @Route("/fuel/{id}/informe/despacho", name="fuel_informe_despacho",
     *     requirements={"id": "\d+"},
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function despachoAction(Request $request, Organizacion $organizacion): Response
    {
        // Obtener usuario relacionado con la organización
        $this->user = $this->getUser2Organizacion($organizacion);

        // Obtener estaciones y servicios de punto de carga
        $estaciones = $this->referenciaManager->findEstacionesMapa();
        $serviciosPuntoCarga = $this->getServiciosPuntoCarga($organizacion);

        // Combinar estaciones con los servicios de punto de carga
        $estaciones = array_merge($estaciones, $serviciosPuntoCarga);

        // Crear el formulario
        $options = ['estaciones' => $organizacion->getPuntocargas()];
        $form = $this->createForm(InformeDespachoType::class, null, $options);

        // Generar mapa
        $mapa = $this->createMapa();

        // Configurar breadcrumb
        $this->bread->pushPorto($request->getRequestUri(), "Informe Despacho");

        // Renderizar la plantilla
        return $this->render('fuel/InformeDespacho/despacho.html.twig', [
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'mapa' => $mapa,
        ]);
    }

    private function getServiciosPuntoCarga(Organizacion $organizacion): array
    {
        $puntocargas = $organizacion->getPuntocargas()->toArray(); // Convertir a array
        return array_filter(
            $puntocargas,
            fn($puntoCarga) => $puntoCarga->getServicio() !== null
        );
    }


    private function createMapa()
    {
        //este mapa es para ver los lugares donde se cargo.
        $mapa = $this->gmapsManager->createMap(true);
        $mapa->setSize('100%', '300px');
        $mapa->setIniZoom(16);
        $mapa->setIniLatitud($this->userlogin->getOrganizacion()->getLatitud());
        $mapa->setIniLongitud($this->userlogin->getOrganizacion()->getLongitud());
        $referencias = $this->referenciaManager->findAsociadas();

        foreach ($referencias as $referencia) {
            //agrego las referencias
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, true);
        }
        return $mapa;
    }

    /**
     * Genera el informe de despacho de combustible.
     * @Route("/fuel/{id}/info/despacho/generar", name="fuel_info_despacho_generar",
     *     requirements={"id": "\d+"},
     *     options={"expose": true}
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function generarAction(Request $request, Organizacion $organizacion): Response
    {
        $this->user = $this->getUser2Organizacion($organizacion);

        // Procesar los datos del formulario
        $formData = $this->parseFormData($request->get('formDespacho'));
        if (!$formData) {
            return $this->json(['status' => 'falla'], 400);
        }

        // Obtener referencias
        $estaciones = $this->referenciaManager->findEstacionesMapa();
        $tipoCombustible = $this->getTipoCombustible($formData['tipoCombustible']);
        $puntoCarga = $this->getPuntoCarga($formData['estacion']);

        // Actualizar datos en la consulta
        $formData['tipoCombustiblestr'] = $tipoCombustible ? $tipoCombustible->getNombre() : 'Todos los tipos de combustibles';
        $formData['estacionstr'] = $puntoCarga ? $puntoCarga->getNombre() : 'Todas las estaciones';

        // Generar el informe        
        $informe = $this->armarInforme($formData, $organizacion, $puntoCarga, $estaciones, $tipoCombustible);

        // Generar el mapa
        $mapa = $this->createMapa();

        // Renderizar la vista y devolver respuesta
        $html = $this->renderView('fuel/InformeDespacho/pantalla.html.twig', [
            'organizacion' => $organizacion,
            'consulta' => $formData,
            'informe' => $informe,
            'mapa' => $mapa,
        ]);

        return $this->json(['status' => 'ok', 'html' => $html], 200);
    }

    /**
     * Parsear los datos del formulario enviados como string.
     */
    private function parseFormData(?string $formDespacho): ?array
    {
        if (!$formDespacho) {
            return null;
        }

        parse_str($formDespacho, $parsedData);
        return $parsedData['informedespacho'] ?? null;
    }

    /**
     * Obtiene el tipo de combustible según el ID.
     */
    private function getTipoCombustible(?string $tipoCombustibleId)
    {
        return $tipoCombustibleId
            ? $this->combustibleManager->findTipoCombustible($tipoCombustibleId)
            : null;
    }

    /**
     * Obtiene el punto de carga según el ID.
     */
    private function getPuntoCarga(?string $estacionId): ?PuntoCarga
    {
        return $estacionId && $estacionId !== "0"
            ? $this->puntocargaManager->findById($estacionId)
            : null;
    }


    public function armarInforme($consulta, $organizacion, $ptoCarga, $estaciones, $tipoCombustible): array
    {
        // Obtener los IDs de las estaciones
        $idsEstaciones = $this->getIdsEstaciones($estaciones);

        // Obtener las cargas según el tipo de consulta
        [$cargasPuntos, $cargasEstacion] = $this->obtenerCargas($consulta, $organizacion, $ptoCarga, $idsEstaciones, $tipoCombustible);

        // Construir el informe base
        $informe = $this->armarArray($cargasPuntos, $cargasEstacion);

        // Calcular totales por clave
        $this->calcularTotales($informe);

        // Calcular los totales generales
        $informe['final'] = $this->calcularTotalesFinales($informe);

        return $informe;
    }

    /**
     * Obtiene las cargas según el tipo de consulta.
     */
    private function obtenerCargas($consulta, $organizacion, $ptoCarga, $idsEstaciones, $tipoCombustible): array
    {
        $cargasPuntos = [];
        $cargasEstacion = [];

        if (is_null($ptoCarga)) {
            // Traer todas las cargas de puntos
            $cargasPuntos = $this->getCargasByPunto($consulta, $organizacion, null, $tipoCombustible);
        } elseif ($ptoCarga instanceof PuntoCarga) {
            // Traer cargas solo para el punto de carga específico
            $cargasPuntos = $this->getCargasByPunto($consulta, $organizacion, $ptoCarga, $tipoCombustible);
        } else {
            // Buscar cargas por estación
            $cargasEstacion = $this->getCargasByEstacion($consulta, $organizacion, null, $idsEstaciones, $tipoCombustible);
        }

        return [$cargasPuntos, $cargasEstacion];
    }

    /**
     * Calcula los totales por clave dentro del informe.
     */
    private function calcularTotales(array &$informe): void
    {
        foreach ($informe as $keyUno => &$grupo) {
            foreach ($grupo as $keyDos => &$detalles) {
                $totales = ['monto' => 0, 'litros' => 0, 'concordancia' => 0];

                foreach ($detalles as $keyTres => $valor) {
                    $totales['monto'] += $valor['monto'];
                    $totales['litros'] += $valor['litros'];
                    $totales['concordancia'] += $valor['concordancia'];
                }

                // Calcular concordancia promedio
                $cantidadReferencias = count($detalles) ?: 1;
                $totales['concordancia'] = $totales['concordancia'] / $cantidadReferencias * 100;

                $detalles['total'] = $totales;
            }
        }
    }

    /**
     * Calcula los totales generales para el informe.
     */
    private function calcularTotalesFinales(array $informe): array
    {
        $totalesFinales = ['monto' => 0, 'litros' => 0, 'concordancia' => 0];
        $cantidadGrupos = count($informe) - 1; // Excluir la clave 'final'

        foreach ($informe as $keyUno => $grupo) {
            foreach ($grupo as $keyDos => $detalles) {
                if (isset($detalles['total'])) {
                    $totalesFinales['monto'] += $detalles['total']['monto'];
                    $totalesFinales['litros'] += $detalles['total']['litros'];
                    $totalesFinales['concordancia'] += $detalles['total']['concordancia'] > 0
                        ? $detalles['total']['concordancia'] / max($cantidadGrupos, 1)
                        : 0;
                }
            }
        }

        return $totalesFinales;
    }

    /**
     * Su propósito es fusionar los datos de $cargasPuntos y $cargasEstacion en una única estructura $informe
     */
    private function armarArray(array $cargasPuntos, array $cargasEstacion): array
    {
        $informe = [];

        foreach ([$cargasPuntos, $cargasEstacion] as $cargas) {
            foreach ($cargas as $keyP => $valueP) {
                foreach ($valueP as $keyN => $valueN) {
                    foreach ($valueN as $keyF => $valueF) {
                        $informe[$keyP][$keyN][$keyF] = $valueF;
                    }
                }
            }
        }
        return $informe;
    }

    private function getTramaAnterior($carga)
    {
        $tramaAnterior = null;
        //trama anterior del servicio que se le realizo la carga

        if ($carga->getId_trama() !== null) {   //tengo trama cuando se hizo la carga
            //voy a buscar la tama puntualmente
            $tramaAnterior = $this->backendManager->trama($carga->getServicio()->getId(), $carga->getId_trama());
            $this->persistirTrama($carga, $tramaAnterior);
        } else {

            $tramaAnterior = $this->backendManager->obtenerTramaAnterior($carga->getServicio()->getId(), $carga->getFecha());

            $this->persistirTrama($carga, $tramaAnterior);
        }

        return $tramaAnterior;
    }

    private function armarNuevaTrama($trama)
    {
        $d = ['oid' => $trama['oid'], 'fecha' => $trama['fecha']];
        if (isset($trama['posicion'])) {
            $d['posicion'] = $trama['posicion'];
        }
        if (isset($trama['odometro'])) {
            $d['odometro'] = $trama['odometro'];
        }
        if (isset($trama['horometro'])) {
            $d['horometro'] = $trama['horometro'];
        }
        return $d;
    }

    /**
     * para una carga y una trama se hace la grabación de la trama en el json de Data sino existe ya la trama
     */
    private function persistirTrama($carga, $trama)
    {
        if ($trama) {
            $data = json_decode($carga->getData(), true);
            if (is_null($carga->getId_trama())) {
                //solo obtengo los datos que me interesan de la trama
                $data['trama'] = $this->armarNuevaTrama($trama);
                $carga->setData(json_encode($data));

                //actualizo datos de la carga segun la trama
                $fechaT = new \DateTime($trama['fecha']);

                $carga->setFechaTrama($fechaT);
                if (isset($trama['odometro']) && !is_null($trama['odometro'])) {
                    $carga->setOdometro((int) ($trama['odometro'] / 1000)); //odometro del servicio
                }
                if (isset($trama['oid'])) {
                    $carga->setId_trama($trama['oid']);
                }
                if (isset($trama['fecha'])) {
                    $carga->setFechaTrama(new \DateTime($trama['fecha']));
                }
                // en latitud/longitud se trae la posicion del vehiculo en el momento de ingreso de la carga
                if (isset($trama['posicion'])) {
                    $carga->setLatitud(floatval($trama['posicion']['latitud']));
                    $carga->setLongitud(($trama['posicion']['longitud']));
                }
                if (isset($data['horometro']) && !is_null($data['horometro']) && $data['horometro'] != '') {
                    $carga->setHorometroTablero(intval($data['horometro']));
                }

                $this->combustibleManager->update($carga);
            }
        }
    }

    private function getCargasByEstacion($consulta, $organizacion, $estacion, $ids, $tipoCombustible)
    {
        $cargasEstacion = $this->combustibleManager->findByEstacionYfecha(
            $consulta['desde'],
            $consulta['hasta'],
            $ids,
            $organizacion->getId(),
            $tipoCombustible,
            $estacion
        );

        $informe = [];
        $i = 1;

        foreach ($cargasEstacion as $carga) {
            if ($carga->getPuntoCarga() === null) {
                // Obtener latitud y longitud
                [$latitud, $longitud, $fechaTrama] = $this->obtenerUbicacionTrama($carga);

                // Calcular concordancia
                $concordancia = $this->referenciaManager->buscarCercaniaByDespacho(
                    $carga->getReferencia(),
                    $latitud,
                    $longitud
                );
                $arr = $this->formatearConcordancia($carga, $concordancia, $latitud, $longitud, $fechaTrama);

                $concord = $this->generarIconoConcordancia($concordancia, $arr);

                // Construir informe
                $informe[$carga->getReferencia()->getId()][$carga->getReferencia()->getNombre()][$carga->getFecha()->format('Y-m-d H:i:s') . $i] = [
                    'servicio' => $carga->getServicio()->getNombre(),
                    'odometro' => $carga->getOdometro() ?: '---',
                    'chofer' => $carga->getChofer() ? $carga->getChofer()->getNombre() : '---',
                    'idServicio' => $carga->getServicio()->getId(),
                    'fecha' => $carga->getFecha()->format('d/m/Y H:i'),
                    'monto' => $carga->getMontoTotal(),
                    'litros' => $carga->getLitrosCarga(),
                    'tipoCombustible' => $carga->getTipoCombustible() ? $carga->getTipoCombustible()->getNombre() : '---',
                    'latitud' => $latitud,
                    'modoingreso' => $carga->getModoIngreso(),
                    'verMapa' => $arr['verMapa'] ?? false,
                    'arrVerMapa' => $arr['arrVerMapa'] ?? null,
                    'longitud' => $longitud,
                    'concordancia' => $concordancia[0]['adentro'],
                    'concordanciastr' => $arr['concordanciastr'] ?? '---',
                    'concordanciaicon' => $concord,
                ];
                $i++;
            }
        }

        return $informe;
    }


    /**
     * Obtiene las cargas de combustible realizadas en uno o varios puntos de carga, según ciertos criterios, y las organiza en una estructura que puede usarse para informes o visualización.
     */
    private function getCargasByPunto($consulta, $organizacion, $ptoCarga, $tipoCombustible): array
    {
        // Convertir fechas a formato SQL
        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);

        // Obtener cargas desde el manager
        $cargasPuntos = $this->combustibleManager->findByPuntoYfecha(
            $desde,
            $hasta,
            $organizacion->getId(),
            $tipoCombustible,
            $ptoCarga
        );

        // Inicializar el informe
        $informe = [];

        foreach ($cargasPuntos as $carga) {
            // Procesar la información básica de la carga
            $data = $this->procesarCargaBasica($carga);

            // Procesar concordancia si aplica
            if ($tramaAnterior = $this->getTramaAnterior($carga)) {
                $data = array_merge($data, $this->procesarConcordancia($carga, $tramaAnterior));
            }

            // Organizar datos en la estructura de informe
            $puntoCargaId = $carga->getPuntoCarga()->getId();
            $puntoCargaNombre = $carga->getPuntoCarga()->getNombre();
            $informe[$puntoCargaId][$puntoCargaNombre][] = $data;
        }

        return $informe;
    }

    /** arma la estructura basica del array */
    private function procesarCargaBasica($carga): array
    {
        $centroCosto = $this->combustibleManager->obtenerCentroCosto($carga);
        return [
            'servicio' => $carga->getServicio()->getNombre(),
            'idServicio' => $carga->getServicio()->getId(),
            'odometro' => $carga->getOdometro() ?: '---',
            'chofer' => $carga->getChofer()?->getNombre() ?: '---',
            'fecha' => $carga->getFecha()->format('d/m/Y H:i:s'),
            'monto' => $carga->getMontoTotal(),
            'litros' => $carga->getLitrosCarga(),
            'tipoCombustible' => $carga->getTipoCombustible()?->getNombre() ?: '---',
            'modoingreso' => $carga->getModoIngreso(),
            'verMapa' => false,
            'arrVerMapa' => null,
            'concordancia' => false,
            'concordanciastr' => '---',
            'concordanciaicon' => null,
            'centroCosto' => $centroCosto != null ? $centroCosto->getNombre() : '',
        ];
    }

    private function procesarConcordancia($carga, $tramaAnterior): array
    {
        $tramaPuntoCarga = $carga->getPuntoCarga()->getServicio()
            ? $this->backendManager->obtenerTramaAnterior($carga->getPuntoCarga()->getServicio()->getId(), $carga->getFecha())
            : 0;

        $concordancia = $this->referenciaManager->buscarCercaniaByDespachoPuntoCarga(
            $carga->getPuntoCarga(),
            $tramaAnterior,
            $tramaPuntoCarga
        );

        $arrConcordancia = $this->formatearConcordanciaByPunto(
            $carga,
            $concordancia,
            $tramaAnterior,
            $carga->getFechaTrama(),
            (new \DateTime($tramaAnterior['fecha']))->format('d/m/Y H:i')
        );

        return [
            'latitud' => $carga->getLatitud(),
            'longitud' => $carga->getLongitud(),
            'verMapa' => $arrConcordancia['verMapa'] ?? false,
            'arrVerMapa' => $arrConcordancia['arrVerMapa'] ?? null,
            'concordancia' => $concordancia[0]['adentro'],
            'concordanciastr' => $arrConcordancia['concordanciastr'] ?? '---',
            'concordanciaicon' => $concordancia[0]['adentro']
                ? '<i class="icon-ok"></i>'
                : '<i class="fa fa-times">' . ($arrConcordancia['concordanciastr'] ?? '') . '</i>',
        ];
    }

    private function obtenerUbicacionTrama($carga)
    {
        $latitud = $carga->getLatitud();
        $longitud = $carga->getLongitud();
        $fechaTrama = $carga->getFechaTrama()?->format('Y-m-d H:i:s');

        if ($latitud === null || $longitud === null) {
            $tramaAnterior = $this->getTramaAnterior($carga);
            $latitud = $tramaAnterior['posicion']['latitud'] ?? null;
            $longitud = $tramaAnterior['posicion']['longitud'] ?? null;
            $fechaTrama = $tramaAnterior['fecha'] ?? null;
        }

        return [$latitud, $longitud, $fechaTrama];
    }

    private function generarIconoConcordancia($concordancia, $arr)
    {
        if ($concordancia[0]['adentro']) {
            return '<i class="icon-ok"></i>';
        }
        return '<i class="fa fa-times">' . ($arr['concordanciastr'] ?? '') . '</i>';
    }


    private function formatearConcordanciaByPunto($carga, $concordancia, $tramaAnterior, $fechaTramaAnterior, $FechaTrama)
    {
        $verMapa = false;
        $arrVerMapa = array();
        $fMayor = clone $carga->getFecha();
        $fMenor = clone $carga->getFecha();
        if (!is_null($carga->getPuntoCarga()->getReferencia()) || !is_null($carga->getPuntoCarga()->getServicio())) {
            if (
                $fechaTramaAnterior > $fMayor->modify('- 12 hour') &&
                $fechaTramaAnterior < $fMenor->modify('+ 12 hour')
            ) {
                if (isset($tramaAnterior['posicion'])) {
                    //$concordanciastr = ' Se encontró a ' . $concordancia[0]['strdistancia'] . ' con fecha ' . $fechaTramaAnterior->format('H:i:s');
                    $concordanciastr = ' El ' .  $fechaTramaAnterior->format('d/m') . ' a las ' . $fechaTramaAnterior->format('H:i:s') . ' se encontró a ' . $concordancia[0]['strdistancia'] . ' del pto de carga';
                    $arrVerMapa = array('latitud' => $tramaAnterior['posicion']['latitud'], 'longitud' => $tramaAnterior['posicion']['longitud']);
                    $verMapa = true;
                } else {
                    $concordanciastr = ' Sin Posición';
                }
            } else {
                $concordanciastr = ' No reporta';
            }
        } else {
            $concordanciastr = ' Sin posición. No reporta';
        }
        return array('concordanciastr' => $concordanciastr, 'verMapa' => $verMapa, 'arrVerMapa' => $arrVerMapa);
    }

    private function formatearConcordancia($carga, $concordancia, $latitud, $longitud, $tramaFecha)
    {
        $verMapa = false;
        $arrVerMapa = [];
        $concordanciastr = 'Sin posición. No reporta'; // Valor por defecto

        // Verificamos que la trama y las coordenadas sean válidas
        if ($tramaFecha && $latitud !== null && $longitud !== null) {
            $tramaFecha = new \DateTime($tramaFecha);
            $fechaTrama = $tramaFecha->format('d/m/Y H:i');

            // Clonamos la fecha de la carga para modificarla sin afectar el objeto original
            $fMayor = clone $carga->getFecha();
            $fMenor = clone $carga->getFecha();

            // Calculamos las fechas con los márgenes
            $fMayor->modify('-12 hours');
            $fMenor->modify('+20 hours');

            // Verificamos si la fecha de la trama está dentro del rango
            if ($tramaFecha > $fMayor && $tramaFecha < $fMenor) {
                // Si las coordenadas están disponibles
                if ($latitud !== null && $longitud !== null) {
                    $concordanciastr = 'Se encontró a ' . $concordancia[0]['strdistancia'] . ' con fecha ' . $fechaTrama;
                    $arrVerMapa = ['latitud' => $latitud, 'longitud' => $longitud];
                    $verMapa = true;
                } else {
                    $concordanciastr = 'Sin Posición';
                }
            } else {
                $concordanciastr = 'No reporta';
            }
        }

        return [
            'concordanciastr' => $concordanciastr,
            'verMapa' => $verMapa,
            'arrVerMapa' => $arrVerMapa,
        ];
    }


    public function getIdsEstaciones($estaciones)
    {
        $ids = array();
        foreach ($estaciones as $estacion) {
            $ids[] = $estacion->getId();
        }
        return $ids;
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/informe/despacho/exportar", name="fuel_informe_despacho_exportar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function exportarAction(Request $request, Organizacion $organizacion, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }

        if (isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Cargas");
            $xls->setHeaderInfo(array(
                'C2' => 'Punto de Despacho',
                'D2' => $consulta['estacionstr'],
                'C3' => 'Desde',
                'D3' => $consulta['desde'],
                'C4' => 'Hasta',
                'D4' => $consulta['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Punto de Despacho', 'width' => 40),
                'B' => array('title' => 'Servicio', 'width' => 40),
                'C' => array('title' => 'Odómetro', 'width' => 40),
                'D' => array('title' => 'Chofer', 'width' => 40),
                'E' => array('title' => 'Fecha', 'width' => 20),
                'F' => array('title' => 'Combustible', 'width' => 20),
                'G' => array('title' => 'Monto', 'width' => 20),
                'H' => array('title' => 'Litros', 'width' => 20),
                'I' => array('title' => 'Concordancia tanque/punto GPS', 'width' => 40),
                'J' => array('title' => 'Centro de Costo', 'width' => 20),
            ));
            $i = 7;
            foreach ($informe as $id => $arr) {
                foreach ($arr as $nombre => $subarr) {
                    if ($nombre != 'monto' && $nombre != 'litros' && $nombre != 'concordancia') {
                        foreach ($subarr as $key => $value) {
                            if ($key !== 'total') {
                                $xls->setRowValues($i, array(
                                    'A' => array('value' => $nombre),
                                    'B' => array('value' => $value['servicio']),
                                    'C' => array('value' => $value['odometro']),
                                    'D' => array('value' => $value['chofer']),
                                    'E' => array('value' => $value['fecha']),
                                    'F' => array('value' => $value['tipoCombustible']),
                                    'G' => array('value' => $value['monto']),
                                    'H' => array('value' => $value['litros']),                                    
                                    'I' => array('value' => ($value['concordancia'] ? 'SI' : 'NO') . ' ' . !($value['concordancia']) ? $value['concordanciastr'] : ''),
                                    'J' => array('value' => $value['centroCosto']),
                                ));
                                $i++;
                            } else {
                                $xls->setRowValues($i, array(
                                    'A' => array('value' => 'Total'),
                                    'B' => array('value' => ''),
                                    'C' => array('value' => ''),
                                    'D' => array('value' => ''),
                                    'E' => array('value' => ''),
                                    'F' => array('value' => ''),
                                    'G' => array('value' => $subarr['total']['monto']),
                                    'H' => array('value' => $subarr['total']['litros']),
                                    'I' => array('value' => $subarr['total']['concordancia']),
                                ));
                                $i++;
                            }
                        }
                    }
                }
            }
            $xls->setRowValues($i, array(
                'A' => array('value' => 'Total Final'),
                'B' => array('value' => ''),
                'C' => array('value' => ''),
                'D' => array('value' => ''),
                'E' => array('value' => ''),
                'F' => array('value' => ''),
                'G' => array('value' => $informe['final']['monto']),
                'H' => array('value' => $informe['final']['litros']),
                'I' => array('value' => $informe['final']['concordancia']),
            ));
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=despachos.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
    }

    private function getUser2Organizacion($organizacion)
    {
        $user = $this->userlogin->getUser();
        if ($organizacion != $this->userlogin->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }
}
