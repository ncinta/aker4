<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleGraficoType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\UtilsManager;

class GraficosController extends AbstractController
{

    private $servicioManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $backendManager;
    private $tanqueManager;

    public function __construct(
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        BackendManager $backendManager,
        TanqueManager $tanqueManager
    ) {
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->backendManager = $backendManager;
        $this->tanqueManager = $tanqueManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\CargaCombustible');
    }

    /**
     * @Route("/fuel/{id}/grafico", name="fuel_grafico",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_GRAFICO_CONSUMO")
     */
    public function graficoAction(Request $request, Organizacion $organizacion)
    {
        $options['servicios'] = $this->servicioManager->findByCanbus($organizacion);
        $form = $this->createForm(CombustibleGraficoType::class, null, $options);

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Grafico de Consumo");
        return $this->render('fuel/Combustible/grafico.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
        ));
    }

    /**
     * @Route("/fuel/{id}/grafico/generar", name="fuel_grafico_generar",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  options={"expose": true},
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_GRAFICO_CONSUMO")
     */
    public function generarAction(Request $request, Organizacion $organizacion)
    {
        $tmp = array();
        parse_str($request->get('formGrafico'), $tmp);
        $data = $tmp['graficoconsumo'];

        $html = null;
        $consulta = array(
            'servicio' => $data['servicio'],
            'desde' => $this->utilsManager->datetime2sqltimestamp($data['desde'], false),
            'hasta' => $this->utilsManager->datetime2sqltimestamp($data['hasta'], true),
            'tipo' => intval($data['tipo']),
        );
        if ($consulta) {
            $servicio = $this->servicioManager->find(intval($consulta['servicio']));
            $consulta['servicionombre'] = $servicio->getNombre();

            switch ($consulta['tipo']) {
                case '1':
                    $data = $this->getGraficoPorcentaje($servicio, $consulta['desde'], $consulta['hasta']);                   
                   // dd($data);
                    $html =  $this->renderView(
                        'fuel/Combustible/grafico_porcentaje.html.twig',
                        array(
                            'consulta' => $consulta,
                            'dt' => $data,
                            'tipoMedidor' =>  count($data) > 1 ? $this->servicioManager->getStrTipoMedidor($servicio) : '',
                            'mensaje' => count($data) > 1 ? '' : 'Sin datos, verifique la configuración de combustible del servicio.',
                            //'dLts' => $dLts->toStrictArray(),
                        )
                    );
                    break;
                case '2':
                    $data = $this->getGraficoLitros($servicio, $consulta['desde'], $consulta['hasta']);
                    $html =  $this->renderView(
                        'fuel/Combustible/grafico_litros.html.twig',
                        array(
                            'consulta' => $consulta,
                            'dt' => $data,
                            'tipoMedidor' =>  count($data) > 1 ? $this->servicioManager->getStrTipoMedidor($servicio) : '',
                            'mensaje' => count($data) > 1 ? '' : 'Sin datos, verifique la configuración de combustible del servicio.',

                        )
                    );
                    break;
                case '3':
                    $data = $this->getGraficoLitrosDistancia($servicio, $consulta['desde'], $consulta['hasta']);
                    $html =  $this->renderView(
                        'fuel/Combustible/grafico_ltsdist.html.twig',
                        array(
                            'consulta' => $consulta,
                            'dt' => $data,
                            'tipoMedidor' =>  count($data) > 1 ? $this->servicioManager->getStrTipoMedidor($servicio) : '',
                            'mensaje' => count($data) > 1 ? '' : 'Sin datos, verifique la configuración de combustible del servicio.',
                        )
                    );
                    break;
                case '4':
                    $data = $this->getGraficoLitrosTiempo($servicio, $consulta['desde'], $consulta['hasta']);
                    $html =  $this->renderView(
                        'fuel/Combustible/grafico_ltstiempo.html.twig',
                        array(
                            'consulta' => $consulta,
                            'dt' => $data,
                            'tipoMedidor' =>  count($data) > 1 ? $this->servicioManager->getStrTipoMedidor($servicio) : '',
                            'mensaje' => count($data) > 1 ? '' : 'Sin datos, verifique la configuración de combustible del servicio.',
                        )
                    );
                    break;
            }
           // dd($data);
            return new Response(json_encode(array('status' => 'ok', 'html' => $html)), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    private function getGraficoLitrosDistancia($servicio, $desde, $hasta)
    {
        if ($servicio->getMedidorCombustible()) {   //siempre que tenga mediciones de combustible.
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }
        $historial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), array());

        $historial['capacidadTanque'] =  $servicio->getTipoMedidorCombustible() == 2 && $servicio->getVehiculo() ? $servicio->getVehiculo()->getLitrosTanque() * 1.5 : null;
        $consumo = $this->filtrarHistorial($historial, true, $servicio->getMedicionCombustible());
        $myArray = array();
        $i = 0;
        $distAcum = 0;
        if (count($consumo) > 0) {
            foreach ($consumo as $value) {
                $distAcum += isset($value['distancia']) ? $value['distancia'] : 0;
                if (isset($isMedicionesLoad) && $isMedicionesLoad && isset($value['tanque'])) {
                    $medicion = $this->tanqueManager->getCapacidad($value['tanque'], $servicio);

                    $myArray[$i]['distancia'] = $distAcum > 0 ? (int) number_format($distAcum / 1000) : 0;
                    $valor = $servicio->getTipoMedidorCombustible() == 1 ? $medicion['porcentaje'] : $medicion['litros'];
                    $myArray[$i]['litros'] = $this->servicioManager->getLtrtanque($servicio, preg_replace("/[^-0-9\.]/", "", intval($valor)));
                    $i++;
                } else {
                    //$myArray[0]['fecha'] = 'NaN';
                    //$myArray[0]['distancia'] = 'NaN';
                    //$myArray[0]['litros'] = 0;
                }
            }
        } else {
            $myArray[0]['distancia'] = 'NaN';
            $myArray[0]['litros'] = 0;
        }

        return $myArray;
    }

    private function getGraficoLitrosTiempo($servicio, $desde, $hasta)
    {
        if ($servicio->getMedidorCombustible()) {   //siempre que tenga mediciones de combustible.
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }
        $historial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), array());
        $historial['capacidadTanque'] =  $servicio->getTipoMedidorCombustible() == 2 && $servicio->getVehiculo() ? $servicio->getVehiculo()->getLitrosTanque() * 1.5 : null;
        $consumo = $this->filtrarHistorial($historial, true, $servicio->getMedicionCombustible());        
        $i = 0;
        $segAcum = 0;
        $myArray = array();
        if (count($consumo) > 0) {
            foreach ($consumo as $value) {
                $segAcum += $value['tiempo'];
                if (isset($isMedicionesLoad) && $isMedicionesLoad && isset($value['tanque'])) {
                    $medicion = $this->tanqueManager->getCapacidad($value['tanque'], $servicio);                    
                    
                    $myArray[$i]['tiempo'] = $segAcum/60;
                    $valor = $servicio->getTipoMedidorCombustible() == 1 ? $medicion['porcentaje'] : $medicion['litros'];
                    $myArray[$i]['litros'] = $this->servicioManager->getLtrtanque($servicio, preg_replace("/[^-0-9\.]/", "", intval($valor)));
                    $i++;
                } else {
                    //no debo poner nada.                    
                }
            }
        } else {
            $myArray[0]['fecha'] = 'NaN';
            $myArray[0]['litros'] = 0;
        }
     //   dd($myArray);
        return $myArray;
    }

    private function getGraficoLitros($servicio, $desde, $hasta)
    {
        if ($servicio->getMedidorCombustible()) {   //siempre que tenga mediciones de combustible.
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }
        $historial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), array());
        // lo usamos para verificar los litros del can y varilla para que no traigan mayores a eso
        $historial['capacidadTanque'] =  $servicio->getTipoMedidorCombustible() == 2 && $servicio->getVehiculo() ? $servicio->getVehiculo()->getLitrosTanque() * 1.5 : null;

        $consumo = $this->filtrarHistorial($historial, true, $servicio->getMedicionCombustible());
        $i = 0;
        $myArray = array();
        if (count($consumo) > 0) {
            foreach ($consumo as $value) {                
                if (isset($isMedicionesLoad) && $isMedicionesLoad && isset($value['tanque']) && $value['tanque'] != null) {
                    $medicion = $this->tanqueManager->getCapacidad($value['tanque'], $servicio);
                    $myArray[$i]['fecha'] = $this->parseFecha($value['fecha']);                    
                    $valor = $servicio->getTipoMedidorCombustible() == 1 ? $medicion['porcentaje'] : $medicion['litros'];
                    $myArray[$i]['litros'] = $this->servicioManager->getLtrtanque($servicio, preg_replace("/[^-0-9\.]/", "", intval($valor)));
                    $i++;
                } else {
                   // $myArray[0]['fecha'] = 'NaN';
                   // $myArray[0]['distancia'] = 'NaN';
                   // $myArray[0]['litros'] = 0;
                }
            }
        } else {
           // $myArray[0]['fecha'] = 'NaN';
           // $myArray[0]['litros'] = 0;
        }
       // dd($myArray);
        return  $myArray;
    }

    private function parseFecha($fecha) {
        $dt = new \DateTime($fecha);
        $arrFecha = [
            'd' => $dt->format('d'),
            'm' => $dt->format('m') - 1,    //javascript toma meses desde 0 a 11.. per jodere noma
            'Y' => $dt->format('Y'),
            'H' => $dt->format('H'),
            'i' => $dt->format('i'),
            's' => $dt->format('s'),
            'str' => $dt->format('d/m H:i:s'),
        ];
        return $arrFecha;
    }

    private function getGraficoPorcentaje($servicio, $desde, $hasta)
    {
        $mensaje = '';
        $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        //    die('////<pre>' . nl2br(var_export($servicio->getId(), true)) . '</pre>////');
        $historial = $this->backendManager->historial($servicio->getId(), $desde, $hasta, array(), array());
        $historial['capacidadTanque'] =  $servicio->getTipoMedidorCombustible() == 2 && $servicio->getVehiculo() ? $servicio->getVehiculo()->getLitrosTanque() * 1.5 : null;
        $consumo = $this->filtrarHistorial($historial, true, $servicio->getMedicionCombustible());
        unset($historial);
        //proceso los datos para armar la grafica del porcentaje
        $i = 0;
        $myArray = array();
        if (count($consumo) > 0) {           
            foreach ($consumo as $value) {                
                $fecha = $this->parseFecha($value['fecha']);
                if (isset($isMedicionesLoad) && $isMedicionesLoad && isset($value['tanque']) && $value['tanque'] != null) {
                    if (isset($value['tanque'])) {                        
                        $myArray[$i]['fecha'] = $fecha;
                        $myArray[$i]['litros'] = $this->servicioManager->getPorcentajeTanque($servicio, intval($value['tanque']));
                        $i++;
                    }
                } elseif ($servicio->getCanbus()) {   //el servico tiene canbus, confiemos en ese dato
                    if (isset($value['tanque'])) {                        
                        $myArray[$i]['fecha'] = $fecha;
                        $myArray[$i]['litros'] = intval($value['tanque']);  //el valor es en 100% siempre en el canbus
                        $i++;
                    }
                } else {                 
                   // $myArray[0]['fecha'] = 'NaN';
                   // $myArray[0]['distancia'] = 'NaN';
                   // $myArray[0]['litros'] = 0;
                }
            }
        } else {          
          //  $myArray[0]['fecha'] = null;
          //  $myArray[0]['litros'] = 0;
        }
        return $myArray;
    }

    private function filtrarHistorial($historial, $filtrar = true, $canbus = false)
    {
        //   die('////<pre>'.nl2br(var_export($canbus, true)).'</pre>////');
        $consumo = array();
        if (!is_null($historial)) {
            reset($historial);
            while ($trama = current($historial)) {
                
                next($historial);
                if (!isset($trama['posicion']))   //descarto las que no tienen posicion.
                continue;
                 if (!isset($trama['distancia']) || $trama['distancia'] < 0)   //descarto las que no tengan distancia o distancia menor a 0
                continue;
                $tCC = array(
                    'fecha' => $trama['fecha'],
                    'contacto' => isset($trama['contacto']) ? $trama['contacto'] : false,
                    'velocidad' => $trama['posicion']['velocidad'],
                    'tanque' => null,
                    'real' => isset($trama['carcontrol']) ? $trama['carcontrol']['porcentaje_tanque'] * 1 : 0,
                    'distancia' => isset($trama['distancia']) ? $trama['distancia'] : 0,
                    'tiempo' => isset($trama['tiempo']) ? $trama['tiempo'] : 0,
                );

                //if ($canbus) {
                if (isset($trama['canbusData']) && isset($trama['canbusData']['nivelCombustible']) && $trama['canbusData']['nivelCombustible'] > 0) {
                    //validamos que los litros sean menor a la capacidad del tanque, si la cap del tanque es null que lo muestre de todas formas
                    if (intval($trama['canbusData']['nivelCombustible']) <= $historial['capacidadTanque'] || $historial['capacidadTanque'] == null) {
                        $tCC['tanque'] = intval($trama['canbusData']['nivelCombustible']);
                        $tCC['real'] = intval($trama['canbusData']['nivelCombustible']);
                    }
                    $filtar = true;
                }
                //                } else {
                //                    $tCC['tanque'] = ($tCC['real'] >= 1) ? $tCC['real'] : 0;  //limpio las mediciones < 1% porque no son validas
                //                }

                if ($filtrar) {
                    //ahora debo modificar las tramas que tengan valores y obtener una linea sin saltos para arriba.
                    if ($tCC['tanque'] != 0) {   //solo para las tramas con datos.                
                        if (!isset($tMin)) { //existe una 
                            $tMin = $tCC;
                        }

                        if ($tMin['tanque'] < $tCC['tanque'] && abs($tMin['tanque'] - $tCC['tanque']) > 10) {  //posible carga de combustible
                            //tengo una carga de combustible mayor de 10%
                            $tMin = $tCC;
                        }

                        //comparo el minimo que tengo con el minimo actual y reemplazo.
                        //              $tCC['tanque'] = min($tMin['tanque'], $tCC['tanque']);
                        // if ($tMin['tanque'] > $tCC['tanque']) {   ///   LO COMENTO PORQUE SE DEFASA DEL VALOR QUE MUESTRA EN EL HISTORIAL
                        //     $tMin = $tCC;   //ahora tomo el nuevo minimo.
                        // }
                    }
                }

                if ($canbus) {
                    if (isset($trama['canbusData'])) {
                        $consumo[] = $tCC;
                    }
                } else {
                    $consumo[] = $tCC;
                }
            }
        }
        // die('////<pre>'.nl2br(var_export($consumo, true)).'</pre>////');
        return $consumo;
    }
}

/**
 GraficosController.php on line 104:
array:2691 [
  0 => array:2 [
    "fecha" => "01/04 12:55"
    "litros" => 98
  ]
  1 => array:2 [
    "fecha" => "01/04 12:55"
    "litros" => 97
  ]
  2 => array:2 [
    "fecha" => "01/04 12:56"
    "litros" => 98
  ]
 
]

 */