<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleInformeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\GeocoderManager;

class InfoRendimientoCargasController extends AbstractController
{

    private $grupoServicioManager;
    private $servicioManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $combustibleManager;
    private $backendManager;
    private $referenciaManager;
    private $tanqueManager;
    private $geocoderManager;
    private $excelManager;

    public function __construct(
        GrupoServicioManager $grupoServicioManager,
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        CombustibleManager $combustibleManager,
        BackendManager $backendManager,
        ReferenciaManager $referenciaManager,
        TanqueManager $tanqueManager,
        GeocoderManager $geocoderManager,
        ExcelManager $excelManager
    ) {
        $this->grupoServicioManager = $grupoServicioManager;
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->combustibleManager = $combustibleManager;
        $this->backendManager = $backendManager;
        $this->referenciaManager = $referenciaManager;
        $this->tanqueManager = $tanqueManager;
        $this->geocoderManager = $geocoderManager;
        $this->excelManager = $excelManager;
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/{id}/info/rendimientocargas", name="fuel_info_rendimientocargas",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function informeAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Informe de Rendimiento");
        $options['servicios'] = $this->servicioManager->findEnabledByUsuario();
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $form = $this->createForm(CombustibleInformeType::class, null, $options);

        return $this->render('fuel/InfoRendimientoCargas/informe.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
        ));
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/{id}/info/rendimientocargas/generar", name="fuel_info_rendimientocargas_generar", requirements={"id": "\d+"}, options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function generarinformeAction(Request $request, Organizacion $organizacion)
    {
        $formData = $this->parseForm($request->get('formRendimiento'));
        $consulta = $formData['consumocombustible'];

        if (!$consulta) {
            return $this->jsonResponse('falla', null, 400);
        }

        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);

        // Verificación de servicio seleccionado
        if ($consulta['servicio'] == '-1') {
            $this->setFlash('error', 'No se seleccionó ningún servicio');
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $arr = $this->generarData($consulta, $desde, $hasta, $organizacion);

        $arr['consulta']['rendimiento'] = $arr['rendimiento'] ?? '---';
        $arr['consulta']['rendimientoHora'] = $arr['rendimientoHora'] ?? '---';
        $arr['consulta']['servicionombre'] = $this->obtenerNombreServicio($consulta);

        $html = $this->generateReportHtml($consulta, $arr);

        return $this->jsonResponse('ok', $html, 200);
    }

    /**
     * Parse form data from request.
     */
    private function parseForm($formData)
    {
        $tmp = [];
        parse_str($formData, $tmp);
        return $tmp;
    }

    /**
     * Generate HTML based on report type.
     */
    private function generateReportHtml(array $consulta, array $arr)
    {
        $consulta['total'] = true;
        return $this->renderView(
            'fuel/InfoRendimientoCargas/result_flota.html.twig',
            array_merge($arr, [
                'totales' => $arr['totales'] ?? null,
                'isCanbus' => $arr['isCanbus'] ?? false,
                'carga0' => $arr['carga0'] ?? null,
            ])
        );
    }

    public function generarData($consulta, $desde, $hasta)
    {
        $informe = [];

        // Obtener los servicios según el tipo de consulta
        $servicios = $this->obtenerServicios($consulta);

        foreach ($servicios as $servicio) {
            $cargas = $this->combustibleManager->findByServicioYfecha($servicio, $desde, $hasta);
            if (count($cargas) > 0) {
                $distancia = $this->calcularDistancia($cargas);
                $distanciaTablero = $this->calcularDistanciaTablero($cargas);
                $horas = $this->calcularHoras($servicio, $cargas);

                // Eliminar primera carga
                //unset($cargas[0]);

                // Cálculo de métricas totales para cada carga
                $resultados = $this->calcularMetricas($servicio, $cargas, $distancia, $distanciaTablero, $horas);
                if ($resultados['cantidad'] > 1) {
                    $detalle = $this->obtenerDetalleServicio($servicio, $cargas);   //obtengo el detalle de las cargas totales
                    $totales = $this->inicializarTotales();
                    $informe[$servicio->getNombre()] = [
                        'isCanbus' => $servicio->getCanbus(),
                        'resumen' => array_merge($resultados, [
                            'fecha_prim' => reset($cargas)->getFecha()->format('d/m H:i'),
                            'fecha_ult' => end($cargas)->getFecha()->format('d/m H:i'),
                        ]),
                        'detalle' => $detalle, // Aquí llamamos a una función que generará el detalle.
                        'totales' => $this->acumularParciales($totales, $detalle)
                    ];
                }
            }
        }
        // dd($informe);
        return ['informe' => $informe, 'consulta' => $consulta];
    }

    // Función para obtener los servicios
    private function obtenerServicios($consulta)
    {
        if ($consulta['servicio'] == '0') {
            return $this->servicioManager->findAll();
        } elseif (substr($consulta['servicio'], 0, 1) == 'g') {
            $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
            return $this->servicioManager->findSoloAsignadosGrupo($grupo);
        } else {
            $servicio = $this->servicioManager->find($consulta['servicio']);
            return [$servicio];
        }
    }

    // Función para generar el nombre del servicio o grupo
    private function obtenerNombreServicio($consulta)
    {
        if ($consulta['servicio'] == '0') {
            return 'Toda la Flota';
        } elseif (substr($consulta['servicio'], 0, 1) == 'g') {
            $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
            return $grupo->getNombre();
        } else {
            $servicio = $this->servicioManager->find($consulta['servicio']);
            return $servicio->getNombre();
        }
    }

    /**
     * Calcular distancia total
     */
    private function calcularDistancia($cargas)
    {
        return end($cargas)->getOdometro() - reset($cargas)->getOdometro();
    }

    /**
     * Calcular distancia total usando el odómetro del tablero
     */
    private function calcularDistanciaTablero($cargas)
    {
        $distanciaTablero = end($cargas)->getOdometroTablero() - reset($cargas)->getOdometroTablero();
        return $distanciaTablero < 0 ? abs($distanciaTablero) : $distanciaTablero;
    }

    /**
     * Calcular horas entre la primera y última trama
     */
    private function calcularHoras($servicio, $cargas)
    {
        $primTrama = $this->obtenerTrama($servicio->getId(), reset($cargas));
        $ultTrama = $this->obtenerTrama($servicio->getId(), end($cargas));

        if (isset($ultTrama['horometro']) && isset($primTrama['horometro'])) {
            return ((intval($ultTrama['horometro'])) - (intval($primTrama['horometro']))) / 60;
        }

        return 0;
    }

    /**
     * Calcular horas entre la primera y última trama
     */
    private function calcularHorasTablero($servicio, $cargas)
    {
        $primCarga =  reset($cargas);
        $ultCarga = end($cargas);
        if (!is_null($primCarga->getHorometroTablero()) && !is_null($ultCarga->getHorometroTablero())) {
            return (intval($ultCarga->getHorometroTablero()) - intval($primCarga->getHorometroTablero()));
        }

        return 0;
    }

    /**
     * Obtener trama anterior
     */
    private function obtenerTrama($servicioId, $consulta)
    {
        $fechaTrama = $consulta->getFechaTrama();
        $fecha = $fechaTrama ? $fechaTrama->modify("+1 second") : $consulta->getFecha();
        return $this->backendManager->obtenerTramaAnterior($servicioId, $fecha);
    }

    /**
     * Calcular métricas y totales
     */
    private function calcularMetricas($servicio, $cargas, $distancia, $distanciaTablero, $horas)
    {
        $litros = 0;
        $montoTotal = 0;
        $cantidad = 0;  // Se empieza en 1 por la eliminación del primer registro

        foreach ($cargas as $entrada) {
            $litros += $entrada->getLitrosCarga();
            $montoTotal += $entrada->getMontoTotal();
            $cantidad++;
        }

        // Cálculos de litros, kilómetros y horas
        $litrosKm = $this->calcularLitrosPorKm($litros, $distancia);
        $kmLitro = $this->calcularKmPorLitro($litros, $distancia);
        $litrosCienKm = $litrosKm * 100;

        $litrosKmTablero = $this->calcularLitrosPorKm($litros, $distanciaTablero);
        $kmLitroTablero = $this->calcularKmPorLitro($litros, $distanciaTablero);
        $litrosCienKmTablero = $litrosKmTablero * 100;

        $litrosHs = $this->calcularLitrosPorHoras($litros, $horas);
        $horasCienHs = $litrosHs * 100;

        // Cálculo adicional por teorico
        $consumoTeoricoPorKm = $this->calcularConsumoTeoricoKm($servicio, $distancia);
        $consumoTeoricoPorHs = $this->calcularConsumoTeoricoHs($servicio, $horas);
        
        $ralenti = 0;  // Esto puede variar si se obtiene de otro cálculo
        $consumoTeoricoRalenti = $this->calcularConsumoTeoricoRalenti($servicio, $ralenti);
        $consumoTeoricoMarcha = $consumoTeoricoPorKm + $consumoTeoricoRalenti;
        // los datos de hs y kms de tablero los acumulo desde lo parcilaes, porque no coinciden 
        return [
            'cantidad' => $cantidad,
            'litros' => $litros,
            'distancia' => $distancia,
            'horas' => $horas,
            'litrosKm' => $litrosKm,
            'kmLitro' => $kmLitro,
            'litrosCienKms' => $litrosCienKm,
            'consumoTeoricoPorKm' => $consumoTeoricoPorKm,
            'consumoTeoricoPorHs' => $consumoTeoricoPorHs,
            'ralenti' => $ralenti,
            'consumoTeoricoRalenti' => $consumoTeoricoRalenti,
            'consumoTeoricoMarcha' => $consumoTeoricoMarcha,
            'litrosHora' => $litrosHs,
            'horasCienLitros' => $horasCienHs,
        ];
    }

    /**
     * Cálculo de litros por kilómetro
     */
    private function calcularLitrosPorKm($litros, $distancia)
    {
        return ($litros > 0 && $distancia > 0) ? ($litros / $distancia) : 0;
    }

    /**
     * Cálculo de kilómetros por litro
     */
    private function calcularKmPorLitro($litros, $distancia)
    {
        return ($litros > 0 && $distancia > 0) ? ($distancia / $litros) : 0;
    }

    /**
     * Cálculo de litros por horas
     */
    private function calcularLitrosPorHoras($litros, $horas)
    {
        return ($litros > 0 && $horas > 0) ? ($litros / $horas) : 0;
    }

    /**
     * Cálculo del consumo teórico por kilómetro
     */
    private function calcularConsumoTeoricoKm($servicio, $distancia)
    {
        // Verificar si el servicio tiene un vehículo antes de acceder a sus métodos
        if ($servicio->getVehiculo() !== null) {
            return ($servicio->getVehiculo()->getRendimiento() !== null && $distancia > 0)
                ? $servicio->getVehiculo()->getRendimiento() * $distancia / 100
                : 0;
        }
        // Si el vehículo es null, retornar 0 o manejar el caso de otra manera
        return 0;
    }

    /**
     * Cálculo del consumo teórico por hora
     */
    private function calcularConsumoTeoricoHs($servicio, $horas)
    {
        // Verificar si el servicio tiene un vehículo antes de acceder a sus métodos
        if ($servicio->getVehiculo() !== null) {
            return ($servicio->getVehiculo()->getRendimiento() !== null && $horas > 0)
                ? $servicio->getVehiculo()->getRendimiento() * $horas / 100
                : 0;

            // Si el vehículo es null, retornar 0 o manejar el caso de otra manera
            return 0;
        }
    }

    /**
     * Cálculo del consumo teórico en ralenti
     */
    private function calcularConsumoTeoricoRalenti($servicio, $ralenti)
    {
        // Verificar si el servicio tiene un vehículo antes de acceder a sus métodos
        if ($servicio->getVehiculo() !== null) {
            return ($servicio->getVehiculo()->getRendimientoHora() !== null && $ralenti > 0)
                ? $servicio->getVehiculo()->getRendimientoHora() * $ralenti
                : 0;
        }
        return 0;
    }

    /**
     * Procesa las cargas y obtiene los detalles
     */
    public function obtenerDetalleServicio($servicio, $cargas)
    {
        $entrada = [];
        $totales = $this->inicializarTotales();

        $consulta_consumo = $this->obtenerConsultaConsumo($servicio, $cargas);

        foreach ($consulta_consumo as $key => $value) {
            if ($key + 1 < count($consulta_consumo)) {
                $entrada[$key] = $this->procesarEntrada($consulta_consumo, $key, $servicio);
            }
        }
        return $entrada;
    }

    private function inicializarTotales()
    {
        return [
            'count' => 0,
            'litros' => 0,
            'kilometros' => 0,
            'horas' => 0,
            'litrosHora' => 0,
            'horasCienLitros' => 0,
            'litrosKm' => 0,
            'litrosCienKms' => 0,
            'horasTablero' => 0,
            'litrosHoraTablero' => 0,
            'litrosCienHorasTablero' => 0,
            'kilometrosTablero' => 0,
            'litrosKmTablero' => 0,
            'litrosCienKmsTablero' => 0,
        ];
    }

    private function obtenerConsultaConsumo($servicio, $cargas)
    {
        $consulta_consumo = [];
        foreach ($cargas as $carga) {
            $tramaAnterior = !is_null($carga->getfechaTrama())
                ? $this->backendManager->obtenerTramaAnterior($servicio->getId(), $carga->getfechaTrama()->modify("+1 second"))
                : $this->backendManager->obtenerTramaAnterior($servicio->getId(), $carga->getFecha());

            $consulta_consumo[] = ['carga' => $carga, 'trama' => $tramaAnterior];
        }
        return $consulta_consumo;
    }

    private function procesarEntrada($cargas, $key, $servicio)
    {
        $entrada = [];
        $fecha1 = $cargas[$key]['carga']->getFecha();
        $entrada['carga1'] = $this->generarCarga($cargas[$key], $servicio);
        $entrada['carga2'] = $this->generarCarga($cargas[$key + 1], $servicio);

        $entrada['parcial'] = $this->calcularParcial($entrada, $key, $cargas, $servicio);

        return $entrada;
    }

    private function generarCarga($cargaData)
    {
        return [
            'tipoCarga' => !is_null($cargaData['carga']->getCargaCompleta()) ? ($cargaData['carga']->getCargaCompleta() ? 'Completa' : 'Parcial') : 'n/n',
            'fecha' => $cargaData['carga']->getFecha()->format('d/m H:i'),
            'fechaCompleta' => $cargaData['carga']->getFecha()->format('d/m/Y H:i:s'),
            'litros' => $cargaData['carga']->getLitrosCarga(),
            'horometro' => isset($cargaData['trama']['horometro']) ? intval($cargaData['trama']['horometro']) / 60 : 0,
            'odometro' => !is_null($cargaData['carga']->getOdometro()) ? $cargaData['carga']->getOdometro() : '0', //$cargaData['trama']['odometro'],
            'odometroTablero' => !is_null($cargaData['carga']->getOdometroTablero()) ? $cargaData['carga']->getOdometroTablero() : -1,
            'horometroTablero' => !is_null($cargaData['carga']->getHorometroTablero()) ? $cargaData['carga']->getHorometroTablero() : -1,
            //  'lugar' => $this->obtenerLugar($cargaData)
        ];
    }

    private function calcularParcial($entrada, $key, $cargas, $servicio)
    {

        $parcial = ['consumoTeoricoPorKm' => 0, 'consumoTeoricoRalenti' => 0, 'ralenti' => 0];
        $parcial['litros'] = floatval($cargas[($key + 1)]['carga']->getLitrosCarga());   //ojo no me cierra que el consumo se saque de la carga2
        $parcial['horas'] = intval($entrada['carga2']['horometro']) - intval($entrada['carga1']['horometro']);
        $parcial['kilometros'] = abs(intval($entrada['carga2']['odometro']) - intval($entrada['carga1']['odometro']));
        $parcial['litrosHora'] = $parcial['horas'] > 0 ? $parcial['litros'] / $parcial['horas'] : 0;
        $parcial['kmLitro'] = $parcial['kilometros'] > 0 ? $parcial['kilometros'] / $parcial['litros'] : 0;
        $parcial['horasCienLitros'] = $parcial['horas'] > 0 ? 100 / $parcial['litrosHora'] : 0;
        $parcial['litrosKm'] = $parcial['kilometros'] > 0 ? $parcial['litros'] / $parcial['kilometros'] : 0;
        $parcial['litrosCienKms'] = $parcial['litrosKm'] > 0 ? $parcial['litrosKm'] * 100 : 0;

        if ($servicio->getVehiculo() !== null) {
            $parcial['consumoTeoricoPorKm'] = $servicio->getVehiculo()->getRendimiento() != null ? $servicio->getVehiculo()->getRendimiento() * intval($parcial['kilometros']) / 100 : 's/d';
            $parcial['consumoTeoricoRalenti'] = $servicio->getVehiculo()->getRendimientoHora() * $parcial['ralenti'];
        }
        $parcial['consumoTotal'] = floatval($parcial['consumoTeoricoRalenti']) + floatval($parcial['consumoTeoricoPorKm']);

        // Kilómetros del tablero
        $odometro1 = intval($entrada['carga1']['odometroTablero']);
        $odometro2 = intval($entrada['carga2']['odometroTablero']);
        
        if ($odometro1 > 0 && $odometro2 > 0) {
            $diferencia = $odometro2 - $odometro1;
            $parcial['kilometrosTablero'] = abs($diferencia); // Convertir a positivo si es negativo
        } else {
            $parcial['kilometrosTablero'] = '---';
        }
        
        $parcial['litrosKmTablero'] = $parcial['kilometrosTablero'] > 0 ? $parcial['litros'] / $parcial['kilometrosTablero'] : 0;
        $parcial['litrosCienKmsTablero'] = $parcial['litrosKmTablero'] > 0 ? floatval($parcial['litrosKmTablero']) * 100 : 0;

        // hs del tablero
        $parcial['horasTablero'] = intval($entrada['carga2']['horometroTablero']) > 0 && intval($entrada['carga1']['horometroTablero']) > 0 ?
            intval($entrada['carga2']['horometroTablero']) - intval($entrada['carga1']['horometroTablero']) : '---';

        $parcial['litrosHoraTablero'] = $parcial['horasTablero'] > 0 ? $parcial['litros'] / $parcial['horasTablero'] : 0;
        $parcial['litrosCienHorasTablero'] = $parcial['litrosHoraTablero'] > 0 ? floatval($parcial['litrosHoraTablero']) * 100 : 0;

        return $parcial;
    }

    private function acumularParciales($totales, $cargas)
    {
        //   dd($cargas);
        foreach ($cargas as $carga) {
            $parcial = $carga['parcial'];
            $totales['count'] += 1;
            $totales['litros'] += $parcial['litros'];
            $totales['kilometros'] += $parcial['kilometros'];
            $totales['horas'] += $parcial['horas'];
            $totales['litrosHora'] += $parcial['litrosHora'];
            $totales['horasCienLitros'] += $parcial['horasCienLitros'];
            $totales['litrosKm'] += $parcial['litrosKm'];
            $totales['litrosCienKms'] += $parcial['litrosCienKms'];

            $totales['horasTablero'] += !is_string($parcial['horasTablero']) ? $parcial['horasTablero'] : 0;
            $totales['litrosHoraTablero'] += !is_string($parcial['litrosHoraTablero']) ? $parcial['litrosHoraTablero'] : 0;
            $totales['litrosCienHorasTablero'] += !is_string($parcial['litrosCienHorasTablero']) ? $parcial['litrosCienHorasTablero'] : 0;
        
            $totales['kilometrosTablero'] += !is_string($parcial['kilometrosTablero']) ? $parcial['kilometrosTablero'] : 0;
            $totales['litrosKmTablero'] += !is_string($parcial['litrosKmTablero']) ? $parcial['litrosKmTablero'] : 0;
            $totales['litrosCienKmsTablero'] += !is_string($parcial['litrosCienKmsTablero']) ? $parcial['litrosCienKmsTablero'] : 0;
        }
        return $totales;
    }

    private function calcularTotalesFinales($totales)
    {
        if ($totales['count'] > 0) {
            // Verificación para evitar la división por cero en litrosHora
            $totales['litrosHora'] = $totales['horas'] > 0 ? $totales['litros'] / $totales['horas'] : 0;

            // Verificación para evitar la división por cero en horasCienLitros
            $totales['horasCienLitros'] = $totales['litrosHora'] > 0 ? 100 / $totales['litrosHora'] : 0;

            // Verificación para evitar la división por cero en litrosKm
            $totales['litrosKm'] = $totales['kilometros'] > 0 ? $totales['litros'] / $totales['kilometros'] : 0;

            // Calcular litrosCienKms solo si litrosKm es mayor a cero
            $totales['litrosCienKms'] = $totales['litrosKm'] > 0 ? $totales['litrosKm'] * 100 : 0;
        }
        return $totales;
    }



    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * Helper to return JSON response.
     */
    private function jsonResponse(string $status, ?string $html, int $statusCode)
    {
        return new Response(json_encode(['status' => $status, 'html' => $html]), $statusCode);
    }
}
