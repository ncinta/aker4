<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organizacion;
use App\Form\fuel\InformeCargasType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\LoggerManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\GeocoderManager;

/**
 * Description of InformeCargaController
 *
 * @author nicolas
 */
class InformeCargaController extends AbstractController
{

    private $referenciaManager;
    private $servicioManager;
    private $grupoServicioManager;
    private $informeUtilsManager;
    private $loggerManager;
    private $geocoderManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $combustibleManager;
    private $tanqueManager;
    private $backendManager;
    private $userloginManager;
    private $organizacionManager;
    private $excelManager;

    public function __construct(
        ReferenciaManager $referenciaManager,
        ServicioManager $servicioManager,
        GrupoServicioManager $grupoServicioManager,
        InformeUtilsManager $informeUtilsManager,
        LoggerManager $loggerManager,
        GeocoderManager $geocoderManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        CombustibleManager $combustibleManager,
        TanqueManager $tanqueManager,
        BackendManager $backendManager,
        UserLoginManager $userloginManager,
        OrganizacionManager $organizacionManager,
        ExcelManager $excelManager
    ) {
        $this->referenciaManager = $referenciaManager;
        $this->servicioManager = $servicioManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->loggerManager = $loggerManager;
        $this->geocoderManager = $geocoderManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->combustibleManager = $combustibleManager;
        $this->tanqueManager = $tanqueManager;
        $this->backendManager = $backendManager;
        $this->userloginManager = $userloginManager;
        $this->organizacionManager = $organizacionManager;
        $this->excelManager = $excelManager;
    }

    private $granTotal = array('litros' => 0, 'monto' => 0, 'cantidad' => 0);

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/informe/cargas", name="fuel_informe_carga",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function cargaAction(Request $request, Organizacion $organizacion)
    {

        $servicios = $this->servicioManager->findEnabledByUsuario();
        $options = array(
            'servicios' => $servicios,
            'estaciones' => $this->referenciaManager->findEstacionesMapa(),
            'grupos' => $this->grupoServicioManager->findAsociadas(),
        );
        $form = $this->createForm(InformeCargasType::class, null, $options);
        
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe Cargas");
        return $this->render('fuel/InformeCarga/cargas.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
        ));
    }
    
    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/informe/cargas/generar", name="fuel_informecarga_generar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function generarAction(Request $request, Organizacion $organizacion)
    {
        $tmp = array();
        parse_str($request->get('formCarga'), $tmp);
        $consulta = $tmp['informecarga'];
        if ($consulta) {
            $user = $this->getUser2Organizacion($organizacion->getId());
            $arrServicios = $this->informeUtilsManager->obtenerServicios($consulta['servicio'], $user);
            $result = $this->armarArray($arrServicios['servicios'], $consulta);
            $consulta['servicionombre'] = $arrServicios['servicionombre'];
            $html =  $this->renderView('fuel/InformeCarga/pantalla.html.twig', array(
                'organizacion' => $organizacion,
                'consulta' => $consulta,
                'informe' => $result,
                'granTotal' => $this->granTotal,

            ));
            return new Response(json_encode(array('status'=> 'ok','html' => $html)), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/fuel/{id}/informe/cargas/exportar", name="fuel_informe_carga_exportar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function exportarAction(Request $request, $consulta = null, $informe = null, $granTotal = null)
    {
        //die('////<pre>' . nl2br(var_export('$horas', true)) . '</pre>////');
        if (intval($request->get('tipo')) == 0) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
            $granTotal = json_decode($request->get('granTotal'), true);
        }        
        if ($consulta['servicio'] == 0) {
            $serv = 'Toda la flota';
        } else {           
           $serv = $consulta['servicionombre'];
        }
        if (isset($consulta) && isset($informe)) {
            //creo el xls
            $xls = $this->excelManager->create("Cargas");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $serv,
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));

            if (isset($consulta['mostrar_detallado'])) {
                //se hace la barra principal
                $xls->setBar(6, array(
                    'A' => array('title' => 'Tipo', 'width' => 20),
                    'B' => array('title' => 'Fecha', 'width' => 20),
                    'C' => array('title' => 'Servicio', 'width' => 20),
                    'D' => array('title' => 'Chofer', 'width' => 20),
                    'E' => array('title' => 'Estación/Punto de Carga', 'width' => 20),
                    'F' => array('title' => 'Ubicación', 'width' => 20),
                    'G' => array('title' => 'Tipo de Carga', 'width' => 20),
                    'H' => array('title' => 'Descripción', 'width' => 20),
                    'I' => array('title' => 'Litros', 'width' => 20),
                    'J' => array('title' => 'Monto', 'width' => 20),
                    'K' => array('title' => 'Litros reales', 'width' => 20),
                    'L' => array('title' => 'Diferencia', 'width' => 20),
                    'M' => array('title' => 'Esstado', 'width' => 20)
                ));
                $i = 7;
                foreach ($informe as $key => $data) {
                    foreach ($data['cargas'] as $carga) {   //esto es para cada servicio
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $carga['strmodoingreso']),
                            'B' => array('value' => $carga['fecha']),
                            'C' => array('value' => $key),
                            'D' => array('value' => $carga['chofer']),
                            'E' => array('value' => $carga['estacion'] . ' / ' . $carga['puntocarga']),
                            'F' => array('value' => $carga['ubicacion']),
                            'G' => array('value' => $carga['tipoCarga']),
                            'H' => array('value' => $carga['descripcion']),
                            'I' => array('value' => $carga['litroscarga']),
                            'J' => array('value' => $carga['monto']),
                            'K' => array('value' => is_null($carga['canbus']) ? '--- ' : $carga['canbus']['canbus_sistema']),
                            'L' => array('value' => is_null($carga['canbus']) ? ' --- ' : $carga['canbus']['canbus_diferencia']),
                            'M' => array('value' => $carga['strestado']),
                        ));
                        $i++;
                    }
                    $xls->setRowValues($i, array(
                        'H' => array('value' => 'TOTAL'),
                        'I' => array('value' => $data['total']['litros']),
                        'J' => array('value' => $data['total']['monto']),
                    ));
                    $i++;
                }
            } else {    //esto es resumen
                $xls->setBar(6, array(
                    'A' => array('title' => 'Servicio', 'width' => 20),
                    'B' => array('title' => 'Cargas', 'width' => 20),
                    'C' => array('title' => 'Monto', 'width' => 20),
                    'D' => array('title' => 'Litros', 'width' => 20),
                ));
                $i = 7;
                foreach ($informe as $key => $data) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $key),
                        'B' => array('value' => count($data['cargas'])),
                        'C' => array('value' => $data['total']['monto']),
                        'D' => array('value' => $data['total']['litros']),
                    ));
                    $i++;
                }
            }
            $xls->setRowValues($i, array(
                'H' => array('value' => 'GRAN TOTAL'),
                'I' => array('value' => $granTotal['litros']),
                'J' => array('value' => $granTotal['monto']),
            ));

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=cargas.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        }
    }

    public function armarArray($servicios, $consulta)
    {
        $arr_servicio = array();

        $arr_canbus = array();
        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true);
        foreach ($servicios as $servicio) {
            $cargas = $this->combustibleManager->findByServicioYfecha($servicio, $desde, $hasta, intval($consulta['ingreso']), intval($consulta['estacion']));
            $arr_carga = array();
            $totLitros = $totMonto = 0;
            foreach ($cargas as $carga) {
                $fecha = date_format($carga->getFecha(), 'd-m-Y H:i:s');
                $direc = $this->geocoderManager->inverso($carga->getLatitud(), $carga->getLongitud());
                $arr_carga[$fecha] = array(
                    'fecha' => $fecha,
                    'ubicacion' => $direc,
                    'litroscarga' => $carga->getLitrosCarga(),
                    'monto' => $carga->getMontoTotal(),
                    'modoingreso' => $carga->getModoIngreso(),
                    'strmodoingreso' => $carga->getStrModoIngreso($carga->getModoIngreso()),
                    'estado' => $carga->getEstado(),
                    'strestado' => $carga->getStrEstado(),
                    'odometro' => $carga->getOdometro(),
                    'descripcion' => !is_null($carga->getDescripcion()) ? $carga->getDescripcion() : '---',
                    'tipoCarga' =>   $carga->getCargaCompleta() ? 'Completa' : 'Parcial',
                    'chofer' => !is_null($carga->getChofer()) ? $carga->getChofer()->getNombre() : '---',
                    'estacion' => !is_null($carga->getReferencia()) ? $carga->getReferencia()->getNombre() : '---',
                    'puntocarga' => !is_null($carga->getPuntoCarga()) ? $carga->getPuntoCarga()->getNombre() : '---',
                );
                $totLitros += $carga->getLitrosCarga();
                $totMonto += $carga->getMontoTotal();
            }

            if ($servicio->getCanbus()) {
                $consulta_consumo = $this->backendManager->informeConsumoCombustible(
                    $servicio->getId(),
                    $desde,
                    $hasta,
                    $cargas,
                    $this->referenciaManager->findAsociadas()
                );
                if ($servicio->getMedidorCombustible()) {   //siempre que tenga mediciones de combustible.
                    $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
                }
                foreach ($consulta_consumo as $entrada) {
                    //aca se establece la direccion.
                    if (is_null($entrada['carga1']['lugar'])) {
                        $direc = $this->geocoderManager->inverso($entrada['carga1']['latitud'], $entrada['carga1']['longitud']);
                        $entrada['carga1']['lugar'] = $direc;
                    } else {
                        $entrada['carga1']['lugar'] = $this->referenciaManager->find($entrada['carga1']['lugar'])->getNombre();
                    }
                    // si tengo el valor del rendimiento de canbus lo piso de lo contrario dejo el que esta
                    $entrada['historial']['rendimiento'] = !is_null($entrada['historial']['rendimiento_canbus']) ? $entrada['historial']['rendimiento_canbus'] : $entrada['historial']['rendimiento'];
                    if (isset($isMedicionesLoad) && $isMedicionesLoad) {
                        $cargaAnt = is_null($entrada['carga1']['canbusDataAnterior']['nivelCombustible']) ? 0 : $entrada['carga1']['canbusDataAnterior']['nivelCombustible'];
                        $cargaPos = is_null($entrada['carga1']['canbusDataPosterior']['nivelCombustible']) ? 0 : $entrada['carga1']['canbusDataPosterior']['nivelCombustible'];

                        $medicionantes = $this->tanqueManager->getCapacidad(intval($cargaAnt), $servicio);
                        $mediciondespues = $this->tanqueManager->getCapacidad(intval($cargaPos), $servicio);
                        $litros = $entrada['carga1']['litros'];
                        $reales = $mediciondespues['litros'] - $medicionantes['litros'];
                        $diferencia = $litros - $reales;
                        $fechaDate = new \DateTime($entrada['carga1']['fecha']);
                        $fecha = date_format($fechaDate, 'd-m-Y H:i:s');
                        $odometro = isset($entrada['carga1']['canbusDataAnterior']['odometro']) ? $entrada['carga1']['canbusDataAnterior']['odometro'] : '---';
                    }
                    $arr_canbus[$fecha] = array(
                        'servicio' => $servicio->getId(),
                        'canbus_diferencia' => $diferencia,
                        'canbus_sistema' => $reales,
                        'canbus_fecha' => $fecha,
                        'canbus_odometro' => $odometro,
                    );
                }
            } else {
                $arr_canbus = array();
            }
            foreach ($arr_carga as $key => $value) {
                if ($value) {
                    $can = null;
                    if (array_key_exists($key, $arr_canbus)) {
                        $can = $arr_canbus[$key];
                    }
                    $arr_servicio[$servicio->getNombre()]['cargas'][] = array_merge($value, array('canbus' => $can));
                }
            }
            if ($totLitros != 0 || $totMonto != 0) {
                $arr_servicio[$servicio->getNombre()]['total'] = array('litros' => $totLitros, 'monto' => $totMonto);
                $this->granTotal['litros'] += $totLitros;
                $this->granTotal['monto'] += $totMonto;
                $this->granTotal['cantidad']++;
            }
        }

        return $arr_servicio;
    }

    private function getUser2Organizacion($id)
    {
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($id);
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        return $user;
    }
}
