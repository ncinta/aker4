<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Entity\Organizacion as Organizacion;
use App\Entity\PuntoCarga as PuntoCarga;
use App\Form\fuel\PuntoCargaType as PuntoCargaType;
use App\Form\fuel\PuntoCargaCombType as PuntoCargaCombType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\PuntoCargaManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\Router\PuntoCargaRouter;

/**
 * Description of PuntoCargaController
 *
 * @author nicolas
 */
class PuntoCargaController extends AbstractController
{

    private $breadcrumbManager;
    private $puntocargaManager;
    private $userloginManager;
    private $servicioManager;
    private $referenciaManager;
    private $combustibleManager;
    private $puntocargaRouter;

    public function __construct(
        BreadcrumbManager $breadcrumbManager,
        PuntoCargaManager $puntocargaManager,
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        CombustibleManager $combustibleManager,
        ReferenciaManager $referenciaManager,
        PuntoCargaRouter $router
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->puntocargaManager = $puntocargaManager;
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->combustibleManager = $combustibleManager;
        $this->puntocargaRouter = $router;
    }

    private function getListMenu()
    {

        $menu = array(
            1 => [
                $this->puntocargaRouter->btnNew(null),
            ]
        );
        return $menu;
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $chofer 
     * @return array $menu
     */
    private function getShowMenu($puntocarga)
    {
        $menu = array(
            1 => [
                $this->puntocargaRouter->btnEdit($puntocarga),
                $this->puntocargaRouter->btnDelete(),
            ],
            2 => [
                $this->puntocargaRouter->btnAddCombustible($puntocarga),

            ]
        );

        return $menu;
    }

    /**
     * Lista todas los puntos de carga de una organizacion
     * @Route("/fuel/puntocarga/{id}/delete", name="fuel_delete_puntocarga",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_COMBUSTIBLE_VER")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->puntocargaManager->delete($id);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Lista todas los puntos de carga de una organizacion
     * @Route("/fuel/puntocarga/{id}/list", name="fuel_puntocarga_list",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_PUNTOCARGA_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Puntos de Cargas");
        $options['combustibles'] = $this->puntocargaManager->getTipoCombustibles();
        $optionsAdd['em'] = $this->getEntityManager();
        $optionsAdd['organizacion'] = $organizacion;
        // die('////<pre>' . nl2br(var_export(count($options['combustibles']), true)) . '</pre>////');
        $puntocargas = $this->puntocargaManager->findAll($organizacion);
        $puntocarga = $this->puntocargaManager->create($organizacion);
        $data = $this->puntocargaManager->generarDashboard($puntocargas);
        $formAdd = $this->createForm(PuntoCargaType::class, $puntocarga, $optionsAdd);
        $formAddComb = $this->createForm(PuntoCargaCombType::class, null, $options);
        return $this->render('fuel/PuntoCarga/list.html.twig', array(
            'menu' => $this->getListMenu(),
            'puntocargas' => $puntocargas,
            'formAdd' => $formAdd->createView(),
            'data' => $data,
            'formAddComb' => $formAddComb->createView(),
        ));
    }

    /**
     * @Route("/fuel/puntocarga/{id}/show", name="fuel_puntocarga_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_PUNTOCARGA_VER")
     */
    public function showAction(Request $request, PuntoCarga $puntocarga)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Punto de Carga");
        $organizacion = $this->userloginManager->getOrganizacion();
        $options['combustibles'] = $this->puntocargaManager->getTipoCombustibles();
        $optionsAdd['em'] = $this->getEntityManager();
        $optionsAdd['organizacion'] = $organizacion;
        $formAddComb = $this->createForm(PuntoCargaCombType::class, null, $options);

        $now = new \DateTime();
        $hasta = $now->format('Y-m-d H:i:s');
        $desde = $now->sub(new \DateInterval('P30D'));
        $desde = $desde->format('Y-m-d H:i:s');
        //die('////<pre>' . nl2br(var_export($hasta, true)) . '</pre>////');
        $despachos = $this->combustibleManager->findByPuntoYfecha($desde, $hasta, null, null, $puntocarga);
        return $this->render('fuel/PuntoCarga/show.html.twig', array(
            'menu' => $this->getShowMenu($puntocarga),
            'puntocarga' => $puntocarga,
            'despachos' => $despachos,
            'formAddComb' => $formAddComb->createView(),
            'delete_form' => $this->createDeleteForm($puntocarga->getId())->createView(),
        ));
    }

    /**
     * @Route("/fuel/puntocarga/add", name="fuel_add_puntocarga",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     *     options={"expose": true},
     *     methods={"GET", "POST"})
     * )
     * @IsGranted("ROLE_PUNTOCARGA_AGREGAR")
     */
    public function addPuntoCargaAction(Request $request)
    {

        $organizacion = $this->userloginManager->getOrganizacion();
        $puntocarga = $this->puntocargaManager->create($organizacion);
        $tmp = array();
        parse_str($request->get('formAdd'), $tmp);
        $dataPuntoCarga = $tmp['puntocarga'];
      //  die('////<pre>' . nl2br(var_export($dataPuntoCarga, true)) . '</pre>////');
        if ($dataPuntoCarga) {
            $dataServ = isset($dataPuntoCarga['servicio']) ? intval($dataPuntoCarga['servicio']) : 0 ;
            $dataRef = isset($dataPuntoCarga['referencia']) ? intval($dataPuntoCarga['referencia']) : 0 ;
            $servicio = ($dataServ > 0) ? $this->servicioManager->find($dataServ) : null;
            $referencia = ($dataRef > 0) ? $this->referenciaManager->find($dataRef) : null;
            $puntocarga->setNombre($dataPuntoCarga['nombre']);
            $puntocarga->setCuit($dataPuntoCarga['cuit']);
            $puntocarga->setCodigo($dataPuntoCarga['codigo']);
            $puntocarga->setServicio($servicio);
            $puntocarga->setReferencia($referencia);
            $puntocarga->setOrganizacion($organizacion);
            $puntocarga = $this->puntocargaManager->save($puntocarga);
        }
        $puntocargas = $this->puntocargaManager->findAll($organizacion);
        $data = $this->puntocargaManager->generarDashboard($puntocargas);
        return new Response(json_encode(array(
            'html' => $this->renderView('fuel/PuntoCarga/puntocargas.html.twig', array(
                'puntocargas' => $puntocargas,
                'data' => $data
            )),
        )));
    }

    /**
     * @Route("/fuel/puntocarga/{id}/edit", name="fuel_edit_puntocarga",
     *     requirements={
     *         "id": "\d+"
     *     },
     *     methods={"GET", "POST"}
     * )
     * @IsGranted("ROLE_PUNTOCARGA_EDITAR")
     */
    public function editAction(Request $request, PuntoCarga $puntocarga)
    {

        $organizacion = $this->userloginManager->getOrganizacion();
        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getEntityManager();
        $form = $this->createForm(PuntoCargaType::class, $puntocarga, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->breadcrumbManager->pop();
                if ($this->puntocargaManager->save($puntocarga)) {
                    $this->setFlash('success', sprintf('Los datos de han sido actualizados'));
                    return $this->redirect($this->breadcrumbManager->getVolver());
                } else {
                    $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas en la grabación del chofer "%s". Es posible que el Id ya exista.', strtoupper($puntocarga->getNombre())));
                }
            }
        }

        return $this->render('fuel/PuntoCarga/edit.html.twig', array(
            'puntocarga' => $puntocarga,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/fuel/puntocarga/combustible/add", name="fuel_add_combustible_puntocarga",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     *     options={"expose": true},
     *     methods={"GET", "POST"})
     * )
     * @IsGranted("ROLE_PUNTOCARGA_EDITAR")
     */
    public function addCombustiblePuntoCargaAction(Request $request)
    {

        $organizacion = $this->userloginManager->getOrganizacion();
        $tmp = array();
        parse_str($request->get('formAddComb'), $tmp);
        $dataPuntoCarga = $tmp['puntocarga_comb'];
        $puntocarga = $this->puntocargaManager->findById(intval($dataPuntoCarga['id']));
        $tipoComb = intval($dataPuntoCarga['tipocombustible']);
        $tipo = $this->puntocargaManager->findTipoCombustible($tipoComb);
        $combustible = $this->puntocargaManager->addCombustible($puntocarga, floatval($dataPuntoCarga['precio']), $tipo);
        $puntocargas = $this->puntocargaManager->findAll($organizacion);
        if ($request->get('vista') == '0') {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/puntocargas.html.twig', array(
                    'puntocargas' => $puntocargas
                )),
            )));
        } else {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/combustibles.html.twig', array(
                    'puntocarga' => $puntocarga
                )),
            )));
        }
    }

    /**
     * @Route("/fuel/puntocarga/combustible/edit", name="fuel_edit_combustible_puntocarga",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     *     options={"expose": true},
     *     methods={"GET", "POST"})
     * )
     * @IsGranted("ROLE_PUNTOCARGA_AGREGAR")
     */
    public function editCombustiblePuntoCargaAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $tmp = array();
        parse_str($request->get('formEditComb'), $tmp);
        $precio = floatval($tmp['puntocarga_comb_edit']['precio']);
        $combustible = $this->puntocargaManager->findPrecioTipoCombustible(intval($tmp['puntocarga_comb_edit']['id']));
        $combustible->setPrecio($precio);
        $updprecio = $this->puntocargaManager->saveComb($combustible);
        $puntocarga = $combustible->getPuntoCarga();
        $puntocargas = $this->puntocargaManager->findAll($organizacion);

        if ($request->get('vista') == '0') {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/puntocargas.html.twig', array(
                    'puntocargas' => $puntocargas
                )),
            )));
        } else {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/combustibles.html.twig', array(
                    'puntocarga' => $puntocarga
                )),
            )));
        }
    }

    /**
     * @Route("/fuel/puntocarga/combustible/delete", name="fuel_delete_combustible_puntocarga", 
     *      methods={"POST"})
     * )
     * @IsGranted("ROLE_PUNTOCARGA_ELIMINAR")
     */
    public function delCombAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $id = $request->get('id');   //es el id del ordentrabajoproducto
        $combustible = $this->puntocargaManager->findPrecioTipoCombustible(intval($id));
        $puntocarga = $combustible->getPuntoCarga();

        $delete = $this->puntocargaManager->deletePrecioTipoCombustible($combustible);

        $puntocargas = $this->puntocargaManager->findAll($organizacion);
       
        if ($request->get('vista') == '0') {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/puntocargas.html.twig', array(
                    'puntocargas' => $puntocargas
                )),
            )));
        } else {
            return new Response(json_encode(array(
                'html' => $this->renderView('fuel/PuntoCarga/combustibles.html.twig', array(
                    'puntocarga' => $puntocarga
                )),
            )));
        }
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
