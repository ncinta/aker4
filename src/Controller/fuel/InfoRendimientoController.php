<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Form\fuel\CombustibleInformeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\app\BackendManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\UserLoginManager;
use App\Model\fuel\CombustibleManager;
use App\Model\app\GeocoderManager;
use App\Model\app\InformeUtilsManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class InfoRendimientoController extends AbstractController
{

    private $grupoServicioManager;
    private $servicioManager;
    private $breadcrumbManager;
    private $utilsManager;
    private $userloginManager;
    private $combustibleManager;
    private $backendManager;
    private $referenciaManager;
    private $tanqueManager;
    private $geocoderManager;
    private $excelManager;
    private $informeUtilsManager;
    private $session;

    public function __construct(
        GrupoServicioManager $grupoServicioManager,
        ServicioManager $servicioManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager,
        UserLoginManager $userloginManager,
        CombustibleManager $combustibleManager,
        BackendManager $backendManager,
        ReferenciaManager $referenciaManager,
        TanqueManager $tanqueManager,
        GeocoderManager $geocoderManager,
        ExcelManager $excelManager,
        InformeUtilsManager $informeUtilsManager,
        SessionInterface $session,
    ) {
        $this->grupoServicioManager = $grupoServicioManager;
        $this->servicioManager = $servicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
        $this->userloginManager = $userloginManager;
        $this->combustibleManager = $combustibleManager;
        $this->backendManager = $backendManager;
        $this->referenciaManager = $referenciaManager;
        $this->tanqueManager = $tanqueManager;
        $this->geocoderManager = $geocoderManager;
        $this->excelManager = $excelManager;
        $this->informeUtilsManager = $informeUtilsManager;
        $this->session = $session;
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/{id}/info/rendimiento", name="fuel_info_rendimiento",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */
    public function informeAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Informe de Rendimiento por Canbus");
        $options['servicios'] = $this->servicioManager->getServiciosVehiculos($organizacion);
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $form = $this->createForm(CombustibleInformeType::class, null, $options);


        return $this->render('fuel/InfoRendimiento/informe.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
            'informe' => null

        ));
    }

    /**
     * Informe de rendimiento de combustible.
     * @Route("/fuel/{id}/info/rendimiento/generar", name="fuel_info_rendimiento_generar",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  options={"expose": true},
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_COMBUSTIBLE_INFORME_RENDIMIENTO")
     */

    public function generarinformeAction(Request $request, Organizacion $organizacion)
    {
        $tmp = array();
        parse_str($request->get('formRendimiento'), $tmp);
        $consulta = $tmp['consumocombustible'];
        $arr = array();
        $con_datos = false;

        if ($consulta) {
            $this->session->clear(); // Elimina todas las keys de la sesión
            $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], true);
            $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], false);
            //aca se obtiene el informe.
            if ($consulta['servicio'] == '-1') {  //no se selecciono nada
                $this->setFlash('error', sprintf('No se seleccionó ningún servicio'));
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $arr = $this->getData($consulta, $desde, $hasta, $organizacion);
            }
            if ($arr['salida'] == '2') { //sale por flota
                $html =  $this->renderView('fuel/InfoRendimiento/result_pantalla.html.twig', array(
                    'informe' => $arr['informe'],
                    'consulta' => $arr['consulta'],
                ));
                $grafico = $this->getBarrasLitros($arr['informe']);
            } else { //sale por unitario
                $html =  $this->renderView('fuel/InfoRendimiento/result_pantalla_unitario.html.twig', array(
                    'informe' => $arr['informe'],
                    'consulta' => $arr['consulta'],
                ));
                $grafico = $this->getBarrasLitros($arr['informe']);
            }
            $this->session->set('data_informe',  $arr['informe']); //para poder exportar a excel

            $nonNullElements = array_filter($arr['informe'], function ($value) {
                return !is_null($value);
            });
            if (!empty($nonNullElements)) {
                $con_datos = true;
            } else {
                $con_datos = false;
            }

            return new Response(json_encode(array(
                'status' => 'ok',
                'con_datos' => $con_datos,
                'html' => $html,
                'grafico' => $grafico
            )), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    public function getData($consulta, $desde, $hasta, $organizacion)
    {
        $informe_canbus = array();
        $informe_not_canbus = array();
        if ($consulta['servicio'] == '0') {
            $consulta['servicionombre'] = 'Toda la Flota';
            $servicios = $this->servicioManager->getServiciosVehiculos($organizacion);
        } elseif (substr($consulta['servicio'], 0, 1) == 'g') {
            $grupo = $this->grupoServicioManager->find(intval(substr($consulta['servicio'], 1)));
            $consulta['servicionombre'] = $grupo->getNombre();
            $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
        } else {
            //sale por 1 solo
            $servicio = $this->servicioManager->find(intval($consulta['servicio']));
            $consulta['servicionombre'] = $servicio->getNombre();
            $periodos = $this->informeUtilsManager->armarPeriodo($desde, $hasta, '1', true);
            foreach ($periodos as $key => $periodo) {
                $dia = (new \DateTime($periodo['desde']))->format('d/m/Y');
                if ($servicio->getEquipo() != null && $servicio->getCanbus()) {
                    // dd($periodo['desde']);
                    $historial = $this->backendManager->historialCanbus($servicio->getId(), $periodo['desde'], $periodo['hasta']);
                    $informe_canbus[] = $this->getRendimientos($dia, $this->formatdata($servicio, $historial, $dia));
                } else { //aca arranca el informe de servicios sin can
                    $historial = $this->backendManager->historial($servicio->getId(), $periodo['desde'], $periodo['hasta']);
                    $informe_not_canbus[] = $this->getRendimientosNotCan($dia, $this->formatdataNotCan($servicio, $historial, $dia));
                }
            }
            $informe = array_merge($informe_canbus, $informe_not_canbus);
            return array('informe' => $informe, 'consulta' => $consulta, 'salida' => '1');
        }

        foreach ($servicios as $servicio) {
            if ($servicio->getEquipo() != null && $servicio->getCanbus()) { //es un servicio sin equipo que está dentro de un grupo
                $historial = $this->backendManager->historialCanbus($servicio->getId(), $desde, $hasta);
                if (count($historial) > 0) {
                    $data = $this->formatdata($servicio, $historial);
                    $informe_canbus[] = $this->getRendimientos($servicio->getId(), $data);
                }
            } else { //aca arranca el informe de servicios sin can
                $historial = $this->backendManager->historial($servicio->getId(), $desde, $hasta);
                $informe_not_canbus[] = $this->getRendimientosNotCan($servicio->getId(),  $this->formatdataNotCan($servicio, $historial));
            }
        }
        $informe = array_merge($informe_canbus, $informe_not_canbus);
        //dd($informe_canbus);

        return array('informe' => $informe, 'consulta' => $consulta, 'salida' => '2');
    }

    // armo array por grafico de litros
    public function getBarrasLitros($informe)
    {
        $data = array();
        foreach ($informe as $d) {
            if ($d != null) {
                foreach ($d as $key => $row) {
                    $data[$key]['nombre'] = $row['nombre'];
                    $data[$key]['odolitro_ralenti'] = isset($row['odolitro_ralenti']) ? $row['odolitro_ralenti'] : 0;
                    $data[$key]['odolitro_movimiento'] = isset($row['odolitro_movimiento']) ? $row['odolitro_movimiento'] : 0;
                    $data[$key]['rendimiento_teorico'] = $row['diff_odometro'] > 0 ? ($row['rendimiento_teorico_cienkms'] * $row['diff_odometro']) / 100 : 0;
                }
            }
        }
        return $data;
    }


    public function formatdata($servicio, $historial, $dia = null) //servicio para flotas , dia para unitario
    {
        if (count($historial) == 0 || $servicio == null) {
            return null;
        }

        $key = $dia != null ? $dia : $servicio->getId(); //agrupamos por servicio para flotas, por dia para 1
        $data = array();
        $rendimiento_teorico_cienkms =  $servicio->getVehiculo()->getRendimiento() != null ? intval($servicio->getVehiculo()->getRendimiento()) : 0;
        $rendimiento_teorico_kmlt = $rendimiento_teorico_cienkms > 0 ? 100 / $rendimiento_teorico_cienkms : 0;
        $rendimiento_teorico_hora =  $servicio->getVehiculo()->getRendimientoHora() != null ? intval($servicio->getVehiculo()->getRendimientoHora()) : 0;

        $data[$key]['nombre'] = $dia != null ? $dia : $servicio->getNombre();
        $data[$key]['canbus'] = true;

        $data[$key]['rendimiento_teorico_cienkms'] = $rendimiento_teorico_cienkms;
        $data[$key]['rendimiento_teorico_km_litro']  = $rendimiento_teorico_kmlt;
        $data[$key]['rendimiento_teorico_hora'] = $rendimiento_teorico_hora;
        $data[$key]['rendimiento_teorico_hora_litro'] = $rendimiento_teorico_hora > 0 ? $this->floatToTime(1 / $rendimiento_teorico_hora) : 0;

        $data[$key]['fecha'][] =  null;
        $data[$key]['odometro'][] = 0;
        $data[$key]['horometro'][] = 0;

        $data[$key]['movimiento']['odolitro'][] = 0;
        $data[$key]['movimiento']['total_odolitro'] = 0;

        $data[$key]['ralenti']['odolitro'][] = 0;
        $data[$key]['ralenti']['total_odolitro'] = 0;
        $historial = array_values($historial);
        foreach ($historial as $k => $trama) {
            $tramaAnt = $k > 0 && isset($historial[$k - 1]) ?  $historial[$k - 1] :  null;
            $canbusData = $trama['canbusData'];
            $odolitro = isset($canbusData['odolitro']) ? $canbusData['odolitro'] : 0;
            $odolitroAnt = $tramaAnt != null && isset($tramaAnt['canbusData']['odolitro']) ? $tramaAnt['canbusData']['odolitro'] : $odolitro;

            if ($trama['contacto'] && isset($trama['posicion']) && $trama['posicion']['velocidad'] > 0) { // está en movimiento
                if ($odolitro - $odolitroAnt > 0) { // si no hay diferencia no las coloco
                    $data[$key]['movimiento']['odolitro'][isset($trama['fecha']) ?  $this->utilsManager->fechaUTC2local(date_create($trama['fecha']), 'd/m/Y H:i:s') : null] = $odolitro - $odolitroAnt;
                    $data[$key]['movimiento']['total_odolitro'] += $odolitro - $odolitroAnt;
                }
            } else { // está en ralenti
                if ($odolitro - $odolitroAnt > 0) { // si no hay diferencia no las coloco
                    $data[$key]['ralenti']['odolitro'][] = $odolitro - $odolitroAnt;
                    $data[$key]['movimiento']['odolitro'][isset($trama['fecha']) ? $this->utilsManager->fechaUTC2local(date_create($trama['fecha']), 'd/m/Y H:i:s') : null] = $odolitro - $odolitroAnt;
                    $data[$key]['ralenti']['total_odolitro'] += $odolitro - $odolitroAnt;
                }
            }
            $data[$key]['odometro'][] = isset($trama['odometro']) ? $trama['odometro'] / 1000 : null; //lo pasamos a kms
            $data[$key]['horometro'][] = isset($trama['horometro']) ? $trama['horometro'] : null;
            $data[$key]['odolitro'][] = $odolitro > 0 ? $odolitro : null; //odolitro para calcular la diferencia
            $data[$key]['fecha'][] = isset($trama['fecha']) ? date_create($trama['fecha']) : null;
        }

        return $data;
    }
    public function formatdataNotCan($servicio, $historial, $dia = null) //servicio para flotas , dia para unitario
    {
        if (count($historial) == 0 || $servicio == null) {
            return null;
        }

        $key = $dia != null ? $dia : $servicio->getId(); //agrupamos por servicio para flotas, por dia para 1

        $data = array();
        $rendimiento_teorico_cienkms =  $servicio->getVehiculo()->getRendimiento() != null ? intval($servicio->getVehiculo()->getRendimiento()) : 0;
        $rendimiento_teorico_kmlt = $rendimiento_teorico_cienkms > 0 ? 100 / $rendimiento_teorico_cienkms : 0;
        $rendimiento_teorico_hora =  $servicio->getVehiculo()->getRendimientoHora() != null ? intval($servicio->getVehiculo()->getRendimientoHora()) : 0;
        $data[$key]['nombre'] = $dia != null ? $dia : $servicio->getNombre();
        $data[$key]['canbus'] = false;

        $data[$key]['rendimiento_teorico_cienkms'] = $rendimiento_teorico_cienkms;
        $data[$key]['rendimiento_teorico_hora'] = $rendimiento_teorico_hora;
        $data[$key]['rendimiento_teorico_km_litro']  = $rendimiento_teorico_kmlt;
        $data[$key]['rendimiento_teorico_hora_litro'] = $rendimiento_teorico_hora > 0 ? $this->floatToTime(1 / $rendimiento_teorico_hora) : 0;
        $data[$key]['fecha'][] =  null;
        $data[$key]['odometro'][] = 0;
        $data[$key]['horometro'][] = 0;

        $historial = array_values($historial);
        foreach ($historial as $k => $trama) {
            if (isset($trama['odometro']) && $trama['odometro'] > 0) {
                $tramaAnt = $k > 0 && isset($historial[$k - 1]) ?  $historial[$k - 1] :  null;
                if (!($trama['contacto'] && isset($trama['posicion']) && $trama['posicion']['velocidad'] > 0)) { // está en ralenti
                    // podemos colocar algo aca adentro si necesita algo de ralenti
                }
                $data[$key]['odometro'][] = isset($trama['odometro']) ? $trama['odometro'] / 1000 : null; //lo pasamos a kms
                $data[$key]['horometro'][] = isset($trama['horometro']) ? $trama['horometro'] : null; //lo pasamos a kms
                $data[$key]['fecha'][] = isset($trama['fecha']) ?  date_create($trama['fecha']) : null; //lo pasamos a kms

            }
        }
        return $data;
    }



    public function getRendimientos($key, $data)
    {
        $datosServ = isset($data[$key]) ? $data[$key] : false;
        if (!$datosServ) {
            return null;
        }
        $odolitroMovimiento = $datosServ['movimiento']['total_odolitro'];
        $odolitroRalenti = $datosServ['ralenti']['total_odolitro'];
        $odolitroTotal = $odolitroRalenti + $odolitroMovimiento;

        $horometroInicio =   isset($datosServ['horometro'][1]) ? $datosServ['horometro'][1] : '00:00';
        $horometroFin =    isset($datosServ['horometro'][count($datosServ['horometro']) - 1]) ? $datosServ['horometro'][count($datosServ['horometro']) - 1] : '00:00';
        $diffHorometro = $this->diffHorometroStr($horometroInicio, $horometroFin);
        $diffHorometroInt = $diffHorometro != null ? $this->diffHorometroInt($diffHorometro) : 0;

        $odometroInicio =  isset($datosServ['odometro'][1]) ? round($datosServ['odometro'][1]) : 0;
        $odometroFin =  isset($datosServ['odometro'][count($datosServ['odometro']) - 1]) ? round($datosServ['odometro'][count($datosServ['odometro']) - 1]) : 0;
        $diffOdometro = round($odometroFin - $odometroInicio);

        $odolitroInicio = isset($datosServ['odolitro'][1]) ? round($datosServ['odolitro'][1]) : 0;
        $odolitroFin =  isset($datosServ['odolitro'][count($datosServ['odolitro']) - 1]) ? round($datosServ['odolitro'][count($datosServ['odolitro']) - 1]) : 0;
        $difOdolitro =  $odolitroFin - $odolitroInicio;

        $km_litro = $diffOdometro > 0 && $diffOdometro != null && $odolitroMovimiento > 0 ? ($diffOdometro / $odolitroMovimiento) : 0;
        // dd($diffHorometroInt);
        $hrs_litro = $diffHorometroInt > 0 && $diffHorometroInt != null && $odolitroTotal > 0 ? ($diffHorometroInt / $odolitroTotal) : 0;

        $data[$key]['km_litro'] = $km_litro;
        $data[$key]['hrs_litro'] = $this->floatToTime($hrs_litro);
        $data[$key]['diff_odometro'] = $diffOdometro;
        $data[$key]['diff_horometro'] = $diffHorometro;
        $data[$key]['diff_odolitro'] = $difOdolitro;

        $data[$key]['odolitro_ralenti'] = $odolitroRalenti;
        $data[$key]['odolitro_movimiento'] = $odolitroMovimiento;

        $data[$key]['inicio']['fecha'] = isset($datosServ['fecha'][1]) ? $datosServ['fecha'][1] : null;
        //dd(  $data[$key]['inicio']['fecha']);
        $data[$key]['inicio']['odometro'] = $odometroInicio;
        $data[$key]['inicio']['horometro'] = $horometroInicio;
        $data[$key]['inicio']['odolitro'] = $odolitroInicio;

        $data[$key]['fin']['fecha'] = $datosServ['fecha'][count($datosServ['fecha']) - 1];
        $data[$key]['fin']['odometro'] = $odometroFin;
        $data[$key]['fin']['horometro'] =  $horometroFin;
        $data[$key]['fin']['odolitro'] = $odolitroFin;

        //  dd($data[$key]['inicio']['odolitro']);
        return $data;
    }

    public function getRendimientosNotCan($key, $data)
    {
        $datosServ = isset($data[$key]) ? $data[$key] : false;

        if (!$datosServ) {
            return null;
        }
        $horometroInicio =   isset($datosServ['horometro'][1]) ? $datosServ['horometro'][1] : null;
        $horometroFin =    isset($datosServ['horometro'][count($datosServ['horometro']) - 1]) ? $datosServ['horometro'][count($datosServ['horometro']) - 1] : null;
        $diffHorometro = $this->diffHorometroStr($horometroInicio, $horometroFin);

        $diffHorometroInt = $diffHorometro != null ? $this->diffHorometroInt($diffHorometro) : 0;

        $odometroInicio =  isset($datosServ['odometro'][1]) ? round($datosServ['odometro'][1]) : 0;
        $odometroFin =  isset($datosServ['odometro'][count($datosServ['odometro']) - 1]) ? round($datosServ['odometro'][count($datosServ['odometro']) - 1]) : 0;
        $diffOdometro = $odometroFin - $odometroInicio;

        $litros_consumidos = $diffOdometro > 0 && $diffOdometro != null ? ($diffOdometro / 100) * $datosServ['rendimiento_teorico_cienkms'] : 0;

        $km_litro = $litros_consumidos > 0 && $diffOdometro > 0 ? $diffOdometro / $litros_consumidos : 0;
        // dd($diffHorometroInt);
        $hrs_litro = $diffHorometroInt > 0 && $diffHorometroInt != null && $litros_consumidos > 0 ? ($diffHorometroInt / $litros_consumidos) : 0;

        $data[$key]['km_litro'] = $km_litro;
        $data[$key]['hrs_litro'] = $this->floatToTime($hrs_litro);
        $data[$key]['diff_odometro'] = $diffOdometro;
        $data[$key]['diff_horometro'] = $diffHorometro;
        // $data[$key]['diff_odolitro'] = $litros_consumidos;
        $data[$key]['diff_odolitro'] = null;


        $data[$key]['inicio']['fecha'] = isset($datosServ['fecha'][1]) ? $datosServ['fecha'][1] : null;
        $data[$key]['inicio']['odometro'] = $odometroInicio;
        $data[$key]['inicio']['horometro'] = $horometroInicio;

        $data[$key]['fin']['fecha'] = $datosServ['fecha'][count($datosServ['fecha']) - 1];
        $data[$key]['fin']['odometro'] = $odometroFin;
        $data[$key]['fin']['horometro'] =  $horometroFin;

        return $data;
    }

    private function diffHorometroStr($inicio, $fin)
    {
        $intervalo = null;

        // Convertir el formato a 'H:i' y eliminar los espacios extra
        $inicio = $inicio != null ? str_replace(['h', 'm', ' '], [':', '', ''], trim($inicio)) : null;
        $fin = $fin != null ? str_replace(['h', 'm', ' '], [':', '', ''], trim($fin)) : null;

        // Crear objetos DateTime
        $datetimeInicio = \DateTime::createFromFormat('H:i', $inicio);
        $datetimeFin = \DateTime::createFromFormat('H:i', $fin);

        // Calcular la diferencia
        if ($datetimeInicio && $datetimeFin) {
            $intervalo = $datetimeInicio->diff($datetimeFin);
        }

        // Verificar si $intervalo no es null
        if ($intervalo !== null) {
            return $intervalo->format('%h h %i m');
        } else {
            return null;  // Manejo de error
        }
    }
    private function diffHorometroInt($horometro)
    {
        // Extraer las horas y minutos usando expresiones regulares
        preg_match('/(\d+) h (\d+) m/', $horometro, $coincidencias);

        // Convertir las horas y minutos a enteros
        $horas = (int)$coincidencias[1];
        $minutos = (int)$coincidencias[2];

        // Convertir los minutos a horas (1 hora = 60 minutos)
        $totalHoras = $horas + ($minutos / 60);

        // Redondear el resultado al valor entero más cercano
        $totalHorasInt = (int) round($totalHoras);

        return $totalHorasInt; // Resultado en horas enteras
    }

    function floatToTime($floatHours)
    {
        // Convertir el valor flotante a minutos
        $totalMinutes = $floatHours * 60;

        // Calcular las horas y los minutos
        $hours = floor($totalMinutes / 60);
        $minutes = floor($totalMinutes % 60);

        // Formatear las horas y los minutos en formato HH:ii
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    /**
     * @Route("/fuel/informendimiento/exportar", options={"expose"=true}, name="fuel_informendimiento_exportar")
     * @Method({"GET", "POST"})
     */
    public function exportarAction(Request $request)
    {

        //$tmp = array();
        // parse_str($request->get('consulta'), $tmp);
        $consulta = json_decode($request->get('consulta'), true);
        $desde = $this->utilsManager->datetime2sqltimestamp($consulta['desde'], true);
        $hasta = $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], false);
        $inicio = '';
        $fin = '';
        $informe = $this->session->get('data_informe');


        //creo el xls
        $xls = $this->excelManager->create("Informe de Rendimiento");
        //  dd($consulta);
        $xls->setHeaderInfo(array(
            'C2' => 'Servicio',
            'D2' => $consulta['servicionombre'],
            'C3' => 'Fecha desde',
            'D3' => $consulta['desde'],
            'C4' => 'Fecha hasta',
            'D4' => $consulta['hasta'],
        ));

        //se hace la barra principal
        $xls->setBar(5, array(
            'A' => array('title' => 'Día', 'width' => 20),
            'B' => array('title' => 'Inicio', 'width' => 20),
            'C' => array('title' => 'Fin', 'width' => 20),
            'D' => array('title' => 'Odómetro Inicial', 'width' => 20),
            'E' => array('title' => 'Odómetro Final', 'width' => 20),
            'F' => array('title' => 'Total Kms', 'width' => 20),

            'G' => array('title' => 'Odlitro Inicial', 'width' => 20),
            'H' => array('title' => 'Odolitro Final', 'width' => 20),
            'I' => array('title' => 'Total Litros', 'width' => 20),

            'J' => array('title' => 'Horómetro Inicial', 'width' => 20),
            'K' => array('title' => 'Horómetro Final', 'width' => 20),
            'L' => array('title' => 'Total Horas', 'width' => 20),

            'M' => array('title' => 'Litros en Mov.', 'width' => 20),
            'N' => array('title' => 'Litros en Ralenti', 'width' => 20),

            'O' => array('title' => 'Km/Lt', 'width' => 20),
            'P' => array('title' => 'Km/Lt(Teórico)', 'width' => 20),
            'Q' => array('title' => 'Litros/100kms(Teórico)', 'width' => 20),

            'R' => array('title' => 'Hs/Litro', 'width' => 20),
            'S' => array('title' => 'Hs/Litro (Teórico)', 'width' => 20),
            'T' => array('title' => 'Litros/Hs (Teórico)', 'width' => 20),



        ));
        $i = 6;

        foreach ($informe as  $data) {
            if ($data != null) {
                foreach ($data as $key => $dia) {
                    if ($consulta['servicio'] == '0') {
                        $inicio = $dia['inicio']['fecha']->format('d/m/Y H:i:s');
                        $fin = $dia['fin']['fecha']->format('d/m/Y H:i:s');
                    } elseif (substr($consulta['servicio'], 0, 1) == 'g') {
                        $inicio = $dia['inicio']['fecha']->format('d/m/Y H:i:s');
                        $fin = $dia['fin']['fecha']->format('d/m/Y H:i:s');
                    } else {
                        $inicio = $dia['inicio']['fecha']->format('H:i:s');
                        $fin = $dia['fin']['fecha']->format('H:i:s');
                    }
                    $odolitroInicio = isset($dia['inicio']['odolitro']) ? $dia['inicio']['odolitro'] : 0;
                    $odolitroFin = isset($dia['fin']['odolitro']) ? $dia['fin']['odolitro'] : '---';
                    $odolitro_movimiento = isset($dia['odolitro_movimiento']) ? $dia['odolitro_movimiento'] : 0;
                    $odolitro_ralenti = isset($dia['odolitro_ralenti']) ? $dia['odolitro_ralenti'] : 0;
                    $diff_odolitro =   $dia['diff_odolitro'] ? $dia['diff_odolitro'] : 0;

                    $xls->setRowValues($i, array(
                        'A' => array('value' => $dia['nombre']),

                        'B' => array('value' => $inicio),
                        'C' => array('value' => $fin),

                        'D' => array('value' => $dia['inicio']['odometro'], 'format' => '0.00'),
                        'E' => array('value' => $dia['fin']['odometro'], 'format' => '0.00'),
                        'F' => array('value' => $dia['diff_odometro'], 'format' => '0.00'),

                        'G' => array('value' => $odolitroInicio, 'format' => '0.00'),
                        'H' => array('value' => $odolitroFin, 'format' => '0.00'),
                        'I' => array('value' => $diff_odolitro, 'format' => '0.00'),

                        'J' => array('value' => $dia['inicio']['horometro']),
                        'K' => array('value' => $dia['fin']['horometro']),
                        'L' => array('value' => $dia['diff_horometro']),

                        'M' => array('value' => isset($dia['odolitro_movimiento']) ? $dia['odolitro_movimiento'] : 0, 'format' => '0.00'),
                        'N' => array('value' => isset($dia['odolitro_ralenti']) ? $dia['odolitro_ralenti'] : 0, 'format' => '0.00'),

                        'O' => array('value' => $dia['km_litro'], 'format' => '0.00'),
                        'P' => array('value' =>  $dia['rendimiento_teorico_km_litro'], 'format' => '0.00'),
                        'Q' => array('value' =>  $dia['rendimiento_teorico_km_litro'], 'format' => '0.00'),

                        'R' => array('value' => $dia['hrs_litro']),
                        'S' => array('value' => $dia['rendimiento_teorico_hora_litro']),
                        'T' => array('value' => $dia['rendimiento_teorico_hora']),

                    ));
                    $i++;
                }
            }
        }

        //genero el archivo.
        $response = $xls->getResponse();
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=informe_rendimiento.xls');

        // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        return $response;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\CargaCombustible');
    }
}
