<?php

namespace App\Controller\fuel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\Router\CombustibleRouter;

class MenuesController extends AbstractController
{

    private $combustibleRouter;
    public function __construct(CombustibleRouter $combustibleRouter)
    {
        $this->combustibleRouter = $combustibleRouter;
    }




    public function renderBtnListAction($servicio)
    {
        $menu = array(
            1 => array(
                $this->combustibleRouter->btnNewCarga($servicio),
                $this->combustibleRouter->btnListCarga($servicio),
                $this->combustibleRouter->btnShow($servicio),
            )
        );
        return $this->render('App:Template:navbar_list.html.twig', array(
            'menu' => $menu
        ));
    }

    public function renderBtnListCargasAction($carga)
    {
        $menu = array(
            1 => array(
                $this->combustibleRouter->btnShow($carga),
                $this->combustibleRouter->btnEdit($carga),
            )
        );
        return $this->render('app/Template/navbar_list.html.twig', array(
            'menu' => $menu
        ));
    }
}
