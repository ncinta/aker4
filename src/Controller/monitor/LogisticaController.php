<?php

namespace App\Controller\monitor;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\monitor\LogisticaType;
use App\Entity\Organizacion;
use App\Entity\Logistica;
use App\Entity\Contacto;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UsuarioManager;
use App\Model\app\PaisManager;
use App\Model\app\Router\LogisticaRouter;
use App\Model\monitor\LogisticaManager;
use App\Model\app\ServicioManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ContactoManager;
use App\Model\monitor\SatelitalManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\Router\ContactoRouter;
use App\Model\app\Router\ServicioRouter;
use App\Model\app\Router\UsuarioRouter;
use App\Form\app\ContactoType;
use App\Form\apmon\UsuarioNewType;
use App\Form\app\ServicioNewType;
use App\Form\app\ServicioType;
use App\Form\monitor\ServicioTransporteType;
use App\Model\app\TipoServicioManager;
use App\Model\monitor\ItinerarioManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class LogisticaController extends AbstractController
{

    private $breadcrumbManager;
    private $logisticaManager;
    private $logisticaRouter;
    private $organizacionManager;
    private $contactoManager;
    private $contactoRouter;
    private $servicioRouter;
    private $servicioManager;
    private $moduloMonitor;
    private $userlogin;
    private $satelitalManager;
    private $tipoServicioManager;
    private $notificadorAgenteManager;
    private $usuarioManager;
    private $usuarioRouter;
    private $paisManager;
    private $itinerarioManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        LogisticaManager $logisticaManager,
        LogisticaRouter $logisticaRouter,
        OrganizacionManager $organizacionManager,
        ContactoManager $contactoManager,
        UserloginManager $userlogin,
        SatelitalManager $satelitalManager,
        ContactoRouter $contactoRouter,
        TipoServicioManager $tipoServicioManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        ItinerarioManager $itinerarioManager,
        ServicioRouter $servicioRouter,
        ServicioManager $servicioManager,
        UsuarioManager $usuarioManager,
        PaisManager $paisManager,
        UsuarioRouter $usuarioRouter,


    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->logisticaManager = $logisticaManager;
        $this->organizacionManager = $organizacionManager;
        $this->logisticaRouter = $logisticaRouter;
        $this->contactoManager = $contactoManager;
        $this->contactoRouter = $contactoRouter;
        $this->servicioRouter = $servicioRouter;
        $this->servicioManager = $servicioManager;
        $this->satelitalManager = $satelitalManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->userlogin = $userlogin;
        $this->usuarioManager = $usuarioManager;
        $this->paisManager = $paisManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->usuarioRouter = $usuarioRouter;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    /**
     * @Route("/monitor/logistica/{idOrg}/list", name="logistica_list")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Logistica");

        $menu = array(
            1 => array($this->logisticaRouter->btnNew($organizacion)),
        );

        $logisticas = $this->logisticaManager->findAllByOrganizacion($organizacion->getId());

        return $this->render('monitor/Logistica/list.html.twig', array(
            'menu' => $menu,
            'logisticas' => $logisticas,
        ));
    }

    /**
     */

    /**
     * @Route("/monitor/logistica/{id}/show", name="logistica_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_VER")
     */
    public function showAction(Request $request, Logistica $logistica)
    {
        $this->moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
        $deleteForm = $this->createDeleteForm($logistica->getId());
        if (!$logistica) {
            throw $this->createNotFoundException('Código de logistica no encontrado.');
        }
        $arrIds = array();
        $menu = array(
            1 => array(
                $this->logisticaRouter->btnEdit($logistica),
                $this->logisticaRouter->btnDelete($logistica)
            )
        );
        if ($this->userlogin->isGranted('ROLE_MONITOR_ADMIN')) {
            $menu[2] = array(
                $this->logisticaRouter->btnNewContacto(),
                $this->servicioRouter->btnSelect(),
                $this->usuarioRouter->btnNew()
            );
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $logistica->getNombre());

        $contacto = $this->contactoManager->create($logistica->getOrganizacion());

        $usuario = $this->usuarioManager->create($logistica->getOrganizacion());

        foreach ($logistica->getServicios() as $servicio) {
            $arrIds[] = $servicio->getId();
        }
        $servicio = $this->servicioManager->create(null, $logistica->getOrganizacion());

        foreach ($servicio->getArrayStrTipoObjetos() as $key => $value) {
            $arr[$value] = $key;
        }
        $serviciosOrg = $this->itinerarioManager->getServiciosByEntity($this->userlogin->getUser(), false);

        $options = array(
            'arrayStrTipoObjetos' => $arr,
            'em' => $this->getEntityManager(),
            'organizacion' => $logistica->getOrganizacion(),
            'arrayStrTipoObjetos' => $servicio->getArrayStrTipoObjetos(),
            'required_portal' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_PORTAL'),
            'arrayStrMedidorCombustible' => $servicio->getArrayStrMedidorCombustible(),
            'monitor' => $this->moduloMonitor,
        );


        return $this->render('monitor/Logistica/show.html.twig', array(
            'menu' => $menu,
            'logistica' => $logistica,
            'formContacto' => $this->createForm(ContactoType::class, $contacto)->createView(),
            'formServicio' => $this->createForm(ServicioTransporteType::class, null, array('servicios' => $serviciosOrg))->createView(),
            'formServicioNew' => $this->createForm(ServicioNewType::class, $servicio, $options)->createView(),
            'delete_form' => $deleteForm->createView(),
            'arrIds' => json_encode($arrIds, true),
            'moduloMonitor' => $this->moduloMonitor,
            'formUsuarioNew' => $this->createForm(UsuarioNewType::class, $usuario, array('paises' =>  $this->paisManager->getArrayPaises()))->createView(),
            'usuario' => $usuario,
            'tipoNotificaciones' => $this->contactoManager->getAllTipoNotif()
           
        ));
    }

    private function getServicios($logistica)
    {
        //obtengo el array de los equipos del itinerario
        $si = array();
        foreach ($logistica->getServicios() as $s) {
            $si[$s->getId()] = $s->getId();
        }

        $servicios = array();
        $serv = $this->servicioManager->findEnabledByUsuario();
        foreach ($serv as $s) {
            if (!in_array($s->getId(), $si)) {
                $servicios[] = $s;
            }
        }
        return $servicios;
    }


    /**
     * @Route("/monitor/logistica/{idOrg}/new", name="logistica_new")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        $logistica = $this->logisticaManager->create($organizacion);
        $form = $this->createForm(LogisticaType::class, $logistica);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->logisticaManager->save($logistica)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('La logistica se ha creado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }


        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nueva Logistica");
        return $this->render('monitor/Logistica/new.html.twig', array(
            'organizacion' => $organizacion,
            'logistica' => $logistica,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/monitor/logistica/{id}/edit", name="logistica_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_EDITAR")
     */
    public function editAction(Request $request, Logistica $logistica)
    {

        if (!$logistica) {
            throw $this->createNotFoundException('Código de logistica no encontrado.');
        }

        $form = $this->createForm(LogisticaType::class, $logistica);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->logisticaManager->save($logistica)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar logistica");
        return $this->render('monitor/Logistica/edit.html.twig', array(
            'logistica' => $logistica,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/monitor/logistica/{id}/delete", name="logistica_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_ELIMINAR")
     */
    public function deleteAction(Request $request, Logistica $logistica)
    {

        $form = $this->createDeleteForm($logistica->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->breadcrumbManager->pop();

            if ($this->logisticaManager->deleteById($logistica->getId())) {
                $this->setFlash('success', 'Logistica eliminada del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar la logistica del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/monitor/logistica/contacto", name="logistica_contacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_EDITAR")
     */
    public function contactoAction(Request $request)
    {
        $idLogistica = intval($request->get('idLogistica'));
        $logistica = $this->logisticaManager->find($idLogistica);
        $usuario = null;
        if (!$logistica) {
            throw $this->createNotFoundException('Logistica no encontrado.');
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);
        if ($request->getMethod() == 'POST' && isset($tmp['contacto'])) {
            $data = $tmp['contacto'];
            
            $contacto =  new Contacto();
            $contacto->setOrganizacion($logistica->getOrganizacion());
            $contacto->setLogistica($logistica);
            
            $contacto = $this->contactoManager->setData($contacto, $data);
            
            
            if ($this->contactoManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion()) == null) { //me fijo que no exista primero
               // die('////<pre>' . nl2br(var_export($this->contactoManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion()), true)) . '</pre>////');
                if ($contacto = $this->contactoManager->save($contacto)) {
                    $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                    //NOTIF_APP para aplcicacion , NOTIF_EMAIL para email
                    $notifApp = $contacto->getByCodename('NOTIF_APP');
                    if ($notifApp) {
                        $usuario = $this->usuarioManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion());
                    }

                    $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                        'contactos' => $logistica->getContactos(),
                     

                    ));
                    return new Response(json_encode(
                        array(
                            'html' => $html,
                            'existe' => false,
                            'notifApp' => $notifApp,
                            'usuario' => $usuario,
                            'nombre' => $contacto->getNombre(),
                            'email' => $contacto->getEmail(),
                            'celular' => $contacto->getCelular()
                        )
                    ), 200);
                }
            }else{
                return new Response(json_encode(array('existe' => true)), 200);
                $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                    'contactos' => $logistica->getContactos(),
                   
                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/logistica/addservicio", name="logistica_addservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function servicioaddAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();
        $id = intval($request->get('id'));
        $idLogistica = intval($request->get('idLogistica'));
        $logistica = $this->logisticaManager->find($idLogistica);
        if (!$logistica) {
            throw $this->createNotFoundException('Logistica no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        //  die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (isset($tmp['servicio'])) {
            foreach ($tmp['servicio']['servicio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $servicio = $this->servicioManager->find($id);
                $servicio->addLogistica($logistica);
                $em->persist($servicio);
            }
            $em->flush();

            foreach ($logistica->getServicios() as $servicio) {
                $arrIds[] = $servicio->getId();
            }
            $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $logistica->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/logistica/newservicio", name="logistica_newservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function servicionewAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();

        $idLogistica = intval($request->get('idLogistica'));
        $logistica = $this->logisticaManager->find($idLogistica);
        if (!$logistica) {
            throw $this->createNotFoundException('Logistica no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        if (isset($tmp['servicio'])) {

            $servicio = $this->servicioManager->create(null, $logistica->getOrganizacion());
            $servicio->setNombre($tmp['servicio']['nombre']);
            $servicio->setDatoFiscal($tmp['servicio']['dato_fiscal']);
            $servicio->setColor($tmp['servicio']['color']);
            $servicio->setTipoObjeto($tmp['servicio']['tipoObjeto']);
            $servicio->setSatelital($this->satelitalManager->find(intval($tmp['servicio']['satelital'])));
            $servicio->setTipoServicio($this->tipoServicioManager->findById(intval($tmp['servicio']['tipoServicio'])));
            $servicio->setIsCustodio(isset($tmp['servicio']['iscustodio']));
            $servicio->addLogistica($logistica);
            if ($this->servicioManager->save($servicio)) {
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);  //1 UPDATE, 2 DELETE
                foreach ($logistica->getServicios() as $servicio) {
                    $arrIds[] = $servicio->getId();
                }
                $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                    'servicios' => $logistica->getServicios(),
                    'arrIds' => json_encode($arrIds, true),
                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/logistica/deleteservicio", name="logistica_deleteservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_AGREGAR")
     */
    public function deleteservicioAction(Request $request)
    {
        $arrIds = array();
        $id = intval($request->get('id'));
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        $idLogistica = intval($request->get('idLogistica'));
        $servicio = $this->servicioManager->find($id);
        $logistica = $this->logisticaManager->find($idLogistica);

        if (!$servicio) {
            throw $this->createNotFoundException('Servicio no encontrado.');
        }

        $servicio->removeLogistica($logistica);
        if ($this->servicioManager->save($servicio)) {
            foreach ($logistica->getServicios() as $servicio) {
                $arrIds[] = $servicio->getId();
            }
            $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $logistica->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }


    /**
     * @Route("/monitor/logistica/newusuario", name="logistica_newusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function usuarionewAction(Request $request)
    {
        $idLogistica = intval($request->get('idLogistica'));
        $logistica = $this->logisticaManager->find($idLogistica);
        if (!$logistica) {
            throw $this->createNotFoundException('Logistica no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        if (isset($tmp['usuario'])) {
            $data = $this->usuarioManager->createForMonitor($tmp, $logistica);
            if ($data === 'true') {
           //     die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');

                $html = $this->renderView('monitor/Usuario/usuarios.html.twig', array(
                    'usuarios' => $logistica->getUsuarios(),
                  

                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }

            return new Response($data, 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
