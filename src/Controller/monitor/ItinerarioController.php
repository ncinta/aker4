<?php

namespace App\Controller\monitor;

use App\Entity\Contacto;
use App\Entity\Notificacion;
use App\Entity\Organizacion;
use App\Form\app\CustodioType;
use App\Model\app\GmapsManager;
use App\Model\app\UtilsManager;
use App\Model\app\ChoferManager;
use App\Model\app\EventoManager;
use App\Model\app\UsuarioManager;
use App\Entity\ItinerarioServicio;
use App\Entity\TemplateItinerario;
use App\Model\app\ContactoManager;
use App\Model\app\GeocoderManager;
use App\Model\app\ServicioManager;
use App\Model\app\UserLoginManager;
use App\Form\monitor\ItinerarioType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ReferenciaManager;
use App\Model\monitor\EmpresaManager;
use App\Form\app\ReferenciaSelectType;
use App\Model\app\NotificacionManager;
use App\Model\app\TipoServicioManager;
use App\Model\monitor\LogisticaManager;
use App\Model\monitor\SatelitalManager;
use App\Entity\Itinerario as Itinerario;
use App\Model\app\Router\ContactoRouter;
use App\Model\app\Router\ServicioRouter;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\TransporteManager;
use App\Model\app\GrupoReferenciaManager;
use App\Form\monitor\EventoItinerarioType;
use App\Model\app\Router\ItinerarioRouter;
use App\Form\monitor\UsuarioItinerarioType;
use App\Model\app\NotificadorAgenteManager;
use App\Form\monitor\ContactoItinerarioType;
use App\Form\monitor\ServicioTransporteType;
use App\Form\monitor\HistoricoItinerarioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\monitor\BitacoraItinerarioManager;
use App\Entity\BitacoraItinerario as BitacoraItinerario;
use App\Model\app\BackendManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Psr\Log\LoggerInterface;

/**
 * Description of ItinerarioController
 *
 * @author nicolas
 */
class ItinerarioController extends AbstractController
{

    const LOGISTICA_CHROBINSON = 5;

    private $logger;
    private $logear = true;
    protected $data;

    private $breadcrumbManager;
    private $itinerarioManager;
    private $itinierarioRouter;
    private $contactoRouter;
    private $servicioRouter;
    private $servicioManager;
    private $empresaManager;
    private $transporteManager;
    private $satelitalManager;
    private $tipoServicioManager;
    private $userloginManager;
    private $usuarioManager;
    private $contactoManager;
    private $referenciaManager;
    private $notificacionManager;
    private $utilsManager;
    private $moduloMonitor;
    private $logisticaManager;
    private $eventoManager;
    private $notificadorAgenteManager;
    private $bitacoraItinerarioManager;
    private $grupoReferenciaManager;
    private $gmapsManager;
    private $geocoderManager;
    private $choferManager;
    private $backendManager;

    private $organizacion;
    private $itinerario;
    private $req;
    private $tmp;
    private $itiServicios;
    private $contactos;
    private $pois;
    private $usuarios;
    private $evento;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ItinerarioManager $itinerarioManager,
        UsuarioManager $usuarioManager,
        ItinerarioRouter $itinierarioRouter,
        ContactoRouter $contactoRouter,
        ServicioRouter $servicioRouter,
        ServicioManager $servicioManager,
        ChoferManager $choferManager,
        EmpresaManager $empresaManager,
        TransporteManager $transporteManager,
        SatelitalManager $satelitalManager,
        ContactoManager $contactoManager,
        TipoServicioManager $tipoServicioManager,
        UserLoginManager $userloginManager,
        ReferenciaManager $referenciaManager,
        NotificacionManager $notificacionManager,
        UtilsManager $utilsManager,
        LogisticaManager $logisticaManager,
        EventoManager $eventoManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        BitacoraItinerarioManager $bitacoraItinerarioManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        GmapsManager $gmapsManager,
        GeocoderManager $geocoderManager,
        BackendManager $backendManager,
        LoggerInterface $logger
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->itinierarioRouter = $itinierarioRouter;
        $this->contactoRouter = $contactoRouter;
        $this->servicioRouter = $servicioRouter;
        $this->servicioManager = $servicioManager;
        $this->empresaManager = $empresaManager;
        $this->transporteManager = $transporteManager;
        $this->satelitalManager = $satelitalManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->userloginManager = $userloginManager;
        $this->usuarioManager = $usuarioManager;
        $this->contactoManager = $contactoManager;
        $this->referenciaManager = $referenciaManager;
        $this->utilsManager = $utilsManager;
        $this->notificacionManager = $notificacionManager;
        $this->logisticaManager = $logisticaManager;
        $this->eventoManager = $eventoManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->bitacoraItinerarioManager = $bitacoraItinerarioManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->gmapsManager = $gmapsManager;
        $this->geocoderManager = $geocoderManager;
        $this->choferManager = $choferManager;
        $this->backendManager = $backendManager;
        $this->logger = $logger;
    }

    /**
     * @Route("/monitor/itinerario/{idOrg}/list", name="itinerario_list",options={"expose": true})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Itinerarios");
        $user = $this->userloginManager->getUser();
        $itinerario = new Itinerario(); // creo un objeto para poder acceder a los estados
        $menu = array(
            1 => array($this->itinierarioRouter->btnAddsList($organizacion)),
            2 => array($this->itinierarioRouter->btnChangeEstado()),
        );
        $options = array(
            'servicios' => $this->servicioManager->findAllByOrganizacion($organizacion),
            'empresas' => $this->empresaManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'satelitales' => $this->satelitalManager->findAllByOrganizacion($organizacion),
            'tipoServicio' => $this->tipoServicioManager->findAll($organizacion),
            'itinerario' => $itinerario,
        );
        $form = $this->createForm(HistoricoItinerarioType::class, null, $options);

        return $this->render('monitor/Itinerario/list.html.twig', array(
            'menu' => $menu,
            'itinerarios' => $this->itinerarioManager->findOpens($user),
            'itinerario' => $itinerario,
            'form' => $form->createView(),
            'filtro' => 'Filtro: default'
        ));
    }

    /**
     * @Route("/monitor/itinerario/{idOrg}/templatelist", name="itinerario_template_list", methods={"GET"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})     
     * @IsGranted("ROLE_TEMPLATE_ITINERARIO_VER")
     */
    public function templatelistAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Templates");
        $user = $this->userloginManager->getUser();
        $menu = array();
        $templates = array();

        foreach ($organizacion->getTemplateItinerarios() as $template) {
            if ($template->getBloqueado() != 1) {

                $templates[] = array(
                    'id' =>  $template->getId(),
                    'nombre' => $template->getNombre(),
                    'datos' => json_decode($template->getData(), true)
                );
            }
        }

        return $this->render('monitor/Template/list.html.twig', array(
            'menu' => $menu,
            'templates' => $templates

        ));
    }

    /**
     * @Route("/monitor/itinerario/{id}/show", name="itinerario_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  options={"expose": true},
     *  methods={"GET"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function showAction(Request $request, Itinerario $itinerario)
    {
        $this->moduloMonitor = $this->userloginManager->isGranted('ROLE_ITINERARIO_VER');
        $this->itinerario = $itinerario;
        $arrIds = array();
        $serviciosIti = array();
        $contactos = array();
        $organizacion =  $itinerario->getOrganizacion();
        $deleteForm = $this->createDeleteForm($itinerario->getId());
        $usuario = $this->userloginManager->getUser();
        $itiserv = new ItinerarioServicio();
        if (!$itinerario) {
            throw $this->createNotFoundException('Código de Itinerario no encontrado.');
        }

        $subMenu = array();
        $subMenu[] = [
            $this->servicioRouter->btnSelect($this->itinerario),
            $this->itinierarioRouter->btnAddCustodio($itinerario),
        ];

        $menu = array(
            1 => array(
                $this->itinierarioRouter->btnEdit($itinerario),
                $this->itinierarioRouter->btnComentario(),
            ),
            2 => array(
                $this->itinierarioRouter->btnAdds($itinerario),
                $this->itinierarioRouter->btnAddEvento($itinerario),
            ),
        );

        if ($itinerario->getEstado() != 9) {
            $menu[7] = array();
        }

        if ($itinerario->getEstado() >= 9) { //si esta cerrado borro todos los botones
            $menu = array();
        }


        foreach ($itinerario->getServicios() as $serv) {
            $arrIds[] = $serv->getServicio()->getId();
            $serviciosIti[] = $serv->getServicio();
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $itinerario->getCodigo());

        $serviciosOrg = $this->itinerarioManager->getServiciosByEntity($usuario, false, $itinerario);

        foreach ($itinerario->getNotificaciones() as $notificacion) {
            foreach ($notificacion->getContactos() as $contacto) {
                $this->contactos[] = $contacto;
            }
        }

        $options = array(
            'em' => $this->getEntityManager(),
            'organizacion' => $organizacion,
            'required_portal' => false,
            'monitor' => $this->moduloMonitor,
        );

        $optionsUsuario = $this->itinerarioManager->getUsuarios($usuario, $itinerario);
        $optionsContacto = $this->itinerarioManager->getContactos($usuario, $itinerario);

        $usuarios = $this->usuarioManager->findAll($itinerario->getOrganizacion());
        $referencias = $this->getReferenciasFree($organizacion);

        $timeline = $this->generarTimeline($itinerario);
        $provincias = $this->getEntityManager()->getRepository('App:Provincia')->findAll();

        $historicos = $this->getHistoricoEventos($itinerario);


        //  die('////<pre>' . nl2br(var_export(count($this->getHistoricoEventos($itinerario)), true)) . '</pre>////');
        //dd($timeline);
        return $this->render('monitor/Itinerario/show.html.twig', array(
            'timeline' => $timeline,
            'itiserv' => $itiserv,
            'menu' => $menu,
            'mapa' => $this->createMap($itinerario, $historicos),
            'bitacora' => $this->getBitacora($itinerario),
            'comentarios' =>  $this->bitacoraItinerarioManager->get($itinerario, BitacoraItinerario::EVENTO_ITINERARIO_COMENTARIO),
            'servicios' => $this->itinerarioManager->getServicios($itinerario),
            'serviciosIti' => $serviciosIti,
            'usuarios' => $usuarios,
            'pois' => $this->itinerarioManager->checkOrderReferencia($itinerario),
            'itinerario' => $itinerario,
            'arrIds' => json_encode($arrIds, true),
            'referencias' => $referencias,
            'eventos' =>  $this->getEventos(),
            'eventos_historico' =>  $historicos,
            'delete_form' => $deleteForm->createView(),
            'tipoNotificaciones' => array(),
            'formServicio' => $this->createForm(ServicioTransporteType::class, null, array('servicios' => $serviciosOrg))->createView(),
            'formReferencia' => $this->createForm(ReferenciaSelectType::class, null, array('referencias' => $referencias, 'select' => 2, 'provincias' => $provincias))->createView(),
            'formUsuario' => $this->createForm(UsuarioItinerarioType::class, null, array('usuarios' => $optionsUsuario))->createView(),
            'formContacto' => $this->createForm(ContactoItinerarioType::class, null, array('contactos' => $optionsContacto))->createView(),
            'formEvento' => $this->createForm(EventoItinerarioType::class)->createView(),
            'formCustodio' => $this->createForm(CustodioType::class, null, array('servicios' =>  $this->itinerarioManager->getServiciosByEntity($usuario, true, $itinerario)))->createView(),
        ));
    }

    private function validateDate($date = null)
    {
        if (is_null($date)) {
            return new \DateTime();
        } else {
            return $date;
        }
    }

    private function fitBound($lat, $lon)
    {
        if (is_float($lat) && is_float($lon)) {
            if ($this->mapBound['min_lat'] === false) {
                $this->mapBound['min_lat'] = $this->mapBound['max_lat'] = $lat;
                $this->mapBound['min_lng'] = $this->mapBound['max_lng'] = $lon;
            } else {
                $this->mapBound['min_lat'] = min($this->mapBound['min_lat'], $lat);
                $this->mapBound['max_lat'] = max($this->mapBound['max_lat'], $lat);
                $this->mapBound['min_lng'] = min($this->mapBound['min_lng'], $lon);
                $this->mapBound['max_lng'] = max($this->mapBound['max_lng'], $lon);
            }
        }
        return true;
    }

    private $mapBound = ['min_lat' => false, 'max_lat' => false, 'min_lng' => false, 'max_lng' => false];

    private function createMap($itinerario, $historicos)
    {
        $mapa = $this->gmapsManager->createMap(true);
        $mapa->setSize('100%', '700px');
        $mapa->setIniZoom(8);

        $status = array();
        $referencia = array();
        $colores = array();
        $consulta_historial = array();
        foreach ($itinerario->getPois() as $poi) {
            $referencia = $poi->getReferencia();
            $referencia->setNombre('[' . $poi->getOrden() . '] - ' . $referencia->getNombre());
            $referencias[] = $referencia;
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, true);
            $this->fitBound($referencia->getLatitud(), $referencia->getLongitud());
        }

        foreach ($itinerario->getServicios() as $servIti) {
            $servicio = $servIti->getServicio();
            
            $fechaInicio = null;
            if (!is_null($servicio->getUltLatitud())) {
                $colores['servicio' . $servicio->getColor()] = is_null($servicio->getColor()) ? '#FFA500' : '#' . $servicio->getColor();  //hago un array con los colores.
                $marker = $this->gmapsManager->addMarkerServicio($mapa, $servicio, true);

                if ($servicio->getUltValido() && $servicio->getUltLatitud() != null && $servicio->getUltLongitud() != null) {
                    $this->fitBound($servicio->getUltLatitud(), $servicio->getUltLongitud());
                }
            }

            if ($itinerario->getEstado() === Itinerario::ESTADO_AUDITORIA || $itinerario->getEstado() === Itinerario::ESTADO_ENCURSO) {
                if ($itinerario->getEstado() == Itinerario::ESTADO_AUDITORIA) { // que busque la fecha en la bitacora
                    $estados = $this->bitacoraItinerarioManager->get($itinerario, BitacoraItinerario::EVENTO_ITINERARIO_ESTADO_UPDATE); //buscamos los cambios de estado en el itiner
                    foreach ($estados as $estado) {
                        if (
                            $estado->getData()['estado'] == $itinerario->getStrEstado(Itinerario::ESTADO_AUDITORIA)
                            && isset($estado->getData()['fecha_inicio']) && $estado->getData()['fecha_inicio'] != null
                        ) { //buscamos en la bitac. el estado de auditoria
                            $fechaInicio = $estado->getData()['fecha_inicio']->format('Y/m/d H:i:s');
                        }
                    }
                    //die('////<pre>' . nl2br(var_export($estados[0]->getData(), true)) . '</pre>////');
                }
                if ($itinerario->getEstado() == Itinerario::ESTADO_ENCURSO && $itinerario->getFechaInicio() != null) { // tomamos la fecha de inicio del itinerario
                    $fechaInicio = $itinerario->getFechaInicio()->format('Y/m/d H:i:s');
                }
                //$fechaInicio = $itinerario->getFechaInicio()->format('Y/m/d H:i:s');

                if ($fechaInicio != null) { // si no tiene fechaInicio que ni entre porque no está en curso, ni en auditoría
                    $puntos_polilinea = array();
                    $fechaFin = (new \DateTime())->format('Y/m/d H:i:s');
                   // $fechaFin = $itinerario->getFechaFin()->format('Y/m/d H:i:s');
                    try {
                        $consulta_historial = $this->backendManager->historial(
                            $servicio->getId(),
                            $fechaInicio,
                            $fechaFin,
                            $referencias,
                            array()
                        );
                        //dd($consulta_historial);
                        while ($trama = current($consulta_historial)) {
                            next($consulta_historial);
                            if (isset($trama['posicion'])) {
                                $puntos_polilinea[] = array($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                                $this->fitBound($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                            }
                        }

                        $color = $this->utilsManager->getColor(rand(0, 12));
                        $stroke = $servicio->getIsCustodio() ? 2 : 5; // si el servicio es custodio que lo resalte menos. 
                        $this->gmapsManager->addRecorridoWithStrokeWeight($mapa, $servicio->getNombre(), $puntos_polilinea, $color, $stroke);
                        // die('////<pre>' . nl2br(var_export($consulta_historial, true)) . '</pre>////');
                    } catch (\Exception $exc) {
                        return null;
                    }
                }
            }
        }

        foreach ($historicos as $evento) {
            if (is_float($evento['latitud']) && is_float($evento['longitud'])) {
                $ptoEvento = $this->gmapsManager->createPosicionIcon($mapa,  $evento['titulo'], 'ballred.png');
                $posicion = array('latitud' => $evento['latitud'], 'longitud' => $evento['longitud']);
                $nombre = $evento['fecha'] . ' - ' . $evento['titulo'];
                $this->gmapsManager->addMarkerPosicion($mapa, $nombre, $posicion, $ptoEvento);
                $this->fitBound($evento['latitud'], $evento['longitud']);
            }
        }

        $mapa->fitBounds($this->mapBound['min_lat'], $this->mapBound['min_lng'], $this->mapBound['max_lat'], $this->mapBound['max_lng']);
        return $mapa;
    }

    private function getCercania($servicio, $referencias)
    {
        if ($referencias) {
            $cercanas = $this->servicioManager->buscarCercania($servicio, $referencias);
            if (count($cercanas) > 0) {
                $i = 0;
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        break;
                    }
                }

                if (!is_null($cercanas)) {
                    $status['leyenda'] = $cercanas[$i]['leyenda'];
                    $status['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                } else {
                    $status['leyenda'] = '-!!!';
                }
            }
            return $status;
        }
        return null;
    }

    private function generarNodoServicio($iti_servicio, $referencias)
    {

        //obtengo el status del servicio para renderizar la data...
        $status = array(
            'st_ultreporte' => $this->servicioManager->getStatusUltReporte($iti_servicio->getServicio()),
            'direccion' => $this->geocoderManager->inverso($iti_servicio->getServicio()->getUltLatitud(), $iti_servicio->getServicio()->getUltLongitud()),
            'icono' => $this->gmapsManager->getIcono($iti_servicio->getServicio()->getUltDireccion(), $iti_servicio->getServicio()->getUltVelocidad()),
            'contacto' => false,
            'cerca' => $this->getCercania($iti_servicio->getServicio(), $referencias),
        );

        $nodo = [
            'fecha' => $this->validateDate($iti_servicio->getServicio()->getUltFechahora()),
            'tipo' => 'servicio',
            'color' => 'tm-service',
            'icono' => $iti_servicio->getServicio()->getIsCustodio() ? 'fa fa-car' : 'fa fa-truck',
            'data' => $this->renderView('monitor/Itinerario/timeline/nodo_servicio.html.twig', [
                'servicio' => $iti_servicio->getServicio(),
                'iti_servicio' => $iti_servicio,
                'status' => $status
            ]),
        ];
        return $nodo;
    }

    private $timeline = array();

    private function addNodo($nodo)
    {
        $exito = false;
        $timestamp = $nodo['fecha']  ? $nodo['fecha']->getTimestamp() : false;
        if (!$timestamp) {
            // die('////<pre>' . nl2br(var_export($nodo, true)) . '</pre>////');
            return true;
        }
        do {
            if (!isset($this->timeline[$timestamp])) {
                $this->timeline[$timestamp] = $nodo;
                $exito = true;
            }
            $timestamp++;
        } while (!$exito);
        return true;
    }

    public function generarTimeline($itinerario)
    {
        $this->timeline = array();
        $now = new \DateTime();
        //agregado de cambios de estado en bitacora
        $estados = $this->bitacoraItinerarioManager->get($itinerario, BitacoraItinerario::EVENTO_ITINERARIO_ESTADO_UPDATE);
        foreach ($estados as $estado) {
            $nodo = [
                'fecha' => $estado->getCreatedAt(),
                'tipo' => 'estado',
                'icono' => 'fa fa-retweet',
                'color' => 'tm-warning',
                'data' => $this->renderView('monitor/Itinerario/timeline/nodo_estado.html.twig', ['estado' => $estado]),
            ];
            $this->addNodo($nodo);
        }


        //agregado de los comentarios
        $comentarios = $this->bitacoraItinerarioManager->get($itinerario, BitacoraItinerario::EVENTO_ITINERARIO_COMENTARIO);
        foreach ($comentarios as $comentario) {
            $nodo = [
                'fecha' => $comentario->getCreatedAt(),
                'tipo' => 'comentario',
                'icono' => 'fa fa-comment',
                'color' => 'tm-comment',
                'data' => $this->renderView('monitor/Itinerario/timeline/nodo_comentario.html.twig', ['comentario' => $comentario]),
            ];
            $this->addNodo($nodo);
        }

        //agrego las referencias        
        $referencias = null;
        $pois = $itinerario->getPois();
        foreach ($pois as $poi) {
            $referencias[] = $poi->getReferencia();
            if (is_null($poi->getFechaIngreso())) {
                $fecha = $this->utilsManager->fechaLocal2UTC($itinerario->getFechaInicio());
            } else {
                $fecha = $this->utilsManager->fechaLocal2UTC($poi->getFechaIngreso());
            }
            $nodo = [
                'fecha' => $fecha,
                'tipo' => 'poi',
                'color' => 'tm-poi',
                'icono' => 'fa fa-map-marker',
                'data' => $this->renderView('monitor/Itinerario/timeline/nodo_poi.html.twig', ['poi' => $poi]),
            ];
            $this->addNodo($nodo);
        }

        //agrego los servicios
        $is = $itinerario->getServicios(); //$is itinerarioServicio
        foreach ($is as $servicio) {
            $nodo = $this->generarNodoServicio($servicio, $referencias);
            $this->addNodo($nodo);
        }

        //informacion
        $info = [
            'fecha' => $itinerario->getCreated_At(),
            'tipo' => 'inicio',
            'icono' => 'fa fa-info',
            'color' => 'tm-comment',
            'data' => $this->renderView('monitor/Itinerario/timeline/nodo_informacion.html.twig', ['itinerario' => $itinerario]),
        ];
        $this->addNodo($info);

        //agrego el inicio        
        $this->addNodo([
            'fecha' => $itinerario->getFechaInicio() ? $this->utilsManager->fechaLocal2UTC($itinerario->getFechaInicio()) : null,
            'tipo' => 'inicio',
            'icono' => 'fa fa-play',
            'color' => 'tm-start',
            'data' => $this->renderView('monitor/Itinerario/timeline/nodo_inicio.html.twig', ['itinerario' => $itinerario]),
        ]);

        //agrego el fin        
        $this->addNodo([
            'fecha' => $itinerario->getFechaFin() ? $this->utilsManager->fechaLocal2UTC($itinerario->getFechaFin()) : null,
            'tipo' => 'inicio',
            'icono' => 'fa fa-stop',
            'color' => 'tm-start',
            'data' => $this->renderView('monitor/Itinerario/timeline/nodo_fin.html.twig', ['itinerario' => $itinerario]),
        ]);


        //agrego los eventos...
        $evIts = $itinerario->getEventoItinerario();
        foreach ($evIts as $ev) {
            $evsH = $ev->getHistoricoItinerario();
            if ($evsH) {
                foreach ($evsH as $ev) {
                    //agrego los eventos
                    //$fecha = $this->utilsManager->fechaLocal2UTC($ev->getFecha());
                    $fecha = $ev->getFecha();
                    $inicio = [
                        'fecha' => $fecha,
                        'tipo' => 'evento',
                        'icono' => 'fa fa-asterisk',
                        'color' => 'tm-danger',
                        'data' => $this->renderView(
                            'monitor/Itinerario/timeline/nodo_evento.html.twig',
                            [
                                'itinerario' => $itinerario,
                                'evento' => $ev,
                                'direccion' => $this->geocoderManager->inverso($ev->getServicio()->getUltLatitud(), $ev->getServicio()->getUltLongitud()),
                                'cerca' => $this->getCercania($ev->getServicio(), $referencias),
                            ]
                        ),
                    ];
                    $this->addNodo($inicio);
                }
            }
        }
        //ordenado de todo...
        ksort($this->timeline);
        return $this->timeline;
    }

    private function getBitacora($itinerario = null)
    {
        if (is_null($itinerario)) {
            $registro = $this->bitacoraItinerarioManager->getRegistros($this->itinerario);
        } else {
            $registro = $this->bitacoraItinerarioManager->getRegistros($itinerario);
        }

        $bitacora = array();
        foreach ($registro as $value) {
            //  die('////<pre>' . nl2br(var_export( BitacoraItinerario::EVENTO_ITINERARIO_COMENTARIO, true)) . '</pre>////');
            $bitacora[] = array(
                'registro' => $value,
                'descripcion' => $this->bitacoraItinerarioManager->parse($value),
            );
        }
        return $bitacora;
    }

    private function getReferenciasFree($organizacion)
    {
        $referencias = $this->referenciaManager->findAsociadas();
        return $referencias;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getEventos()
    {
        $param = array();
        $statusError = false;
        $servicios = $this->itinerario->getServicios();
        $eventosIti = $this->eventoManager->findAllByItinerario($this->itinerario);
        // die('////<pre>' . nl2br(var_export(count($eventosIti), true)) . '</pre>////');
        foreach ($eventosIti as $evIti) {

            if ($evIti->getEvento() && $evIti->getTipoEvento()) {
                $param[$evIti->getId()] = [
                    'id' => $evIti->getId(),
                    'tipoEvento' => ['nombre' => $evIti->getTipoEvento()->getNombre(), 'codename' => $evIti->getTipoEvento()->getCodename()],
                    'alarmas' => $evIti->getHistoricoItinerario() != null ? count($evIti->getHistoricoItinerario()) : null
                ];


                $param[$evIti->getId()]['evento'] = $evIti->getEvento();
            }

            $status = $this->eventoManager->getStatusAgentes($evIti->getEvento());

            foreach ($servicios as $servicio) {
                if (!is_null($status)) {
                    $param[$evIti->getId()]['servicios'][$servicio->getServicio()->getId()] = array(
                        'servicio' => $servicio,
                        'inServicio' => $this->eventoManager->inServicio($servicio->getServicio(), $evIti->getEvento()),
                        'status' => in_array($servicio->getServicio()->getId(), isset($status['servicios']) ? $status['servicios'] : []),

                    );
                    if (!$statusError) {
                        $statusError = !in_array($servicio->getServicio()->getId(), isset($status['servicios']) ? $status['servicios'] : []);
                    }
                }
            }
        }

        // $param['statusError'] = $statusError;

        //esto es para que si el it esta cerrado no pueda reconfigurar los eventos.
        if ($this->itinerario->getEstado() == 9) {
            $statusError = false;
        }
        return ['eventos' => $param, 'statusError' => $statusError];
    }

    private function getHistoricoEventos($itinerario)
    {
        $historicos = array();
        // que muestre eventos si  el itinerario se encuentra en auditoría o en curso
        if ($itinerario->getEstado() == Itinerario::ESTADO_AUDITORIA || $itinerario->getEstado() == Itinerario::ESTADO_ENCURSO) {

            $eventosIti = $this->eventoManager->findAllByItinerario($itinerario);
            foreach ($eventosIti as $evIti) {
                if ($evIti->getHistoricoItinerario()) {
                    foreach ($evIti->getHistoricoItinerario() as $historico) {
                        $historicos[$historico->getId()] = [
                            'id' => $historico->getId(),
                            'titulo' => $historico->getTitulo(),
                            'ejecutor' => $historico->getEjecutor() ? $historico->getEjecutor()->getNombre() : '---',
                            'latitud' => $historico->getData() && isset($historico->getData()['data']['posicion']['latitud']) ? $historico->getData()['data']['posicion']['latitud'] : '---',
                            'longitud' => $historico->getData() && isset($historico->getData()['data']['posicion']['longitud']) ? $historico->getData()['data']['posicion']['longitud'] : '---',
                            'fecha' => $historico->getFecha() ? $historico->getFecha()->format('d/m/Y H:i:s') : '---',
                            'fecha_vista' => $historico->getFechaVista() ? $historico->getFechaVista()->format('d/m/Y H:i:s') : '---',
                            'respuesta' => $historico->getRespuesta() != null && $historico->getRespuesta() != '' ? $historico->getRespuesta() : '---',
                            'tipoEvento' => ['nombre' => $evIti->getTipoEvento()->getNombre(), 'codename' => $evIti->getTipoEvento()->getCodename()],
                            'datos' => $historico->getData() && isset($historico->getData()['cuerpo']) ?  $historico->getData()['cuerpo'] : null,
                        ];
                    }
                    // die('////<pre>' . nl2br(var_export($historicos, true)) . '</pre>////');
                }
            }
        }

        return $historicos;
    }

    /**
     * @Route("/monitor/itinerario/{idOrg}/new", name="itinerario_new")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }
        $usuario = $this->userloginManager->getUser();
        //creo el itinerario
        $itinerario = $this->itinerarioManager->create($organizacion, $usuario);
        //seteo las opciones del itinerario segun lo que necesito
        $options = [
            'organizacion' => $organizacion,
            'empresas' => $this->empresaManager->findByUsuario($usuario),
            'userlogin' => $usuario,
            'em' => $this->getEntityManager(),
        ];
        $form = $this->createForm(ItinerarioType::class, $itinerario, $options);

        $optionsUsuario = $this->itinerarioManager->getUsuarios($usuario, null);
        $optionsContacto = $this->itinerarioManager->getContactos($usuario, null);
        $referencias = $this->getReferenciasFree($organizacion);
        $provincias = $this->getEntityManager()->getRepository('App:Provincia')->findAll();

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Itinerario");
        return $this->render('monitor/Itinerario/new.html.twig', array(
            'organizacion' => $organizacion,
            'userlogin' => $usuario,
            'form' => $form->createView(),
            'formServicio' => $this->createForm(ServicioTransporteType::class, null, array('servicios' => $this->itinerarioManager->getServiciosByEntity($usuario, false, null)))->createView(),
            'formReferencia' => $this->createForm(ReferenciaSelectType::class, null, array('referencias' => $referencias,  'provincias' => $provincias))->createView(),
            'formUsuario' => $this->createForm(UsuarioItinerarioType::class, null, array('usuarios' => $optionsUsuario))->createView(),
            'formContacto' => $this->createForm(ContactoItinerarioType::class, null, array('contactos' => $optionsContacto))->createView(),
            'formEvento' => $this->createForm(EventoItinerarioType::class)->createView(),
            'formCustodio' => $this->createForm(CustodioType::class, null, array('servicios' =>  $this->itinerarioManager->getServiciosByEntity($usuario, true, null)))->createView(),
        ));
    }

    /**
     * @Route("/monitor/itinerario/{idOrg}/create", name="itinerario_create")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_AGREGAR")
     */
    public function createAction(Request $request, Organizacion $organizacion)
    {

        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }
        $this->organizacion = $organizacion;
        //creo el itinerario


        parse_str($request->get('formItinerario'), $tmp);
        $this->data = $tmp['itinerario'];
        //die('////<pre>' . nl2br(var_export($tmp['itinerario'], true)) . '</pre>////');

        $this->req = $request;
        $this->createItinerario();

        if ($this->itinerarioManager->save($this->itinerario)) {
            $this->createContenido();

            if ($request->get('create_template') === 'true') {
                //aca empiezo de crear el template
                $template = $this->createTemplate();
            }

            $this->setFlash('success', sprintf('El itinerario se ha creado con éxito'));


            return new Response(json_encode(array('status' => 'ok', 'id' => $this->itinerario->getId())), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    private function createItinerario()
    {

        $fi = new \DateTime($this->utilsManager->datetime2sqltimestamp($this->data['fechaInicio'], false), new \DateTimeZone($this->organizacion->getTimezone()));
        $ff = new \DateTime($this->utilsManager->datetime2sqltimestamp($this->data['fechaFin'], false), new \DateTimeZone($this->organizacion->getTimezone()));
        $usuario = $this->userloginManager->getUser();

        $empresa = $usuario->getEmpresa() ? $usuario->getEmpresa() : $this->empresaManager->find(intval($this->data['empresa']));
        $logistica = $usuario->getLogistica() ? $usuario->getLogistica() : $this->logisticaManager->find(intval($this->data['logistica']));

        $this->itinerario = $this->itinerarioManager->create($this->organizacion);
        $this->itinerario->setCodigo($this->data['codigo']);
        $this->itinerario->setEstado(Itinerario::ESTADO_NUEVO);
        $this->itinerario->setNota($this->data['nota']);
        $this->itinerario->setFechaInicio($fi);
        $this->itinerario->setFechaFin($ff);
        $this->itinerario->setEmpresa($empresa);
        $this->itinerario->setLogistica($logistica);
        $this->itinerario->addUsuario($usuario);
        $this->itinerario->setModoIngreso(intval($this->req->get('modoIngreso')));
    }

    /**
     * @Route("/monitor/template/{id}/createtemplate", name="monitor_template_createiti")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_AGREGAR")
     */
    public function crearitifromtempAction(Request $request, TemplateItinerario $template)
    {

        if (!$template) {
            throw $this->createNotFoundException('Template no encontrado.');
        }
        $usuario = $this->userloginManager->getUser();
        $organizacion = $template->getOrganizacion();
        //creo el itinerario
        $itinerario = $this->itinerarioManager->create($organizacion, $usuario);
        //seteo las opciones del itinerario segun lo que necesito
        $options = [
            'organizacion' => $organizacion,
            'empresas' => $this->empresaManager->findByUsuario($usuario),
            'userlogin' => $usuario,
            'em' => $this->getEntityManager(),
        ];
        $form = $this->createForm(ItinerarioType::class, $itinerario, $options);

        $optionsUsuario = $this->itinerarioManager->getUsuarios($usuario, null);
        $optionsContacto = $this->itinerarioManager->getContactos($usuario, null);
        $referencias = $this->getReferenciasFree($organizacion);
        $provincias = $this->getEntityManager()->getRepository('App:Provincia')->findAll();

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Itinerario");
        // die('////<pre>' . nl2br(var_export('asd', true)) . '</pre>////');
        $data =  json_decode($template->getData(), true);
        return $this->render('monitor/Itinerario/new.html.twig', array(
            'organizacion' => $organizacion,
            'template' => $template,
            'data' => $data,
            'userlogin' => $usuario,
            'form' => $form->createView(),
            'formServicio' => $this->createForm(ServicioTransporteType::class, null, array('servicios' => $this->itinerarioManager->getServiciosByEntity($usuario, false, null)))->createView(),
            'formReferencia' => $this->createForm(ReferenciaSelectType::class, null, array('referencias' => $referencias, 'provincias' => $provincias))->createView(),
            'formUsuario' => $this->createForm(UsuarioItinerarioType::class, null, array('usuarios' => $optionsUsuario))->createView(),
            'formContacto' => $this->createForm(ContactoItinerarioType::class, null, array('contactos' => $optionsContacto))->createView(),
            'formEvento' => $this->createForm(EventoItinerarioType::class)->createView(),
            'formCustodio' => $this->createForm(CustodioType::class, null, array('servicios' =>  $this->itinerarioManager->getServiciosByEntity($usuario, true, null)))->createView(),
        ));
    }


    private function createTemplate()
    {
        $em = $this->getEntityManager();
        $template = new TemplateItinerario();
        $template->setOrganizacion($this->itinerario->getOrganizacion());
        $template->setNombre($this->req->get('template_nombre'));
        $data = array();

        $tmp = array();
        parse_str($this->req->get('formItinerario'), $tmp);

        $d = $tmp['itinerario'];

        $data['servicios'] = null;
        $data['contactos'] = null;
        $data['usuarios'] = null;
        $data['referencias'] = null;
        $data['eventos'] = null;
        $data['custodios'] = null;
        $data['nota'] = $d['nota'];

        $data['logistica'] = $this->itinerario->getLogistica() ? array('id' => $this->itinerario->getLogistica()->getId(), 'nombre' => $this->itinerario->getLogistica()->getNombre()) : null;
        $data['cliente'] = $this->itinerario->getEmpresa() ? array('id' => $this->itinerario->getEmpresa()->getId(), 'nombre' => $this->itinerario->getEmpresa()->getNombre()) : null;


        $servicios_json = json_decode($this->req->get('formServicio'), true);

        foreach ($servicios_json as $value) {
            //die('////<pre>' . nl2br(var_export($value, true)) . '</pre>////');
            $id = intval($value['id']);
            $servicio = $this->servicioManager->find($id);
            $data['servicios'][$servicio->getId()] = array(
                'id' => $servicio->getId(),
                'nombre' => $servicio->getNombre(),
                'nro_contenedor' => $value['nro_contenedor'],
                'nro_bill' => $value['bill'],
                'patente_acoplado' => $value['patente_acoplado']
            );
            $chofer = $value['chofer'] !== '' ? $this->choferManager->find(intval($value['chofer'])) : null;

            if ($chofer) {
                $data['servicios'][$servicio->getId()]['chofer'] = array(
                    'id' => $chofer->getId(),
                    'nombre' => $chofer->getNombre()
                );
            }
        }
        parse_str($this->req->get('selectServ'), $tmp);
        if (isset($tmp['servicio'])) {
            foreach ($tmp['servicio']['servicio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = intval(substr($value, 0, $pos - 1));
                $data['servicios'][$id]['val_select'] = $value;
            }
        }


        parse_str($this->req->get('formCustodio'), $tmp);
        if (isset($tmp['custodio'])) {
            foreach ($tmp['custodio']['custodio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $servicio = $this->servicioManager->find($id);
                $data['custodios'][$servicio->getId()] = array(
                    'id' => $servicio->getId(),
                    'nombre' => $servicio->getNombre(),
                    'val_select' => $value,
                );
            }
        }

        $referencias = json_decode($this->req->get('formReferencia'), true);

        foreach ($referencias as $ref) {
            //die('////<pre>' . nl2br(var_export($ref, true)) . '</pre>////');
            $referencia = $this->referenciaManager->find($ref['id']);
            $data['referencias'][$referencia->getId()] = array(
                'id' => $referencia->getId(),
                'nombre' => $referencia->getNombre(),
                'orden' => $ref['orden'],
                'ingreso' => $ref['arribo'] != 'Arribo' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($ref['arribo'], false)) : 'Arribo',
                'egreso' => $ref['salida'] != 'Salida' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($ref['salida'], false)) : 'Salida',
                'notificaciones' => null, //habŕa que crearlas cuando cuando se utilicen 
            );
        }

        parse_str($this->req->get('formContacto'), $tmp);
        if (isset($tmp['contactos'])) {
            foreach ($tmp['contactos']['contacto'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $contacto = $this->contactoManager->find($id);
                $data['contactos'][$contacto->getId()] = array(
                    'id' => $contacto->getId(),
                    'nombre' => $contacto->getNombre(),
                    'val_select' => $value,

                );
            }
        }

        parse_str($this->req->get('formUsuario'), $tmp);

        if (isset($tmp['usuario'])) {
            foreach ($tmp['usuario']['usuario'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $usuario = $this->usuarioManager->find($id);
                $data['usuarios'][$usuario->getId()] = array(
                    'id' => $usuario->getId(),
                    'nombre' => $usuario->getNombre(),
                    'val_select' => $value

                );
            }
        }

        parse_str($this->req->get('formEvento'), $tmp);
        //   die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        isset($tmp['eventos']['pois']) ? $data['eventos']['pois'] = 1  : $data['eventos']['pois'] = 0;
        isset($tmp['eventos']['panico']) ? $data['eventos']['panico'] = 1  : $data['eventos']['panico'] = 0;
        isset($tmp['eventos']['sensores']) ? $data['eventos']['sensores'] = 1  : $data['eventos']['sensores'] = 0;
        isset($tmp['eventos']['detencion']) ? $data['eventos']['detencion'] =  (int)$tmp['eventos']['detencion_tiempo'] : 0;


        $template->setData(json_encode($data, true));
        $em->persist($template);
        $em->flush();
    }

    private function createContenido()
    {
        $em = $this->getEntityManager();


        $pois = array();
        $contactos = array();
        $tmp = array();

        $bitacora = $this->bitacoraItinerarioManager->newItinerario($this->userloginManager->getUser(), $this->itinerario);

        $this->itiServicios = $this->setServicios();
        $bitacora = $this->bitacoraItinerarioManager->updateServicio($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getServicios());


        parse_str($this->req->get('formCustodio'), $tmp);
        if (isset($tmp['custodio'])) {
            foreach ($tmp['custodio']['custodio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $servicio = $this->servicioManager->find($id);
                $itiServ = new ItinerarioServicio();
                $itiServ->setItinerario($this->itinerario);
                $itiServ->setServicio($servicio);
                $em->persist($itiServ);
            }
            $em->flush();
            $bitacora = $this->bitacoraItinerarioManager->updateCustodios($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getServicios());
        }


        $referencias = json_decode($this->req->get('formReferencia'), true);
        // die('////<pre>' . nl2br(var_export($referencias, true)) . '</pre>////');
        foreach ($referencias as $ref) {
            $stopType = $ref['stopType'] != 0 ? $ref['stopType'] : null;
            $referencia = $this->referenciaManager->find($ref['id']);
            $poi = $this->itinerarioManager->addPoi($this->itinerario, $referencia, $ref['orden'], null, $stopType);
            $hArribo = $ref['arribo'] != 'Arribo' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($ref['arribo'], false)) : null;
            $hPartida = $ref['salida'] != 'Salida' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($ref['salida'], false)) : null;
            $clock = $this->itinerarioManager->setClock($poi, $hArribo, $hPartida);
            $this->pois[] = $poi;
        }
        $bitacora = $this->bitacoraItinerarioManager->updateReferencia($this->userloginManager->getUser(), $this->itinerario, $pois);




        parse_str($this->req->get('formContacto'), $tmp);
        if (isset($tmp['contactos'])) {
            foreach ($tmp['contactos']['contacto'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $contacto = $this->contactoManager->find($id);
                $x = $this->itinerarioManager->addContacto($this->itinerario, $contacto);
                $this->contactos[] = $contacto;
            }
            $bitacora = $this->bitacoraItinerarioManager->updateContactos($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getContactos());
        }

        parse_str($this->req->get('formUsuario'), $tmp);
        if (isset($tmp['usuario'])) {
            foreach ($tmp['usuario']['usuario'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $usuario = $this->usuarioManager->find($id);
                $this->usuarios[] = $this->usuarioManager->find($id);
                $x = $this->itinerarioManager->addUsuario($this->itinerario, $usuario);
            }
            $bitacora = $this->bitacoraItinerarioManager->updateUsuarios($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getUsuarios());
        }

        parse_str($this->req->get('formEvento'), $tmp);

        if (isset($tmp['eventos']['pois']) || isset($tmp['eventos']['panico']) || isset($tmp['eventos']['detencion']) || isset($tmp['eventos']['sensores'])) {
            $createEv = $this->createEventos($tmp);
        }
    }

    private function createEventos($tmp)
    {
        //  die('////<pre>' . nl2br(var_export($tmp , true)) . '</pre>////');
        $eventos = array();
        if (isset($tmp['eventos']['pois'])) {
            $this->createEvento('E/S #', 'EV_IO_REFERENCIA', $tmp);
        }

        if (isset($tmp['eventos']['panico'])) {
            //crear evento panico
            $this->createEvento('PAN #', 'EV_PANICO', $tmp);
        }

        if (isset($tmp['eventos']['detencion'])) {
            $this->createEvento('DET #', 'EV_DETENIDO', $tmp);
        }

        if (isset($tmp['eventos']['sensores'])) {
            $this->createEvento('MOTIVO #', 'EV_MOTIVOTX', $tmp);
        }

        return true;
    }

    private function createEvento($prefix, $codename, $tmp)
    {
        //crear evento de e/s 
        $nombre = $prefix . $this->itinerario->getCodigo();
        $this->evento = $this->eventoManager->setForItinerario($nombre, $codename, $this->organizacion);

        if ($codename == 'EV_IO_REFERENCIA') {
            $grupo = $this->grupoReferenciaManager->create($this->organizacion); //le creo un grupo temporal hasta que se cierre o elimine
            $grupo->setNombre("ITINERARIO-" . $this->itinerario->getCodigo()); // pongo nombre con codigo tmp para poder buscarlo y eliminarlo despues
            if ($this->pois) {
                foreach ($this->pois as $poi) {
                    $this->grupoReferenciaManager->addReferencia($poi->getReferencia());
                }
            }

            $grupo = $this->grupoReferenciaManager->save($grupo);
            $this->itinerario->setGrupoReferencia($grupo);
            $param = $this->eventoManager->addParam($this->evento, 'VAR_GRUPO_REFERENCIA_IN', $grupo->getId());
            //notificar el grupo de ref
            $notificar = $this->notificadorAgenteManager->notificar($grupo, 1);
        } else if ($codename == 'EV_DETENIDO') {
            //crear evento detencion 
            $tiempo =  (int)$tmp['eventos']['detencion_tiempo'];
            $param = $this->eventoManager->addParam($this->evento, 'VAR_TIEMPO_DETENIDO', $tiempo);
        } else if ($codename == 'EV_MOTIVOTX') {
            $motivos = array(
                'VAR_PUERTACHOFER',
                'VAR_PUERTACABINA',
                'VAR_PUERTATRASERA',
                'VAR_ANTIVANDALICO',
                'VAR_REDUCCIONVELOCIDAD',
                'VAR_ENCENDIDO',
                'VAR_APAGADO',
                'VAR_CORTECOMBUSTIBLE',
                'VAR_ENGANCHE',
                'VAR_DESENGANCHE',
                'VAR_FRENADA',
                'VAR_ACELERACION',
            );
            foreach ($motivos as $motivo) {
                $param = $this->eventoManager->addParam($this->evento, $motivo, 1);
            }
        } else {
            $codename = 'EV_PANICO';
            // nada
        }

        // die('////<pre>' . nl2br(var_export($codename , true)) . '</pre>////');
        $evItinerario = $this->eventoManager->setEventoItinerario($this->evento, $this->itinerario, $codename);
        $addServicios = $this->addServicios();

        if ($this->itinerario->getEstado() == 1 || $this->itinerario->getEstado() == 2) { // ya está en curso o auditoría, activarlos
            $this->evento->setActivo(true);
            $this->evento = $this->eventoManager->save($this->evento);
        }

        $bitacora = $this->bitacoraItinerarioManager->updateEvento(
            $this->userloginManager->getUser(),
            $this->itinerario,
            $this->evento,
            $this->evento->getActivo() ? 'Activo' : 'Inactivo'
        );
        $notif = $this->createNotificacion();
        $notificar = $this->notificadorAgenteManager->notificar($this->evento, 1);
        return;
    }

    /**
     * para un itinerario y un evento, se agregan los servicios del itinerario al evento.
     */
    private function addServicios()
    {
        $em = $this->getEntityManager();
        //ahora grabo los servicios.
        if ($this->evento) {
            foreach ($this->itiServicios as $itiServicio) {
                $this->evento->addServicio($itiServicio->getServicio());
                $em->persist($this->evento);
            }
            $em->flush();
        }
    }

    private function createNotificacion()
    {
        if ($this->contactos) {
            //ahora grabo los contactos
            foreach ($this->contactos as $contacto) {
                foreach ($contacto->getTipoNotificaciones() as $tipo) {
                    $notif = 2;
                    if ($tipo->getCodename() == 'NOTIF_APP') {
                        $notif = 1;
                    } elseif ($tipo->getCodename() == 'NOTIF_EMAIL') {
                        $notif = 2;
                    } else {
                        $notif = 2; // si no viene nada o si no viene ninguno de esos codename, que notifique por mail
                    }
                    $evento = $this->eventoManager->addNotificacion(
                        $this->evento,
                        $contacto,
                        $notif
                    );
                }
            }
        }

        return true;
    }

    private function deleteNotificacion($evento, $contacto)
    {
        $em = $this->getEntityManager();
        $notificaciones = $this->eventoManager->findNotifByEvContacto(
            $evento,
            $contacto,
        );
        foreach ($notificaciones as $notificacion) {
            $em->remove($notificacion);
        }
        $em->flush();
        return true;
    }

    /**
     * @Route("/monitor/itinerario/{id}/edit", name="itinerario_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function editAction(Request $request, Itinerario $itinerario)
    {

        if (!$itinerario) {
            throw $this->createNotFoundException('Código de Itinerario no encontrado.');
        }
        $itinerarioOld = clone $itinerario;
        $usuario = $this->userloginManager->getUser();
        $options = [
            'organizacion' => $itinerario->getOrganizacion(),
            'empresas' => $this->empresaManager->findByUsuario($usuario),
            'userlogin' => $usuario,
            'em' => $this->getEntityManager(),
        ];
        $form = $this->createForm(ItinerarioType::class, $itinerario, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $fi = new \DateTime($this->utilsManager->datetime2sqltimestamp($itinerario->getFechaInicio(), false), new \DateTimeZone($itinerario->getOrganizacion()->getTimezone()));
            $ff = new \DateTime($this->utilsManager->datetime2sqltimestamp($itinerario->getFechaFin(), false), new \DateTimeZone($itinerario->getOrganizacion()->getTimezone()));
            $itinerario->setFechaInicio($fi);
            $itinerario->setFechaFin($ff);
            $this->breadcrumbManager->pop();

            if ($this->itinerarioManager->save($itinerario)) {
                $bitacora = $this->bitacoraItinerarioManager->updateItinerario($this->userloginManager->getUser(), $itinerario, $itinerarioOld);
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Itinerario");
        return $this->render('monitor/Itinerario/edit.html.twig', array(
            'itinerario' => $itinerario,
            'userlogin' => $this->userloginManager->getUser(),
            'form' => $form->createView(),
            'empresa' => $itinerario->getEmpresa(),
            'logistica' => $itinerario->getLogistica()
        ));
    }

    /**
     * @Route("/monitor/itinerario/addservicio", name="itinerario_addservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function servicioaddAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();
        $servicios = array();
        $custodios = array();
        $servicios_bitacora = array();
        $tmp = array();

        $this->req = $request;
        parse_str($this->req->get('form'), $tmp);

        $idItinerario = intval($this->req->get('idItinerario'));
        $this->itinerario = $this->itinerarioManager->find($idItinerario);

        if (!$this->itinerario) {
            throw $this->createNotFoundException('Itinerario no encontrado.');
        }

        if (isset($tmp['custodio'])) {
            foreach ($tmp['custodio']['custodio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $servicio = $this->servicioManager->find($id);
                $save = $this->itinerarioManager->addServicio($this->itinerario, $servicio);
                $custodios[] = $servicio;
            }

            $this->itinerario = $this->itinerarioManager->find($idItinerario);
            $this->itiServicios = $this->itinerario->getServicios();
        } else {
            $this->itiServicios = $this->setServicios();
            $bitacora = $this->bitacoraItinerarioManager->updateServicio($this->userloginManager->getUser(), $this->itinerario, $servicios_bitacora);
        }

        foreach ($this->itinerario->getServicios() as $serv) {
            $arrIds[] = $serv->getServicio()->getId();
            $servicios[] =  $serv->getServicio();
        }
        $html = $this->renderView('monitor/Itinerario/servicios.html.twig', array(
            'servicios' => $this->itinerario->getServicios(),
            'arrIds' => json_encode($arrIds, true),
        ));

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora($this->itinerario),
        ));


        //aca agrego el servicio al evento.
        foreach ($this->itinerario->getEventoItinerario() as $evIt) {
            //die('////<pre>' . nl2br(var_export($evIt->getID(), true)) . '</pre>////');

            $this->evento = $evIt->getEvento();
            $ev = $this->addServicios();
        }
        $notificar = $this->notificarEventos();

        return new Response(json_encode(array('html' => $html, 'htmlBitacora' => $htmlBitacora)), 200);
    }

    private function setServicios()
    {
        $em = $this->getEntityManager();
        $servicios_json = json_decode($this->req->get('formServicio'), true);
        $servicios = array();
        foreach ($servicios_json as $value) {
            $id = intval($value['id']);
            $servicio = $this->servicioManager->find($id);
            $chofer = $value['chofer'] !== '' ? $this->choferManager->find(intval($value['chofer'])) : null;

            $itiServ = new ItinerarioServicio();
            $itiServ->setItinerario($this->itinerario);
            $itiServ->setServicio($servicio);
            $itiServ->setChofer($chofer);
            $itiServ->setBill($value['bill']);
            $itiServ->setPatenteAcoplado($value['patente_acoplado']);
            $itiServ->setNroContenedor($value['nro_contenedor']);
            $itiServ->setEstado(0);

            $em->persist($itiServ);
            $servicios_bitacora[] = $servicio;
            $servicios[] = $itiServ;
        }
        $em->flush();
        return $servicios;
    }

    private function notificarEventos()
    {
        $eventosIt = $this->itinerario->getEventoItinerario();
        foreach ($eventosIt as $evIt) {
            if ($evIt->getEvento() &&  $evIt->getEvento()->getActivo()) {
                $notificar = $this->notificadorAgenteManager->notificar($evIt->getEvento(), 1);
            }
        }
        return true;
    }

    /**
     * @Route("/monitor/itinerario/addcontacto", name="itinerario_addcontacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function addContactoAction(Request $request)
    {
        $idIt = $request->get('idIt');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $this->req = $request;
        $contactos = array();
        $tmp = array();
        parse_str($request->get('formContacto'), $tmp);
        if (isset($tmp['contactos'])) {
            foreach ($tmp['contactos']['contacto'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $contacto = $this->contactoManager->find($id);

                $x = $this->itinerarioManager->addContacto($this->itinerario, $contacto);
                $this->contactos[] = $contacto;
            }
            foreach ($this->itinerario->getEventoItinerario() as $evIt) {
                $notif = $this->createNotificacion($evIt->getEvento(), $contactos);
            }

            $bitacora = $this->bitacoraItinerarioManager->updateContactos($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getContactos());
            $notificar = $this->notificarEventos();
        }
        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora(),
        ));

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Contacto/contactos.html.twig', array(
                'contactos' => $this->itinerario->getContactos(),
                'itinerario' => $this->itinerario

            )),
            'htmlBitacora' => $htmlBitacora
        )));
    }

    /**
     * @Route("/monitor/itinerario/delservicio", name="itinerario_delservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function delservicioAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();
        $idServ = $request->get('idServ');

        $idIt = $request->get('id');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $servicio = $this->servicioManager->find($idServ);
        $servicios_bitacora = array();
        $servicios = array();
        $custodios = array();

        $isCustodio = $servicio->getIsCustodio();
        if ($this->itinerarioManager->delServicio($this->itinerario, $servicio)) {
            if ($isCustodio) { // lo necesito para saber si grabo update de custodio o de servicio
                foreach ($this->itinerario->getServicios() as $serv) {
                    if ($serv->getServicio()->getIsCustodio()) {
                        $custodios[] = $serv->getServicio();
                    }
                }
                $bitacora = $this->bitacoraItinerarioManager->updateCustodios($this->userloginManager->getUser(), $this->itinerario, $custodios);
            } else {
                foreach ($this->itinerario->getServicios() as $serv) {
                    if (!$serv->getServicio()->getIsCustodio()) {
                        $servicios_bitacora[] = $serv->getServicio();
                    }
                }
                $bitacora = $this->bitacoraItinerarioManager->updateServicio($this->userloginManager->getUser(), $this->itinerario, $servicios_bitacora);
            }

            foreach ($this->itinerario->getServicios() as $serv) {
                $arrIds[] = $serv->getServicio()->getId();
                $servicios[] = $serv->getServicio();
            }
        }
        //   die('////<pre>' . nl2br(var_export($servicio->getId(), true)) . '</pre>////');

        foreach ($this->itinerario->getEventoItinerario() as $evIt) {
            $evento = $evIt->getEvento();
            if ($evento) {
                $evento->removeServicio($servicio);
                $em->persist($evento);
                $em->flush();
            }
        }

        $notificar = $this->notificarEventos();

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora($this->itinerario),
        ));

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/servicios.html.twig', array(
                'servicios' => $this->itinerario->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            )),
            'htmlBitacora' => $htmlBitacora
        )));
    }

    /**
     * @Route("/monitor/itinerario/editservicio", name="itinerario_editservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function editServicioAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();
        $servicios = array();
        $idServ = $request->get('idServ');


        $idIt = $request->get('id');
        $itinerario = $this->itinerarioManager->find($idIt);
        $servicio = $this->servicioManager->find($idServ);
        $iti_serv = $this->itinerarioManager->findItiServ($itinerario, $servicio);

        $tmp = array();
        parse_str($request->get('formServicio'), $tmp);
        $data = $tmp['formServicioEdit'];
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        if (isset($data['chofer_0'])) {
            $chofer = $this->choferManager->findById(intval($data['chofer_0']));
            $iti_serv->setChofer($chofer);
        }
        $iti_serv->setPatenteAcoplado($data['patente_acoplado']);
        $iti_serv->setBill($data['bill']);
        $iti_serv->setNroContenedor($data['nro_contenedor']);
        $em->persist($iti_serv);

        $em->flush();

        foreach ($itinerario->getServicios() as $serv) {
            $arrIds[] = $serv->getServicio()->getId();
            $servicios[] = $serv->getServicio();
        }



        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $servicios,
                'itinerario' => $itinerario,
                'arrIds' => json_encode($arrIds, true),
            )),


        )));
    }

    /**
     * @Route("/monitor/itinerario/addreferencia", name="itinerario_addreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function addReferenciaAction(Request $request)
    {
        $idIt = $request->get('id');
        $this->req = $request;
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $tmp = array();
        $pois = array();
        parse_str($request->get('formReferencia'), $tmp);
        $dataRef = $tmp['referencia']['referencia'];
        $tot = count($this->itinerario->getPois());
        if ($this->itinerario->getGrupoReferencia()) {
            $grupo = $this->grupoReferenciaManager->setGrupo($this->itinerario->getGrupoReferencia()); //buscamos el grupo del itinerario
        } else {
            $grupo = $this->grupoReferenciaManager->create($this->itinerario->getOrganizacion()); //le creo un grupo temporal hasta que se cierre o elimine
            $grupo->setNombre("ITINERARIO-" . $this->itinerario->getCodigo()); // pongo nombre con codigo tmp para poder buscarlo y eliminarlo despues
            $grupo = $this->grupoReferenciaManager->save($grupo);
            $this->itinerario->setGrupoReferencia($grupo);
        }
        //die('////<pre>' . nl2br(var_export($grupo->getId(), true)) . '</pre>////');
        foreach ($dataRef as $idR) {
            $tot++;
            $referencia = $this->referenciaManager->find($idR);
            if (!$grupo->contieneReferencia($referencia)) {
                $grupo = $this->grupoReferenciaManager->addReferencia($referencia);
                // $notificar = $this->notificadorAgenteManager->notificar($grupo, 1);
                $pois[] = $this->itinerarioManager->addPoi($this->itinerario, $referencia, $tot);
                $bitacora = $this->bitacoraItinerarioManager->updateReferencia($this->userloginManager->getUser(), $this->itinerario, $pois);
            }
        }

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora(),
        ));
        $notificar = $this->notificarEventos();

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $this->itinerarioManager->checkOrderReferencia($this->itinerario),
            )),
            'htmlBitacora' => $htmlBitacora
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/monitor/itinerario/historico", name="itinerario_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function historicoAction(Request $request)
    {
        $tmp = array();
        $itinerarios = array();
        $consulta = array();
        $fecha_desde = null;
        $fecha_hasta = null;
        parse_str($request->get('formHistIt'), $tmp);
        $form = $tmp['historico'];
        $user = $this->userloginManager->getUser();
        if ($form) {
            if ($form['fecha_desde'] != '' && $form['fecha_hasta'] != '') {
                $ok = $this->utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
                if ($ok) {    //el formato esta correcto
                    $fecha_desde = $form['fecha_desde'];
                    $fecha_hasta = $form['fecha_hasta'];
                    $consulta['fecha_desde'] = $fecha_desde;
                    $consulta['fecha_hasta'] = $fecha_hasta;
                }
            }
            if ($form['servicio'] == '0') {  //es para toda la flota.
                $servicio = null;
                $strServ = 'Todos Servicios';
            } else {
                $servicio = $this->servicioManager->find(intval($form['servicio']));
                $strServ = $servicio->getNombre();
            }
            $consulta['servicio'] = $strServ;

            if (($form['tipoServicio']) > 0) {
                $tipoServicio = $this->tipoServicioManager->findById(intval($form['tipoServicio']));
                $strTipo = $tipoServicio->getNombre();
            } else {
                $tipoServicio = null;
                $strTipo = 'Todos Tipos Vehículos';
            }
            $consulta['tipoServicio'] = $strTipo;

            if (($form['transporte']) > 0) {
                $transporte = $this->transporteManager->find($form['transporte']);
                $strTransporte = $transporte->getNombre();
            } else {
                $transporte = null;
                $strTransporte = 'Todos Transportes';
            }
            $consulta['transporte'] = $strTransporte;

            if (($form['satelital']) > 0) {
                $satelital = $this->satelitalManager->find($form['satelital']);
                $strSatelital = $satelital->getNombre();
            } else {
                $satelital = null;
                $strSatelital = 'Todos Satelitales';
            }
            $consulta['satelital'] = $strSatelital;

            if (intval($form['empresa']) > 0) {
                $empresa = $this->empresaManager->find(intval($form['empresa']));
                $strEmpresa = $empresa->getNombre();
            } else {
                $empresa = null;
                $strEmpresa = 'Todas Empresas';
            }
            $consulta['satelital'] = $strSatelital;

            $fecha_desde = $fecha_desde != null ? $this->utilsManager->datetime2sqltimestamp($fecha_desde, true) : null;
            $fecha_hasta = $fecha_hasta != null ? $this->utilsManager->datetime2sqltimestamp($fecha_hasta, false) : null;
            //  die('////<pre>' . nl2br(var_export($user->getId(), true)) . '</pre>////');
            $itinerarios = $this->itinerarioManager->historico($user->getOrganizacion(), intval($form['estado']), $user, $servicio, $tipoServicio, $transporte, $satelital, $empresa, $fecha_desde, $fecha_hasta);  //parche
        }

        $consulta['servicio'] = 'Todos';
        $consulta['tipoServicio'] = 'Todos';
        $consulta['transporte'] = 'Todos';
        $consulta['satelital'] = 'Todos';
        $consulta['empresa'] = 'Todos';
        //  die('////<pre>' . nl2br(var_export($desde, true)) . '</pre>////');

        if (!is_null($fecha_desde) && !is_null($fecha_hasta)) {
            $filtro = sprintf('<b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> ', $consulta['fecha_desde'], $consulta['fecha_hasta'], $strServ, $strTipo, $strTransporte, $strSatelital, $strEmpresa);
        } else {
            //decode filtro
            $filtro = sprintf('<b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> ', $strServ, $strTipo, $strTransporte, $strSatelital, $strEmpresa);
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/itinerarios.html.twig', array(
                'itinerarios' => $itinerarios,
            )),
            'itinerarios' => json_encode($itinerarios),
            'filtro' => $filtro,
            //                    'consulta' => json_encode($consulta)
        )));
        //        }
    }

    /**
     * @Route("/monitor/itinerario/delreferencia", name="itinerario_delreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function delReferenciaAction(Request $request)
    {
        $poi = $request->get('idRef');

        $idIt = $request->get('id');
        $this->req = $request;
        $this->itinerario = $this->itinerarioManager->find($idIt);

        $referencia = $this->itinerarioManager->delReferencia($this->itinerario, $poi); //elimino el pio y me devuelve la ref para eliminarla del grupo
        $bitacora = $this->bitacoraItinerarioManager->updateReferencia($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getPois());
        $grupo = $this->grupoReferenciaManager->setGrupo($this->itinerario->getGrupoReferencia());
        $gr = $this->grupoReferenciaManager->removeReferencia($referencia);
        $notificar = $this->notificadorAgenteManager->notificar($grupo, 1);

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora(),
        ));
        $notificar = $this->notificarEventos();
        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $this->itinerarioManager->checkOrderReferencia($this->itinerario),
            )),
            'htmlBitacora' => $htmlBitacora
        )));
    }

    /**
     * @Route("/monitor/itinerario/upreferencia", name="itinerario_upreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function upReferenciaAction(Request $request)
    {
        $idPoi = $request->get('idRef');   //es el id del itinerarioReferencia
        //busco el itinerarios
        $idIt = $request->get('id');
        $itinerario = $this->itinerarioManager->find($idIt);

        $this->itinerarioManager->upReferencia($itinerario, $idPoi);

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $this->itinerarioManager->checkOrderReferencia($itinerario),
            )),
        )));
    }

    /**
     * @Route("/monitor/itinerario/downreferencia", name="itinerario_downreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function downReferenciaAction(Request $request)
    {
        $poi = $request->get('idRef');   //es el id del itinerarioReferencia
        //busco el itinerarios
        $idIt = $request->get('id');
        $itinerario = $this->itinerarioManager->find($idIt);

        $this->itinerarioManager->downReferencia($itinerario, $poi);

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $this->itinerarioManager->checkOrderReferencia($itinerario),
            )),
        )));
    }

    /**
     * @Route("/monitor/itinerario/changestoptype", name="itinerario_changestoptype")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function changestoptypeAction(Request $request)
    {
        $p = $request->get('idRef');   //es el id del itinerarioReferencia
        //busco el itinerarios
        $idIt = $request->get('id');
        $itinerario = $this->itinerarioManager->find($idIt);
        $stopType =  $request->get('stopType') != 0 ? $request->get('stopType') : null;
        $poi = $this->itinerarioManager->findPoi(intval($p));
        $poi->setStopType($stopType);
        $poi = $this->itinerarioManager->savePoi($poi);


        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $this->itinerarioManager->checkOrderReferencia($itinerario),
            )),
        )));
    }

    /**
     * @Route("/monitor/itinerario/getclock", name="itinerario_getclock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function getClockAction(Request $request)
    {
        $poi = $request->get('idItRef');
        $poi = $this->itinerarioManager->findPoi(intval($poi));
        return new Response(json_encode(array(
            'ingreso' => $poi !== null && $poi->getFechaIngreso() ? $poi->getFechaIngreso()->format('d/m/Y H:i') : null,
            'egreso' => $poi !== null &&  $poi->getFechaEgreso() ? $poi->getFechaEgreso()->format('d/m/Y H:i') : null,
        )));
    }

    /**
     * @Route("/monitor/itinerario/setclock", name="itinerario_setclock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function setClockAction(Request $request)
    {
        $poi = $request->get('idItRef');
        $poi = $this->itinerarioManager->findPoi($poi);
        $itinerario = $poi->getItinerario();

        $tmp = array();
        parse_str($request->get('formHorario'), $tmp);
        $data = $tmp['horario'];
        $hArribo = new \DateTime($this->utilsManager->datetime2sqltimestamp($data['arribo'], false));
        $hPartida = new \DateTime($this->utilsManager->datetime2sqltimestamp($data['partida'], false));
        $this->itinerarioManager->setClock($poi, $hArribo, $hPartida);
        $arr = $this->itinerarioManager->checkOrderReferencia($itinerario);
        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/referencias.html.twig', array(
                'pois' => $arr,
            )),
        )));
    }

    /**
     * @Route("/monitor/itinerario/addusuario", name="itinerario_addusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function addUsuarioAction(Request $request)
    {
        $idIt = $request->get('id');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $this->req = $request;

        $tmp = array();
        parse_str($request->get('formUsuario'), $tmp);
        if (isset($tmp['usuario'])) {
            foreach ($tmp['usuario']['usuario'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $usuario = $this->usuarioManager->find($id);
                $x = $this->itinerarioManager->addUsuario($this->itinerario, $usuario);
            }
            $bitacora = $this->bitacoraItinerarioManager->updateUsuarios($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getUsuarios());
        }

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora(),
        ));
        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/usuarios.html.twig', array(
                'usuarios' => $this->itinerario->getUsuarios(),
                'itinerario' => $this->itinerario

            )),
            'htmlBitacora' => $htmlBitacora
        )));
    }

    /**
     * @Route("/monitor/itinerario/delusuario", name="itinerario_delusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function delUsuarioAction(Request $request)
    {
        $idUser = $request->get('idUser');   //es el id del usuario
        $usuario = $this->usuarioManager->find($idUser);

        $idIt = $request->get('id');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $this->req = $request;

        $this->itinerarioManager->delUsuario($itinerario, $usuario);

        return new Response(json_encode(array(
            'html' => $this->renderView('monitor/Itinerario/usuarios.html.twig', array(
                'usuarios' => $itinerario->getUsuarios(),
                'itinerario' => $this->itinerario

            )),
        )));
    }

    /**
     * @Route("/monitor/itinerario/editestado", name="itinerario_editestado")
     * @IsGranted("ROLE_ITINERARIO_ESTADO")
     */
    public function editestadoAction(Request $request)
    {
        $estado = (int)$request->get('estado');
        $vista = $request->get('vista');
        $estadoOld = '';
        $organizacion = $this->userloginManager->getOrganizacion();
        //$fecha = $request->get('fecha') && $request->get('fecha') !== '' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('fecha'), false), new \DateTimeZone($organizacion->getTimezone())) : null;
        $fecha = $request->get('fecha') && $request->get('fecha') !== '' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('fecha'), false)) : null;
        $nota = $request->get('nota');
        $tmp = array();
        $html = '';

        if (strpos($request->get('formCheck'), 'formCheck') !== false) { //viene por list de itis, trae el form de los checks
            parse_str($request->get('formCheck'), $tmp);
        } else {
            $tmp['formCheck'] = array(intval($request->get('formCheck')) => 'on'); //viene por mapa, trae el id del iti
        }

        // die('////<pre>' . nl2br(var_export($tmp['formCheck'], true)) . '</pre>////');
        foreach ($tmp['formCheck'] as $key => $value) {
            $itinerario = $this->itinerarioManager->find($key);
            $estadoOld = $itinerario->getStrEstado();
            //inicio data y datos anteriores para la bitacora
            $data = array(
                'codigo' => $itinerario->getCodigo(),
                'estado_old' => $estadoOld,
                'estado' => $itinerario->getStrEstado($estado),
                'nota' => $nota,
            );
            if ($estado == $itinerario::ESTADO_CERRADO || $estado == $itinerario::ESTADO_ELIMINADO) {
                $data['fecha_inicio'] = $itinerario->getFechaInicio();
                $data['fecha_fin'] = $fecha;
                $it = $this->itinerarioManager->cerrar($itinerario, null, $fecha, $nota);
            } else {
                if ($estado == $itinerario::ESTADO_ENCURSO || $estado == $itinerario::ESTADO_AUDITORIA || 
                    $estado == $itinerario::ESTADO_DEMORADO || $estado == $itinerario::ESTADO_PERNOCTADO ||
                    $estado == $itinerario::ESTADO_IMPLANTADO || $estado ==  $itinerario::ESTADO_NUEVO ) {
                    if ($fecha && $estado == $itinerario::ESTADO_ENCURSO) {
                        $itinerario->setFechaInicio($fecha);
                        $data['fecha_inicio'] = $fecha;
                    }
                    $evento  = $this->endabeldDisabledEv($itinerario, $estado);
                } else {
                    // los otros estados pendiente,cancelado, desconoce,etc
                }
                $itinerario->setEstado($estado);
                $it = $this->itinerarioManager->save($itinerario);
            }

            //aca debo actualizar el estado para CHR
            if (!is_null($itinerario->getLogistica()) && $itinerario->getLogistica()->getId() == self::LOGISTICA_CHROBINSON) {
                $this->notificarChr($itinerario);
            }

            $bitacora = $this->bitacoraItinerarioManager->updateEstado($this->userloginManager->getUser(), $itinerario, $data);

            $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
                'bitacora' => $this->getBitacora($itinerario),
            ));

            if ($vista == 'list') { // si la vista es list armamos el array. si es show no hace falta porque el ajax va a refrescar la pag.
                $itinerarios = $this->itinerarioManager->findOpens($this->userloginManager->getUser());
                $html = $this->renderView('monitor/Itinerario/itinerarios.html.twig', array(
                    'itinerarios' => $itinerarios
                ));
            } elseif ($vista == 'mapa') {
                $html = "<label id='labelEstado_'" . $itinerario->getId() . " style='font-size: 9px; margin-left: 10px;' class='label label-" . $itinerario->getStrBadge() . "'>" . $itinerario->getStrEstado() . "</label>";
                //  die('////<pre>' . nl2br(var_export($html, true)) . '</pre>////');
            }
        }

        return new Response(json_encode(array('html' => $html, 'idItinerario' => $vista == 'mapa' ? $itinerario->getId() : null)));
    }

    private function endabeldDisabledEv($itinerario, $estado)
    {
        foreach ($itinerario->getEventoItinerario() as $evIt) {
          //  die('////<pre>' . nl2br(var_export($estado . ' ' . Itinerario::ESTADO_NUEVO, true)) . '</pre>////');
            $evento = $evIt->getEvento();
            if ($evento) {
                $evento->setActivo(true);
                if ($estado == Itinerario::ESTADO_NUEVO) {
                    $evento->setActivo(false); //si pasa a nuevo desactivo todo
                } elseif (
                    $estado == Itinerario::ESTADO_IMPLANTADO && $evento->getTipoEvento()->getCodename() == 'EV_MOTIVOTX' ||
                    $estado == Itinerario::ESTADO_IMPLANTADO && $evento->getTipoEvento()->getCodename() == 'EV_DETENIDO'
                ) {
                    $evento->setActivo(false);
                }
                $evento = $this->eventoManager->save($evento);
                $notificar = $this->notificadorAgenteManager->notificar($evento, 1); //actualizamos el vento
            }
        }
        return $evento;
    }

    /** hace toda la logica para el envio del cambio de estado a CHR */
    private $estadosCerrado = [Itinerario::ESTADO_CERRADO, Itinerario::ESTADO_ELIMINADO];
    private $estadosListoViaje = [Itinerario::ESTADO_ENCURSO];
    const ITINERARIO_ADMITIDO = 1;
    const ITINERARIO_COMPLETADO = 2;

    private function notificarChr($itinerario)
    {
        if (in_array($itinerario->getEstado(), $this->estadosCerrado)) {
            //enviar D1 = Completed Unloading at Delivery Location
            $this->log('Completado viaje id:' . $itinerario->getId());
            $token =  $this->notificadorAgenteManager->getTokenChr();
            if ($token) { //si no tiene token es en vano enviar
                $this->log('Token -> ' . $token);

                foreach ($itinerario->getServicios() as $ItServicio) {  //tengo q hacerlo por cada servicio del itinerairio. CHR usa un solo servicio por itinerario siempre
                    $jsonCHR = json_encode($this->toArrayDataChr($itinerario, self::ITINERARIO_COMPLETADO, $ItServicio->getServicio()));
                    $this->log('json -> ' . $jsonCHR);
                    $notificarChr = $this->notificadorAgenteManager->sendDataChr($jsonCHR, $token);
                    $this->log('Result -> ' . $notificarChr);
                }
            }
        } elseif (in_array($itinerario->getEstado(), $this->estadosListoViaje)) {
            //enviar XB = Shipment Acknowledged
            $this->log('Listo viaje id:' . $itinerario->getId());
            $token =  $this->notificadorAgenteManager->getTokenChr();
            if ($token) { //si no tiene token es en vano enviar
                $this->log('Token -> ' . $token);

                foreach ($itinerario->getServicios() as $ItServicio) {  //tengo q hacerlo por cada servicio del itinerairio. CHR usa un solo servicio por itinerario siempre
                    $jsonCHR = json_encode($this->toArrayDataChr($itinerario, self::ITINERARIO_ADMITIDO, $ItServicio->getServicio()));
                    $this->log('json -> ' . $jsonCHR);
                    $notificarChr = $this->notificadorAgenteManager->sendDataChr($jsonCHR, $token);
                    $this->log('Result -> ' . $notificarChr);
                }
            }
        } else {
            /** otros estado que no hay que notificar */
        }
        return true;
    }

    /**
     * $itinerario = es el itinerario en cuestion
     * $estado = 1: admitido o 2: completado
     * $servicio = es el servicio del itinerario, siempre es uno pero tengo que pasarlo por la relacion de la BD
     */
    private function toArrayDataChr($itinerario, $estado, $servicio)
    {
        $transitType = "Road";
        $location = null;
        $inPoi = null;
        $shipmentIdentifier = array(
            "shipmentNumber" => $itinerario->getCodigo(), //codigo de itinerario
        );
        $fecha = new \DateTime();   //fecha actual en UTC
        $dateTime = array(
            "eventDateTime" => $fecha ? $fecha->format('Y-m-d') . 'T' . $fecha->format('H:i:s.v') . 'Z' : null
        );

        $direccion = $this->getDireccion($servicio);   //debo buscar siempre la direccion

        if ($estado == self::ITINERARIO_ADMITIDO) {
            /** hay que armar para notificar que esta listo para iniciar el viaje */
            $stopType = 'P';  //es picked
            $eventoCode = 'XB';   //XB = Shipment Acknowledged
            if ($itinerario->getPois()) {
                foreach ($itinerario->getPois() as $poi) {
                    $referencias[] = $poi->getReferencia();
                }

                //busco si esta dentro de un poi
                $cercaResult = $this->servicioManager->buscarCercania($servicio, $referencias);
                if (count($cercaResult) > 0) {
                    foreach ($cercaResult as $cerca) {
                        if ($cerca['adentro']) {
                            $inPoi = $cerca;
                            break;   //ya encontre donde estaba asi que salto el boocle
                        }
                    }
                }
            }
            if ($inPoi) {   //esta en una poi debo mandar la data del poi               
                $referencia = $this->referenciaManager->find($inPoi['id']);
                $poi = $this->itinerarioManager->findPoiByItiRef($itinerario, $referencia); //obtengo el poi
                $location = array(
                    "type" => trim($poi->getStopType()),
                    "stopSequenceNumber" => $poi->getOrden(),
                    "name" => $referencia->getNombre(), //nombre de la referencia
                    "locationId" => $referencia->getCodigoExterno(), //WarehoseCode
                    "address" =>  array(
                        "city" => $referencia->getCiudad() ? $referencia->getCiudad() : "", // 
                        "stateProvinceCode" => $referencia->getProvincia() ?  $referencia->getProvincia()->getCode() : "", //BS AS
                        "country" => "AR"
                    )
                );
            } else {  //esta en cualquier lado debo enviar datos de la ruta.
                $location = array(
                    //"type" => null,    no enviar esto directamente aunque lo pida la doc
                    //"stopSequenceNumber" => null, no enviar esto directamente aunque lo pida la doc
                    "address" =>  array(
                        "city" => $direccion['ciudad'],
                        "stateProvinceCode" => $direccion['pcia'],
                        "country" => "AR",
                        "latitude" => $servicio->getUltLatitud(),
                        "longitude" => $servicio->getUltLongitud(),
                    )
                );
            }
        } elseif ($estado == self::ITINERARIO_COMPLETADO) {
            $stopType = 'D';  //es droped
            $eventoCode = 'D1';     //D1 = Completed Unloading at Delivery Location

            //debo buscar el ultimo poi para cerrarlo alli.            
            $lastPoi = $this->itinerarioManager->findLastPoi($itinerario);
            if ($lastPoi) {   //armar array con el poi final                
                $location = array(
                    "type" => trim($lastPoi->getStopType()),
                    "stopSequenceNumber" => $lastPoi->getOrden(),
                    "name" => $lastPoi->getReferencia()->getNombre(), //nombre de la referencia
                    "locationId" => $lastPoi->getReferencia()->getCodigoExterno(), //WarehoseCode
                    "address" =>  array(
                        "city" => $direccion['ciudad'],
                        "stateProvinceCode" => $direccion['pcia'],
                        "country" => "AR",
                        "latitude" => $servicio->getUltLatitud(),
                        "longitude" => $servicio->getUltLongitud(),
                    )
                );
            } else {
                $location = array(
                    //"type" => null,    no enviar esto directamente aunque lo pida la doc
                    //"stopSequenceNumber" => null, no enviar esto directamente aunque lo pida la doc
                    "address" =>  array(
                        "city" => $direccion['ciudad'],
                        "stateProvinceCode" => $direccion['pcia'],
                        "country" => "AR",
                        "latitude" => $servicio->getUltLatitud(),
                        "longitude" => $servicio->getUltLongitud(),
                    )
                );
            }
        } else {
            /** ignorar cualquier otra cosa que nunca vendra */
        }
        return array(
            'eventCode' => $eventoCode,
            'shipmentIdentifier' => $shipmentIdentifier,
            'dateTime' => $dateTime,
            'location' => $location
        );
    }

    private function getDireccion($servicio)
    {
        $data = ['ciudad' => '---', 'pcia' => '---'];

        $direc = $this->geocoderManager->inversoRaw($servicio->getUltLatitud(), $servicio->getUltLongitud());
        if (isset($direc['exito']) && $direc['exito']) {

            if ($direc['provided_by'] == 'vmap3') {
                $data['ciudad'] = $direc['direccion'];
            } else {
                if (isset($direc['address'])) {
                    if (!isset($direc['address']['city'])) {
                        $data['ciudad'] = isset($direc['address']['village']) ? $direc['address']['village'] : '---';
                    } else {
                        $data['ciudad'] = $direc['address']['city'];
                    }
                } else {
                    $data['ciudad'] =  '---';
                }

                if (isset($direc['address']['state'])) {
                    $p = $this->itinerarioManager->findProvinciaByNombre($direc['address']['state']);
                    if ($p) {
                        $data['pcia'] = $p->getCode();
                    } else {
                        $data['pcia'] = '---';
                    }
                }
            }
        }
        return $data;
    }

    private function log($str)
    {
        if ($this->logear) {
            $this->logger->notice('ITI: ' . $str);
        }
    }
    /**
     * @Route("/monitor/itinerario/servicio/editestado", name="itinerario_servicio_editestado")
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function editestadoservicioAction(Request $request)
    {
        $estado = (int)$request->get('estado');
        $idItinerario = (int) $request->get('idItinerario');
        $idServicio = (int) $request->get('idServicio');
        $this->itinerario = $this->itinerarioManager->find($idItinerario);
        $servicio = $this->servicioManager->find($idServicio);
        $itiserv = $this->itinerarioManager->findItiServ($this->itinerario, $servicio);

        //inicio data y datos anteriores
        $data = array(
            'codigo' => $this->itinerario->getCodigo(),
            'servicio' => $servicio->getNombre(),
            'estado_old' => $itiserv->getStrEstado(),
        );
        $itiserv->setEstado($estado);
        $itiserv = $this->itinerarioManager->saveItiServ($itiserv);
        $data['estado'] = $itiserv->getStrEstado();
        $bitacora = $this->bitacoraItinerarioManager->updateEstadoServicio($this->userloginManager->getUser(), $this->itinerario, $data);

        $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
            'bitacora' => $this->getBitacora($this->itinerario),
        ));

        //die('////<pre>' . nl2br(var_export($itinerario->getId(), true)) . '</pre>////');
        return new Response(json_encode(array('respuesta' => true)));
    }

    /**
     * @Route("/monitor/itinerario/deleteusuario", name="itinerario_deleteusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function deleteusuarioAction(Request $request)
    {
        $id = intval($request->get('id'));
        $idIt = intval($request->get('idIt'));
        $usuario = $this->usuarioManager->find($id);
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $usuarios = $this->itinerario->getUsuarios();
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        if (!$usuario) {
            throw $this->createNotFoundException('Usuario no encontrado.');
        }
        if ($this->itinerarioManager->delUsuario($this->itinerario, $usuario)) {
            $b = $this->bitacoraItinerarioManager->updateUsuarios($this->userloginManager->getUser(), $this->itinerario, $usuarios);

            $html = $this->renderView('monitor/Itinerario/usuarios.html.twig', array(
                'usuarios' => $this->itinerario->getUsuarios(),
                'itinerario' => $this->itinerario,

            ));

            $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
                'bitacora' => $this->getBitacora($this->itinerario),
            ));

            return new Response(json_encode(array('html' => $html,  'htmlBitacora'  => $htmlBitacora)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/itinerario/deletecontacto", name="itinerario_deletecontacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function deletecontactoAction(Request $request)
    {
        $id = intval($request->get('id'));
        $idIt = intval($request->get('idIt'));
        $contacto = $this->contactoManager->find($id);
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $this->req = $request;
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        if (!$contacto) {
            throw $this->createNotFoundException('Contacto no encontrado.');
        }

        if ($this->itinerarioManager->delContacto($this->itinerario, $contacto)) {
            foreach ($this->itinerario->getEventoItinerario() as $evIt) {
                $notif = $this->deleteNotificacion($evIt->getEvento(), $contacto); //eliminamos las notificaciones 
            }


            $b = $this->bitacoraItinerarioManager->updateContactos($this->userloginManager->getUser(), $this->itinerario, $this->itinerario->getContactos());
            $notificar = $this->notificarEventos();
            $html = $this->renderView('monitor/Itinerario/contactos.html.twig', array(
                'contactos' => $this->itinerario->getContactos(),

            ));

            $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
                'bitacora' => $this->getBitacora(),
            ));

            return new Response(json_encode(array('html' => $html, 'htmlBitacora' => $htmlBitacora)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/itinerario/asignareventos", name="itinerario_asignar_eventos")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR") 
     */
    public function asignareventosAction(Request $request)
    {
        $this->req = $request;
        if ($this->req->getMethod() == 'POST') {
            $servicio = $this->servicioManager->findById($request->get('servicio'));
            $this->evento = $this->eventoManager->findById($request->get('evento'));
            $this->itinerario = $this->itinerarioManager->find($request->get('itinerario'));
            if ($this->req->get('accion') == 1) {
                //es para vincular
                $servicio = $this->eventoManager->saveServicioEv($servicio, $this->evento);
                $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $this->itinerario, $this->evento, 'Activo');
                $notificar = $this->notificadorAgenteManager->notificar($this->evento, 1);
                $html = $this->renderView('monitor/Itinerario/btn_evento.html.twig', array(
                    'evento_id' => $this->evento->getId(),
                    'servicio_id' => $servicio->getId(),
                    'accion' => 2,
                ));
            } else {
                //devincular
                $servicio = $this->eventoManager->removeServicioEv($servicio, $this->evento);
                $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $this->itinerario, $this->evento, 'Inactivo');
                $notificar = $this->notificadorAgenteManager->notificar($this->evento, 2);
                $html = $this->renderView('monitor/Itinerario/btn_evento.html.twig', array(
                    'evento_id' => $this->evento->getId(),
                    'servicio_id' => $servicio->getId(),
                    'accion' => 1,
                ));
            }

            return new Response(json_encode(array('html' => $html)), 200);
        }
    }


    /**
     * @Route("/itinerario/addcomentario", name="itinerario_addcomentario")
     * @Method({"POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function addcomentarioAction(Request $request)
    {
        $mensaje = $request->get('comentario');
        $id = $request->get('id');
        if ($mensaje && $id) {
            $itinerario = $this->itinerarioManager->find($id);
            $comentario = $this->bitacoraItinerarioManager->addComentario($this->userloginManager->getUser(), $itinerario, $mensaje);

            $htmlBitacora = $this->renderView('monitor/Itinerario/bitacora.html.twig', array(
                'bitacora' => $this->getBitacora($itinerario),
            ));

            $strPopup = $this->renderView('monitor/Itinerario/comentarios.html.twig', array(
                'itinerario' => $itinerario,
                'comentarios' => $this->bitacoraItinerarioManager->get($itinerario, BitacoraItinerario::EVENTO_ITINERARIO_COMENTARIO),
            ));
            return new Response(json_encode(array(
                'strPopup' => $strPopup,
                'htmlBitacora' => $htmlBitacora
            )));
        }
        return new Response(json_encode(array('mensaje' => 'error')));
    }


    /**
     * @Route("/monitor/itinerario/addevento", name="itinerario_addevento")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function addeventoAction(Request $request)
    {
        $this->req = $request;
        $idIt = $this->req->get('idItinerario');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $this->contactos =  $this->itinerario->getContactos();
        $this->pois =  $this->itinerario->getPois();
        $this->organizacion = $this->itinerario->getOrganizacion();
        $tmp = array();
        parse_str($this->req->get('formEvento'), $tmp);
        if (isset($tmp['eventos'])) {
            foreach ($tmp['eventos'] as $key => $value) {
                //  die('////<pre>' . nl2br(var_export($key , true)) . '</pre>////');
                if ($key == 'panico' || $key == 'detencion' || $key == 'pois' || $key == 'sensores') {
                    if ($key == 'panico') {
                        $codename = 'EV_PANICO';
                        $arr = array('eventos' => array($key => $tmp['eventos'][$key]));
                    } elseif ($key == 'detencion') {
                        $codename = 'EV_DETENIDO';
                        $tiempo = (int) $tmp['eventos']['detencion_tiempo'];
                        $arr = array('eventos' => array($key => $tmp['eventos'][$key], 'detencion_tiempo' => $tiempo));
                    } elseif ($key == 'pois') {
                        $codename = 'EV_IO_REFERENCIA';
                        $arr = array('eventos' => array($key => $tmp['eventos'][$key]));
                    } else { // sensores
                        $codename = 'EV_MOTIVOTX';
                        $arr = array('eventos' => array($key => $tmp['eventos'][$key]));
                    }
                    $exist = $this->eventoManager->existInIt($this->itinerario, $codename); //verifico que no exista el evento primero
                    if (!$exist) {
                        $this->itiServicios = $this->itinerario->getServicios();
                        $evIt = $this->createEventos($arr);
                    }
                }
            }
            //$status = $this->eventoManager->getStatusServiciosAgentes($evento);            
            return new Response(json_encode(array('html' =>  $this->renderView('monitor/Itinerario/eventos.html.twig', array(
                'itinerario' => $this->itinerario,
                'eventos' =>  $this->getEventos(),
                'eventos_historico' =>  $this->getHistoricoEventos($this->itinerario),
            )))), 200);
        }
        return new Response(json_encode(array('html' => 'error')));
    }

    /**
     * @Route("/monitor/itinerario/editdetencion", name="itinerario_editdetencion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function editdetencionAction(Request $request)
    {
        $idIt = $request->get('idItinerario');
        $itinerario = $this->itinerarioManager->find($idIt);

        $tmp = array();
        parse_str($request->get('formDetencion'), $tmp);
        // die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (isset($tmp['eventos'])) {
            if ((int)$tmp['eventos']['detencion_tiempo'] > 0) {
                $evParam = $this->eventoManager->findParamById((int)$tmp['eventos']['detencion_id']);
                $evParam->setValor((int)$tmp['eventos']['detencion_tiempo']);
                $evParam = $this->eventoManager->saveEvParam($evParam);
                return new Response(json_encode(array('data' => $evParam->getValor(), 'data_str' => "(" . $evParam->getValor() . "min)")));
            }
        }
        return new Response(json_encode(array('data' => 'error')));
    }

    /**
     * @Route("/monitor/itinerario/deleteevento", name="itinerario_deleteevento")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function deleteEvento(Request $request)
    {
        $idIt = $request->get('idItinerario');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $em = $this->getEntityManager();

        $id = $request->get('id');
        $this->evento = $this->eventoManager->findById($id);
        if ($this->evento) {
            if ($this->notificadorAgenteManager->notificar($this->evento, 2)) {
                $this->eventoManager->remove($id);
                return new Response(json_encode(array('data' => 'ok')));
            }
        }
        return new Response(json_encode(array('data' => 'error')));
    }

    /**
     * @Route("/monitor/itinerario/changeestadoevento", name="itinerario_evento_estado")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function changeestadoAction(Request $request)
    {
        $idIt = $request->get('idItinerario');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $em = $this->getEntityManager();

        $id = $request->get('id');
        $this->evento = $this->eventoManager->findById(intval($id));
        if ($this->evento) {
            if ($this->evento->getActivo()) {
                $this->evento->setActivo(false);
            } else {
                $this->evento->setActivo(true);
            }
            //  die('////<pre>' . nl2br(var_export($this->evento->getId(), true)) . '</pre>////');
            //if ($this->notificadorAgenteManager->notificar($this->evento, 1)) {
            $this->eventoManager->save($this->evento);
            return new Response(json_encode(array('data' => $this->evento->getId(), 'activo' => $this->evento->getActivo())));
            //}
        }
        return new Response(json_encode(array('data' => 'error')));
    }

    /**
     * @Route("/monitor/itinerario/resendevento", name="itinerario_resendevento"), methods={"GET", "POST"}
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function resendEvento(Request $request)
    {
        $idIt = $request->get('idItinerario');
        $this->itinerario = $this->itinerarioManager->find($idIt);
        $em = $this->getEntityManager();
        if ($this->itinerario) {
            $eventosIti = $this->eventoManager->findAllByItinerario($this->itinerario);

            foreach ($eventosIti as $evIt) {
                $this->evento = $evIt->getEvento();
                if (is_null($this->evento)) {
                    $em->remove($evIt);
                } else {
                    if ($this->addServicios()) {
                        $this->notificadorAgenteManager->notificar($this->evento, 1);
                    }
                }
            }
            $em->flush();
            return new Response(json_encode(array('data' => 'ok')));
        }
        return new Response(json_encode(array('data' => 'error')));
    }

    /**
     * @Route("/monitor/itinerario/timeline", name="itinerario_timeline"), methods={"GET"}
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function timelineAction(Request $request)
    {
        $idIt = $request->get('idItinerario');
        $itinerario = $this->itinerarioManager->find($idIt);
        $timeline = $this->generarTimeline($itinerario);
        $html = $this->renderView('monitor/Itinerario/timeline.html.twig', ['timeline' => $timeline]);
        return new Response(json_encode(array('html' => $html)));
    }


    /**
     * @Route("/monitor/itinerario/getchoferes", name="itinerario_getchoferes")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_AGREGAR")
     */
    public function getchoferesAction(Request $request)
    {
        $servicio = $this->servicioManager->findById(intval($request->get('id')));
        $choferes = $this->itinerarioManager->getChoferesByEntity(
            $servicio->getTransporte() ? $servicio->getTransporte()->getChoferes() : $servicio->getOrganizacion()->getChoferes()
        );
        return new Response(json_encode(array('id' => $servicio->getId(), 'choferes' => count($choferes) > 0 ? $choferes : 0)));
    }
}
