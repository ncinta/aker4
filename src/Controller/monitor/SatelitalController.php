<?php

namespace App\Controller\monitor;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\monitor\SatelitalType;
use App\Entity\Organizacion;
use App\Entity\Satelital;
use App\Model\app\BreadcrumbManager;
use App\Model\monitor\SatelitalManager;
use App\Model\app\ServicioManager;
use App\Model\app\Router\SatelitalRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SatelitalController extends AbstractController
{

    private $breadcrumbManager;
    private $satelitalManager;
    private $satelitalRouter;
    private $servicioManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        SatelitalManager $satelitalManager,
        SatelitalRouter $satelitalRouter,
        ServicioManager $servicioManager,
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->satelitalManager = $satelitalManager;
        $this->satelitalRouter = $satelitalRouter;
        $this->servicioManager = $servicioManager;
    }

    /**
     * @Route("/satelital/{idOrg}/list", name="satelital_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_SATELITAL_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Satelital");

        $menu = array(
            1 => array($this->satelitalRouter->btnNew($organizacion)),
        );

        $satelital = $this->satelitalManager->findAllByOrganizacion($organizacion->getId());
        return $this->render('monitor/Satelital/list.html.twig', array(
            'menu' => $menu,
            'satelitales' => $satelital,
        ));
    }

    /**
     */

    /**
     * @Route("/satelital/{id}/show", name="satelital_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_VER")
     */
    public function showAction(Request $request, Satelital $satelital)
    {

        $deleteForm = $this->createDeleteForm($satelital->getId());
        if (!$satelital) {
            throw $this->createNotFoundException('Código de Satelital no encontrado.');
        }
        $arrIds = array();
        $menu = array(
            1 => array(
                $this->satelitalRouter->btnEdit($satelital),
                $this->satelitalRouter->btnDelete($satelital),
            )
        );
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $satelital->getNombre());

        foreach ($satelital->getServicios() as $servicio) {
            $arrIds[] = $servicio->getId();
        }
        return $this->render('monitor/Satelital/show.html.twig', array(
            'menu' => $menu,
            'satelital' => $satelital,
            'delete_form' => $deleteForm->createView(),
            'arrIds' => json_encode($arrIds, true),
            
        ));
    }

    /**
     * @Route("/monitor/satelital/{idOrg}/new", name="satelital_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_SATELITAL_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {

        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el satelital
        $satelital = $this->satelitalManager->create($organizacion);

        $form = $this->createForm(SatelitalType::class, $satelital);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->satelitalManager->save($satelital)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El satelital se ha creado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }


        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Satelital");
        return $this->render('monitor/Satelital/new.html.twig', array(
            'organizacion' => $organizacion,
            'satelital' => $satelital,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/monitor/satelital/{id}/edit", name="satelital_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SATELITAL_EDITAR")
     */
    public function editAction(Request $request, Satelital $satelital)
    {
        if (!$satelital) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $form = $this->createForm(SatelitalType::class, $satelital);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->satelitalManager->save($satelital)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Servicio");
        return $this->render('monitor/Satelital/edit.html.twig', array(
            'satelital' => $satelital,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/monitor/satelital/{id}/delete", name="satelital_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_SATELITAL_ELIMINAR")
     */
    public function deleteAction(Request $request, Satelital $satelital)
    {

        $form = $this->createDeleteForm($satelital->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->satelitalManager->deleteById($satelital->getId())) {
                $this->setFlash('success', 'Satelital eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el servicio del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/monitor/satelital/deleteservicio", name="satelital_deleteservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_AGREGAR")
     */
    public function deleteservicioAction(Request $request)
    {
        $arrIds = array();
        $id = intval($request->get('id'));
        $idSatelital = intval($request->get('idSatelital'));
        $servicio = $this->servicioManager->find($id);
        $satelital = $this->satelitalManager->find($idSatelital);
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');

        if (!$servicio) {
            throw $this->createNotFoundException('Servicio no encontrado.');
        }

        $servicio->setSatelital(null);
        if ($this->servicioManager->save($servicio)) {
            foreach ($satelital->getServicios() as $servicio) {
                $arrIds[] = $servicio->getId();
            }
            $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $satelital->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }


    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
