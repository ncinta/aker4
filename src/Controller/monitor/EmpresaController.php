<?php

namespace App\Controller\monitor;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\monitor\EmpresaType;
use App\Form\apmon\UsuarioNewType;
use App\Entity\Organizacion;
use App\Entity\Empresa;
use App\Entity\Contacto;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ContactoManager;
use App\Model\app\Router\EmpresaRouter;
use App\Model\app\Router\ContactoRouter;
use App\Model\app\Router\UsuarioRouter;
use App\Model\monitor\EmpresaManager;
use App\Form\app\ContactoType;
use App\Model\app\PaisManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UsuarioManager;
use App\Model\app\NotificadorAgenteManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class EmpresaController extends AbstractController
{

    private $breadcrumbManager;
    private $emrpesaManager;
    private $contactoManager;
    private $empresaRouter;
    private $contactoRouter;
    private $usuarioRouter;
    private $organizacionManager;
    private $paisManager;
    private $usuarioManager;
    private $userlogin;
    private $notificadorAgenteManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        EmpresaManager $emrpesaManager,
        ContactoManager $contactoManager,
        EmpresaRouter $empresaRouter,
        ContactoRouter $contactoRouter,
        UsuarioRouter $usuarioRouter,
        PaisManager $paisManager,
        UsuarioManager $usuarioManager,
        OrganizacionManager $organizacionManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UserLoginManager $userlogin
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->emrpesaManager = $emrpesaManager;
        $this->organizacionManager = $organizacionManager;
        $this->contactoManager = $contactoManager;
        $this->empresaRouter = $empresaRouter;
        $this->contactoRouter = $contactoRouter;
        $this->usuarioRouter = $usuarioRouter;
        $this->paisManager = $paisManager;
        $this->usuarioManager = $usuarioManager;
        $this->userlogin = $userlogin;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    /**
     * @Route("/monitor/empresa/{idOrg}/list", name="empresa_list")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Clientes");

        $menu = array(
            1 => array($this->empresaRouter->btnNew($organizacion)),
        );

        $empresas = $this->emrpesaManager->findAllByOrganizacion($organizacion->getId());

        return $this->render('monitor/Empresa/list.html.twig', array(
            'menu' => $menu,
            'empresas' => $empresas,
        ));
    }

    /**
     */

    /**
     * @Route("/monitor/empresa/{id}/show", name="empresa_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_VER")
     */
    public function showAction(Request $request, Empresa $empresa)
    {

        $deleteForm = $this->createDeleteForm($empresa->getId());
        if (!$empresa) {
            throw $this->createNotFoundException('Código de cliente no encontrado.');
        }
        $menu = array(
            1 => array(
                $this->empresaRouter->btnEdit($empresa),
                $this->empresaRouter->btnDelete($empresa),
            ),
        );
        if ($this->userlogin->isGranted('ROLE_MONITOR_ADMIN')) {
            $menu[2] = array(
                    $this->empresaRouter->btnNewContacto($empresa->getOrganizacion()),
                    $this->usuarioRouter->btnNew()
            );
        }


        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $empresa->getNombre());



        $contacto = $this->contactoManager->create($empresa->getOrganizacion());
        $usuario = $this->usuarioManager->create($empresa->getOrganizacion());
        return $this->render('monitor/Empresa/show.html.twig', array(
            'menu' => $menu,
            'formContacto' => $this->createForm(ContactoType::class, $contacto)->createView(),
            'formUsuarioNew' => $this->createForm(UsuarioNewType::class, $usuario, array('paises' =>  $this->paisManager->getArrayPaises()))->createView(),
            'empresa' => $empresa,
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView(),
            'tipoNotificaciones' => $this->contactoManager->getAllTipoNotif()
          

        ));
    }

    /**
     * @Route("/monitor/empresa/{idOrg}/new", name="empresa_new")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo la empresa
        $empresa = $this->emrpesaManager->create($organizacion);
        $form = $this->createForm(EmpresaType::class, $empresa);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->emrpesaManager->save($empresa)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El cliente se ha creado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }


        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Cliente");
        return $this->render('monitor/Empresa/new.html.twig', array(
            'organizacion' => $organizacion,
            'empresa' => $empresa,
            'form' => $form->createView(),
        ));
    }

    /**
     * Crea un nuevo servicio a partir de un equipo. 
     * @IsGranted("ROLE_EMPRESA_AGREGAR")
     */
    public function newmodalAction(Request $request)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Crear Cliente");

        if ($request->getMethod() == 'GET') {
            $idorg = $request->get('idorg');
            $nombre = $request->get('nombre');
            $organizacion = $this->organizacionManager->find($idorg);
            if (!$organizacion) {
                throw $this->createNotFoundException('Organización no encontrado.');
            }
            //creo la empresa
            $empresa = $this->emrpesaManager->create($organizacion);
            $empresa->setNombre($nombre);
            $this->breadcrumbManager->pop();
            if ($this->emrpesaManager->save($empresa)) {
                $empresas = $this->emrpesaManager->findAllByOrganizacion($organizacion);
                $select_obj = array();
                foreach ($empresas as $empresa) {
                    $select_obj[$empresa->getId()] = $empresa->getNombre();
                }

                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El Cliente se ha creado con éxito'));
                return new Response(json_encode($select_obj));
            }
        }
    }

    /**
     * @Route("/monitor/empresa/{id}/edit", name="empresa_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_EDITAR")
     */
    public function editAction(Request $request, Empresa $empresa)
    {

        if (!$empresa) {
            throw $this->createNotFoundException('Código de cliente no encontrado.');
        }

        $form = $this->createForm(EmpresaType::class, $empresa);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->emrpesaManager->save($empresa)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Cliente");
        return $this->render('monitor/Empresa/edit.html.twig', array(
            'empresa' => $empresa,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/monitor/empresa/{id}/delete", name="empresa_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_ELIMINAR")
     */
    public function deleteAction(Request $request, Empresa $empresa)
    {

        $form = $this->createDeleteForm($empresa->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->breadcrumbManager->pop();

            if ($this->emrpesaManager->deleteById($empresa->getId())) {
                $this->setFlash('success', 'Cliente eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el cliente del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/monitor/empresa/contacto", name="empresa_contacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_EDITAR")
     */
    public function contactoAction(Request $request)
    {

        $idEmpresa = intval($request->get('idEmpresa'));
        $empresa = $this->emrpesaManager->find($idEmpresa);
        $usuario = null;
        if (!$empresa) {
            throw $this->createNotFoundException('Cliente no encontrado.');
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);
        if ($request->getMethod() == 'POST' && isset($tmp['contacto'])) {
            $data = $tmp['contacto'];

            $contacto =  new Contacto();
            $contacto->setOrganizacion($empresa->getOrganizacion());
            $contacto->setEmpresa($empresa);

            $contacto = $this->contactoManager->setData($contacto, $data);

            if ($this->contactoManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion()) == null) {
                if ($contacto = $this->contactoManager->save($contacto)) {
                    $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                    //NOTIF_APP para aplcicacion , NOTIF_EMAIL para email
                    $notifApp = $contacto->getByCodename('NOTIF_APP');
                    if ($notifApp) {
                        $usuario = $this->usuarioManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion());
                    }

                    $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                        'contactos' => $empresa->getContactos(),
                    

                    ));
                    return new Response(json_encode(
                        array(
                            'html' => $html,
                            'existe' => false,
                            'notifApp' => $notifApp,
                            'usuario' => $usuario,
                            'nombre' => $contacto->getNombre(),
                            'email' => $contacto->getEmail(),
                            'celular' => $contacto->getCelular()
                        )
                    ), 200);
                }
            }else{
                return new Response(json_encode(array('existe' => true)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }



    /**
     * @Route("/monitor/empresa/deletecontacto", name="empresa_deletecontacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTACTO_AGREGAR")
     */
    public function deletecontactoAction(Request $request)
    {
        $id = intval($request->get('id'));
        $contacto = $this->contactoManager->find($id);
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        $empresa = $contacto->getEmpresa();
        if (!$contacto) {
            throw $this->createNotFoundException('Contacto no encontrado.');
        }


        if ($this->contactoManager->delete($contacto)) {
            $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                'contactos' => $empresa->getContactos(),
               
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/empresa/newusuario", name="empresa_newusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EMPRESA_EDITAR")
     */
    public function usuarionewAction(Request $request)
    {
        $idEmpresa = intval($request->get('idEmpresa'));
        $empresa = $this->emrpesaManager->find($idEmpresa);
        if (!$empresa) {
            throw $this->createNotFoundException('Empresa no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        if (isset($tmp['usuario'])) {
            $data = $this->usuarioManager->createForMonitor($tmp, $empresa);
            if ($data === 'true') {
                //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                $html = $this->renderView('monitor/Usuario/usuarios.html.twig', array(
                    'usuarios' => $empresa->getUsuarios(),
                 

                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }

            return new Response($data, 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }




    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
