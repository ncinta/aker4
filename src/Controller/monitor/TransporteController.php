<?php

namespace App\Controller\monitor;

use App\Entity\Chofer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\monitor\TransporteType;
use App\Form\app\ServicioType;
use App\Entity\Organizacion;
use App\Entity\Transporte;
use App\Entity\Contacto;
use App\Entity\Licencia;
use App\Form\apmon\ChoferType;
use App\Form\apmon\LicenciaEditType;
use App\Form\apmon\LicenciaType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UsuarioManager;
use App\Model\app\PaisManager;
use App\Model\monitor\SatelitalManager;
use App\Model\monitor\TransporteManager;
use App\Model\app\Router\TransporteRouter;
use App\Model\app\Router\ChoferRouter;
use App\Model\app\Router\ContactoRouter;
use App\Model\app\Router\ServicioRouter;
use App\Model\app\Router\UsuarioRouter;
use App\Form\app\ContactoType;
use App\Form\apmon\UsuarioNewType;
use App\Model\app\ContactoManager;
use App\Form\app\ServicioNewType;
use App\Form\monitor\ServicioTransporteType;
use App\Model\app\ChoferManager;
use App\Model\app\NotificadorAgenteManager;
use App\Model\app\TipoServicioManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\app\UtilsManager;
use PharIo\Manifest\License;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class TransporteController extends AbstractController
{
    protected $utilsManager;
    private $breadcrumbManager;
    private $transporteManager;
    private $contactoManager;
    private $servicioManager;
    private $userlogin;
    private $satelitalManager;
    private $tipoServicioManager;
    private $usuarioManager;
    private $paisManager;
    private $transporteRouter;
    private $choferRouter;
    private $organizacionManager;
    private $choferManager;
    private $itinerarioManager;
    private $notificadorAgenteManager;
    private $contactoRouter;
    private $servicioRouter;
    private $usuarioRouter;
    private $moduloMonitor;


    function __construct(
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        TransporteManager $transporteManager,
        ContactoManager $contactoManager,
        ServicioManager $servicioManager,
        UserloginManager $userlogin,
        SatelitalManager $satelitalManager,
        TipoServicioManager $tipoServicioManager,
        UtilsManager $utilsManager,
        UsuarioManager $usuarioManager,
        PaisManager $paisManager,
        ItinerarioManager $itinerarioManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        ContactoRouter $contactoRouter,
        TransporteRouter $transporteRouter,
        ServicioRouter $servicioRouter,
        UsuarioRouter $usuarioRouter,
        ChoferRouter $choferRouter,
        ChoferManager $choferManager,

    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->transporteManager = $transporteManager;
        $this->contactoManager = $contactoManager;
        $this->servicioManager = $servicioManager;
        $this->transporteRouter = $transporteRouter;
        $this->organizacionManager = $organizacionManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->usuarioManager = $usuarioManager;
        $this->paisManager = $paisManager;
        $this->satelitalManager = $satelitalManager;
        $this->contactoRouter = $contactoRouter;
        $this->servicioRouter = $servicioRouter;
        $this->usuarioRouter = $usuarioRouter;
        $this->choferRouter = $choferRouter;
        $this->userlogin = $userlogin;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->choferManager = $choferManager;
        $this->utilsManager = $utilsManager;
    }

    /**
     * @Route("/monitor/transporte/{idOrg}/list", name="transporte_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_TRANSPORTE_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Transportes");

        $menu = array(
            1 => array($this->transporteRouter->btnNew($organizacion)),
        );

        $transportes = $this->transporteManager->findAllByOrganizacion($organizacion->getId());
        return $this->render('monitor/Transporte/list.html.twig', array(
            'menu' => $menu,
            'transportes' => $transportes,
        ));
    }

    /**
     */

    /**
     * @Route("/monitor/transporte/{id}/show", name="transporte_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_VER")
     */
    public function showAction(Request $request, Transporte $transporte)
    {
        $arrIds = array();
        $deleteForm = $this->createDeleteForm($transporte->getId());
        if (!$transporte) {
            throw $this->createNotFoundException('Código de Transporte no encontrado.');
        }

        $menu[1] =  array(
            $this->transporteRouter->btnEdit($transporte),
            $this->transporteRouter->btnDelete($transporte)

        );
        if ($this->userlogin->isGranted('ROLE_MONITOR_ADMIN')) {
            $menu[2] = array(
                $this->contactoRouter->btnNew($transporte->getOrganizacion()),
                $this->servicioRouter->btnSelect(),
                $this->usuarioRouter->btnNew(),
                $this->choferRouter->btnNew()

            );
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $transporte->getNombre());

        $serviciosOrg = $this->itinerarioManager->getServiciosByEntity($this->userlogin->getUser(), false);

        $this->moduloMonitor = $this->userlogin->isGranted('ROLE_ITINERARIO_VER');
        //creo el servicio
        $servicio = $this->servicioManager->create(null, $transporte->getOrganizacion());
        foreach ($servicio->getArrayStrTipoObjetos() as $key => $value) {
            $arr[$value] = $key;
        }

        foreach ($transporte->getServicios() as $servicio) {
            $arrIds[] = $servicio->getId();
        }

        $options = array(
            'arrayStrTipoObjetos' => $arr,
            'em' => $this->getEntityManager(),
            'organizacion' => $transporte->getOrganizacion(),
            'arrayStrTipoObjetos' => $servicio->getArrayStrTipoObjetos(),
            'required_portal' => $this->organizacionManager->isModuloEnabled($servicio->getOrganizacion(), 'CLIENTE_PORTAL'),
            'arrayStrMedidorCombustible' => $servicio->getArrayStrMedidorCombustible(),
            'monitor' => $this->moduloMonitor,
        );

        // die('////<pre>' . nl2br(var_export(count($serviciosOrg), true)) . '</pre>////');
        $contacto = $this->contactoManager->create($transporte->getOrganizacion());

        $usuario = $this->usuarioManager->create($transporte->getOrganizacion());
        $chofer = new Chofer();
        $licencia = new Licencia();

        return $this->render('monitor/Transporte/show.html.twig', array(
            'menu' => $menu,
            'transporte' => $transporte,
            'formContacto' => $this->createForm(ContactoType::class, $contacto)->createView(),
            'formServicio' => $this->createForm(ServicioTransporteType::class, null, array('servicios' => $serviciosOrg))->createView(),
            'formServicioNew' => $this->createForm(ServicioNewType::class, $servicio, $options)->createView(),
            'formChoferNew' => $this->createForm(ChoferType::class, $chofer)->createView(),
            'formUsuarioNew' => $this->createForm(UsuarioNewType::class, $usuario, array('paises' =>  $this->paisManager->getArrayPaises()))->createView(),
            'formLicenciaNew' => $this->createForm(LicenciaType::class, $licencia)->createView(),
            'formLicenciaEdit' => $this->createForm(LicenciaEditType::class, $licencia)->createView(),
            'delete_form' => $deleteForm->createView(),
            'arrIds' => json_encode($arrIds, true),
            'moduloMonitor' => $this->moduloMonitor,
            'usuario' => $usuario,
            'tipoNotificaciones' => $this->contactoManager->getAllTipoNotif()

        ));
    }

    /**
     * @Route("/monitor/transporte/{idOrg}/new", name="transporte_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_TRANSPORTE_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {

        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el transporte
        $transporte = $this->transporteManager->create($organizacion);

        $form = $this->createForm(TransporteType::class, $transporte);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->transporteManager->save($transporte)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El transporte se ha creado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }


        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Transporte");
        return $this->render('monitor/Transporte/new.html.twig', array(
            'organizacion' => $organizacion,
            'transporte' => $transporte,
            'form' => $form->createView(),
        ));
    }

    /**
     * Crea un nuevo servicio a partir de un equipo. 
     * @IsGranted("ROLE_TRANSPORTE_AGREGAR")
     */
    public function newmodalAction(Request $request)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Crear Transporte");

        if ($request->getMethod() == 'GET') {
            $idorg = $request->get('idorg');
            $nombre = $request->get('nombre');
            $organizacion = $this->organizacionManager->find($idorg);
            if (!$organizacion) {
                throw $this->createNotFoundException('Organización no encontrado.');
            }
            //creo el transporte
            $transporte = $this->transporteManager->create($organizacion);
            $transporte->setNombre($nombre);
            $this->breadcrumbManager->pop();
            if ($this->transporteManager->save($transporte)) {
                $transportes = $this->transporteManager->findAllByOrganizacion($organizacion);
                $select_obj = array();
                foreach ($transportes as $transporte) {
                    $select_obj[$transporte->getId()] = $transporte->getNombre();
                }
                //die('////<pre>' . nl2br(var_export('wer', true)) . '</pre>////');
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El transporte se ha creado con éxito'));
                return new Response(json_encode($select_obj));
            }
        }
    }

    /**
     * @Route("/monitor/transporte/{id}/edit", name="transporte_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function editAction(Request $request, Transporte $transporte)
    {

        if (!$transporte) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $form = $this->createForm(TransporteType::class, $transporte);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->transporteManager->save($transporte)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Servicio");
        return $this->render('monitor/Transporte/edit.html.twig', array(
            'transporte' => $transporte,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/monitor/transporte/{id}/delete", name="transporte_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_ELIMINAR")
     */
    public function deleteAction(Request $request, Transporte $transporte)
    {
        $form = $this->createDeleteForm($transporte->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($this->transporteManager->deleteById($transporte->getId())) {
                $this->setFlash('success', 'Transporte eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el servicio del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }


    /**
     * @Route("/monitor/transporte/contacto", name="transporte_contacto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function contactoAction(Request $request)
    {
        //  die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        $idTransporte = intval($request->get('idTransporte'));
        $transporte = $this->transporteManager->find($idTransporte);
        $usuario = null;
        if (!$transporte) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);
        if ($request->getMethod() == 'POST' && isset($tmp['contacto'])) {
            $data = $tmp['contacto'];

            $contacto =  new Contacto();
            $contacto->setOrganizacion($transporte->getOrganizacion());
            $contacto->setTransporte($transporte);

            $contacto = $this->contactoManager->setData($contacto, $data);



            if ($this->contactoManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion()) == null) { //me fijo que no exista primero
                // die('////<pre>' . nl2br(var_export($this->contactoManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion()), true)) . '</pre>////');
                if ($contacto = $this->contactoManager->save($contacto)) {
                    $notificar = $this->notificadorAgenteManager->notificar($contacto, 1);
                    //NOTIF_APP para aplcicacion , NOTIF_EMAIL para email
                    $notifApp = $contacto->getByCodename('NOTIF_APP');
                    if ($notifApp) {
                        $usuario = $this->usuarioManager->findByPhone($contacto->getCelular(), $this->userlogin->getOrganizacion());
                    }

                    $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                        'contactos' => $transporte->getContactos(),


                    ));
                    return new Response(json_encode(
                        array(
                            'html' => $html,
                            'existe' => false,
                            'notifApp' => $notifApp,
                            'usuario' => $usuario,
                            'nombre' => $contacto->getNombre(),
                            'email' => $contacto->getEmail(),
                            'celular' => $contacto->getCelular()
                        )
                    ), 200);
                }
            } else {
                return new Response(json_encode(array('existe' => true)), 200);
                $html = $this->renderView('monitor/Contacto/contactos.html.twig', array(
                    'contactos' => $transporte->getContactos(),

                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/transporte/addservicio", name="transporte_addservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function servicioaddAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();
        $id = intval($request->get('id'));
        $idTransporte = intval($request->get('idTransporte'));
        $transporte = $this->transporteManager->find($idTransporte);
        if (!$transporte) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        //  die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (isset($tmp['servicio'])) {
            foreach ($tmp['servicio']['servicio'] as $value) {
                $pos = strpos($value, '-'); //buscamos el id del usuario
                $id = substr($value, 0, $pos - 1);
                $servicio = $this->servicioManager->find($id);
                $servicio->setTransporte($transporte);
                $em->persist($servicio);
            }
            $em->flush();

            foreach ($transporte->getServicios() as $servicio) {
                $arrIds[] = $servicio->getId();
            }
            $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $transporte->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/transporte/newservicio", name="transporte_newservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function servicionewAction(Request $request)
    {
        $em = $this->getEntityManager();
        $arrIds = array();

        $idTransporte = intval($request->get('idTransporte'));
        $transporte = $this->transporteManager->find($idTransporte);
        if (!$transporte) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        if (isset($tmp['servicio'])) {

            $servicio = $this->servicioManager->create(null, $transporte->getOrganizacion());
            $servicio->setNombre($tmp['servicio']['nombre']);
            $servicio->setDatoFiscal($tmp['servicio']['dato_fiscal']);
            $servicio->setColor($tmp['servicio']['color']);
            $servicio->setTipoObjeto($tmp['servicio']['tipoObjeto']);
            $servicio->setTransporte($transporte);
            $servicio->setSatelital($this->satelitalManager->find(intval($tmp['servicio']['satelital'])));
            $servicio->setTipoServicio($this->tipoServicioManager->findById(intval($tmp['servicio']['tipoServicio'])));
            $servicio->setIsCustodio(isset($tmp['servicio']['iscustodio']));
            if ($this->servicioManager->save($servicio)) {
                $notificar = $this->notificadorAgenteManager->notificar($servicio, 1);  //1 UPDATE, 2 DELETE
                foreach ($transporte->getServicios() as $servicio) {
                    $arrIds[] = $servicio->getId();
                }
                $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                    'servicios' => $transporte->getServicios(),
                    'arrIds' => json_encode($arrIds, true),
                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/transporte/newusuario", name="transporte_newusuario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function usuarionewAction(Request $request)
    {
        $idTransporte = intval($request->get('idTransporte'));
        $transporte = $this->transporteManager->find($idTransporte);
        if (!$transporte) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        if (isset($tmp['usuario'])) {
            $data = $this->usuarioManager->createForMonitor($tmp, $transporte);
            if ($data === 'true') {

                $html = $this->renderView('monitor/Usuario/usuarios.html.twig', array(
                    'usuarios' => $transporte->getUsuarios(),


                ));
                return new Response(json_encode(array('html' => $html)), 200);
            }

            return new Response($data, 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/monitor/transporte/newchofer", name="transporte_newchofer")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function chofernewAction(Request $request)
    {
        $idTransporte = intval($request->get('idTransporte'));
        $transporte = $this->transporteManager->find($idTransporte);
        if (!$transporte) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        // die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (isset($tmp['chofer'])) {
            $chofer = $this->choferManager->create($transporte->getOrganizacion());
            $chofer->setTransporte($transporte);
            $chofer->setNombre($tmp['chofer']['nombre']);
            $chofer->setCuit($tmp['chofer']['cuit']);
            $chofer->setDocumento($tmp['chofer']['documento']);
            $chofer->setTelefono($tmp['chofer']['telefono']);
            $chofer->setApiCode($chofer->getTelefono() ? $this->utilsManager->getApiCode() : null);
            $data = $this->choferManager->save($chofer);

            $html = $this->renderView('apmon/Chofer/choferes.html.twig', array(
                'choferes' => $transporte->getChoferes(),


            ));
            return new Response(json_encode(array('html' => $html)), 200);


          
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }


    /**
     * @Route("/monitor/transporte/deleteservicio", name="transporte_deleteservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TRANSPORTE_EDITAR")
     */
    public function deleteservicioAction(Request $request)
    {
        $arrIds = array();
        $id = intval($request->get('id'));
        $idTransporte = intval($request->get('idTransporte'));
        $servicio = $this->servicioManager->find($id);
        $transporte = $this->transporteManager->find($idTransporte);
        // die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');

        if (!$servicio) {
            throw $this->createNotFoundException('Servicio no encontrado.');
        }

        $servicio->setTransporte(null);
        if ($this->servicioManager->save($servicio)) {
            foreach ($transporte->getServicios() as $servicio) {
                $arrIds[] = $servicio->getId();
            }
            $html = $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $transporte->getServicios(),
                'arrIds' => json_encode($arrIds, true),
            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }

        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
