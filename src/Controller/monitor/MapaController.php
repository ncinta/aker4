<?php

namespace App\Controller\monitor;

/**
 * Description of MapaController
 *
 * @author nicolas
 */

use App\Entity\EventoHistorial;
use App\Entity\Itinerario;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Form\monitor\SeguimientoType;
use App\Model\monitor\EmpresaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\TransporteManager;
use App\Model\monitor\SatelitalManager;
use App\Model\monitor\EventoItinerarioManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\GmapsManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\EventoTemporalManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MapaController extends AbstractController
{

    private $breadcrumbManager;
    private $itinerarioManager;
    private $servicioManager;
    private $empresaManager;
    private $transporteManager;
    private $satelitalManager;
    private $userloginManager;
    private $gmapsManager;
    private $referenciaManager;
    private $eventhistManager;
    private $eventoItinerarioManager;
    private $eventoTemporalManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ItinerarioManager $itinerarioManager,
        ReferenciaManager $referenciaManager,
        ServicioManager $servicioManager,
        EmpresaManager $empresaManager,
        TransporteManager $transporteManager,
        EventoHistorialManager $eventhistManager,
        SatelitalManager $satelitalManager,
        UserLoginManager $userloginManager,
        GmapsManager $gmapsManager,
        EventoItinerarioManager $eventoItinerarioManager,
        EventoTemporalManager $eventoTemporalManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->servicioManager = $servicioManager;
        $this->empresaManager = $empresaManager;
        $this->transporteManager = $transporteManager;
        $this->satelitalManager = $satelitalManager;
        $this->userloginManager = $userloginManager;
        $this->gmapsManager = $gmapsManager;
        $this->referenciaManager = $referenciaManager;
        $this->eventhistManager = $eventhistManager;
        $this->eventoItinerarioManager = $eventoItinerarioManager;
        $this->eventoTemporalManager = $eventoTemporalManager;
    }

    /**
     * @Route("/monitor/itinerario/{idOrg}/mapa", name="itinerario_mapa")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {
        $servicios = array();
        $data = array();
        $idsServ = array();
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Mapa");

        $itinerarios = $this->getItinerarios();    //obtengo los itineararios del usuario

        $options = array(
            'empresas' => $this->empresaManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'satelitales' => $this->satelitalManager->findAllByOrganizacion($organizacion),
            'itinerarios' => $itinerarios
        );
        foreach ($itinerarios as $itinerario) {
            $itinerarioServicios = $itinerario->getServicios();
            $idsRef = array();
            foreach ($itinerarioServicios as $servicio) {
                $servicios[] = $servicio->getServicio();
                $labelClass[$servicio->getServicio()->getId()] = 'servicio';
            }

            //obtengo las referencias del itinerario            
            foreach ($itinerario->getPois() as $poi) {
                $idsRef[$itinerario->getId()][] = $poi->getReferencia()->getId();
            }

            $data[$itinerario->getId()] = array(
                'itinerario' => $itinerario,
                'idsRef' => $idsRef,
                'servicios' => $this->servicioManager->checkStatusServicioItinerario($itinerario),
            );

            //aca seteo el color del marker por medio de su clase.            
            foreach ($data[$itinerario->getId()]['servicios'] as $i => $value) {
                if (isset($data[$itinerario->getId()]['servicios'][$i]['labelColor'])) {
                    $labelClass[$i] = 'servicio_' . $data[$itinerario->getId()]['servicios'][$i]['labelColor'];
                }
            }
        }

        $mapa = $this->gmapsManager->createMap(true);
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $colores = array();
        foreach ($servicios as $servicio) {
            if (!is_null($servicio->getUltLatitud())) {
                $idsServ[] = $servicio->getId();

                $colores['servicio' . $servicio->getColor()] = is_null($servicio->getColor()) ? '#FFA500' : '#' . $servicio->getColor();  //hago un array con los colores.

                $this->gmapsManager->addMarkerServicio($mapa, $servicio, true, $labelClass[$servicio->getId()]);

                if ($servicio->getUltValido() && $servicio->getUltLatitud() != null && $servicio->getUltLongitud() != null) {
                    if ($min_lat === false) {
                        $min_lat = $max_lat = $servicio->getUltLatitud();
                        $min_lng = $max_lng = $servicio->getUltLongitud();
                    } else {
                        $min_lat = min($min_lat, $servicio->getUltLatitud());
                        $max_lat = max($max_lat, $servicio->getUltLatitud());
                        $min_lng = min($min_lng, $servicio->getUltLongitud());
                        $max_lng = max($max_lng, $servicio->getUltLongitud());
                    }
                }
            }
        }

        $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
        return $this->render(
            'monitor/Mapa/itinerarios.html.twig',
            array(
                'filtro' => 'Filtro: default',
                'organizacion' => $organizacion,
                'form' => $this->createForm(SeguimientoType::class, null, $options)->createView(),
                'data' => $data,
                'mapa' => $mapa,
                'showRef' => array(),
                'itinerario' => new Itinerario(),
                'colores' => $colores,
                'eventos' => array(),
                'idsServ' => $idsServ,
                'default_referencias' => $this->gmapsManager->getShowReferencias() ? '1' : '0',
            )
        );
    }

    /**
     * @Route("/monitor/mapa/itinerario/historico", name="mapa_itinerario_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function historicoAction(Request $request)
    {

        $tmp = array();
        $selectItis = array();
        $data = array();
        $eventos = array();
        parse_str($request->get('formHistIt'), $tmp);
        $organizacion = $this->userloginManager->getOrganizacion();
        $user = $this->userloginManager->getUser();
        $form = $tmp['historico'];

        if ($form) {
            $empresas = isset($form['empresa']) ? array_map('intval', $form['empresa']) : null;
            $satelitales = isset($form['satelital']) ? array_map('intval', $form['satelital']) : null;
            
          // die('////<pre>' . nl2br(var_export($satelitales, true)) . '</pre>////');
            $transportes = isset($form['transporte']) ? array_map('intval', $form['transporte']) : null;
            $itinerarios = $this->itinerarioManager->findByOptions($organizacion, '-1', $empresas, $satelitales, $transportes);
            $itinerariosAll =$this->itinerarioManager->findInCurso($user);
            $servicios = array();
            $visibles = array();
            foreach ($itinerarios as $itinerario) {
                $idsRef = array();
                if ($itinerario->getPois() != null) {
                    foreach ($itinerario->getPois() as $poi) {
                        $idsRef[$itinerario->getId()][] = $poi->getReferencia()->getId();
                    }
                }

                if (count($itinerario->getServicios()) > 0) { //si no tiene servicios es en vano mostrarlos
                    $data[] = array(
                        'itinerario' => $itinerario,
                        'idsRef' => $idsRef,
                        'servicios' => $this->servicioManager->checkStatusServicioItinerario($itinerario),
                    );
                }

                if (count($itinerarios) > 0) {
                    foreach ($itinerarios as $itinerario) {
                        $idsRef = array();
                        if ($itinerario->getPois() != null) {
                            foreach ($itinerario->getPois() as $poi) {
                                $idsRef[$itinerario->getId()][] = $poi->getReferencia()->getId();
                            }
                        }

                        foreach ($itinerario->getServicios() as $servicio) {
                            $visibles[$servicio->getServicio()->getId()] = true;
                        }


                        if (count($itinerario->getServicios()) > 0) { //si no tiene servicios es en vano mostrarlos
                            $servicios[] = $servicio->getServicio();
                            $data[] = array(
                                'itinerario' => $itinerario,
                                'idsRef' => $idsRef,
                                'servicios' => $this->servicioManager->checkStatusServicioItinerario($itinerario),
                            );
                        }
                        $selectItis[] = $itinerario->getId();
                    }
                } else {
                    $selectItis[] = -1;
                }
                
                foreach ($itinerariosAll as $iti) { //es para ocultar los otros
                    foreach ($iti->getServicios() as $servicio) {
                        if (!in_array($iti, $itinerarios))
                        $visibles[$servicio->getServicio()->getId()] = false;
                    }
                }
                
              //  die('////<pre>' . nl2br(var_export($selectItis, true)) . '</pre>////');
                //$eventos = $this->getEventos($servicios);
                return new Response(
                    json_encode(
                        array(
                            'html' => $this->renderView('monitor/Mapa/itinerario.html.twig', array(
                                'data' => $data,
                                'showRef' => array()
                            )),
                            'selectItis' => json_encode($selectItis),
                            'htmlEventos' => $this->renderView('monitor/Mapa/eventos.html.twig', array(
                                'eventos' => null
                            )),
                        )
                    )
                );
            }
            return new Response(json_encode('data'));
        }
        return new Response(json_encode('error'));
    }

    private function getStatusServicio($servicio, $referencias = null)
    {
        $status = array(
            'st_ultreporte' => $this->servicioManager->getStatusUltReporte($servicio),
            'direccion' => '',
            'icono' => $this->gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            'cerca' => array('leyenda' => '---')
        );


        $status['direccion'] = 'aca';


        if ($referencias) {
            $cercanas = $this->servicioManager->buscarCercania($servicio, $referencias);
            if (count($cercanas) > 0) {
                $i = 0;
                foreach ($cercanas as $key => $value) {
                    if ($value['adentro']) {
                        $i = $key;
                        break;
                    }
                }

                if (!is_null($cercanas)) {
                    $status['cerca']['leyenda'] = $cercanas[$i]['leyenda'];
                    $status['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                } else {
                    $status['cerca']['leyenda'] = '-!!!';
                }
            }
        }
        return $status;
    }

    /**
     * @Route("/monitor/itinerario/referencia/viewajax", name="itinerario_referencias_viewAJAX")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function referenciasviewAJAXAction(Request $request)
    {
        $visible = !((intval($request->get('visible'))) % 2 == 0);    //determina si es visible o no.
        $referencias = array();
        foreach ($request->get('referencias') as $key => $value) {
            $referencias[] = $this->referenciaManager->find($value);
        }
        if (intval($request->get('visible')) == 1) {   //es el priemro de todos
            $respuesta = $this->gmapsManager->castReferencias($referencias);
        } else {
            $respuesta = $this->gmapsManager->castVisibilidadReferencias($referencias, $visible);
        }

        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/monitor/itinerario/posiciones/renderpanel", name="itinerario_posiciones_renderpanel")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function posicionesrenderAction(Request $request)
    {
        $servicios = array();
        $data = array();
        $serv = json_decode($request->get('servicios'));

        foreach ($serv as $id) {
            $servicios[] = $this->servicioManager->find($id);
        }
        foreach ($servicios as $servicio) {
            foreach ($servicio->getItinerarios() as $itserv) {
                $status = $this->getStatusServicio($servicio, $itserv->getItinerario());
            }
            $data[$servicio->getId()] = $status;
        }
        return new Response(json_encode($data));
    }

    /**
     * @Route("/monitor/itinerario/mapa/showservicios", name="mapa_itinerario_showservicios", methods={"GET"})     
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function showserviciosMapaAction(Request $request)
    {
        $servicios = array();
        $visibles = array();
        $idsRef = array();
        $checkStatus = array();
        $posiciones = array();

        $hiddens = json_decode($request->get('hiddens'));
        $shows = json_decode($request->get('shows'));
        $showRef = $request->get('showRef');

        if (count($shows) == 0) {
            $itinerarios = $this->getItinerarios();

            foreach ($itinerarios as $itinerario) {
                $shows[] = $itinerario->getId();
            }
        }
        foreach ($hiddens as $id) {
            if ($id > 0) {   /// viene 0 o -1 en el json
                $itinerario = $this->itinerarioManager->find($id);
                if ($itinerario != null) {
                    foreach ($itinerario->getPois() as $poi) { //se agregan las referencias al itinerario.
                        $idsRef[$itinerario->getId()][] = $poi->getReferencia()->getId();
                    }
                    foreach ($itinerario->getServicios() as $servicio) {
                        $servicios[] = $servicio->getServicio();
                        //$visibles[$servicio->getId()] = false;
                        $posiciones[$servicio->getServicio()->getId()] = $this->setHidden($servicio->getServicio());
                    }
                }
            }
        }
        $data = array();
        $serviciosShows = array();
        foreach ($shows as $id) {    //recorro los itinerarios a mostrar
            if ($id > 0) {   /// viene 0 o -1 en el json
                $itinerario = $this->itinerarioManager->find($id);
                if ($itinerario != null) {
                    foreach ($itinerario->getPois() as $poi) { //se agregan las referencias al itinerario.
                        $idsRef[$itinerario->getId()][] = $poi->getReferencia()->getId();
                    }
                    foreach ($itinerario->getServicios() as $serv) {
                        $servicio = $serv->getServicio();
                        $servicios[] = $servicio;
                        $serviciosShows[] = $servicio;     //obtengo los servicios a mostrar solamente
                        $visibles[$servicio->getId()] = true;
                    }
                    $checkStatus = $this->servicioManager->checkStatusServicioItinerario($itinerario);
                }

                // si no tiene servicios no se va a mostrar el itinerario en la lista de estados.
                if (count($checkStatus) > 0) {
                    $data[$id] = array(
                        'itinerario' => $itinerario,
                        'idsRef' => $idsRef,
                        'servicios' => $checkStatus,
                    );

                    foreach ($serviciosShows as $servicio) {
                        if (isset($data[$id]['servicios'][$servicio->getId()])) {
                            $posiciones[$servicio->getId()] = $this->setPosicion($servicio, $visibles, $data[$id]['servicios'][$servicio->getId()]);
                        }
                    }
                }
            }
        }

        return new Response(
            json_encode(
                array(
                    'html' => $this->renderView('monitor/Mapa/itinerario.html.twig', array(
                        'data' => $data,
                        'showRef' => $showRef == null ? array() : $showRef
                    )),
                    'posiciones' => json_encode($posiciones),
                )
            )
        );
    }

    /**
     * @Route("/monitor/itinerario/mapa/eventos", name="mapa_itinerario_eventos")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function eventosAction(Request $request)
    {
        $servicios = array();
        $itinerarios = array();
        $its = json_decode($request->get('its'));
        $data = array();
        $serviciosShows = array();
        foreach ($its as $id) {    //recorro los itinerarios a mostrar
            if ($id > 0) {   /// viene 0 o -1 en el json
                $itinerarios[] = $this->itinerarioManager->find($id);
            }
        }

        $eventos = $this->getEventos($itinerarios);        
        if (count($eventos) > 0) {
            return new Response(
                json_encode(
                    array(
                        'htmlEventos' => $this->renderView(
                            'monitor/Mapa/eventos.html.twig',
                            array(
                                'eventos' => $eventos,
                            )
                        )
                    )
                )
            );
        } else {
            return new Response(json_encode(array('htmlEventos' => null)));
        }
    }

    private function getItinerarios()
    {
        $user = $this->userloginManager->getUser();
        return $this->itinerarioManager->findInCurso($user);
    }

    protected function getEventos($itinerarios)
    {
        $data = array();        
        $eventos = $this->eventoTemporalManager->findByItinerarios($itinerarios);      
        $lastIdRead = $this->userloginManager->getUltEventoVisto();

        foreach ($eventos as $evento) {
            $data[] = array(
                'id' => $evento->getId(),
                'fecha' => $evento->getFecha(),
                'itinerario' => $evento->getItinerario()->getCodigo(),
                'servicio' => $evento->getServicio()->getNombre(),
                'nombre' => $evento->getEvento()->getNombre(),
                'titulo' => $evento->getTitulo(),
                'eventohistorial_id' => $evento->getEventoHistorial()->getId(),
                'fechaVista' => null,
                'nuevo' => $evento->getId() > $lastIdRead,
            );            
        }
        return $data;
    }

    protected function setHidden($servicio)
    {
        return array(
            'id_sc' => 'servicio_' . $servicio->getId(),
            'visible' => false,
        );
    }

    protected function setPosicion($servicio, $visibles, $status)
    {
        $posicion = array(
            'id_sc' => 'servicio_' . $servicio->getId(),
            'latitud' => $servicio->getUltLatitud(),
            'longitud' => $servicio->getUltLongitud(),
            'texto' => $servicio->getUltVelocidad() . ' km/h',
            'visible' => true,
            'label_clase' => 'servicio_' . $status['labelColor'],
            'new_icono' => $this->gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
        );
        if ($visibles[$servicio->getId()] == true) {
        } else {
        }
        return $posicion;
    }

    /**
     * @Route("/monitor/itinerario/evento/registrar", name="itinerario_evento_registrar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function registrareventoAction(Request $request)
    {

        $evento = $this->eventhistManager->findById(intval($request->get('id')));
        //aca grabo en la session los nuevos.

        $request->getSession()->set('last_timestamp_evento', new \DateTime());
        $eventos = array(
            'cant_total' => 1,
            'cant_nuevos' => 1,
            'ultimo' => isset($evento) ? $this->procesarUltimoEvento($evento, 1) : null,
        );

        return new Response(json_encode(array('eventos' => $eventos)));
    }

    private function procesarUltimoEvento($evento, $count)
    {

        //armo la botonera para mostar en la pantalla
        $menuEv[] = array('name' => 'Ver Equipo', 'url' => $this->generateUrl(
            'servicio_show',
            array('id' => $evento->getServicio()->getId(), 'tab' => 'main')
        ));
        if (
            $this->userloginManager->isGranted('ROLE_EVENTO_HISTORICO_VER') ||
            $this->userloginManager->isGranted('ROLE_APMON_PANICO_HISTORICO')
        ) {
            $menuEv[] = array('name' => 'Ir al Evento', 'url' => $this->generateUrl('app_eventohistorico_show', array('id' => $evento->getId())));
        }
        if ($evento->getServicio()->getCorteMotor() == true) {
            $menuEv[] = array(
                'name' => 'Corte de Motor',
                'url' => $this->generateUrl('main_programacion_corte', array('id' => $evento->getServicio()->getId())),
            );
        }
        if (
            $evento->getEvento()->getRegistrar() == true && $evento->getFechaVista() == null &&
            ($this->userloginManager->isGranted('ROLE_EVENTO_REGISTRAR') ||
                $this->userloginManager->isGranted('ROLE_APMON_PANICO_REGISTRAR'))
        ) {
            $menuEv[] = array(
                'name' => 'Registrar',
                'url' => $this->generateUrl('app_eventohistorico_registrar', array('id' => $evento->getId())),
            );
        }

        if (is_array($evento->getData())) {
            $data = $evento->getData();
        } else {
            $data = json_decode($evento->getData());
        }

        return array(
            'id' => $evento->getId(),
            'title' => $this->renderView('app/Template/alerta_evento_title.html.twig', array(
                'evento' => $evento,
                'panico' => $evento->getEvento()->getTipoEvento()->getCodename() == 'EV_PANICO',
            )),
            'count' => $this->renderView(
                'app/Template/alerta_evento_count.html.twig',
                array(
                    'count' => $count
                )
            ),
            'body' => $this->renderView('app/Template/alerta_evento_data.html.twig', array(
                'evento' => $evento,
                'body' => $data,
                'popupmenuevento' => $menuEv
            )),
        );
    }

    /**
     * @Route("/monitor/mapa/setupvisto", name="mapa_evento_setupvisto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function setupvistoAction(Request $request)
    {
        $idEvento = intval($request->get('id'));  //es el id del eventoTemporal
        if (!is_null($idEvento) && $idEvento > 0) {
            $evento = $this->eventhistManager->findById($idEvento);
            //dd('class-> '.get_class($evento). ' id: '.$evento->getId());
            if ($evento instanceof EventoHistorial) {
                // die('////<pre>' . nl2br(var_export('asd', true)) . '</pre>////');
                $this->eventhistManager->setupVisto($evento, $this->userloginManager->getUser());
                $historico = $evento;
            } else {
                $this->eventhistManager->setupVistoTemporal($evento, $this->userloginManager->getUser());
                $historico = $evento->getEventoHistorial();
            }
            //aca debo determinar si es un evento itinerario y meterlo en el itinerario correspondiente.                        
            if ($evento->getEvento()->getClase() == 3 && count($evento->getEvento()->getHistoricoItinerario()) > 0) {
                $this->eventoItinerarioManager->setupVisto($evento->getEvento()->getHistoricoItinerario(), $this->userloginManager->getUser(), $historico);
            }
        }
        return new Response(json_encode(array('respuesta' => 'ok', 'id' => $evento->getId())));
    }
}
