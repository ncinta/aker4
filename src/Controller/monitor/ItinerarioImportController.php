<?php

namespace App\Controller\monitor;

use Exception;
use App\Entity\Poi;
use App\Entity\Itinerario;
use App\Entity\Organizacion;
use App\Form\app\ServicioType;
use App\Model\app\UtilsManager;
use App\Model\app\EquipoManager;
use App\Model\app\EventoManager;
use App\Model\app\UsuarioManager;
use App\Entity\ItinerarioServicio;
use App\Model\app\ContactoManager;
use App\Model\app\ServicioManager;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\TransporteManager;
use App\Model\monitor\EmpresaManager;
use App\Form\app\ReferenciaSelectType;
use App\Model\app\TipoServicioManager;
use App\Model\monitor\SatelitalManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\app\GrupoReferenciaManager;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Form\monitor\import\ReferenciaType;
use App\Model\app\NotificadorAgenteManager;
use App\Form\monitor\import\ServicioNewType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\monitor\BitacoraItinerarioManager;
use App\Form\monitor\import\ItinerarioImportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Model\monitor\ImportRobinsonManager;

class ItinerarioImportController extends AbstractController
{

    private $session;
    private $breadcrumbManager;
    private $servicioManager;
    private $referenciaManager;
    private $grupoReferenciaManager;
    private $itinerarioManager;
    private $bitacoraItinerarioManager;
    private $empresaManager;
    private $userloginManager;
    private $utilsManager;
    private $equipoManager;
    private $satelitalManager;
    private $transporteManager;
    private $eventoManager;
    private $tipoServicioManager;
    private $notificadorAgenteManager;
    private $userManager;
    private $contactoManager;
    private $importRobinsonManager;

    const USR_SYNGENTA = 3363; //id
    const USR_ROBINSON = 5465; //id CH4PL
    const USR_ALARMAS = 3323; //id
    const USR_SADI = 2041; //monitoreosadi

    const CONTACTO_MONITOREO = 965;

    const EMPRESA_MYTMC = 33;
    const EMPRESA_MYTMC_PUERTO = 229;
    const EMPRESA_TEKNAL = 235;

    const LOGISTICA_CHROBINSON = 5;
    const LOGISTICA_P44 = 17;   //VER QUE ID TIENE
    const LOGISTICA_TEKNAL = 19;    //ver id de teknal

    const EVENTO_CHR_E_S = ['id' => 1, 'prefix' =>  'E/S #'];
    const EVENTO_CHR_PAN = ['id' => 2, 'prefix' => 'PAN #'];
    const EVENTO_CHR_MOTIVO = ['id' => 3, 'prefix' => 'MOTIVO #'];

    const CHR_CLIENTES_TIPOS_VALIDOS = [2 => 'chr', 3 => 'p44', 4 => 'teknal'];

    private const IMPORT_CONFIG = [
        'chr' => [
            'logistica_id' => self::LOGISTICA_CHROBINSON,
            'empresa_id' => self::EMPRESA_MYTMC,
            'usuarios' => [
                self::USR_SYNGENTA,
                self::USR_ROBINSON,
                self::USR_ALARMAS,
                self::USR_SADI
            ],
            'eventos' => [
                self::EVENTO_CHR_E_S,
                self::EVENTO_CHR_PAN,
                self::EVENTO_CHR_MOTIVO
            ]
        ],
        'p44' => [
            'logistica_id' => self::LOGISTICA_P44,
            'empresa_id' => self::EMPRESA_MYTMC_PUERTO,
            'usuarios' => [
                self::USR_SYNGENTA,
                self::USR_ROBINSON,
                self::USR_ALARMAS,
                self::USR_SADI
            ],
            'eventos' => [
                self::EVENTO_CHR_E_S,
                self::EVENTO_CHR_PAN,
                self::EVENTO_CHR_MOTIVO
            ]
        ],
        'teknal' => [
            'logistica_id' => self::LOGISTICA_TEKNAL,
            'empresa_id' => self::EMPRESA_TEKNAL,
            'usuarios' => [
                self::USR_ROBINSON,
                self::USR_ALARMAS,
                self::USR_SADI
            ],
            'eventos' => null
        ]
    ];

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        ItinerarioManager $itinerarioManager,
        BitacoraItinerarioManager $bitacoraItinerarioManager,
        UserLoginManager $userloginManager,
        EmpresaManager $empresaManager,
        UtilsManager $utilsManager,
        EquipoManager $equipoManager,
        TransporteManager $transporteManager,
        SatelitalManager $satelitalManager,
        TipoServicioManager $tipoServicioManager,
        EventoManager $eventoManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UsuarioManager $userManager,
        ContactoManager $contactoManager,
        ImportRobinsonManager $importRobinsonManager,
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->bitacoraItinerarioManager = $bitacoraItinerarioManager;
        $this->userloginManager = $userloginManager;
        $this->empresaManager = $empresaManager;
        $this->utilsManager = $utilsManager;
        $this->equipoManager = $equipoManager;
        $this->satelitalManager = $satelitalManager;
        $this->transporteManager = $transporteManager;
        $this->tipoServicioManager = $tipoServicioManager;
        $this->eventoManager = $eventoManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
        $this->userManager = $userManager;
        $this->contactoManager = $contactoManager;
        $this->importRobinsonManager = $importRobinsonManager;
    }
    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }


    /**
     * @Route("/monitor/itinerarioimp/{id}/index", name="itinerario_import_index",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_AGREGAR")
     */
    public function indexAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Importación de Itinerarios");
        $form = $this->createForm(ItinerarioImportType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newFile = sprintf("%s_%s_%s.xls", $this->create_url_slug($organizacion->getNombre()), date('YmdHis'), '');
            $upload = $this->getParameter('kernel.project_dir');
            $uploadDir = $upload . "/web/uploads/import/";
            if ($form['archivo']->getData() === null) {
                $this->setFlash('error', 'No se pudo procesar el archivo');
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
            $form['archivo']->getData()->move($uploadDir, $newFile);
            if (file_exists($uploadDir . '/' . $newFile)) {  //el archivo se subio correctamente.
                $this->session = rand(12345, 65535 * 888);
            }
            $tipoImportacion = $request->get('import')['tipo'];
            if (array_key_exists($tipoImportacion, self::CHR_CLIENTES_TIPOS_VALIDOS)) { //es CH ROBINSON, son los indices del combolist del form

                $data = $this->importRobinsonManager->processFile(
                    $uploadDir . '/' . $newFile,
                    $this->getImportType($tipoImportacion),
                    $organizacion
                );
                //aca tengo errores en el encabezado.
                if (isset($data['isValid']) && !$data['isValid']) {
                    return $this->render('monitor/Itinerario/importacion.html.twig', array(
                        'organizacion' => $organizacion,
                        'form' => $form->createView(),
                        'errors' => $data['errors']
                    ));
                }
                $referencias = $this->referenciaManager->findAsociadas();
                $servicios = $this->servicioManager->findActivos($organizacion);
                $moduloMonitor =  $this->userloginManager->isGranted('ROLE_ITINERARIO_VER');
                $provincias = $this->getEntityManager()->getRepository('App:Provincia')->findAll();

                return $this->render('monitor/Itinerario/chr/pre_import.html.twig', array(
                    'data' => $data,
                    'session' => $this->session,
                    'filename' => $form['archivo']->getData()->getClientOriginalName(),
                    'organizacion' => $organizacion,
                    'formReferencia' => $this->createForm(ReferenciaSelectType::class, null, array('referencias' => $referencias, 'select' => 1, 'provincias' => $provincias))->createView(),
                    'formServicio' => $this->createForm(ServicioType::class, null, array('servicios' => $servicios, 'select' => 1))->createView(),
                    'formServicioNew' => $this->createForm(ServicioNewType::class, null, array('em' => $this->getEntityManager(), 'organizacion' => $organizacion,))->createView(),
                    'formReferenciaNew' => $this->createForm(ReferenciaType::class, null, array('em' => $this->getEntityManager(), 'organizacion' => $organizacion, 'provincias' => $provincias,))->createView(),
                    'iconos' => glob('images/iconos/*.*'),
                    'moduloMonitor' => $moduloMonitor,
                ));
            } else { //es sadi u otro
                $data = $this->procesarSadi($uploadDir . '/' . $newFile, $organizacion);
                return $this->render('monitor/Itinerario/pre_import.html.twig', array(
                    'data' => $data,
                    'session' => $this->session,
                    'filename' => $form['archivo']->getData()->getClientOriginalName(),
                    'organizacion' => $organizacion,
                ));
            }
        }
        return $this->render('monitor/Itinerario/importacion.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView(),
        ));
    }

    private function create_url_slug($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }


    private function procesarSadi($file, $organizacion)
    {
        if (!file_exists($file)) {
            exit("No existe el archivo -> " . $file);
        }
        try { //si el tipo de archivo no es xlsx que caiga en el catch
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $objPHPExcel = $reader->load($file);
        } catch (Exception $e) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            $reader->setReadDataOnly(true);
            $objPHPExcel = $reader->load($file);
        }

        $data = null;
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $i = $row->getRowIndex();
                if ($i > 1) {  //avanzo hasta los datos
                    $cellTurno = $worksheet->getCell('H' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaInicio)
                    $cellTurno = $cellTurno != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp(intval($cellTurno))) : '';

                    $cellLibre = $worksheet->getCell('N' . $i)->getCalculatedValue(); // cell turno (viene sin segundos) (fechaFin)

                    $cellLibre = $cellLibre != '' ? gmdate('d/m/Y H:i:s', Date::excelToTimestamp(intval($cellLibre))) : '';
                    if ($cellLibre < $cellTurno) {   //esto es porque se pone en 00 hs la hora de fin y no machea nunca
                        $cellLibre = $cellTurno;
                    }

                    $data[] = [
                        'cellNombre' => $worksheet->getCell('A' . $i)->getCalculatedValue(), // Nombre + Nro + Contenedor (A + B + F) = codigo itinerario
                        'cellNro' => $worksheet->getCell('B' . $i)->getCalculatedValue(), //---
                        'cellCliente' => $worksheet->getCell('C' . $i)->getCalculatedValue(), //cliente
                        'cellDepOrigen' => $worksheet->getCell('D' . $i)->getCalculatedValue(), //poi 1 (en orden primera)
                        'cellDepDestino' => $worksheet->getCell('E' . $i)->getCalculatedValue(), //poi 2 (en orden seg)
                        'cellContenedor' => $worksheet->getCell('F' . $i)->getCalculatedValue(), //--- 
                        'cellTransporte' => $worksheet->getCell('G' . $i)->getCalculatedValue(),
                        'cellFechaInicio' =>  $cellTurno,
                        'cellChofer' => $worksheet->getCell('I' . $i)->getCalculatedValue(),
                        'cellCelularChofer' => $worksheet->getCell('J' . $i)->getCalculatedValue(),
                        'cellServicio' => $worksheet->getCell('K' . $i)->getCalculatedValue(), //(tractor)
                        'cellPlazaVacios' => $worksheet->getCell('M' . $i)->getCalculatedValue(), //poi3 (en orden tercera)
                        'cellFechaFin' =>  $cellLibre,
                        'cellBL' => $worksheet->getCell('O' . $i)->getCalculatedValue(),
                    ];
                }
            }
        }
        unset($objPHPExcel);  //esto no lo voy a usar mas.
        //aca ya he leido el archivo.       
        return $this->mostrarDatos($data, $organizacion);
    }


    private function mostrarDatos($data, $organizacion)
    {
        $importado = null;
        if (!is_null($data)) {
            $em = $this->getDoctrine()->getManager();
            foreach ($data as $row) {

                $codigo = $this->create_url_slug($row['cellNombre'] . ' ' . $row['cellNro'] . ' ' . $row['cellContenedor']);

                //busco un itinerario igual existente.
                $itinerario = $em->getRepository('App:Itinerario')->findOneBy(array(
                    'codigo' => $codigo
                ));

                if ($itinerario === null) {
                    $fechaInicio = null;
                    $fechaFin = null;
                    $empresa = $this->empresaManager->findByNombre($row['cellCliente']);

                    $fechaInicio = $row['cellFechaInicio'];
                    $fechaFin = $row['cellFechaFin'];
                    $nota  = "Chofer: " . $row['cellChofer'] . ', Celular: ' . $row['cellCelularChofer'] . ', BL: ' . $row['cellBL'];

                    //busco el servicio.
                    $patente = str_replace(['-', ' '], '', $row['cellServicio']);
                    $servicio = $this->servicioManager->findByPatente($patente);

                    $ref1 =  $this->referenciaManager->findByNombre($row['cellDepOrigen']);
                    $ref2 =  $this->referenciaManager->findByNombre($row['cellDepDestino']);
                    $ref3 =  $this->referenciaManager->findByNombre($row['cellPlazaVacios']);
                    $referencias[1] = $ref1 !== null ? $ref1->getNombre() : null;
                    $referencias[2] = $ref2 !== null ? $ref2->getNombre() : null;
                    $referencias[3] = $ref3 !== null ? $ref3->getNombre() : null;
                    $idsRef =  array($ref1 ? $ref1->getId() : null, $ref2 ? $ref2->getId() : null, $ref3 ? $ref3->getId() : null); //lo uso para el ajax

                    $transporte = $servicio && $servicio->getTransporte() ? $servicio->getTransporte() : null;
                    //die('////<pre>' . nl2br(var_export($itinerario->getPois()[0]->getReferencia()->getId(), true)) . '</pre>////');
                    $importado[] = array(
                        'codigo' => $codigo,
                        'empresa' => $empresa !== null ? array('id' => $empresa->getId(), 'nombre' => $empresa->getNombre()) : null,
                        'servicio' =>  $servicio !== null ? array('id' => $servicio->getId(), 'nombre' => $servicio->getNombre()) : null,
                        'transporte' =>  $transporte !== null ? array('id' => $transporte->getId(), 'nombre' => $transporte->getNombre()) : null,
                        'referencias' =>  $referencias,
                        'idsRef' => $idsRef,
                        'nota' =>  $nota,
                        'fechaInicio' =>  $fechaInicio,
                        'fechaFin' =>  $fechaFin,
                    );
                    unset($itineriario);
                }
                unset($servicio);
            }
        }
        return $importado;
    }

    /**
     * @Route("/monitor/itinerarioimp/procesar", name="itinerario_import_procesar",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function procesarAction(Request $request)
    {
        $arr = array(
            'codigo' => $request->get('codigo'),
            'empresa' => $request->get('empresa'),
            'nota' => $request->get('nota'),
            'servicio' => $servicio = $this->servicioManager->find((int)$request->get('servicio')),
            'fecha_inicio' => $request->get('fecha_inicio'),
            'fecha_fin' => $request->get('fecha_fin'),
            'referencias' => json_decode($request->get('referencias'), true),
        );
        $response = $this->createItinerario($arr);

        // $notificar = $this->notificarEventos($itinerario);
        return new Response(json_encode($response));
    }


    private function createItinerario($arr, $itinerario = null)
    {

        $em = $this->getDoctrine()->getManager();
        $return = array('status' => false, 'codigo' => $arr['codigo']);

        $organizacion = $this->userloginManager->getOrganizacion();
        $update = true; //es una actualizacion de itinerario (chr)
        if ($itinerario == null) {
            $itinerario = $this->itinerarioManager->create($organizacion);
            $update = false;
        }

        $itinerario->setCodigo($arr['codigo']);
        $itinerario->setNota($arr['nota']);

        $fechaInicio = $arr['fecha_inicio'] !== '' ?
            new \DateTime(
                $this->utilsManager->datetime2sqltimestamp($arr['fecha_inicio'], false),
                new \DateTimeZone($itinerario->getOrganizacion()->getTimezone())
            ) : null;
        $fechaFin =  $arr['fecha_fin'] !== '' ?
            new \DateTime(
                $this->utilsManager->datetime2sqltimestamp($arr['fecha_fin'], false),
                new \DateTimeZone($itinerario->getOrganizacion()->getTimezone())
            ) : null;
        if (($fechaFin && $fechaInicio) && ($fechaFin < $fechaInicio)) {
            return $return;
        }

        $itinerario->setFechaInicio($fechaInicio);
        $itinerario->setFechaFin($fechaFin);
        // $itinerario->setNota('<p>Chofer: <b>' . isset($arr['chofer']) ? $arr['chofer'] : '---' . '</b></p>');
        $itinerario->setModoIngreso(1); //1 = EXCEL
        if (isset($arr['referencia_externa'])) {
            $itinerario->setReferencia_externa($arr['referencia_externa']);
        }
        if (isset($arr['cliente_externo'])) {
            $itinerario->setCliente_externo($arr['cliente_externo']);
        }
        try {
            $itinerario = $this->itinerarioManager->save($itinerario);
        } catch (Exception $e) {
            return $return;
        }

        $config = $this->getImportConfiguration($arr['tipo']);
        $logistica = $config['logistica'];
        $empresa = $config['empresa'];
        $contacto = $config['contacto'];
        $arrUsr = $config['usuarios'];

        $itinerario->setLogistica($logistica);
        $itinerario->setEmpresa($empresa);
        //agrego los usuarios por default
        foreach ($arrUsr as $usr) {
            if ($usr) {
                if (!is_null($usr)) {
                    $addUser = $this->itinerarioManager->addUsuario($itinerario, $usr);
                }
            }
        }
        //agrego el contacto por default
        if ($contacto) {
            $x = $this->itinerarioManager->addContacto($itinerario, $contacto);
        }

        $servicio = $arr['servicio'];

        $itinerario = $this->addPois($organizacion, $itinerario, $arr);

        if ($update && ($arr['tipo'] == 'chr' || $arr['tipo'] == 'p44')) { //si se esta actualizando el itinerario y no encuentra el servicio puede que sea otro (esto solo para CHR)
            $itinerario = $this->removePois($itinerario, $arr);
        }
        if ($servicio) {
            $serv = $this->addServicio($itinerario, $servicio, $update, $arr);
        } else { //si no encuentra el servicio por la patente, que elimine al anterior y que no ponga nada
            foreach ($itinerario->getServicios() as $itS) {
                $em->remove($itS);
            }
        }

        // Configuración de eventos según IMPORT_CONFIG
        $config = self::IMPORT_CONFIG[$arr['tipo']] ?? null;
        if ($config && !empty($config['eventos'])) {
            foreach ($config['eventos'] as $evento) {
                if ($evento === self::EVENTO_CHR_E_S) {
                    $this->createEventoIO($itinerario);
                }
                if ($evento === self::EVENTO_CHR_PAN) {
                    $this->createEventoPanico($itinerario);
                }
                if ($evento === self::EVENTO_CHR_MOTIVO) {
                    $this->createEventoMotivo($itinerario);
                }
            }
        }

        $return['status'] = true;
        $return['id'] = $itinerario->getId();

        return $return;
    }

    private function addServicio($itinerario, $servicio, $update, $arr)
    {
        $em = $this->getEntityManager();
        $itiServ = $em->getRepository('App:ItinerarioServicio')->findOneBy(array('itinerario' => $itinerario->getId(), 'servicio' => $servicio->getId()));
        if ($itiServ == null) { //no encuentra el servicio, puede que no tenga o tenga otro
            if ($update && in_array($arr['tipo'], self::CHR_CLIENTES_TIPOS_VALIDOS)) { //si se esta actualizando el itinerario y no encuentra el servicio puede que sea otro (esto solo para CHR)
                foreach ($itinerario->getServicios() as $itS) { // lo eliminamos porque chr solamente usa 1 solo y lo quiere actualizar
                    $em->remove($itS);
                }
                $em->flush();
            }
            $itiServ = new ItinerarioServicio(); //creamos el nuevo servicio
        }
        $itiServ->setItinerario($itinerario);
        $itiServ->setServicio($servicio);
        $itiServ->setPatenteAcoplado($arr['acoplado']);
        $em->persist($itiServ);
        $em->flush();
    }


    private function addPois($organizacion, $itinerario, $arr)
    {
        $em = $this->getEntityManager();
        $grupoRef = $itinerario->getGrupoReferencia();
        $data = null;
        $referencia = null;
        $eta = null;
        $stopType = null;
        $state = null;
        if (!$grupoRef) {
            $grupoRef = $this->grupoReferenciaManager->create($itinerario->getOrganizacion()); //le creo un grupo temporal hasta que se cierre o elimine
            $grupoRef->setNombre("ITINERARIO-" . $itinerario->getCodigo()); // pongo nombre con codigo tmp para poder buscarlo y eliminarlo despues
            foreach ($itinerario->getPois() as $poi) {
                $grupoRef->addReferencia($poi->getReferencia());
            }
            $grupoRef = $this->grupoReferenciaManager->save($grupoRef);
            $itinerario->setGrupoReferencia($grupoRef);
            $itinerario = $this->itinerarioManager->save($itinerario);
        }
        foreach ($arr['referencias'] as $key => $arr) {
            if ($arr) {
                $data = $arr;
                if (isset($arr['tipo'])) {
                    if (in_array($arr['tipo'], self::CHR_CLIENTES_TIPOS_VALIDOS)) {
                        $referencia = $this->referenciaManager->findByCodigoExterno($arr['codigo_externo']);
                        $state = $arr['state'];
                        $eta = $arr['eta'];
                        $stopType = $arr['stopType'];
                    } else {
                        $referencia = $this->referenciaManager->find($data);
                    }
                } else {
                    $referencia = $this->referenciaManager->find($data);
                }
            }

            if ($referencia) {
                $referencia->setState($state);
                $em->persist($referencia);
                $poi = $this->itinerarioManager->addPoi($itinerario, $referencia, $key, $eta, $stopType);
                $grupoRef->addReferencia($referencia);
                $em->persist($grupoRef);
            }
        }
        $em->flush();

        return $itinerario;
    }

    private function removePois($itinerario, $arr)
    {
        $em = $this->getEntityManager();
        $grupoRef = $itinerario->getGrupoReferencia();
        foreach ($itinerario->getPois() as $poi) {
            $existe = false;
            $codigo = $poi->getReferencia()->getCodigoExterno();
            foreach ($arr['referencias'] as $key => $data) {
                if (in_array($arr['tipo'], self::CHR_CLIENTES_TIPOS_VALIDOS)) {
                    if ($data) {
                        $referencia = $this->referenciaManager->findByCodigoExterno($data['codigo_externo']);
                        if ($referencia) {
                            $poiVerifica = $em->getRepository('App:Poi')->findOneBy(array('itinerario' => $itinerario->getId(), 'referencia' => $referencia->getId()));
                            if ($poiVerifica != null && $codigo == $referencia->getCodigoExterno()) {
                                $existe = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!$existe) { // en el foreach anterior buscamos la referencia por el codigo que viene del import si no existe la borramos, porque la sacaron
                if ($grupoRef) {
                    $remove = $grupoRef->removeReferencia($poi->getReferencia());
                    $em->persist($grupoRef);
                }
                $em->remove($poi);
            }
        }
        $em->flush();

        return $itinerario;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/monitor/itinerarioimp/updatereferencia", name="itinerario_import_update_referencia",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function updatereferenciaAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formReferencia'), $tmp);
        // die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        $id = $tmp['referencia']['referencia'];
        $ciudad = isset($tmp['referencia']) &&  isset($tmp['referencia']['ciudad']) ? $tmp['referencia']['ciudad'] : null;
        $provincia = isset($tmp['referencia']) &&  isset($tmp['referencia']['provincia']) ? $tmp['referencia']['provincia'] : null;
        $codigo_externo = $request->get('codigo_externo');
        $referencia = $this->referenciaManager->updateCodigoExterno($id, $codigo_externo, $ciudad, $provincia);

        return new Response(json_encode(array('status' => $referencia ? true : false)));
    }

    /**
     * @Route("/monitor/itinerarioimp/checkprovincia", name="itinerario_import_check_provincia",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function checkprovinciaAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formReferencia'), $tmp);
        $id = $tmp['referencia']['referencia'];
        $codigo_externo = $request->get('codigo_externo');
        $referencia = $this->referenciaManager->find($id);
        $return = array(
            'ciudad' => $referencia->getCiudad() ? true : false,
            'provincia' => $referencia->getProvincia() ? true : false
        );

        return new Response(json_encode(array('check' => $return)));
    }

    /**
     * @Route("/monitor/itinerarioimp/updateservicio", name="itinerario_import_update_servicio",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function updateservicioAction(Request $request)
    {
        $tmp = array();
        parse_str($request->get('formServicio'), $tmp);
        $id = $tmp['servicio']['servicio'];
        $patente = $request->get('patente');
        $servicio = null;
        if ($patente != '' && $patente != null) { //verificamos que venga la patente, para seguir el procedimiento
            $servicio = $this->servicioManager->updatePatente($id, $patente);
        }

        return new Response(json_encode(array('status' => $servicio ? true : false)));
    }


    /**
     * @Route("/monitor/itinerarioimp/createservicio", name="itinerario_import_create_servicio",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function createservicioAction(Request $request)
    {
        $em = $this->getEntityManager();
        $organizacion = $this->userloginManager->getOrganizacion();
        $tmp = array();
        parse_str($request->get('formServicio'), $tmp);
        $data = $tmp['servicio'];
        $modelo = $em->getRepository('App:Modelo')->findOneBy(array('id' => 149)); //buscamos el modelo VIRTUAL

        $equipo = $this->equipoManager->create($organizacion);
        $equipo->setMdmid(trim($data['patente']));
        $equipo->setEstado(3);
        $equipo->setModelo($modelo);
        try {
            $equipo = $this->equipoManager->save($equipo);
        } catch (UniqueConstraintViolationException $e) {
            return new Response(json_encode(array('status' =>  false)));
        }

        $transporte = $this->transporteManager->find((int)$data['transporte']);
        $satelital = $this->satelitalManager->find((int)$data['satelital']);
        $tipoServicio = $this->tipoServicioManager->findById(5); //por default es un camión
        $servicio = $this->servicioManager->createFromImportIti($equipo, $organizacion, $tipoServicio, $transporte, $satelital, $data);

        return new Response(json_encode(array('status' => $servicio ? true : false)));
    }

    /**
     * @Route("/monitor/itinerarioimp/createreferencia", name="itinerario_import_create_referencia",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function createreferenciaAction(Request $request)
    {
        $em = $this->getEntityManager();
        $organizacion = $this->userloginManager->getOrganizacion();
        $tmp = array();
        parse_str($request->get('formreferencia'), $tmp);
        $data = $tmp['referencia'];
        $referencia = $this->referenciaManager->findByCodigoExterno(trim($data['codigoExterno']));
        if ($referencia == null) {
            $referencia = $this->referenciaManager->create($organizacion);
            $referencia->setNombre($data['nombre']);
            $referencia->setCodigoExterno($data['codigoExterno']);
            $referencia->setLatitud($data['latitud']);
            $referencia->setLongitud($data['longitud']);
            $referencia->setDireccion($data['direccion']);
            $referencia->setRadio($data['radio']);
            // $referencia->setDescripcion($data['descripcion']);
            $referencia->setPathIcono($data['pathIcono']);
            $referencia->setCiudad($data['ref_ciudad']);

            $provincia = $em->getRepository("App:Provincia")->findOneBy(array("code" => $data['ref_provincia']));
            $referencia->setProvincia($provincia);

            $referencia = $this->referenciaManager->save($referencia);
        } else { //no existe, seguimos con el proceso
            return new Response(json_encode(array('status' =>  false)));
        }
        return new Response(json_encode(array('status' => $referencia ? true : false)));
    }

    /**
     * @Route("/monitor/itinerarioimp/chr/procesar", name="itinerario_import_chr_procesar",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function procesarchrAction(Request $request)
    {
        $arr = array();
        $nombre = $request->get("codigo");
        $em = $this->getEntityManager();
        $template = $em->getRepository('App:TemplateItinerario')->findOneBy(array('nombre' => $nombre));
        $delete = $this->deleteTemplate($template); // //lo borro para no tener problemas con duplicidad.
        $datos = json_decode($template->getData(), true);

        $itinerario = $this->itinerarioManager->findByCodigo($nombre);
        $update = $itinerario != null ? true : false;

        $servicio = $this->servicioManager->findByPatente($datos['cellServicio']);
        $arr = array(
            'codigo' => $nombre,
            'empresa' => null,
            'nota' => null,
            'tipo' => $datos['tipo'],
            'servicio' => $servicio,
            'acoplado' => $datos['cellAcoplado'],
            'chofer' => $datos['cellChofer'],
            'fecha_inicio' => $datos['cellFechaInicio'],
            'fecha_fin' => $datos['cellFechaFin'],
            'referencia_externa' => isset($datos['cellReferencia']) ? $datos['cellReferencia'] : null,
            'cliente_externo' => isset($datos['cellCustomerId']) ? $datos['cellCustomerId'] : null
        );
        $arr['referencias'] = array();
        foreach ($datos['pois'] as $poi) {

            //   die('////<pre>' . nl2br(var_export($poi['cellPoiOrden'], true)) . '</pre>////');
            $arr['referencias'][$poi['cellPoiOrden']] = array(
                'tipo' => $datos['tipo'],
                'codigo_externo' => $poi['cellPoiCode'],
                'eta' => $poi['cellPoiEta'],
                'state' => $poi['cellPoiState'],
                'stopType' => $poi['cellPoiStopType']
            );
        }
        $response = $this->createItinerario($arr, $itinerario);
        //  dd($response);
        // $notificar = $this->notificarEventos($itinerario);
        return new Response(json_encode($response));
    }

    /**
     * @Route("/monitor/itinerarioimp/chr/terminar", name="itinerario_import_chr_terminar",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function terminarAction(Request $request)
    {
        $em = $this->getEntityManager();
        $organizacion = $this->userloginManager->getOrganizacion();
        //busco todos los templates que sean de import chr
        $templates = $em->getRepository('App:TemplateItinerario')->findBy(array('organizacion' => $organizacion->getId(), 'bloqueado' => 1));
        foreach ($templates as $template) {
            $delete = $this->deleteTemplate($template); //borro todo por las dudas de alguno que haya quedado si procesar 
        }
        return new Response(json_encode(array('status' => true, 'id' => $organizacion->getId())));
    }

    private function deleteTemplate($template)
    {

        $em = $this->getEntityManager();
        $em->remove($template);
        $em->flush();

        return true;
    }


    private function createEventoIO($itinerario)
    {
        $em = $this->getEntityManager();
        $nombreEvIO = self::EVENTO_CHR_E_S['prefix'] . $itinerario->getCodigo();
        $eventoIO = null;
        $grupo = $itinerario->getGrupoReferencia();

        //crear evento de e/s 
        $eventoIO = $this->eventoManager->setForItinerario($nombreEvIO, 'EV_IO_REFERENCIA', $itinerario->getOrganizacion());
        $param = $this->eventoManager->addParam($eventoIO, 'VAR_GRUPO_REFERENCIA_IN', $grupo->getId());
        $evItinerario = $this->eventoManager->setEventoItinerario($eventoIO, $itinerario, 'EV_IO_REFERENCIA');

        //    $notificarGrupo = $this->notificadorAgenteManager->notificar($grupo, 1);
        //      $notificar = $this->notificadorAgenteManager->notificar($eventoIO, 1);
        $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $itinerario, $eventoIO, 'Activo');
    }

    private function createEventoPanico($itinerario)
    {
        $em = $this->getEntityManager();
        //crear evento de e/s 
        $nombreEvPanic = self::EVENTO_CHR_PAN['prefix'] . $itinerario->getCodigo();


        $eventoPanico = $this->eventoManager->setForItinerario($nombreEvPanic, 'EV_PANICO',  $itinerario->getOrganizacion());
        $evItinerario = $this->eventoManager->setEventoItinerario($eventoPanico, $itinerario, 'EV_PANICO');

        $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $itinerario, $eventoPanico, 'Activo');
    }

    private function addServicioEvento($itinerario, $codename)
    {
        $em = $this->getEntityManager();
        //crear evento de e/s 
        $nombreEv = $codename . $itinerario->getCodigo();
        $evento = null;
        $eventos = $this->eventoManager->findAllByItinerario($itinerario);
        //dd($eventos);
        foreach ($eventos as $ev) {
            if ($ev->getEvento()->getNombre() == $nombreEv) {
                $evento = $ev->getEvento();
            }
        }

        foreach ($itinerario->getServicios() as $itiServicio) {
            if ($evento != null) {
                $evento->addServicio($itiServicio->getServicio());
                $em->persist($evento);
                $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $itinerario, $evento, 'Activo');
                //  $notificar = $this->notificadorAgenteManager->notificar($evento, 1);
            }
        }
        $em->flush();
    }

    private function createEventoMotivo($itinerario)
    {
        $em = $this->getEntityManager();
        //crear evento de e/s 
        $nombreEvMotivo = self::EVENTO_CHR_MOTIVO['prefix'] . $itinerario->getCodigo();
        $eventoMotivo = null;

        $eventoMotivo = $this->eventoManager->setForItinerario($nombreEvMotivo, 'EV_MOTIVOTX',  $itinerario->getOrganizacion());
        //agrego los parametros a controlar
        $motivos = [
            'VAR_PUERTACHOFER',
            'VAR_PUERTACABINA',
            'VAR_PUERTATRASERA',
            'VAR_ANTIVANDALICO',
            'VAR_REDUCCIONVELOCIDAD',
            'VAR_ENCENDIDO',
            'VAR_APAGADO',
            'VAR_CORTECOMBUSTIBLE',
            'VAR_ENGANCHE',
            'VAR_DESENGANCHE',
            'VAR_FRENADA',
            'VAR_ACELERACION'
        ];
        foreach ($motivos as $motivo) {
            $param = $this->eventoManager->addParam($eventoMotivo, $motivo, 1);
        }
        $evItinerario = $this->eventoManager->setEventoItinerario($eventoMotivo, $itinerario, 'EV_MOTIVOTX');

        // $notificar = $this->notificadorAgenteManager->notificar($eventoMotivo, 1);
        if ($eventoMotivo) {
            $bitacora = $this->bitacoraItinerarioManager->updateEvento($this->userloginManager->getUser(), $itinerario, $eventoMotivo, 'Activo');
        }
    }



    /**
     * @Route("/monitor/itinerarioimp/cargareventos", name="itinerario_import_cargareventos",  options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_EDITAR")
     */
    public function cargareventosAction(Request $request)
    {
        $id = intval($request->get('id'));
        $itinerario = $this->itinerarioManager->find($id);

        // Buscar la configuración que corresponde a la logística del itinerario
        $logisticaId = $itinerario->getLogistica()->getId();
        $clienteId = $itinerario->getEmpresa()->getId();
        $config = null;


        foreach (self::IMPORT_CONFIG as $tipoCliente => $importConfig) {
            if (
                $importConfig['logistica_id'] === $logisticaId &&
                $importConfig['empresa_id'] === $clienteId
            ) {
                $config = $importConfig;
                break;
            }
        }

        if ($config) {
            // Si encontramos configuración y tiene eventos definidos, los agregamos
            if ($config && $config['eventos'] !== null) {
                foreach ($config['eventos']['prefix'] as $eventoPrefix) {
                    $this->addServicioEvento($itinerario, $eventoPrefix);
                }
            }

            /**  
       if ($itinerario->getMonitoreo()->getId() !== self::LOGISTICA_TEKNAL) {
            $evIO = $this->addServicioEvento($itinerario, 'E/S #');
            $eventoPanico = $this->addServicioEvento($itinerario, 'PAN #');
            $eventoMotivo = $this->addServicioEvento($itinerario, 'Motivo #');
        }
             */
        }
        return new Response(json_encode(array('status' => true, 'id' => $itinerario->getId(), 'codigo' => $itinerario->getCodigo())));
    }

    private function getImportType($tipoImportacion)
    {
        switch ($tipoImportacion) {
            case '2':
                return ImportRobinsonManager::IMPORT_TYPE_CHR;
            case '3':
                return ImportRobinsonManager::IMPORT_TYPE_P44;
            case '4':
                return ImportRobinsonManager::IMPORT_TYPE_TEKNAL;
            default:
                throw new Exception('Tipo de importación no válido');
        }
    }

    private function getImportConfiguration($tipo)
    {
        if (!isset(self::IMPORT_CONFIG[$tipo])) {
            throw new \Exception('Tipo de importación no válido');
        }

        $config = self::IMPORT_CONFIG[$tipo];
        $em = $this->getEntityManager();

        $logistica = $em->getRepository('App:Logistica')->findOneBy(['id' => $config['logistica_id']]);
        $empresa = $config['empresa_id'] ? $this->empresaManager->find($config['empresa_id']) : null;
        $contacto = $this->contactoManager->find(self::CONTACTO_MONITOREO);

        // Siempre incluir el usuario actual
        $arrUsr = [$this->userloginManager->getUser()];

        // Agregar usuarios configurados
        foreach ($config['usuarios'] as $userId) {
            $user = $this->userManager->find($userId);
            if ($user) {
                $arrUsr[] = $user;
            }
        }

        return [
            'logistica' => $logistica,
            'empresa' => $empresa,
            'contacto' => $contacto,
            'usuarios' => $arrUsr
        ];
    }
}
