<?php

namespace App\Controller\monitor;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\monitor\ItinerarioType;
use App\Entity\Organizacion;
use App\Entity\Itinerario;
use App\Form\monitor\HistoricoServicioType;
use App\Model\monitor\EmpresaManager;
use App\Model\monitor\ItinerarioManager;
use App\Model\monitor\TransporteManager;
use App\Model\monitor\SatelitalManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\TipoServicioManager;
use App\Model\app\UserLoginManager;
use App\Model\app\Router\ItinerarioRouter;
use App\Model\app\ReferenciaManager;
use App\Model\app\EventoHistorialManager;
use App\Model\app\GeocoderManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of ServicioController
 *
 * @author nicolas
 */
class ServicioController extends AbstractController
{

    private $breadcrumbManager;
    private $itinerarioManager;
    private $servicioManager;
    private $empresaManager;
    private $transporteManager;
    private $itinerarioRouter;
    private $satelitalManager;
    private $userloginManager;
    private $tiposervicioManager;
    protected $geocoder;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ItinerarioManager $itinerarioManager,
        ServicioManager $servicioManager,
        EmpresaManager $empresaManager,
        TransporteManager $transporteManager,
        ItinerarioRouter $itinerarioRouter,
        SatelitalManager $satelitalManager,
        UserLoginManager $userloginManager,
        GeocoderManager $geocoder,
        TipoServicioManager $tiposervicioManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->itinerarioManager = $itinerarioManager;
        $this->servicioManager = $servicioManager;
        $this->empresaManager = $empresaManager;
        $this->itinerarioRouter = $itinerarioRouter;
        $this->satelitalManager = $satelitalManager;
        $this->userloginManager = $userloginManager;
        $this->transporteManager = $transporteManager;
        $this->tiposervicioManager = $tiposervicioManager;
        $this->geocoder = $geocoder;
    }

    /**
     * @Route("/monitor/servicio/{idOrg}/status", name="servicio_status")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Itinerarios");
        $arrIds = array();
        $menu = array();
        $reportando = array();
        $retrasados = array();
        $sin_reportar = array();

        $options = array(
            'empresas' => $this->empresaManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'satelitales' => $this->satelitalManager->findAllByOrganizacion($organizacion),
            'tipoServicio' => $this->tiposervicioManager->findAll($organizacion),
            'itinerario' => $this->itinerarioManager->findAllByOrganizacion($organizacion),
            'servicio' => $this->servicioManager->findAllByUsuario($this->userloginManager->getUser()),
        );

        $form = $this->createForm(HistoricoServicioType::class, null, $options);
        $usuario = $this->userloginManager->getUser();
        $serv = $this->servicioManager->historico(
            $usuario,
            null,
            null,
            null,
            null,
            null,
            null,
            0
        ); //buscar solo abiertos

        foreach ($serv as $servicio) {
            $arrIds[] = $servicio->getId();
        }

        $servicios = $this->servicioManager->checkStatusServicioMonitor($serv)['status'];

        foreach ($servicios as $servicio) {
            //    die('////<pre>' . nl2br(var_export($servicio['estado'], true)) . '</pre>////');
            if ($servicio['estado'] == 10) {
                $reportando[] = $servicio['servicio'];
            } elseif ($servicio['estado'] == 11) {
                $retrasados[] = $servicio['servicio'];
            } else {
                $sin_reportar[] = $servicio['servicio'];
            }
        }

        return $this->render('monitor/Servicio/status.html.twig', array(
            'menu' => $menu,
            'reportando' => $reportando,
            'retrasados' => $retrasados,
            'sin_reportar' => $sin_reportar,
            'arrIds' => json_encode($arrIds, true),
            'form' => $form->createView(),
            'filtro' => 'Filtro: default'
        ));
    }

    /**
     * @Route("/monitor/servicio/historico", name="servicio_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function historicoAction(Request $request)
    {

        $tmp = array();
        $consulta = array();
        $arrIds = array();
        $reportando = array();
        $retrasados = array();
        $sin_reportar = array();

        parse_str($request->get('formHistServicio'), $tmp);
        $form = $tmp['historico'];
        $organizacion = $this->userloginManager->getOrganizacion();
        if ($form) {
            $tipoServicio = -2;
            if ($form['tipoServicio'] == -1) {

                $tipoServicio = -1;
            } else {
                if (($form['tipoServicio']) > 0) {
                    $tipoServicio = $this->tiposervicioManager->findById(intval($form['tipoServicio']));
                } else {
                    if ($form['tipoServicio'] == -2) {
                        $tipoServicio = -2;
                    }
                }
                $transporte = isset($form['transporte']) && ($form['transporte']) > 0 ? $this->transporteManager->find($form['transporte']) : null;
                $satelital = (isset($form['satelital']) && $form['satelital']) > 0 ? $this->satelitalManager->find($form['satelital']) : null;
                $empresa = isset($form['empresa']) && ($form['empresa']) > 0 ? $this->empresaManager->find($form['empresa']) : null;
                $itinerario = isset($form['itinerario']) && isset($form['itinerario']) ? $form['itinerario'] : null;
                $servFiltro = isset($form['servicio']) ? $form['servicio'] : null;
                $estado = isset($form['estado']) && isset($form['estado']) ? $form['estado'] : null;
            }

            $usuario = $this->userloginManager->getUser();

            $serv = $this->servicioManager->historico(
                $usuario,
                $tipoServicio,
                $transporte,
                $satelital,
                $empresa,
                $itinerario,
                $servFiltro,
                $estado
            );

            $servicios = $this->servicioManager->checkStatusServicioMonitor($serv)['status'];

            foreach ($serv as $servicio) {
                $arrIds[] = $servicio->getId();
            }

            foreach ($servicios as $servicio) {
                if ($servicio['estado'] == 10) {
                    $reportando[] = $servicio['servicio'];
                } elseif ($servicio['estado'] == 11) {
                    $retrasados[] = $servicio['servicio'];
                } else {
                    $sin_reportar[] = $servicio['servicio'];
                }
            }
        }

        return new Response(json_encode(array(
            'html_sin_reportar' => $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $sin_reportar,  'arrIds' => json_encode($arrIds, true),
            )), 'html_reportando' => $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $reportando,  'arrIds' => json_encode($arrIds, true),
            )), 'html_retrasados' => $this->renderView('monitor/Servicio/servicios.html.twig', array(
                'servicios' => $retrasados,  'arrIds' => json_encode($arrIds, true),
            )),
            'count_retrasados' => count($retrasados),
            'count_reportando' => count($reportando),
            'count_sin_reportar' => count($sin_reportar),
            'arrIds' => json_encode($arrIds, true),
        )));
    }

    /**
     * @Route("/monitor/servicio/historico/datamap", name="servicio_historico_datamap")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ITINERARIO_VER")
     */
    public function datamapAction(Request $request)
    {
        $arrIds = is_array($request->get('arrIds')) ? $request->get('arrIds') : json_decode($request->get('arrIds'));
        $arrData = array();
        $organizacion = null;
        foreach ($arrIds as $id) {
            $servicio = $this->servicioManager->find($id);
            if (is_null($organizacion)) {
                $organizacion = $servicio->getOrganizacion();
            }
            $estado = $this->servicioManager->procesarStatusServicioByMonitor($servicio, null, true, false);
            $arrData[$servicio->getId()] = array(
                'id' => $servicio->getId(),
                'icono' => $estado['icono'],
                'ultVelocidad' => is_null($servicio->getUltVelocidad()) ? '---' : $servicio->getUltVelocidad(),
                'direccion' => $estado['direccion'],
                'contacto' => $estado['contacto'],
                'leyenda' => isset($estado['cerca']) ? $estado['cerca']['leyenda'] : null,
                'iconoRef' => isset($estado['cerca']['icono']) ? $estado['cerca']['icono'] : null,
                'posicion' => ['latitud' => $servicio->getUltLatitud(), 'longitud' => $servicio->getUltLongitud()],
            );          
        }

        /** data = ['direccion' => 'Av San Martin', 'posicion' => ['latitud' => -32.123, 'longitud' => -64.123]] */
        $arrData = $this->geocoder->inversoBatch($arrData, $organizacion);
      
        return new Response(json_encode(array(
            'arrData' => $arrData
        )));
    }
}
