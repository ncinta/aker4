<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\mante\MecanicoType;
use App\Form\mante\InformeMecanicoType;
use App\Entity\Organizacion;
use App\Entity\Mecanico;
use App\Model\app\MecanicoManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\Router\MecanicoRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MecanicoController extends AbstractController
{

    private $mecanicoManager;
    private $mecanicoRouter;
    private $breadcrumbManager;
    private $organizacionManager;
    private $userloginManager;
    private $ordentrabajoManager;
    private $utilsManager;
    private $excelManager;

    function __construct(
        MecanicoManager $mecanicoManager,
        MecanicoRouter $mecanicoRouter,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        UserLoginManager $userloginManager,
        OrdenTrabajoManager $ordentrabajoManager,
        UtilsManager $utilsManager,
        ExcelManager $excelManager
    ) {
        $this->mecanicoManager = $mecanicoManager;
        $this->mecanicoRouter = $mecanicoRouter;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->userloginManager = $userloginManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->utilsManager = $utilsManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/mante/mecanico/{idOrg}/list", name="mecanico_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_MECANICO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {
        $mecanicos = $this->mecanicoManager->findAll($organizacion);
        $menu = array();
        $mecanico = $this->mecanicoManager->create($organizacion);
        $form = $this->createForm(MecanicoType::class, $mecanico);
        //agrega un chip en una distribucion
        $menu = array(
            1 => array($this->mecanicoRouter->btnNew($organizacion)),
            2 => array($this->mecanicoRouter->btnInforme($organizacion))
        );
        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de Mecanicos");
        return $this->render('mante/Mecanico/list.html.twig', array(
            'menu' => $this->mecanicoRouter->toCalypso($menu),
            'form' => $form->createView(),
            'mecanicos' => $mecanicos,
            'organizacin' => $organizacion,
        ));
    }

    /**
     * @Route("/mante/mecanico/{id}/show", name="mecanico_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_VER")
     */
    public function showAction(Request $request, Mecanico $mecanico)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), $mecanico->getNombre());
        $deleteForm = $this->createDeleteForm($mecanico->getId());
        return $this->render('mante/Mecanico/show.html.twig', array(
            'menu' => $this->getShowMenu($mecanico),
            'mecanico' => $mecanico,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $mecanico 
     * @return array $menu
     */
    private function getShowMenu($mecanico)
    {
        $menu = array(
            1 => array($this->mecanicoRouter->btnEdit($mecanico)),
            2 => array($this->mecanicoRouter->btnDelete($mecanico))
        );
        return $this->mecanicoRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/mecanico/{idOrg}/new", name="mecanico_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_MANT_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {

        $mecanico = $this->mecanicoManager->create($organizacion);
        $form = $this->createForm(MecanicoType::class, $mecanico);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->mecanicoManager->save($mecanico)) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado el mecanico <b>%s</b> en el sistema.', strtoupper($mecanico))
                );
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para crear el mecanico "%s" en el sistema. Es posible que el Id ya exista.', strtoupper($mecanico)));
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Mecanico");
        return $this->render('mante/Mecanico/new.html.twig', array(
            'organizacion' => $organizacion,
            'mecanico' => $mecanico,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/mante/mecanico/{id}/edit", name="mecanico_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_EDITAR")
     */
    public function editAction(Request $request, Mecanico $mecanico)
    {

        $form = $this->createForm(MecanicoType::class, $mecanico);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->breadcrumbManager->pop();
                if ($this->mecanicoManager->save($mecanico)) {
                    $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($mecanico)));
                    return $this->redirect($this->breadcrumbManager->getVolver());
                } else {
                    $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas en la grabación del mecanico <b>%s</b>. Es posible que el Id ya exista.', strtoupper($mecanico)));
                }
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Mecanico");
        return $this->render('mante/Mecanico/edit.html.twig', array(
            'mecanico' => $mecanico,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/mecanico/{id}/delete", name="mecanico_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_ELIMINAR")
     */
    public function deleteAction(Request $request, Mecanico $mecanico)
    {
        $form = $this->createDeleteForm($mecanico->getId());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->mecanicoManager->delete($mecanico);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/mante/mecanico", name="mecanico_get", options={"expose": true})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_VER")
     */
    public function getAction(Request $request)
    {
        $idOrg = $request->get('idOrg');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find($idOrg);
        //recargo los productos y devuelvo
        $mecanico = $this->mecanicoManager->findAllQuery($organizacion, $search);

        $str = array();
        foreach ($mecanico as $prod) {
            $str[] = array('id' => $prod->getId(), 'text' => $prod->getNombre());
        }

        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/mante/mecanico/addmecanico", name="mecanico_addmecanico",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     *     options={"expose": true},
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_EDITAR")
     */
    public function addMecanicoAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $mecanico = $this->mecanicoManager->create($organizacion);
        $tmp = array();
        parse_str($request->get('formMecanico'), $tmp);

        $dataMecanico = $tmp['mecanico'];

        if ($dataMecanico) {
            $mecanico->setNombre($dataMecanico['nombre']);
            $mecanico->setDocumento($dataMecanico['documento']);
            $mecanico->setSeguro($dataMecanico['seguro']);
            $mecanico->setTelefonoParticular($dataMecanico['telefonoParticular']);
            $mecanico->setTelefonoContacto($dataMecanico['telefonoContacto']);
            if ($dataMecanico['costoHora']) {
                $mecanico->setCostoHora(intval($dataMecanico['costoHora']));
            } else {
                $mecanico->setCostoHora(0);
            }
            $mecanico->setTipo($dataMecanico['tipo']);
            $mecanico->setOrganizacion($organizacion);
            $mecanico = $this->mecanicoManager->save($mecanico);
        }
        $mecanicos = $this->mecanicoManager->findAll($organizacion);
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/Mecanico/mecanicos.html.twig', array(
                'mecanicos' => $mecanicos
            )),
        )));
    }

    /**
     * @Route("/mante/mecanico/delmecanico", name="mecanico_delmecanico",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     *     options={"expose": true},
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_ELIMINAR")
     */
    public function delMecanicoAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $idMecanico = $request->get('idMecanico');   //es el id del ordentrabajoproducto
        $mecanico = $this->mecanicoManager->findById($idMecanico);
        $remove = $this->mecanicoManager->delete($mecanico);
        $mecanicos = $this->mecanicoManager->findAll($organizacion);
        //die('////<pre>' . nl2br(var_export(count($sectores), true)) . '</pre>////');
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/Mecanico/mecanicos.html.twig', array(
                'mecanicos' => $mecanicos
            )),
        )));
    }

    /**
     * @Route("/mante/mecanico/{id}/informe", name="mecanico_informe",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_VER")
     */
    public function informeAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Informe de Mecánicos");
        $options['mecanicos'] = $this->mecanicoManager->findAll($organizacion);
        $form = $this->createForm(InformeMecanicoType::class, null, $options);
        $form->handleRequest($request);
        $mecanico = array();
        $arr = array();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->get('informe');
            $ok = $this->utilsManager->isValidDesdeHasta('d/m/Y H:i', $data['desde'], $data['hasta']);
            if ($ok) {
                $fecha_desde = $this->utilsManager->datetime2sqltimestamp($data['desde'] . ':00');
                $fecha_hasta = $this->utilsManager->datetime2sqltimestamp($data['hasta'] . ':59');
                if ($data['mecanico'] == 0) {
                    $mecanicos = $this->mecanicoManager->findAll($organizacion);
                    $data['mecaniconombre'] = 'Todos';
                } else {
                    $mecanico = $this->mecanicoManager->find($data['mecanico']);
                    $mecanicos[] = $mecanico;
                    $data['mecaniconombre'] = $mecanico->getNombre();
                }
                foreach ($mecanicos as $mecanico) {
                    $totalMecanico = 0;
                    $ordenes = $this->ordentrabajoManager->findByMecanico($fecha_desde, $fecha_hasta, $mecanico);
                    if (count($ordenes) > 0) {
                        $arr[$mecanico->getId()]['mecanico'] = $mecanico->getNombre();
                    }
                    foreach ($ordenes as $orden) {
                        $arr[$mecanico->getId()]['ordenes'][] = array(
                            'mecanico' => $mecanico->getNombre(),
                            'identificador' => is_null($orden->getOrdenTrabajo()->getIdentificador()) ? '---' : $orden->getOrdenTrabajo()->getFormatIdentificador(),
                            'tipo' => ($orden->getOrdenTrabajo()->getTipo() == 0) ? 'C' : 'P',
                            'fecha' => is_null($orden->getOrdenTrabajo()->getFecha()) ? '---' : $orden->getOrdenTrabajo()->getFecha()->format('d/m/Y'),
                            'taller' => is_null($orden->getOrdenTrabajo()->getTaller()) ? '---' : $orden->getOrdenTrabajo()->getTaller()->getNombre(),
                            'sector' => is_null($orden->getOrdenTrabajo()->getTallerSector()) ? '---' : $orden->getOrdenTrabajo()->getTallerSector()->getNombre(),
                            'descripcion' => is_null($orden->getOrdenTrabajo()->getDescripcion()) ? '---' : $orden->getOrdenTrabajo()->getDescripcion(),
                            'cantHoras' => is_null($orden->getHoras()) ? '---' : $orden->getHoras(),
                            'costo' => is_null($orden->getCosto()) ? '---' : $orden->getCosto(),
                            'costoTotal' => is_null($orden->getCostoTotal()) ? '---' : $orden->getCostoTotal(),
                        );
                        $totalMecanico += $orden->getCostoTotal();
                        $arr[$mecanico->getId()]['totalMecanico'] = $totalMecanico;
                    }
                }

                return $this->render('mante/Mecanico/pantalla.html.twig', array(
                    'arr' => $arr,
                    'consulta' => $data,
                    'mecanicos' => $mecanicos
                ));
            }
        }
        return $this->render('mante/Mecanico/informe.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/mante/mecanico/informe/exportar", name="mecanico_informe_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MECANICO_VER")
     */
    public function exportarAction(Request $request, $tipo = null, $consulta = null, $informe = null)
    {
        if ($informe == null) {
            $consulta = json_decode($request->get('consulta'), true);
            $informe = json_decode($request->get('informe'), true);
        }
        if (isset($consulta) && isset($informe)) {

            //creo el xls
            $xls = $this->excelManager->create("Informe de Mecánicos");

            $xls->setHeaderInfo(array(
                'C2' => 'Mecánico',
                'D2' => $consulta['mecaniconombre'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['hasta'],
            ));


            $xls->setBar(6, array(
                'A' => array('title' => 'Mecánico', 'width' => 20),
                'B' => array('title' => 'Fecha', 'width' => 15),
                'C' => array('title' => 'Tipo', 'width' => 15),
                'D' => array('title' => 'Identificador', 'width' => 15),
                'E' => array('title' => 'Taller', 'width' => 20),
                'F' => array('title' => 'Sector', 'width' => 15),
                'G' => array('title' => 'Descripción', 'width' => 15),
                'H' => array('title' => 'Cant. Horas', 'width' => 15),
                'I' => array('title' => 'Costo Hora', 'width' => 15),
                'J' => array('title' => 'Costo Total', 'width' => 15),
            ));
            $i = 7;
            foreach ($informe as $arr) {   //esto es para cada servicio
                foreach ($arr['ordenes'] as $datos) {   //esto es para cada servicio
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $datos['mecanico']),
                        'B' => array('value' => $datos['fecha']),
                        'C' => array('value' => $datos['tipo']),
                        'D' => array('value' => $datos['identificador']),
                        'E' => array('value' => $datos['taller']),
                        'F' => array('value' => $datos['sector']),
                        'G' => array('value' => $datos['descripcion']),
                        'H' => array('value' => $datos['cantHoras']),
                        'I' => array('value' => $datos['costo'], 'format' => '0.00'),
                        'J' => array('value' => $datos['costoTotal'], 'format' => '0.00'),
                    ));
                    $i++;
                }
            }
            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=mecanicos.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('mecanico_list'));
        }
    }
}
