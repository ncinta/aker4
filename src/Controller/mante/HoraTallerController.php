<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion;
use App\Entity\HoraTaller;
use App\Form\mante\HoraTallerType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\HoraTallerManager;
use App\Model\app\Router\HoraTallerRouter;
use App\Model\app\OrdenTrabajoHoraTallerManager;
use App\Model\app\TallerExternoManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of HoraTallerController
 *
 * @author nicolas
 */
class HoraTallerController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @Route("/mante/horataller", name="horataller")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function getAction(Request $request, OrganizacionManager $organizacionManager, HoraTallerManager $horaTallerManager)
    {
        $idOrg = $request->get('idOrg');
        $search = $request->get('search');
        $organizacion = $organizacionManager->find($idOrg);
        //recargo los productos y devuelvo
        $horas = $horaTallerManager->findAllQuery($organizacion, $search);

        $str = array();
        foreach ($horas as $prod) {
            $x = $prod->getNombre();
            $str[] = array('id' => $prod->getId(), 'text' => $x);
        }

        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/mante/horataller/add", name="horataller")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function addAction(
        Request $request,
        OrganizacionManager $organizacionManager,
        HoraTallerManager $horaTallerManager,
        OrdenTrabajoHoraTallerManager $ordentrabajoHoraTallerManager
    ) {

        $idOrg = $request->get('idOrg');
        $organizacion = $organizacionManager->find($idOrg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);

        if ($request->getMethod() == 'POST' && isset($tmp['horatallernew'])) {
            $data = $tmp['horatallernew'];
            $hora = $horaTallerManager->create($organizacion);
            $hora->setOrganizacion($organizacion);
            $hora->setNombre($data['nombre']);
            if ($horaTallerManager->save($hora)) {
                //recargo las horas y devuelvo
                $horas = $ordentrabajoHoraTallerManager->findAllByOrganizacion($organizacion);
                $str = array();
                foreach ($horas as $prod) {
                    $x = $prod->getHoraTaller()->getNombre();
                    $str[$x] = array($prod->getId());
                }

                $html = $this->renderView('mante/HoraTaller/content_list.html.twig', array(
                    'horas' => $horas,
                ));
                return new Response(json_encode(array('status' => 'ok', 'data' => $str, 'html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/mante/horataller/{idorg}/new", name="horataller_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"})
     * @IsGranted("ROLE_MANT_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        HoraTallerManager $horatallerManager,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoManager $tallerExternoManager
    ) {

        $horaTaller = $horatallerManager->create($organizacion);
        $form = $this->createForm(HoraTallerType::class, $horaTaller);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $horataller = $tallerExternoManager->save($horaTaller);
            $breadcrumbManager->pop();
            $this->setFlash('success', sprintf('La descripción de la hora taller de grabó con exito'));
            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nueva hora taller");
        return $this->render('mante/HoraTaller/new.html.twig', array(
            'organizacion' => $organizacion,
            'horataller' => $horaTaller,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/horataller/{id}/edit", name="horataller_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_EDITAR")
     */
    public function editAction(
        Request $request,
        HoraTaller $horataller,
        HoraTallerManager $horatallerManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $form = $this->createForm(HoraTallerType::class, $horataller);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $horaTaller = $horatallerManager->save($horataller);
            $this->setFlash('success', sprintf('La descripción de hora taller se ha modificado con éxito'));
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar Taller");
        return $this->render('mante/HoraTaller/edit.html.twig', array(
            'horataller' => $horataller,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/horataller/{idorg}/list", name="horataller_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        HoraTallerManager $horatallerManager,
        BreadcrumbManager $breadcrumbManager,
        HoraTallerRouter $horatallerRouter
    ) {

        $horastaller = $horatallerManager->findAllByOrganizacion($organizacion);
        $menu = array(
            1 => array($horatallerRouter->btnNew($organizacion)),
        );
        $breadcrumbManager->push($request->getRequestUri(), "Horas Taller");

        return $this->render('mante/HoraTaller/list.html.twig', array(
            'menu' => $horatallerRouter->toCalypso($menu),
            'horastaller' => $horastaller,
        ));
    }

    /**
     * @Route("/mante/horataller/{id}/show", name="horataller_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function showAction(
        Request $request,
        HoraTaller $horataller,
        BreadcrumbManager $breadcrumbManager,
        HoraTallerRouter $horatallerRouter
    ) {

        $deleteForm = $this->createDeleteForm($horataller->getId());
        if (!$horataller) {
            throw $this->createNotFoundException('Código de Hora taller no encontrado.');
        }

        $breadcrumbManager->push($request->getRequestUri(), $horataller->getNombre());

        return $this->render('mante/HoraTaller/show.html.twig', array(
            'menu' => $this->getShowMenu($horataller, $horatallerRouter),
            'horataller' => $horataller,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getShowMenu($horataller, $horatallerRouter)
    {
        $menu = array(
            1 => array($horatallerRouter->btnEdit($horataller)),
            2 => array($horatallerRouter->btnDelete($horataller))
        );
        return $horatallerRouter->toCalypso($menu);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/mante/horataller/{id}/delete", name="horataller_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_ELIMINAR")
     */
    public function deleteAction(Request $request, $id, BreadcrumbManager $breadcrumbManager, HoraTallerManager $horatallerManager)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();

            if ($horatallerManager->deleteById($id)) {
                $this->setFlash('success', 'Hora Taller eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar la hora del sistema.');
            }
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }
}
