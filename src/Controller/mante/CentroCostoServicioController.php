<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Prestacion;
use App\Entity\CentroCosto;
use App\Form\mante\CentroCostoServicioType;
use App\Form\mante\CentroCostoServicioRetirarType;
use App\Form\mante\CentroCostoServicioDeleteType;
use App\Form\mante\CentroCostoServicioPausarType;
use App\Form\mante\CentroCostoServicioIniciarType;
use App\Form\mante\CentroCostoServicioAddStopType;
use App\Form\mante\CentroCostoServicioEditType;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\PrestacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\UtilsManager;
use App\Model\app\BitacoraCentroCostoManager;
use App\Model\app\CentroCostoManager;
use App\Model\app\Router\CentroCostoRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CentroCostoServicioController extends AbstractController
{

    private $bread;
    private $centrocosto;
    private $ccRouter;
    private $prestacion;
    private $bitacoraCCManager;
    private $servicioManager;
    private $utils;
    private $userlogin;

    public function __construct(
        BreadcrumbManager $bread,
        BitacoraCentroCostoManager $bitacoraCCManager,
        CentroCostoManager $centrocostoManager,
        CentroCostoRouter $centrocostoRouter,
        PrestacionManager $prestacionManager,
        ServicioManager $servicioManager,
        UtilsManager $utilsManager,
        UserLoginManager $userloginManager
    ) {
        $this->bitacoraCCManager = $bitacoraCCManager;
        $this->bread = $bread;
        $this->centrocosto = $centrocostoManager;
        $this->ccRouter = $centrocostoRouter;
        $this->prestacion = $prestacionManager;
        $this->utils = $utilsManager;
        $this->servicioManager = $servicioManager;
        $this->userlogin = $userloginManager;
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/asignar", name="centrocosto_servicio_asignar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_AGREGAR")
     */
    public function asignarAction(Request $request, CentroCosto $centrocosto)
    {
        $form = $this->createForm(CentroCostoServicioType::class, null, array(
            'organizacion' => $centrocosto->getOrganizacion(),
            'em' => $this->getEntityManager(),
            'centro_id' => $centrocosto->getId(),
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            foreach ($data['servicio'] as $serv) {
                //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
                $servicio = $this->servicioManager->find(intval($serv));

                $centrocostoAnt = null;
                //busco la prestacion activa actual.
                $prestacionActiva = $this->prestacion->findActivo($servicio);
                if ($prestacionActiva) {   //rescato el centro de costo
                    $centrocostoAnt = $prestacionActiva->getCentroCosto();
                }

                //configurar la prestacion nueva para el servicio y el centro de costo
                $prestacion = $this->prestacion->create($centrocosto->getOrganizacion());
                if ($data['inicio_prestacion'] != '') {
                    $desde = $this->utils->datetime2sqltimestamp($data['inicio_prestacion'], true);
                    $desde = new \DateTime($desde, new \DateTimeZone($centrocosto->getOrganizacion()->getTimezone()));
                } else {
                    $desde = new \DateTime();
                    $desde->setTimezone(new \DateTimeZone($centrocosto->getOrganizacion()->getTimezone()));
                }
                $prestacion->setInicioPrestacion($desde);
                $prestacion->setServicio($servicio);
                $prestacion->setCentroCosto($centrocosto);
                $prestacion->setEstado(1); //activo la prestación
                if (isset($data['horasUso'])) {
                    $prestacion->setHorasUso(intval($data['horasUso']));
                }
                //ahora desactivo la prestacion anterior
                $motivo = sprintf('Se pasa al centro de costo %s', $prestacion->getCentroCosto()->getNombre());
                if ($prestacionActiva) {
                    $this->prestacion->desactivarPrestacion($prestacionActiva, $motivo, $desde, null);
                }

                if ($this->prestacion->save($prestacion)) {
                    //agregar el servicio al nuevo centro de costo.
                    if ($this->centrocosto->addServicio($centrocosto, $servicio)) {
                        $this->bitacoraCCManager->asignarServicio($this->userlogin->getUser(), $centrocosto, $servicio, $data['inicio_prestacion'], intval($data['horasUso']));

                        if ($centrocostoAnt) {   //esto es para que si no esta asociado a nada no retire de ningun lado.
                            $this->bitacoraCCManager->retirarServicio($this->userlogin->getUser(), $centrocostoAnt, $servicio, $motivo);
                        }
                    }
                }
            }
            //el servicio q se esta creando es un vehiculo
            $this->setFlash('success', sprintf('El servicio se ha agregado con éxito'));

            $this->bread->pop();
            return $this->redirect($this->bread->getVolver());
        }

        $this->bread->pushPorto($request->getRequestUri(), "Nuevo CentroCosto");
        return $this->render('mante/CentroCostoServicio/new.html.twig', array(
            'centrocosto' => $centrocosto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/retirarmany", name="centrocosto_servicio_retirarmany")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_AGREGAR")
     */
    public function retirarManyAction(Request $request, CentroCosto $centrocosto)
    {
        $form = $this->createForm(CentroCostoServicioRetirarType::class, null, array(
            'em' => $this->getEntityManager(),
            'centro_id' => $centrocosto->getId(),
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $fechaFin = new \DateTime();
            if (isset($data['fin_prestacion'])) {
                $fechaFin = $this->utils->datetime2sqltimestamp($data['fin_prestacion'], true);
                $fechaFin = new \DateTime($fechaFin);
            }
            foreach ($data['servicio'] as $serv) {
                $servicio = $this->servicioManager->find(intval($serv));
                $prestacion = $this->prestacion->findByCcServicio($servicio, $centrocosto);
              
                if ($this->prestacion->desactivarPrestacion($prestacion, $data['motivo'], $fechaFin, $servicio)) {

                    $bitacora = $this->bitacoraCCManager->retirarServicio($this->userlogin->getUser(), $centrocosto, $servicio, $data['motivo'], $data['fin_prestacion']);
                }
            }

            //el servicio q se esta creando es un vehiculo
            $this->setFlash('success', sprintf('servicios retirados con  con éxito'));
            $this->bread->pop();
            return $this->redirect($this->bread->getVolver());
        }

        $this->bread->pushPorto($request->getRequestUri(), "Desafectar servicios");
        return $this->render('mante/CentroCostoServicio/retirar_many.html.twig', array(
            'centrocosto' => $centrocosto,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/mante/prestacion/{id}/servicio/editar", name="prestacion_servicio_editar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_AGREGAR")
     */
    public function editarAction(
        Request $request,
        Prestacion $prestacion,
        BreadcrumbManager $breadcrumbManager,
        PrestacionManager $prestacionManager,
        UtilsManager $utilsManager
    ) {

        $form = $this->createForm(CentroCostoServicioEditType::class, null, array(
            'prestacion' => $prestacion,
            'em' => $this->getEntityManager(),
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($data['inicio_prestacion'] != '') {
                $desde = $utilsManager->datetime2sqltimestamp($data['inicio_prestacion'], true);
                $desde = new \DateTime($desde, new \DateTimeZone($prestacion->getCentroCosto()->getOrganizacion()->getTimezone()));
            } else {
                $desde = new \DateTime();
                $desde->setTimezone(new \DateTimeZone($prestacion->getCentroCosto()->getOrganizacion()->getTimezone()));
            }
            $prestacion->setInicioPrestacion($desde);
            $prestacion->setEstado(1); //activo la prestación
            if (isset($data['horasUso'])) {
                $prestacion->setHorasUso(intval($data['horasUso']));
            }
            //ahora desactivo la prestacion anterior
            $motivo = sprintf('Se pasa al centro de costo %s', $prestacion->getCentroCosto()->getNombre());
            if ($prestacionManager->save($prestacion)) {
                $this->setFlash('success', sprintf('La prestación se ha modificado con éxito'));

                $breadcrumbManager->pop();
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        $breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Prestación");
        return $this->render('mante/CentroCostoServicio/edit.html.twig', array(
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    private function toFechaSQL($organizacion, $fechaIn)
    {
        $fecha = new \DateTime($fechaIn, new \DateTimeZone($organizacion->getTimezone()));
        return $fecha;
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/retirar", name="centrocosto_servicio_retirar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_ELIMINAR")
     */
    public function retirarAction(
        Request $request,
        Prestacion $prestacion,
        BreadcrumbManager $breadcrumbManager,
        PrestacionManager $prestacionManager,
        UserLoginManager $userloginManager,
        UtilsManager $utilsManager,
        BitacoraCentroCostoManager $bitacoraCCManage
    ) {

        $form = $this->createForm(CentroCostoServicioDeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $fechaFin = new \DateTime();
            if (isset($data['fin_prestacion'])) {
                $fechaFin = $utilsManager->datetime2sqltimestamp($data['fin_prestacion'], true);
                $fechaFin = new \DateTime($fechaFin);
            }
            $servicio = $prestacion->getServicio();
            $centroCosto = $servicio->getCentroCosto();

            if ($prestacionManager->desactivarPrestacion($prestacion, $data['motivo'], $fechaFin, $prestacion->getServicio())) {

                $breadcrumbManager->pop();
                $bitacora = $bitacoraCCManage->retirarServicio($userloginManager->getUser(), $centroCosto, $servicio, $data['motivo'], $data['fin_prestacion']);
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        $breadcrumbManager->pushPorto($request->getRequestUri(), "Retirar Servicio");
        return $this->render('mante/CentroCostoServicio/retiro.html.twig', array(
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/iniciar", name="centrocosto_servicio_iniciar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_ELIMINAR")
     */
    public function iniciarAction(
        Request $request,
        Prestacion $prestacion,
        BreadcrumbManager $breadcrumbManager,
        PrestacionManager $prestacionManager,
        UtilsManager $utilsManager
    ) {

        $form = $this->createForm(CentroCostoServicioIniciarType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $finPausa = new \DateTime();
            if (isset($data['fin_pausa'])) {
                $f = $utilsManager->datetime2sqltimestamp($data['fin_pausa'], true);
                $finPausa = new \DateTime($f);
            }


            if ($prestacionManager->iniciarPrestacion($prestacion, $finPausa)) {
                $breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
        $breadcrumbManager->pushPorto($request->getRequestUri(), "Retirar Servicio");
        return $this->render('mante/CentroCostoServicio/iniciar.html.twig', array(
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/pausar", name="centrocosto_servicio_pausar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_ELIMINAR")
     */
    public function pausarAction(
        Request $request,
        Prestacion $prestacion,
        BreadcrumbManager $breadcrumbManager,
        PrestacionManager $prestacionManager,
        UtilsManager $utilsManager
    ) {

        $form = $this->createForm(CentroCostoServicioPausarType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $inicioPausa = new \DateTime();
            if (isset($data['inicio_pausa'])) {
                $inicioPausa = new \DateTime($utilsManager->datetime2sqltimestamp($data['inicio_pausa'], true));
            }

            if ($prestacionManager->pausarPrestacion($prestacion, $data['motivo'], $inicioPausa)) {
                $breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        $breadcrumbManager->pushPorto($request->getRequestUri(), "Retirar Servicio");
        return $this->render('mante/CentroCostoServicio/pausar.html.twig', array(
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/servicio/addstop", name="centrocosto_servicio_addstop")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_ELIMINAR")
     */
    public function addstopAction(
        Request $request,
        Prestacion $prestacion,
        BreadcrumbManager $breadcrumbManager,
        PrestacionManager $prestacionManager,
        UtilsManager $utilsManager
    ) {

        $form = $this->createForm(CentroCostoServicioAddStopType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $inicioPausa = new \DateTime();
            if (isset($data['inicio_pausa'])) {
                $inicioPausa = new \DateTime($utilsManager->datetime2sqltimestamp($data['inicio_pausa'], true));
                $finPausa = new \DateTime($utilsManager->datetime2sqltimestamp($data['fin_pausa'], true));
            }

            if ($prestacionManager->addStop($prestacion, $data['motivo'], $inicioPausa, $finPausa)) {
                $breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($breadcrumbManager->getVolver());
            }
        }

        $breadcrumbManager->pushPorto($request->getRequestUri(), "Retirar Servicio");
        return $this->render('mante/CentroCostoServicio/addstop.html.twig', array(
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
