<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\VehiculoModelo;
use App\Entity\Organizacion;
use App\Entity\VehiculoFichaTecnica as VehiculoFichaTecnica;
use App\Form\mante\VehiculoModeloType;
use App\Form\mante\VehiculoFichaTecnicaType;
use App\Model\app\UserLoginManager;
use App\Model\app\VehiculoModeloManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DepositoManager;
use App\Model\app\ProductoManager;
use App\Model\app\ActividadManager;
use App\Model\app\VehiculoFichaTecnicaManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of VehiculoModeloController
 *
 * @author nicolas
 */
class VehiculoModeloController extends AbstractController
{

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu($organizacion, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_FLOTA_AGREGAR')) {
            $menu['Agregar Modelo'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('vehiculomodelo_new', array('idOrg' => $organizacion->getId()))
            );
        }
        return $menu;
    }

    private function getShowMenu($modelo, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_FLOTA_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('vehiculomodelo_edit', array('id' => $modelo->getId()))
            );
            if ($userloginManager->isGranted('ROLE_FLOTA_ELIMINAR')) {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
            if ($userloginManager->isGranted('ROLE_FLOTA_EDITAR')) {
                $menu['Producto'] = array(
                    'imagen' => 'icon-plus icon-blue',
                    'url' => '#modalProducto',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
        return $menu;
    }

    /**
     * @Route("/mante/vehiculomodelos/{idOrg}/list", name="vehiculomodelo_list")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        VehiculoModeloManager $vehiculomodeloManager,
        UserLoginManager $userloginManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Modelos");
        $modelos = $vehiculomodeloManager->find($organizacion);

        return $this->render('mante/VehiculoModelo/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion, $userloginManager),
            'modelos' => $modelos,
        ));
    }

    /**
     * @Route("/mante/vehiculomodelo/{idOrg}/new", name="vehiculomodelo_new",
     *  *     requirements={
     *         "id": "\d+"
     *     },
     * ) 
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_FLOTA_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        VehiculoModeloManager $vehiculomodeloManager
    ) {

        $modelo = $vehiculomodeloManager->create($organizacion);
        $options['em'] = $this->getEntityManager();
        $options['organizacion'] = $organizacion;
        $form = $this->createForm(VehiculoModeloType::class, $modelo, $options);
        if ($request->getMethod() == 'POST') {
            $modelo->setNombre($request->get('vehiculomodelo')['nombre']);
            if ($vehiculomodeloManager->save($modelo)) {
                $breadcrumbManager->pop();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha creado el modelo <b>%s</b> en el sistema.', strtoupper($modelo))
                );
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para crear el modelo "%s" en el sistema. Es posible que el Id ya exista.', strtoupper($modelo)));
            }
            return $this->redirect($this->generateUrl('vehiculomodelo_show', array(
                'id' => $modelo->getId(),
            )));
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Modelo");
        return $this->render('mante/VehiculoModelo/new.html.twig', array(
            'organizacion' => $organizacion,
            'modelo' => $modelo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/vehiculomodelo/{id}/edit", name="vehiculomodelo_edit",
     *     requirements={
     *         "id": "\d+",
     *     }, 
     * )
     * @Method({"GET", "POST"})
     */
    public function editAction(
        Request $request,
        VehiculoModelo $modelo,
        BreadcrumbManager $breadcrumbManager,
        VehiculoModeloManager $vehiculomodeloManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Editar Modelo");
        $options['em'] = $this->getEntityManager();
        $options['organizacion'] = $modelo->getOrganizacion();
        $form = $this->createForm(VehiculoModeloType::class, $modelo, $options);
        if ($request->getMethod() == 'POST') {
            $modelo->setNombre($request->get('vehiculomodelo')['nombre']);
            if ($vehiculomodeloManager->save($modelo)) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha modificó el modelo <b>%s</b> en el sistema.', strtoupper($modelo))
                );
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para modificar el modelo "%s" en el sistema. Es posible que el Id ya exista.', strtoupper($modelo)));
            }

            return $this->redirect($this->generateUrl('vehiculomodelo_show', array(
                'id' => $modelo->getId(),
            )));
        }
        return $this->render('mante/VehiculoModelo/edit.html.twig', array(
            'modelo' => $modelo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/vehiculomodelo/{id}/show", name="vehiculomodelo_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(
        Request $request,
        VehiculoModelo $modelo,
        BreadcrumbManager $breadcrumbManager,
        DepositoManager $depositoManager,
        UserLoginManager $userloginManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Ver Modelo");
        $organizacion = $modelo->getOrganizacion();
        $depositos = $depositoManager->findAllByOrganizacion($organizacion);
        $formProducto = $this->createForm(VehiculoFichaTecnicaType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));
        return $this->render('mante/VehiculoModelo/show.html.twig', array(
            'modelo' => $modelo,
            'depositos' => $depositos,
            'formProducto' => $formProducto->createView(),
            'menu' => $this->getShowMenu($modelo, $userloginManager),
            'delete_form' => $this->createDeleteForm($modelo->getId())->createView(),
        ));
    }

    /**
     * @Route("/mante/vehiculomodelo/{id}/delete", name="vehiculomodelo_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     */
    public function deleteAction(
        Request $request,
        VehiculoModelo $modelo,
        BreadcrumbManager $breadcrumbManager,
        VehiculoModeloManager $vehiculomodeloManager
    ) {

        $form = $this->createDeleteForm($modelo->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();
            if ($vehiculomodeloManager->deleteById($modelo->getId())) {
                $this->get('session')->getFlashBag()->add(
                    'success',
                    sprintf('Se ha eliminó el modelo  en el sistema.')
                );
            } else {
                $this->get('session')->getFlashBag()->add('error', sprintf('Hubo problemas para el  modelo en el sistema. Es posible que el Id no exista.'));
            }
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    /**
     * @Route("/mante/vehiculomodelo/producto/add", name="vehiculomodelo_producto_add",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_AGREGAR")
     */
    public function addProductoAction(
        Request $request,
        VehiculoModeloManager $vehiculomodeloManager,
        ProductoManager $productoManager,
        ActividadManager $actividadManager,
        VehiculoFichaTecnicaManager $fichatecnicaManager
    ) {
        $idModelo = $request->get('id');
        $modelo = $vehiculomodeloManager->findOne($idModelo);

        $tmp = array();
        parse_str($request->get('formProducto'), $tmp);
        $dataProd = $tmp['vehiculofichatecnica'];
        if ($dataProd) {
            $producto = $productoManager->find($dataProd['producto']);
            $actividad = $actividadManager->find($dataProd['actividad']);
            $ficha = new VehiculoFichaTecnica();
            $ficha->setActividad($actividad);
            $ficha->setProducto($producto);
            $ficha->setCantidad($dataProd['cantidad']);
            $ficha->setFrecuenciaCambio($dataProd['frecuenciaCambio']);
            $ficha->setAlternativo($dataProd['alternativo']);
            $fichat = $fichatecnicaManager->save($ficha);
            $modelo->addFichasTecnicas($fichat);
            $modelo = $vehiculomodeloManager->save($modelo);
        }

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/VehiculoModelo/productos.html.twig', array(
                'modelo' => $modelo
            )),
        )));
    }

    /**
     * @Route("/mante/vehiculomodelo/producto/del", name="vehiculomodelo_producto_del",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_AGREGAR")
     */
    public function delProductoAction(Request $request, VehiculoFichaTecnicaManager $fichatecnicaManager, VehiculoModeloManager $vehiculomodeloManager)
    {
        $idFicha = $request->get('idFicha');   //es el id del ordentrabajoproducto

        $idModelo = $request->get('id');
        $modelo = $vehiculomodeloManager->findOne($idModelo);

        $fichatecnicaManager->delFichaTecnica($idFicha);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/VehiculoModelo/productos.html.twig', array(
                'modelo' => $modelo
            )),
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }
}
