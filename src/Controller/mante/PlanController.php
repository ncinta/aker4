<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Form\mante\PlanType;
use App\Entity\Organizacion;
use App\Entity\Mantenimiento;
use App\Model\app\BreadcrumbManager;
use App\Model\app\MantenimientoManager;
use App\Model\app\Router\PlanRouter;
use App\Model\app\Router\ActividadRouter;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of Mantenimiento
 *
 * @author claudio
 */
class PlanController extends AbstractController
{

    /**
     * @Route("/mante/plan/{idOrg}/list", name="plan_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        MantenimientoManager $mantenimientoManager,
        PlanRouter $planRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Mantenimiento");
        $mantenimientos = $mantenimientoManager->findAsociados($organizacion);
        $menu = array(
            1 => array($planRouter->btnNew($organizacion)),
        );
        $menu = $planRouter->toCalypso($menu);
        return $this->render('mante/Plan/list.html.twig', array(
            'menu' => $menu,
            'mantenimientos' => $mantenimientos,
        ));
    }

    /**
     * Lista todas las cargas de combustible de un servicio
     * @Route("/mante/plan/{id}/show", name="plan_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_VER")
     */
    public function showAction(
        Request $request,
        Mantenimiento $mantenimiento,
        BreadcrumbManager $breadcrumbManager,
        PlanRouter $planRouter,
        ActividadRouter $actividadRouter,
        UserLoginManager $userloginManager
    ) {
        if (!$mantenimiento) {
            throw $this->createNotFoundException('Código de Mantenimiento no encontrado.');
        }

        $breadcrumbManager->push($request->getRequestUri(), $mantenimiento->getNombre());

        return $this->render('mante/Plan/show.html.twig', array(
            'menu' => $this->getShowMenu($mantenimiento, $planRouter, $actividadRouter, $userloginManager),
            'mantenimiento' => $mantenimiento,
            'delete_form' => $this->createDeleteForm($mantenimiento->getId())->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $mantenimiento 
     * @return array $menu
     */
    private function getShowMenu($mantenimiento, $planRouter, $actividadRouter, $userloginManager)
    {
        $menu = array();
        $usrOrg = $userloginManager->getOrganizacion();
        if ($usrOrg == $mantenimiento->getPropietario() || $usrOrg->getTipoOrganizacion() == 1) {

            $menu = array(
                1 => array($planRouter->btnEdit($mantenimiento)),
                2 => array(
                    $actividadRouter->btnNew($mantenimiento),
                ),
                3 => array($planRouter->btnDelete($mantenimiento)),
            );
        }
        return $planRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/plan/{idOrg}/new", name="plan_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_MANT_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        MantenimientoManager $mantenimientoManager
    ) {

        $mant = $mantenimientoManager->create($organizacion);
        $form = $this->createForm(PlanType::class, $mant);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($mantenimientoManager->save($mant)) {
                $breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Se ha creado el mantenimiento <b>%s</b>.', strtoupper($mant->getNombre())));
                return $this->redirect($breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'No se cumplen las condiciones para grabar el mantenimiento');
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Mantenimiento");
        return $this->render('mante/Plan/new.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView(),
            'mantenimiento' => $mant,
        ));
    }

    /**
     * @Route("/mante/plan/{id}/edit", name="plan_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_EDITAR")
     */
    public function editAction(Request $request, Mantenimiento $mant, BreadcrumbManager $breadcrumbManager, MantenimientoManager $mantenimientoManager)
    {

        if (!$mant) {
            throw $this->createNotFoundException('Código de Mantenimiento no encontrado.');
        }
        $form = $this->createForm(PlanType::class, $mant);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $breadcrumbManager->pop();
            $mantenimientoManager->save($mant);

            $this->setFlash('success', sprintf('Los datos de <b>%s</b> han sido actualizados', strtoupper($mant->getNombre())));
            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Editar Mantenimiento");
        return $this->render('mante/Plan/edit.html.twig', array(
            'form' => $form->createView(),
            'mantenimiento' => $mant,
        ));
    }

    /**
     * @Route("/mante/plan/{id}/delete", name="plan_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        Mantenimiento $mantenimiento,
        BreadcrumbManager $breadcrumbManager,
        MantenimientoManager $mantenimientoManager
    ) {
        $form = $this->createDeleteForm($mantenimiento->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();
            $mantenimientoManager->delete($mantenimiento->getId());
            $this->setFlash('success', 'El plan se eliminó correctamente');
        }
        return $this->redirect($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/mante/plan/data", name="plan_dataAJAX")
     * @Method({"GET"})
     */
    public function dataAjaxAction(Request $request, MantenimientoManager $mantenimientoManager, UtilsManager $utilsManager)
    {
        $id = $request->get('id');
        $mant = null;
        if ($id != null) {
            $mant = $mantenimientoManager->find($id);
            $array = $utilsManager->obj2array($mant->getVars());
        }
        return new Response(json_encode($array));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
