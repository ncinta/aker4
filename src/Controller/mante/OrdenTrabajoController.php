<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Organizacion as Organizacion;
use App\Entity\TareaOtMecanico as TareaOtMecanico;
use App\Entity\TareaOtProducto as TareaOtProducto;
use App\Entity\OrdenTrabajo as OrdenTrabajo;
use App\Form\mante\OrdenTrabajoType;
use App\Form\mante\OrdenTrabajoServicioType;
use App\Form\mante\OrdenTrabajoProductoType;
use App\Form\mante\HistoricoOrdenTrabajoType;
use App\Form\mante\OrdenTrabajoMecanicoType;
use App\Form\mante\OrdenTrabajoExternoType;
use App\Form\mante\OrdenTrabajoHorasTallerType;
use App\Form\mante\OrdenTrabajoTareaType;
use App\Form\mante\OrdenTrabajoCerrarType;
use App\Model\app\ServicioManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\DepositoManager;
use App\Model\app\TallerManager;
use App\Model\app\SectorTallerManager;
use App\Model\app\CentroCostoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\app\TareaMantManager;
use App\Model\app\ProductoManager;
use App\Model\app\MecanicoManager;
use App\Model\app\StockManager;
use App\Model\app\TareaOtManager;
use App\Model\app\HoraTallerManager;
use App\Model\crm\PeticionManager;
use App\Model\app\Router\OrdenTrabajoRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Snappy\Pdf;

class OrdenTrabajoController extends AbstractController
{

    protected $tallerManager;
    protected $servicioManager;
    protected $gruposervicioManager;
    protected $centrocostoManager;
    protected $ordentrabajoManager;
    protected $breadcrumbManager;
    protected $depositoManager;
    protected $peticionManager;
    protected $tareamantManager;
    protected $ordentrabajoRouter;
    protected $stockManager;
    protected $productoManager;
    protected $horatallerManager;
    protected $tareaotManager;
    protected $utilsManager;
    protected $mecanicoManager;
    protected $userloginManager;
    protected $sectorManager;
    protected $excelManager;

    function __construct(
        TallerManager $tallerManager,
        ServicioManager $servicioManager,
        PeticionManager $peticionManager,
        UtilsManager $utilsManager,
        GrupoServicioManager $gruposervicioManager,
        CentroCostoManager $centrocostoManager,
        OrdenTrabajoManager $ordentrabajoManager,
        BreadcrumbManager $breadcrumbManager,
        DepositoManager $depositoManager,
        OrdenTrabajoRouter $ordentrabajoRouter,
        TareaOtManager $tareaotManager,
        TareaMantManager $tareamantManager,
        ProductoManager $productoManager,
        StockManager $stockManager,
        HoraTallerManager $horatallerManager,
        MecanicoManager $mecanicoManager,
        SectorTallerManager $sectorManager,
        UserLoginManager $userloginManager,
        ExcelManager $excelManager
    ) {
        $this->tallerManager = $tallerManager;
        $this->servicioManager = $servicioManager;
        $this->gruposervicioManager = $gruposervicioManager;
        $this->centrocostoManager = $centrocostoManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->depositoManager = $depositoManager;
        $this->peticionManager = $peticionManager;
        $this->tareamantManager = $tareamantManager;
        $this->ordentrabajoRouter = $ordentrabajoRouter;
        $this->stockManager = $stockManager;
        $this->productoManager = $productoManager;
        $this->horatallerManager = $horatallerManager;
        $this->tareaotManager = $tareaotManager;
        $this->utilsManager = $utilsManager;
        $this->mecanicoManager = $mecanicoManager;
        $this->sectorManager = $sectorManager;
        $this->userloginManager = $userloginManager;
        $this->excelManager = $excelManager;
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/list", name="ordentrabajo_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de OT");
        $costoTotal = 0;
        $fecha = date('Y-m-j');
        $fecha_desde = date('d/m/Y', strtotime('-7 day', strtotime($fecha)));
        $fecha_hasta = date('d/m/Y', time());
        $talleres = $this->tallerManager->find($organizacion);
        $choice_sector = array();
        $choice_sector[0] = 'Todos';
        $countCerradas = $countEnCurso = $countAbiertas = 0;
        foreach ($talleres as $taller) {
            foreach ($taller->getSectores() as $sector) {
                $choice_sector[$taller->getNombre()][$sector->getId()] = $sector->getNombre();
            }
        }
        $options = array(
            'servicios' => $this->servicioManager->findAllByOrganizacion($organizacion),
            'grupos' => $this->gruposervicioManager->findAsociadas($organizacion),
            'sectores' => $choice_sector,
            'talleres' => $talleres,
            'depositos' => $this->depositoManager->findAllByOrganizacion($organizacion),
            'centrocosto' => $this->centrocostoManager->findAllByOrganizacion($organizacion)
        );
        $form = $this->createForm(HistoricoOrdenTrabajoType::class, null, $options);
        $ordenes = $this->ordentrabajoManager->findAll($organizacion, null, (new \DateTime())->modify('-7 days'), new \DateTime());

        foreach ($ordenes as $orden) {
            $costoTotal += $orden->getCostoTotal();
            if ($orden->getEstado() == 0 || $orden->getEstado() == null) {
                $countAbiertas++;
            } elseif ($orden->getEstado() == 1) {
                $countEnCurso++;
            } else {
                $countCerradas++;
            }
        }

        $abiertas = number_format($countAbiertas > 0 ? ($countAbiertas / count($ordenes)) * 100 : 0, 2);
        $cerradas = number_format($countCerradas > 0 ? ($countCerradas / count($ordenes)) * 100 : 0, 2);
        $enCurso = number_format($countEnCurso > 0 ? ($countEnCurso / count($ordenes)) * 100 : 0, 2);
        //die('////<pre>' . nl2br(var_export($enCurso, true)) . '</pre>////');
        $consulta['fecha_desde'] = $fecha_desde;
        $consulta['fecha_hasta'] = $fecha_hasta;
        $consulta['sector'] = 'Todos';
        $consulta['tipo'] = 'Todos';
        $consulta['deposito'] = 'Todos';
        $consulta['servicio'] = 'Todos';

        return $this->render('mante/OrdenTrabajo/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'ordenes' => $this->ordentrabajoManager->toArray($ordenes),
            'organizacion' => $organizacion,
            'form' => $form->createView(),
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'costoTotal' => $costoTotal,
            'consulta' => $consulta,
            'abiertas' => $abiertas,
            'cerradas' => $cerradas,
            'enCurso' => $enCurso,
            'filtro' => 'Filtro: default',
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array(
            1 => array($this->ordentrabajoRouter->btnNew($organizacion)),
        );
        return $this->ordentrabajoRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/new", name="ordentrabajo_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        //creo la orden        
        $orden = $this->ordentrabajoManager->create($organizacion);
        if (!is_null($request->get('peticion'))) {
            $peticion = $this->peticionManager->find($request->get('peticion'));
            $orden->addPeticion($peticion);
        }
        if ($request->get('servicio')) {
            $options['servicios'] = array($this->servicioManager->find($request->get('servicio')));
        } else {
            $options['servicios'] = $this->servicioManager->findAllByOrganizacion($organizacion);
        }
        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getEntityManager();

        $form = $this->createForm(OrdenTrabajoType::class, $orden, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->ordentrabajoManager->save($orden)) {
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('ordentrabajo_step2', array('id' => $orden->getId())));
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Nueva Orden");
        return $this->render('mante/OrdenTrabajo/new.html.twig', array(
            'orden' => $orden,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/{idservicio}/newservicio", name="ordentrabajo_newservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_AGREGAR")
     */
    public function newservicioAction(Request $request, Organizacion $organizacion, $idservicio)
    {
        //creo la orden        
        $servicio = $this->servicioManager->find(intval($idservicio));
        $orden = $this->ordentrabajoManager->create($organizacion);
        if (!is_null($request->get('peticion'))) {
            $peticion = $this->peticionManager->find($request->get('peticion'));
            $orden->addPeticion($peticion);
        }
        $options['organizacion'] = $organizacion;
        $options['servicio'] = $servicio;
        $options['em'] = $this->getEntityManager();

        $form = $this->createForm(OrdenTrabajoServicioType::class, $orden, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $orden->setServicio($servicio);
            if ($this->ordentrabajoManager->save($orden)) {
                $this->breadcrumbManager->pop();
                return $this->redirect($this->generateUrl('ordentrabajo_step2', array('id' => $orden->getId())));
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Nueva Orden");
        return $this->render('mante/OrdenTrabajo/newservicio.html.twig', array(
            'orden' => $orden,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/step2", name="ordentrabajo_step2")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_AGREGAR")
     */
    public function step2Action(Request $request, OrdenTrabajo $orden)
    {

        $organizacion = $orden->getServicio()->getOrganizacion();
        $formProducto = $this->createForm(OrdenTrabajoProductoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));
        $formMecanico = $this->createForm(OrdenTrabajoMecanicoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));
        $formExterno = $this->createForm(OrdenTrabajoExternoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));
        $formHorasTaller = $this->createForm(OrdenTrabajoHorasTallerType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));

        $formTareasOt = $this->createForm(OrdenTrabajoTareaType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager()
        ));

        $formCerrar = $this->createForm(OrdenTrabajoCerrarType::class, null);

        $formEditTareasOt = $this->createForm(\App\Form\mante\TareaOtEditType::class);

        if ($orden->getEstado() != 2) {
            $menu = array(
                1 => array($this->ordentrabajoRouter->btnCerrar($orden)),
                2 => array($this->ordentrabajoRouter->btnEdit($orden)),
                3 => array($this->ordentrabajoRouter->btnTareaOt($orden)),
                4 => array($this->ordentrabajoRouter->btnInsumo($orden)),
                5 => array($this->ordentrabajoRouter->btnTaller($orden)),
                6 => array($this->ordentrabajoRouter->btnMecanico($orden)),
                7 => array($this->ordentrabajoRouter->btnProveedor($orden)),
                8 => array($this->ordentrabajoRouter->btnPeticion($orden)),
            );
        }
        $menu[9] = array($this->ordentrabajoRouter->btnDelete($orden));
        $menu[10] = array($this->ordentrabajoRouter->btnExport($orden));
        $d = array();
        if ($orden->getTipo() == 1) {   //entro solo para los preventivos.
            $tmant = $this->tareamantManager->findByServicio($orden->getServicio());
            foreach ($tmant as $tarea) {
                $status = $this->tareamantManager->analizeMantenimiento($tarea);

                //recorro las actividades de los mants del servicio.
                $acts = array();
                foreach ($tarea->getMantenimiento()->getActividades() as $act) {
                    $acts[$act->getId()] = array(
                        'actividad' => $act,
                        'producto' => $this->tareamantManager->getProductoServicio($act, $orden->getServicio()),
                    );
                }

                $d[] = array(
                    'id' => $tarea->getId(),
                    'plan' => $tarea,
                    'actividades' => $acts,
                    'status' => $status,
                );
            }
        }

        $peticiones = array();
        $ids = array();
        foreach ($orden->getPeticiones() as $pet) {
            $ids[] = $pet->getId();
        }
        foreach ($orden->getServicio()->getPeticiones() as $pet) {
            if (!in_array($pet->getId(), $ids)) {
                if (!$pet->getEstado()->getIsCerrado()) {
                    $peticiones[] = $pet;
                }
            }
        }
        $actividades = array();
        
        if($orden->getTipo() == 1){
            foreach ($orden->getMantenimientos() as $otMant) {
                foreach ($otMant->getServicioMantenimiento()->getMantenimiento()->getActividades() as $act) {
                    $actividades[] = $act;
                }
            }
        }   

        $this->breadcrumbManager->push($request->getRequestUri(), sprintf("Orden Trabajo %s", $orden->getFormatIdentificador()));
        return $this->render('mante/OrdenTrabajo/step2.html.twig', array(
            'menu' => $this->ordentrabajoRouter->toCalypso($menu),
            'orden' => $orden,
            'peticiones' => $peticiones,
            'productos' => $this->getEntityManager()->getRepository('App:Producto')->byOrganizacion($organizacion),
            'mantenimientos' => $d,
            'formProducto' => $formProducto->createView(),
            'formMecanico' => $formMecanico->createView(),
            'formExterno' => $formExterno->createView(),
            'formHorasTaller' => $formHorasTaller->createView(),
            'formTareasOt' => $formTareasOt->createView(),
            'formCerrar' => $formCerrar->createView(),
            'formEditTareasOt' => $formEditTareasOt->createView(),
            'delete_form' => $this->createDeleteForm($orden->getId())->createView(),
            'actividades' => $actividades,
        ));
    }

    /**
     */

    /**
     * @Route("/mante/ordentrabajo/{id}/edit", name="ordentrabajo_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function editAction(Request $request, OrdenTrabajo $orden)
    {
        $fechaOld = $orden->getFecha();
        if (!$orden) {
            throw $this->createNotFoundException('Código de Evento no encontrado.');
        }
        $organizacion = $orden->getServicio()->getOrganizacion();
        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getEntityManager();

        $form = $this->createForm(OrdenTrabajoType::class, $orden, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->ordentrabajoManager->save($orden)) {
                if ($fechaOld != $orden->getFecha() && $orden->getTipo() == 1) {
                    foreach ($orden->getMantenimientos() as $otMante) {
                        if ($otMante->getServicioMantenimiento()->getMantenimiento()->getPorDias()) {
                            if ($this->ordentrabajoManager->addMantenimiento($orden, $otMante->getServicioMantenimiento(), 'Se Modifica')) {
                                $this->setFlash('success', 'La orden de trabajo se actualizo correctamente');
                                $this->breadcrumbManager->pop();
                                return $this->redirect($this->generateUrl('ordentrabajo_step2', array('id' => $orden->getId())));
                            } else {
                                $this->setFlash('error', 'Error al crear la orden de trabajo');
                            }
                        }
                    }
                }
                $this->breadcrumbManager->pop();
                $this->setFlash('success', sprintf('Se ha actualizado la órden Nro <b>%d</b>.', $orden->getId()));
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Nueva Orden");
        return $this->render('mante/OrdenTrabajo/edit.html.twig', array(
            'orden' => $orden,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/ordentrabajo/addproducto", name="ordentrabajo_addproducto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addProductoAction(Request $request)
    {

        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);
        $tmp = array();
        parse_str($request->get('formProducto'), $tmp);
        $dataProd = $tmp['producto'];
        $actPrecio = isset($dataProd['actualizar']) ? true : false;
        $unitario = $actPrecio ? floatval($dataProd['unitario']) : null;
        $producto = $this->productoManager->find($dataProd['producto']);
        if ($orden->getDeposito() != null) {
            if ($dataProd['unitario'] == '---') {
                $dataProd['unitario'] = 0;
            }
            if ($dataProd['cantidad'] == '---') {
                $dataProd['cantidad'] = 1;
            }
            $dataProd['unitario'] =  str_replace(",", ".", $dataProd['unitario']);
            //  die('////<pre>' . nl2br(var_export($dataProd['unitario'], true)) . '</pre>////');
            $stock = $this->stockManager->restarStock($orden->getDeposito(), $producto, intval($dataProd['cantidad']), $unitario);
            $x = $this->ordentrabajoManager->addProducto($orden, $dataProd['producto'], intval($dataProd['cantidad']), floatval($dataProd['unitario']), $actPrecio);
        } else {
            return new Response(json_encode(array('error_deposito' => 'error_deposito')));
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/productos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delproducto", name="ordentrabajo_delproducto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delProductoAction(Request $request)
    {

        $idOTP = $request->get('idProd');   //es el id del ordentrabajoproducto
        $idOT = $request->get('id');
        $idDep = $request->get('idDep');
        $deposito = $this->depositoManager->find($idDep);
        $orden = $this->ordentrabajoManager->find($idOT);
        $productoOT = $this->ordentrabajoManager->findProdOT($idOTP);
        $stock = $this->stockManager->agregar($deposito, $productoOT->getProducto(), intval($request->get('cantidad')));
        $this->ordentrabajoManager->delProducto($orden, $productoOT);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/productos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/addhorataller", name="ordentrabajo_addhorataller")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addHoraTallerAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $tmp = array();
        parse_str($request->get('formHoraTaller'), $tmp);
        $dataHora = $tmp['hora'];
        $dataHora['unitario'] = str_replace(",", ".", $dataHora['unitario']);
        $actPrecio = isset($dataHora['actualizar']) ? true : false;
        $hora = $this->horatallerManager->find($dataHora['horaTaller']);
        //  die('////<pre>' . nl2br(var_export($dataHora['unitario'], true)) . '</pre>////');
        $x = $this->ordentrabajoManager->addHoraTaller($orden, $dataHora['horaTaller'], intval($dataHora['cantidad']), floatval($dataHora['unitario']), $actPrecio);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/horasTaller.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delHoraTaller", name="ordentrabajo_delhorataller")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delHoraTallerAction(Request $request)
    {
        $idOTHT = $request->get('idHora');   //es el id del ordentrabajoproducto
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);
        $horatallerOT = $this->ordentrabajoManager->findHoraTallerOT($idOTHT);
        $this->ordentrabajoManager->delHoraTaller($orden, $horatallerOT);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/horasTaller.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/addmecanico", name="ordentrabajo_addmecanico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addMecanicoAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $tmp = array();
        parse_str($request->get('formMecanico'), $tmp);
        $dataMec = $tmp['mecanico'];

        $x = $this->ordentrabajoManager->addMecanico($orden, $dataMec['mecanico'], intval($dataMec['cantidad']));

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/mecanicos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delmecanico", name="ordentrabajo_delmecanico", methods={"POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delMecanicoAction(Request $request)
    {
        $idOTM = $request->get('idMeca');   //es el id del ordentrabajoproducto

        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $this->ordentrabajoManager->delMecanico($orden, $idOTM);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/mecanicos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/delete", name="ordentrabajo_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @IsGranted("ROLE_ORDENTRABAJO_ELIMINAR")
     */
    public function deleteAction(Request $request, OrdenTrabajo $orden)
    {
        $form = $this->createDeleteForm($orden->getId());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->ordentrabajoManager->delete($orden);
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/mante/ordentrabajo/addmantenimiento", name="ordentrabajo_addmantenimiento")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addMantenimientoAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $tmp = array();
        parse_str($request->get('form'), $tmp);
        $dataMant = isset($tmp['plan']) ? $tmp['plan'] : null;
        $dataProd = isset($tmp['cant']) ? $tmp['cant'] : null;

        //grabar los mantenimientos en la orden
        if ($dataMant) {
            $horometro = $dataMant['horometro'] == '' ? null : intval($dataMant['horometro']);
            $odometro = $dataMant['odometro'] == '' ? null : intval($dataMant['odometro']);
            if (!is_null($horometro)) {
                $orden->setHorometro($horometro);
            }
            if (!is_null($odometro)) {
                $orden->setOdometro($odometro);
            }

            foreach ($dataMant as $id => $mant) {
                if ($id != 'horometro' && $id != 'odometro') {
                    $tarea = $this->tareamantManager->find($id);
                    $x = $this->ordentrabajoManager->addMantenimiento($orden, $tarea, 'Se Crea');
                }
            }
        }

        //grabar los productos segun los mantenimientos.
        if ($dataProd) {
            foreach ($dataProd as $id => $cant) {
                $this->ordentrabajoManager->addProducto($orden, $id, intval($cant), 0, false);
            }
        }

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/mantenimientos.html.twig', array(
                'orden' => $orden
            )),
            'productos' => $this->renderView('mante/OrdenTrabajo/productos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delmantenimiento", name="ordentrabajo_delmantenimiento")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delMantenimientoAction(Request $request)
    {
        $idOTM = $request->get('idMant');   //es el id del ordentrabajoproducto

        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $this->ordentrabajoManager->delMantenimiento($orden, $idOTM);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/mantenimientos.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/addexterno", name="ordentrabajo_addexterno")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addExternoAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $tmp = array();
        parse_str($request->get('formExterno'), $tmp);
        $dataExt = isset($tmp['externo']) ? $tmp['externo'] : null;
        //grabar los productos segun los mantenimientos.
        if ($dataExt) {
            $this->ordentrabajoManager->addTallerExternoProducto(
                $orden,
                intval($dataExt['tallerExterno']),
                intval($dataExt['cantidad']),
                $dataExt['descripcion'],
                intval($dataExt['costoTotal']),
                $dataExt['producto']
            );
        }

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/terceros.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delexterno", name="ordentrabajo_delexterno")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delExternoAction(Request $request)
    {
        $idOTE = $request->get('idExt');   //es el id del ordentrabajoproducto
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $this->ordentrabajoManager->delTallerExterno($orden, $idOTE);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/terceros.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/addpeticion", name="ordentrabajo_addpeticion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addPeticionAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);
        $tmp = array();
        parse_str($request->get('formPeticion'), $tmp);
        if ($tmp) {
            $peticion = $this->ordentrabajoManager->addPeticion($orden, $tmp);
        }

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/peticiones.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/delpeticion", name="ordentrabajo_delpeticion")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delPeticionAction(Request $request)
    {
        $idOTE = $request->get('idExt');   //es el id del ordentrabajoproducto
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $this->ordentrabajoManager->delPeticion($orden, $idOTE);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/terceros.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/addtarea", name="ordentrabajo_addtarea")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addTareasAction(Request $request)
    {
        $idOT = $request->get('id');
        $orden = $this->ordentrabajoManager->find($idOT);

        $tmp = array();
        parse_str($request->get('formTareasOt'), $tmp);
        $descripcion = $tmp['tarea']['descripcion'];
        $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($tmp['tarea']['fecha'], true));
        $mecanicos = isset($tmp['tarea']['mecanico']) ? $tmp['tarea']['mecanico'] : array();

        $tarea = $this->tareaotManager->create($orden);
        $tarea->setDescripcion($descripcion);
        $tarea->setFecha($fecha);
        $tarea->setEstado(0); // estado nueva
        $this->tareaotManager->save($tarea);

        foreach ($mecanicos as $mec) {
            $mecanico = $this->mecanicoManager->find($mec);
            $tpm = new TareaOtMecanico();
            $tpm->setMecanico($mecanico);
            $tpm->setTarea($tarea);
            $tarea->addMecanicos($tpm);
            $mecanico->addTareas($tpm);
            $this->getEntityManager()->persist($tpm);
            $this->getEntityManager()->flush();
        }
        $actividades = array();
        
        if($orden->getTipo() == 1){
            foreach ($orden->getMantenimientos() as $otMant) {
                foreach ($otMant->getServicioMantenimiento()->getMantenimiento()->getActividades() as $act) {
                    $actividades[] = $act;
                }
            }
        }

        $estadoOrden = $this->ordentrabajoManager->cambiarEstado($orden);
        
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                'orden' => $orden,
                'actividades' => $actividades
            )),
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/mante/ordentrabajo/historico", name="ordentrabajo_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_VER")
     */
    public function historicoAction(Request $request)
    {

        $tmp = array();
        $ordenes = array();
        $ots = array();
        $consulta = array();
        parse_str($request->get('formHistOT'), $tmp);
        $form = $tmp['historico'];
        //   die('////<pre>' . nl2br(var_export($form, true)) . '</pre>////');
        $organizacion = $this->userloginManager->getOrganizacion();
        if ($form) {
            $ok = $this->utilsManager->isValidDesdeHasta('d/m/Y', $form['fecha_desde'], $form['fecha_hasta']);
            if ($ok) {    //el formato esta correcto
                $fecha_desde = $form['fecha_desde'];
                $fecha_hasta = $form['fecha_hasta'];
                $consulta['fecha_desde'] = $fecha_desde;
                $consulta['fecha_hasta'] = $fecha_hasta;
                $tipo = ($form['tipo']) < 0 ? null : $form['tipo'];
                $strTipo = '';
                if ($form['tipo'] < 0) {
                    $strTipo = 'C y P';
                } elseif ($form['tipo'] == 0) {
                    $strTipo = 'Correctivo';
                } elseif ($form['tipo'] == 1) {
                    $strTipo = 'Preventivo';
                }
                $consulta['tipo'] = $strTipo;

                if ($form['servicio'] == '0') {  //es para toda la flota.
                    $servicios = $this->servicioManager->findAllByOrganizacion($organizacion);
                    $strServ = 'Todos los Servicios';
                } else {
                    if (substr($form['servicio'], 0, 1) == 'g') {   //es para un grupo de servicios
                        $grupo = $this->gruposervicioManager->find(intval(substr($form['servicio'], 1)));
                        $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                        $strServ = 'Grupos ' . $grupo->getNombre();
                    } else {
                        $serv = $this->servicioManager->find(intval($form['servicio']));
                        $servicios = array($serv);
                        $strServ = $serv->getNombre();
                    }
                }
                $consulta['servicio'] = $strServ;

                if (($form['sector']) > 0) {
                    $sector = $this->sectorManager->findOne($form['sector']);
                    $strSector = $sector->getNombre();
                } else {
                    $sector = null;
                    $strSector = 'Todos sector';
                }
                $consulta['sector'] = $strSector;

                if (isset($form['deposito']) && ($form['deposito']) > 0) {
                    $deposito = $this->depositoManager->find($form['deposito']);
                    $strDeposito = $deposito->getNombre();
                } else {
                    $deposito = null;
                    $strDeposito = 'Todos Deposito';
                }
                $consulta['deposito'] = $strDeposito;

                if (isset($form['deposito']) && ($form['centrocosto']) > 0) {
                    $centrocosto = $this->centrocostoManager->find($form['centrocosto']);
                    $strCentrocosto = $centrocosto->getNombre();
                } else {
                    $centrocosto = null;
                    $strCentrocosto = 'Todos C.Costos';
                }
                $consulta['centrocosto'] = $strCentrocosto;

                if (isset($form['deposito']) && ($form['taller']) > 0) {
                    $taller = $this->tallerManager->findOne(intval($form['taller']));
                    $strTaller = $taller->getNombre();
                } else {
                    $taller = null;
                    $strTaller = 'Todos Talleres';
                }
                $consulta['taller'] = $strTaller;
                $costoTotal = 0;
                foreach ($servicios as $servicio) {
                    $ots[] = $this->ordentrabajoManager->historico(
                        $organizacion,
                        $this->utilsManager->datetime2sqltimestamp($fecha_desde . ' 00:00:00'),
                        $this->utilsManager->datetime2sqltimestamp($fecha_hasta . ' 23:59:59'),
                        $servicio,
                        $tipo,
                        $sector,
                        $deposito,
                        $centrocosto,
                        $taller
                    );
                }
                foreach ($ots as $key => $value) {
                    foreach ($value as $ot) {
                        $ordenes[] = $ot;
                    }
                }
            }
            foreach ($ordenes as $orden) {
                $costoTotal += $orden->getCostoTotal();
            }

            $consulta['fecha_desde'] = $fecha_desde;
            $consulta['fecha_hasta'] = $fecha_hasta;
            $consulta['sector'] = 'Todos';
            $consulta['tipo'] = 'Todos';
            $consulta['taller'] = 'Todos';
            $consulta['deposito'] = 'Todos';
            $consulta['servicio'] = 'Todos';
            $consulta['centrocosto'] = 'Todos';

            //decode filtro
            $filtro = sprintf(
                'Del <b>%s</b> al <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b> | <b>%s</b>',
                $fecha_desde,
                $fecha_hasta,
                $strTipo,
                $strServ,
                $strSector,
                $strDeposito,
                $strCentrocosto,
                $strTaller
            );
            $arr = $this->ordentrabajoManager->toArray($ordenes);
            return new Response(json_encode(array(
                'html' => $this->renderView('mante/OrdenTrabajo/ordenes.html.twig', array(
                    'ordenes' => $arr,
                    'costoTotal' => $costoTotal
                )),
                'ordenes' => json_encode($arr),
                'costoTotal' => json_encode($costoTotal),
                'filtro' => $filtro,
                'consulta' => json_encode($consulta)
            )));
        }
    }

    /**
     * @Route("/mante/ordentrabajo/exportar", name="ordentrabajo_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_VER")
     */
    public function exportarAction(Request $request)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $informe = json_decode($request->get('informe'), true);
        $costoTotal = json_decode($request->get('costoTotal'), true);
        if (isset($consulta) && isset($informe)) {
            $i = 10;
            //creo el xls
            $xls = $this->excelManager->create("Informe de Odenes de Trabajo");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicio'],
                'C3' => 'Fecha desde',
                'D3' => $consulta['fecha_desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['fecha_hasta'],
                'C5' => 'Tipo',
                'D5' => $consulta['tipo'],
                'C6' => 'Sector',
                'D6' => $consulta['sector'],
                'C7' => 'Depósito',
                'D7' => $consulta['deposito'],
            ));

            $xls->setBar(9, array(
                'A' => array('title' => 'Fecha', 'width' => 20),
                'B' => array('title' => 'ID', 'width' => 10),
                'C' => array('title' => 'Interno', 'width' => 10),
                'D' => array('title' => 'Externo', 'width' => 10),
                'E' => array('title' => 'Tipo', 'width' => 10),
                'F' => array('title' => 'Servicio', 'width' => 20),
                'G' => array('title' => 'Mecánico', 'width' => 30),
                'H' => array('title' => 'Horas Mec', 'width' => 10),
                'I' => array('title' => 'Costo Mec', 'width' => 10),
                'J' => array('title' => 'Importe', 'width' => 10),
                'K' => array('title' => 'Descripción', 'width' => 30),
            ));
            foreach ($informe as $key => $value) {
                $f = new \DateTime($value['fecha']['date']);

                $xls->setRowValues($i, array(
                    'A' => array('value' => $f->format('d/m/Y')),
                    'B' => array('value' => $value['identificador']),
                    'C' => array('value' => $value['numeroInterno']),
                    'D' => array('value' => $value['numeroExterno']),
                    'E' => array('value' => $value['tipo']),
                    'F' => array('value' => $value['servicio']),
                    'G' => array('value' => $value['mecanico']),
                    'H' => array('value' => !is_null($value['horasMecanico']) ? $value['horasMecanico'] : '0', 'format' => '0.00'),
                    'I' => array('value' => !is_null($value['costoMecanico']) ? $value['costoMecanico'] : '0', 'format' => '0.00'),
                    'J' => array('value' => !is_null($value['costoTotal']) ? $value['costoTotal'] : '0', 'format' => '0.00'),
                    'K' => array('value' => $value['descripcion']),
                ));
                $i++;
            }
            $xls->setRowValues($i, array(
                'H' => array('value' => 'Cantidad de Ordenes: ' . count($informe)),
                'I' => array('value' => 'Costo Total: ' . $costoTotal),
            ));

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=ordenestrabajo.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('ordentrabajo_list'));
        }
    }

    /**
     * @Route("/mante/ordentrabajo/cerrar", name="ordentrabajo_cerrar", methods={"GET","POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function cerrarAction(Request $request)
    {
        $idOT = $request->get('id');
        $fecha = $request->get('fecha') != '' ? new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('fecha'), true)) : new \DateTime();
        $orden = $this->ordentrabajoManager->find($idOT);
        $orden->setEstado(2);
        $orden->setFechaCierre($fecha);    //es la fecha que carga el operador
        $orden->setCloseAt(new \DateTime());
        $motivoPeticion = sprintf('Se cierra orden de trabajo Nro. %s', $orden->getFormatIdentificador());
        foreach ($orden->getPeticiones() as $peticion) {
            $this->peticionManager->setCerrada($peticion, $fecha, $motivoPeticion);
        }
        $save = $this->ordentrabajoManager->save($orden);

        if ($save) {
            foreach ($orden->getTareas() as $tarea) {
                if ($tarea->getEstado() != 2) { //cierro todas las tareas ya que se cierrra la ot
                    $this->tareaotManager->cerrar($tarea);
                }
            }
        }
        $this->ordentrabajoManager->recalcularCostoOrden($orden);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                'orden' => $orden
            )),
            'htmlEstado' => $this->renderView('mante/OrdenTrabajo/otestado.html.twig', array(
                'orden' => $orden
            )),
        )));
    }

    /**
     * @Route("/mante/ordentrabajo/{id}/export", name="ordentrabajo_export",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @IsGranted("ROLE_ORDENTRABAJO_VER")
     */
    public function exportAction(Request $request, OrdenTrabajo $ordenTrabajo, Pdf $snappy)
    {

        $filename = sprintf('ordenTrabajo%d.pdf', $ordenTrabajo->getId());
        $html = $this->renderView('mante/ExportOrdenTrabajo/export.html.twig', array(
            'orden' => $ordenTrabajo,
        ));
        //return new Response($html);
        return new Response(
            $snappy->getOutputFromHtml($html, array(
                //'orientation' => 'Landscape',
                'default-header' => false
            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $filename,
                'charset' => 'UTF-8',
            )
        );
    }

    /**
     * @Route("/mante/ordentrabajo/deltareaot", name="ordentrabajo_deltareaot")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delTareaotAction(Request $request)
    {
        $idOT = intval($request->get('id'));
        $idTarea = intval($request->get('idTarea'));
        // die('////<pre>' . nl2br(var_export($idTarea, true)) . '</pre>////');
        $tarea = $this->tareaotManager->deleteById($idTarea);
        $orden = $this->ordentrabajoManager->find($idOT);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                'orden' => $orden
            )),
            'costoNeto' => number_format($orden->getCostoNeto(), 2),
            'costoTotal' => number_format($orden->getCostoTotal(), 2),
        )));
    }
}
