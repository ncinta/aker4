<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Entity\Organizacion;
use App\Entity\Deposito;
use App\Form\mante\DepositoType;
use App\Form\mante\StockType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DepositoManager;
use App\Model\app\StockManager;
use App\Model\app\Router\DepositoRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DepositoController extends AbstractController
{

    /**
     * @Route("/mante/deposito/{idorg}/list", name="deposito_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"})
     * @IsGranted("ROLE_APMON_STOCK_ADMIN")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        DepositoManager $depositoManager,
        DepositoRouter $depositoRouter
    ) {

        $menu = array(
            1 => array($depositoRouter->btnNew($organizacion)),
        );
        $menu = $depositoRouter->toCalypso($menu);

        $depositos = $depositoManager->findAllByOrganizacion($organizacion->getId());
        $breadcrumbManager->push($request->getRequestUri(), "depositos");

        return $this->render('mante/Deposito/list.html.twig', array(
            'menu' => $menu,
            'depositos' => $depositos,
        ));
    }

    /**
     * @Route("/mante/deposito/{id}/show", name="deposito_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_STOCK_ADMIN")
     */
    public function showAction(
        Request $request,
        Deposito $deposito,
        BreadcrumbManager $breadcrumbManager,
        StockManager $stockManager,
        DepositoRouter $depositoRouter
    ) {

        $deleteForm = $this->createDeleteForm($deposito->getId());
        if (!$deposito) {
            throw $this->createNotFoundException('Código de Deposito no encontrado.');
        }
        $options['organizacion'] = $deposito->getOrganizacion();
        $formStock = $this->createForm(StockType::class, null, $options);
        $stocks = $stockManager->findByDeposito($deposito);

        $breadcrumbManager->push($request->getRequestUri(), $deposito->getNombre());
        return $this->render('mante/Deposito/show.html.twig', array(
            'menu' => $this->getShowMenu($deposito, $depositoRouter),
            'deposito' => $deposito,
            'stocks' => $stocks,
            'delete_form' => $deleteForm->createView(),
            'formStock' => $formStock->createView()
        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($deposito, $depositoRouter)
    {

        $menu = array(
            1 => array($depositoRouter->btnEdit($deposito)),
            2 => array($depositoRouter->btnAddStock($deposito)),
            3 => array($depositoRouter->btnDelete($deposito))
        );
        return $depositoRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/deposito/{id}/delete", name="deposito_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_STOCK_ADMIN")
     */
    public function deleteAction(
        Request $request,
        Deposito $deposito,
        BreadcrumbManager $breadcrumbManager,
        DepositoManager $depositoManager
    ) {

        $form = $this->createDeleteForm($deposito->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $breadcrumbManager->pop();

            if ($depositoManager->deleteById($deposito->getId())) {
                $this->setFlash('success', 'Deposito eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el deposito del sistema.');
            }
        }

        return $this->redirect($this->generateUrl('deposito_list', array(
            'idorg' => $deposito->getOrganizacion()->getId(),
        )));
    }

    /**
     * @Route("/mante/deposito/{idorg}/new", name="deposito_new",
     *     requirements={
     *         "idorg": "\d+",
     *     },
     * )
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"})
     * @IsGranted("ROLE_APMON_STOCK_ADMIN")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        DepositoManager $depositoManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el depos
        $deposito = $depositoManager->create($organizacion);

        $form = $this->createForm(DepositoType::class, $deposito);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                if ($depositoManager->save($deposito)) {
                    //el servicio q se esta creando es un vehiculo
                    //    $this->setFlash('success', sprintf('El deposito se ha creado con éxito'));

                    return $this->redirect($breadcrumbManager->getVolver());
                }
            }
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Deposito");
        return $this->render('mante/Deposito/new.html.twig', array(
            'organizacion' => $organizacion,
            'deposito' => $deposito,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/deposito/{id}/edit", name="deposito_edit",
     *     requirements={
     *         "id": "\d+",
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_STOCK_ADMIN")
     */
    public function editAction(
        Request $request,
        Deposito $deposito,
        DepositoManager $depositoManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        if (!$deposito) {
            throw $this->createNotFoundException('Código de Depósito no encontrado.');
        }

        $form = $this->createForm(DepositoType::class, $deposito);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $breadcrumbManager->pop();

                if ($depositoManager->save($deposito)) {
                    //el servicio q se esta creando es un vehiculo
                    //  $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                    return $this->redirect($breadcrumbManager->getVolver());
                }
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar Deposito");
        return $this->render('mante/Deposito/edit.html.twig', array(
            'deposito' => $deposito,
            'form' => $form->createView(),
        ));
    }
}
