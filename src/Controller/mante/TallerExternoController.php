<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\mante\TallerExternoType;
use App\Entity\TallerExterno;
use App\Entity\Organizacion;
use App\Model\app\Router\TallerExternoRouter;
use App\Model\app\TallerExternoManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\OrganizacionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of TallerController
 *
 * @author nicolas
 */
class TallerExternoController extends AbstractController
{

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, TallerExternoRouter $tallerexternoRouter)
    {
        $menu = array(
            1 => array($tallerexternoRouter->btnNew($organizacion))
        );
        return $tallerexternoRouter->toCalypso($menu);
    }

    private function getShowMenu($tallerexterno, TallerExternoRouter $tallerexternoRouter)
    {
        $menu = array(
            1 => array($tallerexternoRouter->btnEdit($tallerexterno)),
            1 => array($tallerexternoRouter->btnDelete($tallerexterno))
        );
        return $tallerexternoRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/tallerexterno/{id}/list", name="tallerexterno_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoRouter $tallerExternoRouter,
        TallerExternoManager $tallerexternoManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Talleres Externos");
        $talleresexternos = $tallerexternoManager->find($organizacion);

        return $this->render('mante/TallerExterno/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion, $tallerExternoRouter),
            'talleres' => $talleresexternos,
        ));
    }

    /**
     * @Route("/mante/tallerexterno/{idorg}/new", name="tallerexterno_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"}))
     * @IsGranted("ROLE_TALLER_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoManager $tallerexternoManager
    ) {

        $tallerexterno = $tallerexternoManager->create($organizacion);
        $form = $this->createForm(TallerExternoType::class, $tallerexterno);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tallerexterno = $tallerexternoManager->save($tallerexterno);
            $this->setFlash('success', sprintf('El taller externo se ha creado con éxito'));
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Taller");
        return $this->render('mante/TallerExterno/new.html.twig', array(
            'organizacion' => $organizacion,
            'taller' => $tallerexterno,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/tallerexterno/{id}/edit", name="tallerexterno_edit",
     *     requirements={
     *         "id": "\d+",
     *     }),
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function editAction(
        Request $request,
        TallerExterno $tallerexterno,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoManager $tallerexternoManager
    ) {

        $form = $this->createForm(TallerExternoType::class, $tallerexterno);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tallerexterno = $tallerexternoManager->save($tallerexterno);
            $this->setFlash('success', sprintf('El tallerexterno se ha modificado con éxito'));
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Editar Taller");
        return $this->render('mante/TallerExterno/edit.html.twig', array(
            'taller' => $tallerexterno,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/tallerexterno/{id}/show", name="tallerexterno_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_VER")
     */
    public function showAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoManager $tallerexternoManager,
        TallerExternoRouter $tallerExternoRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Ver Taller");
        $tallerexterno = $tallerexternoManager->findOne($id);
        return $this->render('mante/TallerExterno/show.html.twig', array(
            'taller' => $tallerexterno,
            'menu' => $this->getShowMenu($tallerexterno, $tallerExternoRouter),
            'delete_form' => $this->createDeleteForm($id)->createView(),
        ));
    }

    /**
     * @Route("/mante/tallerexterno/{id}/delete", name="tallerexterno_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        TallerExternoManager $tallerexternoManager
    ) {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();
            if ($tallerexternoManager->deleteById($id)) {
                $this->setFlash('success', 'Taller eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el tallerexterno del sistema.');
            }
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    /**
     * @Route("/mante/tallerexterno", name="tallerexterno")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_VER")
     */
    public function getAction(Request $request, TallerExternoManager $tallerexternoManager, OrganizacionManager $organizacionManager)
    {
        $idOrg = $request->get('idOrg');
        $search = $request->get('search');
        $organizacion = $organizacionManager->find($idOrg);
        //recargo los talleres y devuelvo
        $productos = $tallerexternoManager->findAllQuery($organizacion, $search);

        $str = array();
        foreach ($productos as $prod) {
            $str[] = array('id' => $prod->getId(), 'text' => $prod->getNombre());
        }

        return new Response(json_encode(array('results' => $str)), 200);
    }
}
