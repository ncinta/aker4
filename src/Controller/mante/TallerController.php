<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Form\mante\TallerType;
use App\Form\mante\TallerSectorType;
use App\Entity\TallerSector as TallerSector;
use App\Entity\Taller as Taller;
use App\Entity\Organizacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Model\app\BreadcrumbManager;
use App\Model\app\TallerManager;
use App\Model\app\Router\TallerRouter;
use App\Model\app\SectorTallerManager;
use App\Model\app\UserLoginManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of TallerController
 *
 * @author nicolas
 */
class TallerController extends AbstractController
{

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion, TallerRouter $tallerRouter)
    {
        $menu = array(
            1 => array($tallerRouter->btnNew($organizacion))
        );
        return $tallerRouter->toCalypso($menu);
    }

    private function getShowMenu($taller, $userloginManager)
    {
        $menu = array();
        if ($userloginManager->isGranted('ROLE_TALLER_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('taller_edit', array('id' => $taller->getId()))
            );
            if ($userloginManager->isGranted('ROLE_TALLER_ELIMINAR')) {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
            $menu['Sector'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => '#modalSector',
                'extra' => 'data-toggle=modal',
            );
        }
        return $menu;
    }

    /**
     * @Route("/mante/taller/{id}/list", name="taller_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_VER")
     */
    public function listAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        TallerManager $tallerManager,
        TallerRouter $tallerRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Listado de Talleres");
        $talleres = $tallerManager->find($organizacion);

        return $this->render('mante/Taller/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion, $tallerRouter),
            'talleres' => $talleres,
        ));
    }

    /**
     * @Route("/mante/taller/{idorg}/new", name="taller_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"}))
     * @IsGranted("ROLE_TALLER_AGREGAR")
     */
    public function newAction(
        Request $request,
        Organizacion $organizacion,
        BreadcrumbManager $breadcrumbManager,
        TallerManager $tallerManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Taller");
        $taller = $tallerManager->create($organizacion);
        $form = $this->createForm(TallerType::class, $taller);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $taller = $tallerManager->save($taller);
            $breadcrumbManager->pop();
            $this->setFlash('success', sprintf('El taller se ha creado con éxito'));
            return $this->redirect($this->generateUrl('taller_list', array(
                'id' => $organizacion->getId(),
            )));
        }

        return $this->render('mante/Taller/new.html.twig', array(
            'organizacion' => $organizacion,
            'taller' => $taller,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/taller/{id}/edit", name="taller_edit",
     *     requirements={
     *         "id": "\d+",
     *     }),
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function editAction(
        Request $request,
        Taller $taller,
        BreadcrumbManager $breadcrumbManager,
        TallerManager $tallerManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Editar Taller");
        $form = $this->createForm(TallerType::class, $taller);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $taller = $tallerManager->save($taller);
            $this->setFlash('success', sprintf('El taller se ha modificado con éxito'));
            $breadcrumbManager->pop();
            return $this->redirect($this->generateUrl('taller_list', array(
                'id' => $taller->getOrganizacion()->getId(),
            )));
        }
        return $this->render('mante/Taller/edit.html.twig', array(
            'taller' => $taller,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/taller/{id}/show", name="taller_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_VER")
     */
    public function showAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        TallerManager $tallerManager,
        UserLoginManager $userloginManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Ver Taller");
        $taller = $tallerManager->findOne($id);
        $sector = new TallerSector();
        $formSector = $this->createForm(TallerSectorType::class, $sector);
        $sectores = $taller->getSectores();
        return $this->render('mante/Taller/show.html.twig', array(
            'taller' => $taller,
            'sectores' => $sectores,
            'formSector' => $formSector->createView(),
            'menu' => $this->getShowMenu($taller, $userloginManager),
            'delete_form' => $this->createDeleteForm($id)->createView(),
        ));
    }

    /**
     * @Route("/mante/taller/{id}/delete", name="taller_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        TallerManager $tallerManager
    ) {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $breadcrumbManager->pop();
            if ($tallerManager->deleteById($id)) {
                $this->setFlash('success', 'Taller eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el taller del sistema.');
            }
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    /**
     * @Route("/mante/taller/addsector", name="taller_addsector")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_ELIMINAR")
     */
    public function addSectorAction(Request $request, TallerManager $tallerManager)
    {
        $idTaller = $request->get('id');
        $taller = $tallerManager->findOne($idTaller);
        $tmp = array();
        parse_str($request->get('formSector'), $tmp);
        $dataSector = $tmp['sector'];
        if ($dataSector) {
            $sector = new TallerSector();
            $sector->setOrganizacion($taller->getOrganizacion());
            $sector->setNombre($dataSector['nombre']);
            $sector->setTelefono($dataSector['telefono']);
            $sector->setRubro($dataSector['rubro']);
            $sector->setHorario($dataSector['horario']);
            $sector->setDescripcion($dataSector['descripcion']);
            $taller->addSector($sector);
            $taller = $tallerManager->save($taller);
            $sectores = $taller->getSectores();
        }

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/Taller/sectores.html.twig', array(
                'sectores' => $sectores
            )),
        )));
    }

    /**
     * @Route("/mante/taller/delsector", name="taller_delsector")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function delSectorAction(Request $request, TallerManager $tallerManager, SectorTallerManager $sectorTallerManager)
    {
        $idSector = $request->get('idSector');   //es el id del ordentrabajoproducto

        $idTaller = $request->get('id');
        $sector = $sectorTallerManager->findOne($idSector);
        $remove = $sectorTallerManager->delete($sector);
        $taller = $tallerManager->findOne($idTaller);
        $sectores = $taller->getSectores();

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/Taller/sectores.html.twig', array(
                'sectores' => $sectores
            )),
        )));
    }

    /**
     * @Route("/taller/{id}/editsector", name="taller_editsector",
     *     requirements={
     *         "id": "\d+",
     *     }),
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     * 
     */
    public function editSectorAction(
        Request $request,
        TallerSector $sector,
        UserLoginManager $userloginManager,
        SectorTallerManager $sectorTallerManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $organizacion = $userloginManager->getOrganizacion();
        $form = $this->createForm(TallerSectorType::class, $sector);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $sector = $sectorTallerManager->save($sector);
            $this->setFlash('success', sprintf('El taller se ha modificado con éxito'));
            $breadcrumbManager->pop();
            return $this->redirect($this->generateUrl('taller_show', array(
                'id' => $sector->getTaller()->getId(),
            )));
        }

        return $this->render('mante/Taller/editsector.html.twig', array(
            'form' => $form->createView(),
            'organizacion' => $organizacion,
        ));
    }
}
