<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Model\app\TareaOtManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ProductoManager;
use App\Model\app\StockManager;
use App\Model\app\DepositoManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\UtilsManager;
use App\Model\app\Router\TareaOtRouter;
use App\Entity\TareaOt;
use App\Form\mante\OrdenTrabajoProductoType;
use App\Form\mante\OrdenTrabajoMecanicoType;
use App\Form\mante\TareaOtEditType;
use App\Form\mante\OrdenTrabajoTareaType;
use App\Form\mante\TareaOtComentarioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of TareaOtController
 *
 * @author nicolas
 */
class TareaOtController extends AbstractController
{

    /**
     * @Route("/mante/tareaot/{id}/show", name="tareaot_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_VER")
     */
    public function showAction(
        Request $request,
        TareaOt $tareaot,
        TareaOtManager $tareaotManager,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        TareaOtRouter $tareaOtRouter
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "ver tarea");

        $organizacion = $userloginManager->getOrganizacion();

        $formProducto = $this->createForm(OrdenTrabajoProductoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));
        $formMecanico = $this->createForm(OrdenTrabajoMecanicoType::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager(),
        ));

        $formComentario = $this->createForm(TareaOtComentarioType::class, null);

        if ($tareaot->getEstado() < 1 || $tareaot->getEstado() == null) {
            $menu[1] = array($tareaOtRouter->btnIniciar($tareaot));
        }
        if ($tareaot->getEstado() == 1) {
            $menu[2] = array($tareaOtRouter->btnCerrar($tareaot));
        }
        if ($tareaot->getEstado() != 2) {
            $menu[3] = array($tareaOtRouter->btnEdit($tareaot));
            $menu[4] = array($tareaOtRouter->btnInsumo());
            $menu[5] = array($tareaOtRouter->btnMecanico());
        }
        // die('////<pre>' . nl2br(var_export($menu, true)) . '</pre>////');
        $menu[6] = array($tareaOtRouter->btnComentario());
        $menu[7] = array($tareaOtRouter->btnDelete($tareaot));

        return $this->render('mante/TareaOt/show.html.twig', array(
            'menu' => $tareaOtRouter->toCalypso($menu),
            'tareaot' => $tareaot,
            'orden' => $tareaot->getOrdenTrabajo(),
            'formProducto' => $formProducto->createView(),
            'formMecanico' => $formMecanico->createView(),
            'formComentario' => $formComentario->createView(),
            'delete_form' => $this->createDeleteForm($tareaot->getId())->createView(),
        ));
    }

    /**
     * @Route("/mante/tareaot/addproducto", name="tareaot_addproducto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addProductoAction(
        Request $request,
        ProductoManager $productoManager,
        OrdenTrabajoManager $ordentrabajoManager,
        TareaOtManager $tareaotManager,
        StockManager $stockManager
    ) {

        //  die('////<pre>' . nl2br(var_export($dataProd, true)) . '</pre>////');

        $id = $request->get('id');
        $tareaot = $tareaotManager->findOne($id);

        $tmp = array();
        parse_str($request->get('formProducto'), $tmp);
        $dataProd = $tmp['producto'];
        $actPrecio = isset($dataProd['actualizar']) ? true : false;
        $unitario = $actPrecio ? intval($dataProd['unitario']) : null;
        $producto = $productoManager->find($dataProd['producto']);

        if ($tareaot->getOrdenTrabajo()->getDeposito() != null) {
            $stock = $stockManager->restarStock($tareaot->getOrdenTrabajo()->getDeposito(), $producto, intval($dataProd['cantidad']), $unitario);
            $tareaProducto = $tareaotManager->findProducto(intval($dataProd['producto']), $tareaot->getId());
            if ($tareaProducto != null) { // si ya tiene este producto
                $x = $tareaotManager->editProducto($tareaProducto, intval($dataProd['cantidad']), intval($dataProd['unitario']), $actPrecio);
            } else { //sino crea uno nuevo
                $x = $tareaotManager->addProducto($tareaot, $dataProd['producto'], intval($dataProd['cantidad']), intval($dataProd['unitario']), $actPrecio);
            }
            $ordentrabajoManager->recalcularCostoOrden($tareaot->getOrdenTrabajo());
        } else {
            return new Response(json_encode(array('error_deposito' => 'error_deposito')));
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaOt/productos.html.twig', array(
                'tareaot' => $tareaot
            )),
            'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
            'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/tareaot/delproducto", name="tareaot_delproducto")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delProductoAction(
        Request $request,
        DepositoManager $depositoManager,
        TareaOtManager $tareaotManager,
        StockManager $stockManager,
        OrdenTrabajoManager $ordentrabajoManager
    ) {

        $idOTP = $request->get('idProd');   //es el id del ordentrabajoproducto
        $idOT = $request->get('id');
        $idDep = $request->get('idDep');
        $deposito = $depositoManager->find($idDep);
        $tareaot = $tareaotManager->findOne($idOT);
        $productoOT = $tareaotManager->findProdOT($idOTP);
        $stock = $stockManager->agregar($deposito, $productoOT->getProducto(), intval($request->get('cantidad')));
        $tareaotManager->delProducto($tareaot, $productoOT);
        $ordentrabajoManager->recalcularCostoOrden($tareaot->getOrdenTrabajo());

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaOt/productos.html.twig', array(
                'tareaot' => $tareaot
            )),
            'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
            'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/tareaot/addmecanico", name="tareaot_addmecanico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addMecanicoAction(Request $request, TareaOtManager $tareaotManager, OrdenTrabajoManager $ordentrabajoManager)
    {
        $idOT = $request->get('id');
        $tareaot = $tareaotManager->findOne($idOT);
        $tmp = array();
        parse_str($request->get('formMecanico'), $tmp);
        $dataMec = $tmp['mecanico'];
        //  die('////<pre>' . nl2br(var_export($tareaotManager->findMecanico(intval($dataMec['mecanico']),$tareaot->getId()), true)) . '</pre>////');
        $tareaMecanico = $tareaotManager->findMecanico(intval($dataMec['mecanico']), $tareaot->getId());
        if ($tareaMecanico != null) { // si ya tiene este mecanico.. modifica
            $x = $tareaotManager->editMecanico($tareaMecanico, intval($dataMec['cantidad']));
        } else { //sino crea uno nuevo
            $x = $tareaotManager->addMecanico($tareaot, $dataMec['mecanico'], intval($dataMec['cantidad']));
        }

        //$ordentrabajoManager->recalcularCostoOrden($tareaot->getOrdenTrabajo());

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaOt/mecanicos.html.twig', array(
                'tareaot' => $tareaot
            )),
            'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
            'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/tareaot/delmecanico", name="tareaot_delmecanico", methods={"GET","POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function delMecanicoAction(Request $request, TareaOtManager $tareaotManager, OrdenTrabajoManager $ordentrabajoManager)
    {
        $idOTM = $request->get('idMeca');   //es el id del ordentrabajoproducto

        $idOT = $request->get('id');
        $tareaot = $tareaotManager->findOne($idOT);
        // die('////<pre>' . nl2br(var_export($tareaot->getId(), true)) . '</pre>////');
        $tareaotManager->delMecanico($tareaot, $idOTM);
        $ordentrabajoManager->recalcularCostoOrden($tareaot->getOrdenTrabajo());

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaOt/mecanicos.html.twig', array(
                'tareaot' => $tareaot
            )),
            'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
            'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
        )));
    }

    /**
     * @Route("/mante/tareaot/modificar", name="tareaot_modificar", methods={"GET","POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function modificarAction(Request $request, TareaOtManager $tareaotManager, UtilsManager $utilsManager)
    {
        $idOT = $request->get('id');
        $tareaot = $tareaotManager->findOne($idOT);
        $tareaot->setFecha(new \DateTime($utilsManager->datetime2sqltimestamp($request->get('fecha'), true)));
        $tareaot->setDescripcion($request->get('descripcion'));
        $save = $tareaotManager->save($tareaot);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                'orden' => $tareaot->getOrdenTrabajo()
            )),
        )));
    }

    /**
     * @Route("/mante/tareaot/iniciar", name="tareaot_iniciar", 
     * methods={"GET","POST"}
     * )
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function iniciarAction(Request $request, TareaOtManager $tareaotManager, OrdenTrabajoManager $ordentrabajoManager)
    {
        $idOT = $request->get('id');
        $tareaot = $tareaotManager->findOne($idOT);
        if ($request->isMethod('POST')) {
            $tareaot->setEstado(1);
            $tareaotManager->save($tareaot);
            $ordentrabajoManager->cambiarEstado($tareaot->getOrdenTrabajo());

            return new Response(json_encode(array(
                'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                    'orden' => $tareaot->getOrdenTrabajo()
                )),
                'htmlEstado' => $this->renderView('mante/OrdenTrabajo/otestado.html.twig', array(
                    'orden' => $tareaot->getOrdenTrabajo()
                )),
                'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
                'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
            )));
        } else {
            //die('////<pre>' . nl2br(var_export( '$idOT', true)) . '</pre>////');
            return new Response(json_encode(array(
                'descripcion' => $tareaot->getDescripcion(),
                'fecha' => $tareaot->getFecha()->format('d-m-Y'),
                'costo' => $tareaot->getCostoNeto(),
            )));
        }
    }

    /**
     * @Route("/mante/tareaot/cerrar", name="tareaot_cerrar",
     *  methods={"GET","POST"}
     * )
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function cerrarAction(Request $request, TareaOtManager $tareaotManager, OrdenTrabajoManager $ordentrabajoManager)
    {
        $idOT = $request->get('id');
        //   die('////<pre>' . nl2br(var_export( $idOT, true)) . '</pre>////');
        $tareaot = $tareaotManager->findOne($idOT);
        if ($request->isMethod('POST')) {     //cierro la tarea
            //$tareaot->setEstado(2);
            $tareaotManager->cerrar($tareaot);

            $ordentrabajoManager->cambiarEstado($tareaot->getOrdenTrabajo());   //cambio el estado de la OT

            //$ordentrabajoManager->recalcularCostoOrden($tareaot->getOrdenTrabajo());


            return new Response(json_encode(array(
                'html' => $this->renderView('mante/OrdenTrabajo/tareasot.html.twig', array(
                    'orden' => $tareaot->getOrdenTrabajo()
                )),
                'htmlEstado' => $this->renderView('mante/OrdenTrabajo/otestado.html.twig', array(
                    'orden' => $tareaot->getOrdenTrabajo()
                )),
                'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
                'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
            )));
        } else {
            return new Response(json_encode(array(
                'descripcion' => $tareaot->getDescripcion(),
                'fecha' => $tareaot->getFecha()->format('d-m-Y'),
                'costoNeto' => number_format($tareaot->getCostoNeto(), 2),
                'costoTotal' => number_format($tareaot->getCostoTotal(), 2),
            )));
        }
    }

    /**
     * @Route("/mante/tareaot/addcomentario", name="tareaot_addcomentario")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function addComentarioAction(Request $request, TareaOtManager $tareaotManager, UserLoginManager $userloginManager)
    {
        $ejecutor = $userloginManager->getUser();
        $id = $request->get('id');
        $tareaot = $tareaotManager->findOne($id);
        $tmp = array();
        parse_str($request->get('formComentario'), $tmp);
        $dataComentario = $tmp['comentario'];
        $x = $tareaotManager->addComentario($tareaot, $dataComentario['comentario'], $ejecutor);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaOt/comentarios.html.twig', array(
                'tareaot' => $tareaot
            )),
        )));
    }

    /**
     * @Route("/mante/tareaot/{id}/edit", name="tareaot_edit")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function editAction(
        Request $request,
        TareaOt $tareaot,
        TareaOtManager $tareaManager,
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        UtilsManager $utilsManager
    ) {

        if ($request->getMethod() == 'POST') {
            $descripcion = $request->get('tarea_edit')['descripcion'];
            $fecha = new \DateTime($utilsManager->datetime2sqltimestamp($request->get('tarea_edit')['fecha'], true));
            $tareaot->setFecha($fecha);
            $tareaot->setDescripcion($descripcion);
            //die('////<pre>' . nl2br(var_export($request->get('tarea'), true)) . '</pre>////');
            if ($tareaManager->save($tareaot)) {
                $breadcrumbManager->pop();
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Edición");
        $organizacion = $userloginManager->getOrganizacion();
        $form = $this->createForm(TareaOtEditType::class);



        return $this->render('mante/TareaOt/edit.html.twig', array(
            'tareaot' => $tareaot,
            'organizacion' => $organizacion,
            'formTareas' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/tareaot/{id}/delete", name="tareaot_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORDENTRABAJO_EDITAR")
     */
    public function deleteAction(Request $request, TareaOt $tarea, TareaOtManager $tareaotManager, OrdenTrabajoManager $ordentrabajoManager, BreadcrumbManager $breadcrumbManager)
    {
        $form = $this->createDeleteForm($tarea->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $ordenTrabajo = $tarea->getOrdenTrabajo();
            $breadcrumbManager->pop();
            if ($tareaotManager->deleteById($tarea->getId())) {
                $ordentrabajoManager->recalcularCostoOrden($ordenTrabajo);
                $this->setFlash('success', 'Tarea eliminada del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar la tarea del sistema.');
            }
        }
        return $this->redirect($breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
