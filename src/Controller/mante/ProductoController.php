<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Entity\Producto as Producto;
use App\Entity\Organizacion as Organizacion;
use App\Form\mante\ProductoType;
use App\Form\mante\StockType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DepositoManager;
use App\Model\app\StockManager;
use App\Model\app\ExcelManager;
use App\Model\app\ProductoManager;
use App\Model\app\Router\ProductoRouter;
use App\Model\app\RubroManager;
use App\Model\app\TareaMantManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\UserLoginManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of ProductoController
 *
 * @author nicolas
 */
class ProductoController extends AbstractController
{

    private $breadcrumbManager;
    private $depositoManager;
    private $rubroManager;
    private $productoRouter;
    private $stockManager;
    private $productoManager;
    private $tareamantManager;
    private $organizacionManager;
    private $ordentrabajoManager;
    private $excelManager;
    private $userLoginManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        DepositoManager $depositoManager,
        TareaMantManager $tareamantManager,
        RubroManager $rubroManager,
        ProductoRouter $productoRouter,
        StockManager $stockManager,
        ProductoManager $productoManager,
        OrganizacionManager $organizacionManager,
        OrdenTrabajoManager $ordentrabajoManager,
        ExcelManager $excelManager,
        UserLoginManager $userLoginManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->depositoManager = $depositoManager;
        $this->rubroManager = $rubroManager;
        $this->productoManager = $productoManager;
        $this->stockManager = $stockManager;
        $this->productoRouter = $productoRouter;
        $this->tareamantManager = $tareamantManager;
        $this->organizacionManager = $organizacionManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->excelManager = $excelManager;
        $this->userLoginManager = $userLoginManager;
    }

    /**
     * @Route("/mante/producto/{idOrg}/list", name="producto_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_STOCK_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Productos");
        $depositos = $this->depositoManager->findAllByOrganizacion($organizacion);
        $st = $this->getDataProductos($organizacion);

        return $this->render('mante/Producto/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'organizacion' => $organizacion,
            'productos' => $st,
            'depositos' => $depositos,
            'rubros' => $this->rubroManager->find($organizacion),
            'formStock' => $this->createForm(StockType::class, null, array('organizacion' => $organizacion))->createView(),
        ));
    }

    private function getDataProductos($organizacion)
    {
        $stocks = $this->stockManager->findAllByOrganizacion($organizacion);
        $productos = $this->productoManager->findAllByOrganizacion($organizacion);
        $st = array();
        foreach ($productos as $producto) {
            $st[$producto->getId()] = array('producto' => $producto, 'depositos' => null);
            $dep = array();
            foreach ($stocks as $stock) {
                if ($stock->getProducto()->getId() == $producto->getId()) {
                    $dep[$stock->getDeposito()->getId()] = array('cantidad' => $stock->getStock(), 'precio' => $stock->getCosto());
                    $st[$producto->getId()] = array('producto' => $producto, 'depositos' => $dep);
                }
            }
        }
        //dd($st);
        return $st;
    }

    /**
     * @Route("/mante/producto/{id}/show", name="producto_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_VER")
     */
    public function showAction(Request $request, Producto $producto)
    {
        if (!$producto) {
            throw $this->createNotFoundException('Código de Producto no encontrado.');
        }
        $organizacion = $this->userLoginManager->getOrganizacion();
        $movs = $this->productoManager->getHistorico($producto, $organizacion);
        $movsEventual = $this->tareamantManager->findHistoricoByProducto($producto);
        $organizacion = $producto->getOrganizacion();
        $this->breadcrumbManager->push($request->getRequestUri(), $producto->getNombre());
        return $this->render('mante/Producto/show.html.twig', array(
            'menu' => $this->getShowMenu($producto),
            'producto' => $producto,
            'stocks' => $this->stockManager->findByProducto($producto),
            'rubros' => $this->rubroManager->find($organizacion),
            'movimientos' => $movs,
            'eventuales' => $movsEventual,
            'depositos' => $this->depositoManager->findAllByOrganizacion($organizacion),
            'delete_form' => $this->createDeleteForm($producto->getId())->createView(),
            'formStock' => $this->createForm(StockType::class, null, array('organizacion' => $organizacion, 'producto' => $producto))->createView(),
            'organizacion' => $organizacion,
        ));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($producto)
    {
        $menu = array(
            1 => array($this->productoRouter->btnEdit($producto, true)),
            2 => array($this->productoRouter->btnAddStock($producto, true)),
            3 => array($this->productoRouter->btnDelete($producto, true)),
        );

        return $this->productoRouter->toCalypso($menu);
    }

    private function getListMenu($organizacion)
    {
        $menu = array(
            1 => array($this->productoRouter->btnNew($organizacion, true)),
            2 => array($this->productoRouter->btnRubro($organizacion, true)),
            3 => array($this->productoRouter->btnExport($organizacion, true)),
        );

        return $this->productoRouter->toCalypso($menu);
    }

    /**
     * @Route("/mante/producto/{idorg}/new", name="producto_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_AGREGAR")
     */
    public function newAction(Request $request, $idorg)
    {

        $organizacion = $this->organizacionManager->find($idorg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el producto
        $producto = $this->productoManager->create($organizacion);
        $options['organizacion'] = $organizacion;
        $options['em'] = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProductoType::class, $producto, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->productoManager->save($producto)) {
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Producto");
        return $this->render('mante/Producto/new.html.twig', array(
            'organizacion' => $organizacion,
            'producto' => $producto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/producto/{id}/edit", name="producto_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_EDITAR")
     */
    public function editAction(Request $request, Producto $producto)
    {

        if (!$producto) {
            throw $this->createNotFoundException('Código de Producto no encontrado.');
        }

        $options['organizacion'] = $producto->getOrganizacion();
        $options['em'] = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProductoType::class, $producto, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();

            if ($producto->getRubro()) {
                $r = $this->rubroManager->findOne($producto->getRubro());
                $producto->setRubro($r);
            }
            if ($this->productoManager->save($producto)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', 'Los datos han sido cambiado con éxito');

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Producto");
        return $this->render('mante/Producto/edit.html.twig', array(
            'producto' => $producto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/producto/cambiarprecio", name="producto_cambiarprecio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_CAMBIARPRECIO")
     */
    public function cambiarprecioAction(Request $request)
    {
        $idProd = $request->get('id');
        $tmp = array();
        parse_str($request->get('formPrecio'), $tmp);
        $tmp['precio'] =  str_replace(",", ".", $tmp['precio']);
        $precio = floatval($tmp['precio']);
        $deposito = $this->stockManager->find($idProd);
        if (!$deposito) {
            throw $this->createNotFoundException('Código de Producto no encontrado.');
        }
        $deposito = $this->stockManager->actualizarPrecio($deposito, $precio);

        return new Response(json_encode(array(
            'html' => $this->renderView('mante/Producto/show_depositos.html.twig', array(
                'stocks' => $this->stockManager->findByProducto($deposito->getProducto()),
            )),
        )));
    }

    /**
     * @Route("/mante/producto/{id}/delete", name="producto_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_ELIMINAR")
     */
    public function deleteAction(Request $request, Producto $producto)
    {
        $form = $this->createDeleteForm($producto->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->productoManager->deleteById($producto->getId())) {
                $this->setFlash('success', 'Producto eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el producto del sistema.');
            }
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @IsGranted("ROLE_STOCK_VER")
     */
    public function findbydepositoAction(Request $request)
    {
        $deposito = $this->depositoManager->find($request->get('id'));
        $stocks = $this->stockManager->findByDeposito($deposito);
        $productos = array();
        foreach ($stocks as $stock) {
            $productos[] = array('id' => $stock->getProducto()->getId(), 'nombre' => $stock->getProducto()->getNombre());
        }
        $json = json_encode($productos);
        return new \Symfony\Component\HttpFoundation\Response($json);
    }

    /**
     * @Route("/mante/producto", name="producto_get")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_VER")
     */
    public function getAction(Request $request)
    {
        $idOrg = $request->get('idOrg');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find($idOrg);
        //recargo los productos y devuelvo
        $productos = $this->productoManager->findAllQuery($organizacion, $search);

        $str = array();
        foreach ($productos as $prod) {
            $x = $prod->getNombre();
            if ($prod->getCodigo() != null) {
                $x .= ' (' . $prod->getCodigo() . ') ';
            }
            $str[] = array('id' => $prod->getId(), 'text' => $x);
        }

        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/mante/producto/getprecio", name="producto_getprecio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_CAMBIARPRECIO")
     */
    public function getPrecioAction(Request $request)
    {
        $idOT = $request->get('id');
        $idProd = $request->get('idProducto');
        $orden = $this->ordentrabajoManager->find(intval($idOT));
        $producto = $this->productoManager->find(intval($idProd));

        $result = $this->productoManager->getPrecio($orden->getDeposito(), $producto);

        if ($result != null) {
            return new Response(json_encode(array(
                'precio' => $result->getCosto(),
                'fecha' => $result->getUltCompra() ? $result->getUltCompra()->format('d/m/Y') : null,
            )), 200);
        } else {
            return new Response(json_encode(array(
                'precio' => 0,
                'fecha' => '---',
            )), 200);
        }
    }

    /**
     * @Route("/mante/producto/add", name="producto_add")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_AGREGAR")
     */
    public function addAction(Request $request)
    {
        $idOrg = intval($request->get('idOrg'));
        $organizacion = $this->organizacionManager->find($idOrg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }
        $tmp = array();
        parse_str($request->get('form'), $tmp);

        if ($request->getMethod() == 'POST' && isset($tmp['productonew'])) {
            $data = $tmp['productonew'];
            $prod = new Producto();
            $prod->setOrganizacion($organizacion);
            $prod->setNombre($data['nombre']);
            $prod->setCodigo($data['codigo']);
            $prod->setMarca($data['marca']);
            $prod->setModelo($data['modelo']);

            $prod->setCodigoBarra($data['codigoBarra']);
            if (isset($data['rubro'])) {
                $rubro = $this->rubroManager->findOne(intval($data['rubro']));
                $prod->setRubro($rubro);
            }
            if ($this->productoManager->save($prod)) {
                //una vez que esta grabado agrego stock ficticio para no tener problema y graba el precio.
                $idDep = $request->get('idDep');
                if ($idDep == 0 && isset($data['deposito'])) {
                    $idDep = intval($data['deposito']);
                }
                $deposito = $this->depositoManager->find($idDep);
                if ($deposito) {
                    $fecha = new \DateTime();
                    $data['precio'] = str_replace(",", ".", $data['precio']);
                    $r = $this->stockManager->agregar($deposito, $prod, 0, $fecha, null, floatval($data['precio']));
                }

                //recargo los productos y devuelvo
                //     die('////<pre>' . nl2br(var_export($idOrg, true)) . '</pre>////');
                $productos = $this->productoManager->findAllByOrganizacion($organizacion);
                $str = array();
                foreach ($productos as $prod) {
                    $x = $prod->getNombre() . ' (' . $prod->getCodigo() . ') ';
                    $str[$x] = array($prod->getId());
                }

                $html = $this->renderView('mante/Producto/content_list.html.twig', array(
                    'productos' => $this->getDataProductos($organizacion),
                    'depositos' => $this->depositoManager->findAllByOrganizacion($organizacion)
                ));

                return new Response(json_encode(array('status' => 'ok', 'data' => $str, 'html' => $html)), 200);
            }
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/mante/producto/{id}/export", name="producto_export")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_VER")
     */
    public function exportarAction(Request $request, Organizacion $organizacion)
    {
        $depositos = $this->depositoManager->findAllByOrganizacion($organizacion);
        if ($depositos) {
            $xls = $this->excelManager->create("stock");
            $xls->setBar(6, array(
                'A' => array('title' => 'Depósito', 'width' => 20, 'color' => '#0000FF'),
                'B' => array('title' => 'Producto', 'width' => 20, 'color' => '#0000FF'),
                'C' => array('title' => 'Rubro', 'width' => 10, 'color' => '#0000FF'),
                'D' => array('title' => 'Código', 'width' => 10, 'color' => '#0000FF'),
                'E' => array('title' => 'Marca', 'width' => 10, 'color' => '#0000FF'),
                'F' => array('title' => 'Modelo', 'width' => 10, 'color' => '#0000FF'),
                'G' => array('title' => 'IVA', 'width' => 20, 'color' => '#0000FF'),
                'H' => array('title' => 'Fecha de compra', 'width' => 20, 'color' => '#0000FF'),
                'I' => array('title' => 'Cantidad', 'width' => 20, 'color' => '#0000FF'),
                'J' => array('title' => 'Precio', 'width' => 20, 'color' => '#0000FF'),
                'K' => array('title' => 'Precio c/iva', 'width' => 20, 'color' => '#0000FF'),
                'L' => array('title' => 'Proveedor', 'width' => 20, 'color' => '#0000FF'),
            ));
            $i = 7;
            foreach ($depositos as $deposito) {
                foreach ($deposito->getStocks() as $stock) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $deposito->getNombre()),
                        'B' => array('value' => $stock->getProducto()->getNombre()),
                        'C' => array('value' => $stock->getProducto()->getRubro() ? $stock->getProducto()->getRubro()->getNombre() : ''),
                        'D' => array('value' => $stock->getProducto()->getCodigo()),
                        'E' => array('value' => $stock->getProducto()->getMarca()),
                        'F' => array('value' => $stock->getProducto()->getModelo()),
                        'G' => array('value' => $stock->getProducto()->getTasaIva()),
                        'H' => array('value' => $stock->getUltCompra() ? $stock->getUltCompra()->format('d/m/Y') : ''),
                        'I' => array('value' => $stock->getStock()),
                        'J' => array('value' => $stock->getCosto()),
                        'K' => array('value' => (1 + ($stock->getProducto()->getTasaIva() / 100)) * $stock->getCosto()),
                        'L' => array('value' => $stock->getUltProveedor() ? $stock->getUltProveedor() : ''),
                    ));
                    $i++;
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=stock.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->setFlash('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('producto_list'));
        }
    }

    function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
