<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Form\mante\ProductoType;
use App\Form\mante\StockType;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DepositoManager;
use App\Model\app\StockManager;
use App\Model\app\ProductoManager;
use App\Model\app\UtilsManager;
use App\Model\app\UserLoginManager;


/**
 * Description of StockController
 *
 * @author nicolas
 */
class StockController extends AbstractController
{

    public function deleteAction(Request $request, $id, StockManager $stockManager, BreadcrumbManager $breadcrumbManager)
    {
        $breadcrumbManager->push($request->getRequestUri(), "Eliminar Producto");
        if ($stockManager->deleteById($id)) {
            $this->setFlash('success', 'Producto eliminado del sistema.');
        } else {
            $this->setFlash('error', 'Error al eliminar el producto del sistema.');
        }

        return $this->redirect($breadcrumbManager->getVolver());
    }

    /**
     * @Route("/mante/stock/add", name="stock_addstock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_AGREGAR")
     */
    public function addStockAction(
        Request $request,
        StockManager $stockManager,
        UtilsManager $utilsManager,
        UserLoginManager $userloginManager,
        DepositoManager $depositoManager,
        ProductoManager $productoManager
    ) {

        $destino = $request->get('destino');
        $organizacion = $userloginManager->getOrganizacion();
        $tmp = array();
        parse_str($request->get('formStock'), $tmp);
        $dataProd = $tmp['stock'];

        if ($dataProd) {
            $producto = $productoManager->find($dataProd['producto']);
            $deposito = $depositoManager->find($dataProd['deposito']);
            $fecha = $dataProd['ult_compra'] ? new \DateTime($utilsManager->datetime2sqltimestamp($dataProd['ult_compra'] , false)) : null;
            $dataProd['costo'] = str_replace(",", ".", $dataProd['costo']);
            $stock = $stockManager->agregar(
                $deposito,
                $producto,
                $dataProd['stock'],
                $fecha,
                $dataProd['ult_proveedor'],
                floatval($dataProd['costo'])
            );
        }

        switch ($destino) {
            case '0': //sale por deposito show.
                return new Response(json_encode(array(
                    'html' => $this->renderView('mante/Deposito/content_show.html.twig', array(
                        'stocks' => $deposito->getStocks(),
                    )),
                )));

                break;
            case '1': //sale por producto list 
                $stocks = $stockManager->findAllByOrganizacion($organizacion);
                $productos = $productoManager->findAllByOrganizacion($organizacion);
                $depositos = $depositoManager->findAllByOrganizacion($organizacion);
                $st = array();
                foreach ($productos as $producto) {
                    $dep = array();
                    foreach ($stocks as $stock) {
                        if ($stock->getProducto()->getId() == $producto->getId()) {
                            $dep[$stock->getDeposito()->getId()] = array('cantidad' => $stock->getStock(), 'precio' => $stock->getCosto());
                            $st[$producto->getNombre()] = array('producto' => $producto, 'depositos' => $dep);
                        }
                    }
                }

                ksort($st);

                return new Response(json_encode(array(
                    'html' => $this->renderView('mante/Producto/content_list.html.twig', array(
                        'productos' => $st,
                        'depositos' => $depositos,
                        'stock' => $st,
                    )),
                )));
                break;
            case '2': //sale por producto show.
                return new Response(json_encode(array(
                    'html' => $this->renderView('mante/Producto/show_depositos.html.twig', array(
                        'stocks' => $stockManager->findByProducto($producto),
                    )),
                )));

                break;
        }
    }

    /**
     * @Route("/mante/stock/del", name="stock_delstock")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_STOCK_ELIMINAR")
     */
    public function delStockAction(
        Request $request,
        UserLoginManager $userloginManager,
        StockManager $stockManager,
        ProductoManager $productoManager,
        DepositoManager $depositoManager
    ) {
        $destino = $request->get('destino');
        $organizacion = $userloginManager->getOrganizacion();
        $idStock = $request->get('idStock');   //es el id del ordentrabajoproducto
        $stock = $stockManager->find(intval($idStock));
        $deposito = $stock->getDeposito();
        $remove = $stockManager->delete($stock);
        switch ($destino) {
            case '0': //sale por deposito show.
                return new Response(json_encode(array(
                    'html' => $this->renderView('mante/Deposito/content_show.html.twig', array(
                        'stocks' => $deposito->getStocks(),
                    )),
                )));
                break;
            case '1':
                if ($remove) {
                    $productos = $productoManager->findAllByOrganizacion($organizacion);
                    $depositos = $depositoManager->findAllByOrganizacion($organizacion);
                }
                return new Response(json_encode(array(
                    'html' => $this->renderView('mante/Producto/content_list.html.twig', array(
                        'productos' => $productos,
                        'depositos' => $depositos,
                        'stock' => '',
                    )),
                )));
                break;
        }
    }



    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
