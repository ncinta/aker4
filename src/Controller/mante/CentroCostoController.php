<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\mante\CentroCostoType;
use App\Form\mante\CentroCostoServicioType;
use App\Entity\Organizacion;
use App\Entity\CentroCosto;
use App\Model\app\OrganizacionManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\PrestacionManager;
use App\Model\app\Router\CentroCostoRouter;
use App\Model\app\CentroCostoManager;
use App\Model\app\IndiceManager;
use App\Model\app\BitacoraCentroCostoManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CentroCostoController extends AbstractController
{

    private $bread;
    private $centrocosto;
    private $ccRouter;
    private $prestacion;
    private $bitacoraCCManager;
    private $indice;
    private $organizacionManager;

    public function __construct(
        BreadcrumbManager $bread,
        BitacoraCentroCostoManager $bitacoraCCManager,
        CentroCostoManager $centrocostoManager,
        CentroCostoRouter $centrocostoRouter,
        PrestacionManager $prestacionManager,
        IndiceManager $indiceManager,
        OrganizacionManager $organizacionManager,
    ) {
        $this->bitacoraCCManager = $bitacoraCCManager;
        $this->bread = $bread;
        $this->centrocosto = $centrocostoManager;
        $this->ccRouter = $centrocostoRouter;
        $this->prestacion = $prestacionManager;
        $this->indice = $indiceManager;
        $this->organizacionManager = $organizacionManager;
    }

    /**
     * @Route("/mante/centrocosto/{idorg}/list", name="centrocosto_list")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idorg"})
     * @IsGranted("ROLE_CENTROCOSTO_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->bread->pushPorto($request->getRequestUri(), "CentroCostos");

        $centrocostos = $this->centrocosto->findAllByOrganizacion($organizacion);
        $indices = array();
        foreach ($centrocostos as $cc) {
            $indices[$cc->getId()] = $this->indice->dataParaBarras($this->indice->getLastInsertCc($cc));
        }

        $menu = array(
            1 => array($this->ccRouter->btnNew($organizacion)),
        );

        return $this->render('mante/CentroCosto/list.html.twig', array(
            'menu' => $menu,
            'centrocostos' => $centrocostos,
            'indices' => $indices,
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/show", name="centrocosto_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_VER")
     * )
     */
    public function showAction(Request $request, CentroCosto $centrocosto)
    {

        $servicios = array();
        $indicesBarraServ = array();
        $deleteForm = $this->createDeleteForm($centrocosto->getId());
        if (!$centrocosto) {
            throw $this->createNotFoundException('Código de Centro de Costo no encontrado.');
        }

        $this->bread->pushPorto($request->getRequestUri(), $centrocosto->getNombre());

        foreach ($centrocosto->getServicios() as $servicio) {
            $servicios[] = array(
                'servicio' => $servicio,
                'prestacion' => $this->prestacion->findActivo($servicio),
            );
        }

        $indiceBarraCC = $this->indice->dataParaBarras($this->indice->getLastInsertCc($centrocosto));

        foreach ($centrocosto->getServicios() as $servicio) {
            $indicesBarraServ[$servicio->getId()] = $this->indice->dataParaBarras($this->indice->getLastInsertServicio($servicio));
        }

        //die('////<pre>' . nl2br(var_export($servicios, true)) . '</pre>////');
        return $this->render('mante/CentroCosto/show.html.twig', array(
            'menu' => $this->getShowMenu($centrocosto),
            'servicios' => $servicios,
            'tab' => 'main',
            'centrocosto' => $centrocosto,
            'indicesBarraCC' => $indiceBarraCC,
            'indicesBarraServ' => $indicesBarraServ,
            //                    'formServicio' => $formServicio->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/showbitacora", name="centrocosto_show_bitacora",
     *     requirements={
     *         "id": "\d+"
     *     },
     *  )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_VER")
     * )
     */
    public function showbitacoraAction(Request $request, CentroCosto $centrocosto)
    {
        $this->bread->pop();
        $this->bread->pushPorto($request->getRequestUri(), $centrocosto->getNombre() . '(Bitacora)');
        $deleteForm = $this->createDeleteForm($centrocosto->getId());
        $formServicio = $this->createForm(CentroCostoServicioType::class, null, array(
            'organizacion' => $centrocosto->getOrganizacion(),
            'em' => $this->getEntityManager(),
        ));
        if (!$centrocosto) {
            throw $this->createNotFoundException('Código de Centro de Costo no encontrado.');
        }
        $registro = $this->bitacoraCCManager->getRegistros($centrocosto);
        $bitacora = array();
        foreach ($registro as $value) {
            $bitacora[] = array(
                'registro' => $value,
                'descripcion' => $this->bitacoraCCManager->parse($value),
            );
        }

        return $this->render('mante/CentroCosto/show.html.twig', array(
            'bitacora' => $bitacora,
            'menu' => $this->getShowMenu($centrocosto),
            'formServicio' => $formServicio->createView(),
            'delete_form' => $deleteForm->createView(),
            'centrocosto' => $centrocosto,
            'tab' => 'bitacora',
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($centrocosto)
    {
        $menu = array(
            1 => array($this->ccRouter->btnEdit($centrocosto)),
            2 => array($this->ccRouter->btnDelete($centrocosto)),
            3 => array($this->ccRouter->btnServicioAsignar($centrocosto)),
            4 => array($this->ccRouter->btnServicioRetirarMany($centrocosto))
        );
        return $menu;
    }

    /**
     * @Route("/mante/centrocosto/{idOrg}/new", name="centrocosto_new")
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_CENTROCOSTO_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion)
    {
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }
        //creo el objetivo
        $objetivo = $this->centrocosto->create($organizacion);
        $form = $this->createForm(CentroCostoType::class, $objetivo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();

            if ($this->centrocosto->save($objetivo)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El objetivo se ha creado con éxito'));

                return $this->redirect($this->bread->getVolver());
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Nuevo CentroCosto");
        return $this->render('mante/CentroCosto/new.html.twig', array(
            'organizacion' => $organizacion,
            'centrocosto' => $objetivo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/centrocosto/{id}/edit", name="centrocosto_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_EDITAR")
     */
    public function editAction(Request $request, CentroCosto $centrocosto)
    {

        if (!$centrocosto) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $form = $this->createForm(CentroCostoType::class, $centrocosto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->bread->pop();

            if ($this->centrocosto->save($centrocosto)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));

                return $this->redirect($this->bread->getVolver());
            }
        }

        $this->bread->pushPorto($request->getRequestUri(), "Editar Servicio");
        return $this->render('mante/CentroCosto/edit.html.twig', array(
            'centrocosto' => $centrocosto,
            'form' => $form->createView(),
        ));
    }

    /**
     * Crea un nuevo servicio a partir de un equipo. 
     * @IsGranted("ROLE_CENTROCOSTO_AGREGAR")
     */
    public function newmodalAction(Request $request)
    {
        $this->bread->pushPorto($request->getRequestUri(), "Crear Centro Costo");
        if ($request->getMethod() == 'GET') {
            $idorg = $request->get('idorg');
            $nombre = $request->get('nombre');
            $organizacion = $this->organizacionManager->find($idorg);
            if (!$organizacion) {
                throw $this->createNotFoundException('Organización no encontrado.');
            }

            //        //creo el objetivo
            $objetivo = $this->centrocosto->create($organizacion);
            $this->bread->pop();
            $objetivo->setNombre($nombre);
            if ($this->centrocosto->save($objetivo)) {
                $objetivos = $this->centrocosto->findAllByOrganizacion($organizacion);
                $select_obj = array();
                foreach ($objetivos as $obj) {
                    $select_obj[$obj->getId()] = $obj->getNombre();
                }
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El objetivo se ha creado con éxito'));
                return new Response(json_encode($select_obj));
            }
        }
    }

    /**
     * @Route("/centrocosto/{id}/delete", name="centrocosto_delete",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CENTROCOSTO_ELIMINAR")
     */
    public function deleteAction(Request $request, CentroCosto $cc)
    {

        $form = $this->createDeleteForm($cc->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->bread->pop();
            if ($this->centrocosto->deleteById($cc->getId())) {
                $this->setFlash('success', 'Centro de Costo eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el servicio del sistema.');
            }
        }

        return $this->redirect($this->bread->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
