<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\mante\TareaMantenimientoType;
use App\Form\mante\TareaMantenimientoListType;
use App\Form\mante\TareaMantenimientoEventualType;
use App\Form\mante\TareaMantenimientoEventualEditType;
use App\Form\mante\TareaMantenimientoEditType;
use App\Form\mante\TareaMantenimientoRegistroType;
use App\Entity\BitacoraServicio;
use App\Entity\Organizacion;
use App\Entity\ServicioMantenimiento;
use App\Entity\Servicio;
use App\Model\app\BreadcrumbManager;
use App\Model\app\TallerManager;
use App\Model\app\SectorTallerManager;
use App\Model\app\MecanicoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\InformeUtilsManager;
use App\Model\app\CentroCostoManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\TareaMantManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\BitacoraServicioManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\MantenimientoManager;
use App\Model\app\StockManager;
use App\Model\app\ProductoManager;
use App\Model\app\DepositoManager;
use App\Model\app\ExcelManager;
use App\Model\app\Router\PlanRouter;
use App\Model\app\Router\TareaMantRouter;
use App\Model\app\Router\OrdenTrabajoRouter;
use App\Model\crm\PeticionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of TareaMantenimientoController
 *
 * @author claudio
 */
class TareaMantenimientoController extends AbstractController
{
    private $moduloCentroCosto = false;

    private $tareaMantRouter;
    private $ordentrabajoRouter;
    private $planRouter;
    private $bread;
    private $userloginManager;
    private $servicioManager;
    private $tareamantManager;
    private $grupoServicioManager;
    private $centrocostoManager;
    private $bitacoraservicioManager;
    private $organizacionManager;
    private $peticionManager;
    private $ordentrabajoManager;
    private $mantenimientoManager;
    private $utilsManager;
    private $tallerManager;
    private $depositoManager;
    private $sectorTallerManager;
    private $productoManager;
    private $mecanicoManager;
    private $stockManager;

    function __construct(
        TareaMantRouter $tareaMantRouter,
        OrdenTrabajoRouter $ordentrabajoRouter,
        PlanRouter $planRouter,
        BreadcrumbManager $bread,
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        TareaMantManager $tareamantManager,
        GrupoServicioManager $grupoServicioManager,
        CentroCostoManager $centrocostoManager,
        BitacoraServicioManager $bitacoraservicioManager,
        OrganizacionManager $organizacionManager,
        PeticionManager $peticionManager,
        OrdenTrabajoManager $ordentrabajoManager,
        UtilsManager $utilsManager,
        TallerManager $tallerManager,
        DepositoManager $depositoManager,
        SectorTallerManager $sectorTallerManager,
        ProductoManager $productoManager,
        MecanicoManager $mecanicoManager,
        StockManager $stockManager,
        MantenimientoManager $mantenimientoManager
    ) {
        $this->tareaMantRouter = $tareaMantRouter;
        $this->ordentrabajoRouter = $ordentrabajoRouter;
        $this->planRouter = $planRouter;
        $this->bread = $bread;
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->tareamantManager = $tareamantManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->centrocostoManager = $centrocostoManager;
        $this->bitacoraservicioManager = $bitacoraservicioManager;
        $this->organizacionManager = $organizacionManager;
        $this->peticionManager = $peticionManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->mantenimientoManager = $mantenimientoManager;
        $this->utilsManager = $utilsManager;
        $this->tallerManager = $tallerManager;
        $this->depositoManager = $depositoManager;
        $this->sectorTallerManager = $sectorTallerManager;
        $this->productoManager = $productoManager;
        $this->mecanicoManager  = $mecanicoManager;
        $this->stockManager = $stockManager;
    }

    /**
     * @Route("/mante/tareamant/{idOrg}/list", name="tarea_mant_list")
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function listAction(Request $request, Organizacion $organizacion)
    {

        $this->bread->pushPorto($request->getRequestUri(), "Mantenimientos");
        $arr_tareas = array();
        $user = $tmant = null;
        $estado = $request->get('estado') ? intval($request->get('estado')) : 0;

        $consulta = array(
            'servicio' => -1,
            'centrocosto' => -1,
            'estado' => $estado,
            'servicionombre' => 'Toda la flota',
            'strCentroCosto' => '---',
            'strEstado' => 'Todos',
        );
        $this->moduloCentroCosto = $this->userloginManager->isGranted('ROLE_APMON_PRESTACION_ADMIN');
        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }
        $servicios = $this->servicioManager->findAllByUsuario($user);
        foreach ($servicios as $servicio) {
            $tmant = $this->tareamantManager->findByServicio($servicio);
            $arr_tareas[] = array(
                'servicio' => $servicio,
                'tareas' => $tmant ? $this->tareamantManager->data($tmant, $estado) : null,
            );
        }

        if ($estado > 0) { //si hay estado elimino las que no tengan estado
            foreach ($arr_tareas as $key => $value) {
                if (is_null($value['tareas'])) {
                    unset($arr_tareas[$key]);
                }
            }
        }

        //aca arranca el menu de opcones
        $menu = array(
            1 => array($this->ordentrabajoRouter->btnNew($servicio->getOrganizacion())),
            2 => array($this->planRouter->btnPlanMantenimiento($servicio->getOrganizacion())),
            3 => array($this->tareaMantRouter->btnNew($servicio->getOrganizacion(), $servicio)),
            4 => array($this->tareaMantRouter->btnInforme($servicio->getOrganizacion())),
        );

        $options['servicios'] = $servicios;
        $options['estado'] = $estado;
        $options['grupos'] = $this->grupoServicioManager->findAsociadas();
        $options['moduloCentroCosto'] = $this->moduloCentroCosto;
        if ($this->moduloCentroCosto) {
            $options['centrocostos'] = $this->centrocostoManager->findAllByOrganizacion($organizacion);
        }


        $form = $this->createForm(TareaMantenimientoListType::class, null, $options);

        return $this->render('mante/TareaMantenimiento/list.html.twig', array(
            'menu' => $menu,
            'organizacion' => $organizacion,
            'moduloCentroCosto' => $this->moduloCentroCosto,
            'tmantenimientos' => $arr_tareas,
            'consulta' => $consulta,
            'filtro' => 'filtro:Default',
            'form' => $form->createView(),
        ));
    }

    public function getStatus($tmant, $servicio)
    {
        $red = $orange = $green = $white = 0;
        $tar = array();
        foreach ($tmant as $tarea) {
            $status = $this->tareamantManager->analizeMantenimiento($tarea);
            if ($status['code'] == 1) {
                $green++;
            } elseif ($status['code'] == 2) {
                $orange++;
            } elseif ($status['code'] == 3) {
                $red++;
            } else {
                $white++;
            }
            $tar[] = array(
                // 'plan' => $tarea->getMantenimientos(),
                'status' => $status
            );
        }

        $arr_tareas = array(
            'servicio' => $servicio,
            'tareas' => $tar,
            'ok' => $green,
            'warning' => $orange,
            'error' => $red
        );
        return $arr_tareas;
    }

    /**
     * @Route("/mante/tareamant/{id}/show", name="tareamant_show",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function showAction(Request $request, ServicioMantenimiento $tarea)
    {
        $data = array();
        if (!$tarea) {
            throw $this->createNotFoundException('Código de Tarea de Mantenimiento no encontrado.');
        }
        $tipoOrg = $this->userloginManager->getOrganizacion()->getTipoOrganizacion();
        $historico = $this->tareamantManager->historico($tarea);
        $i = 0;
        foreach ($historico as $mant) {
            $data[] = array(
                'rowspan' => $i,
                'registro' => $mant,
                'actividades' => $mant->getHistoricoActividad(),
                'bitacora' => $this->bitacoraservicioManager->findByHistorico($tarea->getServicio(), $mant->getId(), BitacoraServicio::EVENTO_MANTENIMIENTO_REGISTRO, $tipoOrg)
            );
        }
        $this->bread->push($request->getRequestUri(), $tarea->getMantenimiento()->getNombre());

        return $this->render('mante/TareaMantenimiento/show.html.twig', array(
            'is_stock' => $this->organizacionManager->isModuloEnabled($tarea->getServicio()->getOrganizacion(), 'MODULO_STOCK'),
            'menu' => $this->getShowMenu($tarea, $this->userloginManager),
            'tarea' => $tarea,
            'historico' => $data,
            'delete_form' => $this->createDeleteForm($tarea->getId())->createView(),
            'status' => $this->tareamantManager->analizeMantenimiento($tarea),
        ));
    }

    /**
     * @Route("/mante/tareamant/{id}/showservicio", name="tareamant_showservicio",
     *     requirements={
     *         "id": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function showservicioAction(Request $request, Servicio $servicio)
    {
        if (!$servicio) {
            throw $this->createNotFoundException('Servicio no encontrado.');
        }
        $consulta['incluir_ok'] = true;
        $this->bread->push($request->getRequestUri(), $servicio->getNombre());
        $tareas = $this->tareamantManager->procesarStatusMantenimientoPorServicio(array($servicio), $consulta);
        //dd($tareas);
        $eventuales = $this->tareamantManager->findHistByServicio($servicio);
        $peticiones = $this->peticionManager->findByServicio($servicio);
        $nuevas = $cerradas = $encurso = array();
        foreach ($peticiones as $peticion) {
            if ($peticion->getEstado()->getPosicion() == 1) { //abierta
                $nuevas[] = $peticion;
            } else if ($peticion->getEstado()->getPosicion() == 2 || $peticion->getEstado()->getPosicion() == 3) { // en curso o en pausa
                $encurso[] = $peticion;
            } elseif ($peticion->getEstado()->getPosicion() == 10) { //cerrada
                $cerradas[] = $peticion;
            }
        }
        $mantenimientos = $this->tareamantManager->findByServicio($servicio);
        $statusmant = array();
        foreach ($mantenimientos as $tarea) {
            $statusmant[$tarea->getId()] = $this->tareamantManager->analizeMantenimiento($tarea);
        }

        $ordenes = $this->ordentrabajoManager->findByServicio($servicio);
        return $this->render('mante/TareaMantenimiento/show_servicio.html.twig', array(
            'menu' => $this->getListMenu($servicio),
            'servicio' => $servicio,
            'tareas' => $tareas,
            'statusmant' => $statusmant,
            'nuevas' => $nuevas,
            'encurso' => $encurso,
            'cerradas' => $cerradas,
            'peticiones' => $peticiones,
            'eventuales' => $eventuales,
            'ordenes' => $this->ordentrabajoManager->toArray($ordenes),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $tarea 
     * @return array $menu
     */
    private function getShowMenu($tarea, $userlogin)
    {
        $menu = array();
        if (($userlogin->getOrganizacion() == $tarea->getMantenimiento()->getPropietario()) ||
            $userlogin->getOrganizacion()->getTipoOrganizacion() == 1
        ) {
            if ($userlogin->isGranted('ROLE_TAREAS_MANT_EDITAR')) {
                $menu['Editar'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl('tareamant_edit', array(
                        'id' => $tarea->getId()
                    ))
                );
            }
            if ($userlogin->isGranted('ROLE_TAREAS_MANT_ELIMINAR')) {
                $menu['Eliminar'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => '#modalDelete',
                    'extra' => 'data-toggle=modal',
                );
            }
        }
        return $menu;
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $tarea 
     * @return array $menu
     */
    private function getListMenu($servicio)
    {
        $menu = array();

        $menu['Servicio'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('servicio_edit', array(
                'id' => $servicio->getId()
            ))
        );
        $menu['Asignar Plan'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('tareamant_newonly', array(
                'idOrg' => $servicio->getOrganizacion()->getId(),
                'id' => $servicio->getId()
            ))
        );
        $menu['Pedido Reparación'] = array(
            'imagen' => 'icon-plus icon-blue',
            'url' => $this->generateUrl('crm_peticion_new', array(
                'idServicio' => $servicio->getId()
            ))
        );
        $menu['Registrar'] = array(
            'imagen' => 'icon-edit icon-blue',
            'url' => $this->generateUrl('ordentrabajo_new', array('id' => $servicio->getOrganizacion()->getId(), 'servicio' => $servicio->getId()))
        );

        return $menu;
    }

    private function getUsuario($idOrg)
    {
        $organizacion = $this->organizacionManager->find($idOrg);
        if ($organizacion->getTipoOrganizacion() == 2) {  //distribuidor
            return $organizacion->getUsuarioMaster();
        } else {
            return $this->userloginManager->getUser();
        }
    }

    /**
     * @Route("/mante/tareamant/{idOrg}/new/{id}", name="tareamant_new",
     *     requirements={
     *         "idOrg": "\d+",
     *         "id": "\d+"
     *     },  
     *   )
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function newAction(Request $request, Organizacion $organizacion, $id)
    {
        //esto es para el bug de tarea de mant desde el distribuidor.
        $usuario = $this->getUsuario($organizacion->getId());
        $servicios = $this->servicioManager->findAllByUsuario($usuario);
        $options = array(
            'servicios' => $id == 0 ? $servicios : array($this->servicioManager->find($id)),
            'grupos' => $this->grupoServicioManager->findAsociadas($organizacion),
            'mantenimientos' => $this->mantenimientoManager->findAsociados($organizacion),
        );
        $form = $this->createForm(TareaMantenimientoType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->bread->pop();
            //obtengo los datos de servicio y mantenimientos.
            if ($data['mantenimiento'] == 0) {
                $this->setFlash('error', 'Debe seleccionar un  mantenimiento');
            } else {
                $mantenimiento = $this->mantenimientoManager->find($data['mantenimiento']);

                if ($data['servicio'] == '0') {  //es para toda la flota.
                    $servicios = $this->servicioManager->findAll($usuario);
                } elseif (substr($data['servicio'], 0, 1) == 'g') {   //es para un grupo de servicios
                    $grupo = $this->grupoServicioManager->find(intval(substr($data['servicio'], 1)));
                    $servicios = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                } else {   //es solo para un servicio
                    $servicios = array($this->servicioManager->find($data['servicio']));
                }
                $tareas = array();
                //grabo la tarea para todos los servicios seleccionados.
                foreach ($servicios as $servicio) {
                    $tmant = $this->tareamantManager->create();
                    $tmant->setServicio($servicio);
                    $tmant->setMantenimiento($mantenimiento);
                    $tmant->setIntervaloDias($data['intervaloDias']);
                    $tmant->setIntervaloKilometros($data['intervaloKilometros']);
                    $tmant->setIntervaloHoras($data['intervaloHoras']);

                    $tmant->setAvisoDias($data['avisoDias']);
                    $tmant->setAvisoKilometros($data['avisoKilometros']);
                    $tmant->setAvisoHoras($data['avisoHoras']);

                    if ($mantenimiento->getPorKilometros()) {
                        $tmant->setProximoKilometros(round($servicio->getUltOdometro() / 1000) + intval($data['intervaloKilometros']));
                    }
                    if ($mantenimiento->getPorHoras()) {
                        $tmant->setProximoHoras(round(($servicio->getUltHorometro() + $data['intervaloHoras'] * 60) / 60));
                    }
                    if ($mantenimiento->getPorDias()) {
                        $fecha = new \DateTime(date('Y-m-d', strtotime('+' . $data['intervaloDias'] . ' day')));
                        $tmant->setProximoDias($fecha);
                    }

                    $this->tareamantManager->save($tmant);
                    $tareas[] = $tmant;
                }
                $this->setFlash('success', 'Se ha creado la tarea de mantenimiento');
                return $this->render('mante/TareaMantenimiento/new_servicios.html.twig', array(
                    'tareas' => $tareas,
                    'porKms' => $mantenimiento->getPorKilometros(),
                    'porDias' => $mantenimiento->getPorDias(),
                    'porHoras' => $mantenimiento->getPorHoras(),
                ));
            }
        }
        $this->bread->pushPorto($request->getRequestUri(), "Nuevo Mantenimiento");
        return $this->render('mante/TareaMantenimiento/new.html.twig', array(
            'organizacion' => $organizacion,
            'servicios' => array('servicionombre' => 'Todos los equipos', 'servicios' => $servicios),
            'form' => $form->createView(),
            'id' => $id
        ));
    }

    /** para un solo servicio
     * @Route("/mante/tareamant/{idOrg}/newonly/{id}", name="tareamant_newonly",
     *     requirements={
     *         "idOrg": "\d+",
     *         "id": "\d+"
     *     },  
     *   )
     * @Method({"GET", "POST"})
     * @ParamConverter(name="organizacion", class="App:Organizacion", options={"id"="idOrg"})
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function newonlyAction(
        Request $request,
        Organizacion $organizacion,
        $id
    ) {

        //esto es para el bug de tarea de mant desde el distribuidor.
        $servicio = $this->servicioManager->find($id);
        $usuario = $this->getUsuario($organizacion->getId());
        $options = array(
            'servicios' => array($servicio),
            'grupos' => array(),
            'mantenimientos' => $this->mantenimientoManager->findAsociados($organizacion),
        );

        $form = $this->createForm(TareaMantenimientoType::class, null, $options);

        $this->bread->pushPorto($request->getRequestUri(), "Nuevo Mantenimiento");
        return $this->render('mante/TareaMantenimiento/new_only.html.twig', array(
            'organizacion' => $organizacion,
            'servicio' => $servicio,
            'form' => $form->createView(),
            'id' => $id
        ));
    }

    /**
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function neweventualAction(Request $request, $id)
    {

        $servicio = $this->servicioManager->find($id);
        $organizacion = $servicio->getOrganizacion();
        $productos = array();

        $options = array(
            'talleres' => $this->tallerManager->find($organizacion),
        );

        $form = $this->createForm(TareaMantenimientoEventualType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $request->get("mant");
            $productos = null;
            if (isset($data['productos'])) {
                //asocio los productos
                foreach ($data['productos'] as $arr) {
                    $producto = $this->productoManager->find($arr['nombre']);
                    $cantidad = $arr['cantidad'];
                    $deposito = $this->depositoManager->find($arr['deposito']);
                    $productos[] = array('producto' => $producto, 'cantidad' => $cantidad, 'deposito' => $deposito);
                    if ($cantidad != '' || $cantidad > 0) {
                        $stock = $this->stockManager->restarStock($deposito, $producto, $cantidad);
                    }
                }
            }

            $this->tareamantManager->registrarEventual($servicio, $data, $productos);
            $this->bread->pop();
            $this->setFlash('success', 'Se ha creado la tarea de mantenimiento');
            return $this->redirect($this->bread->getVolver());
        }

        $depositos = $this->depositoManager->findAllByOrganizacion($organizacion);
        $mecas = $this->mecanicoManager->findAll($organizacion);
        $this->bread->push($request->getRequestUri(), "Nuevo Mant. Eventual");
        return $this->render('mante/TareaMantenimiento/neweventual.html.twig', array(
            'organizacion' => $organizacion,
            'servicio' => $servicio,
            'productos' => $productos,
            'depositos' => $depositos,
            'mecanicos' => $mecas,
            'form' => $form->createView(),
        ));
    }

    /**
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function editeventualAction(
        Request $request,
        $id,
        TallerManager $tallerManager,
    ) {
        $tareaH = $this->tareamantManager->findHistoricoById($id);
        $organizacion = $tareaH->getServicio()->getOrganizacion();
        //esto es para el bug de tarea de mant desde el distribuidor.
        if ($organizacion->getTipoOrganizacion() == 2) {  //distribuidor
            $usuario = $organizacion->getUsuarioMaster();
        } else {
            $usuario = $this->userloginManager->getUser();
        }
        $options['talleres'] = $tallerManager->find($organizacion);
        $form = $this->createForm(TareaMantenimientoEventualEditType::class, $tareaH, $options);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->tareamantManager->saveHistorico($tareaH);
                $this->bread->pop();
            }
            $this->setFlash('success', 'Se ha actualizado la tarea de mantenimiento');
            return $this->redirect($this->bread->getVolver());
        }

        $this->bread->push($request->getRequestUri(), "Cambio Mant. Eventual");
        return $this->render('mante/TareaMantenimiento/editeventual.html.twig', array(
            'organizacion' => $organizacion,
            'tarea' => $tareaH,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/tareamant/proximoupdate", name="tareamant_proximo_update")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function proximoupdateAction(
        Request $request
    ) {

        $data = $request->get('mant_kms');
        if (!is_null($data)) {
            foreach ($data as $key => $value) {
                $tmant = $this->tareamantManager->find($key);
                $tmant->setProximoKilometros(intval($value));
                $this->tareamantManager->save($tmant);
            }
        }
        $data = $request->get('mant_hs');
        if (!is_null($data)) {
            foreach ($data as $key => $value) {
                $tmant = $this->tareamantManager->find($key);
                $tmant->setProximoHoras(intval($value));
                $this->tareamantManager->save($tmant);
            }
        }
        $data = $request->get('mant_dias');
        if (!is_null($data)) {
            foreach ($data as $key => $value) {
                $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp(date($value), true), null);
                $tmant = $this->tareamantManager->find($key);
                $tmant->setProximoDias($fecha);
                $this->tareamantManager->save($tmant);
            }
        }
        return $this->redirect($this->bread->getVolver());
    }

    /**
     * @Route("/mante/tareamant/{id}/edit", name="tareamant_edit",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_EDITAR")
     */
    public function editAction(
        Request $request,
        ServicioMantenimiento $tarea,
    ) {

        if (!$tarea) {
            throw $this->createNotFoundException('Código de Tarea de Mantenimiento no encontrado.');
        }
        $form = $this->createForm(TareaMantenimientoEditType::class, $tarea);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->bread->pop();
                $this->tareamantManager->save($tarea);
                $data = array(
                    'observacion' => sprintf('%s %s, %d', 'Se Modifica', 'Tarea de Mantenimiento', $tarea->getId()),
                    'costo' => null,
                    'fecha' => (new \DateTime())->format("d/m/Y H:i:s"),
                    'deposito' => null,
                    'taller' => null,
                );
                if ($tarea->getMantenimiento()->getPorDias()) {
                    $proxVto = $tarea->getProximoDias();
                    $data['dato_proximo'] = $proxVto;
                    $data['dato_proxima_fecha'] = $proxVto->format('d-m-Y');
                } elseif ($tarea->getMantenimiento()->getPorHoras()) {
                    $data['dato_proximo'] = $tarea->getProximoHoras();
                    $data['dato_realizacion'] = null;
                } elseif ($tarea->getMantenimiento()->getPorKilometros()) {
                    $data['dato_proximo'] = $tarea->getProximoKilometros();
                    $data['dato_realizacion'] = null;
                }
                $this->bread->pop();
                $this->tareamantManager->registrarTarea($tarea, $data);
                $this->setFlash('success', 'Los datos de la tarea de mantenimiento han sido actualizados');

                return $this->redirect($this->bread->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }
        $this->bread->push($request->getRequestUri(), "Editar Mantenimiento");
        return $this->render('mante/TareaMantenimiento/edit.html.twig', array(
            'form' => $form->createView(),
            'tarea' => $tarea,
            'status' => $this->tareamantManager->analizeMantenimiento($tarea),
        ));
    }

    /**
     * @Route("/mante/tareamant/query", name="tareamant_queryAJAX")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function queryAjaxAction(
        Request $request,
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        TareaMantManager $tareamantManager
    ) {

        //Se pone esta linea para desactivar los calculos de ment pendientes por ajax
        return new Response('', 200);

        $user = $userloginManager->getUser();
        // die('////<pre>' . nl2br(var_export('asd', true)) . '</pre>////');

        return new Response(json_encode($this->calcularPendientes($request, $user, $servicioManager, $tareamantManager)));
    }

    private function calcularPendientes(Request $request, $user, $servicioManager, $tareamantManager)
    {
        $servicios = $servicioManager->findAllByUsuario($user);
        $mante = $this->getMante($servicios, $tareamantManager);
        $countMant = count($mante) <= 10 ? count($mante) : '10+';
        $html = $this->render(
            'app/Template/alerta_mant.html.twig',
            array(
                'cantidad' => $countMant,
                'mantenimientos' => array_slice($mante, 0, 10),
                'organizacion' => $user->getOrganizacion()
            )
        );
        $htmlPorto = $this->render(
            'app/Template_aker/alerta_mant.html.twig',
            array(
                'cantidad' => $countMant,
                'mantenimientos' => array_slice($mante, 0, 10),
                'organizacion' => $user->getOrganizacion()
            )
        );
        if ($request->get('mantpend') == 0) {
            $nuevas = 0;
        } else {
            $nuevas = $countMant - $request->get('mantpend');
        }
        $request->getSession()->set('mantpend', $countMant);
        $request->getSession()->set('btnmantpend', str_replace('"', '\"', preg_replace('/^\s+|\n|\r|\s+$/m', '', $html)));
        return array(
            'cantidad' => $countMant,
            'nuevas' => $nuevas,
            'html' => $html,
            'html_aker' => $htmlPorto,
            'organizacion' => $user->getOrganizacion(),
        );
    }

    public function getMante($servicios, $tareamantManager)
    {
        $mante = array();
        foreach ($servicios as $servicio) {
            $tmant = $tareamantManager->findByServicio($servicio);
            foreach ($tmant as $tarea) {
                $status = $tareamantManager->analizeMantenimiento($tarea);
                if ($status['code'] >= 2) {
                    $mante[] = array(
                        'id' => $tarea->getid(),
                        'idServicio' => $servicio->getId(),
                        'status' => $status,
                        'message' => sprintf('%s hacer %s', $tarea->getServicio(), $tarea->getMantenimiento()->getNombre())
                    );
                }
            }
        }
        return $mante;
    }

    /**
     * @Route("/mante/tareamant/{id}/registrar", name="tareamant_registrar",
     *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_EDITAR")
     */
    public function registrarAction(
        Request $request,
        $id,
    ) {

        $tarea = $this->tareamantManager->find($id);
        if (!$tarea) {
            throw $this->createNotFoundException('Código de Tarea de Mantenimiento no encontrado.');
        }
        $dataTareAnt = array(
            'proximoHoras' => !is_null($tarea->getProximoHoras()),
            'proximoDias' => !is_null($tarea->getProximoDias()),
            'proximoKm' => !is_null($tarea->getProximoKilometros()),
            'status' => $this->tareamantManager->analizeMantenimiento($tarea),
            'odometro' => round($tarea->getServicio()->getUltOdometro() !== 0 ? $tarea->getServicio()->getUltOdometro() / 1000 : 0),
            'horometro' => $tarea->getServicio()->getUltHorometro() !== 0 ? round($tarea->getServicio()->getUltHorometro() / 60) : 0,
        );
        $option = array(
            'odometro' => round($tarea->getServicio()->getUltOdometro() !== 0 ? $tarea->getServicio()->getUltOdometro() / 1000 : 0),
            'horometro' => $tarea->getServicio()->getUltHorometro() !== 0 ? round($tarea->getServicio()->getUltHorometro() / 60) : 0,
            'porHoras' => $tarea->getMantenimiento()->getPorHoras(),
            'porDias' => $tarea->getMantenimiento()->getPorDias(),
            'porKilometros' => $tarea->getMantenimiento()->getPorKilometros(),
            'talleres' => $this->tallerManager->find($tarea->getServicio()->getOrganizacion()),
            'depositos' => $this->depositoManager->findAllByOrganizacion($tarea->getServicio()->getOrganizacion()),
            'sectores' => $this->sectorTallerManager->findAll($tarea->getServicio()->getOrganizacion()),
            'centroscosto' => $this->centrocostoManager->findAllByOrganizacion($tarea->getServicio()->getOrganizacion()),
        );
        $form = $this->createForm(TareaMantenimientoRegistroType::class, null, $option);
        $form->handleRequest($request);


        if ($request->getMethod() == 'POST') {
            $data = $request->get('mant');
            $ot = $this->setOt($data, $tarea);
            if ($ot) {
                $this->setFlash('success', 'La orden de trabajo se creo correctamente');
                $this->bread->pop();
                return $this->redirect($this->generateUrl('ordentrabajo_step2', array('id' => $ot->getId())));
            } else {
                $this->setFlash('error', 'Error al crear la orden de trabajo');
            }
        }
        $this->bread->push($request->getRequestUri(), "Registro de mantenimiento");

        $mecas = $this->mecanicoManager->findAll($tarea->getServicio()->getOrganizacion());
        return $this->render('mante/TareaMantenimiento/registro.html.twig', array(
            'mecanicos' => $mecas,
            'form' => $form->createView(),
            'tarea' => $tarea,
            'status' => $this->tareamantManager->analizeMantenimiento($tarea),
        ));
    }

    private function setOt($data, $tarea)
    {
        $fecha = isset($data['dato_realizacion_fecha']) ? new \DateTime($this->utilsManager->datetime2sqltimestamp($data['dato_realizacion_fecha'], true)) :
            new \DateTime($this->utilsManager->datetime2sqltimestamp($data['fecha'], false));
        $taller = isset($data['taller']) && ($data['taller'] != '') ? $this->tallerManager->findOne($data['taller']) : null;
        $deposito = isset($data['deposito']) && ($data['deposito'] != '') ? $this->depositoManager->find($data['deposito']) : null;
        $sector = isset($data['sector']) && ($data['sector'] != '') ? $this->sectorTallerManager->find($data['sector']) : null;
        $centrocosto = isset($data['centrocosto']) && ($data['centrocosto'] != '') ? $this->centrocostoManager->find($data['centrocosto']) : null;

        $ordenTrabajo = $this->ordentrabajoManager->create($tarea->getServicio()->getOrganizacion());
        $ordenTrabajo->setServicio($tarea->getServicio());
        $ordenTrabajo->setFecha($fecha);
        $ordenTrabajo->setDescripcion($data['observacion']);
        $ordenTrabajo->setTaller($taller);
        $ordenTrabajo->setDeposito($deposito);
        $ordenTrabajo->setTallerSector($sector);
        $ordenTrabajo->setCentroCosto($centrocosto);
        //TODO: El dato_realizacion puede no ser odometro, depende de que se haya ingresado en el formulario. Deberiamos ir a buscarlo a mongo para traer el odometro correcto.
        $ordenTrabajo->setTipo(1); //preventivo
        $ordenTrabajo->setOdometro(!is_null($ordenTrabajo->getServicio()->getUltOdometro()) ?  intval($ordenTrabajo->getServicio()->getUltOdometro() / 1000) : null);
        $ordenTrabajo->setHorometro(!is_null($ordenTrabajo->getServicio()->getUltHorometro()) ? intval($ordenTrabajo->getServicio()->getUltHorometro() / 60) : null);

        if ($tarea->getMantenimiento()->getPorKilometros()) {
            $ordenTrabajo->setOdometro(isset($data['dato_realizacion']) ? intval($data['dato_realizacion']) : null);
        } elseif ($tarea->getMantenimiento()->getPorHoras()) {
            $ordenTrabajo->setHorometro(isset($data['dato_realizacion']) ? intval($data['dato_realizacion']) : null);
        }

        //Este if no hace nada importante... setea el prosimohoras en caso que venga...
        if (isset($data['dato_proximo'])) {
            if (intval($data['dato_proximo']) > 0) {
                $tarea->setProximoHoras(intval($data['dato_proximo']));
            } else {
                $tarea->setProximoHoras(0);
            }
        } else {
            $tarea->setProximoHoras(null);
        }

        $ot = $this->ordentrabajoManager->save($ordenTrabajo);

        $crear = $this->ordentrabajoManager->addMantenimiento($ot, $tarea, 'Se Crea');
        return $ot;
    }

    /**
     * @Route("/mante/tareamant/{id}/delete", name="tareamant_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MANT_ELIMINAR")
     */
    public function deleteAction(
        Request $request,
        ServicioMantenimiento $tarea,
    ) {

        $form = $this->createDeleteForm($tarea->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->bread->pop();
            $tareaOld = $tarea;
            $this->tareamantManager->delete($tarea->getId());
            $this->bitacoraservicioManager->deleteTareaMant($this->userloginManager->getUser(), $tareaOld->getServicio(), $tareaOld);
            $this->setFlash('success', 'La tarea se eliminó correctamente');
        }
        return $this->redirect($this->bread->getVolver());
    }

    /**
     * @Route("/mante/tareamant/filtrar", name="tareamant_filtrar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function filtrarAction(
        Request $request,
        CentroCostoManager $centrocostoManager,
        InformeUtilsManager $informeUtilsManager,
    ) {
        $tmp = array();
        parse_str($request->get('formFlota'), $tmp);
        $form = $tmp['mant'];
        $organizacion = $this->userloginManager->getOrganizacion();
        if ($form) {
            $servicios = $this->getData($form, $informeUtilsManager, $centrocostoManager);
            $data = $this->tareamantManager->filtrar(
                $servicios['data']['servicios'],
                $servicios['estado']
            );  //parche
        }
        return new Response(json_encode(array(
            'html' => $this->renderView('mante/TareaMantenimiento/list_content.html.twig', array(
                'tmantenimientos' => $data,
                'moduloCentroCosto' => $this->moduloCentroCosto,
                'consulta' => $servicios['consulta'],
                'organizacion' => $organizacion
            )),
            'filtro' => $servicios['filtro'],
        )));
    }

    /**
     * @Route("/mante/tareamant/exportar", name="tareamant_exportar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function exportarAction(
        Request $request,
        ExcelManager $excelManager,
        CentroCostoManager $centrocostoManager,
        InformeUtilsManager $informeUtilsManager,
    ) {

        $consulta = json_decode($request->get('consulta'), true);

        $organizacion = $this->userloginManager->getOrganizacion();
        if (!is_null($consulta)) {
            $servicios = $this->getData($consulta, $informeUtilsManager, $centrocostoManager);
            $data = $this->tareamantManager->filtrar(
                $servicios['data']['servicios'],
                $servicios['estado']
            );  //parche
            $xls = $excelManager->create("Informe de Flota");
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $consulta['servicionombre'],
                'C3' => 'Estado',
                'D3' => $consulta['strEstado'],
            ));
            $this->moduloCentroCosto = $this->userloginManager->isGranted('ROLE_APMON_PRESTACION_ADMIN');
            if ($this->moduloCentroCosto) {
                $xls->setHeaderInfo(array(
                    'C4' => 'Centro de Costo',
                    'D4' => $consulta['strCentroCosto'],
                ));
            }

            $xls->setBar(6, array(
                'A' => array('title' => 'Servicio', 'width' => 20),
                'B' => array('title' => 'Centro de Costo', 'width' => 20),
                'C' => array('title' => 'Ult. Reporte', 'width' => 20),
                'D' => array('title' => 'Odómetro Actual', 'width' => 20),
                'E' => array('title' => 'Horometro Actual', 'width' => 20),
                'F' => array('title' => 'Actividades', 'width' => 20),
                'G' => array('title' => 'Estado', 'width' => 20),
            ));
            $i = 7;
            foreach ($data as $value) {
                if ($value['tareas']) {
                    foreach ($value['tareas'] as $tarea) {
                        $xls->setRowValues($i, array(
                            'A' => array('value' => $value['servicio']->getNombre()),
                            'B' => array('value' => !is_null($value['servicio']->getCentroCosto()) ? $value['servicio']->getCentroCosto()->getNombre() : '---'),
                            'C' => array('value' => !is_null($value['servicio']->getUltFechaHora()) ? $value['servicio']->getUltFechaHora()->format('d/m/Y H:i') : '---'),
                            'D' => array('value' => !is_null($value['servicio']->getUltOdometro()) ? $value['servicio']->getUltOdometro() / 1000 : '---'),
                            'E' => array('value' => !is_null($value['servicio']->getUltHorometro()) ? $value['servicio']->formatHorometro() : '---'),
                            'F' => array('value' => !is_null($tarea['plan']) ? $tarea['plan']->getMantenimiento()->getNombre() : '---'),
                            'G' => array('value' => !is_null($tarea['status']) ? $tarea['status']['message'] : '---'),
                        ));
                        $i++;
                    }
                }
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=mantenimietos.xls');

            // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('tareamant_list', array('idOrg' => $organizacion->getId())));
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    protected function getData($form, $informeUtilsManager, $centrocostoManager)
    {
        $servicios = null;
        $centrocosto = null;
        $strServicio = 'Todos los servicios';
        $strCC = 'Todos los centro de costos';
        $strEstado = 'Todos los estados';
        $consulta = array(
            'servicio' => -1,
            'centrocosto' => -1,
            'estado' => 0,
        );
        if (($form['servicio']) > -1) {
            //obtengo los servicios a incluir en el informe.
            $servicios = $informeUtilsManager->obtenerServicios($form['servicio']);
            $consulta['servicionombre'] = $servicios['servicionombre'];
            $consulta['servicio'] = $form['servicio'];
            $strServicio = $servicios['servicionombre'];
        } else {
            $servicios = null;
            $consulta['servicionombre'] = '---';
            $strServicio = '---';
        }
        $this->moduloCentroCosto = $this->userloginManager->isGranted('ROLE_APMON_PRESTACION_ADMIN');
        if ($this->moduloCentroCosto) {
            if (($form['centrocosto']) > -1) {
                //obtengo los servicios a incluir en el informe.
                $centrocosto = $centrocostoManager->find(intval($form['centrocosto']));
                $consulta['centrocosto'] = $centrocosto->getId();
                $consulta['strCentroCosto'] = $centrocosto->getNombre();
                $strCC = $centrocosto->getNombre();
                $servicios['servicios'] = $centrocosto->getServicios();
            } else {
                $centrocosto = null;
                $strCC = '---';
                $consulta['strCentroCosto'] = $strCC;
            }
        }
        $servicios['servicios'] = ($servicios == null && $centrocosto == null) ? $informeUtilsManager->obtenerServicios('0')['servicios'] : $servicios['servicios'];

        $estado = intval($form['estado']);
        if ($estado == 3) {
            $consulta['estado'] = 3;
            $strEstado = "Vencidos";
        } elseif ($estado == 2) {
            $consulta['estado'] = 2;
            $strEstado = "Por Vencer";
        } else {
            $estado = null;
            $strEstado = "Todos";
        }
        $consulta['strEstado'] = $strEstado;


        $filtro = sprintf('<b>%s</b> | <b>%s</b> | <b>%s</b> ', $strServicio, $strCC, $strEstado);

        return array(
            'data' => $servicios,
            'filtro' => $filtro,
            'estado' => $estado,
            'consulta' => $consulta
        );
    }

    /**
     * @Route("/mante/tareamant/getservicios", name="mante_tareamant_getservicios")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function getserviciosAction(Request $request, InformeUtilsManager $informeUtilsManager)
    {
        $servicio = $request->get('servicio');
        $arrServicios = $informeUtilsManager->obtenerServicios($servicio);
        //  die('////<pre>' . nl2br(var_export(count($arrServicios['servicios']), true)) . '</pre>////');
        $html = $this->renderView('mante/TareaMantenimiento/servicios.html.twig', array(
            'servicios' => $arrServicios,

        ));
        return new Response(json_encode(array('status' => 'OK', 'html' => $html, 'servicionombre' => $arrServicios['servicionombre'])), 200);


        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/mante/tareamant/gettemplatetarea", name="mante_tareamant_gettemplatetarea", methods={"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_VER")
     */
    public function gettemplatetareaAction(
        Request $request,
        TallerManager $tallerManager,
        DepositoManager $depositoManager,
        SectorTallerManager $sectorTallerManager,
    ) {
        $id = (int) $request->get('servicio');
        $tipoMant = (int) $request->get('tipoMantenimiento');
        $servicio = $this->servicioManager->find($id);
        $organizacion = $servicio->getOrganizacion();

        $option = [
            'odometro' => round($servicio->getUltOdometro() !== 0 ? $servicio->getUltOdometro() / 1000 : 0),
            'horometro' => $servicio->getUltHorometro() !== 0 ? round($servicio->getUltHorometro() / 60) : 0,
            'talleres' => $tallerManager->find($organizacion),
            'depositos' => $depositoManager->findAllByOrganizacion($organizacion),
            'sectores' => $sectorTallerManager->findAll($organizacion),
            'centroscosto' => $this->centrocostoManager->findAllByOrganizacion($servicio->getOrganizacion()),
        ];

        $formTarea = $this->createForm(TareaMantenimientoRegistroType::class, null, $option);

        $html = $this->renderView('mante/TareaMantenimiento/dato_proximo.html.twig', [
            'formTarea' => $formTarea->createView(),
            'tipoMant' => $tipoMant,
        ]);

        return new Response(json_encode(['status' => 'OK', 'html' => $html]), 200);
    }

    /**
     * @Route("/mante/tareamant/newajax", name="tareamant_newajax")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function newajaxAction(Request $request)
    {

        $tmp = array();
        $id = (int)$request->get('servicio');
        $servicio = $this->servicioManager->find($id);
        parse_str($request->get('form'), $tmp);
        $data = $tmp['mant'];
        $mantenimiento = $this->mantenimientoManager->find((int)$data['mantenimiento']);

        if (!$servicio || !$mantenimiento) { //si no existe alguno de los 2 que no haga nada
            return new Response(json_encode(array(
                'status' => 'ERROR', 'html' => 'Error',
            )), 200);
        }

        $tmant = $this->tareamantManager->create();
        $tmant->setServicio($servicio);
        $tmant->setMantenimiento($mantenimiento);
        $tmant->setIntervaloDias((int)$data['intervaloDias']);
        $tmant->setIntervaloKilometros((int)$data['intervaloKilometros']);
        $tmant->setIntervaloHoras((int)$data['intervaloHoras']);
        //die('////<pre>' . nl2br(var_export($data['intervaloDias'], true)) . '</pre>////');

        $tmant->setAvisoDias((int)$data['avisoDias']);
        $tmant->setAvisoKilometros((int)$data['avisoKilometros']);
        $tmant->setAvisoHoras((int)$data['avisoHoras']);

        if ($mantenimiento->getPorKilometros()) {
            $tmant->setProximoKilometros(round($servicio->getUltOdometro() / 1000) + intval($data['intervaloKilometros']));
        }
        if ($mantenimiento->getPorHoras()) {
            //$valor = round(($servicio->getUltHorometro() + (intval($data['intervaloHoras']) * 60)) / 60);
            $valor = intval($data['dato_proximo']);
            $tmant->setProximoHoras($valor);
        }
        if ($mantenimiento->getPorDias()) {
            $fecha = new \DateTime(date('Y-m-d', strtotime('+' . $data['intervaloDias'] . ' day')));
            $tmant->setProximoDias($fecha);
        }

        try {
            $tmant = $this->tareamantManager->save($tmant);
            $this->bitacoraservicioManager->createTareaMant($this->userloginManager->getUser(), $servicio, $tmant);
            if ($this->isCreatedOt($data)) { //quiere decir que viene adjuntada una tarea para hacer (newonly)
                $this->setOt($data, $tmant);
                $arr = array(
                    'status' => 'OK',
                    "id" => $id,
                );
            } else {
                $arr = array(
                    'status' => 'WARNING',
                    "id" => $id,
                );
            }
            return new Response(json_encode($arr), 200);
        } catch (Exception $e) {
            return new Response(json_encode(array(
                'status' => 'ERROR', 'html' => 'Error',
            )), 404);
        }
    }

    private function isCreatedOt($data)
    {
        $setOT = false;
        if ($data['fecha'] != '') {
            if ($data['intervaloDias'] != '' && $data['avisoDias'] != '') {
                $setOT = true;
            } elseif ($data['intervaloKilometros'] != '' && $data['avisoKilometros'] != '') {
                $setOT = true;
            } elseif ($data['intervaloHoras'] != '' && $data['avisoHoras'] != '') {
                $setOT = true;
            } else {
                $setOT = false;
            }
        }

        return $setOT;
    }

    /**
     * @Route("/mante/tareamant/getdatapopover", name="tareamant_getdatapopover")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TAREAS_MANT_AGREGAR")
     */
    public function getdatapopoverAction(Request $request)
    {
        $tmp = array();
        $id = (int)$request->get('id');
        $tarea = $this->tareamantManager->find($id);
        try {
            $last = $this->tareamantManager->getLastHistorico($tarea);
            $html = $this->renderView('mante/TareaMantenimiento/data_popover.html.twig', [
                'last' => $last,
                'tarea' => $tarea,
            ]);

            return new Response(json_encode(array('html' => $html)), 200);
        } catch (Exception $e) {
            return new Response(json_encode(array(
                'status' => 'ERROR', 'html' => 'Error',
            )), 404);
        }
    }
}
