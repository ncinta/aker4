<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\mante\ActividadType;
use App\Form\mante\ActividadProductoType;
use App\Entity\Actividad;
use App\Entity\ActividadProducto;
use App\Entity\Producto;
use App\Entity\Mantenimiento;
use App\Model\app\ActividadManager;
use App\Model\app\ActividadProductoManager;
use App\Model\app\ProductoManager;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Description of ActividadController
 *
 * @author nicolas
 */
class ActividadController extends AbstractController
{

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function getSecurityContext()
    {
        return $this->get('security.token_storage');
    }

    /**
     * @Route("/mante/actividad/{id}/new", name="actividad_new",
     *  *     requirements={
     *         "id": "\d+"
     *     }, 
     * )
     * @Method({"GET", "POST"})
     */
    public function newAction(
        Request $request,
        Mantenimiento $mantenimiento,
        BreadcrumbManager $breadcrumbManager,
        ProductoManager $productoManager,
        ActividadManager $actividadManager
    ) {

        $user = $this->getSecurityContext()->getToken()->getUser();
        $organizacion = $user->getOrganizacion();

        $productos = $productoManager->findAllByOrganizacion($organizacion);
        if (!$mantenimiento) {
            throw $this->createNotFoundException('Mantenimiento no encontrado.');
        }
        //creo el producto
        $actividad = $actividadManager->create($mantenimiento);
        $form = $this->createForm(ActividadType::class, $actividad);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $request->get("actividad");
            //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
            //creo la actividad
            $actividad = new Actividad();
            $actividad->setNombre($data['nombre']);
            $actividad->setMantenimiento($mantenimiento);

            if ($actividadManager->save($actividad)) {
                $breadcrumbManager->pop();
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nueva Activividad");
        return $this->render('mante/Actividad/new.html.twig', array(
            'actividad' => $actividad,
            'mantenimiento' => $mantenimiento,
            'productos' => $productos,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/actividad/{id}/edit", name="actividad_edit",
     *     requirements={
     *         "id": "\d+",
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function editAction(
        Request $request,
        Actividad $actividad,
        BreadcrumbManager $breadcrumbManager,
        ProductoManager $productoManager,
        ActividadManager $actividadManager,
        UserLoginManager $userloginManager
    ) {
        //creo el producto
        $form = $this->createForm(ActividadType::class, $actividad);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($actividadManager->save($actividad)) {
                $breadcrumbManager->pop();
                return $this->redirect($breadcrumbManager->getVolver());
            }
        }
        $breadcrumbManager->push($request->getRequestUri(), "Edición");

        $organizacion = $userloginManager->getOrganizacion();
        $productos = $productoManager->findAllByOrganizacion($organizacion);

        return $this->render('mante/Actividad/edit.html.twig', array(
            'actividad' => $actividad,
            'mantenimiento' => $actividad,
            'form' => $form->createView(),
            'productos' => $productos
        ));
    }

    /**
     * @Route("/mante/actividad/findproductos", name="actividad_findproductos",
     *     requirements={
     *         "id": "\d+",
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function findproductosAction(Request $request, ActividadManager $actividadManager)
    {
        $actividad = $actividadManager->find($request->get('id'));
        $arr_data = array();
        if (!is_null($actividad)) {
            foreach ($actividad->getProductos() as $producto) {
                $arr_data[$producto->getProducto()->getId()] = $producto->getProducto()->getNombre();
            }
        }
        return new Response(json_encode($arr_data));
    }

    /**
     * @Route("/mante/actividad/{id}/newproducto", name="actividad_producto_new",
     *     requirements={
     *         "id": "\d+",
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function newProductoAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        ActividadManager $actividadManager,
        ActividadProductoManager $actividadProductoManager
    ) {

        $actividad = $actividadManager->find($id);
        if (!$actividad) {
            throw $this->createNotFoundException('Mantenimiento no encontrado.');
        }

        //creo el producto
        $actProducto = new ActividadProducto();
        $actProducto->setActividad($actividad);
        $form = $this->createForm(ActividadProductoType::class, $actProducto);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $actProducto = $actividadProductoManager->save($actProducto);
            $breadcrumbManager->pop();
            return $this->redirect($breadcrumbManager->getVolver());
        }
        $breadcrumbManager->push($request->getRequestUri(), "Nuevo Producto");
        return $this->render('apmon/Actividad/newProducto.html.twig', array(
            'actividad' => $actividad,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/mante/actividad/{id}/deleteproducto", name="actividad_producto_delete",
     *     requirements={
     *         "id": "\d+",
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_TALLER_EDITAR")
     */
    public function deleteProductoAction(
        Request $request,
        $id,
        BreadcrumbManager $breadcrumbManager,
        ActividadProductoManager $actividadProductoManager
    ) {

        $actividad = $actividadProductoManager->find($id);
        $idM = $actividad->getActividad()->getMantenimiento();

        if ($actividadProductoManager->delete($actividad)) {
            $this->setFlash('success', 'Producto eliminado del sistema.');
        } else {
            $this->setFlash('error', 'Error al eliminar el producto del sistema.');
        }
        $breadcrumbManager->push($request->getRequestUri(), "Eliminar");
        $breadcrumbManager->pop();
        return $this->redirect($breadcrumbManager->getVolver());
    }
}
