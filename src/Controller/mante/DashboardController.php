<?php

namespace App\Controller\mante;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Peticion;
use App\Entity\Servicio;
use App\Entity\Organizacion;
use App\Entity\FiltroPeticion;
use App\Form\crm\PeticionType;
use App\Form\crm\PeticionEditHeadType;
use App\Form\crm\PeticionFilter;
use App\Form\crm\PanelType;
use App\Form\crm\PeticionEditType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UsuarioManager;
use App\Model\app\UtilsManager;
use App\Model\app\ServicioManager;
use App\Model\crm\PeticionManager;
use App\Model\crm\CategoriaManager;
use App\Model\app\CentroCostoManager;
use App\Model\crm\EstadoManager;
use App\Model\crm\NotificadorManager;
use App\Model\app\OrdenTrabajoManager;
use App\Model\app\MantenimientoManager;
use App\Model\app\IndiceManager;
use Knp\Snappy\Pdf;

/**
 * Description of DashboardController
 *
 * @author nicolas
 */
class DashboardController extends AbstractController
{

    private $cantidad = 5; //cantidad de divisores que quiero para mostrar en el grafico de panel
    private $breadcrumbManager;
    private $userloginManager;
    private $servicioManager;
    private $peticionManager;
    private $usuarioManager;
    private $estadoManager;
    private $notificadorManager;
    private $categoriaManager;
    private $centroCostoManager;
    private $utilsManager;
    private $mantenimientoManager;
    private $ordentrabajoManager;
    private $indiceManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userloginManager,
        EstadoManager $estadoManager,
        NotificadorManager $notificadorManager,
        ServicioManager $servicioManager,
        PeticionManager $peticionManager,
        UsuarioManager $usuarioManager,
        CategoriaManager $categoriaManager,
        CentroCostoManager $centroCostoManager,
        UtilsManager $utilsManager,
        MantenimientoManager $mantenimientoManager,
        OrdenTrabajoManager $ordentrabajoManager,
        IndiceManager $indiceManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->peticionManager = $peticionManager;
        $this->usuarioManager = $usuarioManager;
        $this->estadoManager = $estadoManager;
        $this->notificadorManager = $notificadorManager;
        $this->categoriaManager = $categoriaManager;
        $this->centroCostoManager = $centroCostoManager;
        $this->utilsManager = $utilsManager;
        $this->mantenimientoManager = $mantenimientoManager;
        $this->ordentrabajoManager = $ordentrabajoManager;
        $this->indiceManager = $indiceManager;
    }

    /**
     * @Route("/mante/{idOrg}/dashboard", name="mante_dashboard",
     *     requirements={
     *         "idOrg": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"}) 
     * @ParamConverter("id", class="App:Organizacion", options={"id" = "idOrg"})
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function dashboardAction(Request $request, Organizacion $organizacion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Dashboard");
        $hoy = new \DateTime();
        $hasta = new \DateTime();
        $ayer = new \DateTime();
        $desde = new \DateTime();
        date_modify($desde, '-1 month');
        date_modify($ayer, '-1 day');
        $ots = $this->ordentrabajoManager->findAll($organizacion, null, $desde, $hasta);
        $peticiones = $this->peticionManager->findAllEstadoByOrg($organizacion, $desde, $hasta);

        $user = $this->userloginManager->getUser();
        $servicios = $this->servicioManager->findAllByOrganizacion($user->getOrganizacion());
        $indices = $this->indiceManager->getIndicesServicios($servicios, $desde, $hasta);

        $form = $this->createForm(PanelType::class);

        return $this->render('mante/Dashboard/show_panel.html.twig', array(
            'filter' => $form->createView(),
            'asignaciones' => $this->getAsignaciones($peticiones),
            'atrasadas' => $this->getAtrasadas($peticiones, $desde, $hasta, true),
            'organizacion' => $organizacion,
            'peticiones' => $peticiones,
            'desde' => date_modify($desde, '-1 month'),
            'hasta' => $hasta,
            'indices_json' => json_encode($this->indiceManager->ordenarIndices($indices))
        ));
    }

    /**
     * @Route("/mante/{idOrganizacion}/ajaxpanel", name="mante_ajaxpanel",
     *     requirements={
     *         "idOrganizacion": "\d+"
     *     },
     * )
     * @Method({"GET", "POST"})  
     * @ParamConverter("id", class="App:Organizacion", options={"id" = "idOrganizacion"})
     * @IsGranted("ROLE_PETICION_VER")
     */
    public function ajaxpanelAction(Request $request, Organizacion $organizacion)
    {

        $fecha_desde = $this->utilsManager->datetime2sqltimestamp($request->get('desde'), false);
        $desde = new \DateTime($fecha_desde, new \DateTimeZone($organizacion->getTimezone()));
        $fecha_hasta = $this->utilsManager->datetime2sqltimestamp($request->get('hasta'), false);
        $hasta = new \DateTime($fecha_hasta, new \DateTimeZone($organizacion->getTimezone()));
        $hasta2 = new \DateTime($fecha_hasta, new \DateTimeZone($organizacion->getTimezone()));
        $ots = $this->ordentrabajoManager->findAll($organizacion, null, $desde, $hasta);
        $peticiones = $this->peticionManager->findAllEstadoByOrg($organizacion, $desde, $hasta);
        $petBarra = $this->getAtrasadas($peticiones, $desde, $hasta, true);

        $asignaciones = $this->getAsignaciones($peticiones);

        $user = $this->userloginManager->getUser();
        $servicios = $this->servicioManager->findAllByOrganizacion($user->getOrganizacion());
        $indices = $this->indiceManager->getIndicesServicios($servicios, $desde, $hasta2);
        //die('////<pre>' . nl2br(var_export(count($indices), true)) . '</pre>////');

        $asignacionesHtml = $this->renderView('mante/Dashboard/asignaciones.html.twig', array(
            'asignaciones' => $asignaciones,
        ));



        return new Response(json_encode(array(
            'status' => 'OK',
            'indices_json' => count($indices) > 0 ? json_encode($this->indiceManager->ordenarIndices($indices)) : null,
            'atrasadas' => $petBarra,
            'petLength' => count($peticiones),
            'otLength' => count($ots),
            'asignacionesHtml' => $asignacionesHtml,

        )));
    }

    private function getCantidad($ots)
    {
        $cantidad['correctivo'] = array();
        $cantidad['preventivo'] = array();
        $cantidad['retrabajo'] = array();

        foreach ($ots as $orden) {
            if ($orden->getTipo() == 0) { //correctivo
                $cantidad['correctivo'][] = $orden;
            } elseif ($orden->getTipo() == 1) { //preventivo
                $cantidad['preventivo'][] = $orden;
            } elseif ($orden->getTipo() == 2) {
                $cantidad['retrabajo'][] = $orden;
            }
        }
        return $cantidad;
    }

    private function getAsignaciones($peticiones)
    {
        $arr = array();
        foreach ($peticiones as $peticion) {
            $arr[] = $peticion->getAsignadoA()?$peticion->getAsignadoA()->getNombre():'Sin asignar';
        }
        $indicadores = $this->initBarras($arr);
        foreach ($peticiones as $peticion) {
            $nombreAsignado = $peticion->getAsignadoA()?$peticion->getAsignadoA()->getNombre():'Sin asignar';
            if ($peticion->getEstado()->getIsDefault()) { //peticion nueva
                $indicadores[$nombreAsignado]['nueva'] = $indicadores[$nombreAsignado]['nueva'] + 1;
            } elseif ($peticion->getEstado()->getIsCurso()) { //peticion en curso
                $indicadores[$nombreAsignado]['encurso'] = $indicadores[$nombreAsignado]['encurso'] + 1;
            } elseif ($peticion->getEstado()->getIsCerrado()) {
                $indicadores[$nombreAsignado]['cerrada'] = $indicadores[$nombreAsignado]['cerrada'] + 1;
            }
            $indicadores[$nombreAsignado]['retrabajo'] = $peticion->getTipo() == 1 ? $indicadores[$nombreAsignado]['retrabajo'] + 1 : $indicadores[$nombreAsignado]['retrabajo'];
            $indicadores[$nombreAsignado]['normal'] = $peticion->getTipo() == 0 ? $indicadores[$nombreAsignado]['normal'] + 1 : $indicadores[$nombreAsignado]['normal'];
        }
        return $indicadores;
    }

    private function initBarras($indicadores)
    {
        $arr = array();
        foreach ($indicadores as $value) {
            $arr[$value]['nueva'] = 0;
            $arr[$value]['encurso'] = 0;
            $arr[$value]['cerrada'] = 0;
            $arr[$value]['retrabajo'] = 0;
            $arr[$value]['normal'] = 0;
        }
        return $arr;
    }

    private function getAtrasadas($peticiones, $desde, $hasta, $now)
    {
        $diferencia = $hasta->diff($desde);
        $totalDias = abs($diferencia->format('%R%a') * 1); //obtengo la diferencia entre las 2 fechas en dias
        $divisor = $totalDias / $this->cantidad;
        $indicadores = $this->getIndicadores($divisor, $hasta, $now);
        $arrAtrasadas = $this->armarArray($indicadores, $peticiones);
        return $arrAtrasadas;
    }

    private function getIndicadores($divisor, $hasta, $now)
    {
        $indicadores = array();
        for ($i = $this->cantidad - 1; $i >= 0; $i--) {
            $indicadores[$i] = $hasta->sub(new \DateInterval('P' . round($divisor) . 'D'))->format('d-m-Y H:i');
        }
        if ($now) {
            array_push($indicadores, (new \DateTime())->format('d-m-Y H:i'));
        } else {
            array_push($indicadores, $hasta->format('d-m-Y H:i'));
        }
        ksort($indicadores);
        return $indicadores;
    }

    private function armarArray($indic, $peticiones)
    {
        $arrOrden = array();
        $initBarras = $this->initBarras($indic);
        $arr = $this->cargarBarras($indic, $peticiones, $initBarras);
        foreach ($arr as $key => $value) {
            $fecha = $key;
            foreach ($value as $key => $value) {
                $arrOrden[$key][$fecha] = $value;
            }
        }

        return $arrOrden;
    }

    private function cargarBarras($indicadores, $peticiones, $arr)
    {
        foreach ($peticiones as $peticion) {
            foreach ($indicadores as $key => $value) {
                $anterior = $key + 1;
                $fechaPost = ($anterior < count($indicadores)) ? new \DateTime($indicadores[$anterior]) : null;
                if ($peticion->getCreated_at() >= new \DateTime($value) && $peticion->getCreated_at() <= $fechaPost) {
                    if ($peticion->getEstado()->getIsDefault()) { //peticion nueva
                        $arr[$value]['nueva'] = $arr[$value]['nueva'] + 1;
                    } elseif ($peticion->getEstado()->getIsCurso()) { //peticion en curso
                        $arr[$value]['encurso'] = $arr[$value]['encurso'] + 1;
                    } elseif ($peticion->getEstado()->getIsCerrado()) {
                        $arr[$value]['cerrada'] = $arr[$value]['cerrada'] + 1;
                    }
                    $arr[$value]['retrabajo'] = $peticion->getTipo() == 1 ? $arr[$value]['retrabajo'] + 1 : $arr[$value]['retrabajo'];
                    $arr[$value]['normal'] = $peticion->getTipo() == 0 ? $arr[$value]['normal'] + 1 : $arr[$value]['normal'];
                }
            }
        }
        return $arr;
    }
}
