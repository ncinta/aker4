<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use de mapas
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\ReferenciaManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\TipoServicioManager;
use App\Model\app\GmapsManager;
use App\Model\app\ReferenciaFormManager;
use App\Model\app\GeocoderManager;

class CercaniaController extends AbstractController
{

    private $breadcrumbManager;
    private $referenciaManager;
    private $userloginManager;
    private $servicioManager;
    private $tiposervicioManager;
    private $gmapsManager;
    private $geocoderManager;
    private $referenciaFormManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        GmapsManager $gmapsManager,
        GeocoderManager $geocoderManager,
        ReferenciaManager $referenciaManager,
        UserLoginManager $userlogin,
        ReferenciaFormManager $referenciaFormManager,
        ServicioManager $servicioManager,
        TipoServicioManager $tiposervicioManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->referenciaManager = $referenciaManager;
        $this->userloginManager = $userlogin;
        $this->servicioManager = $servicioManager;
        $this->tiposervicioManager = $tiposervicioManager;
        $this->gmapsManager = $gmapsManager;
        $this->geocoderManager = $geocoderManager;
        $this->referenciaFormManager = $referenciaFormManager;
    }

    /**
     * @Route("/apmon/mapa/cercaserv", name="apmon_mapa_cerca_servicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MAPA_CERCANIA_SERVICIO")
     */
    public function cercaservicioAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Cercania de Servicio");

        //tengo que incorporar todas las referencias 
        $referencias = $this->referenciaManager->findAsociadas();
        if ($referencias) {
            return $this->render('apmon/Cercania/cercaservicio.html.twig', array(
                'referencias' => $referencias,
                'cantidad_servicios' => range(1, count((array)$this->servicioManager->findEnabledByUsuario())),
                'tipo_servicio' => $this->tiposervicioManager->findAll(),
                'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_CERCANIA'),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'No tiene referencias asociadas por lo que no puede usar este servicio');
            $this->breadcrumbManager->pop();
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * @Route("/apmon/mapa/cercaserv/consulta", name="apmon_mapa_cerca_servicio_consultar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MAPA_CERCANIA_SERVICIO")
     */
    public function cercaservicioconsultaAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Cercania de Servicio");

        $mapa = $this->gmapsManager->createMap();
        $referencias = $this->referenciaManager->findAsociadas();

        $referencia = $this->referenciaManager->find($request->get('selectReferencia'));
        $centroReferencia = array('latitud' => $referencia->getLatitud(), 'longitud' => $referencia->getLongitud());
        //meto la referencia en el mapa.
        $this->gmapsManager->addMarkerReferencia($mapa, $referencia, true, false, false);

        if (!is_null($referencia)) {

            /**
             * ---------DETERMINACION DE CECANIA DE UN SERVICIO---------
             */
            //busco todos los servicios habilitados para el usuario.
            $servicios = $this->servicioManager->findEnabledByUsuario();
            if ($request->get('selectTipoServicio') != 0) {
                //debo filtrar por tipo de servicio
                $tmp = array();
                foreach ($servicios as $servicio) {
                    if ($servicio->getTipoServicio()->getId() == $request->get('selectTipoServicio')) {
                        $tmp[] = $servicio;
                    }
                }
                $servicios = $tmp;
            }
            $cantServ = $request->get('selectCantidadServicios') != '0' ? $request->get('selectCantidadServicios') : null;

            //obtengo la cercania del servicio con la referencia en cuestion
            $posiciones = $this->referenciaManager->buscarCercania($referencia, $servicios, $cantServ);

            //hago calculos extras y obtengo info adicional
            foreach ($posiciones as $i => $posicion) {
                //obtengo el servicio en cuestion                
                $servicio = $this->servicioManager->find($posicion['servicio_id']);
                $status[$servicio->getId()] = array(
                    'icono' => $this->gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
                );
                //meto el servicio en el mapa.
                $this->gmapsManager->addMarkerServicio($mapa, $servicio, true);

                //meto el servicio en el array de datos.
                $posiciones[$i]['servicio'] = $servicio;
                if ($posicion['adentro']) {
                    $posiciones[$i]['str_cercania'] = sprintf('%s esta en la referencia', $posicion['nombre']);
                } else {
                    $posiciones[$i]['str_cercania'] = sprintf('%s se encuentra a %s al %s', $posicion['nombre'], $posicion['strdistancia'], $posicion['bearing']['direccion']);
                }

                //establezco el status.
                $posiciones[$i]['st_ultreporte'] = $this->servicioManager->getStatusUltReporte($servicio);

                //aca empieza el tema de la direccion.
                if ($request->get('checkVerDireccion')) {
                    $posiciones[$i]['direccion'] = $this->geocoderManager->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());
                }

                //busco la ref mas cercana al servicio
                $ref_cercana = $this->servicioManager->buscarCercania($servicio, $referencias, 1);
                if ($ref_cercana) {
                    $posiciones[$i]['cercana_al_servicio'] = $ref_cercana[0];
                }
                //meto la polinilea en el mapa.
                $this->gmapsManager->addCamino($mapa, 'distancia_' . $servicio->getId(), array(
                    $centroReferencia,
                    array('latitud' => $posiciones[$i]['latitud'], 'longitud' => $posiciones[$i]['longitud'])
                ), $i);
            }
            if (count($posiciones) > 0) {
                $this->gmapsManager->fitBounds($mapa, array($centroReferencia, array('latitud' => $posiciones[0]['latitud'], 'longitud' => $posiciones[0]['longitud'])));

                //metos las referencias al mapa.
                $referencias = $this->referenciaManager->findAsociadas();
                foreach ($referencias as $ref) {
                    $this->gmapsManager->addMarkerReferencia($mapa, $ref, false, false);
                }

                //tengo que renderizar el mapa para mostrar.
                return $this->render('apmon/Cercania/cercaserviciomapa.html.twig', array(
                    'mapa' => $mapa,
                    'servicios' => $servicios,
                    'status' => $status,
                    'formcr_referencia' => $this->referenciaFormManager->createReferenciaForm($servicio->getOrganizacion())->createView(),
                    'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($servicio->getOrganizacion())->createView(),
                    'referencias' => $referencias,
                    'cercanos' => $posiciones,
                    'consulta' => array(
                        'referencia' => $referencia->getNombre(),
                        'cantidadservicios' => $request->get('selectCantidadServicios') != 0 ? $request->get('selectCantidadServicios') : 'Todos',
                        'tipo_servicio' => $request->get('selectTipoServicio') != 0 ? $this->tiposervicioManager->findById($request->get('selectTipoServicio')) : 'Cualquier tipo',
                        'verDireccion' => $request->get('checkVerDireccion'),
                    ),
                ));
            } else {
                $error = 'No hay servicios de esa clase cercanos a la referencia';
            }
        } else {
            $error = 'Referencia no encontrada';
        }
        //no se cumple alguna condicion.
        return $this->render('apmon/Cercania/cercaservicio.html.twig', array(
            'referencias' => $referencias,
            'cantidad_servicios' => range(1, count((array)$this->servicioManager->findEnabledByUsuario())),
            'tipo_servicio' => $this->tiposervicioManager->findAll(),
            'error' => $error
        ));
    }
}
