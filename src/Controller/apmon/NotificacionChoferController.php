<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\UtilsManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ExcelManager;
use App\Model\app\NotificacionChoferManager;
use App\Model\app\ContactoManager;
use App\Model\app\ChoferManager;
use App\Entity\NotificacionChofer;
use App\Form\apmon\NotificacionChoferType;
use App\Model\app\Router\NotificacionChoferRouter;

/**
 * Description of NotificacionChofer
 *
 * @author nicolas
 */
class NotificacionChoferController extends AbstractController
{

    protected $userloginManager;
    protected $breadcrumbManager;
    protected $organizacionManager;
    protected $choferManager;
    protected $utilsManager;
    protected $ibuttonManager;
    protected $histIbuttonMAnager;
    protected $licenciaManager;
    protected $excelManager;
    protected $notifRouter;
    protected $notifchoferManager;
    protected $contactoManager;

    function __construct(
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        ExcelManager $excelManager,
        NotificacionChoferManager $notifchoferManager,
        UtilsManager $utilsManager,
        NotificacionChoferRouter $notifRouter,
        ContactoManager $contactoManager,
        ChoferManager $choferManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->userloginManager = $userloginManager;
        $this->utilsManager = $utilsManager;
        $this->excelManager = $excelManager;
        $this->notifchoferManager = $notifchoferManager;
        $this->notifRouter = $notifRouter;
        $this->contactoManager = $contactoManager;
        $this->choferManager = $choferManager;
    }

    /**
     * @Route("/apmon/chofer/notificacion/{idorg}/list", name="apmon_chofer_notificacion_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function listAction(Request $request, $idorg)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de grupo de Contactos");
        
        $organizacion = $this->organizacionManager->find($idorg);
        $grupos = $this->notifchoferManager->findAll($organizacion);

        $menu[1] = array($this->notifRouter->btnNew());
        // die('////<pre>' . nl2br(var_export($menu, true)) . '</pre>////');

        return $this->render('apmon/NotifChofer/list.html.twig', array(
            'organizacion' => $organizacion,
            'grupos' => $grupos,
            'menu' => $menu
        ));
    }

    /**
     * @Route("/apmon/chofer/notificacion/{id}/show", name="apmon_chofer_notificacion_show")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function showAction(Request $request, NotificacionChofer $notificacion)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Grupo de contacto");
        $choferes = $this->getChoferes($notificacion);
        //  die('////<pre>' . nl2br(var_export(count($choferes), true)) . '</pre>////');
        $menu = $this->getShowMenu($notificacion);
        $deleteForm = $this->createDeleteForm($notificacion->getId());
        $contactos = $this->getContactos($notificacion);

        return $this->render('apmon/NotifChofer/show.html.twig', array(
            'grupo' => $notificacion,
            'contactos' => $contactos,
            'choferes' => $choferes,
            'delete_form' => $deleteForm->createView(),
            'menu' => $menu
        ));
    }

    private function getChoferes($notificacion)
    {
        $choferes_ = $this->choferManager->findAll($notificacion->getOrganizacion());
        foreach ($choferes_ as $key => $chofer) {
            foreach ($notificacion->getNotifChof() as $notifChof) {
                if ($chofer->getId() == $notifChof->getChofer()->getId()) {
                    unset($choferes_[$key]);
                }
            }
        }
        $choferes = array_values($choferes_);
        return $choferes;
    }

    private function getContactos($notificacion)
    {
        $contactos_ = $this->contactoManager->findAll($notificacion->getOrganizacion());
        foreach ($contactos_ as $key => $contacto) {
            foreach ($notificacion->getNotifContact() as $notifContact) {
                if ($contacto->getId() == $notifContact->getContacto()->getId()) {
                    unset($contactos_[$key]);
                }
            }
        }
        $contactos = array_values($contactos_);
        return $contactos;
    }

    private function getShowMenu($notificacion)
    {
        $menu[1] = array($this->notifRouter->btnEdit($notificacion->getId()));
        $menu[2] = array($this->notifRouter->btnDelete());
        $menu[3] = array($this->notifRouter->btnAddChofer());
        $menu[4] = array($this->notifRouter->btnAddContacto());

        return $menu;
    }

    /**
     * @Route("/apmon/notificacionchofer/{id}/edit",name="apmon_notificacionchofer_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NotificacionChofer $notificacion)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Modificar Grupo");
        if (!$notificacion) {
            throw $this->createNotFoundException('Código de Grupo de Notificaciones no encontrado.');
        }
        $form = $this->createForm(NotificacionChoferType::class, $notificacion);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->notifchoferManager->save($notificacion);

            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($notificacion->getNombre())));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        return $this->render('apmon/NotifChofer/edit.html.twig', array(
            'grupo' => $notificacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/notificacionchofer/{id}/delete", name="apmon_notificacionchofer_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, NotificacionChofer $notificacion)
    {

        if (!$notificacion) {
            throw $this->createNotFoundException('Código de grupo  no encontrado.');
        }

        $form = $this->createDeleteForm($notificacion->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->notifchoferManager->delete($notificacion);
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    /**
     * @Route("/apmon/notificacionchofer/newajax", options={"expose"=true}, name="apmon_notificacionchofer_newajax")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tmp = array();
        $organizacion = $this->userloginManager->getOrganizacion();
        parse_str($request->get('form'), $tmp);
        $data = $tmp['grupo'];
        // die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $grupo = $this->notifchoferManager->create($organizacion);
        $grupo->setNombre($data['nombre']);
        $grupo = $this->notifchoferManager->save($grupo);
        $grupos = $this->notifchoferManager->findAll($organizacion);
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $grupo->getId(),
            'html' => $this->renderView('apmon/NotifChofer/grupos.html.twig', array(
                'grupos' => $grupos
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/editajax", options={"expose"=true}, name="apmon_notificacionchofer_editajax")
     * @Method({"GET", "POST"})
     */
    public function editajaxAction(Request $request)
    {
        $tmp = array();
        $organizacion = $this->userloginManager->getOrganizacion();
        parse_str($request->get('form'), $tmp);
        $data = $tmp['edit'];
        // die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $grupo = $this->notifchoferManager->find(intval($request->get('id')));
        $grupo->setNombre($data['grupoNombre']);
        $grupo = $this->notifchoferManager->save($grupo);
        $grupos = $this->notifchoferManager->findAll($organizacion);
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $grupo->getId(),
            'html' => $this->renderView('apmon/NotifChofer/grupos.html.twig', array(
                'grupos' => $grupos
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/deleteajax", options={"expose"=true}, name="apmon_notificacionchofer_deleteajax")
     * @Method({"GET", "POST"})
     */
    public function deleteajaxAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $grupo = $this->notifchoferManager->find(intval($request->get('id')));
        $grupo = $this->notifchoferManager->delete($grupo);
        $grupos = $this->notifchoferManager->findAll($organizacion);
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/NotifChofer/grupos.html.twig', array(
                'grupos' => $grupos
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/addchofer", options={"expose"=true}, name="apmon_notificacionchofer_addchofer")
     * @Method({"GET", "POST"})
     */
    public function addchoferAction(Request $request)
    {
        $tmp = array();
        $choferes_ = array();
        $organizacion = $this->userloginManager->getOrganizacion();
        parse_str($request->get('form'), $tmp);
        $data = $tmp['chofer'];
        $grupo = $this->notifchoferManager->find(intval($request->get('id')));
        foreach ($data as $key => $value) {
            $choferes_[] = $this->choferManager->find(intval($key));
        }
        $notifChof = $this->notifchoferManager->addChofer($grupo, $choferes_);
        $choferes = $this->getChoferes($grupo);

        //die('////<pre>' . nl2br(var_export(count($choferes), true)) . '</pre>////');

        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/NotifChofer/choferes.html.twig', array(
                'choferes' => $grupo->getNotifChof()
            )),
            'form' => $this->renderView('apmon/NotifChofer/form_chofer.html.twig', array(
                'choferes' => $choferes
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/addcontacto", options={"expose"=true}, name="apmon_notificacionchofer_addcontacto")
     * @Method({"GET", "POST"})
     */
    public function addcontactoAction(Request $request)
    {
        $tmp = array();
        $contactos_ = array();
        $organizacion = $this->userloginManager->getOrganizacion();
        parse_str($request->get('form'), $tmp);
        $data = $tmp['contacto'];
        $grupo = $this->notifchoferManager->find(intval($request->get('id')));
        foreach ($data as $key => $value) {
            $contactos_[] = $this->contactoManager->find(intval($key));
        }
        $notifContacto = $this->notifchoferManager->addContacto($grupo, $contactos_);
        $contactos = $this->getContactos($grupo);

        //die('////<pre>' . nl2br(var_export(count($choferes), true)) . '</pre>////');

        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/NotifChofer/contactos.html.twig', array(
                'contactos' => $grupo->getNotifContact()
            )),
            'form' => $this->renderView('apmon/NotifChofer/form_contacto.html.twig', array(
                'contactos' => $contactos
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/deletechofer", options={"expose"=true}, name="apmon_notificacionchofer_deletechofer")
     * @Method({"GET", "POST"})
     */
    public function deletechoferAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $notifChof = $this->notifchoferManager->findNotifChof(intval($request->get('id')));
        //   die('////<pre>' . nl2br(var_export($notifChof->getId(), true)) . '</pre>////');
        $grupo = $notifChof->getNotificacion();
        $delete = $this->notifchoferManager->deleteNotifChof($notifChof);

        $choferes = $this->getChoferes($grupo);



        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/NotifChofer/choferes.html.twig', array(
                'choferes' => $grupo->getNotifChof()
            )),
            'form' => $this->renderView('apmon/NotifChofer/form_chofer.html.twig', array(
                'choferes' => $choferes
            )),
        )));
    }

    /**
     * @Route("/apmon/notificacionchofer/deletecontacto", options={"expose"=true}, name="apmon_notificacionchofer_deletecontacto")
     * @Method({"GET", "POST"})
     */
    public function deletecontactoAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $notifContact = $this->notifchoferManager->findNotifContact(intval($request->get('id')));
        //   die('////<pre>' . nl2br(var_export($notifChof->getId(), true)) . '</pre>////');
        $grupo = $notifContact->getNotificacion();
        $delete = $this->notifchoferManager->deleteNotifContact($notifContact);

        $contactos = $this->getContactos($grupo);



        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/NotifChofer/contactos.html.twig', array(
                'contactos' => $grupo->getNotifContact()
            )),
            'form' => $this->renderView('apmon/NotifChofer/form_contacto.html.twig', array(
                'contactos' => $contactos
            )),
        )));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
