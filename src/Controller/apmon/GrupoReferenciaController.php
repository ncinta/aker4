<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Form\apmon\GrupoReferenciaType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\NotificadorAgenteManager;
use App\Entity\GrupoReferencia;

/**
 * Description of GrupoReferenciaController
 *
 * @author yesica
 */
class GrupoReferenciaController extends AbstractController {

    private $userloginManager;
    private $grupoReferenciaManager;
    private $breadcrumbManager;
    private $referenciaManager;
    private $notificadorAgenteManager;

    function __construct(UserLoginManager $userloginManager, GrupoReferenciaManager $grupoReferenciaManager,
            BreadcrumbManager $breadcrumbManager, ReferenciaManager $referenciaManager, NotificadorAgenteManager $notificadorAgenteManager) {
        $this->userloginManager = $userloginManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->referenciaManager = $referenciaManager;
        $this->notificadorAgenteManager = $notificadorAgenteManager;
    }

    private function getEntityManager() {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository() {
        return $this->getEntityManager()->getRepository('App\Entity\GrupoReferencia');
    }

    private function getSecurityContext() {
        return $this->get('security.token_storage');
    }

    /**
     * @Route("/apmon/gruporeferencia/list", name="apmon_grupo_referencia_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_VER")
     */
    public function listAction(Request $request) {
        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de Grupos");

        $organizacion = $this->userloginManager->getOrganizacion();
        $gruposref = $this->grupoReferenciaManager->findAsociadas($organizacion);
        return $this->render('apmon/GrupoReferencia/list.html.twig', array(
                    'menu' => $this->getListMenu($organizacion),
                    'gruposref' => $gruposref,
                    'organizacion' => $organizacion,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion) {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_AGREGAR')) {
            $menu['Agregar Grupo'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_grupo_referencia_new', array(
                    'idorg' => $organizacion->getId())));
        }
        return $menu;
    }

    /**
     * @Route("/apmon/gruporeferencia/{id}/show", name="apmon_grupo_referencia_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_VER") 
     */
    public function showAction(Request $request, GrupoReferencia $grupoRef) {
        if (!$grupoRef) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), $grupoRef->getNombre());

        $deleteForm = $this->createDeleteForm($grupoRef->getId());

        return $this->render('apmon/GrupoReferencia/show.html.twig', array(
                    'menu' => $this->getShowMenu($grupoRef),
                    'gruporef' => $grupoRef,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $grupoRef 
     * @return array $menu
     */
    private function getShowMenu($grupoRef) {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_grupo_referencia_edit', array(
                    'id' => $grupoRef->getId())));
        }
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_GRUPO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/gruporeferencia/{idorg}/new", name="apmon_grupo_referencia_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGREGAR") 
     */
    public function newAction(Request $request, $idorg = null) {

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Grupo");
        $organizacion = $this->userloginManager->getOrganizacion();
        $grupoRef = $this->grupoReferenciaManager->create($organizacion);
        $form = $this->createForm(GrupoReferenciaType::class, $grupoRef);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->grupoReferenciaManager->save();
            $userlogin = $this->userloginManager->getUser();

            //NOTE 09/08/2023: Se elimina esta asignacion porque segar cada vez que crea un grupo se le asigna automaticamente al usuario logueado.
            //$asignarUsr = $this->grupoReferenciaManager->asignarUsuario($grupoRef->getId(), $userlogin);
            
            $notificar = $this->notificadorAgenteManager->notificar($grupoRef, 1);
            return $this->redirect($this->generateUrl('apmon_grupo_referencia_show', array(
                                'id' => $grupoRef->getId())));
        }

        return $this->render('apmon/GrupoReferencia/new.html.twig', array(
                    'gruporef' => $grupoRef,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/gruporeferencia/{id}/edit", name="apmon_grupo_referencia_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_EDITAR") 
     */
    public function editAction(Request $request, GrupoReferencia $grupoRef) {

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Grupo");
        if (!$grupoRef) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }
        $form = $this->createForm(GrupoReferenciaType::class, $grupoRef);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->grupoReferenciaManager->save($grupoRef);
            $notificar = $this->notificadorAgenteManager->notificar($grupoRef, 1);

            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($grupoRef->getNombre())));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        return $this->render('apmon/GrupoReferencia/edit.html.twig', array(
                    'gruporef' => $grupoRef,
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/gruporeferencia/{id}/delete", name="apmon_grupo_referencia_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_ELIMINAR") 
     */
    public function deleteAction(Request $request, GrupoReferencia $grupoRef) {
        if (!$grupoRef) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $form = $this->createDeleteForm($grupoRef->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $grupo = new GrupoReferencia();
            $grupo->setId($grupoRef->getId());
            $grupo->setUpdatedAt($grupoRef->getUpdatedAt());
            $grupo->setCreatedAt($grupoRef->getCreatedAt());
            $this->grupoReferenciaManager->delete($grupoRef);
            $notificar = $this->notificadorAgenteManager->notificar($grupo, 2);
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
                        ->getForm()
        ;
    }

    protected function setFlash($action, $value) {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/apmon/gruporeferencia/{id}/agrupar", name="apmon_grupo_referencia_agrupar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGRUPAR") 
     */
    public function agruparAction(Request $request, GrupoReferencia $grupo) {

        $organizacion = $this->userloginManager->getOrganizacion();
        if (!$grupo || !$organizacion) {
            throw $this->createNotFoundException('Código de Grupo de Referencias no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), "Agrupar Referencias");

        $disponibles = $this->referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion);
        $asignados = $this->referenciaManager->findAllByGrupo($grupo);

        return $this->render('apmon/GrupoReferencia/agrupar.html.twig', array(
                    'grupo' => $grupo,
                    'disponibles' => $disponibles,
                    'asignados' => $asignados,
                    'organizacion' => $organizacion,
        ));
    }

    /**
     * @Route("/apmon/gruporeferencia/asignar", name="apmon_grupo_referencias_asignarreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_AGRUPAR") 
     */
    public function asignarReferenciaAction(Request $request) {
        $grupo = $this->grupoReferenciaManager->find($request->get('idgrp'));
        $organizacion = $this->userloginManager->getOrganizacion();

        $idsReferencias = unserialize($request->get('referencias'));
        if ($grupo && count($idsReferencias) > 0) {
            foreach ($idsReferencias as $key => $value) {
                $this->grupoReferenciaManager->addReferencia($value);
            }
        } $notificar = $this->notificadorAgenteManager->notificar($grupo, 1);

        $respuesta = array(
            'disponibles' => $this->renderView('apmon/GrupoReferencia/show_disponibles.html.twig', array(
                'disponibles' => $this->referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion)
            )),
            'asignados' => $this->renderView('apmon/GrupoReferencia/show_asignados.html.twig', array(
                'asignados' => $this->referenciaManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/apmon/gruporeferencia/retirar", name="apmon_grupo_referencias_retirarreferencia")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_GRUPO_DESAGRUPAR") 
     */
    public function retirarReferenciaAction(Request $request) {

        $grupo = $this->grupoReferenciaManager->find($request->get('idgrp'));
        $organizacion = $this->userloginManager->getOrganizacion();

        $idsReferencias = unserialize($request->get('referencias'));
        if ($grupo && count($idsReferencias) > 0) {
            foreach ($idsReferencias as $key => $value) {
                $this->grupoReferenciaManager->removeReferencia($value);
            }
        }
        $notificar = $this->notificadorAgenteManager->notificar($grupo, 1);
        //   die('////<pre>'.nl2br(var_export($idsReferencias, true)).'</pre>////');
        $respuesta = array(
            'disponibles' => $this->renderView('apmon/GrupoReferencia/show_disponibles.html.twig', array(
                'disponibles' => $this->referenciaManager->findAllSinAsignar2Grupo($grupo, $organizacion)
            )),
            'asignados' => $this->renderView('apmon/GrupoReferencia/show_asignados.html.twig', array(
                'asignados' => $this->referenciaManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }

}
