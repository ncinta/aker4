<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\apmon\HistorialType;
use GMaps\Geocoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\ServicioManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ChoferManager;
use App\Model\app\UtilsManager;
use App\Model\app\ExcelManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\GmapsManager;
use App\Model\app\ReferenciaFormManager;
use App\Model\app\BackendManager;
use App\Model\app\GeocoderManager;

class HistorialController extends AbstractController
{

    private $breadcrumbManager;
    private $gmapsManager;
    private $servicioManager;
    private $referenciaManager;
    private $userloginManager;
    private $referenciaFormManager;
    private $utilsManager;
    private $tanqueManager;
    private $choferManager;
    private $geocoderManager;
    private $backendManager;
    private $excelManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ExcelManager $excelManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        ReferenciaManager $referenciaManager,
        UserLoginManager $userloginManager,
        ReferenciaFormManager $referenciaFormManager,
        UtilsManager $utilsManager,
        TanqueManager $tanqueManager,
        ChoferManager $choferManager,
        GeocoderManager $geocoderManager,
        BackendManager $backendManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->gmapsManager = $gmapsManager;
        $this->servicioManager = $servicioManager;
        $this->referenciaManager = $referenciaManager;
        $this->userloginManager = $userloginManager;
        $this->referenciaFormManager = $referenciaFormManager;
        $this->utilsManager = $utilsManager;
        $this->tanqueManager = $tanqueManager;
        $this->choferManager = $choferManager;
        $this->geocoderManager = $geocoderManager;
        $this->backendManager = $backendManager;
        $this->excelManager = $excelManager;
    }

    /**
     *
     * @Route("/apmon/historial", name="apmon_historial",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function historialAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Historial de posiciones");
        $mapa = $this->gmapsManager->createMap(true);

        $id = $request->get('id');  //rescato el id si viene desde otro mapa.
        if (is_null($id)) {
            //entro aca porque tengo que buscar todos los servicios.
            $options['servicios'] = $this->servicioManager->findEnabledByUsuario();
        } else {
            //entro aca porque vengo de otro mapa y tengo que agregar el servicio en el mapa.
            $servicio = $this->servicioManager->find($id);
            $options['servicios'] = array($servicio);
            $this->gmapsManager->addMarkerServicio($mapa, $servicio, false);
        }

        $form = $this->createForm(HistorialType::class, null, $options);

        $this->gmapsManager->addPolyline($mapa, 'recorrido', null);

        //puntoA
        $puntoinicio = $this->gmapsManager->createPosicionIcon($mapa, 'puntoA', 'iconA.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'A', null, $puntoinicio);

        $puntofinal = $this->gmapsManager->createPosicionIcon($mapa, 'puntoB', 'iconB.png');
        $this->gmapsManager->addMarkerPosicion($mapa, 'B', null, $puntofinal);

        //agrego las referencias al mapa
        if (isset($servicio) && $servicio->getOrganizacion()->getTipoOrganizacion() == 2) {  //es un distiboudor
            $organizacion = $servicio->getOrganizacion();
            $mapa->setIniLatitud($organizacion->getLatitud());
            $mapa->setIniLongitud($organizacion->getLongitud());
            $referencias = $this->referenciaManager->findAsociadas($organizacion);
        } else {
            $organizacion = $this->userloginManager->getOrganizacion();
            $referencias = $this->referenciaManager->findAsociadas();
        }
        //die('////<pre>' . nl2br(var_export('aca estoy', true)) . '</pre>////');
        return $this->render('apmon/Historial/historial.html.twig', array(
            'organizacion' => $organizacion,
            'form' => $form->createView(),
            'historial' => null,
            'periodo' => null,
            'default_referencias' => '0',
            'mapa' => $mapa,
            'formcr_referencia' => $this->referenciaFormManager->createReferenciaForm($organizacion)->createView(),
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($organizacion)->createView(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_HISTORIAL'),
            'limite_historial' => $this->utilsManager->getLimiteHistorial(),
        ));
    }

    /**
     *
     * @Route("/apmon/historial/generar", name="apmon_historial_generar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */

    public function generarAction(Request $request)
    {
        //obtengo y parse el request.
        $form_consulta = array();
        parse_str($request->get('historial'), $form_consulta);
        $consulta = $form_consulta['historial'];

        $servicio = $this->servicioManager->find($consulta['servicio']);
        $consulta['nombre'] = $servicio->getNombre();
        $consulta['canbus'] = $servicio->getCanbus();
        //die('////<pre>'.nl2br(var_export($consulta, true)).'</pre>////');

        unset($form_consulta); //no uso mas el $form_consulta.
        if ($consulta) {
            if (!$this->utilsManager->isValidDesdeHasta('d/m/Y H:i', $consulta['desde'], $consulta['hasta'])) {
                $error = 'El rango o formato de fechas es invalido.';
            } else {
                //obtengo el periodo a consultar.
                $consulta['periodo'] = $this->utilsManager->periodo2array(
                    $this->utilsManager->datetime2sqltimestamp($consulta['desde'], false),
                    $this->utilsManager->datetime2sqltimestamp($consulta['hasta'], true)
                );

                foreach ($consulta['periodo'] as $key => $value) {
                    $consulta['periodo'][$key]['title'] = substr($value['desde'], 8, 2) . '/' . substr($value['desde'], 5, 2) . '/' . substr($value['desde'], 0, 4);
                }

                //aca grabo los datos de la session
                $this->get('session')->set('consulta', $consulta);

                $historial = $this->getHistorial(0, $consulta, $servicio);
            }
        } else {
            $historial = array();
        }
        if (isset($error)) {
            return new Response(json_encode($this->armarResponse(-1, $consulta, $error, true)));
        } else {
            return new Response(json_encode($this->armarResponse(0, $consulta, $historial, true)));
        }
    }

    /**
     * Realiza el renderizado de los datos de historial.
     * @param type $page
     * @param type $consulta
     * @param array $historial
     * @param type $inicial
     * @return type response ajax
     */
    private function armarResponse($page, $consulta, $historial, $inicial)
    {
        if ($page == -1) {   //si es -1 es porque se pudrio algo....
            return array(
                'recorrido' => array('id_sc' => 'recorrido', 'visible' => false),
                'puntoA' => array('id_sc' => 'punto_A', 'visible' => false),
                'puntoB' => array('id_sc' => 'punto_B', 'visible' => false),
                'html' => $this->renderView('apmon/Historial/historial_error.html.twig', array(
                    'page' => $page,
                    'consulta' => $consulta,
                    'historial' => $historial,
                ))
            );
        }
        if ($inicial) {
            $template = 'apmon/Historial/historial_data.html.twig';
        } else {
            $template = 'apmon/Historial/historial_grilla.html.twig';
            //            die('////<pre>'.nl2br(var_export($template, true)).'</pre>////');
        }
        $html = $this->renderView($template, array(
            'page' => $page,
            'consulta' => $consulta,
            'periodo' => $consulta['periodo'],
            'historial' => is_null($historial) || count($historial) == 0 ? null : $historial['historial'],
        ));


           // dd($historial);
        if (is_null($historial) || count($historial) == 0) {    //es nulo el historial....
            return array(
                'recorrido' => array('id_sc' => 'recorrido', 'visible' => false),
                'puntoA' => array('id_sc' => 'punto_A', 'visible' => false),
                'puntoB' => array('id_sc' => 'punto_B', 'visible' => false),
                'html' => $html
            );
        } else {
            //   die('////<pre>'.nl2br(var_export(count($historial), true)).'</pre>////');
            if (!is_null($historial['puntoA'])) {
                $ptoA = array('id_sc' => 'punto_A', 'latitud' => $historial['puntoA']['latitud'], 'longitud' => $historial['puntoA']['longitud'], 'visible' => true);
            } else {
                $ptoA = array('id_sc' => 'punto_A', 'visible' => false);
            }
            if (!is_null($historial['puntoB'])) {
                $ptoB = array('id_sc' => 'punto_B', 'latitud' => $historial['puntoB']['latitud'], 'longitud' => $historial['puntoB']['longitud'], 'visible' => true);
            } else {
                $ptoB = array('id_sc' => 'punto_B', 'visible' => false);
            }
            return array(
                'recorrido' => array('id_sc' => 'recorrido', 'puntos' => $historial['puntos_polilinea']),
                'puntoA' => $ptoA,
                'puntoB' => $ptoB,
                'html' => $html
            );
        }
    }

    /**
     *
     * @Route("/apmon/historial/generar/date", name="apmon_historial_generar_date",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function generardateAction(Request $request)
    {
        $page = $request->get('page');
        //obtengo y parse el request.
        $consulta = $this->get('session')->get('consulta');
        if ((!is_null($consulta)) && $page >= 0) {
            $historial = $this->getHistorial($page, $consulta, $this->servicioManager->find($consulta['servicio']));
        } else {
            $historial = array();
        }
        return new Response(json_encode($this->armarResponse($page, $consulta, $historial, false)));
    }

    /**
     * Obtiene el historial de posicione.
     * @param type $id  es la posicion que se quiere consultar dentro de $consulta['periodo']
     * @param type $consulta es la consulta que se pasa con todos los parametros que se quieren mostrar.
     * @param array $opciones  (sin uso)
     * @return type $historial
     */
    private function getHistorial($id, $consulta, $servicio)
    {
        $opciones = array();
        try {
            $consulta_historial = $this->backendManager->historial(
                intval($consulta['servicio']),
                $consulta['periodo'][$id]['desde'],
                $consulta['periodo'][$id]['hasta'],
                $this->referenciaManager->findAsociadas(),
                $opciones
            );
        } catch (\Exception $exc) {
            return null;
        }

        if ($servicio->getMedidorCombustible()) {
            $isMedicionesLoad = $this->tanqueManager->loadMediciones($servicio, null);
        }


        $historial = array();
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $puntos_polilinea = array();
        $posicion_id = 0;
        $puntoA = null;
        if ($consulta_historial) {
            reset($consulta_historial);
        }
        while ($trama = current($consulta_historial)) {
            next($consulta_historial);

            if (isset($trama['_id'])) {
                $trama['oid'] = $trama['_id']['$id'];
            }


            //die('////<pre>'.nl2br(var_export($trama, true)).'</pre>////');
            //se inicia el procesamiento de la trama obtenida
            $trama['distancia'] = isset($trama['distancia']) ? $trama['distancia'] : '0';
            $trama['tiempo'] = isset($trama['tiempo']) ? 'T ' . $this->utilsManager->segundos2tiempo($trama['tiempo']) : '0';
            if (isset($trama['posicion'])) {
                $puntos_polilinea[] = array(
                    $trama['posicion']['latitud'],
                    $trama['posicion']['longitud'],
                    $trama['posicion']['direccion'],
                    $trama['posicion']['velocidad'],
                    $trama['oid']
                );
            }

            //establesco la fecha de recepcion
            $datetime1 = new \DateTime($trama['fecha']);
            $datetime2 = new \DateTime($trama['fecha_recepcion']);
            $intervalo = $datetime1->diff($datetime2);
            $trama['recepcion'] = $datetime2->format('d-m-Y H:i:s') . ' (' . $intervalo->format('%R%H:%I:%S') . ')';

            //obtengo las direcciones
            if (isset($trama['posicion']) && isset($consulta['mostrar_direcciones'])) {
                $direc = $this->geocoderManager->inverso($trama['posicion']['latitud'], $trama['posicion']['longitud']);
                $trama['direccion'] = $direc;
            }
            //aca armo el nombre y el icono de la ref si existe.
            if (isset($trama['referencia_id']) && $trama['referencia_id'] != 0) {
                $ref = $this->referenciaManager->find($trama['referencia_id']);
                if ($ref) {
                    $trama['referencia'] = array(
                        'nombre' => $ref->getNombre(),
                        'pathIcono' => $ref->getPathIcono(),
                    );
                }
            }

            //chofer
            if (isset($trama['ibutton']) && $trama['ibutton'] != null && $trama['ibutton'] != 0) {
                //ibutton de chofer
                $chofer = $this->choferManager->findByDallas($trama['ibutton']);
                if ($chofer) {
                    $trama['chofer']['nombre'] = $chofer->getNombre();
                }
            }

            //$trama['carcontrol']['porcentaje_tanque'] = rand(0, 100);
            //empieza la historia con los tanques de combustible.
            if ($servicio->getMedidorCombustible()) {
                $tanque = array('porcentaje' => -1, 'title' => '');
                if (isset($trama['carcontrol']) && !is_null($trama['carcontrol'])) {
                    $carcontrol = $trama['carcontrol'];
                    if (isset($carcontrol['porcentaje_tanque']) && $carcontrol['porcentaje_tanque'] != 0.0 && isset($trama['contacto']) && $trama['contacto']) {
                        $tanque = array(
                            'porcentaje' => $carcontrol['porcentaje_tanque'],
                            'title' => sprintf('%s %%', $carcontrol['porcentaje_tanque'])
                        );

                        if ($isMedicionesLoad) {
                            $medicion = $this->tanqueManager->getLitros($carcontrol['porcentaje_tanque']);
                            if ($medicion['modo'] == -1) {
                                $tanque['litros'] = '<' . $medicion['litros'];
                            } elseif ($medicion['modo'] == 1) {
                                $tanque['litros'] = '>' . $medicion['litros'];
                            } else {
                                $tanque['litros'] = $medicion['litros'];
                            }
                        }
                    }
                    $historialtanque[] = $tanque;

                    $trama['tanque'] = $tanque;
                }
            }

            //    die('////<pre>' . nl2br(var_export( $trama['tanque'], true)) . '</pre>////');
            //se procesa el estado de los inputs.
            $inputs = array();
            if ($servicio->getEntradasDigitales() === true) {
                foreach ($servicio->getInputs() as $input) {
                    // $trama['entrada_2'] = rand(1, 5) == 1;   //borrar esta linea, solo test.
                    $valor = isset($trama[$input->getModeloSensor()->getCampoTrama()]) ? $trama[$input->getModeloSensor()->getCampoTrama()] : false;
                    //aca armo la leyenda teneindo en cuenta si hay icono o no.
                    $inputs[] = array(
                        'nombre' => $input->getNombre(),
                        'icono' => !is_null($input->getIcono()) ? ($valor ? $input->getIcono() : str_replace('input_on', 'input_off', $input->getIcono())) : null,
                        'leyenda' => $input->getAbbr() . ': ' . ($valor ? 'ON' : 'OFF')
                    );
                }
            }
            $trama['inputs'] = $inputs;


            //============= cerrojo ==============                
            if ($servicio->getCorteCerrojo() === true) {
                $trama['cerrojo'] = array(
                    'estado_cerrojo' => $this->servicioManager->getStatusCerrojo($servicio, $trama),
                    //                    'estado_gabinete' => $this->servicioManager->getStatusGabinete($servicio, $trama),
                );
            }

            //para ver canbus
            if (isset($trama['canbusInicioTrayecto'])) {
                $trama['canbus'] = 0;
            }
            if (isset($trama['canbusData'])) {
                $trama['canbus'] = 1;
            }
            if (isset($trama['canbusFinTrayecto'])) {
                $trama['canbus'] = 2;
            }
            //aca se decide si la trama se agrega a listado de historial.
            if (isset($trama['posicion'])) {
                if ($posicion_id == 0) {
                    $puntoA = array('latitud' => $trama['posicion']['latitud'], 'longitud' => $trama['posicion']['longitud']);
                    $odometro_inicial = isset($trama['odometro']) ? $trama['odometro'] : 0;
                    $tiempo_inicial = $trama['fecha'];
                }
                $ultima = $trama; //es para tener la ultima trama;
                if ($consulta['solo_referencias'] == '1') {
                    if (isset($trama['referencia_id']) && $trama['referencia_id'] != 0) {
                        $historial[] = $trama;
                    }
                } else {
                    $historial[] = $trama;
                }
            } else {  //no tengo posicion
                if (isset($consulta['mostrar_sinposicion'])) {
                    $historial[] = $trama;
                }
            }
            $posicion_id++;
        }
        unset($consulta_historial);

        if (isset($ultima) && !is_null($ultima)) {
            $puntoB = array('latitud' => $ultima['posicion']['latitud'], 'longitud' => $ultima['posicion']['longitud']);
        } else {
            $puntoB = null;
        }
        //        die('////<pre>' . nl2br(var_export($historialtanque, true)) . '</pre>////');
        return array(
            'historial' => $historial,
            'puntos_polilinea' => $puntos_polilinea,
            'puntoA' => $puntoA,
            'puntoB' => $puntoB,
        );
    }

    /**
     *
     * @Route("/apmon/historial/exportar", name="apmon_historial_exportar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function exportarAction(Request $request)
    {
        $consulta = json_decode($request->get('consulta'), true);
        $historial = json_decode($request->get('historial'), true);
        $page = json_decode($request->get('page'), true);

        //die('////<pre>' . nl2br(var_export($historial, true)) . '</pre>////');
        if (isset($consulta) && isset($historial)) {
            //creo el xls
            $xls = $this->excelManager->create("Historial de Posiciones");
            $servicio = $this->servicioManager->find(intval($consulta['servicio']));
            $xls->setHeaderInfo(array(
                'C2' => 'Servicio',
                'D2' => $servicio->getNombre(),
                'C3' => 'Fecha desde',
                'D3' => $consulta['periodo'][$page]['desde'],
                'C4' => 'Fecha hasta',
                'D4' => $consulta['periodo'][$page]['hasta'],
            ));

            $xls->setBar(6, array(
                'A' => array('title' => 'Fecha', 'width' => 20),
                'B' => array('title' => 'Referencia', 'width' => 20),
                'C' => array('title' => 'Dirección', 'width' => 20),
                'D' => array('title' => 'Latitud', 'width' => 15),
                'E' => array('title' => 'Longitud', 'width' => 15),
                'F' => array('title' => 'Velocidad', 'width' => 10),
                'G' => array('title' => 'Dirección', 'width' => 10),
                'H' => array('title' => 'Bat. (%)', 'width' => 10),
                'I' => array('title' => 'Odometro', 'width' => 10),
                'J' => array('title' => 'Distancia (mts)', 'width' => 10),
                'K' => array('title' => 'Tiempo', 'width' => 10),
                'L' => array('title' => 'Contacto', 'width' => 10),
                'M' => array('title' => 'Entrada 1', 'width' => 10),
                'N' => array('title' => 'Entrada 2', 'width' => 10),
                'O' => array('title' => 'Entrada 3', 'width' => 10),
                'P' => array('title' => 'Motivo Tx', 'width' => 10),
                'Q' => array('title' => 'Tanque (%)', 'width' => 10),
                'R' => array('title' => 'Tanque (lts)', 'width' => 10),
            ));

            $i = ord('S');
            foreach ($servicio->getSensores() as $sensor) {
                $xls->setBar(6, array(chr($i) => array('title' => $sensor->getNombre(), 'width' => 20)));
                $i++;
            }
            $i = 7;
            foreach ($historial as $key => $value) {
                $fecha = new \DateTime($value['fecha']);


                $xls->setCellValue('A' . $i, $fecha->format('d/m/Y H:i:s'));
                if (isset($value['referencia'])) {
                    $xls->setCellValue('B' . $i, $value['referencia']['nombre']);
                }
                $xls->setCellValue('C' . $i, isset($value['direccion']) ? $value['direccion'] : '---');
                if (isset($value['posicion'])) {
                    $xls->setCellValue('D' . $i, $value['posicion']['latitud']);
                    $xls->setCellValue('E' . $i, $value['posicion']['longitud']);
                    $xls->setCellValue('F' . $i, $value['posicion']['velocidad']);
                    $xls->setCellValue('G' . $i, $value['posicion']['direccion']);
                }
                $xls->setCellValue('H' . $i, isset($value['bateria_interna']) ? $value['bateria_interna'] : '0');
                if (isset($value['odometro'])) {
                    $xls->setCellValue('I' . $i, (intval($value['odometro'])) / 1000, '0');
                }
                $xls->setCellValue('J' . $i, $value['distancia'], '0');
                $xls->setCellValue('K' . $i, $value['tiempo'], 'H:MM:SS');
                $xls->setCellValue('L' . $i, isset($value['contacto']) && $value['contacto'] == true ? 'SI' : 'NO');
                $xls->setCellValue('M' . $i, isset($value['entrada_1']) && $value['entrada_1'] == true ? 'SI' : 'NO');
                $xls->setCellValue('N' . $i, isset($value['entrada_2']) && $value['entrada_2'] == true ? 'SI' : 'NO');
                $xls->setCellValue('O' . $i, isset($value['entrada_3']) && $value['entrada_3'] == true ? 'SI' : 'NO');
                $xls->setCellValue('P' . $i, isset($value['motivo']) ? $value['motivo'] : '---', '0');
                if (isset($value['tanque'])) {
                    $xls->setCellValue('Q' . $i, isset($value['tanque']) ? $value['tanque']['porcentaje'] : '---', '0');
                    $xls->setCellValue('R' . $i, isset($value['tanque']) && isset($value['tanque']['litros']) ? $value['tanque']['litros'] : '---', '0');
                }
                $letra = ord('S');
                if (isset($value['sensores'])) {   //esto es para los seriados.
                    $tramaSensor = array();     //si se saca esto se produce un arraster de valores anteriores.
                    foreach ($value['sensores'] as $sens) {
                        if (isset($sens['id_sensor']) && isset($sens['medicion_sensor'])) {
                            $tramaSensor[$sens['id_sensor']] = $sens['medicion_sensor'];
                        }
                    }

                    foreach ($servicio->getSensores() as $sensor) {
                        if ($sensor->getModeloSensor()->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                            if (isset($tramaSensor[$sensor->getSerie()])) {
                                $xls->setCellValue(chr($letra) . $i, $tramaSensor[$sensor->getSerie()], '0');
                            }
                        } else {  //no es seriado
                            $campo = $sensor->getModeloSensor()->getCampoTrama();
                            if (isset($value[$campo])) {
                                $xls->setCellValue(chr($letra) . $i, $value[$campo], '0');
                            }
                        }
                        $letra++;
                    }
                }
                $i++;
            }

            //genero el archivo.
            $response = $xls->getResponse();
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'attachment;filename=historial.xls');
            $response->headers->set('Cache-Control', 'maxage=1');
            return $response;
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Error interno al generar la exportación');
            return $this->redirect($this->generateUrl('apmon_homepage'));
        }
    }
}
