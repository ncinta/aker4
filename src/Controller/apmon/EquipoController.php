<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Form\apmon\EquipoType;
use App\Form\dist\ChipDisponibleType;
use App\Form\apmon\ServicioSinAsignarType;
use App\Form\apmon\EquipoRetiroType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\EquipoManager;
use App\Model\app\ChipManager;
use App\Model\app\NotificadorAgenteManager;
use App\Entity\Equipo;

class EquipoController extends AbstractController
{

    private $breadcrumbManager;
    private $userloginManager;
    private $equipoManager;
    private $chipManager;
    private $notificadorAgenteManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ChipManager $chipManager,
        NotificadorAgenteManager $notificadorAgenteManager,
        UserLoginManager $userloginManager,
        EquipoManager $equipoManager
    ) {
        $this->breadcrumbManager =  $breadcrumbManager;
        $this->userloginManager =  $userloginManager;
        $this->equipoManager =  $equipoManager;
        $this->chipManager =  $chipManager;
        $this->notificadorAgenteManager =  $notificadorAgenteManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Equipo');
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * @Route("/apmon/equipo/list", name="apmon_equipo_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER") 
     */
    public function listAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de Equipos");

        $organizacion = $this->userloginManager->getOrganizacion();
        $equipos = $this->equipoManager->findAll($organizacion);

        return $this->render('apmon/Equipo/list.html.twig', array(
            'menu' => $this->getListMenu(),
            'equipos' => $equipos,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu()
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_AGREGAR')) {
            $menu['Agregar Equipo'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_equipo_new')
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/equipo/{id}/show", name="apmon_equipo_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_VER") 
     */
    public function showAction(Request $request, Equipo $equipo)
    {

        if (!$equipo) {
            throw $this->createNotFoundException('Código de Equipo no encontrado.');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), $equipo->getImei());

        $deleteForm = $this->createDeleteForm($equipo->getId());

        return $this->render('apmon/Equipo/show.html.twig', array(
            'menu' => $this->getShowMenu($equipo),
            'equipo' => $equipo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/apmon/equipo/new", name="apmon_equipo_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_AGREGAR") 
     */
    public function newAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Equipo");
        $servicio = 0;
        $organizacion = $this->userloginManager->getOrganizacion();
        $equipo = $this->equipoManager->create($organizacion->getId());
        $form = $this->createForm(EquipoType::class, $equipo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (!$this->equipoManager->ifExist($equipo->getMdmid())) {
                if (!$this->equipoManager->ifExistImei($equipo->getImei())) {
                    $this->breadcrumbManager->pop();

                    if ($this->equipoManager->save($equipo)) {
                        $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                        $this->setFlash('success', sprintf('Se ha creado el equipo "%s" en el sistema.', strtoupper($equipo)));
                        //TODO: sino quiero asociar le chip y solo crear el servio se romperia
                        // la logica del negocio por lo que se deberia avisar al usuario de esto.
                        if ($request->get('asociar_chip') === '1') {
                            if ($request->get('crear_servicio') === '1') {
                                $servicio = 1;
                            } else {
                                $servicio = 0;
                            }
                            return $this->redirect($this->generateUrl('apmon_equipo_asociarchip', array(
                                'idEquipo' => $equipo->getId(),
                                'crearservicio' => $request->get('crear_servicio')
                            )));
                        } else {
                            if ($request->get('crear_servicio') === '1') {
                                return $this->redirect($this->generateUrl('app_servicio_new', array(
                                    'idequipo' => $equipo->getId()
                                )));
                            } else {
                                return $this->redirect($this->generateUrl('apmon_equipo_show', array(
                                    'id' => $equipo->getId()
                                )));
                            }
                        }
                    }
                }
            }
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }

        return $this->render('apmon/Equipo/new.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/equipo/{id}/edit", name="apmon_equipo_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_EDITAR") 
     */
    public function editAction(Request $request, Equipo $equipo)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Equipo");

        if (!$equipo) {
            throw $this->createNotFoundException('Código de Equipo no encontrado.');
        }
        $form = $this->createForm(EquipoType::class, $equipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->equipoManager->save($equipo)) {
                $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($equipo)));
            } else {
                $this->setFlash('success', sprintf('No se ha podido actualiar los datos de %s', strtoupper($equipo)));
            }
            return $this->redirect($this->breadcrumbManager->getVolver());
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }
        return $this->render('apmon/Equipo/edit.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($equipo)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_equipo_edit', array(
                    'id' => $equipo->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_EQUIPO_CHIP_ASIGNAR')) {
            $menu['Asociar Chip'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl(
                    'apmon_equipo_asociarchip',
                    array(
                        'idEquipo' => $equipo->getId(),
                        'crearservicio' => 0
                    )
                )
            );
        }
        if (is_null($equipo->getServicio())) {
            if ($this->userloginManager->isGranted('ROLE_EQUIPO_FLOTA_ASIGNAR')) {
                $menu['Asignar Servicio'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl(
                        'apmon_equipo_asignarservicio',
                        array(
                            'id' => $equipo->getId()
                        )
                    )
                );
            }
            if ($this->userloginManager->isGranted('ROLE_FLOTA_AGREGAR')) {
                $menu['Crearle Servicio'] = array(
                    'imagen' => 'icon-edit icon-blue',
                    'url' => $this->generateUrl(
                        'app_servicio_new',
                        array(
                            'idequipo' => $equipo->getId()
                        )
                    )
                );
            }
        } else {
            if ($this->userloginManager->isGranted('ROLE_EQUIPO_FLOTA_RETIRAR')) {
                $menu['Retirar Equipo'] = array(
                    'imagen' => 'icon-minus icon-blue',
                    'url' => $this->generateUrl('apmon_equipo_retiro', array('id' => $equipo->getId())),
                    'extra' => 'data-toggle=modal',
                );
            }
        }

        if ($equipo->getPropietario() == $this->userloginManager->getOrganizacion() && $this->userloginManager->isGranted('ROLE_EQUIPO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/apmon/equipo/{id}/retiro", name="apmon_equipo_retiro",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_RETIRAR") 
     */
    public function retiroequipoAction(Request $request, Equipo $equipo)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Retiro Equipo");

        $form = $this->createForm(EquipoRetiroType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $retiro = $form->getData();
            if ($this->equipoManager->retirar($equipo->getId(), $retiro)) {
                $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                $this->setFlash('success', 'Equipo retirado del servicio');
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Error al retirar el equipo del servicio');
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
        return $this->render('sauron/Equipo/retiroequipo.html.twig', array(
            'equipo' => $equipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/equipo/{id}/asignarservicio", name="apmon_equipo_asignarservicio",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_FLOTA_ASIGNAR") 
     */
    public function asignarservicioAction(Request $request, Equipo $equipo)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Asignar Servicio");

        $em = $this->getEntityManager();

        if (!$equipo) {
            throw $this->createNotFoundException('Código de Equipo no encontrado.');
        }
        $qbServicios = $em
            ->getRepository('App:Servicio')
            ->findAllServiciosSinAsignar($equipo->getOrganizacion());
        //die('////<pre>' . nl2br(var_export(count($qbServicios), true)) . '</pre>////');
        if ($qbServicios != null) {
            $options['queryResult'] = $qbServicios;

            $form = $this->createForm(ServicioSinAsignarType::class, null, $options);
            // verificamos si el formulario enviado es valido
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $idServicio = $request->get('servicio');
                if ($this->equipoManager->asignarServicio($equipo, $idServicio["servicio"])) {
                    $notificar = $this->notificadorAgenteManager->notificar($equipo, 1);
                    $this->setFlash('success', 'El equipo ha sido asociacido al servicio seleccionado');
                    return ($this->breadcrumbManager->getVolver());
                } else {
                    $this->setFlash('error', 'Error al asociar el servicio al equipo');
                }
            } else {
                return $this->render('apmon/Equipo/asignarservicio.html.twig', array(
                    'form' => $form->createView(),
                    'equipo' => $equipo,
                ));
            }
        } else {
            $this->setFlash('error', sprintf('No existen servicios disponibles para asociar al equipo.'));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * @Route("/apmon/equipo/{idEquipo}/{crearservicio}/asignarservicio", name="apmon_equipo_asociarchip")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_CHIP_ASIGNAR") 
     */
    public function asociarchipAction(Request $request, $idEquipo, $crearservicio)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Asociar chip");
        $em = $this->getEntityManager();
        $equipo = $this->getRepository()->find($idEquipo);
        if (!$equipo) {
            throw $this->createNotFoundException('Código de Equipo no encontrado.');
        }
        $qbChips = $em
            ->getRepository('App:Chip')
            ->findAllChipsDisponibles($equipo->getOrganizacion());
        if ($qbChips != null) {
            $options['queryResult'] = $qbChips;
            $form = $this->createForm(ChipDisponibleType::class, null, $options);
            // verificamos si el formulario enviado es valido
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->breadcrumbManager->pop();
                if ($this->equipoManager->asociarChip($equipo, $request->get('chip'))) {
                    if ($crearservicio == '1') {
                        return $this->redirect($this->generateUrl('app_servicio_new', array(
                            'idequipo' => $equipo->getId()
                        )));
                    } else {
                        return $this->redirect($this->breadcrumbManager->getVolver());
                    }
                }
            } else {
                return $this->render('apmon/Equipo/asociarchip.html.twig', array(
                    'form' => $form->createView(),
                    'equipo' => $equipo,
                    'crearservicio' => $crearservicio,
                ));
            }
        } else {
            $this->setFlash('error', sprintf('No existen chips disponibles para asociar al equipo.'));
            if ($crearservicio == '1') {

                return $this->redirect($this->generateUrl('app_servicio_new', array(
                    'idequipo' => $equipo->getId()
                )));
            } else {

                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }
    }

    /**
     * @Route("/apmon/equipo/{idChip}/retirarchip", name="apmon_equipo_retirarchip")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_CHIP_RETIRAR") 
     */
    public function retirarchipAction($idChip)
    {
        if ($this->chipManager->retirar($idChip)) {
            $this->setFlash('success', 'Chip retirado del equipo con éxito.');
        } else {
            $this->setFlash('success', 'Problemas al retirar el chip del equipo.');
        }


        return $this->redirect($this->breadcrumbManager->getVolver());
    }


    /**
     * @Route("/apmon/equipo/{id}/delete", name="apmon_equipo_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_EQUIPO_ELIMINAR") 
     */
    public function deleteAction(Request $request, Equipo $equipo)
    {
        $form = $this->createDeleteForm($equipo->getId());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $eq = new Equipo();
            $eq->setId($equipo->getId());
            $eq->setCreatedAt($equipo->getCreatedAt());
            $eq->setUpdatedAt($equipo->getUpdatedAt());
            $this->breadcrumbManager->pop();
            if ($this->equipoManager->delete($equipo->getId())) {
                $notificar = $this->notificadorAgenteManager->notificar($equipo, 2);
                $this->setFlash('success', 'El equipo ha sido eliminado.');
            }

            return ($this->breadcrumbManager->getVolver());
        }
    }
}
