<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\apmon\PrestacionType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\CentroCostoManager;
use App\Model\app\TransporteManager;
use App\Model\app\PrestacionManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioManager;
use App\Model\app\ContratistaManager;
use App\Model\app\UtilsManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Entity\Prestacion;

class PrestacionController extends AbstractController
{

    private $userloginManager;
    private $prestacionManager;
    private $breadcrumbManager;
    private $organizacionManager;
    private $centroCostoManager;
    private $transporteManager;
    private $servicioManager;
    private $contratistaManager;
    private $utilsManager;

    function __construct(
        UserLoginManager $userloginManager,
        PrestacionManager $prestacionManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        CentroCostoManager $centroCostoManager,
        TransporteManager $transporteManager,
        ServicioManager $servicioManager,
        ContratistaManager $contratistaManager,
        UtilsManager $utilsManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->prestacionManager = $prestacionManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->centroCostoManager = $centroCostoManager;
        $this->transporteManager = $transporteManager;
        $this->servicioManager = $servicioManager;
        $this->contratistaManager = $contratistaManager;
        $this->utilsManager = $utilsManager;
    }

    /**
     * @Route("/apmon/prestacion/{idorg}/list", name="apmon_prestacion_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_VER")
     */
    public function listAction(Request $request, $idorg)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Prestaciones");

        $organizacion = $this->organizacionManager->find($idorg);
        $prestaciones = $this->prestacionManager->findAllByOrganizacion($idorg);
        $arrayStatus = array();
        $now = new \DateTime();
        foreach ($prestaciones as $prestacion) {
            if ($prestacion->getInicio_prestacion() <= $now && $now <= $prestacion->getFin_prestacion()) {
                $arrayStatus[$prestacion->getId()] = 'Ahora';
            } elseif ($prestacion->getInicio_prestacion() > $now) {
                $arrayStatus[$prestacion->getId()] = 'Futuro';
            } elseif ($prestacion->getFin_prestacion() < $now) {
                $arrayStatus[$prestacion->getId()] = 'Terminado';
            } else {
                $arrayStatus[$prestacion->getId()] = '---';
            }
        }

        return $this->render('apmon/Prestacion/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'prestaciones' => $prestaciones,
            'status' => $arrayStatus,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_PRESTACION_AGREGAR')) {
            $menu['Agregar'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_prestacion_new', array(
                    'idorg' => $organizacion->getId()
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/prestacion/{id}/show", name="apmon_prestacion_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_VER") 
     */
    public function showAction(Request $request, Prestacion $prestacion)
    {

        $deleteForm = $this->createDeleteForm($prestacion->getId());
        if (!$prestacion) {
            throw $this->createNotFoundException('Código de Prestacion no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), 'Ver Prestación');

        return $this->render('apmon/Prestacion/show.html.twig', array(
            'menu' => $this->getShowMenu($prestacion),
            'prestacion' => $prestacion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($prestacion)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_PRESTACION_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_prestacion_edit', array(
                    'id' => $prestacion->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_PRESTACION_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/apmon/prestacion/{idorg}/new", name="apmon_prestacion_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_AGREGAR")
     */
    public function newAction(Request $request, $idorg)
    {

        $organizacion = $this->organizacionManager->find($idorg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el prestacion
        $prestacion = $this->prestacionManager->create($organizacion);
        $options['em'] = $this->getDoctrine()->getManager();
        $options['organizacion'] = $organizacion;
        $form = $this->createForm(PrestacionType::class, $prestacion, $options);
        if ($request->getMethod() == 'POST') {
            $objectivo = ($request->get('prestacion')['objetivo'] != 0) ? $this->centroCostoManager->find($request->get('prestacion')['objetivo']) : null;
            $transporte = ($request->get('prestacion')['transporte'] != 0) ? $this->transporteManager->find($request->get('prestacion')['transporte']) : null;
            $contratista = ($request->get('prestacion')['contratista'] != 0) ? $this->contratistaManager->find($request->get('prestacion')['contratista']) : null;
            //la hora se graba en hora local del cliente
            //obtengo la fecha y la paso a hora utc para que se guarde asi.
            $servicios_post = isset($request->get('prestacion')['servicios']) ? $request->get('prestacion')['servicios'] : null;
            if (!is_null($servicios_post)) {
                $arr_servicios = array();
                foreach ($servicios_post as $serv) {
                    $servicio = $this->servicioManager->find($serv);
                    $prestacion->addServicio($servicio);
                }
            }
            $desde = $hasta = new \DateTime();
            //            die('////<pre>'.nl2br(var_export($request->get('prestacion'), true)).'</pre>////');
            if ($request->get('prestacion')['inicio_prestacion'] != '') {
                $prestacion->setInicioPrestacion($this->toFechaSQL($organizacion, $request->get('prestacion')['inicio_prestacion']));
                $desde = $this->utilsManager->datetime2sqltimestamp($request->get('prestacion')['inicio_prestacion'], false);
            }

            if ($request->get('prestacion')['fin_prestacion'] != '') {
                $prestacion->setFinPrestacion($this->toFechaSQL($organizacion, $request->get('prestacion')['fin_prestacion']));
                $hasta = $this->utilsManager->datetime2sqltimestamp($request->get('prestacion')['fin_prestacion'], true);
            }
            $prestacion->setObjetivo($objectivo);
            $prestacion->setTransporte($transporte);
            $prestacion->setContratista($contratista);
            if ($hasta <= $desde) {
                $this->setFlash('error', sprintf('La fecha de inicio no puede ser mayor a la de fin'));
                $this->breadcrumbManager->pop();
                return $this->breadcrumbManager->getVolver();
            } else {
                if ($this->prestacionManager->save($prestacion)) {
                    //el servicio q se esta creando es un vehiculo
                    $this->setFlash('success', sprintf('El prestacion se ha creado con éxito'));

                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Prestacion");
        return $this->render('apmon/Prestacion/new.html.twig', array(
            'organizacion' => $organizacion,
            'objetivos' => $this->centroCostoManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'contratistas' => $this->contratistaManager->findAllByOrganizacion($organizacion),
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    private function toFechaSQL($organizacion, $fechaIn)
    {
        $sqldate = $this->utilsManager->datetime2sqltimestamp($fechaIn, false);
        $fecha = new \DateTime($sqldate, new \DateTimeZone($organizacion->getTimezone()));
        return $fecha;
    }

    /**
     * @Route("/apmon/prestacion/{id}/edit", name="apmon_prestacion_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_EDITAR") 
     */
    public function editAction(Request $request, Prestacion $prestacion)
    {

        $servicioHold = $prestacion->getServicios();
        if (!$prestacion) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }

        $options['em'] = $this->getDoctrine()->getManager();
        $options['organizacion'] = $prestacion->getOrganizacion();
        $form = $this->createForm(PrestacionType::class, $prestacion, $options);
        $organizacion = $prestacion->getOrganizacion();
        if ($request->getMethod() == 'POST') {
            $objectivo = ($request->get('prestacion')['objetivo'] != 0) ? $this->centroCostoManager->find($request->get('prestacion')['objetivo']) : $prestacion->getObjetivo();
            $transporte = ($request->get('prestacion')['transporte'] != 0) ? $this->transporteManager->find($request->get('prestacion')['transporte']) : $prestacion->getTransporte();
            $contratista = ($request->get('prestacion')['contratista'] != 0) ? $this->contratistaManager->find($request->get('prestacion')['contratista']) : $prestacion->getContratista();
            //la hora se graba en hora local del cliente
            //obtengo la fecha y la paso a hora utc para que se guarde asi.
            $servicios_post = isset($request->get('prestacion')['servicios']) ? $request->get('prestacion')['servicios'] : null;
            if (count($servicioHold) > 0) {
                foreach ($servicioHold as $servicio) {
                    $prestacion->removeServicio($servicio);
                    $servicio->removePrestacion($prestacion);
                    $servicio = $this->servicioManager->save($servicio);
                }
            }
            if (!is_null($servicios_post)) {
                $arr_servicios = array();
                foreach ($servicios_post as $serv) {
                    $servicio = $this->servicioManager->find($serv);
                    $prestacion->addServicio($servicio);
                }
            } else {
                // si viene vacio borramos todo
                foreach ($prestacion->getServicios() as $servicio) {
                    $prestacion->removeServicio($servicio);
                    $servicio->removePrestacion($prestacion);
                    $servicio = $this->servicioManager->save($servicio);
                }
            }
            $prestacion->setInicioPrestacion($this->toFechaSQL($organizacion, $request->get('prestacion')['inicio_prestacion']));
            $prestacion->setFinPrestacion($this->toFechaSQL($organizacion, $request->get('prestacion')['fin_prestacion']));
            $prestacion->setObjetivo($objectivo);
            $prestacion->setTransporte($transporte);
            $prestacion->setContratista($contratista);
            $desde = $this->utilsManager->datetime2sqltimestamp($request->get('prestacion')['inicio_prestacion'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($request->get('prestacion')['fin_prestacion'], true);
            if ($hasta <= $desde) {
                $this->setFlash('error', sprintf('La fecha de inicio no puede ser mayor a la de fin'));
                $this->breadcrumbManager->pop();
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                if ($this->prestacionManager->save($prestacion)) {
                    //el servicio q se esta creando es un vehiculo
                    $this->setFlash('success', sprintf('El prestacion se ha editado con éxito'));

                    $this->breadcrumbManager->pop();
                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }


        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Prestacion");
        return $this->render('apmon/Prestacion/edit.html.twig', array(
            'organizacion' => $organizacion,
            'objetivos' => $this->centroCostoManager->findAllByOrganizacion($organizacion),
            'transportes' => $this->transporteManager->findAllByOrganizacion($organizacion),
            'contratistas' => $this->contratistaManager->findAllByOrganizacion($organizacion),
            'prestacion' => $prestacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/prestacion/{id}/delete", name="apmon_prestacion_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_ELIMINAR") 
     */
    public function deleteAction(Request $request, Prestacion $prestacion)
    {

        $form = $this->createDeleteForm($prestacion->getId());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->breadcrumbManager->pop();

            if ($this->prestacionManager->deleteById($prestacion->getId())) {
                $this->setFlash('success', 'Prestacion eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el servicio del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
