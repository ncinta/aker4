<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\apmon\UsuarioNewType;
use App\Form\apmon\UsuarioEditType;
use App\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\PaisManager;
use App\Model\app\UsuarioManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ServicioManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\UtilsManager;
use PDOException;

class UsuarioController extends AbstractController
{

    private $breadcrumbManager;
    private $userlogin;
    private $utilsManager;
    private $servicioManager;
    private $paisManager;
    private $usuarioManager;
    private $organizacionManager;
    private $grupoReferenciaManager;

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Usuario');
    }

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        UserLoginManager $userlogin,
        UtilsManager $utilsManager,
        ServicioManager $servicioManager,
        PaisManager $paisManager,
        UsuarioManager $usuarioManager,
        OrganizacionManager $organizacionManager,
        GrupoReferenciaManager $grupoReferenciaManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->userlogin = $userlogin;
        $this->paisManager = $paisManager;
        $this->utilsManager = $utilsManager;
        $this->servicioManager = $servicioManager;
        $this->usuarioManager = $usuarioManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
    }

    /**
     * @Route("/apmon/usuario/list", name="apmon_usuario_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_VER") 
     */
    public function listAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de Usuarios");

        $organizacion = $this->userlogin->getOrganizacion();

        $em = $this->getEntityManager();
        $user = $this->userlogin->getUser();

        if ($user->getLogistica() != null) {
            $usuarios = $user->getLogistica()->getUsuarios();
        } elseif ($user->getTransporte() != null) {
            $usuarios = $user->getTransporte()->getUsuarios();
        }else{
            $usuarios = $em->getRepository('App:Organizacion')->findAllUsuarios($organizacion);
        }

        return $this->render(
            'apmon/Usuario/list.html.twig',
            array(
                'menu' => $this->getListMenu($organizacion),
                'usuarios' => $usuarios,
                'url_shell' => $this->userlogin->findHelpShell('TUTOENEX_CREARUSUARIO'),
            )
        );
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if ($this->userlogin->isGranted('ROLE_USUARIO_AGREGAR')) {
            $menu['Agregar Usuario'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_usuario_new')
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/usuario/{id}/show", name="apmon_usuario_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_VER") 
     */
    public function showAction(Request $request, Usuario $usuario)
    {

        if (!$usuario) {
            throw $this->createNotFoundException('Código de Usuario no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), $usuario->getNombre());

        $usuarioFechas = array(
            'lastLogin' => $this->utilsManager->fechaUTC2local($usuario->getLastLogin()),
            'updatedAt' => $this->utilsManager->fechaUTC2local($usuario->getUpdatedAt()),
            'createdAt' => $this->utilsManager->fechaUTC2local($usuario->getCreatedAt()),
        );
        //die('////<pre>' . nl2br(var_export($usuarioFechas, true)) . '</pre>////');
        $deleteForm = $this->createDeleteForm($usuario->getId());

        return $this->render('apmon/Usuario/show.html.twig', array(
            'usuario' => $usuario,
            'usuariofechas' => $usuarioFechas,
            'menu' => $this->getShowMenu($usuario),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $usuario
     * @return array $menu
     */
    private function getShowMenu($usuario)
    {
        $menu = array();
        if ($this->userlogin->isGranted('ROLE_USUARIO_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_usuario_edit', array(
                    'id' => $usuario->getId()
                ))
            );
            //esto es para que el usuario no pueda cambiarse los permisos solo
            if ($usuario->getId() != $this->userlogin->getUser()->getId()) {
                $menu['Permisos'] = array(
                    'imagen' => '',
                    'url' => $this->generateUrl('apmon_usuario_permiso', array(
                        'id' => $usuario->getId()
                    ))
                );
            }
        }
        if ($this->userlogin->isGranted('ROLE_USUARIO_CAMBIARCLAVE')) {
            $menu['Cambiar Clave'] = array(
                'imagen' => '',
                'url' => $this->generateUrl('user_profile_password_change', array(
                    'id' => $usuario->getId()
                ))
            );
        }
        if ($this->userlogin->isGranted('ROLE_USUARIO_SERVICIO_VER')) {
            //el usuario master no puede tener equipos asignados. Debe ver todos !!!!
            $menu['Ver Servicios Asignados'] = array(
                'imagen' => '',
                'url' => $this->generateUrl('apmon_usuario_servicios_asignados', array(
                    'id' => $usuario->getId()
                ))
            );
        }
        if ($this->userlogin->isGranted('ROLE_USUARIO_REFERENCIA_VER')) {
            //el usuario master no puede tener referencias asignadas. Debe ver todos !!!!
            $menu['Ver Referencias Asignados'] = array(
                'imagen' => '',
                'url' => $this->generateUrl('apmon_usuario_referencias_asignadas', array(
                    'id' => $usuario->getId()
                ))
            );
        }
        if ($this->userlogin->isGranted('ROLE_USUARIO_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/apmon/usuario/new", name="apmon_usuario_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_AGREGAR") 
     */
    public function newAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Usuario");

        $organizacion = $this->userlogin->getOrganizacion();
        if (!$organizacion) {
            throw $this->createNotFoundException('Código de cliente no encontrado.');
        }
        $usuario = $this->usuarioManager->create($organizacion);

        $options['paises'] = $this->paisManager->getArrayPaises();
        $form = $this->createForm(UsuarioNewType::class, $usuario, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            try {
                $this->usuarioManager->save($usuario);
                $this->setFlash('success', 'El usuario ha sido creado');

                return $this->redirect($this->generateUrl('apmon_usuario_permiso', array(
                    'id' => $usuario->getId()
                )));
            } catch (PDOException $e) {
                $this->setFlash('error', sprintf('Error en la creación del usuario. Es posible que ya exista el usuario o email. (%s)', toString($e)));
            }
        }
        return $this->render('apmon/Usuario/new.html.twig', array(
            'organizacion' => $organizacion,
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/usuario/{id}/edit", name="apmon_usuario_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_EDITAR") 
     */
    public function editAction(Request $request, Usuario $usuario)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Usuario");

        $em = $this->getEntityManager();

        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $options['organizacion'] = $usuario->getOrganizacion();
        $options['em'] = $em;
        $options['paises'] = $this->paisManager->getArrayPaises();
        $form = $this->createForm(UsuarioEditType::class, $usuario, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            // se graba el usuario
            $em->persist($usuario);

            //se setean los roles segun estan en las tablas de permisos.
            $usuario->setRoles(array());
            if ($usuario->getOrganizacion()->getUsuarioMaster() == $usuario) {
                if ($usuario->getOrganizacion()->getTipoOrganizacion() == '1') {
                    $usuario->addRole('ROLE_MASTER_DISTRIBUIDOR');
                } else {
                    $usuario->addRole('ROLE_MASTER_CLIENTE');
                }
            }
            foreach ($usuario->getPermisos() as $permiso) {
                $usuario->addRole($permiso->getCodename());
            }

            // se graba el usuario
            $em->persist($usuario);
            $em->flush();

            $this->setFlash('success', sprintf('Los datos de %s han sido actualizados', $usuario));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }

        $modulos = $em->getRepository('App:Organizacion')
            ->getModulosActivos($usuario->getOrganizacion());

        return $this->render('apmon/Usuario/edit.html.twig', array(
            'modulos' => $modulos,
            'usuario' => $usuario,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/usuario/{id}/permiso", name="apmon_usuario_permiso",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_EDITAR") 
     */
    public function permisoAction(Request $request, Usuario $usuario)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), 'Asignar Permisos');
        $roles = array();
        $rolesOld = array();

        foreach ($usuario->getPermisos() as $permiso) {
            $rolesOld[] = $permiso->getCodename();
        }
        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }
        if ($usuario == $this->userlogin->getUser()) {
            throw $this->createNotFoundException('Código de usuario inaccesible.');
        }
        $modulos = $this->organizacionManager->getModulosActivos($usuario->getOrganizacion());

        if ($request->getMethod() == 'POST') {
            $form = $request->get('permiso');

            $this->breadcrumbManager->pop();
            //borro todos los roles del usuario
            $usuario->setRoles(array());
            $usuario->setPermisos(null);
            //le asigino el role de master en caso de que sea.
            if ($usuario->getOrganizacion()->getUsuarioMaster() == $usuario) {
                $permiso = $this->getEntityManager()->getRepository('App:Permiso')->findOneBy(array('codename' => 'ROLE_MASTER_CLIENTE'));
                $usuario->addPermiso($permiso);
                $usuario->addRole('ROLE_MASTER_CLIENTE');
            }
            if (!is_null($form)) {
                //agrego los permisos que estan en el form.
                foreach ($form as $codename) {
                    $permiso = $this->getEntityManager()->getRepository('App:Permiso')->findOneBy(array('codename' => $codename));
                    $usuario->addPermiso($permiso);
                    $usuario->addRole($codename);
                    $roles[] = $codename;
                }
            }
            //  die('////<pre>' . nl2br(var_export($rolesOld, true)) . '</pre>////');
            // se graba el usuario
            $diff_add = array_diff($roles, $rolesOld);
            $diff_remove = array_diff($rolesOld, $roles);
            $this->usuarioManager->save($usuario, $diff_add, $diff_remove);

            $this->setFlash('success', sprintf('Los permisos de "%s" han sido actualizados', $usuario));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        return $this->render('apmon/Usuario/permiso.html.twig', array(
            'modulos' => $modulos,
            'usuario' => $usuario,
            'tipo' => $usuario->getOrganizacion()->getTipoOrganizacion(),
        ));
    }

    /**
     * @Route("/apmon/usuario/{id}/delete", name="apmon_usuario_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_ELIMINAR") 
     */
    public function deleteAction(Request $request, Usuario $usuario)
    {

        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }
        //$return = $this->generateUrl('apmon_usuario_show');

        $form = $this->createDeleteForm($usuario->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->usuarioManager->delete($usuario);
            $this->setFlash('success', 'Usuario Eliminado con éxito');
            $this->breadcrumbManager->pop();
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }


    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }


    /**
     * @Route("/apmon/usuario/{id}/serviciosasignados", name="apmon_usuario_servicios_asignados",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_SERVICIO_VER") 
     */
    public function serviciosasginadosAction(Request $request, Usuario $usuario)
    {

        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $disponibles = $this->servicioManager->findAllSinAsignar2Usuario($usuario);
        $asignados = $this->servicioManager->findSoloAsignadosUsuario($usuario);

        return $this->render('apmon/Usuario/serviciosasginados.html.twig', array(
            'usuario' => $usuario,
            'disponibles' => $disponibles,
            'asignados' => $asignados,
        ));
    }

    /**
     * @Route("/apmon/usuario/servicio/asignar", name="apmon_usuario_servicios_asignar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_SERVICIO_ASOCIAR") 
     */
    public function servicioasignarAction(Request $request)
    {
        $usuario = $this->usuarioManager->find($request->get('id'));

        $idsServicios = unserialize($request->get('servicios'));
        if ($usuario && count($idsServicios) > 0) {
            foreach ($idsServicios as $value) {
                $this->servicioManager->asignarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('apmon/Usuario/show_disponibles.html.twig', array(
                'disponibles' => $this->servicioManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('apmon/Usuario/show_asignados.html.twig', array(
                'asignados' => $this->servicioManager->findSoloAsignadosUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/apmon/usuario/servicio/eliminar", name="apmon_usuario_servicios_eliminar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_SERVICIO_DESASOCIAR") 
     */
    public function servicioeliminarAction(Request $request)
    {
        $usuario = $this->usuarioManager->find($request->get('id'));

        $idsServicios = unserialize($request->get('servicios'));
        if ($usuario && count($idsServicios) > 0) {
            foreach ($idsServicios as $value) {
                $this->servicioManager->eliminarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('apmon/Usuario/show_disponibles.html.twig', array(
                'disponibles' => $this->servicioManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('apmon/Usuario/show_asignados.html.twig', array(
                'asignados' => $this->servicioManager->findSoloAsignadosUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/apmon/usuario/{id}/referenciasasignadas", name="apmon_usuario_referencias_asignadas")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_VER") 
     */
    public function referenciasasignadasAction(Request $request, Usuario $usuario)
    {

        if (!$usuario) {
            throw $this->createNotFoundException('Código de usuario no encontrado.');
        }

        $disponibles = $this->grupoReferenciaManager->findAllSinAsignar2Usuario($usuario);
        $asignados = $this->grupoReferenciaManager->findAllByUsuario($usuario);

        return $this->render('apmon/Usuario/referenciasasignadas.html.twig', array(
            'usuario' => $usuario,
            'disponibles' => $disponibles,
            'asignados' => $asignados,
        ));
    }

    /**
     * @Route("/apmon/usuario/referencias/asignar", name="apmon_usuario_referencias_asignar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_ASOCIAR") 
     */
    public function referenciasasignarAction(Request $request)
    {
        $usuario = $this->usuarioManager->find($request->get('id'));

        $idsGrpRef = unserialize($request->get('referencias'));
        if ($usuario && count($idsGrpRef) > 0) {
            foreach ($idsGrpRef as $key => $value) {
                $this->grupoReferenciaManager->asignarUsuario($value, $usuario);
            }
        }

        $respuesta = array(
            'disponibles' => $this->renderView('apmon/Usuario/show_grp_disponibles.html.twig', array(
                'disponibles' => $this->grupoReferenciaManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('apmon/Usuario/show_grp_asignados.html.twig', array(
                'asignados' => $this->grupoReferenciaManager->findAllByUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/apmon/usuario/referencias/eliminar", name="apmon_usuario_referencias_eliminar")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_USUARIO_REFERENCIA_DESASOCIAR") 
     */
    public function referenciaseliminarAction(Request $request)
    {
        $usuario = $this->usuarioManager->find($request->get('id'));

        $idsGrpRef = unserialize($request->get('referencias'));
        if ($usuario && count($idsGrpRef) > 0) {
            foreach ($idsGrpRef as $key => $value) {
                $this->grupoReferenciaManager->eliminarUsuario($value, $usuario);
            }
        }
        $respuesta = array(
            'disponibles' => $this->renderView('apmon/Usuario/show_grp_disponibles.html.twig', array(
                'disponibles' => $this->grupoReferenciaManager->findAllSinAsignar2Usuario($usuario)
            )),
            'asignados' => $this->renderView('apmon/Usuario/show_grp_asignados.html.twig', array(
                'asignados' => $this->grupoReferenciaManager->findAllByUsuario($usuario)
            )),
        );
        return new Response(json_encode($respuesta));
    }
}
