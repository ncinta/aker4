<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\apmon\IbuttonType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\IbuttonManager;
use App\Model\app\HistoricoIbuttonManager;
use App\Model\app\OrganizacionManager;
use App\Entity\Ibutton;

/**
 * Description of IbuttonController
 *
 * @author nicolas
 */
class IbuttonController extends AbstractController
{

    protected $userloginManager;
    protected $breadcrumb;
    protected $organizacionManager;
    protected $ibuttonManager;
    protected $histIbuttonManager;

    function __construct(
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        IbuttonManager $ibuttonManager,
        HistoricoIbuttonManager $histIbuttonManager
    ) {
        $this->breadcrumb = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->userloginManager = $userloginManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->histIbuttonManager = $histIbuttonManager;
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($id)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_IBUTTON_AGREGAR')) {
            $menu['Agregar Ibutton'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_ibutton_new', array('idorg' => $id))
            );
        }
        return $menu;
    }

    private function getShowMenu($ibutton)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_IBUTTON_EDITAR')) {
            if ($ibutton->getEstado() == 1) { // solamente puede tomar acciones si se encuentra en deposito
                $menu['Editar'] = array(
                    'imagen' => 'icon-editar icon-blue',
                    'url' => $this->generateUrl('apmon_ibutton_edit', array('id' => $ibutton->getId()))
                );
                if ($this->userloginManager->isGranted('ROLE_IBUTTON_ELIMINAR')) {
                    $menu['Eliminar'] = array(
                        'imagen' => 'icon-minus icon-blue',
                        'url' => '#modalDelete',
                        'extra' => 'data-toggle=modal',
                    );
                }
            }
        }
        return $menu;
    }

    /**
     * @Route("/apmon/ibutton/{idorg}/list", name="apmon_ibutton_list")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request, $idorg)
    {

        $this->breadcrumb->push($request->getRequestUri(), 'Ibuttons');
        $organizacion = $this->organizacionManager->find($idorg);
        $ibuttons = $this->ibuttonManager->find($organizacion);
        return $this->render('apmon/Ibutton/list.html.twig', array(
            'idorg' => $idorg,
            'ibuttons' => $ibuttons,
            'menu' => $this->getListMenu($idorg)
        ));
    }

    /**
     * @Route("/apmon/ibutton/{idorg}/new", name="apmon_ibutton_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idorg)
    {

        $this->breadcrumb->push($request->getRequestUri(), "Nuevo Ibutton");
        $organizacion = $this->organizacionManager->find($idorg);
        $ibutton = $this->ibuttonManager->create($organizacion);

        $form = $this->createForm(IbuttonType::class, $ibutton);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // dd($request->get('ibutton'));
            $dallas = $request->get('ibutton')['dallas'];
            $codigo = $request->get('ibutton')['dallas'];
            $tipo = intval($request->get('ibutton')['tipo']);
            if($tipo == 0){
                $this->breadcrumb->push($request->getRequestUri(), "Error Tipo");
                $this->setFlash('error', sprintf('Debe serleccionar un tipo de dispositivo'));
                return $this->redirect($this->breadcrumb->getVolver());
            }
            
            if ($tipo == 1) { // es ibutton colocamos como codigo el dallas al reves
                if (strlen($dallas) < 16) {
                    $this->breadcrumb->push($request->getRequestUri(), "Error Ibutton");
                    $this->setFlash('error', sprintf('Faltan caracteres'));
                    return $this->redirect($this->breadcrumb->getVolver());
                }
                $i = strlen($dallas) - 1;
                $j = 0;
                $arr_cod = array();
                // recorro el dallas y lo doy vuelta para crear el codigo
                while ($i > 0) {
                    $arr_cod[$j] = $dallas[$i - 1];
                    $arr_cod[$j + 1] = $dallas[$i];
                    $i = $i - 2;
                    $j = $j + 2;
                }
                $codigo = implode($arr_cod);    //en el codigo esta lo que viene en la trama
            }
            $ibutton->setDallas($dallas);
            $ibutton->setCodigo($codigo);   //siempre buscar por codigo.
            $ibutton->setOrganizacion($organizacion);
            $ibutton->setEstado(1); // 1 = INACTIVO (DEPOSITO)   2= ACTIVO (VEHICULO) 3 = EXTRAVIADO
            $ibutton = $this->ibuttonManager->save($ibutton);
            $this->breadcrumb->pop();
            $this->setFlash('success', sprintf('El ibutton se ha creado con éxito'));
            return $this->redirect($this->generateUrl('apmon_ibutton_list', array(
                'idorg' => $organizacion->getId(),
            )));
        }

        return $this->render('apmon/Ibutton/new.html.twig', array(
            'organizacion' => $organizacion,
            'ibutton' => $ibutton,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/ibutton/{id}/edit", name="apmon_ibutton_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ibutton $ibutton)
    {

        $this->breadcrumb->push($request->getRequestUri(), "Editar Ibutton");

        $oldDallas = $ibutton->getDallas();

        $form = $this->createForm(IbuttonType::class, $ibutton);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tipo = intval($request->get('ibutton')->getTipo());
            //dd(intval($request->get('ibutton')->getTipo()));
            $dallas = $request->get('ibutton')->getDallas();
            $codigo = $dallas;
            if($tipo == 0){
                $this->breadcrumb->push($request->getRequestUri(), "Error Tipo");
                $this->setFlash('error', sprintf('Debe serleccionar un tipo de dispositivo'));
                return $this->redirect($this->breadcrumb->getVolver());
            }
            if ($tipo == 1) { // es ibutton colocamos como codigo el dallas al reves
                if (strlen($dallas) < 16) {
                    $this->breadcrumb->push($request->getRequestUri(), "Error Ibutton");
                    $this->setFlash('error', sprintf('Faltan caracteres'));
                    return $this->redirect($this->breadcrumb->getVolver());
                }
                if (strcasecmp($dallas, $oldDallas) != 0) { // si el dallas nuevo es distinto grabamos
                    $i = strlen($dallas) - 1;
                    $j = 0;
                    $arr_cod = array();
                    // recorro el dallas y lo doy vuelta para crear el codigo
                    while ($i > 0) {
                        $arr_cod[$j] = $dallas[$i - 1];
                        $arr_cod[$j + 1] = $dallas[$i];
                        $i = $i - 2;
                        $j = $j + 2;
                    }
                    $codigo = implode($arr_cod);
                }
            }
            $ibutton->setDallas($dallas);
            $ibutton->setCodigo($codigo);
            $ibutton = $this->ibuttonManager->save($ibutton);
            $this->setFlash('success', sprintf('El ibutton se ha modificado con éxito'));
            $this->breadcrumb->pop();
            return $this->redirect($this->generateUrl('apmon_ibutton_list', array(
                'idorg' => $ibutton->getOrganizacion()->getId(),
            )));
        }
        return $this->render('apmon/Ibutton/edit.html.twig', array(
            'ibutton' => $ibutton,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/ibutton/{id}/historico", name="apmon_ibutton_historico",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function historicoAction(Request $request, Ibutton $ibutton)
    {

        $this->breadcrumb->push($request->getRequestUri(), "Historico Ibutton");
        $historico = $this->histIbuttonManager->findByIbutton($ibutton);

        return $this->render('apmon/Ibutton/show.html.twig', array(
            'ibutton' => $ibutton,
            'historico' => $historico,
            'menu' => $this->getShowMenu($ibutton, $this->userloginManager),
            'delete_form' => $this->createDeleteForm($ibutton->getId())->createView(),
            'tab' => 'historico'
        ));
    }

    /**
     * @Route("/apmon/ibutton/{id}/show", name="apmon_ibutton_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Ibutton $ibutton)
    {

        $this->breadcrumb->push($request->getRequestUri(), "Ver Ibutton");

        return $this->render('apmon/Ibutton/show.html.twig', array(
            'ibutton' => $ibutton,
            'menu' => $this->getShowMenu($ibutton),
            'delete_form' => $this->createDeleteForm($ibutton->getId())->createView(),
            'tab' => 'main'
        ));
    }

    /**
     * @Route("/apmon/ibutton/{id}/delete", name="apmon_ibutton_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Ibutton $ibutton)
    {

        $form = $this->createDeleteForm($ibutton->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumb->pop();

            if ($this->ibuttonManager->deleteById($ibutton->getId())) {
                $this->setFlash('success', 'Ibutton eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el ibutton del sistema.');
            }
        }

        return $this->redirect($this->breadcrumb->getVolver());
    }
}
