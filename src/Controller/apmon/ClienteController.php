<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Form\apmon\ClienteEditType;
use App\Entity\Organizacion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\gpm\ClienteManager;
use App\Model\app\OrganizacionManager;


class ClienteController extends AbstractController
{

    protected $organizacionManager;
    protected $clienteManager;

    function __construct(OrganizacionManager $organizacionManager, ClienteManager $clienteManager)
    {
        $this->organizacionManager = $organizacionManager;
        $this->clienteManager = $clienteManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Organizacion');
    }

    /**
     * @Route("/apmon/cliente/{id}/show/{tab}", name="apmon_cliente_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_VER")
     */
    public function showAction(
        Request $request,
        Organizacion $cliente,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        $tab = null
    ) {

        if ($userloginManager->getOrganizacion()->getId() == $cliente->getId()) {
            $breadcrumbManager->push($request->getRequestUri(), 'Mi Cuenta');
        } else {
            $breadcrumbManager->push($$request->getRequestUri(), $cliente->getNombre() . '(Datos)');
        }

        return $this->render('apmon/Cliente/show.html.twig', array(
            'menu' => $this->getShowMenu($cliente, $userloginManager),
            'cliente' => $cliente,
            'tab' => $tab
        ));
    }

    /**
     * @Route("/apmon/cliente/{id}/show/{tab}", name="apmon_cliente_show_deposito",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_DEPOSITO_ADMIN")
     */
    public function showdepositoAction(
        Request $request,
        Organizacion $cliente,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        $tab = null
    ) {

        $breadcrumbManager->pop();
        $em = $this->getEntityManager();
        $breadcrumbManager->push($request->getRequestUri(), $cliente->getNombre() . '(Deposito)');

        //traigo todo los clientes del dist.
        $equipos = $em->getRepository('App:Equipo')->findAllEquipos($cliente);

        return $this->render('apmon/Cliente/show.html.twig', array(
            'menu' => $this->getShowMenu($cliente, $userloginManager),
            'cliente' => $cliente,
            'equipos' => $equipos,
            'tab' => $tab
        ));
    }

    /**
     * @Route("/apmon/cliente/{id}/show/{tab}", name="apmon_cliente_show_servicios",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_APMON_FLOTA_ADMIN")
     */
    public function showserviciosAction(
        Request $request,
        Organizacion $cliente,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        $tab = null
    ) {

        $breadcrumbManager->pop();
        $em = $this->getEntityManager();

        $breadcrumbManager->push($request->getRequestUri(), $cliente->getNombre() . '(Servicios)');

        //traigo todo los clientes del dist.
        $servicios = $em->getRepository('App:Servicio')->findAllServicios($cliente);

        return $this->render('apmon/Cliente/show.html.twig', array(
            'menu' => $this->getShowMenu($cliente, $userloginManager),
            'cliente' => $cliente,
            'servicios' => $servicios,
            'tab' => $tab
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $cliente 
     * @return array $menu
     */
    private function getShowMenu($cliente, $userlogin)
    {
        $menu = array();
        if ($userlogin->isGranted('ROLE_ORGANIZACION_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_cliente_edit', array(
                    'id' => $cliente->getId()
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/cliente/{id}/edit", name="apmon_cliente_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_ORGANIZACION_EDITAR")
     */
    public function editAction(
        Request $request,
        Organizacion $cliente,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager
    ) {

        $breadcrumbManager->push($request->getRequestUri(), "Editar Cliente");

        $em = $this->getEntityManager();

        if (!$cliente) {
            throw $this->createNotFoundException('Código de Cliente no encontrado.');
        }

        $form = $this->createForm(ClienteEditType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $breadcrumbManager->pop();
                $em->persist($cliente);
                $em->flush();

                $this->setFlash('success', 'Los datos de su cuenta han sido actualizados.');

                return $breadcrumbManager->getVolver();
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        return $this->render('apmon/Cliente/edit.html.twig', array(
            'return' => $this->getReturnEdit($cliente, $userloginManager),
            'cliente' => $cliente,
            'form' => $form->createView(),
        ));
    }

    private function getReturnEdit($cliente, $userlogin)
    {
        return $this->generateUrl('apmon_cliente_show', array(
            'id' => $cliente->getId(),
            'tab' => 'main'
        ));
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/gpm/cliente/getcliente",options={"expose"=true},  name="gpm_cliente_get")
     * @Method({"GET", "POST"})
     */
    public function getClienteAction(Request $request)
    {
        $id = $request->get('id');
        $search = $request->get('search');
        $organizacion = $this->organizacionManager->find(intval($id));
        // die('////<pre>' . nl2br(var_export($proyecto->getId(), true)) . '</pre>////');
        $clientes = $this->clienteManager->findAllQuery($organizacion, $search);
        $str = $this->getArrayClientes($clientes);
        return new Response(json_encode(array('results' => $str)), 200);
    }

    /**
     * @Route("/gpm/cliente/new", options={"expose"=true}, name="gpm_cliente_new")
     * @Method({"GET", "POST"})
     */
    public function newajaxAction(Request $request)
    {
        $id = $request->get('id');
        $organizacion = $this->organizacionManager->find($id);
        $tmp = array();
        parse_str($request->get('formCliente'), $tmp);
        //  die('////<pre>' . nl2br(var_export($id, true)) . '</pre>////');
        $dataCont = $tmp['cliente'];
        $cliente = $this->clienteManager->create($organizacion);
        $cliente->setNombre($dataCont['nombre']);
        $clieSave = $this->clienteManager->save($cliente);
        return new Response(json_encode(array('id' => $clieSave->getId(), 'nombre' => $clieSave->getNombre())), 200);
    }

    private function getArrayClientes($clientes)
    {
        $str = array();
        foreach ($clientes as $cont) {
            $x = $cont->getNombre();
            $str[] = array('id' => $cont->getId(), 'text' => $x);
        }
        return $str;
    }
}
