<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Vehiculo;
use App\Form\app\VehiculoType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ServicioManager;
use App\Model\app\VehiculoModeloManager;

class VehiculoController extends AbstractController
{

    private $breadcrumbManager;
    private $servicioManager;
    private $userloginManager;
    private $vehiculomodeloManager;

    function __construct(
        BreadcrumbManager $breadcrumbManager,
        ServicioManager $servicioManager,
        UserLoginManager $userloginManager,
        VehiculoModeloManager $vehiculomodeloManager
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->servicioManager = $servicioManager;
        $this->userloginManager = $userloginManager;
        $this->vehiculomodeloManager = $vehiculomodeloManager;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Vehiculo');
    }

    /**
     *
     * @Route("/apmon/vehiculo/{idservicio}/new", name="apmon_vehiculo_new",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_AGREGAR") 
     */
    public function newAction(Request $request, $idservicio)
    {

        $servicio = $this->servicioManager->find($idservicio);

        $vehiculo = new Vehiculo();
        $vehiculo->setServicio($servicio);

        $form = $this->getVehiculoForm($servicio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $data = $request->get('appbundle_vehiculotype');
            $patente = $this->servicioManager->existePatente($data['patente'] == '' ? null : $data['patente']);

            if ($patente) {
                $this->setFlash('error', sprintf('La patente <b>%s</b> ya se encuentra registrada para <b>%s</b>', $data['patente'], $vehiculo->getServicio()));
            } else {
                $vehiculo->setPatente($data['patente'] == '' ? null : $data['patente']);
                $vehiculo->setMarca($data['marca'] == '' ? null : $data['marca']);
                $vehiculo->setModelo($data['modelo'] == '' ? null : $data['modelo']);
                $vehiculo->setColor($data['color'] == '' ? null : $data['color']);
                $vehiculo->setNumeroMotor($data['numeroMotor'] == '' ? null : $data['numeroMotor']);
                $vehiculo->setNumeroChasis($data['numeroChasis'] == '' ? null : $data['numeroChasis']);
                $vehiculo->setCedulaVerde($data['cedulaVerde'] == '' ? null : $data['cedulaVerde']);
                $vehiculo->setLitrosTanque($data['litrosTanque'] == '' ? null : $data['litrosTanque']);
                $vehiculo->setRendimiento($data['rendimiento'] == '' ? null : $data['rendimiento']);
                $vehiculo->setRendimientoHora($data['rendimientoHora'] == '' ? null : $data['rendimientoHora']);
                $vehiculo->setAnioFabricacion($data['anioFabricacion'] == '' ? null : $data['anioFabricacion']);
                if ($data['carModelo'] != '') {

                    $carModelo = $this->vehiculomodeloManager->findOne($data['carModelo']);
                    $vehiculo->setCarModelo($carModelo);
                }

                foreach ($vehiculo->getDatosExtra() as $dato) {
                    $dato->setVehiculo($vehiculo);
                }
                $servicio->setVehiculo($vehiculo);
                $em = $this->getEntityManager();
                $em->persist($vehiculo);
                $em->flush();
                $this->setFlash('success', sprintf('La configuración del vehiculo <b>%s</b> ha sido exitoso.', strtoupper($vehiculo->getServicio())));

                if ($vehiculo->getOdometro() != null) {   //cambio el odometro por variable de ajuste
                    $this->servicioManager->cambiarOdometro($servicio, $vehiculo->getOdometro());
                }
                if ($vehiculo->getHorometro() != null) {   //cambio el odometro por variable de ajuste
                    $this->servicioManager->cambiarHorometro($servicio, $vehiculo->getHorometro());
                }

                return $this->redirect($this->generateUrl('servicio_show', array(
                    'id' => $vehiculo->getServicio()->getId(),
                    'tab' => 'main',
                )));
            }
        }

        $this->breadcrumbManager->push($request->getRequestUri(), 'Nuevo Vehiculo');
        return $this->render('apmon/Vehiculo/new.html.twig', array(
            'servicio' => $servicio,
            'vehiculo' => $vehiculo,
            'form' => $form->createView()
        ));
    }

    private function getVehiculoForm($servicio)
    {
        $options = array(
            'required_patente' => $this->userloginManager->isModuloEnabled('CLIENTE_SHELL'),
            'required_portal' => $this->userloginManager->isModuloEnabled('CLIENTE_PORTAL'),
            'organizacion' => $this->userloginManager->getOrganizacion(),
            'new' => true,
            //'em' => $this->getEntityManager(),
            'requiredTanque' => ($servicio->getTipoMedidorCombustible() == 2 ||
                $servicio->getTipoMedidorCombustible() == 3) ? true : false
        );

        return $this->createForm(VehiculoType::class, null, $options);
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
