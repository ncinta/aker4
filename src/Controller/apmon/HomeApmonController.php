<?php

namespace App\Controller\apmon;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\DominioManager;
use App\Model\app\NotificadorAgenteManager;

class HomeApmonController extends AbstractController
{

    private $bread;
    private $userlogin;
    private $dominioManager;
    private $notificador;

    public function __construct(BreadcrumbManager $bread, UserLoginManager $userlogin, DominioManager $dominioManager, NotificadorAgenteManager $notificador)
    {
        $this->bread = $bread;
        $this->userlogin = $userlogin;
        $this->dominioManager = $dominioManager;
        $this->notificador = $notificador;
    }

    /**
     * home del cliente
     * @Route("/apmon/homepage", name="apmon_homepage", requirements={"id": "\d+"})
     * @Route("/apmon/home", name="apmon_home", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     */
    public function homepageAction(Request $request, $redirect = null)
    {
        //redirigir para cambiar clave.
        $usr = $this->userlogin->getUser();
        // die('////<pre>' . nl2br(var_export($usr, true)) . '</pre>////');
        if ($usr->getChangePassword() == true) {
            return $this->redirect($this->generateUrl(
                'user_profile_password_change',
                array(
                    'id' => $usr->getId()
                )
            ));
        }
        $this->bread->clear($request->getRequestUri(), "Inicio");
        //aca armo los menu.
        $menuMapas = $this->getMenuMapas();
        $menuFlota = $this->getMenuFlota();
        $menuInformes = $this->getMenuInformes();
        $menuConfig = $this->getMenuConfig();
        //die('////<pre>' . nl2br(var_export($menuConfig, true)) . '</pre>////');
        $menuMonitor = $this->getMenuMonitor();
        $menuAyuda = $this->getMenuAyuda();
        $menuMante = $this->getMenuMante();
        $menuAgro = $this->getMenuAgro();
        //aca los grabo en la session.
        $session = $request->getSession();
        $session->set('user', $usr->getNombre());
        $session->set('menuMapas', $menuMapas);
        $session->set('menuFlota', $menuFlota);
        $session->set('menuInformes', $menuInformes);
        $session->set('menuConfig', $menuConfig);
        $session->set('menuMonitor', $menuMonitor);
        $session->set('menuAyuda', $menuAyuda);
        $session->set('menuMante', $menuMante);
        $session->set('menuAgro', $menuAgro);

        $session->set('mantpend', 0);
        $session->set('renderMenu', true);

        //grabo el logo y el icono en la sesion        
        $this->setIcono2Sesion($request);

        if (!is_null($redirect)) {
            if ($redirect == 1) {
                return $this->redirect($this->generateUrl('apmon_equipos'));
            } elseif (false) {
            } else {
            }
        } else {
            return $this->render('apmon/Home/homepage.html.twig', array(
                'menuMapas' => $menuMapas,
                'menuFlota' => $menuFlota,
                'menuInformes' => $menuInformes,
                'menuConfig' => $menuConfig,
                'menuMonitor' => $menuMonitor,
                'menuAyuda' => $menuAyuda,
                'menuMante' => $menuMante,
                'menuAgro' => $menuAgro,
                //'url_shell' => $this->userlogin->findHelpShell('TUTOENEX_ALL'),
            ));
        }
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Organizacion');
    }

    private function getMenuMapas()
    {
        $menu = array();
        if ($this->userlogin->isGranted('ROLE_MAPA_FLOTA_VER')) {
            $menu[] = $this->menu('Toda la flota', $this->generateUrl('apmon_equipos'), '');
        }
        if ($this->userlogin->isGranted('ROLE_MAPA_HISTORIAL')) {
            $menu[] = $this->menu('Historial de posiciones', $this->generateUrl('apmon_historial'), '');
        }
        if ($this->userlogin->isGranted('ROLE_MAPA_ULTIMAS_POSICIONES')) {
            $menu[] = $this->menu('Últimas posiciones', $this->generateUrl('apmon_posiciones'), '');
        }
        if ($this->userlogin->isGranted('ROLE_MAPA_CERCANIA_SERVICIO')) {
            $menu[] = $this->menu('Ubicar vehiculo mas cercano', $this->generateUrl('apmon_mapa_cerca_servicio'), '');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_CANBUS_ONLINE')) {
            $menu[] = $this->menu('Seguimiento Canbus', $this->generateUrl('apmon_serviciocanbus_list', array('idOrg' => $this->userlogin->getOrganizacion()->getId())), '');
        }
        return $menu;
    }

    private function getMenuFlota()
    {
        $menu = array();
        if ($this->userlogin->isGranted('ROLE_FLOTA_VER')) {
            $menu[] = $this->menu('Administrar Flota', $this->generateUrl('servicio_list', array('id' => $this->userlogin->getOrganizacion()->getId())), '');
        }

        if (($this->userlogin->isModuloEnabled('MANAGER_COMBUSTIBLE') && $this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER'))) {
            $menu[] = $this->menu('Combustible', $this->generateUrl('fuel_index', array('id' => $this->userlogin->getOrganizacion()->getId())), 'Acceso al menú de administración del consumo de combustible');
        }
        if ($this->userlogin->isGranted('ROLE_REFERENCIA_VER')) {
            $menu[] = $this->menu('Referencias', $this->generateUrl('referencia_list', array('id' => $this->userlogin->getOrganizacion()->getId())), '');
        }
        if ($this->userlogin->isGranted('ROLE_REFERENCIA_GRUPO_VER')) {
            $menu[] = $this->menu('Grupos de Referencias', $this->generateUrl('apmon_grupo_referencia_list'), '');
        }
        if ($this->userlogin->isGranted('ROLE_FLOTA_GRUPO_ADMIN')) {
            $menu[] = $this->menu('Grupos de Servicios', $this->generateUrl('apmon_grupo_servicio_list'), '');
        }
        if ($this->userlogin->isModuloEnabled('CLIENTE_EVENTOS')) {
            //administracion de eventos
            if ($this->userlogin->isGranted('ROLE_EVENTO_ADMIN')) {
                $menu[] = $this->menu('Administrar Eventos', $this->generateUrl('main_evento_list', array('id' => $this->userlogin->getOrganizacion()->getId())), '');
            }
            //ver eventos historicos.
            if ($this->userlogin->isGranted('ROLE_EVENTO_HISTORICO_VER')) {
                $menu[] = $this->menu('Historico de Eventos', $this->generateUrl('app_eventohistorico_list', array('id' => $this->userlogin->getOrganizacion()->getId())), '');
            }
            //panico de historicos.
            //if ($this->userlogin->isGranted('ROLE_APMON_PANICO_HISTORICO')) {
            //    $menu[] = $this->menu('Historico de Pánicos', $this->generateUrl('main_panico_historico', array('id' => $this->userlogin->getOrganizacion()->getId())), '');
            //}
        }
        //  Centro de costos
        if ($this->userlogin->isModuloEnabled('CLIENTE_PRESTACIONES')) {
            if ($this->userlogin->isGranted('ROLE_PRESTACION_ADMIN')) {
                $menu[] = $this->menu('Horometros y Odometros', $this->generateUrl('servicio_list_odometro', array('id' => $this->userlogin->getOrganizacion()->getId())), '');

                $menu[] = $this->menu('Centro de Costos', $this->generateUrl('centrocosto_list', array(
                    'idorg' => $this->userlogin->getOrganizacion()->getId()
                )), '');
            }
        }

        if ($this->userlogin->isModuloEnabled('CLIENTE_CHOFER')) {
            if ($this->userlogin->isGranted('ROLE_CHOFER_VER')) {
                $menu[] = $this->menu('Choferes', $this->generateUrl('apmon_chofer_index', array('idorg' => $this->userlogin->getOrganizacion()->getId())), '');
            }
        }

        if (($this->userlogin->isModuloEnabled('CLIENTE_PROYECTO'))) {
            $menu[] = $this->menu('Gestión de Proyectos', $this->generateUrl('gpm_index', array('id' => $this->userlogin->getOrganizacion()->getId())), 'Acceso al menú de gestión de proyectos');
        }

        return $menu;
    }

    private function menu($label, $url, $title, $imagen = null)
    {
        return array('label' => $label, 'url' => $url, 'title' => $title, 'imagen' => $imagen);
    }

    private function getMenuInformes()
    {
        $menu = array();
        if (($this->userlogin->isModuloEnabled('CLIENTE_INFORMES'))) {
            $menu[] = $this->menu('Central de Informes', $this->generateUrl(
                'informe_home',
                array(
                    'id' => $this->userlogin->getOrganizacion()->getid()
                )
            ), 'Informe de distancias recorridas de la flota y/o servicios.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_DISTANCIAS')) {
            $menu[] = $this->menu('Distancias recorridas', $this->generateUrl(
                'informe_distancias',
                array(
                    'id' => $this->userlogin->getOrganizacion()->getid()
                )
            ), 'Informe de distancias recorridas de la flota y/o servicios.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_VELOCIDADES')) {
            $menu[] = $this->menu('Tiempos y Velocidades', $this->generateUrl(
                'informe_velocidades',
                array(
                    'id' => $this->userlogin->getOrganizacion()->getid()
                )
            ), 'Informe de tiempos y velocidades máximas registradas en los servicios y flota');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_EXCESO_VELOCIDAD')) {
            $menu[] = $this->menu('Exceso de Velocidad', $this->generateUrl('informe_excesosvelocidad'), 'Informe de excesos de velocidades registradas');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_PORTALES')) {
            $menu[] = $this->menu('Control Pórticos de Autopistas', $this->generateUrl('informe_portal'), 'Informe de costos por pase por portales de autopistas.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_PEAJES')) {
            $menu[] = $this->menu('Control Peajes de Rutas', $this->generateUrl('informe_peaje'), 'Informe de costos de peajes de rutas por uso de los vehículos');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_DETENCIONES')) {
            $menu[] = $this->menu('Detenciones', $this->generateUrl('informe_detenciones', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe de detenciones de servicios');
        }
        if (($this->userlogin->isModuloEnabled('MANAGER_COMBUSTIBLE') && $this->userlogin->isGranted('ROLE_COMBUSTIBLE_VER'))) {
            $menu[] = $this->menu('Cargas de Combustible', $this->generateUrl('fuel_informe_carga', array('id' => $this->userlogin->getOrganizacion()->getid())), 'Informe de cargas de combustible registradas');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_CONSOLIDADO')) {
            $menu[] = $this->menu('Consolidado de Detenciones', $this->generateUrl('informe_consolidado', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe consolidado de detenciones de servicios');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_RALENTI')) {
            $menu[] = $this->menu('Detenciones en Ralenti', $this->generateUrl('informe_ralenti'), 'Informe de tiempo y detenciones de vehiculo en ralenti (con motor encendido y sin moverse)');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_HISTORICO')) {
            $menu[] = $this->menu('Historial', $this->generateUrl('informe_historico'), 'Informe de historial de posiciones para expostación a Excel y CSV.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_PASO_REFERENCIA')) {
            $menu[] = $this->menu('Paso por Referencia', $this->generateUrl('informe_paso'), 'Se informa por cuales referencias ha pasado uno o varios servicios.');
        }

        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_RECORRIDO_REFERENCIA')) {
            $menu[] = $this->menu('Recorrido por Referencia', $this->generateUrl('informe_recorrido_referencia'), 'Se informa los recorridos de determinados servicios dentro una referencia.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_REFERENCIA')) {
            $menu[] = $this->menu('Referencia', $this->generateUrl('informe_referencia'), 'Se informa para una determinada referencia que servicios han pasado por ella.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_GRAFICO_SENSOR_TH')) {
            $menu[] = $this->menu('Gráfico de Sensores', $this->generateUrl('informe_grafico_sensor_th'), 'Gráfico de temperatura y humedad para los vehiculos que tienen dichos sensores.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INPUTS')) {
            $menu[] = $this->menu('Activación Entradas Digitales', $this->generateUrl('informe_inputs'), 'Informe que muestra el cambio del estado de sensores de los vehiculos');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_PRUDENCIA')) {
            $menu[] = $this->menu('Prudencia Vial', $this->generateUrl('informe_prudencia'), 'Informe de prudencia vial sobre calles, rutas y autopistas.');
            $menu[] = $this->menu('Indice de responsabilidad Vial', $this->generateUrl('informe_responsabilidad'), 'Indice de responsabilidad vial sobre calles, rutas y autopistas.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_ESTADIA')) {
            $menu[] = $this->menu('Estadía en referencia', $this->generateUrl('informe_estadia'), 'Informa aquellos vehiculos que han permanecido por mas de un determinado tiempo en las referencias.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_CANBUS')) {
            $menu[] = $this->menu('Historial de Canbus', $this->generateUrl('informe_canbus_historico', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe con todas las transmisiones del equipo Canbus');
            /* $menu[] = $this->menu('Canbus Resumido', $this->generateUrl('informe_canbus_resumen', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe resumido de viajes con canbus para los sevicios que lo tienen habilitado.');*/
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_INFRACCION')) {
            $menu[] = $this->menu('Infracciones', $this->generateUrl('informe_infraccion', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe de infracciones tales como aceleración y disminución de velocidad bruscas.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_CHOFER')) {
            $menu[] = $this->menu('Choferes', $this->generateUrl('informe_chofer', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe de choferes, distancias y duración de viajes por Servicio.');
        }
        if ($this->userlogin->isGranted('ROLE_APMON_INFORME_MANEJO')) {
            $menu[] = $this->menu('Manejo', $this->generateUrl('informe_manejo', array(
                'id' => $this->userlogin->getOrganizacion()->getid()
            )), 'Informe de servicios, distancias y duración de viajes por Chofer');
        }

        return $menu;
    }

    private function getMenuMonitor()
    {
        $menu = array();
        $org = ['idOrg' => $this->userlogin->getOrganizacion()->getId()];
        if ($this->userlogin->isGranted('ROLE_ITINERARIO_VER')) {
            $menu[] = $this->menu('Itinerarios', $this->generateUrl('itinerario_list', $org), '');
            if ($this->userlogin->isGranted('ROLE_TEMPLATE_ITINERARIO_VER')) {
                $menu[] = $this->menu('Templates', $this->generateUrl('itinerario_template_list', $org), '');
            }
            $menu[] = $this->menu('Estado de Servicios', $this->generateUrl('servicio_status', $org), '');
            $menu[] = $this->menu('Seguimiento Online', $this->generateUrl('itinerario_mapa', $org), '');
        }
        $subMenu = array();

        if ($this->userlogin->isGranted('ROLE_TRANSPORTE_VER')) {
            $subMenu[] = $this->menu('Transportes', $this->generateUrl('transporte_list', $org), '');
        }
        if ($this->userlogin->isGranted('ROLE_SATELITAL_VER')) {
            $subMenu[] = $this->menu('Satelital', $this->generateUrl('satelital_list', $org), '');
        }

        if ($this->userlogin->isGranted('ROLE_EMPRESA_VER')) {
            $subMenu[] = $this->menu('Clientes', $this->generateUrl('empresa_list', $org), '');
            $subMenu[] = $this->menu('Logisticas', $this->generateUrl('logistica_list', $org), '');
        }
        //$subMenu[] = $this->menu('Logisticas', $this->generateUrl('logistica_list', $org),'');
        //$subMenu[] = $this->menu('Clientes', $this->generateUrl('empresa_list', $org),'');
        $menu[] = ['label' => 'Configuración', 'submenu' => $subMenu];
        // dd($menu);

        return $menu;
    }

    private function getMenuConfig()
    {
        $menu = array();
        if ($this->userlogin->isGranted('ROLE_USUARIO_VER')) {
            $menu[] = $this->menu('Usuarios', $this->generateUrl('apmon_usuario_list'), '');
        }

        if ($this->userlogin->isGranted('ROLE_EQUIPO_VER')) {
            $menu[] = $this->menu('Equipos', $this->generateUrl('apmon_equipo_list'), '');
        }

        if ($this->userlogin->isGranted('ROLE_CHIP_VER')) {
            $menu[] = $this->menu('Chips de GPRS', $this->generateUrl('dist_chip_list'), '');
        }

        if ($this->userlogin->isGranted('ROLE_CONTACTO_VER')) {
            $menu[] = $this->menu('Contactos', $this->generateUrl('main_contacto_list', array(
                'id' => $this->userlogin->getOrganizacion()->getId()
            )), '');
        }


        if ($this->userlogin->isGranted('ROLE_PRESTACION_CONTRATISTA')) {
            $menu[] = $this->menu('Contratistas', $this->generateUrl('apmon_contratista_list', array(
                'idorg' => $this->userlogin->getOrganizacion()->getId()
            )), '');
        }
        if ($this->userlogin->isGranted('ROLE_PRESTACION_OBJETIVO')) {
            $menu[] = $this->menu('Centro de Costos', $this->generateUrl('centrocosto_list', array(
                'idorg' => $this->userlogin->getOrganizacion()->getId()
            )), '');
        }
        if ($this->userlogin->isGranted('ROLE_IBUTTON_VER')) {
            $menu[] = $this->menu('Depósito de Ibutton', $this->generateUrl('apmon_ibutton_list', array('idorg' => $this->userlogin->getOrganizacion()->getId())), '');
        }


        return $menu;
    }

    private function getMenuMante()
    {
        $menu = array();
        if ($this->userlogin->isModuloEnabled('CLIENTE_MANTENIMIENTO')) {
            if ($this->userlogin->isGranted('ROLE_APMON_MANT_ADMIN')) {
                //die('////<pre>' . nl2br(var_export('$menuAgro', true)) . '</pre>////');
                if ($this->userlogin->isGranted('ROLE_PETICION_VER')) {
                    $menu[] = $this->menu('Dashboard', $this->generateUrl('mante_dashboard', array('idOrg' => $this->userlogin->getOrganizacion()->getid())), '');
                    $menu[] = $this->menu('Pedido de Reparación', $this->generateUrl('crm_peticion_main', array('id' => $this->userlogin->getOrganizacion()->getid())), '');
                }
                if ($this->userlogin->isGranted('ROLE_APMON_TAREA_MANT_ADMIN')) {
                    $menu[] = $this->menu('Orden de Trabajo', $this->generateUrl('ordentrabajo_list', array('id' => $this->userlogin->getOrganizacion()->getid())), '');
                    $menu[] = $this->menu('Panel de Estado de Mantenimientos', $this->generateUrl('tarea_mant_list', array('idOrg' => $this->userlogin->getOrganizacion()->getid())), '');
                    $menu[] = $this->menu('Plan de Mantenimientos', $this->generateUrl('plan_list', array('idOrg' => $this->userlogin->getOrganizacion()->getid())), '');
                }

                if ($this->userlogin->isModuloEnabled('MODULO_STOCK')) {
                    $menu[] = $this->menu('Productos / Repuestos / Insumos', $this->generateUrl('producto_list', array(
                        'idOrg' => $this->userlogin->getOrganizacion()->getid()
                    )), '');
                    $menu[] = $this->menu('Depósitos', $this->generateUrl('deposito_list', array(
                        'idorg' => $this->userlogin->getOrganizacion()->getid()
                    )), '');
                    $menu[] = $this->menu('Mecánicos', $this->generateUrl('mecanico_list', array(
                        'idOrg' => $this->userlogin->getOrganizacion()->getid()
                    )), '');
                    $menu[] = $this->menu('Modelo de Vehículos', $this->generateUrl('vehiculomodelo_list', array(
                        'idOrg' => $this->userlogin->getOrganizacion()->getid()
                    )), '');
                    $menu[] = $this->menu('Informe de Producto por Rubro', $this->generateUrl('app_rubro_informe', array(
                        'id' => $this->userlogin->getOrganizacion()->getid()
                    )), 'Reporte de uso de productos agrupado por rubro.');
                    $menu[] = $this->menu('Horas Taller', $this->generateUrl('horataller_list', array(
                        'idorg' => $this->userlogin->getOrganizacion()->getid()
                    )), 'Crear descripciones de horas taller');
                }

                if ($this->userlogin->isGranted('ROLE_APMON_INFORME_MANTENIMIENTO')) {
                    if ($this->userlogin->isGranted('ROLE_TALLER_VER')) {
                        $menu[] = $this->menu('Talleres', $this->generateUrl('taller_list', array('id' => $this->userlogin->getOrganizacion()->getid())), '');
                        $menu[] = $this->menu('Talleres Externos', $this->generateUrl('tallerexterno_list', array('id' => $this->userlogin->getOrganizacion()->getid())), '');
                    }
                    $menu[] = $this->menu('Informe Estado por Servicio', $this->generateUrl('informe_status_mantenimiento', array(
                        'id' => $this->userlogin->getOrganizacion()->getid()
                    )), 'Informe de estado de mantenimiento por Servicio');
                    $menu[] = $this->menu('Informe Tareas por Estado', $this->generateUrl('informe_mantenimiento', array(
                        'id' => $this->userlogin->getOrganizacion()->getid()
                    )), '');

                    //                $menu[] = $this->menu('Historial de Mantenimiento', $this->generateUrl('informe_historial_mant', array(
                    //                            'id' => $user->getOrganizacion()->getid())), '');
                }
            }
        }
        return $menu;
    }

    //
    private function getMenuAgro()
    {
        $menu = array();
        $hashReingenio = null;
        $user = $this->userlogin->getUser();
        if ($this->userlogin->isModuloEnabled('MODULO_AGRO')) {
            if ($this->userlogin->isGranted('ROLE_AGRO_INFORME_REFERENCIA')) {
                $menu[] = $this->menu('Informe Trabajo en Referencia', $this->generateUrl('informe_agro_referencia', array('id' => $this->userlogin->getOrganizacion()->getid())), '');
            }
            if ($this->userlogin->isGranted('ROLE_APMON_INFORME_DISTANCIAS')) {
                $menu[] = $this->menu('Distancias, Tiempos y Velocidades', $this->generateUrl(
                    'informe_disttiempo',
                    array(
                        'id' => $this->userlogin->getOrganizacion()->getid()
                    )
                ), 'Informe de distancias recorridas, tiempos y velocidades de la flota y/o servicios.');
            }

            if ($this->userlogin->isGranted('ROLE_AGRO_ACCESO_REINGENIO')) {                
                $data = $user->getData();
                if ($data != null && array_key_exists('hash_reingenio', $data)) { //si no existe el hash se lo agregamos)                     
                    $url = $this->notificador->getUrlReingenio($data['hash_reingenio']);                   
                    $menu[] = $this->menu('Panel de Zafra', $url, '');
                }
            }
        }
        return $menu;
    }

    /**
     * El menu de ayuda tiene otra estructura del menu.
     * @param type $user
     * @return string
     */
    private function getMenuAyuda()
    {
        return array();
    }

    /**
     * @Template("apmon/Home/informes.html.twig")     
     */
    public function informesAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Informes');
        return array('listmenu' => $this->getMenuInformes($this->userlogin->getUser()));
    }

    /**
     * @Template("apmon/Home/mantenimientos.html.twig")     
     */
    public function mantenimientosAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Mantenimientos');
        return array('listmenu' => $this->getMenuMante($this->userlogin->getUser()));
    }

    /**
     * @Template("apmon/Home/flotas.html.twig")     
     */
    public function flotasAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Flota');
        return array('listmenu' => $this->getMenuFlota($this->userlogin->getUser()));
    }

    /**
     * @Template("apmon/Home/mapas.html.twig")     
     */
    public function mapasAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Mapas');
        return array(
            'listmenu' => $this->getMenuMapas(),
        );
    }

    /**
     * @Template("apmon/Home/config.html.twig")     
     */
    public function configAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Configuración');
        return array(
            'listmenu' => $this->getMenuConfig(),
        );
    }

    /**
     * @Template("apmon/Home/mante.html.twig")     
     */
    public function manteAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'Mantenimiento');
        return array(
            'listmenu' => $this->getMenuMante($this->userlogin->getUser()),
        );
    }

    /**
     * @Template("apmon/Home/agro.html.twig")     
     */
    public function agroAction(Request $request)
    {
        $this->bread->push($request->getRequestUri(), 'AGRO');
        return array(
            'listmenu' => $this->getMenuAgro($this->userlogin->getUser()),
        );
    }

    private function setIcono2Sesion(Request $request)
    {
        $dominio = $this->dominioManager->find($request->getHost());
        if ($dominio) {
            $logo = [
                'nombre' => $dominio->getOrganizacion()->getNombre(),
                'mensaje' => $dominio->getMensaje(),
                'imagen' => 'images/logos/' . $dominio->getPathlogo(),
            ];
            $i = explode('.', $dominio->getPathlogo());
            $icono = 'images/ico/' . $i[0] . '.ico';
        } else {
            $icono = 'security.ico';
        }
        $request->getSession()->set('icono', $icono);
        // dd($logo);
    }
}
