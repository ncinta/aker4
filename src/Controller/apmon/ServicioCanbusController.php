<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\ServicioCanbus;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\ServicioCanbusManager;
use App\Model\app\ServicioManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\UtilsManager;

/**
 * Description of CanbusController
 *
 * @author nicolas
 */
class ServicioCanbusController extends AbstractController
{

    private $organizacionManager;
    private $servicioCanbusManager;
    private $servicioManager;
    private $userloginManager;
    private $breadcrumbManager;
    private $utilsManager;

    function __construct(
        OrganizacionManager $organizacionManager,
        ServicioCanbusManager $servicioCanbusManager,
        ServicioManager $servicioManager,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        UtilsManager $utilsManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->servicioCanbusManager = $servicioCanbusManager;
        $this->servicioManager = $servicioManager;
        $this->userloginManager = $userloginManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->utilsManager = $utilsManager;
    }

    /**
     * @Route("/apmon/canbus/{idOrg}/list", name="apmon_serviciocanbus_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_VER")
     */
    public function listAction(Request $request, $idOrg)
    {

        $servicios = $this->obtenerDatos($request, $idOrg);

        return $this->render('apmon/ServicioCanbus/list.html.twig', array(
            'servicios' => $servicios,
            'idOrg' => $idOrg
        ));
    }

    /**
     * Obtiene todos los servicios que tengan canbus los ingresa a un array y 
     * los envía a listAction para enviarlos a la vista
     */
    public function obtenerDatos(Request $request, $idOrg)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Seguimiento Online Canbus");
        $user = $this->userloginManager->getUser();
        $organizacion = $this->organizacionManager->find($idOrg);

        if ($organizacion != $this->userloginManager->getOrganizacion()) {
            $user = $organizacion->getUsuarioMaster();
        }

        //obtengo los servicios que tienen canbus solamente.
        $servicios = array();
        $servs = $this->servicioManager->findAllByUsuario($user);
        foreach ($servs as $value) {
            if ($value->getCanbus()) {
                $servicios[] = $this->servicioCanbusManager->findByService($value->getId());
            }
        }
        $i = 0;
        $arr_global = array();
        if ($servicios) {
            foreach ($servicios as $servicio) {
                if ($servicio != null && $servicio->getDataFi() != null) {
                    $json = json_decode($servicio->getDataFi());
                    $json = get_object_vars($json);
                    // obtengo odometro y odolitro para luego calcular el rendimiento
                    $odometro = $json['odometroFin'] - $json['odometroInicio'];
                    $odolitro = $json['odolitroFin'] - $json['odolitroInicio'];
                    if ($odometro > 0 && $odolitro > 0) {
                        $rendimiento = ($odometro / $odolitro);
                    } else {
                        $rendimiento = 0;
                    }
                    $arr_global[$i]['idServicio'] = $servicio->getServicio()->getId();
                    $arr_global[$i]['idCanbus'] = $servicio->getId();
                    $arr_global[$i]['nombre'] = $servicio->getServicio()->getNombre();
                    //  $arr_global[$i]['ultFechahora'] = $servicio->getServicio()->getUltfechahora();
                    $arr_global[$i]['fechaFinViaje'] = date_format($servicio->getUltFechaHora(), 'd/m/Y H:i:s');
                    $arr_global[$i]['duracion'] = $this->utilsManager->segundos2tiempo($json['duracionTray']);
                    $arr_global[$i]['distancia'] = $json['distanciaTray'];
                    $arr_global[$i]['tiempoMotorMarcha'] = $this->utilsManager->segundos2tiempo($json['tiempoMotorMarcha']);
                    $arr_global[$i]['tiempoRalenti'] = $this->utilsManager->segundos2tiempo($json['tiempoRalenti']);
                    $arr_global[$i]['tiempoCrucero'] = $this->utilsManager->segundos2tiempo($json['tiempoCrucero']);
                    $arr_global[$i]['consumoMarcha'] = $json['consumoMarcha'];
                    $arr_global[$i]['consumoRalenti'] = $json['consumoRalenti'];
                    $arr_global[$i]['consumoCrucero'] = $json['consumoCrucero'];
                    $arr_global[$i]['rendimiento'] = round($rendimiento, 1);
                    $arr_global[$i]['maxRPM'] = $json['maxRPMTray'];
                    $arr_global[$i]['velMax'] = $json['velMaxTray'];
                    $i++;
                }
            }
        }
        return $arr_global;
    }

    /**
     * @Route("/apmon/canbus/refresh", name="apmon_serviciocanbus_refresh")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_PRESTACION_VER")
     */
    public function refreshAction(Request $request)
    {

        $idOrg = $request->get('idOrg');
        $servicios = $this->obtenerDatos($request, $idOrg);
        $html = $response = $this->renderView('apmon/ServicioCanbus/list_content.html.twig', array(
            'servicios' => $servicios,
            'idOrg' => $idOrg
        ));

        return new Response($html);
    }

    /**
     * @Route("/apmon/canbus/{id}/show", name="apmon_serviciocanbus_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, ServicioCanbus $canbus)
    {

        if (!$canbus) {
            throw $this->createNotFoundException('Canbus no encontrado.');
        }
        $json = json_decode($canbus->getDataFi());
        $json = get_object_vars($json);
        $odometro = $json['odometroFin'] - $json['odometroInicio'];
        $odolitro = $json['odolitroFin'] - $json['odolitroInicio'];
        if ($odometro > 0 && $odolitro > 0) {
            $rendimiento = ($odometro / $odolitro);
        } else {
            $rendimiento = 0;
        }
        $json['duracion'] = $this->utilsManager->segundos2tiempo($json['duracionTray']);
        $json['distancia'] = $json['distanciaTray'];
        $json['tiempoMotorMarcha'] = $this->utilsManager->segundos2tiempo($json['tiempoMotorMarcha']);
        $json['tiempoRalenti'] = $this->utilsManager->segundos2tiempo($json['tiempoRalenti']);
        $json['tiempoCrucero'] = $this->utilsManager->segundos2tiempo($json['tiempoCrucero']);
        $json['rendimiento'] = round($rendimiento, 1);
        $json['ult_fechahora'] = $canbus->getUltFechaHora()->format('d/m/Y H:i:s');
        //die('////<pre>' . nl2br(var_export($json['ult_fechahora'], true)) . '</pre>////');
        $this->breadcrumbManager->push($request->getRequestUri(), $canbus->getServicio()->getNombre());
        $equipo = $canbus->getServicio()->getEquipo();

        $this->render('apmon/ServicioCanbus/show.html.twig', array(
            'canbus' => $canbus,
            'equipo' => $equipo,
            'data' => $json
        ));
    }
}
