<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Categoria;
use App\Form\apmon\CategoriaType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;

/**
 * Implementa el controlador para las categorias de las referencias.
 * 
 * @todo Eliminar el acceso por medio de EntityManager y pasarlo a Manager.
 * @author yesica
 */
class CategoriaController extends AbstractController
{

    private $userloginManager;
    private $breadcrumbManager;

    function __construct(UserLoginManager $userlogin, BreadcrumbManager $breadcrumb)
    {
        $this->userloginManager = $userlogin;
        $this->breadcrumbManager = $breadcrumb;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    private function getRepository()
    {
        return $this->getEntityManager()->getRepository('App\Entity\Categoria');
    }

    private function getSecurityContext()
    {
        return $this->get('security.token_storage');
    }

    /**
     *
     * @Route("/apmon/categoria/list", name="apmon_categoria_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_CATEGORIA_VER")
     */
    public function listAction(Request $request)
    {
        $this->breadcrumbManager->push($request->getRequestUri(), "Listado de Categorias");
        $em = $this->getEntityManager();
        $organizacion = $this->userloginManager->getOrganizacion();
        $categorias = $em->getRepository('App:Categoria')
            ->findAllCategorias($organizacion);

        return $this->render('apmon/Categoria/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'categorias' => $categorias,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_CATEGORIA_AGREGAR')) {
            $menu['Agregar Categoria'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_categoria_new', array(
                    'idorg' => $organizacion->getId()
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/categoria/{id}/show", name="apmon_categoria_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_CATEGORIA_VER") 
     */
    public function showAction(Request $request, Categoria $categoria)
    {

        if (!$categoria) {
            throw $this->createNotFoundException('Código de Categoria no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), $categoria->getNombre());

        $deleteForm = $this->createDeleteForm($categoria->getId());

        return $this->render('apmon/Categoria/show.html.twig', array(
            'menu' => $this->getShowMenu($categoria),
            'categoria' => $categoria,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $categoria 
     * @return array $menu
     */
    private function getShowMenu($categoria)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_CATEGORIA_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_categoria_edit', array(
                    'id' => $categoria->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_REFERENCIA_CATEGORIA_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/apmon/categoria/{idorg}/new", name="apmon_categoria_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_CATEGORIA_AGREGAR") 
     */
    public function newAction(Request $request, $idorg)
    {
        $em = $this->getEntityManager();
        $organizacion = $em->getRepository('App:Organizacion')
            ->findOrganizacionById($idorg);

        $categoria = new Categoria();
        $categoria->setOrganizacion($organizacion);

        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $em->persist($categoria);
            $em->flush();

            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Nueva Categoria");
        return $this->render('apmon/Categoria/new.html.twig', array(
            'categoria' => $categoria,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/categoria/{id}/show", name="apmon_categoria_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_CATEGORIA_EDITAR") 
     */
    public function editAction(Request $request, Categoria $categoria)
    {
        $em = $this->getEntityManager();

        if (!$categoria) {
            throw $this->createNotFoundException('Código de Categoria no encontrado.');
        }
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $em->persist($categoria);
            $em->flush();
            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($categoria->getNombre())));
            return $this->redirect($this->breadcrumbManager->getVolver());
        } else {
            $this->setFlash('error', 'Los datos no son válidos');
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Modificar Categoria");
        return $this->render('apmon/Categoria/edit.html.twig', array(
            'categoria' => $categoria,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/categoria/{id}/delete", name="apmon_categoria_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_REFERENCIA_CATEGORIA_ELIMINAR") 
     */
    public function deleteAction(Request $request, Categoria $categoria)
    {
        $em = $this->getEntityManager();

        if (!$categoria) {
            throw $this->createNotFoundException('Código de Categoria no encontrado.');
        }
        $form = $this->createDeleteForm($categoria->getId());
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $this->breadcrumbManager->pop();
            $em->remove($categoria);
            $em->flush();
        }

        return $this->breadcrumbManager->getVolver();
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }
}
