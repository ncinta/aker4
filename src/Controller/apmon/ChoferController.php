<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\apmon\ChoferType;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\HistoricoIbutton;
use App\Entity\Chofer;
use App\Entity\ChoferServicioHistorico;
use App\Entity\Licencia;
use App\Form\apmon\HistoricoIbuttonType;
use App\Form\apmon\ChoferIbuttonType;
use App\Form\apmon\ChoferServicioFilter;
use App\Form\app\ServicioType;
use App\Form\apmon\LicenciaType;
use App\Form\apmon\LicenciaEditType;
use App\Form\apmon\ChoferApicodeType;
use App\Form\apmon\ChoferTomarServicioType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\ChoferManager;
use App\Model\app\UtilsManager;
use App\Model\app\HistoricoIbuttonManager;
use App\Model\app\IbuttonManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\LicenciaManager;
use App\Model\app\ExcelManager;
use App\Model\app\ServicioManager;
use App\Model\app\BitacoraManager;
use App\Model\app\Router\ChoferRouter;

class ChoferController extends AbstractController
{

    protected $userloginManager;
    protected $breadcrumbManager;
    protected $organizacionManager;
    protected $choferManager;
    protected $utilsManager;
    protected $ibuttonManager;
    protected $histIbuttonMAnager;
    protected $licenciaManager;
    protected $excelManager;
    protected $servicioManager;
    protected $bitacoraManager;
    protected $choferRouter;

    function __construct(
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        OrganizacionManager $organizacionManager,
        ChoferManager $choferManager,
        UtilsManager $utilsManager,
        IbuttonManager $ibuttonManager,
        ExcelManager $excelManager,
        HistoricoIbuttonManager $histIbuttonMAnager,
        LicenciaManager $licenciaManager,
        ServicioManager $servicioManager,
        BitacoraManager $bitacoraManager,
        ChoferRouter $choferRouter
    ) {
        $this->breadcrumbManager = $breadcrumbManager;
        $this->organizacionManager = $organizacionManager;
        $this->userloginManager = $userloginManager;
        $this->choferManager = $choferManager;
        $this->utilsManager = $utilsManager;
        $this->ibuttonManager = $ibuttonManager;
        $this->histIbuttonMAnager = $histIbuttonMAnager;
        $this->licenciaManager = $licenciaManager;
        $this->excelManager = $excelManager;
        $this->servicioManager = $servicioManager;
        $this->bitacoraManager = $bitacoraManager;
        $this->choferRouter = $choferRouter;
    }

    private function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }


    /**
     * @Route("/apmon/chofer/{idorg}/index", name="apmon_chofer_index")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function indexAction(Request $request, $idorg)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Choferes");
        $organizacion = $this->organizacionManager->find($idorg);

        if ($this->organizacionManager->isModuloEnabled($organizacion, 'CLIENTE_CHOFER')) {
            $menu['Choferes'] = array(
                'url' => $this->generateUrl('apmon_chofer_list', array('idorg' => $organizacion->getId())),
                'descripcion' => 'Accede a los choferes registrados del cliente.'
            );
            $menu['Históricos'] = array(
                'url' => $this->generateUrl('apmon_chofer_historico_informe', array('idorg' => $organizacion->getId())),
                'descripcion' => 'Historial de toma y entrega de servicios'
            );

            if ($this->userloginManager->isGranted('ROLE_IBUTTON_VER')) {
                $menu['Depósito de Ibutton'] = array(
                    'url' => $this->generateUrl('apmon_ibutton_list', array('idorg' => $organizacion->getId())),
                    'descripcion' => 'Depósito de ibutton registrado en el cliente.'
                );
            }

            /** INFORMES * */
            if ($this->userloginManager->isGranted('ROLE_APMON_INFORME_CHOFER')) {
                $menu['Informe de choferes'] = array(
                    'url' => $this->generateUrl('informe_chofer', array('id' => $organizacion->getId())),
                    'descripcion' => 'Informe de choferes, distancias y duración de viajes por Servicio.'
                );
            }
            if ($this->userloginManager->isGranted('ROLE_APMON_INFORME_MANEJO')) {
                $menu['Informe de manejo'] = array(
                    'url' => $this->generateUrl('informe_manejo', array('id' => $organizacion->getId())),
                    'descripcion' => 'Informe de servicios, distancias y duración de viajes por Chofer'
                );
            }
            $menu['Informe de Exceso de Velocidad'] = array(
                'url' => $this->generateUrl('informe_excesosvelocidad_chofer', array('id' => $organizacion->getId())),
                'descripcion' => 'Genera un informe de los excesos de velocidad cometido por los choferes dentro de un período de tiempo.'
            );
            $menu['Informe de Ralenti'] = array(
                'url' => $this->generateUrl('informe_ralenti_chofer', array('id' => $organizacion->getId())),
                'descripcion' => 'Genera un informe de los ralentis detectados para los choferes en un período de tiempo. Ralenti es cuando el vehiculo esta con el motor encendido pero no se esta moviendo.'
            );
            $menu['Recepción y entregas por servicio'] = array(
                'url' => $this->generateUrl('informe_choferflota', array('id' => $organizacion->getId())),
                'descripcion' => 'Genera un resumen las ultimas entregas y devoluciones realizadas sobre los servicios'
            );
        }

        return $this->render('apmon/Chofer/index.html.twig', array(
            'panel' => $menu,
        ));
    }

    /**
     * @Route("/apmon/chofer/{idorg}/list", name="apmon_chofer_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function listAction(Request $request, $idorg)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Choferes");
        $organizacion = $this->organizacionManager->find($idorg);
        $choferes = $this->choferManager->findAll($organizacion);

        $chofer = new Chofer();
        $licencia = new Licencia();

        if ($this->userloginManager->isGranted('ROLE_CHOFER_VER')) {
            $menu[1] = array(
                $this->choferRouter->btnNew(),
                $this->choferRouter->btnExport(),
                $this->choferRouter->btnContactos($organizacion),

            );
        }

        return $this->render('apmon/Chofer/list.html.twig', array(
            'menu' => $menu,
            'organizacion' => $organizacion,
            'choferes' => $choferes,
            'formChoferNew' => $this->createForm(ChoferType::class, $chofer)->createView(),
            'formLicenciaNew' => $this->createForm(LicenciaType::class, $licencia)->createView(),
            'formLicenciaEdit' => $this->createForm(LicenciaEditType::class, $licencia)->createView(),
        ));
    }

    /**
     * @Route("/apmon/orico/{idorg}/informe", name="apmon_chofer_historico_informe")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function historicolistAction(Request $request, $idorg)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Histórico");
        $organizacion = $this->organizacionManager->find($idorg);
        // die('////<pre>' . nl2br(var_export(count($organizacion), true)) . '</pre>////');
        $formHistorico = $this->createForm(ChoferServicioFilter::class, null, array(
            'organizacion' => $organizacion,
            'em' => $this->getEntityManager()
        ))->createView();
        $desde = (new \DateTime())->modify('-30 days');
        $hasta = new \DateTime();

        return $this->render('apmon/Chofer/historico.html.twig', array(
            'organizacion' => $organizacion,
            'formHistoricoFilter' => $formHistorico,
            'historico' => $this->choferManager->historicoFilter($organizacion, $desde, $hasta),

        ));
    }


    /**
     * @Route("/apmon/chofer/{id}/show/{tab}", name="apmon_chofer_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER") 
     */
    public function showAction(Request $request, Chofer $chofer, $tab)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $chofer->getNombre());
        $deleteForm = $this->createDeleteForm($chofer->getId());
        $bitacora = array();
        $formHistorico = $this->createForm(ChoferServicioFilter::class, null, array(
            'organizacion' => $chofer->getOrganizacion(),
            'em' => $this->getEntityManager(),
        ))->createView();

        // die('////<pre>' . nl2br(var_export(count($registrosHist), true)) . '</pre>////');
        $registros = $this->bitacoraManager->obtenerByChofer($chofer);
        foreach ($registros as $registro) {
            $bitacora[] = array(
                'registro' => $registro,
                'ejecutor' => $registro->getEjecutor(),
                'descripcion' => $this->bitacoraManager->parse($registro),
            );
        }
        $desde = (new \DateTime())->modify('-30 days');
        $hasta = new \DateTime();

        return $this->render('apmon/Chofer/show.html.twig', array(
            'menu' => $this->getShowMenu($chofer),
            'chofer' => $chofer,
            'bitacora' => $bitacora,
            'tab' => is_null($tab) ? 'main' : $tab,
            'historico' => $this->choferManager->historicoFilter($chofer->getOrganizacion(), $desde, $hasta, $chofer),
            'delete_form' => $deleteForm->createView(),
            'formHistoricoFilter' => $formHistorico,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $chofer 
     * @return array $menu
     */
    private function getShowMenu($chofer)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_CHOFER_EDITAR')) {
            $menu[1] = array(
                $this->choferRouter->btnEdit($chofer),
                $this->choferRouter->btnApicode($chofer)
            );
        }
        if ($this->userloginManager->isGranted('ROLE_CHOFER_ELIMINAR') && is_null($chofer->getIbutton())) {
            $menu[2] = array(
                $this->choferRouter->btnDelete()
            );
        }


        if ($chofer->getIbutton()) {
            if ($this->userloginManager->isGranted('ROLE_CHOFER_DESASIGNAR_IBUTTON')) {
                $menu[3] = array(
                    $this->choferRouter->btnRetirarIbutton($chofer)
                );
            }
        } else {
            if ($this->userloginManager->isGranted('ROLE_CHOFER_ASIGNAR_IBUTTON')) {
                $menu[4] = array($this->choferRouter->btnAsignarIbutton($chofer));
            }
        }
        if ($chofer->getServicio()) {
            $menu[5] = array($this->choferRouter->btnRetirarServicio($chofer));
        } else {
            $menu[5] = array($this->choferRouter->btnAsignarServicio($chofer));
        }
        $menu[6] = [
            $this->choferRouter->btnTomarServicio($chofer),
            $this->choferRouter->btnEntregarServicio($chofer)            
        ];

        return $menu;
    }

    /**
     * @Route("/apmon/chofer/new", name="apmon_chofer_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_AGREGAR") 
     */
    public function newAction(Request $request)
    {
        $id = intval($request->get('id'));
        $organizacion = $this->organizacionManager->find($id);
        if (!$organizacion) {
            throw $this->createNotFoundException('Transporte no encontrado.');
        }
        $tmp = array();

        parse_str($request->get('form'), $tmp);
        // die('////<pre>' . nl2br(var_export($tmp, true)) . '</pre>////');
        if (isset($tmp['chofer'])) {
            $chofer = $this->choferManager->create($organizacion);
            $chofer->setNombre($tmp['chofer']['nombre']);
            $chofer->setCuit($tmp['chofer']['cuit']);
            $chofer->setDocumento($tmp['chofer']['documento']);
            $chofer->setTelefono($tmp['chofer']['telefono']);
            $chofer->setApiCode($chofer->getTelefono() ? $this->utilsManager->getApiCode() : null);
            $data = $this->choferManager->save($chofer);

            $html = $this->renderView('apmon/Chofer/choferes.html.twig', array(
                'choferes' => $organizacion->getChoferes(),


            ));
            return new Response(json_encode(array('html' => $html)), 200);
        }
        return new Response(json_encode(array('status' => 'falla')), 400);
    }

    /**
     * @Route("/apmon/chofer/{id}/edit", name="apmon_chofer_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function editAction(Request $request, Chofer $chofer)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Chofer");
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chip no encontrado.');
        }
        $form = $this->createForm(ChoferType::class, $chofer);

        $telefOld = $chofer->getTelefono();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $telefono = $request->get('chofer')->getTelefono();
            if ($telefono && $telefono !== $telefOld) {
                $chofer->setApiCode($this->utilsManager->getApiCode());
            }

            // die('////<pre>' . nl2br(var_export($telefOld, true)) . '</pre>////');
            $this->breadcrumbManager->pop();
            if ($this->choferManager->save($chofer)) {
                $this->setFlash('success', sprintf('Los datos del chofer han sido actualizados'));
                if ($telefono && $telefono !== $telefOld) {
                    $this->setFlash('success', sprintf('Se generó el apicode ' . strtoupper($chofer->getApiCode())));
                }
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', sprintf('Hubo problemas en la grabación del chofer "%s". Es posible que el Id ya exista.', strtoupper($chofer)));
            }
        }
        return $this->render('apmon/Chofer/edit.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/{id}/delete", name="apmon_chofer_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_ELIMINAR") 
     */
    public function deleteAction(Request $request, Chofer $chofer)
    {
        $form = $this->createDeleteForm($chofer->getId());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->choferManager->delete($chofer->getId());
            $this->setFlash('success', sprintf('Chofer eliminado exitosamente'));
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/apmon/chofer/{id}/asignaributton", name="apmon_chofer_asignar_ibutton",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function asignaributtonAction(Request $request, Chofer $chofer)
    {

        if (!$chofer) {
            throw $this->createNotFoundException('Código de chip no encontrado.');
        }
        $options['ibuttons'] = $this->ibuttonManager->findByEstado(1, $chofer->getOrganizacion());
        $form = $this->createForm(ChoferIbuttonType::class, null, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $ibutton = $this->ibuttonManager->findOne($request->get('chofer_ibutton')['ibutton']);
            $ibutton->setEstado(2); // ESTADO 2 = ASIGNADO
            $ibutt = $this->ibuttonManager->save($ibutton);
            if ($ibutt != null) {
                $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('chofer_ibutton')['fecha'], false));
                $chofer->setFechaAsignacion($fecha);
                $chofer->setIbutton($ibutt);
                if ($this->choferManager->save($chofer)) {

                    //el servicio q se esta creando es un vehiculo
                    $this->setFlash('success', sprintf('El ibutton se ha asignado con éxito'));

                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Asignar Ibutton");
        return $this->render('apmon/Chofer/asignar_ibutton.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/{id}/desasignaributton", name="apmon_chofer_desasignar_ibutton",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function desasignaributtonAction(Request $request, Chofer $chofer)
    {

        if (!$chofer) {
            throw $this->createNotFoundException('Código de chip no encontrado.');
        }
        $form = $this->createForm(HistoricoIbuttonType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($chofer != null) {
                $ibutton = $chofer->getIbutton();
                $fecha = new \DateTime($this->utilsManager->datetime2sqltimestamp($request->get('historico_ibutton')['fecha'], false));
                $historico = new HistoricoIbutton();
                $historico->setIbutton($ibutton);
                $historico->setChofer($chofer);
                $historico->setDesde($chofer->getFechaAsignacion());
                $historico->setHasta($fecha);
                $historico->setMotivo($request->get('historico_ibutton')['motivo']);
                if ($this->histIbuttonMAnager->save($historico)) {
                    $ibutton->setEstado($request->get('historico_ibutton')['accion']);
                    $ibutton = $this->ibuttonManager->save($ibutton);
                    $chofer->setIbutton(null);
                    $chofer = $this->choferManager->save($chofer);
                    $this->breadcrumbManager->pop();
                    //el servicio q se esta creando es un vehiculo
                    $this->setFlash('success', sprintf('El ibutton se ha desasignado con éxito'));

                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Desasignar Ibutton");
        return $this->render('apmon/Chofer/desasignar_ibutton.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/newlicencia", options={"expose"=true}, name="apmon_chofer_newlicencia")
     * @Method({"GET", "POST"})
     */
    public function newlicenciaAction(Request $request)
    {
        $tmp = array();
        $chofer = $this->choferManager->find(intval($request->get('id')));
        parse_str($request->get('form'), $tmp);
        $data = $tmp['licencia'];
        // die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $licencia = $this->choferManager->addLicencia($chofer, $data);
        $choferes = $chofer->getTransporte() ? $chofer->getTransporte()->getChoferes() : $this->choferManager->findAll($chofer->getOrganizacion());
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $licencia->getId(),
            'html' => $this->renderView('apmon/Chofer/choferes.html.twig', array(
                'choferes' => $choferes
            )),
        )));
    }

    /**
     * @Route("/apmon/chofer/editlicencia", options={"expose"=true}, name="apmon_chofer_editlicencia")
     * @Method({"GET", "POST"})
     */
    public function editlicenciaAction(Request $request)
    {
        $tmp = array();
        $licencia = $this->licenciaManager->find(intval($request->get('id')));
        $chofer = $licencia->getChofer();
        parse_str($request->get('form'), $tmp);
        $data = $tmp['licencia_edit'];
        //die('////<pre>' . nl2br(var_export($data, true)) . '</pre>////');
        $edit = $this->licenciaManager->edit($licencia, $data);
        $choferes = $chofer->getTransporte() ? $chofer->getTransporte()->getChoferes() : $this->choferManager->findAll($licencia->getChofer()->getOrganizacion());
        return new Response(json_encode(array(
            'estado' => true,
            'id' => $licencia->getId(),
            'html' => $this->renderView('apmon/Chofer/choferes.html.twig', array(
                'choferes' => $choferes
            )),
        )));
    }

    /**
     * @Route("/apmon/chofer/deletelicencia", options={"expose"=true}, name="apmon_chofer_deletelicencia")
     * @Method({"GET", "POST"})
     */
    public function deletelicenciaAction(Request $request)
    {
        $organizacion = $this->userloginManager->getOrganizacion();
        $chofer = $this->licenciaManager->delete(intval($request->get('id')));

        $choferes = $chofer->getTransporte() ? $chofer->getTransporte()->getChoferes() : $this->choferManager->findAll($organizacion);

        return new Response(json_encode(array(
            'estado' => true,
            'id' => $organizacion->getId(),
            'html' => $this->renderView('apmon/Chofer/choferes.html.twig', array(
                'choferes' => $choferes
            )),
        )));
    }

    /**
     * @Route("/apmon/chofer/exportar", name="apmon_chofer_exportar")
     * @Method({"GET", "POST"})
     */
    public function exportarAction(Request $request)
    {
        //creo el xls
        $xls = $this->excelManager->create("Listado de Choferes");

        $xls->setHeaderInfo(array(
            'C2' => 'Choferes',
        ));

        //se hace la barra principal
        $xls->setBar(4, array(
            'A' => array('title' => 'Chofer', 'width' => 20),
            'B' => array('title' => 'Ibutton', 'width' => 20),
            'C' => array('title' => 'Id. Fiscal', 'width' => 20),
            'D' => array('title' => 'Teléfono', 'width' => 20),
            'E' => array('title' => 'Nro. Licencia', 'width' => 25),
            'F' => array('title' => 'Categoría Licencia', 'width' => 25),
            'G' => array('title' => 'Vencimiento Licencia', 'width' => 25),
        ));
        $i = 5;
        $choferes = $this->choferManager->findAll($this->userloginManager->getOrganizacion());
        foreach ($choferes as $chofer) {
            if (count($chofer->getLicencias()) > 0) {
                foreach ($chofer->getLicencias() as $licencia) {
                    $xls->setRowValues($i, array(
                        'A' => array('value' => $chofer->getNombre()),
                        'B' => array('value' => $chofer->getIbutton() ? $chofer->getIbutton()->getDallas() : ''),
                        'C' => array('value' => $chofer->getCuit()),
                        'D' => array('value' => $chofer->getTelefono()),
                        'E' => array('value' => $licencia->getNumero()),
                        'F' => array('value' => $licencia->getCategoria()),
                        'G' => array('value' => ($licencia->getVencimiento())->format('d/m/Y')),
                    ));
                    $i++;
                }
            } else {
                $xls->setRowValues($i, array(
                    'A' => array('value' => $chofer->getNombre()),
                    'B' => array('value' => $chofer->getIbutton() ? $chofer->getIbutton()->getDallas() : ''),
                    'C' => array('value' => $chofer->getCuit()),
                    'D' => array('value' => $chofer->getTelefono()),
                ));
                $i++;
            }
        }
        //genero el archivo.
        $response = $xls->getResponse();
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=choferes.xls');

        // Si usa una conexión https debe configurar estos dos header para compatibilidad con IE headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        return $response;
    }

    /**
     * @Route("/apmon/chofer/{id}/asignarservicio", name="apmon_chofer_asignar_servicio",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function asignarservicioAction(Request $request, Chofer $chofer)
    {
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chip no encontrado.');
        }
        $servicios =  $this->servicioManager->findAllByOrganizacion($chofer->getOrganizacion());
        $options['servicios'] = array();
        $options['select'] = 1;
        foreach ($servicios as $servicio) {
            if ($servicio->getVehiculo()) {
                $options['servicios'][] = $servicio;
            }
        }
        $form = $this->createForm(ServicioType::class, null, $options);
        if ($request->get('servicio')) {
            $this->breadcrumbManager->pop();
            // die('////<pre>' . nl2br(var_export($request->get('servicio')['servicio'], true)) . '</pre>////');
            $servicio = $this->servicioManager->find(intval($request->get('servicio')['servicio']));
            if ($servicio != null) {
                $chofer->setServicio($servicio);
                if ($this->choferManager->save($chofer)) {
                    $ejecutor = $this->userloginManager->getUser();
                    $bitacora = $this->bitacoraManager->choferAsignarServicio($ejecutor, $chofer, $servicio);
                    $this->setFlash('success', sprintf('El servicio se ha asignado con éxito'));

                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Asignar Servicio");
        return $this->render('apmon/Chofer/asignar_servicio.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/{id}/tomarservicio", name="apmon_chofer_tomar_servicio",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function tomarservicioAction(Request $request, Chofer $chofer)
    {
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chofer no encontrado.');
        }
        $choferServicioHist = new ChoferServicioHistorico();
        $options['servicios'] = $this->servicioManager->findAllByOrganizacion($chofer->getOrganizacion());
        $options['estados'] = $choferServicioHist->getArrayStrEstado();

        $form = $this->createForm(ChoferTomarServicioType::class, null, $options);
        if ($request->get('chofer_servicio')) {
            $this->breadcrumbManager->pop();
            $data = $request->get('chofer_servicio');
            $servicio = $this->servicioManager->find(intval($data['servicio']));
            if ($servicio != null) {
                $chofer->setServicio($servicio);
                if ($this->choferManager->save($chofer)) {
                    $fecha = $data['fecha']  != '' ? $this->utilsManager->datetime2sqltimestamp($data['fecha'], true) : null;
                    $fecha = $fecha != null ? new \DateTime($fecha) : null;
                    $data['fecha'] = $fecha ? $fecha->format('Y/m/d') . 'T' . $fecha->format('H:i:s') : null;
                    $historico = $this->choferManager->addHistorico($chofer, $servicio, 0, $data);
                    if ($historico) {
                        $ejecutor = $this->userloginManager->getUser();
                        $bitacora = $this->bitacoraManager->choferServicio($ejecutor, 0, $historico);
                        $this->setFlash('success', sprintf('El servicio se ha asignado con éxito'));
                    } else {
                        $this->setFlash('error', sprintf('Error al tomar el servicio'));
                    }

                    return $this->redirect($this->breadcrumbManager->getVolver());
                }
            }
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Tomar Servicio");
        return $this->render('apmon/Chofer/tomar_servicio.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/{id}/entregarservicio", name="apmon_chofer_entregar_servicio",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function entregarservicioAction(Request $request, Chofer $chofer)
    {
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chofer no encontrado.');
        }
        $choferServicioHist = new ChoferServicioHistorico();
        $options['servicios'] = $this->servicioManager->findAllByOrganizacion($chofer->getOrganizacion());
        $options['estados'] = $choferServicioHist->getArrayStrEstado();

        $form = $this->createForm(ChoferTomarServicioType::class, null, $options);
        if ($request->get('chofer_servicio')) {
            $this->breadcrumbManager->pop();
            $servicio = $chofer->getServicio();
            $data = $request->get('chofer_servicio');
            //$chofer->setServicio(null);   //no debo retirar el servicio del chofer cuando hace entrega
            //if ($this->choferManager->save($chofer)) {
                $fecha = $data['fecha']  != '' ? $this->utilsManager->datetime2sqltimestamp($data['fecha'], true) : null;
                $fecha = $fecha != null ? new \DateTime($fecha) : null;
                $data['fecha'] = $fecha ? $fecha->format('Y/m/d') . 'T' . $fecha->format('H:i:s') : null;
                $historico = $this->choferManager->addHistorico($chofer, $servicio, 1, $data);
                if ($historico) {
                    $ejecutor = $this->userloginManager->getUser();                    
                    $bitacora = $this->bitacoraManager->choferServicio($ejecutor, 1, $historico);
                    $this->setFlash('success', sprintf('El servicio se ha entregado con éxito'));
                } else {
                    $this->setFlash('error', sprintf('Error al entregar el servicio'));
                }

                return $this->redirect($this->breadcrumbManager->getVolver());
            //}
        }
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Entregar Servicio");
        return $this->render('apmon/Chofer/entregar_servicio.html.twig', array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/chofer/{id}/desasignarservicio", name="apmon_chofer_desasignar_servicio",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_EDITAR") 
     */
    public function desasignarservicioAction(Request $request, Chofer $chofer)
    {
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Desasignar Servicio");
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chofer no encontrado.');
        }
        if ($chofer != null) {
            $servicio = $chofer->getServicio();

            $bitacora = $this->bitacoraManager->choferQuitarServicio($this->userloginManager->getUser(), $chofer, $servicio);

            $chofer->setServicio(null);
            $chofer = $this->choferManager->save($chofer);

            $this->breadcrumbManager->pop();
            //el servicio q se esta creando es un vehiculo
            $this->setFlash('success', sprintf('El servicio se ha desasignado con éxito'));

            return $this->redirect($this->breadcrumbManager->getVolver());
        }
    }

    /**
     * @IsGranted("ROLE_CHOFER_EDITAR")
     * @Route("/apmon/chofer/{id}/apicode", name="apmon_chofer_generarapicode",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     */
    public function generarapicodeAction(Request $request, $id)
    {
        $chofer = $this->choferManager->find($id);
        if (!$chofer) {
            throw $this->createNotFoundException('Código de chofer no encontrado.');
        }

        $form = $this->createForm(ChoferApicodeType::class, $chofer);

        $apicode = $request->get('c');   //recupero el apicode que se envio
        if (is_null($apicode)) {        //debo generar el apicode
            $apicode = $this->utilsManager->getApiCode();
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->breadcrumbManager->pop();

                // se graba el usuario
                $chofer->setApicode($apicode);
                $this->choferManager->save($chofer);

                $this->setFlash('success', sprintf('Los datos de %s han sido actualizados', $chofer));
                return $this->redirect($this->breadcrumbManager->getVolver());
            } else {
                $this->setFlash('error', 'Los datos no son válidos');
            }
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Generar Apicode");
        return $this->render("apmon/Chofer/apicode.html.twig", array(
            'chofer' => $chofer,
            'apicode' => $apicode,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/chofer/historico", name="chofer_historico")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CHOFER_VER")
     */
    public function historicoAction(Request $request)
    {
        $tmp = array();
        $historicos = array();
        $consulta = array();
        parse_str($request->get('formHistChofer'), $tmp);
        $form = $tmp['historico'];
        $user = $this->userloginManager->getUser();
        $chofer = null;
        $servicio = null;
        if ($request->get('chofer')) {
            $id = $request->get('chofer');
            $chofer = $this->choferManager->findById(intval($id));
        }
        if ($request->get('servicio')) {
            $id = $request->get('servicio');
            $servicio = $this->servicioManager->findById(intval($id));
        }
        if ($form) {
            $organizacion = $this->userloginManager->getOrganizacion();
            $desde = $this->utilsManager->datetime2sqltimestamp($form['desde'], false);
            $hasta = $this->utilsManager->datetime2sqltimestamp($form['hasta'], true);
            $servicio = $servicio ? $servicio : $this->servicioManager->find(intval($form['servicio']));
            $chofer = $chofer ? $chofer : $this->choferManager->find(intval($form['chofer']));
            $estado = isset($form['estado']) ? intval($form['estado']) : null;

            //  die('////<pre>' . nl2br(var_export($desde, true)) . '</pre>////');
            $historicos = $this->choferManager->historicoFilter($organizacion, $desde, $hasta, $chofer, $servicio, $estado);
        }


        return new Response(json_encode(array(
            'html' => $this->renderView('apmon/Chofer/content_historico.html.twig', array(
                'historico' => $historicos,
            ))
        )));
    }
}
