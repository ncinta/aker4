<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\apmon\GrupoServicioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\GrupoServicioManager;
use App\Entity\GrupoServicio;
use App\Model\app\ServicioManager;
use App\Model\app\IndiceManager;
use App\Model\app\Router\GrupoServicioRouter;

/**
 * Description of GrupoServicioController
 *
 * @author claudio
 */
class GrupoServicioController extends AbstractController
{

    private $userloginManager;
    private $servicioManager;
    private $grupoServicioManager;
    private $breadcrumbManager;
    private $indice;

    function __construct(
        UserLoginManager $userloginManager,
        ServicioManager $servicioManager,
        GrupoServicioRouter $gsRouter,
        GrupoServicioManager $grupoServicioManager,
        BreadcrumbManager $breadcrumbManager,
        IndiceManager $indiceManager
    ) {
        $this->userloginManager = $userloginManager;
        $this->servicioManager = $servicioManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->gsRouter = $gsRouter;
        $this->indice = $indiceManager;
    }

    /**
     * @Route("/apmon/gruposervicio/list", name="apmon_grupo_servicio_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_VER")
     */
    public function listAction(Request $request)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Listado de Grupos");

        $organizacion = $this->userloginManager->getOrganizacion();
        $gruposerv = $this->grupoServicioManager->findAsociadas($organizacion);
        $indices = array();
        foreach ($gruposerv as $gs) {
            $indices[$gs->getId()] = $this->indice->dataParaBarras($this->indice->getLastInsertGs($gs));
        }
        return $this->render('apmon/GrupoServicio/list.html.twig', array(
            'menu' => $this->getListMenu(),
            'gruposerv' => $gruposerv,
            'indices' => $indices,
            'organizacion' => $organizacion,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu()
    {
        return array(
            1 => array($this->gsRouter->btnNew()),
        );

    }

    /**
     * @Route("/apmon/gruposervicio/{id}/show", name="apmon_grupo_servicio_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_VER") 
     */
    public function showAction(Request $request, GrupoServicio $grupoServ)
    {
        if (!$grupoServ) {
            throw $this->createNotFoundException('Código de Grupo de Servicios no encontrado.');
        }
        $indicesBarraServ = array();
        $this->breadcrumbManager->pushPorto($request->getRequestUri(), $grupoServ->getNombre());

        $deleteForm = $this->createDeleteForm($grupoServ->getId());

        $indiceBarraGS = $this->indice->dataParaBarras($this->indice->getLastInsertGs($grupoServ));
        
      
        foreach ($grupoServ->getServicios() as $servicio) {
            $indicesBarraServ[$servicio->getId()] = $this->indice->dataParaBarras($this->indice->getLastInsertServicio($servicio));
        }
       // dd($indicesBarraServ);

        return $this->render('apmon/GrupoServicio/show.html.twig', array(
            'menu' => $this->getShowMenu($grupoServ),
            'gruposerv' => $grupoServ,
            'indicesBarraGS' => $indiceBarraGS,
            'indicesBarraServ' => $indicesBarraServ,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $grupoServ 
     * @return array $menu
     */
    private function getShowMenu($grupoServ)
    {
        return array(
            1 => array($this->gsRouter->btnEdit($grupoServ)),
            2 => array($this->gsRouter->btnAgrupar($grupoServ)),
            3 => array($this->gsRouter->btnDelete($grupoServ))
        );
    }

    /**
     * @Route("/apmon/gruposervicio/new", name="apmon_grupo_servicio_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_AGREGAR") 
     */
    public function newAction(Request $request)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Nuevo Grupo");
        $organizacion = $this->userloginManager->getOrganizacion();
        $grupoServ = $this->grupoServicioManager->create($organizacion);
        $form = $this->createForm(GrupoServicioType::class, $grupoServ);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->grupoServicioManager->save();
            return $this->redirect($this->generateUrl('apmon_grupo_servicio_show', array(
                'id' => $grupoServ->getId()
            )));
        }

        return $this->render('apmon/GrupoServicio/new.html.twig', array(
            'gruposerv' => $grupoServ,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/apmon/gruposervicio/{id}/edit", name="apmon_grupo_servicio_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_EDITAR") 
     */
    public function editAction(Request $request, GrupoServicio $grupoServ)
    {

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Editar Grupo");
        if (!$grupoServ) {
            throw $this->createNotFoundException('Código de Grupo de Servicios no encontrado.');
        }
        $form = $this->createForm(GrupoServicioType::class, $grupoServ);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->grupoServicioManager->save($grupoServ);

            $this->setFlash('success', sprintf('Los datos de "%s" han sido actualizados', strtoupper($grupoServ->getNombre())));
            return $this->redirect($this->breadcrumbManager->getVolver());
        }
        return $this->render('apmon/GrupoServicio/edit.html.twig', array(
            'gruposerv' => $grupoServ,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/gruposervicio/{id}/delete", name="apmon_grupo_servicio_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_ELIMINAR") 
     */
    public function deleteAction(Request $request, GrupoServicio $grupoServ)
    {

        if (!$grupoServ) {
            throw $this->createNotFoundException('Código de Grupo de Servicios no encontrado.');
        }

        $form = $this->createDeleteForm($grupoServ->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            $this->grupoServicioManager->delete($grupoServ);
            $this->setFlash('success', sprintf('El grupo de servicio fue eliminado'));
        }
        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/apmon/gruposervicio/{id}/agrupar", name="apmon_grupo_servicio_agrupar",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_AGRUPAR") 
     */
    public function agruparAction(Request $request, GrupoServicio $grupo)
    {

        $organizacion = $this->userloginManager->getOrganizacion();
        if (!$grupo || !$organizacion) {
            throw $this->createNotFoundException('Código de Grupo de Servicios no encontrado.');
        }

        $this->breadcrumbManager->pushPorto($request->getRequestUri(), "Agrupar Servicios");

        $disponibles = $this->servicioManager->findAllSinAsignar2Grupo($grupo, $organizacion);
        $asignados = $this->servicioManager->findSoloAsignadosGrupo($grupo);

        return $this->render('apmon/GrupoServicio/agrupar.html.twig', array(
            'grupo' => $grupo,
            'disponibles' => $disponibles,
            'asignados' => $asignados,
            'organizacion' => $organizacion,
        ));
    }

    /**
     * @Route("/apmon/gruposervicio/asignar", name="apmon_grupo_servicio_asignarservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_AGRUPAR") 
     */
    public function asignarServicioAction(Request $request)
    {

        $grupo = $this->grupoServicioManager->find($request->get('idgrp'));
        $idsServicios = unserialize($request->get('servicios'));

        if ($grupo && count($idsServicios) > 0) {
            foreach ($idsServicios as $value) {
                $this->grupoServicioManager->addServicio($value);
            }
        }
        $respuesta = array(
            'disponibles' => $this->renderView('apmon/GrupoServicio/show_disponibles.html.twig', array(
                'disponibles' => $this->servicioManager->findAllSinAsignar2Grupo($grupo)
            )),
            'asignados' => $this->renderView('apmon/GrupoServicio/show_asignados.html.twig', array(
                'asignados' => $this->servicioManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }

    /**
     * @Route("/apmon/gruposervicio/retirar", name="apmon_grupo_servicio_retirarservicio")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_FLOTA_GRUPO_DESAGRUPAR") 
     */
    public function retirarServicioAction(Request $request)
    {
        $grupo = $this->grupoServicioManager->find($request->get('idgrp'));
        $idsServicios = unserialize($request->get('servicios'));
        if ($grupo && count($idsServicios) > 0) {
            foreach ($idsServicios as $value) {
                $this->grupoServicioManager->removeServicio($value);
            }
        }
        //            die('////<pre>'.nl2br(var_export($idsServicios, true)).'</pre>////');

        $respuesta = array(
            'disponibles' => $this->renderView('apmon/GrupoServicio/show_disponibles.html.twig', array(
                'disponibles' => $this->servicioManager->findAllSinAsignar2Grupo($grupo)
            )),
            'asignados' => $this->renderView('apmon/GrupoServicio/show_asignados.html.twig', array(
                'asignados' => $this->servicioManager->findAllByGrupo($grupo)
            )),
        );
        return new Response(json_encode($respuesta));
    }
}
