<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\apmon\ContratistaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Model\app\BreadcrumbManager;
use App\Model\app\UserLoginManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\ContratistaManager;
use App\Entity\Contratista;

class ContratistaController extends AbstractController
{

    protected $organizacionManager;
    protected $contratistaManager;
    protected $userloginManager;
    protected $breadcrumbManager;

    function __construct(
        OrganizacionManager $organizacionManager,
        ContratistaManager $contratistaManager,
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager
    ) {
        $this->organizacionManager = $organizacionManager;
        $this->contratistaManager = $contratistaManager;
        $this->userloginManager = $userloginManager;
        $this->breadcrumbManager = $breadcrumbManager;
    }

    /**
     * @Route("/apmon/contratista/{idorg}/list", name="apmon_contratista_list")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTRATISTA_VER") 
     */
    public function listAction(Request $request, $idorg)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Contratistas");

        $organizacion = $this->organizacionManager->find($idorg);
        $contratistas = $this->contratistaManager->findAllByOrganizacion($idorg);

        return $this->render('apmon/Contratista/list.html.twig', array(
            'menu' => $this->getListMenu($organizacion),
            'contratistas' => $contratistas,
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $organizacion 
     * @return array $menu
     */
    private function getListMenu($organizacion)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_CONTRATISTA_AGREGAR')) {
            $menu['Agregar'] = array(
                'imagen' => 'icon-plus icon-blue',
                'url' => $this->generateUrl('apmon_contratista_new', array(
                    'idorg' => $organizacion->getId()
                ))
            );
        }
        return $menu;
    }

    /**
     * @Route("/apmon/contratista/{id}/show", name="apmon_contratista_show",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTRATISTA_VER") 
     */
    public function showAction(Request $request, Contratista $contratista)
    {

        $deleteForm = $this->createDeleteForm($contratista->getId());
        if (!$contratista) {
            throw $this->createNotFoundException('Código de Contratista no encontrado.');
        }

        $this->breadcrumbManager->push($request->getRequestUri(), $contratista->getNombre());
        return $this->render('apmon/Contratista/show.html.twig', array(
            'menu' => $this->getShowMenu($contratista),
            'contratista' => $contratista,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Devuelve un array con el menu de opciones.
     * @param type $equipo 
     * @return array $menu
     */
    private function getShowMenu($servicio)
    {
        $menu = array();
        if ($this->userloginManager->isGranted('ROLE_CONTRATISTA_EDITAR')) {
            $menu['Editar'] = array(
                'imagen' => 'icon-edit icon-blue',
                'url' => $this->generateUrl('apmon_contratista_edit', array(
                    'id' => $servicio->getId()
                ))
            );
        }
        if ($this->userloginManager->isGranted('ROLE_CONTRATISTA_ELIMINAR')) {
            $menu['Eliminar'] = array(
                'imagen' => 'icon-minus icon-blue',
                'url' => '#modalDelete',
                'extra' => 'data-toggle=modal',
            );
        }

        return $menu;
    }

    /**
     * @Route("/apmon/contratista/{idorg}/new", name="apmon_contratista_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTRATISTA_AGREGAR") 
     */
    public function newAction(Request $request, $idorg)
    {
        $organizacion = $this->organizacionManager->find($idorg);
        if (!$organizacion) {
            throw $this->createNotFoundException('Organización no encontrado.');
        }

        //creo el contratista
        $contratista = $this->contratistaManager->create($organizacion);
        $form = $this->createForm(ContratistaType::class, $contratista);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->contratistaManager->save($contratista)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El contratista se ha creado con éxito'));
                return $this->redirect($this->breadcrumbManager->getVolver());
            }
        }


        $this->breadcrumbManager->push($request->getRequestUri(), "Nuevo Contratista");
        return $this->render('apmon/Contratista/new.html.twig', array(
            'organizacion' => $organizacion,
            'contratista' => $contratista,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/contratista/{id}/edit", name="apmon_contratista_edit",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTRATISTA_EDITAR") 
     */
    public function editAction(Request $request, Contratista $contratista)
    {

        if (!$contratista) {
            throw $this->createNotFoundException('Código de Servicio no encontrado.');
        }
        $form = $this->createForm(ContratistaType::class, $contratista);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->contratistaManager->save($contratista)) {
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('Los datos han sido cambiado con éxito'));
                return $this->breadcrumbManager->getVolver();
            }
        }
        $this->breadcrumbManager->push($request->getRequestUri(), "Editar Servicio");
        return $this->render('apmon/Contratista/edit.html.twig', array(
            'contratista' => $contratista,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/apmon/contratista/{id}/delete", name="apmon_contratista_delete",
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_CONTRATISTA_ELIMINAR") 
     */
    public function deleteAction(Request $request, Contratista $contratista)
    {
        $form = $this->createDeleteForm($contratista->getId());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->breadcrumbManager->pop();
            if ($this->contratistaManager->deleteById($contratista->getId())) {
                $this->setFlash('success', 'Contratista eliminado del sistema.');
            } else {
                $this->setFlash('error', 'Error al eliminar el contratista del sistema.');
            }
        }

        return $this->redirect($this->breadcrumbManager->getVolver());
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class)
            ->getForm();
    }

    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->add($action, $value);
    }

    /**
     * @Route("/apmon/contratista/newmodal", name="apmon_contratista_modal_new")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_OBJETIVO_AGREGAR") 
     */
    public function newmodalAction(Request $request, Contratista $contratista)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Crear Contratista");
        if ($request->getMethod() == 'GET') {
            $idorg = $request->get('idorg');
            $nombre = $request->get('nombre');
            $organizacion = $this->organizacionManager->find($idorg);
            if (!$organizacion) {
                throw $this->createNotFoundException('Organización no encontrado.');
            }

            //creo el contratista
            $contratista = $this->contratistaManager->create($organizacion);
            $this->breadcrumbManager->pop();
            $contratista->setNombre($nombre);
            if ($this->contratistaManager->save($contratista)) {
                $contratistas = $this->contratistaManager->findAllByOrganizacion($organizacion);
                $select_obj = array();
                foreach ($contratistas as $contratista) {
                    $select_obj[$contratista->getId()] = $contratista->getNombre();
                }
                //el servicio q se esta creando es un vehiculo
                $this->setFlash('success', sprintf('El contratista se ha creado con éxito'));
                return new Response(json_encode($select_obj));
            }
        }
    }
}
