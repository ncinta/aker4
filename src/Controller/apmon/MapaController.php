<?php

namespace App\Controller\apmon;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Servicio;
use App\Form\apmon\PosicionesType;
use Symfony\Component\HttpFoundation\Response;
use GMaps\Map;
use GMaps\Geocoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Model\app\UserLoginManager;
use App\Model\app\BreadcrumbManager;
use App\Model\app\GmapsManager;
use App\Model\app\ServicioManager;
use App\Model\fuel\TanqueManager;
use App\Model\app\ReferenciaManager;
use App\Model\app\ReferenciaFormManager;
use App\Model\app\GrupoServicioManager;
use App\Model\app\BackendManager;
use App\Model\app\UtilsManager;
use App\Model\app\GeocoderManager;
use App\Model\app\ChoferManager;
use App\Model\app\GrupoReferenciaManager;
use App\Model\app\OrganizacionManager;
use App\Model\app\Cache\MapaCacheManager;

class MapaController extends AbstractController
{

    const DIAS_LIMITE_VISUALIZACION = 20;   //dspues de los 20 no se procesa nada del servicio

    private $userloginManager;
    private $breadcrumbManager;
    private $gmapsManager;
    private $servicioManager;
    private $tanqueManager;
    private $utilsManager;
    private $referenciaFormManager;
    private $referenciaManager;
    private $grupoServicioManager;
    private $choferManager;
    private $geocoderManager;
    private $backendManager;
    private $organizacionManager;
    private $grupoReferenciaManager;
    private $cacheServicio;

    function __construct(
        UserLoginManager $userloginManager,
        BreadcrumbManager $breadcrumbManager,
        GmapsManager $gmapsManager,
        ServicioManager $servicioManager,
        TanqueManager $tanqueManager,
        UtilsManager $utilsManager,
        ReferenciaFormManager $referenciaFormManager,
        ReferenciaManager $referenciaManager,
        GrupoServicioManager $grupoServicioManager,
        ChoferManager $choferManager,
        GeocoderManager $geocoderManager,
        BackendManager $backendManager,
        OrganizacionManager $organizacionManager,
        GrupoReferenciaManager $grupoReferenciaManager,
        MapaCacheManager $cache
    ) {
        $this->userloginManager = $userloginManager;
        $this->breadcrumbManager = $breadcrumbManager;
        $this->gmapsManager = $gmapsManager;
        $this->servicioManager = $servicioManager;
        $this->tanqueManager = $tanqueManager;
        $this->utilsManager = $utilsManager;
        $this->referenciaFormManager = $referenciaFormManager;
        $this->referenciaManager = $referenciaManager;
        $this->grupoServicioManager = $grupoServicioManager;
        $this->choferManager = $choferManager;
        $this->geocoderManager = $geocoderManager;
        $this->backendManager = $backendManager;
        $this->organizacionManager = $organizacionManager;
        $this->grupoReferenciaManager = $grupoReferenciaManager;
        $this->cacheServicio = $cache;
    }

    /**
     * ver flota
     * @Route("/apmon/equipos", name="apmon_equipos")
     * @Method({"GET", "POST"})
     * @IsGranted("ROLE_MAPA_FLOTA_VER")
     */
    public function equiposAction(Request $request)
    {

        $this->breadcrumbManager->push($request->getRequestUri(), "Flota en mapa");
        $mapa = $this->gmapsManager->createMap(true);

        $serviciosAll = $this->servicioManager->findEnabledByUsuario();
        
        $min_lat = $max_lat = $min_lng = $max_lng = false;
        $status = array();
        $colores = array();
        $servicios = [];
        foreach ($serviciosAll as $servicio) {
            if (!is_null($servicio->getUltLatitud())) {
                $colores['servicio' . $servicio->getColor()] = is_null($servicio->getColor()) ? '#FFA500' : '#' . $servicio->getColor();  //hago un array con los colores.              
                $marker = $this->gmapsManager->addMarkerServicio($mapa, $servicio, true);
                if ($this->isMostrable($servicio)) {
                    $servicios[] = $servicio;
                    
                    $this->cacheServicio->update($servicio);    //guardo el dato en cache

                    if ($min_lat === false) {
                        $min_lat = $max_lat = $servicio->getUltLatitud();
                        $min_lng = $max_lng = $servicio->getUltLongitud();
                    } else {
                        $min_lat = min($min_lat, $servicio->getUltLatitud());
                        $max_lat = max($max_lat, $servicio->getUltLatitud());
                        $min_lng = min($min_lng, $servicio->getUltLongitud());
                        $max_lng = max($max_lng, $servicio->getUltLongitud());
                    }
                } else {
                   // dd($servicio);
                }
            }
        }

        $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
        return $this->render('apmon/Mapa/equipos.html.twig', array(
            'mapa' => $mapa,
            'colores' => $colores,
            'servicios' => $servicios,
            'status' => $this->getStatusServicios($servicios, $this->gmapsManager->getShowReferencias()),
            'default_referencias' => $this->gmapsManager->getShowReferencias() ? '1' : '0',
            'datos' => array(),
            'formcr_referencia' => $this->referenciaFormManager->createReferenciaForm($this->userloginManager->getOrganizacion())->createView(),
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($this->userloginManager->getOrganizacion())->createView(),
            'grupos_servicio' => $this->grupoServicioManager->findAsociadas(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_VERFLOTA'),
        ));
    }

    private function isMostrable($servicio)
    {        
        return !is_null($servicio->getUltLatitud()) && !is_null($servicio->getUltLongitud());
    }    

    /**
     * @Route("/apmon/equiposAJAX", name="apmon_equiposAJAX")
     * @Method({"GET", "POST"})
     */
    public function equiposAJAXAction(Request $request)
    {
        //obetengo todo los visibles.
        $verCercana = $request->get('cercana') == '1';
        $refreshAll = $request->get('all');
        $ver = $request->get('ver');
        $grupos = $request->get('grupos') != null ? $request->get('grupos') : null;

        $servicios = $this->getServicios($grupos, $refreshAll);
        $visibles = array();
        foreach ($servicios as $servicio) {
            if ($this->isMostrable($servicio)) {
                $visibles[$servicio->getId()] = true;
            }
        }

        //obtengo todos y seteo los que son visibles o no
        $data = $html = array();
        $ajax = true;
        foreach ($servicios as $servicio) {
            if ($this->isMostrable($servicio)) {
                $data[] = array(
                    'id_sc' => 'servicio_' . $servicio->getId(),
                    'latitud' => $servicio->getUltLatitud(),
                    'longitud' => $servicio->getUltLongitud(),
                    'texto' => $servicio->getUltVelocidad() . ' km/h',
                    'visible' => isset($visibles[$servicio->getId()]) ? $visibles[$servicio->getId()] : false,
                    'new_icono' => $this->gmapsManager->getDataIconoFlecha($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
                );
                //aca armo el html para cada servicio.
                $status = $this->getStatusServicio($servicio, $this->gmapsManager->getShowReferencias(), $verCercana, $ver == '1', $ajax);
                // dd($status);
                $html[] = array(
                    'id_sc' => 'servicio_' . $servicio->getId(),
                    'data' => trim($this->renderView('apmon/Mapa/equipo_div.html.twig', array(
                        'servicio' => $servicio,
                        'status' => [$servicio->getId() => $status]
                    ))),
                );
              //  $this->cacheServicio->update($servicio);
            }
        }
        return new Response(json_encode(array('data' => $data, 'html' => $html)));
    }

    public function refreshAction(Request $request, $servicios)
    {
        $response = $this->render('apmon/Mapa/equipos_list.html.twig', array(
            'servicios' => $servicios,
            'status' => $this->getStatusServicios($servicios, $this->gmapsManager->getShowReferencias()),
        ));
        return $response;
    }

    /**
     * Devuelve un conjunto de servicios. Si se le pasa un string con los id's de 
     * los grupos que tiene obtener, devuelve todos los servicios de esos grupos.
     * @param string $grps_id
     * @return Servicio $servicios
     */
    public function getServicios($grps_id = null, $refreshAll = null)
    {
        $servicios = array();
        //aca proceso los servicios si vienen la consulta por grupos.
        if (!is_null($grps_id) && $grps_id != '') {
            $grps_id = explode(',', $grps_id);
            foreach ($grps_id as $grp) {
                $grupo = $this->grupoServicioManager->find($grp);
                //die('////<pre>' . nl2br(var_export($grupo->getId(), true)) . '</pre>////');
                //TODO: Esta consulta debe cambiarse para que solo traiga los equipos actualizados y no todos.
                $tmps1 = $this->servicioManager->findSoloAsignadosGrupo($grupo);
                foreach ($tmps1 as $serv) {
                    $servicios[$serv->getId()] = $serv;
                }
            }
        } elseif ($refreshAll) {
            //llamo con 0 para que traiga a todos
            $servicios = $this->servicioManager->findLastUpdated(null);
        } else {
            //obtengo solo los servicios actualizados.                  
            $servicios = $this->servicioManager->findLastUpdated(300);           //300 segundos                         
        }
        
        return $servicios;
    }

    private $referencias = null;

    private function getStatusServicio(
        $servicio,
        $showDefaultCercania = null,
        $verCercana = false,
        $verDireccion = false,
        $ajax
    ) {
        $ultTrama = array();
        $status = array(
            'st_ultreporte' => $this->servicioManager->getStatusUltReporte($servicio),
            'vencido' => $this->isVencido($servicio),
            'direccion' => '',
            'icono' => $this->gmapsManager->getIcono($servicio->getUltDireccion(), $servicio->getUltVelocidad()),
            'contacto' => false,
        );
        if (!$this->isVencido($servicio)) {
            // si tengo que refrescar con direccion.
            if ($verDireccion) {
                $status['direccion'] = $this->geocoderManager->inverso($servicio->getUltLatitud(), $servicio->getUltLongitud());
            }

            //tengo que mostrar las cercanas
            if (is_null($showDefaultCercania) ? $verCercana : ($showDefaultCercania || $verCercana)) {
                if (is_null($this->referencias)) {  //cargo las referencias si no las tengo.
                    $this->referencias = $this->referenciaManager->findAsociadas($this->userloginManager->getUser());
                }
                if (count($this->referencias) > 0) {
                    $cercanas = $this->servicioManager->buscarCercania($servicio, $this->referencias, 1);
                    $i = 0;
                    foreach ($cercanas as $key => $value) {
                        if ($value['adentro']) {
                            $i = $key;
                            break;
                        }
                    }
                    if (!is_null($cercanas)) {
                        $status['cerca']['leyenda'] = $cercanas[$i]['leyenda'];
                        $status['cerca']['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                    }
                }
            }

            //ult_trama
            if (!is_null($servicio->getUltTrama())) {
                $ultTrama = json_decode($servicio->getUltTrama(), true);
                //chofer
                if ($ajax) { //actualiza posicion por mapa
                    $status['chofer'] = $servicio->getChofer() ? $servicio->getChofer() : null; //con ajax necesita el id del servicio en la key
                    if (is_null($status['chofer'])) { //si no tiene chofer asignado que busque por ibutton
                        if (isset($ultTrama['ibutton']) && ($ultTrama['ibutton'] != 'NULL') && ($ultTrama['ibutton'] != 0 || $ultTrama['ibutton'] != null)) {
                            $status['chofer'] = $this->choferManager->findByDallas($ultTrama['ibutton']);
                            if (is_null($status['chofer']) && !is_null($ultTrama['ibutton'])) {
                                $status['chofer'] = ['nombre' => $ultTrama['ibutton']];
                            }
                        }
                    }
                } else { //entra por primera vez
                    $status['chofer'] = $servicio->getChofer() ? $servicio->getChofer() : null;
                    if (is_null($status['chofer'])) { //si no tiene chofer asignado que busque por ibutton
                        if (isset($ultTrama['ibutton']) && ($ultTrama['ibutton'] != 'NULL') && ($ultTrama['ibutton'] != 0 || $ultTrama['ibutton'] != null)) {
                            $status['chofer'] = $this->choferManager->findByDallas($ultTrama['ibutton']);
                            if (is_null($status['chofer']) && !is_null($ultTrama['ibutton'])) {
                                $status['chofer'] = ['nombre' => $ultTrama['ibutton']];
                            }
                        }
                    }
                }
                $status['contacto'] = isset($ultTrama['contacto']) ? $ultTrama['contacto'] : false;

                //se visualiza el estado de los inputs.
                $inputs = array();
                if ($servicio->getEntradasDigitales() === true) {
                    foreach ($servicio->getInputs() as $input) {
                        //$ultTrama['entrada_2'] = rand(1, 2) == 1;   //borrar esta linea, solo test.
                        $valor = isset($ultTrama[$input->getModeloSensor()->getCampoTrama()]) ? $ultTrama[$input->getModeloSensor()->getCampoTrama()] : false;
                        //aca armo la leyenda teneindo en cuenta si hay icono o no.
                        $inputs[] = array(
                            'nombre' => $input->getNombre(),
                            'icono' => !is_null($input->getIcono()) ? ($valor ? $input->getIcono() : str_replace('input_on', 'input_off', $input->getIcono())) : null,
                            'leyenda' => $input->getAbbr() . ': ' . ($valor ? 'ON' : 'OFF')
                        );
                    }
                }
                $status['inputs'] = $inputs;

                //============= cerrojo ==============                
                if ($servicio->getCorteCerrojo() === true) {
                    $status['cerrojo'] = array(
                        'estado_cerrojo' => $this->servicioManager->getStatusCerrojo($servicio, $ultTrama),
                    );
                }

                //========= sensores ==============
                $sensores = array();
                foreach ($servicio->getSensores() as $sensor) {
                    $modelo = $sensor->getModeloSensor();
                    if ($modelo->isSeriado() == true) {   //es seriado, debo buscar el valor en el packlet de sensores.
                        if ($sensor->getUltFechahora() != null) {
                            $fecha = $this->utilsManager->fechaUTC2local($sensor->getUltFechahora());
                            $sensores[$sensor->getAbbr()] = array(
                                'sensor' => $sensor,
                                'inrango' => $this->sensorInRange($sensor, $sensor->getUltValor()),
                                'dato' => round($sensor->getUltValor()),
                                'color' => $this->sensorColor($sensor, $sensor->getUltValor(), $sensor->getUltFechahora()),
                                'title' => sprintf('%s (%d a %d %s) bat: %d%% el %s', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida(), $sensor->getUltBateria(), date_format($fecha, 'd/m/Y H:i:s'))
                            );
                            if (!is_null($sensor->getUltBateria())) {
                                $sensores[$sensor->getAbbr()]['alertabateria'] = $sensor->getUltBateria() < 5;
                            }
                        } else {
                            $sensores[$sensor->getAbbr()] = array(
                                'sensor' => $sensor,
                                'dato' => '---',
                                'title' => sprintf('%s (%d a %d) No se ha informado.', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                            );
                        }
                    } else {  //no es seriado
                        $campo = $sensor->getModeloSensor()->getCampoTrama();
                        if (isset($ultTrama[$campo])) {
                            $sensores[$sensor->getAbbr()] = array(
                                'sensor' => $sensor,
                                'inrango' => $this->sensorInRange($sensor, $ultTrama[$campo]),
                                'color' => $this->sensorColor($sensor, $ultTrama[$campo], null),
                                'dato' => $ultTrama[$campo],
                                'title' => sprintf('%s (%d a %d %s)', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                            );
                        } else {
                            $sensores[$sensor->getAbbr()] = array(
                                'sensor' => $sensor,
                                'dato' => '---',
                                'title' => sprintf('%s (%d a %d) No se ha informado.', $sensor->getNombre(), $sensor->getValorMinimo(), $sensor->getValorMaximo(), $sensor->getMedida())
                            );
                        }
                    }
                    ksort($sensores);
                }
                $status['sensores'] = $sensores;
                $tanque = $this->tanqueManager->getTanque($servicio, $ultTrama);
                //die('////<pre>'.nl2br(var_export($tanque, true)).'</pre>////');
                if ($tanque) {
                    $status['carcontrol']['tanque'] = $tanque;
                }
            }
        }
        return $status;
    }

    private function getStatusServicios($servicios, $showDefaultCercania = null, $verCercana = false, $verDireccion = false)
    {
        $status = array();
        $ajax = false;
        foreach ($servicios as $servicio) {
            $status[$servicio->getId()] = $this->getStatusServicio($servicio, $showDefaultCercania, $verCercana, $verDireccion, $ajax);
        }
        return $status;
    }

    private function sensorInRange($sensor, $valorSensor)
    {
        if (!is_null($sensor->getValorMinimo()) && !is_null($sensor->getValorMaximo())) {
            return $sensor->getValorMinimo() <= $valorSensor && $valorSensor <= $sensor->getValorMaximo();
        } else {
            return true;
        }
    }

    private function sensorColor($sensor, $valorSensor, $fecha)
    {
        $color = '#FFF';
        if ($this->sensorInRange($sensor, $valorSensor)) {
            $color = '#EEEEEE';
        } else {
            $color = '#F3583F';
        }
        //aca controlo la fecha
        if (!is_null($fecha)) {
            $intervalo = date_diff(date_create(), date_create($fecha->format('Y-m-d H:i:s')));
            $intervalo = $intervalo->format('%h');
            //die('////<pre>'.nl2br(var_export($intervalo, true)).'</pre>////');
            if ($intervalo > 2) {
                $color = 'yellow';
            }
        }
        //{{ campo.inrango is defined ? (campo.inrango ?  "#EEEEEE":  "#F13109") : "#F8F677"}}
        return $color;
    }

    function restaHoras($horaIni, $horaFin)
    {
        return $this->utilsManager->segundos2tiempo((strtotime($horaFin) - strtotime($horaIni)));
    }

    /**
     * @Route("/apmon/referenciasAJAX", name="apmon_referencias_viewAJAX")
     * @Method({"GET", "POST"})
     */
    public function referenciasviewAJAXAction(Request $request)
    {
        $visible = !((intval($request->get('visible'))) % 2 == 0);    //determina si es visible o no.

        if (!is_null($request->get('organizacion_id'))) {
            $organizacion = $this->organizacionManager->find($request->get('organizacion_id'));
        } else {
            $organizacion = $this->userloginManager->getOrganizacion();
        }
        $grupos = $this->grupoReferenciaManager->findAllByUsuario();
        $referencias = array();
        if (count($grupos) > 0) {
            foreach ($grupos as $grupo) {
                foreach ($grupo->getReferencias() as $referencia) {
                    $referencias[] = $referencia;
                }
            }
        } else {
            $referencias = $this->referenciaManager->findAsociadas($organizacion);
        }

        if (intval($request->get('visible')) == 1) {   //es el priemro de todos            
            $respuesta = $this->gmapsManager->castReferencias($referencias);
        } else {
            $respuesta = $this->gmapsManager->castVisibilidadReferencias($referencias, $visible);
        }

        return new Response(json_encode($respuesta));
    }

    /**
     *
     * @Route("/apmon/posiciones", name="apmon_posiciones")
     * @Method({"GET", "POST"})
     */
    public function ultimasposicionesAction(Request $request)
    {
        $status = null;
        $servicio = null;
        $id = $request->get('id');  //rescato el id si viene desde otro mapa.
        if (is_null($id)) {
            $options['servicios'] = $this->servicioManager->findEnabledByUsuario();
        } else {
            $options['servicios'] = array($this->servicioManager->find($id));
        }

        $form = $this->createForm(PosicionesType::class, null, $options);
        //creo el mapa.
        $mapa = $this->gmapsManager->createMap(true);

        //tengo que incorporar todas las referencias en el mapa, luego las muestro
        //u oculto por ajax. Pero si o si tiene que estar credas.
        $referencias = $this->referenciaManager->findAsociadas();
        foreach ($referencias as $referencia) {
            //agrego las referencias
            $this->gmapsManager->addMarkerReferencia($mapa, $referencia, $this->gmapsManager->getShowReferencias(), true);
        }

        $consulta = $request->get('posiciones');

        //aca grabo los datos de la session
        $this->get('session')->set('consulta', $consulta);

        $historial = array();
        if ($consulta) {
            //$form->bind($consulta);
            //se formatea el hasta
            $hasta = new \DateTime('', new \DateTimeZone($this->userloginManager->getUser()->getTimezone()));
            $hasta = date_format($hasta, 'Y-m-d H:i:s');
            //se calcula el desde
            $tiempo = $consulta['tiempo'];
            $desde = date('Y-m-d H:i:s', strtotime("$hasta -$tiempo minute"));

            $consulta_historial = $this->backendManager->historial(
                intval($consulta['servicio']),
                $desde,
                $hasta
            );
            $servicio = $this->servicioManager->findById(intval($consulta['servicio']));
            $min_lat = $max_lat = $min_lng = $max_lng = false;
            $puntos_polilinea = array();
            reset($consulta_historial);
            while ($trama = current($consulta_historial)) {
                next($consulta_historial);
                if (isset($trama['posicion'])) {
                    $posicion = $trama['posicion'];
                    $trama = $this->getDatosPosiciones($trama);

                    //agrego los puntos de la polilinea.
                    $puntos_polilinea[] = array($posicion['latitud'], $posicion['longitud']);

                    //calculo marco general.
                    if ($min_lat === false) {
                        $min_lat = $max_lat = $posicion['latitud'];
                        $min_lng = $max_lng = $posicion['longitud'];
                    } else {
                        $min_lat = min($min_lat, $posicion['latitud']);
                        $max_lat = max($max_lat, $posicion['latitud']);
                        $min_lng = min($min_lng, $posicion['longitud']);
                        $max_lng = max($max_lng, $posicion['longitud']);
                    }
                }
                $historial[] = $trama;
            }
            $marker = $this->gmapsManager->addMarkerServicio($mapa, $this->servicioManager->find(intval($consulta['servicio'])), true);
            //hago la reversa para que el mas nuevo quede arriba.
            $puntos_polilinea = array_reverse($puntos_polilinea);
            $historial = array_reverse($historial);
            $this->gmapsManager->addPolyline($mapa, 'servicio_' . $consulta['servicio'], $puntos_polilinea);
            $mapa->fitBounds($min_lat, $min_lng, $max_lat, $max_lng);
            $status = !is_null($consulta['servicio']) ? $this->getStatusServicios($servicio, $this->gmapsManager->getShowReferencias()) : null;
        } else {
            $historial = false;
        }

        //       die('////<pre>' . nl2br(var_export($status, true)) . '</pre>////');

        return $this->render('apmon/Mapa/posiciones.html.twig', array(
            'servicio' => $servicio,
            'status' => $status,
            'form' => $form->createView(),
            'historial' => $historial,
            'mapa' => $mapa,
            'formcr_referencia' => $this->referenciaFormManager->createReferenciaForm($this->userloginManager->getOrganizacion())->createView(),
            'formrr_referencia' => $this->referenciaFormManager->createRedibujoForm($this->userloginManager->getOrganizacion())->createView(),
            'grupos_servicio' => $this->grupoServicioManager->findAsociadas(),
            'url_shell' => $this->userloginManager->findHelpShell('TUTOENEX_ULTPOSICIONES'),
            'default_referencias' => $this->gmapsManager->getShowReferencias() ? '1' : '0',
        ));
    }

    /**
     * @Route("/apmon/posiciones/refresh", name="apmon_posiciones_refresh")
     * @Route("/apmon/posiciones/actualizar", name="apmon_posiciones_actualizar")
     * @Method({"GET", "POST"})
     */
    public function posicionesrefreshAction(Request $request)
    {

        //recupero los datos de la session.
        $consulta = $this->get('session')->get('consulta');
        $servicio = $this->servicioManager->findById(intval($consulta['servicio']));
        //se formatea el hasta
        $hasta = new \DateTime('', new \DateTimeZone($this->userloginManager->getUser()->getTimezone()));
        $hasta = date_format($hasta, 'Y-m-d H:i:s');

        //se calcula el desde
        $tiempo = isset($consulta['tiempo']) ? $consulta['tiempo'] : 5;
        $desde = date('Y-m-d H:i:s', strtotime("$hasta -$tiempo minute"));
        try {
            $consulta_historial = $this->backendManager->historial(intval($consulta['servicio']), $desde, $hasta);
        } catch (\Exception $exc) {
            return null;
        }

        $i = 0;
        $trama = array();  //en puntos tengo todas las tramas sobre el mapa
        $mark = array();
        $puntos_polilinea = $historial = array();
        reset($consulta_historial);
        while ($trama = current($consulta_historial)) {
            next($consulta_historial);
            if (isset($trama['posicion'])) {
                $posicion = $trama['posicion'];

                //agrego los puntos de la trama.
                $trama = $this->getDatosPosiciones($trama);

                //agrego los puntos de la polilinea.
                $puntos_polilinea[] = array($posicion['latitud'], $posicion['longitud']);
            }
            $historial[] = $trama;
        }

        $puntos_polilinea = array_reverse($puntos_polilinea);

        $historial = array_reverse($historial);

        //este mark es la posicion actual del equipo.
        if (isset($posicion)) {
            $mark[] = array(
                'id_sc' => 'servicio_' . $consulta['servicio'],
                'latitud' => $posicion['latitud'],
                'longitud' => $posicion['longitud'],
                'texto' => $posicion['velocidad'] . ' km/h',
                'new_icono' => $this->gmapsManager->getDataIconoFlecha($posicion['direccion'], $posicion['velocidad']),
            );
        }

        $respuesta[] = array(
            'id_sc' => 'servicio_' . $consulta['servicio'],
            'puntos' => $puntos_polilinea,
            'data' => $historial,
            'mark' => $mark,
        );

        return new Response(json_encode($respuesta));
    }

    /**
     *
     * @Route("/apmon/posiciones/renderpanel", name="apmon_posiciones_renderpanel")
     * @Method({"GET", "POST"})
     */
    public function posicionesrenderpanelAction(Request $request)
    {

        $organizacion = $this->userloginManager->getOrganizacion();
        $verdirecciones = $request->get('ver');
        $vercercanas = $request->get('cercana');
        //die('////<pre>'.nl2br(var_export($vercercanas, true)).'</pre>////');
        $respuesta = $request->get('respuesta');
        $historial = array();
        if (isset($respuesta['0']['data'])) {
            $puntos = $respuesta['0']['data'];
            foreach ($puntos as $datos) {
                //obtengo las direcciones
                if (isset($datos['posicion']) && isset($verdirecciones) && $verdirecciones == '1') {
                    $direc = $this->geocoderManager->inverso($datos['posicion']['latitud'], $datos['posicion']['longitud']);
                    $datos['direccion'] = $direc;
                }

                //tengo que mostrar las cercanas
                if ($request->get('cercana') == '1') {
                    if (!isset($referencias)) {  //cargo las referencias si no las tengo.
                        $referencias = $this->referenciaManager->findAsociadas($organizacion);
                    }
                    if (isset($datos['posicion'])) {
                        $cercanas = $this->servicioManager->buscarCercaniaPosicion($datos['posicion']['latitud'], $datos['posicion']['longitud'], $referencias);
                        // die('////<pre>'.nl2br(var_export($cercanas, true)).'</pre>////');
                        $i = 0;
                        foreach ($cercanas as $key => $value) {
                            if ($value['adentro']) {
                                $i = $key;
                                break;
                            }
                        }
                        if (count($cercanas) > 0) {
                            $datos['leyenda'] = $cercanas[$i]['leyenda'];
                            $datos['icono'] = !is_null($cercanas[$i]['icono']) ? $cercanas[$i]['icono'] : null;
                        }
                    }
                }

                $historial[] = $datos;
            }
        }

        $mapa = new Map();
        $response = $this->render('apmon/Mapa/posiciones_list.html.twig', array(
            'historial' => $historial,
            'mapa' => $mapa,
        ));
        return $response;
    }

    private function getDatosPosiciones($trama, $icono_id = null)
    {
        if (isset($trama['posicion'])) {
            $trama['valido'] = true;
            $trama['icono_id'] = $icono_id;
        } else {
            $trama['valido'] = false;
            $trama['icono_id'] = '';
        }
        $trama['distancia'] = isset($trama['distancia']) ? $trama['distancia'] : '0';
        $trama['tiempo'] = isset($trama['tiempo']) ? 'T ' . $this->utilsManager->segundos2tiempo($trama['tiempo']) : '0';

        if (isset($trama['referencia_id'])) {
            $trama['referencia'] = $this->referenciaManager->find($trama['referencia_id']);
        }

        return $trama;
    }

    private function isVencido($servicio)
    {
        if (is_null($servicio->getUltFechahora())) {
            return true;
        }

        $now = new \Datetime();
        $diferencia = $now->diff($servicio->getUltFechahora());
        return ($diferencia->days > self::DIAS_LIMITE_VISUALIZACION);
    }
}
