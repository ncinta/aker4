<?php

namespace App\Entity;

class FiltroPeticion
{

    protected $desde;
    protected $hasta;
    protected $fechatodos;
    protected $prioridad;
    protected $categoria;
    protected $estado;
    protected $asignado_a;
    protected $organizacion;
    protected $servicio;
    protected $tipo;
    protected $centrocosto;

    function getDesde()
    {
        return $this->desde;
    }

    function getHasta()
    {
        return $this->hasta;
    }

    function getAsignado_a()
    {
        return $this->asignado_a;
    }

    function setDesde($desde)
    {
        $this->desde = $desde;
    }

    function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    function setAsignado_a($asignado_a)
    {
        $this->asignado_a = $asignado_a;
    }

    function getPrioridad()
    {
        return $this->prioridad;
    }

    function getCategoria()
    {
        return $this->categoria;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;
    }

    function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getAsignadoA()
    {
        return $this->asignado_a;
    }

    function setAsignadoA($asignado_a)
    {
        $this->asignado_a = $asignado_a;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    function getCentrocosto()
    {
        return $this->centrocosto;
    }

    function setCentrocosto($centrocosto)
    {
        $this->centrocosto = $centrocosto;
    }

    function getFechatodos()
    {
        return $this->fechatodos;
    }

    function setFechatodos($fechatodos)
    {
        $this->fechatodos = $fechatodos;
    }
}
