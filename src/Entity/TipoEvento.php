<?php

//Clase que especifica si un vehiculo es camioneta, auto, retroexcavadora, etc...

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\TipoVehiculo
 *
 * @ORM\Table(name="tipoevento")
 * @ORM\Entity
 */
class TipoEvento
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre Nombre del tipo de evento p/ej: "Panico" "Exceso Velocidad en Referencia"
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $tipo
     *
     * @ORM\Column(name="codename", type="string", length=255, unique=true)
     */
    private $codename;

    /** Variables
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\VariableEvento", inversedBy="tiposEvento", cascade={"persist"})
     * @ORM\JoinTable(name="tipoEventoVariables",
     *      joinColumns={@ORM\JoinColumn(name="tipoevento_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="variableevento_id", referencedColumnName="id")}
     *      )
     */
    protected $variables;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evento", mappedBy="tipoEvento")
     */
    protected $eventos;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoItinerario", mappedBy="tipoEvento")
     */
    protected $eventosItinerario;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getCodename()
    {
        return $this->codename;
    }

    public function setCodename($codename)
    {
        $this->codename = strtoupper($codename);
    }

    public function getVariables()
    {
        return $this->variables;
    }

    public function setVariables($variables)
    {
        $this->variables = $variables;
    }

    public function getEventos()
    {
        return $this->eventos;
    }

    public function setEventos($eventos)
    {
        $this->eventos = $eventos;
    }

    public function __construct()
    {
        $this->variables = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addVariables($variable)
    {
        if (!$this->hasVariable($variable)) {
            //die('////<pre>'.nl2br(var_export($variable->getNombre(), true)).'</pre>////');
            $this->variables->add($variable);
            //$variable->setTiposEvento($this);
        }
    }

    public function removeVariable($variable)
    {
        $this->variables->removeElement($variable);
    }

    public function hasVariable($variable)
    {
        return $this->variables->contains($variable);
    }

    /**
     * Get the value of eventosItinerario
     */ 
    public function getEventosItinerario()
    {
        return $this->eventosItinerario;
    }

    /**
     * Set the value of eventosItinerario
     *
     * @return  self
     */ 
    public function setEventosItinerario($eventosItinerario)
    {
        $this->eventosItinerario = $eventosItinerario;

        return $this;
    }
}
