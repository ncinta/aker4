<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\MotivoReporte
 *
 * @ORM\Table(name="modelomotivoreporte")
 * @ORM\Entity
 */
class ModeloMotivoReporte {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo", inversedBy="motivosReporte")     
     */
    private $modelo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MotivoReporte")
     */
    private $motivo;

    /**
     * @var text $codigo
     *
     * @ORM\Column(name="codigo", type="string", length=255)
     */
    private $codigo;
    
    public function getId() {
        return $this->id;
    }

    public function getModelo() {
        return $this->modelo;
    }

    public function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    public function getMotivo() {
        return $this->motivo;
    }

    public function setMotivo($motivo) {
        $this->motivo = $motivo;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }


}
