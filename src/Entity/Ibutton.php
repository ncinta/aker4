<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of Ibutton
 *
 * @author nicolas
 */

/**
 * App\Entity\Ibutton
 *
 * @ORM\Table(name="ibutton")
 * @ORM\Entity(repositoryClass="App\Repository\IbuttonRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ibutton
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $dallas
     *
     * @ORM\Column(name="dallas", type="string", nullable=true)
     */
    private $dallas;  // codigo que se encuentra en chapa leido desde izq a derecha

    /**
     * @var string $codigo
     *
     * @ORM\Column(name="codigo", type="string", nullable=true)
     */
    private $codigo; // codigo reportado por el equipo es el dallas pero leido alreves.

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var integer $estado
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="ibuttons")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var chofer
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Chofer", mappedBy="ibutton")
     */
    private $chofer;

    /**
     * @var historicoibutton
     *
     * @ORM\OneToMany(targetEntity="App\Entity\HistoricoIbutton", mappedBy="ibutton")
     */
    private $historicoibutton;

    private $tipo;

    function getId()
    {
        return $this->id;
    }

    function getDallas()
    {
        return $this->dallas;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDallas($dallas)
    {
        $this->dallas = strtoupper($dallas);
    }

    function setCodigo($codigo)
    {
        $this->codigo = strtoupper($codigo);
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getChofer()
    {
        return $this->chofer;
    }

    function setChofer(chofer $chofer)
    {
        $this->chofer = $chofer;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion(Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }
    function getHistoricoibutton()
    {
        return $this->historicoibutton;
    }

    function setHistoricoibutton(HistoricoIbutton $historicoibutton)
    {
        $this->historicoibutton = $historicoibutton;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(?int $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }
}
