<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of PuntoCargaTipoCombustible
 *
 * @author nicolas
 */

/**
 * App\Entity\PrecioTipoCombustible
 *
 * @ORM\Table(name="precio_tipo_combustible")
 * @ORM\Entity(repositoryClass="App\Repository\PrecioTipoCombustibleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PrecioTipoCombustible
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="PuntoCarga", mappedBy="puntoCargaTipoCombustible",cascade ={"persist"})
     */
    //  protected $puntoCarga;

    /**
     * @ORM\OneToMany(targetEntity="TipoCombustible", mappedBy="puntoCargaTipoCombustible",cascade ={"persist"})
     */
    //  protected $tipoCombustible;

    /**
     * @var puntoCarga
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\PuntoCarga", inversedBy="precioTipoCombustibles")     
     * @ORM\JoinColumn(name="punto_carga_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $puntoCarga;

    /**
     * @var tipoCombustible
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoCombustible", inversedBy="precioTipoCombustibles")     
     * @ORM\JoinColumn(name="tipo_combustible_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $tipoCombustible;

    /**
     * @var float $precio
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;

    function getId()
    {
        return $this->id;
    }

    function getPuntoCarga()
    {
        return $this->puntoCarga;
    }

    function getTipoCombustible()
    {
        return $this->tipoCombustible;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setPuntoCarga($puntoCarga)
    {
        $this->puntoCarga = $puntoCarga;
    }

    function setTipoCombustible($tipoCombustible)
    {
        $this->tipoCombustible = $tipoCombustible;
    }

    function getPrecio()
    {
        return $this->precio;
    }

    function setPrecio($precio)
    {
        $this->precio = $precio;
    }
}
