<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="modulo")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ModuloRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Modulo
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string")
     */
    protected $nombre;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="codename", type="string", nullable=true)
     */
    protected $codename;

    /**
     * Indica el alcance del módulo. 
     * 0 = no definido. Se usa tanto para dist y clientes.
     * 1 = solo backend. solo lo pueden usar los distribuidores
     * 2 = solo frontend. solo para los clientes.
     * @ORM\Column(name="alcance", type="integer", nullable = true)
     */
    protected $alcance;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aplicacion", inversedBy="modulos")
     * @ORM\JoinColumn(name="aplicacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $aplicacion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Organizacion", mappedBy="modulos")
     */
    private $organizaciones;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", mappedBy="modulos")
     */
    private $usuarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Permiso", mappedBy="modulo", cascade={"persist"})
     */
    protected $permisos;

    public function __construct()
    {
        $this->organizaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->permisos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set aplicacion
     *
     * @param App\Entity\Aplicacion $aplicacion
     */
    public function setAplicacion(\App\Entity\Aplicacion $aplicacion)
    {
        $this->aplicacion = $aplicacion;
    }

    /**
     * Get aplicacion
     *
     * @return App\Entity\Aplicacion 
     */
    public function getAplicacion()
    {
        return $this->aplicacion;
    }

    /**
     * Add organizacion
     *
     * @param App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(\App\Entity\Organizacion $organizacion)
    {
        $this->organizaciones->add($organizaciones);
    }

    /**
     * Get organizaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOrganizaciones()
    {
        return $this->organizaciones;
    }

    public function setOrganizaciones($organizaciones)
    {
        $this->organizaciones = $organizaciones;
    }

    public function getPermisos()
    {
        return $this->permisos;
    }

    public function setPermisos($permisos)
    {
        $this->permisos = $permisos;
    }

    public function addPermiso(\App\Entity\Permiso $permiso)
    {
        $this->permisos->add($permiso);
        return $this;
    }

    public function removePermiso(\App\Entity\Permiso $permiso)
    {
        $this->permisos->removeElement($permiso);
        return $this;
    }

    public function getAlcance()
    {
        return $this->alcance;
    }

    public function setAlcance($alcance)
    {
        return $this->alcance = $alcance;
    }

    public function getCodename()
    {
        return $this->codename;
    }

    public function setCodename($codename)
    {
        $this->codename = $codename;
    }
}
