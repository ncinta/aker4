<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Pais
 *
 * @ORM\Table(name="pais")
 * @ORM\Entity(repositoryClass="App\Repository\PaisRepository")
 */
class Pais
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="time_zone", type="string", nullable=true)
     */
    protected $timeZone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Organizacion", mappedBy="pais")
     */
    protected $organizaciones;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add organizaciones
     *
     * @param App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(\App\Entity\Organizacion $organizaciones)
    {
        $this->organizaciones[] = $organizaciones;
    }

    /**
     * Get organizaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOrganizaciones()
    {
        return $this->organizaciones;
    }

    public function getTimeZone()
    {
        return $this->timeZone;
    }

    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }
}
