<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Taller
 *
 * @ORM\Table(name="taller")
 * @ORM\Entity(repositoryClass="App\Repository\TallerRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Taller
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string $direccion
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var string $rubro
     *
     * @ORM\Column(name="rubro", type="string", length=255, nullable=true)
     */
    private $rubro;

    /**
     * @var string $horario
     *
     * @ORM\Column(name="horario", type="string", length=255, nullable=true)
     */
    private $horario;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $nota
     *
     * @ORM\Column(name="nota", type="string", length=255, nullable=true)
     */
    private $nota;

    /**
     * @var string $propio
     *
     * @ORM\Column(name="propio", type="boolean")
     */
    private $propio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimientoHistorico", mappedBy="taller", cascade={"persist"})
     */
    protected $servicioMantenimientos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="taller", cascade={"persist"})
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="talleres")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TallerSector", mappedBy="taller", cascade={"persist","remove"})
     */
    protected $sectores;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getDireccion()
    {
        return $this->direccion;
    }

    function getRubro()
    {
        return $this->rubro;
    }

    function getHorario()
    {
        return $this->horario;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getNota()
    {
        return $this->nota;
    }

    function getPropio()
    {
        return $this->propio;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    function setRubro($rubro)
    {
        $this->rubro = $rubro;
    }

    function setHorario($horario)
    {
        $this->horario = $horario;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setNota($nota)
    {
        $this->nota = $nota;
    }

    function setPropio($propio)
    {
        $this->propio = $propio;
    }

    function getServicioMantenimientos()
    {
        return $this->servicioMantenimientos;
    }

    function setServicioMantenimientos($servicioMantenimientos)
    {
        $this->servicioMantenimientos = $servicioMantenimientos;
    }

    function getTelefono()
    {
        return $this->telefono;
    }

    function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getSectores()
    {
        return $this->sectores;
    }

    function setSectores($sectores)
    {
        $this->sectores = $sectores;
    }

    public function addSector($sector)
    {
        if (!$this->sectores->contains($sector)) {
            $this->sectores[] = $sector;
            $sector->setTaller($this);
        }
    }

    public function removeSector($sector)
    {
        $this->sectores->removeElement($sector);
    }
}
