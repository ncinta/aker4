<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\ResponsableGpm
 *
 * @ORM\Table(name="responsable_gpm")
 * @ORM\Entity(repositoryClass="App\Repository\ResponsableGpmRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResponsableGpm
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Persona", inversedBy="responsable", cascade={"persist"})
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $persona;

    /**
     * @ORM\OneToMany(targetEntity="ActividadGpm", mappedBy="responsable")
     */
    protected $actividades;

    /**
     * @ORM\OneToMany(targetEntity="Proyecto", mappedBy="responsable")
     */
    protected $proyectos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="responsables")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    function getId()
    {
        return $this->id;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }

    function getPersona()
    {
        return $this->persona;
    }

    function setPersona($persona)
    {
        $this->persona = $persona;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Add ActividadGpm
     *
     * @param App\Entity\ActividadGpm $actividad
     */
    public function addActividad(ActividadGpm $actividad)
    {
        $this->actividades[] = $actividad;
    }

    /**
     * Add ActividadGpm
     *
     * @param App\Entity\Proyecto $proyecto
     */
    public function addProyecto(Proyecto $proyecto)
    {
        $this->proyectos[] = $proyecto;
    }
}
