<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * Description of VehiculoExtra
 *
 * @author nicolas
 */

/**
 * @ORM\Table(name="vehiculo_extra")
 * @ORM\Entity(repositoryClass="App\Repository\VehiculoExtraRepository")
 */
class VehiculoExtra
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="dato", type="string", length=255)
     */
    private $dato;

    /**
     * @ORM\Column(name="nota", type="text", nullable=true)
     */
    private $nota;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true) 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true) 
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehiculo", inversedBy="datosExtra", cascade={"persist"})
     * @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $vehiculo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoDatoExtra", inversedBy="datosExtra", cascade={"persist"})
     * @ORM\JoinColumn(name="tipodatoextra_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $tipoDatoExtra;

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getDato()
    {
        return $this->dato;
    }

    function getNota()
    {
        return $this->nota;
    }

    function setNota($nota)
    {
        $this->nota = $nota;
    }


    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getTipoDatoExtra()
    {
        return $this->tipoDatoExtra;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDato($dato)
    {
        $this->dato = $dato;
    }


    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setTipoDatoExtra($tipoDatoExtra)
    {
        $this->tipoDatoExtra = $tipoDatoExtra;
    }

    function getVehiculo()
    {
        return $this->vehiculo;
    }

    function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;
    }
}
