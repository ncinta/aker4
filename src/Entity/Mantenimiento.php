<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="mantenimiento")
 * @ORM\Entity(repositoryClass="App\Repository\MantenimientoRepository")
 */
class Mantenimiento {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer $intervalo_dias
     *
     * @ORM\Column(name="intervalo_dias", type="integer", nullable=true)
     */
    private $intervaloDias;

    /**
     * @var integer $intervalo_kilometros
     *
     * @ORM\Column(name="intervalo_kilometros", type="integer", nullable=true)
     */
    private $intervaloKilometros;

    /**
     * @var integer $intervalo_horas
     *
     * @ORM\Column(name="intervalo_horas", type="integer", nullable=true)
     */
    private $intervaloHoras;

    /**
     * @var integer $aviso_dias
     *
     * @ORM\Column(name="aviso_dias", type="integer", nullable=true)
     */
    private $avisoDias;

    /**
     * @var integer $aviso_kilometros
     *
     * @ORM\Column(name="aviso_kilometros", type="integer", nullable=true)
     */
    private $avisoKilometros;

    /**
     * @var integer $aviso_horas
     *
     * @ORM\Column(name="aviso_horas", type="integer", nullable=true)
     */
    private $avisoHoras;
    
    /**
     * Debe eliminarse
     * @ORM\Column(name="carmodelo_id", type="integer", nullable=true)
     */
    private $carmodelo_id;

    /**
     * @var boolean $por_horas
     *
     * @ORM\Column(name="por_horas", type="boolean", nullable=true)
     */
    private $porHoras;

    /**
     * @var boolean $por_dias
     *
     * @ORM\Column(name="por_dias", type="boolean", nullable=true)
     */
    private $porDias;

    /**
     * @var boolean $por_kilometros
     *
     * @ORM\Column(name="por_kilometros", type="boolean", nullable=true)
     */
    private $porKilometros;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Organizacion", mappedBy="mantenimientos", cascade={"persist"})
     */
    private $organizaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="mantenimientos_propietario")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimiento", mappedBy="mantenimiento", cascade={"persist","remove"})
     */
    protected $servicioMantenimientos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actividad", mappedBy="mantenimiento", cascade={"persist","remove"})
     */
    protected $actividades;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->nombre;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getIntervaloDias() {
        return $this->intervaloDias;
    }

    public function setIntervaloDias($intervaloDias) {
        $this->intervaloDias = $intervaloDias;
    }

    public function getIntervaloKilometros() {
        return $this->intervaloKilometros;
    }

    public function setIntervaloKilometros($intervaloKilometros) {
        $this->intervaloKilometros = $intervaloKilometros;
    }

    public function getIntervaloHoras() {
        return $this->intervaloHoras;
    }

    public function setIntervaloHoras($intervaloHoras) {
        $this->intervaloHoras = $intervaloHoras;
    }

    public function getAvisoDias() {
        return $this->avisoDias;
    }

    public function setAvisoDias($avisoDias) {
        $this->avisoDias = $avisoDias;
    }

    public function getAvisoKilometros() {
        return $this->avisoKilometros;
    }

    public function setAvisoKilometros($avisoKilometros) {
        $this->avisoKilometros = $avisoKilometros;
    }

    public function getAvisoHoras() {
        return $this->avisoHoras;
    }

    public function setAvisoHoras($avisoHoras) {
        $this->avisoHoras = $avisoHoras;
    }

    public function getOrganizaciones() {
        return $this->organizaciones;
    }

    public function setOrganizaciones($organizaciones) {
        $this->organizaciones = $organizaciones;
    }

    public function getPropietario() {
        return $this->propietario;
    }

    public function setPropietario($propietario) {
        $this->propietario = $propietario;
    }

    public function getServicioMantenimientos() {
        return $this->servicioMantenimientos;
    }

    public function setServicioMantenimientos($servicioMantenimientos) {
        $this->servicioMantenimientos = $servicioMantenimientos;
    }

    /**
     * Add organizaciones
     *
     * @param App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(\App\Entity\Organizacion $organizaciones) {
        $this->organizaciones[] = $organizaciones;
    }

    public function getPorHoras() {
        return $this->porHoras;
    }

    public function setPorHoras($porHoras) {
        $this->porHoras = $porHoras;
    }

    public function getPorDias() {
        return $this->porDias;
    }

    public function setPorDias($porDias) {
        $this->porDias = $porDias;
    }

    public function getPorKilometros() {
        return $this->porKilometros;
    }

    public function setPorKilometros($porKilometros) {
        $this->porKilometros = $porKilometros;
    }

    function getVars() {
        return get_object_vars($this);
    }

    public function data2array() {
        $d['id'] = $this->id;
        return $d;
    }
    
    function getActividades() {
        return $this->actividades;
    }

    function setActividades($actividades) {
        $this->actividades = $actividades;
    }



}
