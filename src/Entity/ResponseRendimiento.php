<?php

namespace App\Entity;

use BeSimple\SoapBundle\ServiceDefinition\Annotation as Soap;

class ResponseRendimiento
{

    /**
     * @Soap\ComplexType("int")
     */
    public $code;

    /**
     * @Soap\ComplexType("string")
     */
    public $message;

    /**
     * @Soap\ComplexType("dateTime", nillable = true)
     */
    public $fecha_inicio;

    /**
     * @Soap\ComplexType("dateTime", nillable = true)
     */
    public $fecha_fin;

    /**
     * @Soap\ComplexType("string", nillable = true)
     */
    public $codigo_autorizacion_inicio;

    /**
     * @Soap\ComplexType("string", nillable = true)
     */
    public $codigo_autorizacion_fin;

    /**
     * @Soap\ComplexType("string", nillable = true)
     */
    public $strfecha;

    /**
     * Se envia la patente.
     * @Soap\ComplexType("string", nillable = true)
     */
    public $camion_id;

    /**
     * @Soap\ComplexType("int", nillable = true)
     */
    public $distancia_recorrida;

    /**
     * @Soap\ComplexType("float", nillable = true)
     */
    public $rendimiento_teorico;

    /**
     * @Soap\ComplexType("float", nillable = true)
     */
    public $rendimiento_real;
}
