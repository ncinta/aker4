<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * App\Entity\ServicioCanbus
 *
 * @ORM\Table(name="serviciocanbus")
 * @ORM\Entity(repositoryClass="App\Repository\ServicioCanbusRepository")
 */
class ServicioCanbus
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="registrocanbus")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $servicio;

    /**
     * @ORM\Column(name="data_fi", type="text", nullable=true)
     */
    private $dataFi;

    /**
     * @ORM\Column(name="data_fa", type="text", nullable=true)
     */
    private $dataFa;

    /**
     * @ORM\Column(name="data_fb", type="text", nullable=true)
     */
    private $dataFb;

    /**
     * @ORM\Column(name="data_fc", type="text", nullable=true)
     */
    private $dataFc;

    /**
     * @var datetime $ultFechaHora
     *
     * @ORM\Column(name="ult_fechahora", type="datetime", nullable=true)
     */
    private $ultFechaHora;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function getDataFi()
    {
        return $this->dataFi;
    }

    function getUltFechaHora()
    {
        return $this->ultFechaHora;
    }

    function setDataFi($dataFi)
    {
        $this->dataFi = $dataFi;
    }

    function setUltFechaHora($ultFechaHora)
    {
        $this->ultFechaHora = $ultFechaHora;
    }

    function getDataFa()
    {
        return $this->dataFa;
    }

    function getDataFb()
    {
        return $this->dataFb;
    }

    function getDataFc()
    {
        return $this->dataFc;
    }

    function setDataFa($dataFa)
    {
        $this->dataFa = $dataFa;
    }

    function setDataFb($dataFb)
    {
        $this->dataFb = $dataFb;
    }

    function setDataFc($dataFc)
    {
        $this->dataFc = $dataFc;
    }


    function getVars()
    {
        return get_object_vars($this);
    }
}
