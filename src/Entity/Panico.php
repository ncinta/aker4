<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;

/**
 * App\Entity\Servicio
 *
 * @ORM\Table(name="panico")
 * @ORM\Entity(repositoryClass="App\Repository\PanicoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Panico
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var time $fechaPanico
     *
     * @ORM\Column(name="fecha_panico", type="datetime", nullable=true)
     */
    private $fecha_panico;

    /**
     * @var time $fechaVista
     *
     * @ORM\Column(name="fecha_vista", type="datetime", nullable=true)
     */
    private $fecha_vista;

    /**
     * @ORM\Column(name="mensaje", type="string", nullable=true)
     */
    private $mensaje;

    /**
     * @ORM\Column(name="respuesta", type="text", nullable=true)
     */
    private $respuesta;

    /**
     * @var float $ultLatitud
     *
     * @ORM\Column(name="ult_latitud", type="float", nullable=true)
     */
    private $ultLatitud;

    /**
     * @var float $ultLongitud
     *
     * @ORM\Column(name="ult_longitud", type="float", nullable=true)
     */
    private $ultLongitud;

    /**
     * @ORM\Column(name="domicilio", type="string", nullable=true)
     */
    private $domicilio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="panicos")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $servicio;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="panicos")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true))
     */
    private $ejecutor;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function getFechaPanico()
    {
        //$date = new \DateTime($this->fecha_panico, new \DateTimeZone('America/Buenos_Aires'));
        //        die('////<pre>'.nl2br(var_export($this->fecha_panico, true)).'</pre>////');
        //        $date = $this->fecha_panico->setTimezone(new \DateTimeZone('America/Buenos_Aires'));
        //        return $date;
        return $this->fecha_panico;
    }

    public function setFechaPanico($fecha_panico)
    {
        $this->fecha_panico = $fecha_panico;
    }

    public function getFechaVista()
    {
        return $this->fecha_vista;
    }

    public function setFechaVista($fecha_vista)
    {
        $this->fecha_vista = $fecha_vista;
    }

    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    public function getRespuesta()
    {
        return $this->respuesta;
    }

    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
    }

    public function getUltLatitud()
    {
        return $this->ultLatitud;
    }

    public function setUltLatitud($ultLatitud)
    {
        $this->ultLatitud = $ultLatitud;
    }

    public function getUltLongitud()
    {
        return $this->ultLongitud;
    }

    public function setUltLongitud($ultLongitud)
    {
        $this->ultLongitud = $ultLongitud;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function getDomicilio()
    {
        return $this->domicilio;
    }

    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;
    }
}
