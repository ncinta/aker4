<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Prestadora
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Prestadora
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chip", mappedBy="prestadora")
     */
    protected $chips;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    public function __construct()
    {
        $this->chips = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add chips
     *
     * @param App\Entity\Chip $chips
     */
    public function addChip(\App\Entity\Chip $chips)
    {
        $this->chips[] = $chips;
    }

    /**
     * Get chips
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChips()
    {
        return $this->chips;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
