<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\OneToOne as OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\GrupoReferencia
 *
 * @ORM\Table(name="gruporeferencia")
 * @ORM\Entity(repositoryClass="App\Repository\GrupoReferenciaRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class GrupoReferencia
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Referencia", inversedBy="grupos")
     * @ORM\JoinTable(name="referenciaGrupoRef",
     *      joinColumns={@ORM\JoinColumn(name="grupo_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="referencia_id", referencedColumnName="id")}
     *      )
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    private $referencias;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Organizacion", mappedBy="gruporeferencias", cascade={"persist"})
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    private $organizaciones;

    /**
     * El propietario esta mapeado en la BD como organizacion_id
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="gruporeferencias_propietario")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $propietario;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", mappedBy="gruposReferencias")
     */
    private $usuarios;

    /**
     * @OneToOne(targetEntity="App\Entity\Itinerario", mappedBy="grupoReferencia")
     */
    private $itinerario;

    public function __construct()
    {
        $this->organizaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referencias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function setReferencias($referencias)
    {
        $this->referencias = $referencias;
    }

    /**
     * Add referencias
     *
     * @param App\Entity\Referencia $referencia
     */
    public function addReferencia($referencia)
    {
        if (!$this->referencias->contains($referencia)) {
            //   die('////<pre>' . nl2br(var_export(!$this->referencias->contains($referencia), true)) . '</pre>////');
            $this->referencias[] = $referencia;
        }
    }

    public function removeReferencia($referencia)
    {
        $this->referencias->removeElement($referencia);
        return $this->referencias;
    }
    /**
     * Get referencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }

    /**
     * Add organizaciones
     *
     * @param App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(\App\Entity\Organizacion $organizaciones)
    {
        $this->organizaciones[] = $organizaciones;
    }

    /**
     * Get organizaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOrganizaciones()
    {
        return $this->organizaciones;
    }

    /**
     * Set propietario
     *
     * @param App\Entity\Organizacion $propietario
     */
    public function setPropietario(\App\Entity\Organizacion $propietario)
    {
        $this->propietario = $propietario;
    }

    /**
     * Get propietario
     *
     * @return App\Entity\Organizacion 
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    function getUsuarios()
    {
        return $this->usuarios;
    }

    function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;
    }

    public function __toString()
    {
        return $this->nombre != null ? $this->nombre : '---';
    }

    /**
     * Get the value of itinerario
     */
    public function getItinerario()
    {
        return $this->itinerario;
    }

    /**
     * Set the value of itinerario
     *
     * @return  self
     */
    public function setItinerario($itinerario)
    {
        $this->itinerario = $itinerario;

        return $this;
    }

    /**
     *
     * @param App\Entity\Referencia $referencia
     */
    public function contieneReferencia($referencia)
    {
        return $this->referencias->contains($referencia);
            
    }
}
