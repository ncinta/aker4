<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Usuario as Usuario;
use App\Entity\Logistica as Logistica;
use App\Entity\Transporte as Transporte;

//use App\Entity\Empresa as Empresa;
//use App\Entity\IndiceMantenimiento as IndiceMantenimiento;

/**
 * App\Entity\Organizacion
 *
 * @ORM\Table(name="organizacion")
 * @ORM\Entity(repositoryClass="App\Repository\OrganizacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Organizacion
{

    private $tipoStrOrganizacion = array(
        0 => 'indefinido',
        1 => 'distribuidor',
        2 => 'cliente'
    );

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="organizacion_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=45)
     */
    private $nombre;

    /**
     * @var string $direccion
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var string $proveedor_mapas
     *
     * @ORM\Column(name="proveedor_mapas", type="string", length=255, nullable=true)
     */
    private $proveedor_mapas;

    /**
     * @var integer $limite_historial
     *
     * @ORM\Column(name="limite_historial", type="integer", nullable=true)
     */
    private $limite_historial;

    /**
     * @var string $telefono_oficina
     *
     * @ORM\Column(name="telefono_oficina", type="string", nullable=true)
     */
    private $telefono_oficina;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string $web_site
     *
     * @ORM\Column(name="web_site", type="string", nullable=true)
     */
    private $web_site;

    /**
     * @var string $web_site
     *
     * @ORM\Column(name="dato_fiscal", type="string", nullable=true)
     */
    private $dato_fiscal;

    /**
     * @var float $latitud
     *
     * @ORM\Column(name="latitud", type="float", nullable=true)
     */
    private $latitud;

    /**
     * @var float $longitud
     *
     * @ORM\Column(name="longitud", type="float", nullable=true)
     */
    private $longitud;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Usuario", cascade={"persist"})
     * @ORM\JoinColumn(name="usuario_master_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario_master;

    /**
     * @ORM\Column(name="tipo_organizacion", type="integer", nullable=true)
     */
    protected $tipo_organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pais", inversedBy="organizaciones")
     * @ORM\JoinColumn(name="pais_id", referencedColumnName="id")
     */
    protected $pais;

    /**
     * @ORM\Column(name="time_zone", type="string", nullable=true)
     */
    protected $timeZone;

    /**
     * @ORM\Column(name="api_key", type="string", nullable=true)
     */
    protected $apiKey;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Organizacion", mappedBy="organizacion_padre")
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    protected $organizaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rubro", mappedBy="organizacion")
     * @ORM\OrderBy({"codigo" = "ASC"})
     */
    protected $rubros;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="organizaciones")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion_padre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Usuario", mappedBy="organizacion");
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    protected $usuarios;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Modulo", inversedBy="organizaciones", cascade={"persist"})
     * @ORM\JoinTable(name="ModuloOrganizacion",
     *   joinColumns={@ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="modulo_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    protected $modulos;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ConfiguracionDistribuidor", cascade={"persist"})
     * @ORM\JoinColumn(name="configuracionDistribuidor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $configuracionDistribuidor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dominio", mappedBy="organizacion", cascade={"persist"})
     */
    protected $dominios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equipo", mappedBy="organizacion")
     */
    protected $equiposServicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equipo", mappedBy="propietario")
     */
    protected $equiposPropios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chip", mappedBy="propietario")
     */
    protected $chips_propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chip", mappedBy="organizacion")
     */
    protected $chips;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GrupoServicio", mappedBy="organizacion")
     */
    protected $gruposServicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chofer", mappedBy="organizacion")
     */
    protected $choferes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mecanico", mappedBy="organizacion")
     */
    protected $mecanicos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CheckCargaCombustible", mappedBy="organizacion")
     */
    protected $checkCargasCombustible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Servicio", mappedBy="organizacion")
     */
    protected $servicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="organizacion")
     */
    protected $ordenesTrabajo;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Categoria", mappedBy="organizacion")
     */
    protected $categorias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referencia", mappedBy="propietario")
     */
    protected $referencias_propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GrupoReferencia", mappedBy="propietario")
     */
    protected $gruporeferencias_propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ibutton", mappedBy="organizacion")
     */
    protected $ibuttons;


    /**
     * @ORM\OneToMany(targetEntity="PuntoCarga", mappedBy="organizacion")
     */
    protected $puntocargas;

    /**
     * @ORM\OneToMany(targetEntity="Proyecto", mappedBy="organizacion")
     */
    protected $proyectos;

    /**
     * @ORM\OneToMany(targetEntity="Cliente", mappedBy="organizacion")
     */
    protected $clientes;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ConfigGpm", mappedBy="organizacion")
     */
    protected $configGpm;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MotivoActividadGpm", mappedBy="organizacion")
     */
    protected $motivosgpm;

    public function getGrupoReferenciasPropietario()
    {
        $this->gruporeferencias_propietario;
    }

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Referencia", inversedBy="organizaciones")
     * @ORM\JoinTable(name="organizacionReferencia",
     *      joinColumns={@ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="referencia_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $referencias;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\ReferenciaGpm", inversedBy="organizaciones",cascade={"persist"})
     * @ORM\JoinTable(name="organizacionReferenciaGpm",
     *      joinColumns={@ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="referenciagpm_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $referenciasGpm;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\GrupoReferencia", inversedBy="organizaciones")
     * @ORM\JoinTable(name="organizacionGruporeferencias",
     *      joinColumns={@ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="gruporeferencia_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $gruporeferencias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bitacora", mappedBy="organizacion",cascade={"persist","remove"});
     */
    protected $bitacora;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacto", mappedBy="organizacion")
     */
    protected $contactos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evento", mappedBy="organizacion")
     */
    protected $eventos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="organizacion")
     */
    protected $peticiones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Empresa", mappedBy="organizacion")
     */
    protected $empresas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Satelital", mappedBy="organizacion")
     */
    protected $satelitales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Itinerario", mappedBy="organizacion")
     */
    protected $itinerarios;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Mantenimiento", inversedBy="organizaciones")
     * @ORM\JoinTable(name="organizacionMantenimiento",
     *      joinColumns={@ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="mantenimiento_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $mantenimientos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mantenimiento", mappedBy="propietario")
     */
    protected $mantenimientos_propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contratista", mappedBy="organizacion")
     */
    protected $contratistas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CentroCosto", mappedBy="organizacion")
     */
    protected $centrosCosto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestacion", mappedBy="organizacion")
     */
    protected $prestaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Deposito", mappedBy="organizacion")
     */
    protected $depositos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="organizacion")
     */
    protected $productos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Taller", mappedBy="organizacion")
     */
    protected $talleres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TallerSector", mappedBy="organizacion")
     */
    protected $sectoresTaller;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TallerExterno", mappedBy="organizacion")
     */
    protected $talleresExternos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HoraTaller", mappedBy="organizacion")
     */
    protected $horasTaller;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoModelo", mappedBy="organizacion")
     */
    protected $vehiculoModelos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResponsableGpm", mappedBy="organizacion")
     */
    protected $responsables;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificacionChofer", mappedBy="organizacion")
     */
    protected $notifChofer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="organizacion")
     */
    protected $eventosTemporal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Logistica", mappedBy="organizacion")
     */
    protected $logisticas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transporte", mappedBy="organizacion")
     */
    protected $transportes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TemplateItinerario", mappedBy="organizacion")
     */
    protected $templateItinerarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChoferServicioHistorico", mappedBy="organizacion")
     */
    protected $choferServicioHistorico;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\informe\Inbox", mappedBy="organizacion")
     */
    protected $informesInbox;


    public function __construct()
    {
        $this->organizaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modulos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->chips = new \Doctrine\Common\Collections\ArrayCollection();
        $this->chips_propietario = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equiposPropios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equiposServicio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dominios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contactos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mantenimientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vehiculoModelos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->horasTaller = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logisticas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transportes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->choferServicioHistorico = new \Doctrine\Common\Collections\ArrayCollection();
        $this->informesInbox = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set proveedor_mapas
     *
     * @param string $proveedorMapas
     */
    public function setProveedorMapas($proveedorMapas)
    {
        $this->proveedor_mapas = $proveedorMapas;
    }

    /**
     * Get proveedor_mapas
     *
     * @return string 
     */
    public function getProveedorMapas()
    {
        return $this->proveedor_mapas;
    }

    /**
     * Set limite_historial
     *
     * @param integer $limiteHistorial
     */
    public function setLimiteHistorial($limiteHistorial)
    {
        $this->limite_historial = $limiteHistorial;
    }

    /**
     * Get limite_historial
     *
     * @return integer 
     */
    public function getLimiteHistorial()
    {
        return $this->limite_historial;
    }

    /**
     * Set telefono_oficina
     *
     * @param string $telefonoOficina
     */
    public function setTelefonoOficina($telefonoOficina)
    {
        $this->telefono_oficina = $telefonoOficina;
    }

    /**
     * Get telefono_oficina
     *
     * @return string 
     */
    public function getTelefonoOficina()
    {
        return $this->telefono_oficina;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set web_site
     *
     * @param string $webSite
     */
    public function setWebSite($webSite)
    {
        $this->web_site = $webSite;
    }

    /**
     * Get web_site
     *
     * @return string 
     */
    public function getWebSite()
    {
        return $this->web_site;
    }

    public function setTipoOrganizacion($tipo_organizacion)
    {
        $this->tipo_organizacion = $tipo_organizacion;
    }

    public function getTipoOrganizacion()
    {
        return $this->tipo_organizacion;
    }

    public function getStrTipoOrganizacion()
    {
        if ($this->tipo_organizacion != null) {
            return $this->tipoStrOrganizacion[$this->tipo_organizacion];
        } else {
            return $this->tipoStrOrganizacion[0];
        }
    }

    public function getOrganizaciones()
    {
        return $this->organizaciones;
    }

    public function getOrganizacionPadre()
    {
        return $this->organizacion_padre;
    }

    public function setOrganizacionPadre($organizacion_padre)
    {
        $this->organizacion_padre = $organizacion_padre;
    }

    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;
    }

    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Set pais
     *
     * @param App\Entity\Pais $pais
     */
    public function setPais(Pais $pais)
    {
        $this->pais = $pais;
    }

    /**
     * Get pais
     *
     * @return App\Entity\Pais 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Add organizaciones
     *
     * @param App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(Organizacion $organizaciones)
    {
        $this->organizaciones[] = $organizaciones;
    }

    /**
     * Add usuarios
     *
     * @param App\Entity\Usuario $usuarios
     */
    public function addUsuario(Usuario $usuarios)
    {
        $this->usuarios[] = $usuarios;
    }

    /**
     * Add modulos
     *
     * @param App\Entity\Modulo $modulos
     */
    public function addModulo(Modulo $modulos)
    {
        $this->modulos[] = $modulos;
    }

    public function addLogistica(Logistica $logistica)
    {
        $this->logisticas[] = $logistica;
    }

    public function addTransporte(Transporte $transporte)
    {
        $this->transportes[] = $transporte;
    }

    /**
     * Get modulos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getModulos()
    {
        return $this->modulos;
    }

    public function setModulos($modulos)
    {
        $this->modulos = $modulos;
    }

    /**
     * Set configuracionDistribuidor
     *
     * @param App\Entity\ConfiguracionDistribuidor $configuracionDistribuidor
     */
    public function setConfiguracionDistribuidor(ConfiguracionDistribuidor $configuracionDistribuidor)
    {
        $this->configuracionDistribuidor = $configuracionDistribuidor;
    }

    /**
     * Get configuracionDistribuidor
     *
     * @return App\Entity\ConfiguracionDistribuidor 
     */
    public function getConfiguracionDistribuidor()
    {
        return $this->configuracionDistribuidor;
    }

    /**
     * Add equiposServicio
     *
     * @param App\Entity\Equipo $equiposServicio
     */
    public function addEquipo(Equipo $equiposServicio)
    {
        $this->equiposServicio[] = $equiposServicio;
    }

    /**
     * Get equiposServicio
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEquiposServicio()
    {
        return $this->equiposServicio;
    }

    /**
     * Get equiposPropios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEquiposPropios()
    {
        return $this->equiposPropios;
    }

    /**
     * Add chips
     *
     * @param App\Entity\Chip $chips
     */
    public function addChip(Chip $chips)
    {
        $this->chips[] = $chips;
    }

    /**
     * Get chips
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChips()
    {
        return $this->chips;
    }

    /**
     * Add referencias
     *
     * @param App\Entity\Referencia $referencias
     */
    public function addReferencia(Referencia $referencias)
    {
        $this->referencias[] = $referencias;
    }

    /**
     * Add referenciasGpm
     *
     * @param App\Entity\ReferenciaGpm $referencia
     */
    public function addReferenciaGpm(ReferenciaGpm $referencia)
    {
        $this->referenciasGpm[] = $referencia;
    }

    /**
     *
     * @param App\Entity\ResponsableGpm $responsable
     */
    public function addResponsable(ResponsableGpm $responsable)
    {
        $this->responsables[] = $responsable;
    }



    /**
     * Get referencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }

    public function removeReferencia(Referencia $referencia)
    {
        $this->referencias->removeElement($referencia);
        return $this;
    }

    public function removeTransporte(Transporte $transporte)
    {
        $this->transportes->removeElement($transporte);
        return $this;
    }

    /**
     * Add gruporeferencias
     *
     * @param App\Entity\GrupoReferencia $gruporeferencias
     */
    public function addGrupoReferencia(GrupoReferencia $gruporeferencias)
    {
        $this->gruporeferencias[] = $gruporeferencias;
    }

    /**
     * Get gruporeferencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrupoReferencias()
    {
        return $this->gruporeferencias;
    }

    public function removeGrupoReferencia(GrupoReferencia $gruporeferencias)
    {
        $this->gruporeferencias->removeElement($gruporeferencias);
        return $this;
    }

    public function removeLogistica(Logistica $logistica)
    {
        $this->logisticas->removeElement($logistica);
        return $this;
    }

    /**
     * Add gruposServicio
     *
     * @param App\Entity\GrupoServicio $gruposServicio
     */
    public function addGrupoServicio(GrupoServicio $gruposServicio)
    {
        $this->gruposServicio[] = $gruposServicio;
    }

    /**
     * Get gruposServicio
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGruposServicio()
    {
        return $this->gruposServicio;
    }

    /**
     * Add servicios
     *
     * @param App\Entity\Servicio $servicios
     */
    public function addServicio(Servicio $servicios)
    {
        $this->servicios[] = $servicios;
    }

    /**
     * Get servicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Add categorias
     *
     * @param App\Entity\Categoria $categorias
     */
    public function addCategoria(Categoria $categorias)
    {
        $this->categorias[] = $categorias;
    }

    /**
     * Get categorias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    public function setUsuarioMaster($usuario_master)
    {
        $this->usuario_master = $usuario_master;
    }

    public function getUsuarioMaster()
    {
        return $this->usuario_master;
    }

    public function getLogo()
    {
        if ($this->isLogoConfig()) {
            $logo = $this->configuracionDistribuidor->getWebPath();
        } else {
            if ($this->tipo_organizacion == 2) {   //la org es un cliente pero no tiene seteado el log
                $logo = $this->getOrganizacionPadre()->getLogo();  //debo devolver el logo del dist.
            } else {
                $logo = null;
            }
        }
        return $logo;
    }

    public function getFileLogo()
    {
        if ($this->configuracionDistribuidor != null) {
            $logo = $this->configuracionDistribuidor->getFilePath();
        } else {
            if ($this->tipo_organizacion != 1) {
                $logo = $this->getOrganizacionPadre()->getFileLogo();
            } else {
                $logo = null;
            }
        }
        return $logo;
    }

    public function isLogoConfig()
    {
        return ($this->configuracionDistribuidor != null) &&
            ($this->configuracionDistribuidor->getWebPath() != null);
    }

    public function getFooter()
    {
        if (($this->tipo_organizacion == 1) && ($this->configuracionDistribuidor != null)) {
            return $this->configuracionDistribuidor->getCopyright();
        } else {
            return 'copyright 2011-2012. Todos los derechos reservados.';
        }
    }

    /**
     * Get chips_propietario
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChipsPropietario()
    {
        return $this->chips_propietario;
    }

    /**
     * Get referencias_propietario
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferenciasPropietario()
    {
        return $this->referencias_propietario;
    }

    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    public function getLatitud()
    {
        return $this->latitud != null ? $this->latitud : 0;
    }

    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    public function getLongitud()
    {
        return $this->longitud != null ? $this->longitud : 0;
    }

    public function getTimeZone()
    {
        if (is_null($this->timeZone)) {
            $this->timeZone = 'America/Buenos_Aires';
        }
        return $this->timeZone;
    }

    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    public function getDatoFiscal()
    {
        return $this->dato_fiscal;
    }

    public function setDatoFiscal($dato_fiscal)
    {
        $this->dato_fiscal = $dato_fiscal;
    }

    public function addDominio(Dominio $dominio)
    {
        $this->dominios[] = $dominio;
    }

    public function getDominios()
    {
        return $this->dominios;
    }

    public function removeDominio(Dominio $dominio)
    {
        $this->dominios->removeElement($dominio);
        return $this;
    }

    public function getBitacora()
    {
        return $this->bitacora;
    }

    public function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;
    }

    public function getEventos()
    {
        return $this->eventos;
    }

    public function setEventos($eventos)
    {
        $this->eventos = $eventos;
    }

    public function getContactos()
    {
        return $this->contactos;
    }

    public function setContactos($contactos)
    {
        $this->contactos = $contactos;
    }

    public function getMantenimientos()
    {
        return $this->mantenimientos;
    }

    public function setMantenimientos($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    /**
     * Add mantenimiento a la organizacion
     */
    public function addMantenimiento($mantenimiento)
    {
        $this->mantenimientos[] = $mantenimiento;
    }

    public function getMantenimientosPropietario()
    {
        return $this->mantenimientos_propietario;
    }

    public function setMantenimientosPropietario($mantenimientos_propietario)
    {
        $this->mantenimientos_propietario = $mantenimientos_propietario;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }


    function getContratistas()
    {
        return $this->contratistas;
    }

    function getPrestaciones()
    {
        return $this->prestaciones;
    }



    function setContratistas($contratistas)
    {
        $this->contratistas = $contratistas;
    }

    function getCentrosCosto()
    {
        return $this->centrosCosto;
    }

    function setCentrosCosto($centrosCosto)
    {
        $this->centrosCosto = $centrosCosto;
    }

    function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;
    }

    function getVars()
    {
        return array(
            'id' => $this->id,
            'nombre' => $this->nombre,
        );
    }

    public function data2array()
    {
        return array(
            'id' => $this->id,
            'nombre' => $this->nombre,
        );
    }

    function getIbuttons()
    {
        return $this->ibuttons;
    }

    function setIbuttons($ibuttons)
    {
        $this->ibuttons = $ibuttons;
    }

    function getDepositos()
    {
        return $this->depositos;
    }

    function getProductos()
    {
        return $this->productos;
    }

    function getTalleres()
    {
        return $this->talleres;
    }

    function setDepositos($depositos)
    {
        $this->depositos = $depositos;
    }

    function setProductos($productos)
    {
        $this->productos = $productos;
    }

    function setTalleres($talleres)
    {
        $this->talleres = $talleres;
    }

    function getMecanicos()
    {
        return $this->mecanicos;
    }

    function setMecanicos($mecanicos)
    {
        $this->mecanicos = $mecanicos;
    }

    function getTalleresExternos()
    {
        return $this->talleresExternos;
    }

    function setTalleresExternos($talleresExternos)
    {
        $this->talleresExternos = $talleresExternos;
    }

    function getRubros()
    {
        return $this->rubros;
    }

    function setRubros($rubros)
    {
        $this->rubros = $rubros;
    }

    function getSectoresTaller()
    {
        return $this->sectoresTaller;
    }

    function setSectoresTaller($sectoresTaller)
    {
        $this->sectoresTaller = $sectoresTaller;
    }

    function getVehiculoModelos()
    {
        return $this->vehiculoModelos;
    }

    function setVehiculoModelos($vehiculoModelos)
    {
        $this->vehiculoModelos = $vehiculoModelos;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    function getHorasTaller()
    {
        return $this->horasTaller;
    }

    function setHorasTaller($horasTaller)
    {
        $this->horasTaller = $horasTaller;
    }

    function getEmpresas()
    {
        return $this->empresas;
    }

    function setEmpresas($empresas)
    {
        $this->empresas = $empresas;
    }

    function getSatelitales()
    {
        return $this->satelitales;
    }

    function getItinerarios()
    {
        return $this->itinerarios;
    }

    function setSatelitales($satelitales)
    {
        $this->satelitales = $satelitales;
    }

    function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;
    }


    function getPuntocargas()
    {
        return $this->puntocargas;
    }

    function setPuntocargas($puntocargas)
    {
        $this->puntocargas = $puntocargas;
    }

    /**
     * Add puntocarga
     *
     * @param App\Entity\PuntoCarga $puntocarga
     */
    public function addPuntoCargas(PuntoCarga $puntocarga)
    {
        $this->puntocargas[] = $puntocarga;
    }

    function getChoferes()
    {
        return $this->choferes;
    }

    function setChoferes($choferes)
    {
        $this->choferes = $choferes;
    }



    function getProyectos()
    {
        return $this->proyectos;
    }

    function getClientes()
    {
        return $this->clientes;
    }

    function setProyectos($proyectos)
    {
        $this->proyectos = $proyectos;
    }

    function setClientes($clientes)
    {
        $this->clientes = $clientes;
    }

    function getReferenciasGpm()
    {
        return $this->referenciasGpm;
    }

    function setReferenciasGpm($referenciasGpm)
    {
        $this->referenciasGpm = $referenciasGpm;
    }


    function getConfigGpm()
    {
        return $this->configGpm;
    }

    function setConfigGpm($configGpm)
    {
        $this->configGpm = $configGpm;
    }

    function getMotivosgpm()
    {
        return $this->motivosgpm;
    }

    function setMotivosgpm($motivosgpm)
    {
        $this->motivosgpm = $motivosgpm;
    }

    function getResponsables()
    {
        return $this->responsables;
    }

    function setResponsables($responsables)
    {
        $this->responsables = $responsables;
    }

    function getNotifChofer()
    {
        return $this->notifChofer;
    }

    function setNotifChofer($notifChofer)
    {
        $this->notifChofer = $notifChofer;
    }

    /**
     * Get the value of eventosTemporal
     */
    public function getEventosTemporal()
    {
        return $this->eventosTemporal;
    }

    /**
     * Set the value of eventosTemporal
     *
     * @return  self
     */
    public function setEventosTemporal($eventosTemporal)
    {
        $this->eventosTemporal = $eventosTemporal;

        return $this;
    }

    /**   
     * Get the value of checkCargasCombustible
     */
    public function getCheckCargasCombustible()
    {
        return $this->checkCargasCombustible;
    }

    /**
     * Set the value of checkCargasCombustible
     *
     * @return  self
     */
    public function setCheckCargasCombustible($checkCargasCombustible)
    {
        $this->checkCargasCombustible = $checkCargasCombustible;

        return $this;
    }

    /**
     * Get the value of logisticas
     */
    public function getLogisticas()
    {
        return $this->logisticas;
    }

    /**
     * Set the value of logisticas
     *
     * @return  self
     */
    public function setLogisticas($logisticas)
    {
        $this->logisticas = $logisticas;

        return $this;
    }

    /**
     * Get the value of transportes
     */
    public function getTransportes()
    {
        return $this->transportes;
    }

    /**
     * Set the value of transportes
     *
     * @return  self
     */
    public function setTransportes($transportes)
    {
        $this->transportes = $transportes;

        return $this;
    }

    /**
     * Get the value of templateItinerarios
     */
    public function getTemplateItinerarios()
    {
        return $this->templateItinerarios;
    }

    /**
     * Set the value of templateItinerarios
     *
     * @return  self
     */
    public function setTemplateItinerarios($templateItinerarios)
    {
        $this->templateItinerarios = $templateItinerarios;

        return $this;
    }

    public function addChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico[] = $choferServicioHistorico;
    }

    public function removeChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico->removeElement($choferServicioHistorico);
        return $this;
    }

    /**
     * Get the value of choferServicioHistorico
     */
    public function getChoferServicioHistorico()
    {
        return $this->choferServicioHistorico;
    }

    /**
     * Get the value of ordenesTrabajo
     */
    public function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    /**
     * Set the value of ordenesTrabajo
     *
     * @return  self
     */
    public function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;

        return $this;
    }

    /**
     * Get the value of informesInbox
     */ 
    public function getInformesInbox()
    {
        return $this->informesInbox;
    }

    /**
     * Set the value of informesInbox
     *
     * @return  self
     */ 
    public function setInformesInbox($informesInbox)
    {
        $this->informesInbox = $informesInbox;

        return $this;
    }
}
