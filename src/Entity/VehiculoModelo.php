<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="vehiculomodelo")
 * @ORM\Entity(repositoryClass="App\Repository\VehiculoModeloRepository")
 */
class VehiculoModelo
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehiculo", mappedBy="carModelo")
     */
    protected $vehiculos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="vehiculoModelos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoFichaTecnica", mappedBy="modelo", cascade={"persist","remove"})
     */
    protected $fichaTecnica;

    public function __construct()
    {
        $this->vehiculos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fichaTecnica = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getVehiculos()
    {
        return $this->vehiculos;
    }

    public function setVehiculos($vehiculos)
    {
        $this->vehiculos = $vehiculos;
    }

    function getFichaTecnica()
    {
        return $this->fichaTecnica;
    }

    function setFichaTecnica($fichaTecnica)
    {
        $this->fichaTecnica = $fichaTecnica;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function addFichasTecnicas($fichaTecnica)
    {
        if (!$this->fichaTecnica->contains($fichaTecnica)) {
            $this->fichaTecnica[] = $fichaTecnica;
            $fichaTecnica->setModelo($this);
        }
    }

    public function removeFichaTecnica($fichaTecnica)
    {
        $this->fichaTecnica->removeElement($fichaTecnica);
    }
}
