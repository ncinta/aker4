<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * App\Entity\Contacto
 *
 * @ORM\Table(name="contacto", indexes={
 *     @ORM\Index(name="idx_contacto_celular_compuesto", columns={"celular"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\ContactoRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Contacto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="contacto_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable = true)
     */
    private $email;

    /** +5492613647658 2613647658
     * @ORM\Column(name="celular", type="string", length=255, nullable = true)
     * @Assert\Length(min = 10, max = 14)
     * @Assert\Regex(
     *     pattern="/^[F0-9]+$/",
     *     message="Contiene caracteres inválidos."
     * )
     */
    private $celular;

    /**
     * @ORM\Column(name="cargo", type="string", length=255, nullable = true)
     */
    private $cargo;

    /**
     * @ORM\Column(name="noticeme", type="string", length=255, nullable = true)
     */
    private $noticeme;

    /**
     * @ORM\Column(name="token", type="string", length=255, nullable = true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="contactos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoContacto", inversedBy="contactos")
     * @ORM\JoinColumn(name="tipoContacto_id", referencedColumnName="id")
     */
    protected $tipoContacto;

    /** Notificaciones
     * @ORM\OneToMany(targetEntity="App\Entity\EventoNotificacion", mappedBy="contacto", cascade={"persist"})
     */
    protected $notificaciones;

    /** NotificacionesHistorial
     * @ORM\OneToMany(targetEntity="App\Entity\EventoHistorialNotificacion", mappedBy="contacto", cascade={"persist"})
     */
    protected $historialNotificaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotifContact", mappedBy="contacto")
     */
    protected $notificacionesChofer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Empresa", inversedBy="contactos")
     * @ORM\JoinColumn(name="empresa_id", nullable=true, referencedColumnName="id")
     */
    protected $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Logistica", inversedBy="contactos")
     * @ORM\JoinColumn(name="logistica_id", nullable=true, referencedColumnName="id")
     */
    protected $logistica;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transporte", inversedBy="contactos")
     * @ORM\JoinColumn(name="transporte_id", nullable=true, referencedColumnName="id", onDelete="SET NULL")
     */
    protected $transporte;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Notificacion", inversedBy="contactos")
     * @ORM\JoinTable(name="contacto_notificacion",
     *      joinColumns={@ORM\JoinColumn(name="notificacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contacto_id", referencedColumnName="id")}
     *      )
     */
    private $notificacionesContacto;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\TipoNotificacion", inversedBy="contactos")
     * @ORM\JoinTable(name="contacto_tiponotificacion",
     *      joinColumns={@ORM\JoinColumn(name="tiponotificacion_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contacto_id", referencedColumnName="id")}
     *      )
     */
    private $tipoNotificaciones;


    /**
     * Inverse Side
     * @ORM\ManyToMany(targetEntity="App\Entity\Itinerario", mappedBy="contactos", cascade={"persist"})
     * @ORM\JoinTable(name="itinerarioContacto")
     */
    protected $itinerarios;


    
    public function __construct()
    {
        $this->tipoNotificaciones = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getCelular()
    {
        return $this->celular;
    }

    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getNotificaciones()
    {
        return $this->notificaciones;
    }

    public function setNotificaciones($notificaciones)
    {
        $this->notificaciones = $notificaciones;
    }

    public function getNoticeme()
    {
        return $this->noticeme;
    }

    public function setNoticeme($noticeme)
    {
        $this->noticeme = $noticeme;
        return $this;
    }

    function getTipoContacto()
    {
        return $this->tipoContacto;
    }

    function setTipoContacto($tipoContacto)
    {
        $this->tipoContacto = $tipoContacto;
    }

    function getChoferes()
    {
        return $this->choferes;
    }

    function setChoferes($choferes)
    {
        $this->choferes = $choferes;
    }

    function getNotificacionesChofer()
    {
        return $this->notificacionesChofer;
    }

    function setNotificacionesChofer($notificacionesChofer)
    {
        $this->notificacionesChofer = $notificacionesChofer;
    }

    /**
     * Get the value of empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set the value of empresa
     *
     * @return  self
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get the value of transporte
     */
    public function getTransporte()
    {
        return $this->transporte;
    }

    /**
     * Set the value of transporte
     *
     * @return  self
     */
    public function setTransporte($transporte)
    {
        $this->transporte = $transporte;

        return $this;
    }

    /**
     * Get owning Side
     */
    public function getNotificacionesContacto()
    {
        return $this->notificacionesContacto;
    }

    /**
     * Set owning Side
     *
     * @return  self
     */
    public function setNotificacionesContacto($notificacionesContacto)
    {
        $this->notificacionesContacto = $notificacionesContacto;

        return $this;
    }

    /**
     * Get the value of token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get the value of logistica
     */
    public function getLogistica()
    {
        return $this->logistica;
    }

    /**
     * Set the value of logistica
     *
     * @return  self
     */
    public function setLogistica($logistica)
    {
        $this->logistica = $logistica;

        return $this;
    }

    /**
     * Get inverse Side
     */
    public function getItinerarios()
    {
        return $this->itinerarios;
    }

    /**
     * Set inverse Side
     *
     * @return  self
     */
    public function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;

        return $this;
    }

    /**
     * Get the value of cargo
     */ 
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set the value of cargo
     *
     * @return  self
     */ 
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get owning Side
     */ 
    public function getTipoNotificaciones()
    {
        return $this->tipoNotificaciones;
    }

    /**
     * Set owning Side
     *
     * @return  self
     */ 
    public function setTipoNotificaciones($tipoNotificaciones)
    {
        $this->tipoNotificaciones = $tipoNotificaciones;

        return $this;
    }

    public function addTipoNotificaciones(\App\Entity\TipoNotificacion $tipo)
    {
        if(!$this->tipoNotificaciones->contains($tipo)){
            $this->tipoNotificaciones[] = $tipo;

        }
    } 
    
    public function removeTipoNotificaciones()
    {
            $this->tipoNotificaciones->clear();
    }

    public function getByCodename($codename){
        foreach($this->getTipoNotificaciones() as $tipo){
            if($tipo->getCodename() == $codename){
                return true;
            }
        }
        return false;
    }   
    
    /**
     * Get the value of historialNotificaciones
     */ 
    public function getHistorialNotificaciones()
    {
        return $this->historialNotificaciones;
    }

    /**
     * Set the value of historialNotificaciones
     *
     * @return  self
     */ 
    public function setHistorialNotificaciones($historialNotificaciones)
    {
        $this->historialNotificaciones = $historialNotificaciones;

        return $this;
    }
}
