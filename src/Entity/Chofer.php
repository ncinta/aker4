<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * App\Entity\Chofer
 *
 * @ORM\Table(name="chofer", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="chofer_idx", columns={"documento","organizacion_id"})
 * })
 * @Unique\UniqueEntity(fields={"documento","organizacion"}, ignoreNull=true, message="Ya existe un chofer con este documento en esta organización")
 * @ORM\Entity(repositoryClass="App\Repository\ChoferRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Chofer
{    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var string Nombre
     *
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;

    /**
     * Es la identificación electronica del dallas
     * @var string dallas
     *
     * @ORM\Column(name="dallas", type="string", nullable=true, unique=true)
     */
    private $dallas;

    /**
     * @var string cuit
     *
     * @ORM\Column(name="cuit", type="string", nullable=true)
     */
    private $cuit;

    /**
     * @var string documento
     *
     * @ORM\Column(name="documento", type="string", nullable=true)
     */
    private $documento;

    /**
     * @var string telefono
     *
     * @ORM\Column(name="telefono", type="string", nullable=true)
     */
    private $telefono;

    /**
     * @var date lic_expedicion
     *
     * @ORM\Column(name="lic_expedicion", type="date", nullable=true)
     */
    private $licenciaExpedicion;

    /**
     * @var date lic_vencimiento
     *
     * @ORM\Column(name="lic_vencimiento", type="date", nullable=true)
     */
    private $licenciaVencimiento;

    /**
     * @var string lic_numero
     *
     * @ORM\Column(name="lic_numero", type="string", nullable=true)
     */
    private $licenciaNumero;

    /**
     * @var date lic_vencimiento
     *
     * @ORM\Column(name="fecha_asignacion", type="datetime", nullable=true)
     */
    private $fechaAsignacion;

    /**
     * @var string lic_categoria
     *
     * @ORM\Column(name="lic_categoria", type="string", nullable=true)
     */
    private $licenciaCategoria;

    /**
     * @ORM\Column(name="api_code", type="string", nullable=true)
     */
    protected $apiCode;

    /**
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="choferes")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var Ibutton
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Ibutton", inversedBy="chofer")
     * @ORM\JoinColumn(name="ibutton_id", referencedColumnName="id", nullable=true)
     */
    private $ibutton;

    /**
     * @var Servicio
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Servicio", inversedBy="chofer")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true)
     */
    private $servicio;

    /**
     * @var historicoibutton
     *
     * @ORM\OneToMany(targetEntity="App\Entity\HistoricoIbutton", mappedBy="chofer")
     */
    private $historicoibutton;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CargaCombustible", mappedBy="chofer")
     */
    protected $cargasCombustible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CheckCargaCombustible", mappedBy="chofer")
     */
    protected $checkCargasCombustible;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Licencia", mappedBy="chofer")
     */
    protected $licencias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotifChof", mappedBy="chofer")
     */
    protected $notificaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bitacora", mappedBy="chofer",cascade={"persist","remove"});
     */
    protected $bitacora;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraServicio", mappedBy="chofer",cascade={"persist","remove"});
     */
    protected $bitacoraServicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItinerarioServicio", mappedBy="chofer",cascade={"persist","remove"});
     */
    protected $itinerarios;

    /**
     * @var Transporte
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Transporte", inversedBy="choferes")
     * @ORM\JoinColumn(name="transporte_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $transporte;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChoferServicioHistorico", mappedBy="chofer")
     */
    protected $choferServicioHistorico;

    public function __construct()
    {
        
        $this->choferServicioHistorico = new \Doctrine\Common\Collections\ArrayCollection();
       
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getDallas()
    {
        return $this->dallas;
    }

    public function setDallas($dallas)
    {
        $this->dallas = $dallas;
    }

    public function getCuit()
    {
        return $this->cuit;
    }

    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    }

    public function getDocumento()
    {
        return $this->documento;
    }

    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function getLicenciaExpedicion()
    {
        return $this->licenciaExpedicion;
    }

    public function setLicenciaExpedicion($licenciaExpedicion)
    {
        $this->licenciaExpedicion = $licenciaExpedicion;
    }

    public function getLicenciaVencimiento()
    {
        return $this->licenciaVencimiento;
    }

    public function setLicenciaVencimiento($licenciaVencimiento)
    {
        $this->licenciaVencimiento = $licenciaVencimiento;
    }

    public function getLicenciaNumero()
    {
        return $this->licenciaNumero;
    }

    public function setLicenciaNumero($licenciaNumero)
    {
        $this->licenciaNumero = $licenciaNumero;
    }

    public function getLicenciaCategoria()
    {
        return $this->licenciaCategoria;
    }

    public function setLicenciaCategoria($licenciaCategoria)
    {
        $this->licenciaCategoria = $licenciaCategoria;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion(Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getIbutton()
    {
        return $this->ibutton;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setIbutton($ibutton)
    {
        $this->ibutton = $ibutton;
    }

    function getFechaAsignacion()
    {
        return $this->fechaAsignacion;
    }

    function setFechaAsignacion($fecha_asignacion)
    {
        $this->fechaAsignacion = $fecha_asignacion;
    }

    function getHistoricoibutton()
    {
        return $this->historicoibutton;
    }

    /**
     *
     * @param App\Entity\HistoricoIbutton $historicoibutton
     */
    function setHistoricoibutton(HistoricoIbutton $historicoibutton)
    {
        $this->historicoibutton = $historicoibutton;
    }

    function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    function setCargasCombustible($cargasCombustible)
    {
        $this->cargasCombustible = $cargasCombustible;
    }

    function getLicencias()
    {
        return $this->licencias;
    }

    function setLicencias($licencias)
    {
        $this->licencias = $licencias;
    }

    function getContactos()
    {
        return $this->contactos;
    }

    function setContactos($contactos)
    {
        $this->contactos = $contactos;
    }

    function getNotificaciones()
    {
        return $this->notificaciones;
    }

    function setNotificaciones($notificaciones)
    {
        $this->notificaciones = $notificaciones;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio): void
    {
        $this->servicio = $servicio;
    }

    public function getBitacora()
    {
        return $this->bitacora;
    }

    public function setBitacora($bitacora): void
    {
        $this->bitacora = $bitacora;
    }

    public function getApiCode()
    {
        return $this->apiCode;
    }

    public function setApiCode($apiCode)
    {
        $this->apiCode = $apiCode;
    }

    /**
     * Get the value of transporte
     *
     * @return  Transporte
     */
    public function getTransporte()
    {
        return $this->transporte;
    }

    /**
     * Set the value of transporte
     *
     * @param  Transporte  $transporte
     *
     * @return  self
     */
    public function setTransporte(Transporte $transporte)
    {
        $this->transporte = $transporte;

        return $this;
    }

    /**
     * Get the value of itinerarios
     */
    public function getItinerarios()
    {
        return $this->itinerarios;
    }

    /**
     * Set the value of itinerarios
     *
     * @return  self
     */
    public function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;

        return $this;
    }

    public function addChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico[] = $choferServicioHistorico;
    }

    public function removeChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico->removeElement($choferServicioHistorico);
        return $this;
    }

    /**
     * Get the value of choferServicioHistorico
     */ 
    public function getChoferServicioHistorico()
    {
        return $this->choferServicioHistorico;
    }
}
