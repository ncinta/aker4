<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ActividadGpm
 *
 * @author nicolas
 */

/**
 * App\Entity\ActividadGpm
 *
 * @ORM\Table(name="actividad_gpm")
 * @ORM\Entity(repositoryClass="App\Repository\ActividadGpmRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActividadGpm
{

    const ESTADO_PROGRAMADO = 0;
    const ESTADO_ENCURSO = 1;
    const ESTADO_FINALIZADO = 2;
    const ESTADO_CERRADO = 3;
    const BAJA_TENSION = 1;
    const MEDIA_TENSION = 2;
    const ALTA_TENSION = 3;

    private $strEstado = array(
        self::ESTADO_PROGRAMADO => 'Programado',
        self::ESTADO_ENCURSO => 'En Curso',
        self::ESTADO_FINALIZADO => 'Finalizado',
        self::ESTADO_CERRADO => 'Cerrado',
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        if ($this->estado != null) {
            return $this->strEstado[$this->estado];
        } else {
            return $this->strEstado[0];
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=true) 
     */
    private $fechaInicio;

    /**
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true) 
     */
    private $fechaFin;

    /**
     * @ORM\Column(name="estado", type="integer", nullable=true) 
     */
    private $estado;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     *
     * @ORM\Column(name="opciones", type="array", nullable=true)
     */
    private $opciones;

    /**
     * @ORM\Column(name="ancho_trabajo", type="float", nullable=true) 
     */
    private $anchoTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="Contratista", inversedBy="actividades")
     * @ORM\JoinColumn(name="contratista_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $contratista;

    /**
     * @ORM\ManyToOne(targetEntity="Proyecto", inversedBy="actividades")
     * @ORM\JoinColumn(name="proyecto_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $proyecto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActgpmServicio", mappedBy="actividad",cascade={"remove"})
     */
    private $actgpmServicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActgpmRefgpm", mappedBy="actividad",cascade={"remove"})
     */
    private $actgpmRefgpm;

    /**
     * Inverse Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Personal", mappedBy="actividad", cascade={"persist"})
     */
    protected $personal;

    /**
     * Inverse Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadManual", mappedBy="actividad", cascade={"persist"})
     */
    protected $ingresos;

    /**
     * @ORM\ManyToOne(targetEntity="ResponsableGpm", inversedBy="actividades")
     * @ORM\JoinColumn(name="responsable_id", referencedColumnName="id")
     */
    protected $responsable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MotivoActividadGpm", inversedBy="actividades")
     * @ORM\JoinColumn(name="motivo_id", referencedColumnName="id")
     */
    protected $motivo;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getContratista()
    {
        return $this->contratista;
    }

    function getProyecto()
    {
        return $this->proyecto;
    }

    function getActgpmServicios()
    {
        return $this->actgpmServicios;
    }

    function getActgpmRefgpm()
    {
        return $this->actgpmRefgpm;
    }

    function getPersonal()
    {
        return $this->personal;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }

    function setProyecto($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    function setActgpmServicios($actgpmServicios)
    {
        $this->actgpmServicios = $actgpmServicios;
    }

    function setActgpmRefgpm($actgpmRefgpm)
    {
        $this->actgpmRefgpm = $actgpmRefgpm;
    }

    /**
     * Add Personal
     *
     * @param App\Entity\Personal $personal
     */
    public function addPersonal(Personal $personal)
    {
        $this->personal[] = $personal;
    }

    public function removePersonal()
    {
        $this->personal = null;
    }

    /**
     * Add servicios
     *
     * @param App\Entity\ActgpmRefgpm $actgpmRefgpm
     */
    public function addActgpmRefgpm(\App\Entity\ActgpmRefgpm $actgpmRefgpm)
    {
        $this->actgpmRefgpm[] = $actgpmRefgpm;
    }

    function getResponsable()
    {
        return $this->responsable;
    }

    function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }

    function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    function getFechaFin()
    {
        return $this->fechaFin;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    }

    function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function addActServicios(\App\Entity\ActgpmServicio $actServicio)
    {
        $this->actgpmServicios[] = $actServicio;
    }

    public function removeActServicios($actServicio)
    {
        $this->actgpmServicios->removeElement($actServicio);
    }

    public function addActReferencias($actReferencia)
    {
        $this->actgpmRefgpm[] = $actReferencia;
    }

    public function removeActReferencias($actReferencia)
    {
        $this->actgpmRefgpm->removeElement($actReferencia);
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function getBajaTension()
    {
        return $this->bajaTension;
    }

    function getMediaTension()
    {
        return $this->mediaTension;
    }

    function getAltaTension()
    {
        return $this->altaTension;
    }

    function setBajaTension($bajaTension)
    {
        $this->bajaTension = $bajaTension;
    }

    function setMediaTension($mediaTension)
    {
        $this->mediaTension = $mediaTension;
    }

    function setAltaTension($altaTension)
    {
        $this->altaTension = $altaTension;
    }

    function getMotivo()
    {
        return $this->motivo;
    }

    function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    function getOpciones()
    {
        return $this->opciones;
    }

    function setOpciones($opciones)
    {
        $this->opciones = $opciones;
    }

    /**
     * Add Ingresos
     *
     * @param App\Entity\ActividadManual $ingresos
     */
    public function addIngreso(ActividadManual $ingreso)
    {
        $this->ingresos[] = $ingreso;
    }

    public function removeIngreso()
    {
        $this->ingresos = null;
    }

    function getAnchoTrabajo()
    {
        return $this->anchoTrabajo;
    }

    function getIngresos()
    {
        return $this->ingresos;
    }

    function setAnchoTrabajo($anchoTrabajo)
    {
        $this->anchoTrabajo = $anchoTrabajo;
    }

    function setIngresos($ingresos)
    {
        $this->ingresos = $ingresos;
    }
}
