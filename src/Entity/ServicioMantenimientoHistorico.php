<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="serviciomantenimientohistorico")
 * @ORM\Entity(repositoryClass="App\Repository\ServicioMantenimientoHistoricoRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class ServicioMantenimientoHistorico
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime Fecha en la que se realizo el manteniemiento.
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var integer $datoUltimo
     *
     * @ORM\Column(name="dato_ultimo", type="integer", nullable=true)
     */
    private $datoUltimo;

    /**
     * Indica cuanto es lo que se registro el odometro (horometro).
     * @var integer $datoRealizado
     *
     * @ORM\Column(name="dato_realizado", type="integer", nullable=true)
     */
    private $datoRealizado;

    /**
     * @var integer $datoProximo
     *
     * @ORM\Column(name="dato_proximo", type="integer", nullable=true)
     */
    private $datoProximo;

    /**
     * @ORM\Column(name="fecha_proximo", type="date", nullable=true)
     */
    private $fechaProximo;

    /**
     * @var text $observacion es el texto descriptivo de lo que se ha realizado
     *
     * @ORM\Column(name="observacion", type="text")
     */
    private $observacion;

    /**
     * @var float $costo
     *
     * @ORM\Column(name="costo", type="float", nullable=true)
     */
    private $costo;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServicioMantenimiento", inversedBy="historicos")
     * @ORM\JoinColumn(name="serviciomantenimiento_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $servicioMantenimiento;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="mantenimientosHistorico")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Taller", inversedBy="servicioMantenimientos")
     * @ORM\JoinColumn(name="taller_id", referencedColumnName="id", nullable=true)
     */
    protected $taller;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Deposito", inversedBy="servicioMantenimientos")
     * @ORM\JoinColumn(name="deposito_id", referencedColumnName="id", nullable=true)
     */
    protected $deposito;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="servicioMantenimientos")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id",nullable=true, onDelete="CASCADE")
     */
    protected $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadHistorico", mappedBy="historico",cascade ={"persist"})
     */
    protected $historicoActividad;

    /**
     * Inverse Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductoServicioMantHist", mappedBy="mantenimiento")
     */
    private $historicos;

    /**
     * Inverse Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MecanicoServicioMantHist", mappedBy="mantenimiento")
     */
    private $mecanicos;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFecha()
    {
        //return $this->fecha;
        //die('////<pre>' . nl2br(var_export($this->fin_prestacion, true)) . '</pre>////');
        if (is_null($this->fecha)) {
            return $this->fecha;
        } elseif ($this->fecha instanceof \DateTime) {
            return date_format($this->fecha, 'd-m-Y');
        }
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getObservacion()
    {
        return $this->observacion;
    }

    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    public function getCosto()
    {
        return $this->costo;
    }

    public function setCosto($costo)
    {
        $this->costo = $costo;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function getServicioMantenimiento()
    {
        return $this->servicioMantenimiento;
    }

    public function setServicioMantenimiento($servicioMantenimiento)
    {
        $this->servicioMantenimiento = $servicioMantenimiento;
    }

    function getVars()
    {
        return get_object_vars($this);
    }

    public function getDatoUltimo()
    {
        return $this->datoUltimo;
    }

    public function setDatoUltimo($datoUltimo)
    {
        $this->datoUltimo = $datoUltimo;
    }

    public function getDatoRealizado()
    {
        return $this->datoRealizado;
    }

    public function setDatoRealizado($datoRealizado)
    {
        $this->datoRealizado = $datoRealizado;
    }

    public function getDatoProximo()
    {
        return $this->datoProximo;
    }

    public function setDatoProximo($datoProximo)
    {
        $this->datoProximo = $datoProximo;
    }

    public function getFechaProximo()
    {
        return $this->fechaProximo;
    }

    public function setFechaProximo($fechaProximo)
    {
        $this->fechaProximo = $fechaProximo;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    function getTaller()
    {
        return $this->taller;
    }

    function setTaller($taller)
    {
        $this->taller = $taller;
    }

    public function data2array()
    {
        $d['id'] = $this->id;
        if (!is_null($this->datoProximo))
            $d['datoProximo'] = $this->datoProximo;
        if (!is_null($this->datoRealizado))
            $d['datoRealizado'] = $this->datoRealizado;
        if (!is_null($this->datoUltimo))
            $d['datoUltimo'] = $this->datoUltimo;
        if (!is_null($this->fecha))
            $d['fecha'] = $this->fecha;
        if (!is_null($this->observacion))
            $d['observacion'] = $this->observacion;
        if (!is_null($this->costo))
            $d['costo'] = $this->costo;
        return $d;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function getHistoricoActividad()
    {
        return $this->historicoActividad;
    }

    function setHistoricoActividad($historico_actividad)
    {
        $this->historicoActividad = $historico_actividad;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }

    function getHistoricos()
    {
        return $this->historicos;
    }

    function setHistoricos($historicos)
    {
        $this->historicos = $historicos;
    }

    public function addHistoricos($historico)
    {
        //die('////<pre>' . nl2br(var_export(count($this->servicios), true)) . '</pre>////');        
        if (!$this->historicos->contains($historico)) {
            $this->historicos[] = $historico;
        }
    }


    public function __construct()
    {
        $this->historicos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mecanicos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getMecanicos()
    {
        return $this->mecanicos;
    }

    function setMecanicos($mecanicos)
    {
        $this->mecanicos = $mecanicos;
    }

    public function addMecanicos($mecanico)
    {
        if (!$this->mecanicos->contains($mecanico)) {
            $this->mecanicos[] = $mecanico;
        }
    }
}
