<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Index;
use App\Entity\EventoHistorial as EventoHistorial;


/**
 * Ver https://akercontrol.atlassian.net/browse/AW-589?atlOrigin=eyJpIjoiNWJhMTg4NmQ1MWRhNGU2MDhiODJjODk4OTQzY2U5NjEiLCJwIjoiaiJ9
 * 
 * @ORM\Table(name="eventohistorialnotificacion", indexes={
 *     @ORM\Index(name="idx_eventohistorialnotificacion_compuesto", columns={"canal", "contacto_id", "eventohistorial_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EventoHistorialNotificacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EventoHistorialNotificacion
{

    //0=inicial 1=enviado 2=recibido 3=visto -1=error
    const ESTADO_INICIAL = 0;
    const ESTADO_ENVIADO = 1;
    const ESTADO_RECIBIDO = 2;
    const ESTADO_VISTO = 3;
    const ESTADO_ERROR = -1;

    private $strEstado = array(
        self::ESTADO_INICIAL => 'Creado',
        self::ESTADO_ENVIADO => 'Enviado',
        self::ESTADO_RECIBIDO => 'Recibido',
        self::ESTADO_VISTO => 'Visto',
        self::ESTADO_ERROR => 'Error'
    );

    public function getStrEstado()
    {
        if ($this->estado != null) {
            return $this->strEstado[$this->estado];
        } else {
            return $this->strEstado[0];
        }
    }

    //(1=celular/push 2=email)
    const CANAL_PUSH = 1;
    const CANAL_EMAIL = 2;

    private $strCanal = array(
        self::CANAL_PUSH => 'Push',
        self::CANAL_EMAIL => 'EMail',
    );

    public function getStrCanal()
    {
        if ($this->canal != null) {
            return $this->strCanal[$this->canal];
        } else {
            return $this->strCanal[0];
        }
    }
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $uid
     *
     * @ORM\Column(name="uid", type="string", nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(name="fecha_envio", type="datetime", nullable=true)
     */
    private $fecha_envio;
    /**
     * @ORM\Column(name="fecha_recepcion", type="datetime", nullable=true)
     */
    private $fecha_recepcion;
    /**
     * @ORM\Column(name="fecha_lectura", type="datetime", nullable=true)
     */
    private $fecha_lectura;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var Contacto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Contacto", inversedBy="historialNotificaciones")
     * @ORM\JoinColumn(name="contacto_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $contacto;

    /**
     * @var Evento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\EventoHistorial", inversedBy="notificaciones")
     * @ORM\JoinColumn(name="eventohistorial_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $eventoHistorial;

    /**
     * @var integer  (0=inicial 1=enviado 2=recibido 3=visto -1=error)
     * @ORM\Column(name="estado", type="integer")
     */
    protected $estado;

    /**
     * @var integer  (1=celular/push 2=email)
     * @ORM\Column(name="canal", type="integer")
     */
    protected $canal;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }


    /**
     * Get $uid
     *
     * @return  string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set $uid
     *
     * @param  string  $uid  $uid
     *
     * @return  self
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get the value of fecha_envio
     */
    public function getFechaEnvio()
    {
        return $this->fecha_envio;
    }

    /**
     * Set the value of fecha_envio
     *
     * @return  self
     */
    public function setFechaEnvio($fecha_envio)
    {
        $this->fecha_envio = $fecha_envio;

        return $this;
    }

    /**
     * Get the value of fecha_recepcion
     */
    public function getFechaRecepcion()
    {
        return $this->fecha_recepcion;
    }

    /**
     * Set the value of fecha_recepcion
     *
     * @return  self
     */
    public function setFechaRecepcion($fecha_recepcion)
    {
        $this->fecha_recepcion = $fecha_recepcion;

        return $this;
    }

    /**
     * Get the value of fecha_lectura
     */
    public function getFechaLectura()
    {
        return $this->fecha_lectura;
    }

    /**
     * Set the value of fecha_lectura
     *
     * @return  self
     */
    public function setFechaLectura($fecha_lectura)
    {
        $this->fecha_lectura = $fecha_lectura;

        return $this;
    }

    /**
     * Get the value of updated_at
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of eventoHistorial
     *
     * @return  Evento
     */
    public function getEventoHistorial()
    {
        return $this->eventoHistorial;
    }

    /**
     * Set the value of eventoHistorial
     *
     * @param  Evento  $eventoHistorial
     *
     * @return  self
     */
    public function setEventoHistorial($eventoHistorial)
    {
        $this->eventoHistorial = $eventoHistorial;

        return $this;
    }

    /**
     * Get (0=inicial 1=enviado 2=recibido 3=visto -1=error)
     *
     * @return  integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set (0=inicial 1=enviado 2=recibido 3=visto -1=error)
     *
     * @param  integer  $estado  (0=inicial 1=enviado 2=recibido 3=visto -1=error)
     *
     * @return  self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of contacto
     *
     * @return  Contacto
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set the value of contacto
     *
     * @param  Contacto  $contacto
     *
     * @return  self
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get (1=celular/push 2=email)
     *
     * @return  integer
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set (1=celular/push 2=email)
     *
     * @param  integer  $canal  (1=celular/push 2=email)
     *
     * @return  self
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }
}
