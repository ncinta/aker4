<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tipodatoextra")
 * @ORM\Entity(repositoryClass="App\Repository\TipoDatoExtraRepository")
 */
class TipoDatoExtra
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoExtra", mappedBy="tipoDatoExtra")
     */
    private $datosExtra;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getDatosExtra()
    {
        return $this->datosExtra;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setDatosExtra($datosExtra)
    {
        $this->datosExtra = $datosExtra;
    }


    function __toString()
    {
        return $this->nombre;
    }
}
