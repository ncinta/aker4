<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="poi")
 * @ORM\Entity(repositoryClass="App\Repository\PoiRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Poi
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="fecha_ingreso", type="datetime", nullable = true) 
     */
    private $fechaIngreso;

    /**
     * @ORM\Column(name="fecha_egreso", type="datetime", nullable = true) 
     */
    private $fechaEgreso;

    /**
     * @ORM\Column(name="orden", type="integer", nullable = true)
     */
    private $orden;

    /**
     *  Indica que tipo de parada es, si es un pick (inicio) o drop (descarga)
     * @ORM\Column(name="stop_type", type="string", nullable=true)
     */
    private $stopType;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="pois")
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $itinerario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referencia", inversedBy="pois")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id",  nullable=true, onDelete="CASCADE")
     */
    protected $referencia;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Notificacion", inversedBy="pois")
     * @ORM\JoinTable(name="poiNotificacion")
     */
    private $notificaciones;

    function __toString()
    {
        return $this->nombre;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }

    function getOrden()
    {
        return $this->orden;
    }

    function getItinerario()
    {
        return $this->itinerario;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;
    }

    function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;
    }

    function setOrden($orden)
    {
        $this->orden = $orden;
    }

    function setItinerario($itinerario)
    {
        $this->itinerario = $itinerario;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    function addNotificacion($notificacion)
    {
        $this->notificaciones[] = $notificacion;
    }
    public function removeNotificacion($notificacion)
    {
        $this->notificaciones->removeElement($notificacion);
    }

    /**
     * Get indica que tipo de parada es, si es un pick (inicio) o drop (descarga)
     */ 
    public function getStopType()
    {
        return $this->stopType;
    }

    /**
     * Set indica que tipo de parada es, si es un pick (inicio) o drop (descarga)
     *
     * @return  self
     */ 
    public function setStopType($stopType)
    {
        $this->stopType = $stopType;

        return $this;
    }
}
