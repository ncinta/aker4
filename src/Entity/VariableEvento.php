<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\UnidadDeMedida;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="variableevento")
 * @ORM\Entity
 */
class VariableEvento
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $codename
     *
     * @ORM\Column(name="codename", type="string", length=255, unique=true)
     */
    private $codename;

    /**
     * @var string $dataType
     *
     * @ORM\Column(name="datatype", type="string", length=255)
     */
    private $dataType;

    /**
     * @var string $cotaMinima
     *
     * @ORM\Column(name="cotaMinima", type="string", length=255, nullable=true)
     */
    private $cotaMinima;

    /**
     * @var string $cotaMaxima
     *
     * @ORM\Column(name="cotaMaxima", type="string", length=255, nullable=true)
     */
    private $cotaMaxima;

    /**
     * @var integer $unidadMedida
     *
     * @ORM\Column(name="unidadMedida", type="string", length=30, nullable=true)
     */
    private $unidadMedida;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\TipoEvento", mappedBy="variables")
     */
    private $tiposEvento;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoParametro", mappedBy="variable")
     */
    private $eventosParametro;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getDataType()
    {
        return $this->dataType;
    }

    public function setDataType($dataType)
    {
        $this->dataType = strtoupper($dataType);
    }

    public function getCotaMinima()
    {
        return $this->cotaMinima;
    }

    public function setCotaMinima($cotaMinima)
    {
        $this->cotaMinima = $cotaMinima;
    }

    public function getCotaMaxima()
    {
        return $this->cotaMaxima;
    }

    public function setCotaMaxima($cotaMaxima)
    {
        $this->cotaMaxima = $cotaMaxima;
    }

    public function getTiposEvento()
    {
        return $this->tiposEvento;
    }

    public function setTiposEvento($tiposEvento)
    {
        $this->tiposEvento = $tiposEvento;
    }

    public function getEventosParametro()
    {
        return $this->eventosParametro;
    }

    public function setEventosParametro($eventosParametro)
    {
        $this->eventosParametro = $eventosParametro;
    }

    public function getCodename()
    {
        return $this->codename;
    }

    public function setCodename($codename)
    {
        $this->codename = strtoupper($codename);
    }

    public function getUnidadMedida()
    {
        return $this->unidadMedida;
    }

    public function setUnidadMedida($unidadMedida)
    {
        $this->unidadMedida = $unidadMedida;
    }
}
