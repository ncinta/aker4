<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\TareaOtProducto
 *
 * @ORM\Table(name="tareaot_producto")
 * @ORM\Entity(repositoryClass="App\Repository\TareaOtProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TareaOtProducto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\Column(name="costo_unitario", type="float", nullable=true)
     */
    private $costoUnitario;

    /**
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @ORM\Column(name="costo_neto", type="float", nullable=true)
     */
    private $costoNeto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TareaOt", inversedBy="productos")
     * @ORM\JoinColumn(name="tareaot_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $tarea;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="tareas")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Deposito", inversedBy="productosTareaUsados")
     * @ORM\JoinColumn(name="deposito_id", referencedColumnName="id",nullable=true)
     */
    protected $deposito;

    function getId()
    {
        return $this->id;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
    function getCantidad()
    {
        return $this->cantidad;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }


    function getCostoUnitario()
    {
        return $this->costoUnitario;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function setCostoUnitario($costoUnitario)
    {
        $this->costoUnitario = $costoUnitario;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function getCostoNeto()
    {
        return $this->costoNeto;
    }

    function setCostoNeto($costoNeto)
    {
        $this->costoNeto = $costoNeto;
    }

    function getTarea()
    {
        return $this->tarea;
    }

    function setTarea($tarea)
    {
        $this->tarea = $tarea;
    }
}
