<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * App\Entity\GrupoServicio
 *
 * @ORM\Table(name="gruposervicio")
 * @ORM\Entity(repositoryClass="App\Repository\GrupoServicioRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class GrupoServicio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="gruposServicio")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organizacion;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Servicio", mappedBy="grupos")
     */
    private $servicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IndiceGs", mappedBy="grupoServicio",cascade={"persist","remove"})
     */
    protected $indiceGs;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function __construct()
    {
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set organizacion
     *
     * @param App\Entity\Organizacion $organizacion
     */
    public function setOrganizacion(\App\Entity\Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get organizacion
     *
     * @return \App\Entity\Organizacion 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Add servicios
     *
     * @param App\Entity\Servicio $servicios
     */
    public function addServicio(\App\Entity\Servicio $servicios)
    {
        $this->servicios->add($servicios);
    }

    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
        return $this->servicios;
    }

    /**
     * Get servicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    public function getIndiceGs()
    {
        return $this->indiceGs;
    }

    public function setIndiceGs($indiceGs): void
    {
        $this->indiceGs = $indiceGs;
    }
}
