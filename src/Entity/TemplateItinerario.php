<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="templateItinerario")
 * @ORM\Entity(repositoryClass="App\Repository\TemplateItinerarioRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class TemplateItinerario
{



    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;



    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="data", type="text", nullable=true)
     * 
     */
    private $data;

    /**
     * @ORM\Column(name="bloqueado", type="integer", nullable=true)
     * 
     */
    private $bloqueado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="templateItinerarios")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    private $organizacion;


    //==========================================================================    
    //==========================================================================    
    //==========================================================================

    function __toString()
    {
        return $this->codigo;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }



    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of created_at
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get the value of organizacion
     */ 
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set the value of organizacion
     *
     * @return  self
     */ 
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }

    /**
     * Get the value of bloqueado
     */ 
    public function getBloqueado()
    {
        return $this->bloqueado;
    }

    /**
     * Set the value of bloqueado
     *
     * @return  self
     */ 
    public function setBloqueado($bloqueado)
    {
        $this->bloqueado = $bloqueado;

        return $this;
    }
}
