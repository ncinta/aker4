<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;


/**
 * App\Entity\Permiso
 * @author Claudio Brandolin <cbrandolin@securityconsultant.com.ar>
 *
 * @ORM\Table(name="permiso", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="codename_idx", columns={"codename"})
 * })
 * @Unique\UniqueEntity(fields={"codename"})
 * @ORM\Entity(repositoryClass="App\Repository\PermisoRepository")
 */
class Permiso
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="permiso_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string $codename
     *
     * @ORM\Column(name="codename", type="string", nullable=false, unique=true)
     * @Assert\NotBlank
     */
    private $codename;

    /**
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string", nullable=true)
     * @Assert\notBlank
     */
    private $descripcion;

    /**
     * Indica si este permiso es usuable o visible por los usuarios. Normalmente todos
     * los permisos son visibles pero hay algunos que solo son usados por el sistema, por 
     * ejemplo los ROLE_MASTER.
     * @ORM\Column(name="usable", type="boolean", nullable=true)
     */
    private $usable;

    /**
     * @var string Indica cuales son los roles que va a tener acceso a ver y/o asignar este permiso
     * @ORM\Column(name="restringido", type="string", nullable=true)
     */
    private $restringido;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", mappedBy="permisos", cascade={"persist"})
     */
    private $usuarios;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modulo", inversedBy="permisos")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $modulo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Perfil", inversedBy="permisos")
     * @ORM\JoinTable(name="permisoPerfil") 
     */
    private $perfiles;

    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->perfiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        if (is_null($this->descripcion)) {
            return $this->codename;
        } else {
            return $this->descripcion;
        }
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Permiso
     *
     * @param string $Permiso
     */
    public function setCodename($codename)
    {
        $this->codename = strtoupper($codename);
    }

    /** set Descripcion
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * Get Permiso
     *
     * @return string $Permiso
     */
    public function getCodename()
    {
        return $this->codename;
    }

    /**
     * @return string $descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
    }

    public function getModulo()
    {
        return $this->modulo;
    }

    public function setPerfiles($perfiles)
    {
        $this->perfiles = $perfiles;
    }

    public function getPerfiles()
    {
        return $this->perfiles;
    }

    public function setUsable($usable)
    {
        $this->usable = $usable;
    }

    public function isUsable()
    {
        return (bool) $this->usable;
    }

    /**
     * Retorna el role que va a tener acceso para visualizar este permiso
     * @return string
     */
    public function getRestringido()
    {
        return $this->restringido;
    }

    /**
     * Permite setear el role que va a poder modificar este permiso. Sino se setea nada
     * todos los usuarios pueden visualizar este permiso de lo contrario solo es visto
     * por aquello que tengan en su perfil el role que aqui figura.
     * @param string $restringido
     */
    public function setRestringido($restringido)
    {
        $this->restringido = $restringido;
    }

    /**
     * Get the value of usuarios
     */ 
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Set the value of usuarios
     *
     * @return  self
     */ 
    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;

        return $this;
    }
}
