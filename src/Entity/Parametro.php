<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Parametro
 *
 * @ORM\Table(name="parametro")
 * @ORM\Entity(repositoryClass="App\Repository\ParametroRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Parametro
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $valorActual
     *
     * @ORM\Column(name="valor_actual", type="string", length=255, nullable=true)
     */
    private $valor_actual;

    /**
     * @var string $valorAnterior
     *
     * @ORM\Column(name="valor_anterior", type="string", length=255, nullable=true)
     */
    private $valor_anterior;

    /**
     * @var string $valorFuturo
     * @ORM\Column(name="valor_futuro", type="string", length=255, nullable=true)
     */
    private $valor_futuro;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var datetime $ult_programacion
     *
     * @ORM\Column(name="ult_seteo", type="datetime", nullable=true)
     */
    private $ult_seteo;

    /**
     * @var datetime $ult_programacion
     *
     * @ORM\Column(name="ult_programacion", type="datetime", nullable=true)
     */
    private $ult_programacion;

    /**
     * Mantiene la cantidad de intentos de programacion que se han realizado.
     * @var integer $intentos
     *
     * @ORM\Column(name="intentos", type="integer", nullable=true)
     */
    private $intentos;

    /**
     * Semaforo que indica si el param esta siendo procesado
     * @var integer $intentos
     *
     * @ORM\Column(name="block", type="boolean", nullable=true)
     */
    private $block;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Variable", inversedBy="parametros", cascade={"persist"})
     * @ORM\JoinColumn(name="variable_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $variable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="parametros", cascade={"persist"})
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variable
     *
     * @param App\Entity\Variable $variable
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;
    }

    /**
     * Get variable
     *
     * @return App\Entity\Variable 
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function getValorActual()
    {
        return $this->valor_actual;
    }

    public function setValorActual($valor_actual)
    {
        $this->valor_actual = $valor_actual;
        return $this;
    }

    public function getValorAnterior()
    {
        return $this->valor_anterior;
    }

    public function setValorAnterior($valor_anterior)
    {
        $this->valor_anterior = $valor_anterior;
        return $this;
    }

    public function getValorFuturo()
    {
        return $this->valor_futuro;
    }

    public function setValorFuturo($valor_futuro)
    {
        $this->valor_futuro = $valor_futuro;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }

    public function getUltProgramacion()
    {
        return $this->ult_programacion;
    }

    public function setUltProgramacion($ult_programacion)
    {
        $this->ult_programacion = $ult_programacion;
        return $this;
    }

    public function getUltSeteo()
    {
        return $this->ult_seteo;
    }

    public function setUltSeteo($ult_seteo)
    {
        $this->ult_seteo = $ult_seteo;
        return $this;
    }

    public function getIntentos()
    {
        return $this->intentos;
    }

    public function setIntentos($intentos)
    {
        $this->intentos = $intentos;
        return $this;
    }

    public function isBlock()
    {
        if (is_null($this->block)) {
            return false;
        } else {
            return $this->block;
        }
    }

    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }
}
