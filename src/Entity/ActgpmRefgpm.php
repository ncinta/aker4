<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\ActividadgpmReferenciagpm
 *
 * @ORM\Table(name="actividadgpm_referenciagpm")
 * @ORM\Entity(repositoryClass="App\Repository\ActgpmRefgpmRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActgpmRefgpm
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ReferenciaGpm", inversedBy="actgpmRefgpm")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $referencia;

    /**
     * @ORM\ManyToOne(targetEntity="ActividadGpm", inversedBy="actgpmRefgpm")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $actividad;

    function getId()
    {
        return $this->id;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }
}
