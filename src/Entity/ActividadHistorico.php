<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ActividadProducto
 *
 * @author nicolas
 */

/**
 * App\Entity\ActividadProducto
 *
 * @ORM\Table(name="actividad_historico")
 * @ORM\Entity(repositoryClass="App\Repository\ActividadHistoricoRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActividadHistorico
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * cantidad del producto empleada a la hora de registrar el mantenimiento
     * @var integer $fecha
     *
     * @ORM\Column(name="cantidad", type="float")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Actividad", inversedBy="historicos",cascade={"persist"})
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    protected $actividad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="historicos")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServicioMantenimientoHistorico", inversedBy="historicoActividad")
     * @ORM\JoinColumn(name="serviciomantenimientohist_id", referencedColumnName="id")
     */
    protected $historico;


    function getId()
    {
        return $this->id;
    }

    function getCantidad()
    {
        return $this->cantidad;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function getHistorico()
    {
        return $this->historico;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }

    function setHistorico($historico)
    {
        $this->historico = $historico;
    }
}
