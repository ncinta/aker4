<?php

//Clase que especifica si un servicio es vehicular o persona que porta el equipo

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\TipoServicio
 *
 * @ORM\Table(name="tiposervicio")
 * @ORM\Entity
 */
class TipoServicio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path_icono", type="string", nullable=true)
     */
    private $pathIcono;

    /**
     * @ORM\Column(name="velocidad_maximo", type="integer", nullable=true)
     */
    private $velocidadMaxima;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $icono;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Servicio", mappedBy="tipoServicio")
     */
    protected $servicios;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    function getVelocidadMaxima()
    {
        return $this->velocidadMaxima;
    }

    function setVelocidadMaxima($velocidadMaxima)
    {
        $this->velocidadMaxima = $velocidadMaxima;
        return $this;
    }


    public function __construct()
    {
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add servicios
     *
     * @param App\Entity\Servicio $servicios
     */
    public function addServicio(\App\Entity\Servicio $servicios)
    {
        $this->servicios[] = $servicios;
    }

    /**
     * Get servicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Set path_icono
     *
     * @param string $path
     */
    public function setPathIcono($path)
    {
        $this->pathIcono = $path;
    }

    /**
     * Get path_icono
     *
     * @return string 
     */
    public function getPathIcono()
    {
        return $this->pathIcono;
    }

    public function getAbsolutePath()
    {
        return null === $this->pathIcono ? null : $this->getUploadRootDir() . '/' . $this->pathIcono;
    }

    public function getWebPath()
    {
        return null === $this->pathIcono ? null : $this->getUploadDir() . '/' . $this->pathIcono;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'images';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->icono) {
            return;
        }

        $extension = $this->icono->guessExtension();
        if (!$extension) {
            // extension cannot be guessed
            $extension = 'bin';
        }

        $randomName = rand(1, 99999) . '.' . $extension;

        $this->icono->move($this->getUploadRootDir(), $randomName);
        // move takes the target directory and then the target filename to move to
        //$this->icono->move($this->getUploadRootDir(), $this->icono->getClientOriginalName());
        // set the path property to the filename where you'ved saved the file
        $this->pathIcono = $randomName;

        // clean up the file property as you won't need it anymore
        $this->icono = null;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
