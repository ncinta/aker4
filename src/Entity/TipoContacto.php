<?php
//Clase que especifica si un contacto es telefonico o mail
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Tipo_contacto
 *
 * @ORM\Table(name="tipocontacto")
 * @ORM\Entity
 */
class TipoContacto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacto", mappedBy="tipoContacto")
     */
    protected $contactos;

    public function __construct()
    {
        $this->contactos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Add contactos
     *
     * @param App\Entity\contacto $contactos
     */
    public function addcontacto(\App\Entity\contacto $contactos)
    {
        $this->contactos[] = $contactos;
    }

    /**
     * Get contactos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}
