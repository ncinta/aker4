<?php

namespace App\Entity;

use BeSimple\SoapBundle\ServiceDefinition\Annotation as Soap;

class ResponseDistancia
{

    /**
     * @Soap\ComplexType("int")
     */
    public $code;

    /**
     * @Soap\ComplexType("string")
     */
    public $message;

    /**
     * @Soap\ComplexType("int")
     */
    public $distancia;

    /**
     * @Soap\ComplexType("dateTime", nillable = true)
     */
    public $fecha_evento;
}
