<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\HistorialServicio
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class HistorialServicio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipo", inversedBy="historialServicios")
     * @ORM\JoinColumn(name="equipo_id", referencedColumnName="id")
     */
    protected $equipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="historialServicios")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id")
     */
    protected $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }


    /**
     * Set equipo
     *
     * @param App\Entity\Equipo $equipo
     */
    public function setEquipo(\App\Entity\Equipo $equipo)
    {
        $this->equipo = $equipo;
    }

    /**
     * Get equipo
     *
     * @return App\Entity\Equipo 
     */
    public function getEquipo()
    {
        return $this->equipo;
    }

    /**
     * Set servicio
     *
     * @param App\Entity\Servicio $servicio
     */
    public function setServicio(\App\Entity\Servicio $servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * Get servicio
     *
     * @return App\Entity\Servicio 
     */
    public function getServicio()
    {
        return $this->servicio;
    }
}
