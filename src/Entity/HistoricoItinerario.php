<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Servicio as Servicio;
use App\Entity\Usuario as Usuario;
use App\Entity\Evento as Evento;
use App\Entity\EventoTemporal as EventoTemporal;

/**
 *
 * @ORM\Table(name="historico_itinerario", indexes={@Index(name="fecha_idhi", columns={"fecha"})})
 * @ORM\Entity(repositoryClass="App\Repository\HistoricoItinerarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HistoricoItinerario
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titulo
     *
     * @ORM\Column(name="titulo", type="string")
     */
    private $titulo;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var time $fechaVista
     *
     * @ORM\Column(name="fecha_vista", type="datetime", nullable=true)
     */
    private $fecha_vista;

    /**
     * @ORM\Column(name="respuesta", type="text", nullable=true)
     */
    private $respuesta;

    /**
     * @var float $latitud
     *
     * @ORM\Column(name="latitud", type="float", nullable=true)
     */
    private $latitud;

    /**
     * @var float $longitud
     *
     * @ORM\Column(name="longitud", type="float", nullable=true)
     */
    private $longitud;

    /**
     * @var Ejecutor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="eventos")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventoItinerario", inversedBy="historicoItinerario")     
     * @ORM\JoinColumn(name="eventoitinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $eventoItinerario;

    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="historicoItinerario")     
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $servicio;

    /**
     * @var Evento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Evento", inversedBy="historicoItinerario")     
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=true, onDelete="SET NULL"))
     */
    private $evento;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="historicoItinerario", cascade={"all"})
     */
    protected $eventoTemporal;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraItinerario", mappedBy="historicoItinerario", cascade={"persist"})
     */
    protected $bitacoraItinerario;

    /**
     * @var string
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        //        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->titulo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getFechaVista()
    {
        return $this->fecha_vista;
    }

    public function getRespuesta()
    {
        return $this->respuesta;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setFechaVista($fecha_vista)
    {
        $this->fecha_vista = $fecha_vista;
        return $this;
    }

    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
        return $this;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
        return $this;
    }


    /**
     * Get the value of eventoItinerario
     *
     * @return  Evento
     */
    public function getEventoItinerario()
    {
        return $this->eventoItinerario;
    }

    /**
     * Set the value of eventoItinerario
     */
    public function setEventoItinerario($eventoItinerario)
    {
        $this->eventoItinerario = $eventoItinerario;

        return $this;
    }

    /**
     * Get the value of bitacoraItinerario
     */
    public function getBitacoraItinerario()
    {
        return $this->bitacoraItinerario;
    }

    /**
     * Set the value of bitacoraItinerario
     *
     * @return  self
     */
    public function setBitacoraItinerario($bitacoraItinerario)
    {
        $this->bitacoraItinerario = $bitacoraItinerario;

        return $this;
    }

    /**
     * Get the value of evento
     *
     * @return  Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set the value of evento
     *
     * @param  Evento  $evento
     *
     * @return  self
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;

        return $this;
    }

    public function removeEvento()
    {
        $this->evento = null;
    }
    function getLatitud()
    {
        return $this->latitud;
    }

    function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    function getLongitud()
    {
        return $this->longitud;
    }
    function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    /**
     * Get the value of eventoTemporal
     */ 
    public function getEventoTemporal()
    {
        return $this->eventoTemporal;
    }

    /**
     * Set the value of eventoTemporal
     *
     * @return  self
     */ 
    public function setEventoTemporal($eventoTemporal)
    {
        $this->eventoTemporal = $eventoTemporal;

        return $this;
    }
}
