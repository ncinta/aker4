<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\ConfiguracionDistribuidor
 *
 * @ORM\Table(name="configuraciondistribuidor")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks() 
 */
class ConfiguracionDistribuidor
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $pathLogo
     *
     * @ORM\Column(name="pathLogo", type="string", nullable=true)
     */
    private $pathLogo;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $logo;

    /**
     * @var string $copyright
     *
     * @ORM\Column(name="copyright", type="text", nullable=true)
     */
    private $copyright;

    /**
     * @ORM\Column(name="css", type="string", nullable=true)
     */
    private $css;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * Dominio. Indica el dominio al que se debe ver cuando antes del login para 
     * determinar el logo del distribuidor.
     * 
     * @ORM\Column(name="dominio", type="string", nullable=true)
     */
    protected $dominio;

    /**
     * es un sha512 que debe ser pasado en cada peticion a webservice para poder 
     * utilizar los service.
     * @var type string
     * @ORM\Column(name="key_webservice", type="string", nullable=true)
     */
    protected $key_webservice;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pathLogo
     *
     * @param string $pathLogo
     */
    public function setPathLogo($pathLogo)
    {
        $this->pathLogo = basename($pathLogo);
    }

    /**
     * Get pathLogo
     *
     * @return string 
     */
    public function getPathLogo()
    {
        return $this->pathLogo;
    }

    /**
     * Set copyright
     *
     * @param string $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * Get copyright
     *
     * @return string 
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set css
     *
     * @param text $css
     */
    public function setCss($css)
    {
        $this->css = $css;
    }

    /**
     * Get css
     *
     * @return text 
     */
    public function getCss()
    {
        return $this->css;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getAbsolutePath()
    {
        return null === $this->pathLogo ? null : $this->getUploadRootDir() . '/' . $this->pathLogo;
    }

    public function getWebPath()
    {
        return null === $this->pathLogo ? null : $this->getUploadDir() . '/' . $this->pathLogo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return '/images/logos';
    }

    public function getFilePath()
    {
        return null === $this->pathLogo ? null : $this->getUploadUrl() . '/' . $this->pathLogo;
    }

    protected function getUploadUrl()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'images/logos';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->logo) {
            return;
        }

        // move takes the target directory and then the target filename to move to
        $this->logo->move($this->getUploadRootDir(), $this->logo->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->pathLogo = $this->logo->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->logo = null;
    }

    public function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    public function getDominio()
    {
        return $this->dominio;
    }

    public function setKeywebservice($key_webservice)
    {
        $this->key_webservice = $key_webservice;
    }

    public function getKeywebservice()
    {
        return $this->key_webservice;
    }
}
