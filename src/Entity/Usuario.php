<?php

// src/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Organizacion;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="usuario_idx", columns={"username"}),
 *      @ORM\UniqueConstraint(name="usuario_idx1", columns={"email"})
 * })
 * @Unique\UniqueEntity(fields={"username"}, message="Ya hay un usuario con nombre")
 * @Unique\UniqueEntity(fields={"email"}, message="Ya hay un usuario con este email")
 * @ORM\HasLifecycleCallbacks() 
 */
class Usuario extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
        $this->permisos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        return $this->roles = $roles;
    }

    public function eraseCredentials()
    {
    }

    //**************************************************
    //EMPIEZAN LOS FIELD PERSONALIZADOS.
    //**************************************************

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="time_zone", type="string", nullable=true)
     */
    protected $timeZone;

    /**
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    protected $nombre;

    /**
     * @ORM\Column(name="vista_mapa", type="integer", nullable=true)
     */
    protected $vista_mapa;

    /**
     * @var boolean $change_password
     *
     * @ORM\Column(name="change_password", type="boolean", nullable=true)
     */
    protected $change_password;

    /**
     * @ORM\Column(name="change_password_every", type="integer", nullable=true)
     */
    protected $change_password_every;

    /**
     * @ORM\Column(name="last_change_password_at", type="datetime", nullable=true)
     */
    protected $last_change_password_at;

    /**
     * @ORM\Column(name="redirigir_login", type="integer", nullable=true)
     */
    protected $redirigir_login;

    /**
     * @var integer $limite_historial
     *
     * @ORM\Column(name="limite_historial", type="integer", nullable=true)
     */
    protected $limite_historial;

    /**
     * @var integer $ultEventoVisto
     *
     * @ORM\Column(name="ult_eventovisto", type="integer", nullable=true)
     */
    protected $ultEventoVisto;

    /**
     * @ORM\Column(name="telefono", type="string", nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(name="api_code", type="string", nullable=true)
     */
    protected $apiCode;

    /**
     * @ORM\Column(name="token", type="string", nullable=true)
     */
    protected $token;


    /**
     * @ORM\Column(name="ult_ip", type="string", nullable=true)
     */
    protected $ultIp;

    /**
     * @ORM\Column(name="ult_host", type="string", nullable=true)
     */
    protected $ultHost;

    /**
     * @ORM\Column(name="mostrar_referencias", type="boolean", nullable=true)
     */
    protected $showReferencias;

    /**
     * @var string $algorithm
     *
     * @ORM\Column(name="algorithm", type="string", nullable=true)
     */
    protected $algorithm;

    /** CAMPOS HEREDADOS DE CALYPSO * */

    /**
     * @var boolean $locked
     *
     * @ORM\Column(name="locked", type="boolean", nullable=true)
     */
    protected $locked;

    /**
     * @var boolean $expired
     *
     * @ORM\Column(name="expired", type="boolean", nullable=true)
     */
    protected $expired;

    /**
     * @var boolean $credentialsExpired
     *
     * @ORM\Column(name="credentials_expired", type="boolean", nullable=true)
     */
    protected $credentialsExpired;

    /**
     * @var datetime $credentialsExpireAt
     * @ORM\Column(name="credentials_expire_at", type="datetime", nullable=true)
     */
    protected $credentialsExpireAt;

    /**
     * @var datetime $expiresAt
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @var boolean $change_password
     *
     * @ORM\Column(name="notificar_peticion", type="boolean", nullable=true)
     */
    protected $notificar_peticion;

    /**

     * @var json

     * @ORM\Column(name="data", type="array", nullable=true)

     */

    protected $data;

    /** FIN CAMPOS HEREDADOS * */

    /**
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="usuarios")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bitacora", mappedBy="ejecutor");
     */
    protected $bitacora;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimientoHistorico", mappedBy="ejecutor");
     */
    protected $mantenimientosHistorico;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="ejecutor");
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Panico", mappedBy="ejecutor");
     */
    protected $panicos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoHistorial", mappedBy="ejecutor");
     */
    protected $eventos;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="usuario")
     */
    protected $eventosTemporal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Empresa", inversedBy="usuarios")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Logistica", inversedBy="usuarios")
     * @ORM\JoinColumn(name="logistica_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $logistica;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Permiso", inversedBy="usuarios", cascade={"persist"})
     * @ORM\JoinTable(name="usuario_permiso",
     *   joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="permiso_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $permisos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Perfil", inversedBy="usuarios")
     * @ORM\JoinColumn(name="perfil_id", referencedColumnName="id")
     */
    protected $perfil;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\GrupoReferencia", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuarioGrupoReferencia",
     *   joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="gruporeferencia_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $gruposReferencias;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Modulo", inversedBy="usuarios")
     * @ORM\JoinTable(name="usuarioModulo")
     */
    protected $modulos;

    /**
     * Inverse Side
     * @ORM\ManyToMany(targetEntity="App\Entity\Itinerario", mappedBy="usuarios", cascade={"persist"})
     * @ORM\JoinTable(name="itinerarioUsuario")
     */
    protected $itinerarios;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Servicio", inversedBy="usuarios")
     * @ORM\JoinTable(name="servicioUsuarios",
     *      joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $servicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="autor")
     */
    protected $peticionesAutor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="asignado_a")
     */
    protected $peticionesAsignadas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraPeticion", mappedBy="ejecutor");
     */
    protected $bitacoraPeticiones;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Peticion", mappedBy="seguidores", cascade={"persist"})
     */
    protected $peticiones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comentario", mappedBy="usuario")
     */
    protected $comentarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CargaCombustible", mappedBy="usuario")
     */
    protected $cargasCombustible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtComentario", mappedBy="ejecutor");
     */
    protected $tareaotComentario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transporte", inversedBy="usuarios")
     * @ORM\JoinColumn(name="transporte_id", nullable=true, referencedColumnName="id", onDelete="SET NULL")
     */
    protected $transporte;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\informe\Inbox", mappedBy="autor")
     */
    protected $informesInbox;


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function _toString()
    {
        return $this->username;
    }

    function getId()
    {
        return $this->id;
    }

    function getCreatedAt()
    {
        return $this->createdAt;
    }

    function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    function getTimeZone()
    {
        return $this->timeZone;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getVistaMapa()
    {
        return $this->vista_mapa;
    }

    function getChangePassword()
    {
        return $this->change_password;
    }

    function getChangePasswordEvery()
    {
        return $this->change_password_every;
    }

    function getLastChangePasswordAt()
    {
        return $this->last_change_password_at;
    }

    function getRedirigirLogin()
    {
        return $this->redirigir_login;
    }

    function getLimiteHistorial()
    {
        return $this->limite_historial;
    }

    function getShowReferencias()
    {
        return $this->showReferencias;
    }

    function getAlgorithm()
    {
        return $this->algorithm;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getBitacora()
    {
        return $this->bitacora;
    }

    function getMantenimientosHistorico()
    {
        return $this->mantenimientosHistorico;
    }

    function getPanicos()
    {
        return $this->panicos;
    }

    function getEventos()
    {
        return $this->eventos;
    }

    function getPermisos()
    {
        return $this->permisos;
    }

    function getPerfil()
    {
        return $this->perfil;
    }

    function getModulos()
    {
        return $this->modulos;
    }

    function getServicios()
    {
        return $this->servicios;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setVistaMapa($vista_mapa)
    {
        $this->vista_mapa = $vista_mapa;
    }

    function setChangePassword($change_password)
    {
        $this->change_password = $change_password;
    }

    function setChangePasswordEvery($change_password_every)
    {
        $this->change_password_every = $change_password_every;
    }

    function setLastChangePasswordAt($last_change_password_at)
    {
        $this->last_change_password_at = $last_change_password_at;
    }

    function setRedirigirLogin($redirigir_login)
    {
        $this->redirigir_login = $redirigir_login;
    }

    function setLimiteHistorial($limite_historial)
    {
        $this->limite_historial = $limite_historial;
    }

    function setShowReferencias($showReferencias)
    {
        $this->showReferencias = $showReferencias;
    }

    function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;
    }

    function setMantenimientosHistorico($mantenimientosHistorico)
    {
        $this->mantenimientosHistorico = $mantenimientosHistorico;
    }

    function setPanicos($panicos)
    {
        $this->panicos = $panicos;
    }

    function setEventos($eventos)
    {
        $this->eventos = $eventos;
    }

    function setPermisos($permisos)
    {
        $this->permisos = $permisos;
    }

    public function addPermiso($permiso)
    {
        $this->permisos[] = $permiso;
    }

    public function removePermiso($permiso)
    {
        $this->permisos->removeElement($permiso);
        return $this->permisos;
    }


    function setPerfil($perfil)
    {
        $this->perfil = $perfil;
    }

    function setModulos($modulos)
    {
        $this->modulos = $modulos;
    }

    function setServicios($servicios)
    {
        $this->servicios = $servicios;
    }

    public function isMaster()
    {
        if ($this->organizacion != null) {
            if ($this->id === $this->getOrganizacion()->getUsuarioMaster()->getId()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getVars()
    {
        $vars = get_object_vars($this);
        unset($vars['organizacion']);
        unset($vars['bitacora']);
        return $vars;
    }

    function getTelefono()
    {
        return $this->telefono;
    }

    function getApiCode()
    {
        return $this->apiCode;
    }

    function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function setApiCode($apiCode)
    {
        $this->apiCode = $apiCode;
    }

    function getGruposReferencias()
    {
        return $this->gruposReferencias;
    }

    function setGruposReferencias($gruposReferencias)
    {
        $this->gruposReferencias = $gruposReferencias;
    }

    /**
     * Add gruporeferencias
     *
     */
    public function addGrupoReferencias($gruporeferencia)
    {
        $this->gruposReferencias->add($gruporeferencia);
    }

    function getUltIp()
    {
        return $this->ultIp;
    }

    function getUltHost()
    {
        return $this->ultHost;
    }

    function setUltIp($ultIp)
    {
        $this->ultIp = $ultIp;
    }

    function setUltHost($ultHost)
    {
        $this->ultHost = $ultHost;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function getPeticionesAutor()
    {
        return $this->peticionesAutor;
    }

    function getPeticionesAsignadas()
    {
        return $this->peticionesAsignadas;
    }

    function getBitacoraPeticiones()
    {
        return $this->bitacoraPeticiones;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function getComentarios()
    {
        return $this->comentarios;
    }

    function setPeticionesAutor($peticionesAutor)
    {
        $this->peticionesAutor = $peticionesAutor;
    }

    function setPeticionesAsignadas($peticionesAsignadas)
    {
        $this->peticionesAsignadas = $peticionesAsignadas;
    }

    function setBitacoraPeticiones($bitacoraPeticiones)
    {
        $this->bitacoraPeticiones = $bitacoraPeticiones;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }

    function getNotificarPeticion()
    {
        return $this->notificar_peticion;
    }

    function setNotificarPeticion($notificar_peticion)
    {
        $this->notificar_peticion = $notificar_peticion;
    }

    function getItinerarios()
    {
        return $this->itinerarios;
    }

    function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;
    }

    /**
     * Add servicios
     */
    public function addServicio($servicio)
    {
        $this->servicios->add($servicio);
    }

    //borra la asociacion usuario - servicio.
    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
        return $this;
    }

    /**
     * Remove gruporeferencias
     *
     */
    public function removeGrupoReferencias($gruporeferencia)
    {
        $this->gruposReferencias->removeElement($gruporeferencia);
        return $this;
    }

    function getEnabled()
    {
        return $this->enabled;
    }

    function getLastLogin()
    {
        return $this->lastLogin;
    }

    function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    function setCargasCombustible($cargasCombustible)
    {
        $this->cargasCombustible = $cargasCombustible;
    }

    function getTareaotComentario()
    {
        return $this->tareaotComentario;
    }

    function setTareaotComentario($tareaotComentario)
    {
        $this->tareaotComentario = $tareaotComentario;
    }

    /**
     * Get the value of eventosTemporal
     */
    public function getEventosTemporal()
    {
        return $this->eventosTemporal;
    }

    /**
     * Set the value of eventosTemporal
     *
     * @return  self
     */
    public function setEventosTemporal($eventosTemporal)
    {
        $this->eventosTemporal = $eventosTemporal;

        return $this;
    }

    public function addEvTemporal($evento)
    {
        $this->eventosTemporal[] = $evento;
    }

    /**
     * Get the value of empresas
     */
    public function getEmpresas()
    {
        return $this->empresas;
    }

    /**
     * Set the value of empresas
     *
     * @return  self
     */
    public function setEmpresas($empresas)
    {
        $this->empresas = $empresas;

        return $this;
    }

    /**
     * Get the value of transporte
     */
    public function getTransporte()
    {
        return $this->transporte;
    }

    /**
     * Set the value of transporte
     *
     * @return  self
     */
    public function setTransporte($transporte)
    {
        $this->transporte = $transporte;

        return $this;
    }

    /**
     * Get the value of empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set the value of empresa
     *
     * @return  self
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get the value of logistica
     */
    public function getLogistica()
    {
        return $this->logistica;
    }

    /**
     * Set the value of logistica
     *
     * @return  self
     */
    public function setLogistica($logistica)
    {
        $this->logistica = $logistica;
    }

    /**
     * Get the value of token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get $ultEventoVisto
     *
     * @return  integer
     */
    public function getUltEventoVisto()
    {
        return $this->ultEventoVisto;
    }

    /**
     * Set $ultEventoVisto
     *
     * @param  integer  $ultEventoVisto  $ultEventoVisto
     *
     * @return  self
     */
    public function setUltEventoVisto($ultEventoVisto)
    {
        $this->ultEventoVisto = $ultEventoVisto;

        return $this;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function addAccesoReingenio($hash)
    {
        $this->data['hash_reingenio'] = $hash;
    }

    public function removeAccesoReingenio()
    {
        unset($this->data['hash_reingenio']);
    }

    /**
     * Get the value of informesInbox
     */ 
    public function getInformesInbox()
    {
        return $this->informesInbox;
    }

    /**
     * Set the value of informesInbox
     *
     * @return  self
     */ 
    public function setInformesInbox($informesInbox)
    {
        $this->informesInbox = $informesInbox;

        return $this;
    }
}
