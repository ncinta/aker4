<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ActividadProducto
 *
 * @author nicolas
 */

/**
 * App\Entity\ActividadProducto
 *
 * @ORM\Table(name="actividad_producto")
 * @ORM\Entity(repositoryClass="App\Repository\ActividadProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActividadProducto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $fecha
     *
     * @ORM\Column(name="cantidad", type="float")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Actividad", inversedBy="productos",cascade={"persist"})
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    protected $actividad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="actividades")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    function getId()
    {
        return $this->id;
    }

    function getCantidad()
    {
        return $this->cantidad;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
}
