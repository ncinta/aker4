<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Producto
 *
 * @ORM\Table(name="producto_serviciomanthist")
 * @ORM\Entity(repositoryClass="App\Repository\ProductoServicioMantHistRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductoServicioMantHist
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServicioMantenimientoHistorico", inversedBy="historicos")
     * @ORM\JoinColumn(name="serviciomanthist_id", referencedColumnName="id",nullable=true)
     */
    protected $mantenimiento;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="productoHistoricos")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;



    function getId()
    {
        return $this->id;
    }

    function getMantenimiento()
    {
        return $this->mantenimiento;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setMantenimiento($mantenimiento)
    {
        $this->mantenimiento = $mantenimiento;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
    function getCantidad()
    {
        return $this->cantidad;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }
}
