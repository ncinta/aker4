<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of HistoricoIbutton
 *
 * @author nicolas
 */

/**
 * App\Entity\HistoricoIbutton
 *
 * @ORM\Table(name="historico_ibutton")
 * @ORM\Entity(repositoryClass="App\Repository\HistoricoIbuttonRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HistoricoIbutton
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $desde
     *
     * @ORM\Column(name="desde", type="datetime", nullable=true)
     */
    private $desde;

    /**
     * @var datetime $hasta
     *
     * @ORM\Column(name="hasta", type="datetime", nullable=true)
     */
    private $hasta;

    /**
     * @var string $motivo
     *
     * @ORM\Column(name="motivo", type="text", nullable=true)
     */
    private $motivo;

    /**
     * @var Chofer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="historicoibutton")     
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $chofer;

    /**
     * @var Ibutton
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Ibutton", inversedBy="historicoibutton")     
     * @ORM\JoinColumn(name="ibutton_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ibutton;
    function getId()
    {
        return $this->id;
    }

    function getDesde()
    {
        return $this->desde;
    }

    function getHasta()
    {
        return $this->hasta;
    }

    function getChofer()
    {
        return $this->chofer;
    }

    function getIbutton()
    {
        return $this->ibutton;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDesde($desde)
    {
        $this->desde = $desde;
    }

    function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    function setChofer(Chofer $chofer)
    {
        $this->chofer = $chofer;
    }

    function setIbutton(Ibutton $ibutton)
    {
        $this->ibutton = $ibutton;
    }
    function getMotivo()
    {
        return $this->motivo;
    }

    function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }
}
