<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Contacto
 *
 * @ORM\Table(name="config_gpm")
 * @ORM\Entity(repositoryClass="App\Repository\ConfigGpmRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class ConfigGpm
{

    const CONFIG_NIVEL_TENSION = FALSE;
    const CONFIG_HECTAREAS = FALSE;
    const CONFIG_CORTE_ENERGIA = FALSE;
    const CONFIG_PERSONAL = FALSE;
    const CONFIG_CONTROL_MANUAL = FALSE;

    private $arrConfig = array(
        'NIVEL_TENSION' => array('label' => 'Usa nivel de tensión?', 'value' => self::CONFIG_NIVEL_TENSION),
        'HECTAREA' =>  array('label' => 'Usa hectareas?', 'value' => self::CONFIG_HECTAREAS),
        'CORTE_ENERGIA' =>  array('label' => 'Usa corte de energia?', 'value' => self::CONFIG_CORTE_ENERGIA),
        'PERSONAL' =>  array('label' => 'Usa personal?', 'value' => self::CONFIG_PERSONAL),
        'CONTROL_MANUAL' =>  array('label' => 'Usa control manual?', 'value' => self::CONFIG_CONTROL_MANUAL)
    );

    public function getArrayConfig()
    {
        return $this->arrConfig;
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Organizacion", inversedBy="configGpm")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\Column(name="vista_actividad", type="array", nullable=true)
     */
    private $vista_actividad;

    function getId()
    {
        return $this->id;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getVista_actividad()
    {
        return $this->vista_actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setVista_actividad($vista_actividad)
    {
        $this->vista_actividad = $vista_actividad;
    }
}
