<?php

namespace App\Entity;


class UnidadDeMedida {
    
    const METROS = 0;
    const KILOMETROS = 1;
    const KILOMETROSHORA = 2;
    const MINUTOS = 3;
     
    public static function getNombreUnidad($tipo) {
         $value = "";
        switch ($tipo) {
            case UnidadDeMedida::METROS:
                $value = "Metros";
                break;
            case UnidadDeMedida::KILOMETROS:
                $value = "Kilometros";
                break;
            case UnidadDeMedida::KILOMETROSHORA:
                $value = "Kilometros/Hora";
                break;
            case UnidadDeMedida::MINUTOS:
                $value = "Minutos";
                break;
            default:
                break;
        }

        return $value;
    }
}
