<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\Referencia
 *
 * @ORM\Table(name="referencia")
 * @ORM\Entity(repositoryClass="App\Repository\ReferenciaRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Referencia
{

    const CLASE_RADIAL = 1;
    const CLASE_POLIGONO = 2;
    const STATUS_NO_CHECK = 0;
    const STATUS_CHECK = 1;
    const STATUS_REDRAW = 2;


    private $arrayClase = array(
        self::CLASE_RADIAL => 'Radial',
        self::CLASE_POLIGONO => 'Poligonal',
    );

    private $arrayStatus = array(
        self::STATUS_NO_CHECK => 'No chequeada',
        self::STATUS_CHECK => 'Chequeada',
        self::STATUS_REDRAW => 'Re Dibujada',
    );


    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var float $latitud
     * @ORM\Column(name="latitud", type="float")
     * @Assert\Regex(
     *     pattern="/^[-+][0-9]+\.[0-9]+$/",
     *     message="No es una latitud válida."
     * )  
     */
    private $latitud;

    /**
     * @var float $longitud
     *
     * @ORM\Column(name="longitud", type="float")
     * @Assert\Regex(
     *     pattern="/^[-+][0-9]+\.[0-9]+$/",
     *     message="No es una longitud válida."
     * )
     */
    private $longitud;

    /**
     * @var text $descripcion
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var text $direccion
     *
     * @ORM\Column(name="direccion", type="text", nullable=true)
     */
    private $direccion;

    /**
     * @var float $radio
     *
     * @ORM\Column(name="radio", type="float", nullable=true)
     */
    private $radio;

    /**
     * @var float $velocidadMaxima
     *
     * @ORM\Column(name="velocidadMaxima", type="float", nullable=true)
     */
    private $velocidadMaxima;

    /**
     * @var float $cortePorRalenti
     *
     * @ORM\Column(name="cortePorRalenti", type="float", nullable=true)
     */
    private $cortePorRalenti;

    /**
     * @var boolean $entrada
     *
     * @ORM\Column(name="entrada", type="boolean", nullable = true)
     */
    private $entrada;

    /**
     * @var boolean $salida
     *
     * @ORM\Column(name="salida", type="boolean", nullable = true)
     */
    private $salida;

    /**
     * @var boolean $entrada
     *
     * @ORM\Column(name="visibilidad", type="boolean", nullable = true)
     */
    private $visibilidad;

    /**
     * @var float $anguloDeteccion
     *
     * @ORM\Column(name="anguloDeteccion", type="float", nullable = true)
     */
    private $anguloDeteccion;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="poligono", type="text", nullable = true)
     */
    private $poligono;

    /**
     * Indica la clase de dibujo que es la referencia.
     * 0 = no check
     * 1 = check.
     * 2 = re draw.
     * @ORM\Column(name="status", type="integer", nullable=true)   
     */
    private $status;


    /**
     * @Assert\File(maxSize="6000000")
     */
    private $kml;

    /**
     * @var text $codigo_externo
     *
     * @ORM\Column(name="codigo_externo", type="text", nullable=true)
     */
    private $codigo_externo;

    /**
     * Indica el color del relleno y linea del dibujo que se tiene que realizar
     * @ORM\Column(name="color", type="text", nullable=true)   
     */
    private $color;

    /**
     * Indica el numero (1..10) de transparencia para el dibujo de la referencia
     * @ORM\Column(name="transparencia", type="text", nullable=true)   
     */
    private $transparencia;

    /**
     * Indica la clase de dibujo que es la referencia.
     * 1 = Radio
     * 2 = Poligono.
     * @ORM\Column(name="clase", type="integer", nullable=true)   
     */
    private $clase;

    /**
     * @ORM\Column(name="area", type="float", nullable=true)
     */
    private $area;


    /** 
     * @ORM\Column(name="ciudad", type="string", nullable=true)
     */
    protected $ciudad;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoReferencia", inversedBy="referencias")
     * @ORM\JoinColumn(name="tipoReferencia_id", referencedColumnName="id")
     */
    protected $tipoReferencia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="referencias")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $categoria;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\GrupoReferencia", mappedBy="referencias")
     */
    private $grupos;

    /**
     * Poi
     * @ORM\OneToMany(targetEntity="App\Entity\Poi", mappedBy="referencia", cascade={"persist"})
     */
    protected $pois;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Servicio", inversedBy="referencias")
     * @ORM\JoinTable(name="referenciaServicio",
     *      joinColumns={@ORM\JoinColumn(name="referencia_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="servicio_id", referencedColumnName="id")}
     *      )
     */
    private $servicios;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Organizacion", mappedBy="referencias", cascade={"persist"})
     */
    private $organizaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="referencias_propietario")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $propietario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PrecioPortal", mappedBy="referencia")
     */
    protected $preciosPortales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CargaCombustible", mappedBy="referencia")
     */
    protected $cargasCombustible;

    /**
     * @ORM\OneToMany(targetEntity="PuntoCarga", mappedBy="referencia")
     */
    private $puntoCarga;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provincia", inversedBy="referencias")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    protected $provincia;

    public function __construct()
    {
        $this->preciosPortales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getPreciosPortales()
    {
        $this->preciosPortales;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        if ($this->nombre == '') {
            return '---';
        } else {
            return $this->nombre;
        }
    }

    /**
     * Set latitud
     *
     * @param float $latitud
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    /**
     * Get latitud
     *
     * @return float 
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    /**
     * Get longitud
     *
     * @return float 
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set descripcion
     *
     * @param text $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * Get descripcion
     *
     * @return text 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set radio
     *
     * @param float $radio
     */
    public function setRadio($radio)
    {
        $this->radio = $radio;
    }

    /**
     * Get radio
     *
     * @return float 
     */
    public function getRadio()
    {
        if (is_null($this->radio)) {  //tiene un radio null... es un error.
            return 100;
        } else {
            return $this->radio;
        }
    }

    /**
     * Set velocidadMaxima
     *
     * @param float $velocidadMaxima
     */
    public function setVelocidadMaxima($velocidadMaxima)
    {
        $this->velocidadMaxima = $velocidadMaxima;
    }

    /**
     * Get velocidadMaxima
     *
     * @return float 
     */
    public function getVelocidadMaxima()
    {
        return $this->velocidadMaxima;
    }

    /**
     * Set cortePorRalenti
     *
     * @param float $cortePorRalenti
     */
    public function setCortePorRalenti($cortePorRalenti)
    {
        $this->cortePorRalenti = $cortePorRalenti;
    }

    /**
     * Get cortePorRalenti
     *
     * @return float 
     */
    public function getCortePorRalenti()
    {
        return $this->cortePorRalenti;
    }

    /**
     * Set entrada
     *
     * @param boolean $entrada
     */
    public function setEntrada($entrada)
    {
        $this->entrada = $entrada;
    }

    /**
     * Get entrada
     *
     * @return boolean 
     */
    public function getEntrada()
    {
        return $this->entrada;
    }

    /**
     * Set salida
     *
     * @param boolean $salida
     */
    public function setSalida($salida)
    {
        $this->salida = $salida;
    }

    /**
     * Get salida
     *
     * @return boolean 
     */
    public function getSalida()
    {
        return $this->salida;
    }

    /**
     * Set visibilidad
     *
     * @param boolean $visibilidad
     */
    public function setVisibilidad($visibilidad)
    {
        $this->visibilidad = $visibilidad;
    }

    /**
     * Get visibilidad
     *
     * @return boolean 
     */
    public function getVisibilidad()
    {
        return $this->visibilidad;
    }

    /**
     * Set anguloDeteccion
     *
     * @param float $anguloDeteccion
     */
    public function setAnguloDeteccion($anguloDeteccion)
    {
        $this->anguloDeteccion = $anguloDeteccion;
    }

    /**
     * Get anguloDeteccion
     *
     * @return float 
     */
    public function getAnguloDeteccion()
    {
        return $this->anguloDeteccion;
    }

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path_icono", type="string", nullable=true)
     */
    private $pathIcono;

    /**provincia
     * @Assert\File(maxSize="6000000")
     */
    public $icono;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Set tipoReferencia
     *
     * @param \App\Entity\TipoReferencia $tipoReferencia
     */
    public function setTipoReferencia(\App\Entity\TipoReferencia $tipoReferencia)
    {
        $this->tipoReferencia = $tipoReferencia;
    }

    /**
     * Get tipoReferencia
     *
     * @return \App\Entity\TipoReferencia 
     */
    public function getTipoReferencia()
    {
        return $this->tipoReferencia;
    }

    /**
     * Set categoria
     *
     * @param \App\Entity\Categoria $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * Get categoria
     *
     * @return App\Entity\Categoria 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Add grupos
     *
     * @param \App\Entity\GrupoReferencia $grupos
     */
    public function addGrupoReferencia(\App\Entity\GrupoReferencia $grupos)
    {
        $this->grupos[] = $grupos;
    }

    /**
     * Get grupos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    /**
     * Add servicios
     *
     * @param \App\Entity\Servicio $servicios
     */
    public function addServicio(\App\Entity\Servicio $servicios)
    {
        $this->servicios[] = $servicios;
    }

    //borra la asociacion referencia - servicio.
    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
        return $this;
    }

    /**
     * Get servicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Add organizaciones
     *
     * @param \App\Entity\Organizacion $organizaciones
     */
    public function addOrganizacion(\App\Entity\Organizacion $organizaciones)
    {
        $this->organizaciones[] = $organizaciones;
    }

    /**
     * Get organizaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOrganizaciones()
    {
        return $this->organizaciones;
    }

    /**
     * Set propietario
     *
     * @param \App\Entity\Organizacion $propietario
     */
    public function setPropietario(\App\Entity\Organizacion $propietario)
    {
        $this->propietario = $propietario;
    }

    /**
     * Get propietario
     *
     * @return \App\Entity\Organizacion 
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * Set path_icono
     *
     * @param string $path
     */
    public function setPathIcono($path)
    {
        $this->pathIcono = $path;
    }

    /**
     * Get path_icono
     *
     * @return string 
     */
    public function getPathIcono()
    {
        if ($this->pathIcono && substr($this->pathIcono, 19 == '/bundles/apmon')) {
            $split = explode('/', $this->pathIcono);
            $name = $split[count($split) - 1];
            return '/' . $this->getUploadDir() . '/' . $name;
            //            die('////<pre>'.nl2br(var_export($this->getUploadDir() . '/' . $name, true)).'</pre>////');
            //            die('////<pre>'.nl2br(var_export($split[count($split)-1], true)).'</pre>////');
        }
        return $this->pathIcono;
    }

    public function getAbsolutePath()
    {
        return null === $this->pathIcono ? null : $this->getUploadRootDir() . '/' . $this->pathIcono;
    }

    public function getWebPath()
    {
        return null === $this->pathIcono ? null : $this->getUploadDir() . '/' . $this->pathIcono;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'images/iconos';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->icono) {
            return;
        }

        $extension = $this->icono->guessExtension();
        if (!$extension) {
            // extension cannot be guessed
            $extension = 'bin';
        }

        $randomName = rand(1, 99999) . '.' . $extension;

        $this->icono->move($this->getUploadRootDir(), $randomName);
        // move takes the target directory and then the target filename to move to
        //$this->icono->move($this->getUploadRootDir(), $this->icono->getClientOriginalName());
        // set the path property to the filename where you'ved saved the file
        $this->pathIcono = $randomName;

        // clean up the file property as you won't need it anymore
        $this->icono = null;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Set poligono
     *
     * @param string $poligono
     */
    public function setPoligono($poligono)
    {
        $this->poligono = $poligono;
    }

    /**
     * Get poligono
     *
     * @return string 
     */
    public function getPoligono()
    {
        return $this->poligono;
    }

    /**
     * Set direccion
     *
     * @param text $descripcion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * Get direccion
     *
     * @return text 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    public function getKml()
    {
        return $this->kml;
    }

    public function setKml($kml)
    {
        $this->kml = $kml;
    }

    public function uploadkml()
    {
        // the file property can be empty if the field is not required
        if (null === $this->kml) {
            return;
        }

        // move takes the target directory and then the target filename to move to
        $this->kml->move($this->getUploadKmlDir(), $this->kml->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        //$this->pathLogo = $this->logo->getClientOriginalName();
        // clean up the file property as you won't need it anymore
        //$this->logo = null;
        return $this->kml;
    }

    public function getKmlPathFile()
    {
        return null === $this->kml ? null : $this->getUploadKmlDir() . '/' . $this->kml->getClientOriginalName();
    }

    protected function getUploadKmlDir()
    {
        $src = dirname(__DIR__); //la ruta de src
        $base = dirname($src);// la ruta base

      //  die('////<pre>' . nl2br(var_export($base.'/web/uploads', true)) . '</pre>////');
        // the absolute directory path where uploaded documents should be saved
        return $base.'/web/uploads/import';
    }

    public function getCodigoExterno()
    {
        return $this->codigo_externo;
    }

    public function setCodigoExterno($codigo_externo)
    {
        $this->codigo_externo = $codigo_externo;
    }

    public function getColor()
    {
        if (is_null($this->color) || $this->color == '') {
            return 'white';
        } else {
            return $this->color;
        }
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getTransparencia()
    {
        if (is_null($this->transparencia) || $this->transparencia == '') {
            return 0.5;
        } else {
            return $this->transparencia;
        }
    }

    public function setTransparencia($transparencia)
    {
        $this->transparencia = $transparencia;
    }

    function getArea()
    {
        return $this->area;
    }

    function setArea($area)
    {
        $this->area = $area;
        return $this;
    }

    public function isOldPoligon()
    {
        $poli = explode(',', $this->getPoligono());        
        return is_null($this->clase) || (count($poli)>3 && $poli[1][0] == '#');
    }

    public function getClase()
    {
        if (is_null($this->clase) || $this->clase == '') {
            if ($this->poligono != '') {
                return self::CLASE_POLIGONO;
            } else {
                return self::CLASE_RADIAL;
            }
        } else {
            return $this->clase;
        }
    }

    public function getStrClase()
    {
        return $this->arrayClase[$this->getClase()];
    }

    public function setClase($clase)
    {
        $this->clase = $clase;
    }

    function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    function setCargasCombustible($cargasCombustible)
    {
        $this->cargasCombustible = $cargasCombustible;
    }

    function getPuntoCarga()
    {
        return $this->puntoCarga;
    }

    function setPuntoCarga($puntoCarga)
    {
        $this->puntoCarga = $puntoCarga;
    }

    function getUnica()
    {
        return false;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get poi
     */
    public function getPois()
    {
        return $this->pois;
    }

    /**
     * Set poi
     *
     * @return  self
     */
    public function setPois($pois)
    {
        $this->pois = $pois;

        return $this;
    }

    /**
     * Get indica la clase de dibujo que es la referencia.
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set indica la clase de dibujo que es la referencia.
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStrStatus()
    {
        return $this->arrayStatus[$this->getStatus()];
    }

    function getArrayStatus()
    {
        return $this->arrayStatus;
    }

    /**
     * Get $state
     *
     * @return  text
     */
    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get the value of provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set the value of provincia
     *
     * @return  self
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get the value of ciudad
     */ 
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set the value of ciudad
     *
     * @return  self
     */ 
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }
}
