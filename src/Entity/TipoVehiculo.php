<?php
//Clase que especifica si un vehiculo es camioneta, auto, retroexcavadora, etc...

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\TipoVehiculo
 *
 * @ORM\Table(name="tipovehiculo")
 * @ORM\Entity(repositoryClass="App\Repository\TipoVehiculoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TipoVehiculo
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $tipo
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehiculo", mappedBy="tipoVehiculo")
     */
    protected $vehiculos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PrecioPortal", mappedBy="tipoVehiculo")
     */
    protected $preciosPortales;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    /**
     * Add vehiculos
     *
     * @param App\Entity\Vehiculo $vehiculos
     */
    public function addVehiculo(\App\Entity\Vehiculo $vehiculos)
    {
        $this->vehiculos[] = $vehiculos;
    }

    /**
     * Get vehiculos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getVehiculos()
    {
        return $this->vehiculos;
    }

    public function __toString()
    {
        return $this->getTipo();
    }
    function getPreciosPortales()
    {
        return $this->preciosPortales;
    }

    function setPreciosPortales($preciosPortales): void
    {
        $this->preciosPortales = $preciosPortales;
    }
}
