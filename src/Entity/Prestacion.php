<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\Prestacion
 *
 * @ORM\Table(name="prestacion")
 * @ORM\Entity(repositoryClass="App\Repository\PrestacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Prestacion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var datetime $inicio_prestacion
     *
     * @ORM\Column(name="inicio_prestacion", type="datetime", nullable=true)
     */
    private $inicio_prestacion;

    /**
     * @var datetime $fin_prestacion
     *
     * @ORM\Column(name="fin_prestacion", type="datetime", nullable=true)
     */

    private $fin_prestacion;

    /**
     * @ORM\Column(name="horas_uso", type="float", nullable=true)
     */
    private $horasUso;

    /**
     * @ORM\Column(name="horas_totales", type="float", nullable=true)
     */
    private $horasTotales;

    /**
     * @ORM\Column(name="horas_paradas", type="float", nullable=true)
     */
    private $horasParadas;

    /**
     * @ORM\Column(name="estado", type="integer", nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(name="motivo", type="string", nullable=true)
     */
    private $motivo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contratista", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="contratista_id", referencedColumnName="id")
     */
    protected $contratista;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CentroCosto", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="centrocosto_id", referencedColumnName="id",onDelete="SET NULL")
     */
    protected $centroCosto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id")
     */
    private $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ParadaPrestacion", mappedBy="prestacion")
     */
    protected $paradas;


    public function __construct()
    {
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function getInicioPrestacion()
    {
        if (is_null($this->inicio_prestacion)) {
            return $this->inicio_prestacion;
        } elseif ($this->inicio_prestacion instanceof \DateTime) {
            //die('////<pre>' . nl2br(var_export(get_class($this->inicio_prestacion), true)) . '</pre>////');
            return date_format($this->inicio_prestacion, 'd/m/Y H:i');
        }
        return $this->inicio_prestacion;
    }

    function getFinPrestacion()
    {
        //die('////<pre>' . nl2br(var_export($this->fin_prestacion, true)) . '</pre>////');
        if (is_null($this->fin_prestacion)) {
            return $this->fin_prestacion;
        } elseif ($this->fin_prestacion instanceof \DateTime) {
            return date_format($this->fin_prestacion, 'd/m/Y H:i');
        }
        return $this->fin_prestacion;
    }

    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
    }

    function getContratista()
    {
        return $this->contratista;
    }

    function getCentroCosto()
    {
        return $this->centroCosto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setInicioPrestacion($inicio_prestacion)
    {
        $this->inicio_prestacion = $inicio_prestacion;
    }

    function setFinPrestacion($fin_prestacion)
    {
        $this->fin_prestacion = $fin_prestacion;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }

    function setCentroCosto($centrocosto)
    {
        $this->centroCosto = $centrocosto;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getInicio_prestacion()
    {
        return $this->inicio_prestacion;
    }

    function getFin_prestacion()
    {
        return $this->fin_prestacion;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function setInicio_prestacion($inicio_prestacion)
    {
        $this->inicio_prestacion = $inicio_prestacion;
    }

    function setFin_prestacion($fin_prestacion)
    {
        $this->fin_prestacion = $fin_prestacion;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function getMotivo()
    {
        return $this->motivo;
    }

    function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    function getHorasUso()
    {
        return $this->horasUso;
    }

    function setHorasUso($horasUso)
    {
        $this->horasUso = $horasUso;
    }

    function getHorasTotales()
    {
        return $this->horasTotales;
    }

    function setHorasTotales($horasTotales)
    {
        $this->horasTotales = $horasTotales;
    }

    function getParadas()
    {
        return $this->paradas;
    }

    function setParadas($paradas)
    {
        $this->paradas = $paradas;
    }

    function getHorasParadas()
    {
        return $this->horasParadas;
    }

    function setHorasParadas($horasParadas)
    {
        $this->horasParadas = $horasParadas;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }
}
