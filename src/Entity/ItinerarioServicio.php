<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\ItinerarioServicio
 *
 * @ORM\Table(name="itinerario_servicio")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ItinerarioServicio
{


    const ESTADO_SINESTADO = 0;  //estado de sevicios en itinerario
    const ESTADO_AUDITORIA = 1;  //estado de sevicios en itinerario
    const ESTADO_CARGA = 2;      ///estado de sevicios en itinerario
    const ESTADO_PERNOCTE = 3;  //estado de sevicios en itinerario
    const ESTADO_VIAJANDO = 4;
    const ESTADO_DESCONOCE = 5; //desconocido por el chofer


    private $strEstado = array(

        'Sin Estado' => self::ESTADO_SINESTADO,
        'Auditoria' => self::ESTADO_AUDITORIA,
        'Implantado' => self::ESTADO_CARGA,
        'Pernocte' => self::ESTADO_PERNOCTE,
        'En Viaje' => self::ESTADO_VIAJANDO,
        'Desconoce' => self::ESTADO_DESCONOCE
    );

    private $badge = array(

        self::ESTADO_SINESTADO => 'default',
        self::ESTADO_AUDITORIA => 'success',
        self::ESTADO_CARGA => 'success',
        self::ESTADO_PERNOCTE => 'danger',
        self::ESTADO_VIAJANDO => 'success',
        self::ESTADO_DESCONOCE => 'default'
    );


    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    /**
     * pueso pasarle un estado para consultar el str seteado, si no paso nada devuelve el estado del entity
     */
    public function getStrEstado($estado = null)
    {
        if (is_null($estado)) {
            return array_search($this->estado, $this->strEstado);
        } else {
            return array_search($estado, $this->strEstado);
        }
    }

    public function getStrBadge()
    {
        return $this->badge[$this->estado];
    }

    public function getColorOrder($order)
    {
        return array_search($order, $this->colorOrder);
    }

    public function getArrayStrOrder()
    {
        return $this->strOrder;
    }

    public function getStrOrder($orden)
    {
        return array_search($orden, $this->strOrder);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="servicios")
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id",nullable=true)     
     */
    private $itinerario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="itinerarios")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id",nullable=true)
     */
    private $servicio;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="itinerarios")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id",nullable=true)
     */
    private $chofer;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="estado", type="integer", nullable = true)
     * 
     */
    private $estado;

    /**
     * @ORM\Column(name="bill", type="string", length=255, nullable = true)
     */
    private $bill;

    /**
     * @ORM\Column(name="nro_contenedor", type="string", length=255, nullable = true)
     */
    private $nroContenedor;

    /**
     * @ORM\Column(name="patente_acoplado", type="string", length=255, nullable = true)
     */
    private $patenteAcoplado;



    /**
     * Get $id
     *
     * @return  integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set $id
     *
     * @param  integer  $id  $id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }



    /**
     * Get $estado
     *
     * @return  integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set $estado
     *
     * @param  integer  $estado  $estado
     *
     * @return  self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }


    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set the value of servicio
     *
     * @return  self
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get the value of itinerario
     */
    public function getItinerario()
    {
        return $this->itinerario;
    }

    /**
     * Set the value of itinerario
     *
     * @return  self
     */
    public function setItinerario($itinerario)
    {
        $this->itinerario = $itinerario;

        return $this;
    }

    function getBadge()
    {
        return $this->badge;
    }

    /**
     * Get the value of bill
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Set the value of bill
     *
     * @return  self
     */
    public function setBill($bill)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get the value of nroContenedor
     */
    public function getNroContenedor()
    {
        return $this->nroContenedor;
    }

    /**
     * Set the value of nroContenedor
     *
     * @return  self
     */
    public function setNroContenedor($nroContenedor)
    {
        $this->nroContenedor = $nroContenedor;

        return $this;
    }


    /**
     * Get the value of patenteAcoplado
     */ 
    public function getPatenteAcoplado()
    {
        return $this->patenteAcoplado;
    }

    /**
     * Set the value of patenteAcoplado
     *
     * @return  self
     */ 
    public function setPatenteAcoplado($patenteAcoplado)
    {
        $this->patenteAcoplado = $patenteAcoplado;

        return $this;
    }

    /**
     * Get the value of chofer
     */ 
    public function getChofer()
    {
        return $this->chofer;
    }

    /**
     * Set the value of chofer
     *
     * @return  self
     */ 
    public function setChofer($chofer)
    {
        $this->chofer = $chofer;

        return $this;
    }
}
