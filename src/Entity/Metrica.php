<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="metrica")
 * @ORM\Entity(repositoryClass="App\Repository\MetricaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Metrica
{

    const TIPO_INFORME = 1;


    private $tipoMetricaDescipcion = array(
        self::TIPO_INFORME => array('desc' => 'Informe', 'nivel' => 0),

    );

    public function getArrayTipoMetrica()
    {
        return $this->tipoMetricaDescipcion;
    }

    public function getStrTipoMetrica()
    {
        if (isset($this->tipo_metrica)) {
            return $this->tipoMetricaDescipcion[$this->tipo_metrica]['desc'];
        } else {
            return '---';
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(name="tipo_metrica", type="integer")
     */
    private $tipo_metrica;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;    
  

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

   
    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {        
        return $this->TipoEventoDescipcion[$this->tipo_evento];
      
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

   

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

   
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

   
   
   

    /**
     * Get the value of tipo_metrica
     */ 
    public function getTipoMetrica()
    {
        return $this->tipo_metrica;
    }

    /**
     * Set the value of tipo_metrica
     *
     * @return  self
     */ 
    public function setTipoMetrica($tipo_metrica)
    {
        $this->tipo_metrica = $tipo_metrica;

        return $this;
    }
}
