<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\PrecioPortal
 *
 * @ORM\Table(name="precioportal")
 * @ORM\Entity(repositoryClass="App\Repository\PrecioPortalRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class PrecioPortal
{

    const TIPO_PORTAL_PORTICO = 0;
    const TIPO_PORTAL_PEAJE = 1;

    private $strTipoPortal = array(
        self::TIPO_PORTAL_PORTICO => 'Pórtico',
        self::TIPO_PORTAL_PEAJE => 'Peaje',
    );

    public function getArrayStrTipoPortal()
    {
        return $this->strTipoPortal;
    }

    public function getStrTipoPortal()
    {
        if ($this->strTipoPortal != null) {
            return $this->strTipoPortal[$this->tipo_portal];
        } else {
            return $this->strTipoPortal[0];
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $diasSemana
     *
     * @ORM\Column(name="diasSemana", type="integer")
     */
    private $diasSemana;

    /**
     * @var time desde
     *
     * @ORM\Column(name="desde", type="time")
     */
    private $desde;

    /**
     * @var time hasta
     *
     * @ORM\Column(name="hasta", type="time")
     */
    private $hasta;

    /**
     * @var precio float
     *
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referencia", inversedBy="preciosPortales")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $referencia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoVehiculo", inversedBy="preciosPortales")
     * @ORM\JoinColumn(name="tipoVehiculo_id", referencedColumnName="id")
     */
    protected $tipoVehiculo;

    /**
     * 0= Portal 1= Peaje
     * 
     * @ORM\Column(name="tipo_portal", type="float", nullable=true)
     */
    protected $tipo_portal;

    public function getTipoportal()
    {
        return $this->tipo_portal;
    }

    public function setTipoportal($tipo_portal)
    {
        $this->tipo_portal = $tipo_portal;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    private $_diasSemana = array(
        1 => 'domingo',
        2 => 'lunes',
        4 => 'martes',
        8 => 'miercoles',
        16 => 'jueves',
        32 => 'viernes',
        64 => 'sabado'
    );

    /**
     * Get diasSemana
     *
     * @return integer
     */
    public function getDiasSemana()
    {
        return $this->diasSemana;
    }

    public function getArrayDiasSemana()
    {
        $diasSemana = array();
        foreach ($this->_diasSemana as $bit => $dia) {
            if ($this->diasSemana & $bit) {
                $diasSemana[] = $dia;
            }
        }
        return $diasSemana;
    }

    /**
     * Set diasSemana
     *
     */
    public function setDiasSemana($diasSemana)
    {
        $mascara = 0;
        foreach ($this->_diasSemana as $bit => $dia) {
            if (in_array($dia, $diasSemana)) {
                $mascara += $bit;
            }
        }
        $this->diasSemana = $mascara;
    }

    /**
     * Get desde
     *
     * @return time
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * Set desde
     *
     * @param time $desde
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;
    }

    /**
     * Get hasta
     *
     * @return time
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * Set hasta
     *
     * @param time $hasta
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set precio
     *
     * @param float precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * Set tipoVehiculo
     *
     * @param App\Entity\TipoVehiculo $tipoVehiculo
     */
    public function setTipoVehiculo(\App\Entity\TipoVehiculo $tipoVehiculo)
    {
        $this->tipoVehiculo = $tipoVehiculo;
    }

    /**
     * Get tipoVehiculo
     *
     * @return App\Entity\TipoVehiculo 
     */
    public function getTipoVehiculo()
    {
        return $this->tipoVehiculo;
    }

    /**
     * Set referencia
     *
     * @param App\Entity\Referencia $referencia
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    /**
     * Get referencia
     *
     * @return App\Entity\Referencia 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function __toString()
    {

        // Retornar referencia->nombre + tipoVehiculo->nombre + desde + hasta + precio?
        return 'PrecioPortal #' . $this->getId();
    }
}
