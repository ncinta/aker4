<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\OrdenTrabajoProducto
 *
 * @ORM\Table(name="ordentrabajo_horataller")
 * @ORM\Entity(repositoryClass="App\Repository\OrdenTrabajoHoraTallerRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class OrdenTrabajoHoraTaller
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\Column(name="costo_unitario", type="float", nullable=true)
     */
    private $costoUnitario;

    /**
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @ORM\Column(name="costo_neto", type="float", nullable=true)
     */
    private $costoNeto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="horasTaller")
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HoraTaller", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="horataller_id", referencedColumnName="id",nullable=true)
     */
    protected $horaTaller;

    function getId()
    {
        return $this->id;
    }

    function getCantidad()
    {
        return $this->cantidad;
    }

    function getCostoUnitario()
    {
        return $this->costoUnitario;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function getCostoNeto()
    {
        return $this->costoNeto;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function getHoraTaller()
    {
        return $this->horaTaller;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function setCostoUnitario($costoUnitario)
    {
        $this->costoUnitario = $costoUnitario;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function setCostoNeto($costoNeto)
    {
        $this->costoNeto = $costoNeto;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function setHoraTaller($horaTaller)
    {
        $this->horaTaller = $horaTaller;
    }
}
