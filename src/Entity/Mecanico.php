<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="mecanico")
 * @ORM\Entity(repositoryClass="App\Repository\MecanicoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Mecanico
{

    const MECANICO_INDEFINIDO = 0;
    const MECANICO_OFICIAL = 1;
    const MECANICO_AYUDANTE = 2;
    const MECANICO_MENSUAL = 3;
    const MECANICO_OFICIAL_ESPECIAL = 4;
    const MECANICO_OFICIAL_MEDIO = 5;

    private $tipoMecanico = array(
        self::MECANICO_INDEFINIDO => 'Indefinido',
        self::MECANICO_OFICIAL => 'Oficial',
        self::MECANICO_AYUDANTE => 'Ayudante',
        self::MECANICO_MENSUAL => 'Mensual',
        self::MECANICO_OFICIAL_ESPECIAL => 'Oficial Especializado',
        self::MECANICO_OFICIAL_MEDIO => 'Medio Oficial',
    );

    public function getArrayTipoMecanico()
    {
        return $this->tipoMecanico;
    }

    public function getStrTipoMecanico()
    {
        if (isset($this->tipo)) {
            return $this->tipoMecanico[$this->tipo];
        } else {
            return $this->tipoMecanico[0];
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var string Nombre
     *
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;

    /**
     * @var string Nombre
     *
     * @ORM\Column(name="costo_hora", type="float", nullable=true)
     */
    private $costoHora;

    /**
     * Tipo es: 0: indefinido 1=Oficial 2=Ayudante 3=Mensual
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="mecanicos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoMecanico", mappedBy="mecanico")
     */
    private $trabajos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtMecanico", mappedBy="mecanico")
     */
    private $tareasMecanico;

    /**
     * @var string Documento
     *
     * @ORM\Column(name="documento", type="string", nullable=true)
     */
    private $documento;

    /**
     * @var string telefonoParticular
     *
     * @ORM\Column(name="telefono_particular", type="string", nullable=true)
     */
    private $telefonoParticular;

    /**
     * @var string telefonoContacto
     *
     * @ORM\Column(name="telefono_contacto", type="string", nullable=true)
     */
    private $telefonoContacto;

    /**
     * @var string seguro
     *
     * @ORM\Column(name="seguro", type="string", nullable=true)
     */
    private $seguro;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCostoHora()
    {
        return $this->costoHora;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCostoHora($costoHora)
    {
        $this->costoHora = $costoHora;
    }

    function setOrganizacion(Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getMecanicoHistoricos()
    {
        return $this->mecanicoHistoricos;
    }

    function setMecanicoHistoricos($mecanicoHistoricos)
    {
        $this->mecanicoHistoricos = $mecanicoHistoricos;
    }

    function getTrabajos()
    {
        return $this->trabajos;
    }

    function setTrabajos($trabajos)
    {
        $this->trabajos = $trabajos;
    }

    function getTipoMecanico()
    {
        return $this->tipoMecanico;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setTipoMecanico($tipoMecanico)
    {
        $this->tipoMecanico = $tipoMecanico;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getTelefonoParticular()
    {
        return $this->telefonoParticular;
    }

    function getTelefonoContacto()
    {
        return $this->telefonoContacto;
    }

    function getSeguro()
    {
        return $this->seguro;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setTelefonoParticular($telefonoParticular)
    {
        $this->telefonoParticular = $telefonoParticular;
    }

    function setTelefonoContacto($telefonoContacto)
    {
        $this->telefonoContacto = $telefonoContacto;
    }

    function setSeguro($seguro)
    {
        $this->seguro = $seguro;
    }

    public function addTrabajos($orden)
    {
        $this->trabajos[] = $orden;
    }

    public function addTareas($tareaot)
    {
        $this->tareasMecanico[] = $tareaot;
    }

    function getTareasMecanico()
    {
        return $this->tareasMecanico;
    }

    function setTareasMecanico($tareasMecanico)
    {
        $this->tareasMecanico = $tareasMecanico;
    }
}
