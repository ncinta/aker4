<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="empresa")
 * @ORM\Entity(repositoryClass="App\Repository\EmpresaRepository")
 * @ORM\HasLifecycleCallbacks() 
 * ****** A empresa no le cambie el nombre por Cliente porque ya existe una entidad con es enombre **********************
 */
class Empresa
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="empresas")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    private $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Itinerario", mappedBy="empresa", cascade={"remove"})
     */
    private $itinerarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Usuario", mappedBy="empresa", cascade={"remove"})
     */
    private $usuarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacto", mappedBy="empresa", cascade={"remove"})
     */
    private $contactos;

    function __toString()
    {
        return $this->nombre;
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getItinerarios()
    {
        return $this->itinerarios;
    }

    function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get the value of usuarios
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Set the value of usuarios
     *
     * @return  self
     */
    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get the value of contactos
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set the value of contactos
     *
     * @return  self
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;

        return $this;
    }

    /**
     * Add contactos
     *
     * @param App\Entity\Contacto $contacto
     */
    public function addContacto(\App\Entity\Contacto $contacto)
    {
        $this->contactos->add($contacto);
    }

    public function removeContacto($contacto)
    {
        $this->contactos->removeElement($contacto);
        return $this->contactos;
    }
}
