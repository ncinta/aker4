<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="cateogoria")
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaCrmRepository")
 */
class CategoriaCrm
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Peticion", mappedBy="categoria")
     */
    private $peticiones;

    function getId()
    {
        return $this->id;
    }

    function __toString()
    {
        return $this->nombre;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }
}
