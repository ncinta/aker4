<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of TareaOt
 *
 * @author nicolas
 *
 * @ORM\Table(name="tareaot")
 * @ORM\Entity(repositoryClass="App\Repository\TareaOtRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class TareaOt
{

    const ESTADO_NUEVO = 0;
    const ESTADO_ENCURSO = 1;
    const ESTADO_CERRADO = 2;

    private $strEstado = array(
        'Nueva' => self::ESTADO_NUEVO,
        'En Curso' => self::ESTADO_ENCURSO,
        'Cerrada' => self::ESTADO_CERRADO,
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        return array_search($this->estado, $this->strEstado);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var float $costo
     *
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @var float $costo
     *
     * @ORM\Column(name="costo_neto", type="float", nullable=true)
     */
    private $costoNeto;

    /**
     * @var string $fecha
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="estado", type="integer")
     * 
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="tareas")
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenTrabajo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtProducto", mappedBy="tarea")
     */
    private $productos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtMecanico", mappedBy="tarea")
     */
    private $mecanicos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtComentario", mappedBy="tarea")
     */
    private $comentarios;

    function getId()
    {
        return $this->id;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getOrdenTrabajo()
    {
        return $this->ordenTrabajo;
    }

    function getProductos()
    {
        return $this->productos;
    }

    function getMecanicos()
    {
        return $this->mecanicos;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setOrdenTrabajo($ordenTrabajo)
    {
        $this->ordenTrabajo = $ordenTrabajo;
    }

    function setProductos($productos)
    {
        $this->productos = $productos;
    }

    function setMecanicos($mecanicos)
    {
        $this->mecanicos = $mecanicos;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function getCostoNeto()
    {
        return $this->costoNeto;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function setCostoNeto($costoNeto)
    {
        $this->costoNeto = $costoNeto;
    }

    public function addProductos($producto)
    {
        $this->productos[] = $producto;
    }

    public function addMecanicos($mecanico)
    {
        $this->mecanicos[] = $mecanico;
    }

    public function addComentarios($comentario)
    {
        $this->comentarios[] = $comentario;
    }
    function getFecha()
    {
        return $this->fecha;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getComentarios()
    {
        return $this->comentarios;
    }

    function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }
}
