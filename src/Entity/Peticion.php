<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="peticion")
 * @ORM\Entity(repositoryClass="App\Repository\PeticionRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Peticion
{

    private $changes;

    const SEGUIDORES = 'Seguidores';
    const ASIGNADO_A = 'Asignación';
    const CATEGORIA = 'Categoría';
    const ESTADO = 'Estado';
    const PRIORIDAD = 'Prioridad';
    const TERMINADO = 'Realizado';
    const FECHA_FIN = 'Fecha Fin';
    const FECHA_INICIO = 'Fecha Inicio';
    const ISPRIVADO = 'Privado';
    const DESCRIPCION = 'Descripción';
    const ASUNTO = 'Asunto';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="asunto", type="string", length=255)
     */
    private $asunto;

    /**
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(name="motivo_cierre", type="string", nullable=true)
     */
    private $motivo_cierre;

    /**
     * @ORM\Column(name="is_privado", type="boolean")
     */
    private $isPrivado;

    /**
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true) 
     */
    private $fecha_inicio;

    /**
     * @ORM\Column(name="fecha_fin", type="date", nullable=true) 
     */
    private $fecha_fin;

    /**
     * @ORM\Column(name="terminado", type="integer", nullable=true) 
     */
    private $terminado;

    /**
     * @ORM\Column(name="resultado", type="integer", nullable=true) 
     */
    private $resultado;

    /**
     * @ORM\Column(name="prioridad", type="integer", nullable=true) 
     */
    private $prioridad;

    /**
     * @ORM\Column(name="tipo", type="integer", nullable=true) 
     */
    private $tipo;

    /**
     * @var datetime esta fecha la vamos a usar para el indice. Se setea con la actual cuando se cierra la ot.
     * @ORM\Column(name="close_at", type="datetime", nullable=true)
     */
    private $close_at;

    /**
     * @ORM\ManyToOne(targetEntity="Estado", inversedBy="peticiones")
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="CategoriaCrm", inversedBy="peticiones")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="peticiones")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $servicio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="peticiones")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="peticionesAutor")
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $autor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="peticionesAsignadas")
     * @ORM\JoinColumn(name="asignado_a_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $asignado_a;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", inversedBy="peticiones", cascade={"persist"})
     * @ORM\JoinTable(name="peticiones_usuarios")
     */
    private $seguidores;

    /**
     * @ORM\OneToMany(targetEntity="Comentario", mappedBy="peticion")
     */
    private $comentarios;

    /**
     * @ORM\OneToMany(targetEntity="BitacoraPeticion", mappedBy="peticion")
     */
    private $bitacora;

    /**
     * @ORM\ManyToOne(targetEntity="Peticion", inversedBy="peticiones")
     * @ORM\JoinColumn(name="padre_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $padre;

    /**
     * @ORM\OneToMany(targetEntity="Peticion", mappedBy="padre")
     */
    private $peticiones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="peticiones")
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenTrabajo;

    public function __construct()
    {
        $this->peticiones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getAsunto()
    {
        return $this->asunto;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getIsPrivado()
    {
        return $this->isPrivado;
    }

    function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    function getFechaFin()
    {
        return $this->fecha_fin;
    }

    function getTerminado()
    {
        return $this->terminado;
    }

    function getPrioridad()
    {
        return $this->prioridad;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getCategoria()
    {
        return $this->categoria;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getAutor()
    {
        return $this->autor;
    }

    function getAsignadoA()
    {
        return $this->asignado_a;
    }

    function getSeguidores()
    {
        return $this->seguidores;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function getComentarios()
    {
        return $this->comentarios;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setAsunto($asunto)
    {
        $this->changes[self::ASUNTO] = array($this->asunto, $asunto);
        $this->asunto = $asunto;
    }

    function setDescripcion($descripcion)
    {
        $this->changes[self::DESCRIPCION] = array($this->descripcion, $descripcion);
        $this->descripcion = $descripcion;
    }

    function setIsPrivado($isPrivado)
    {
        $this->changes[self::ISPRIVADO] = array($this->isPrivado, $isPrivado);
        $this->isPrivado = $isPrivado;
    }

    function setFechaInicio($fecha_inicio)
    {
        if (!(is_null($this->fecha_inicio) && is_null($fecha_inicio))) {
            $this->changes[self::FECHA_INICIO] = array(
                is_null($this->fecha_inicio) ? '' : $this->fecha_inicio->format('d-m-Y'),
                is_null($fecha_inicio) ? '' : $fecha_inicio->format('d-m-Y')
            );
        }
        $this->fecha_inicio = $fecha_inicio;
    }

    function setFechaFin($fecha_fin)
    {
        if (!(is_null($this->fecha_fin) && is_null($fecha_fin))) {
            $this->changes[self::FECHA_FIN] = array(
                is_null($this->fecha_fin) ? '' : $this->fecha_fin->format('d-m-Y'),
                is_null($fecha_fin) ? '' : $fecha_fin->format('d-m-Y'),
            );
        }
        $this->fecha_fin = $fecha_fin;
    }

    function setTerminado($terminado)
    {
        // $this->changes[self::TERMINADO] = array($this->terminado, $terminado);
        $this->terminado = $terminado;
    }

    function setPrioridad($prioridad)
    {
        $this->changes[self::PRIORIDAD] = array($this->strPrioridad($this->prioridad), $this->strPrioridad($prioridad));
        $this->prioridad = $prioridad;
    }

    function setEstado($estado)
    {
        $this->changes[self::ESTADO] = array(
            is_null($this->estado) ? '' : $this->estado->getNombre(),
            is_null($estado) ? '' : $estado->getNombre()
        );
        //die('////<pre>' . nl2br(var_export($this->changes, true)) . '</pre>////');
        $this->estado = $estado;
    }

    function setCategoria($categoria)
    {
        $this->changes[self::CATEGORIA] = array(
            is_null($this->categoria) ? '' : $this->categoria->getNombre(),
            is_null($categoria) ? '' : $categoria->getNombre()
        );
        $this->categoria = $categoria;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setAutor($autor)
    {
        $this->autor = $autor;
    }

    function setAsignadoA($asignado_a)
    {
        $this->changes[self::ASIGNADO_A] = array(
            is_null($this->asignado_a) ? '' : $this->asignado_a->getNombre(),
            is_null($asignado_a) ? '' : $asignado_a->getNombre()
        );
        $this->asignado_a = $asignado_a;
    }

    function setSeguidores($seguidores)
    {
        //$this->changes[self::SEGUIDORES] = array($this->seguidores, $seguidores);
        $this->seguidores = $seguidores;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }

    function addComentario($comentario)
    {
        $this->comentarios[] = $comentario;
        return $this;
    }

    function getBitacora()
    {
        return $this->bitacora;
    }

    function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;
    }

    function getResultado()
    {
        return $this->resultado;
    }

    function setResultado($resultado)
    {
        $this->resultado = $resultado;
    }

    private $arrayPrioridad = array(
        0 => array('color' => 'info', 'texto' => 'baja',  'color_text' => '#4f4f4f'),
        1 => array('color' => 'default', 'texto' => 'normal', 'color_text' => '#4f4f4f'),
        2 => array('color' => 'warning', 'texto' => 'alta', 'color_text' => '#ed9c28'),
        3 => array('color' => 'danger', 'texto' => 'urgente' , 'color_text' => '#d2322d'),
        4 => array('color' => 'danger', 'texto' => 'inmediata' , 'color_text' => '#d2322d'),
    );

    function colorPrioridad()
    {
        return $this->arrayPrioridad[$this->prioridad]['color'];
    }
 function colorTxtPrioridad()
    {
        return $this->arrayPrioridad[$this->prioridad]['color_text'];
    }

    function strPrioridad()
    {
        if (array_key_exists($this->prioridad, $this->arrayPrioridad)) {
            return $this->arrayPrioridad[$this->prioridad]['texto'];
        } else {
            return '';
        }
    }

    function getPadre()
    {
        return $this->padre;
    }

    function setPadre($padre)
    {
        $this->padre = $padre;
    }

    function getChanges()
    {
        return $this->changes;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function __toString()
    {
        return $this->id;
    }

    function strFechaInicio()
    {        
        if (!is_null($this->fecha_inicio)) {
          // dd($this->fecha_inicio->getTimezone());
            $now = new \DateTime();
            if($this->fecha_inicio->getTimezone()){
                $now->setTimezone($this->fecha_inicio->getTimezone());
            }
            $diff = $now->diff($this->fecha_inicio);
            if ($now == $this->fecha_fin) {
                return 'Hoy';
            } elseif ($diff->invert == 1) {
                return sprintf('%s<br><small>Hace %d día/s</small>', $this->fecha_inicio->format('d-m-Y'), $diff->days);
            } else {
                if ($diff->days == 0) {
                    return sprintf('%s<br><small>Mañana</small>', $this->fecha_inicio->format('d-m-Y'));
                } else {
                    return sprintf('%s<br><small>Dentro de %d día/s</small>', $this->fecha_inicio->format('d-m-Y'), $diff->days);
                }
            }
            return $this->fecha_inicio->format('d-m-Y');
        } else {            
            return null;
        }
    }

    function strFechaFin()
    {
        return $this->fecha_fin->format('d-m-Y');
    }

    function getOrdenTrabajo()
    {
        return $this->ordenTrabajo;
    }

    function setOrdenTrabajo($ordenTrabajo)
    {
        $this->ordenTrabajo = $ordenTrabajo;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setTipo($tipo)
    {
        if ($tipo == true) {
            $this->tipo = 1;
        } else {
            $this->tipo = 0;
        }
    }

    function getstrTipo()
    {
        if ($this->tipo == 1) {
            return 'Retrabajo';
        } else {
            return 'Normal';
        }
    }

    public function getMotivoCierre()
    {
        return $this->motivo_cierre;
    }

    public function setMotivoCierre($motivo_cierre): void
    {
        $this->motivo_cierre = $motivo_cierre;
    }

    public function getCloseAt()
    {
        return $this->close_at;
    }

    public function setCloseAt($close_at): void
    {
        $this->close_at = $close_at;
    }
}
