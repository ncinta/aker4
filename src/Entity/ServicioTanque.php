<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * App\Entity\ServicioTanque
 *
 * @ORM\Table(name="serviciotanque", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="medicion_idx", columns={"servicio_id", "porcentaje"})
 * })
 * @Unique\UniqueEntity(fields={"servicio_id", "porcentaje"})
 * @ORM\Entity(repositoryClass="App\Repository\ServicioTanqueRepository")
 */
class ServicioTanque
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="litros", type="float")
     */
    private $litros;

    /**
     * @var integer porcentaje
     *
     * @ORM\Column(name="porcentaje", type="float")
     */
    private $porcentaje;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="medicionestanque")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLitros()
    {
        return $this->litros;
    }

    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setLitros($litros)
    {
        $this->litros = $litros;
        return $this;
    }

    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
        return $this;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }

    function getVars()
    {
        return get_object_vars($this);
    }
}
