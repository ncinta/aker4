<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="servicioinputs")
 * @ORM\Entity(repositoryClass="App\Repository\ServicioInputsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ServicioInputs
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false )
     */
    private $nombre;

    /**
     * @ORM\Column(name="abbr", type="string", length=5, nullable=false )
     * @Assert\Length(min = 2, max = 5)
     */
    private $abbr;

    /**
     * @var string $icono
     *
     * @ORM\Column(name="icono", type="string", nullable=true)
     */
    private $icono;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ModeloSensor", inversedBy="servicios")
     * @ORM\JoinColumn(name="modelosensor_id", referencedColumnName="id")
     */
    protected $modeloSensor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="inputs")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function getModeloSensor()
    {
        return $this->modeloSensor;
    }

    public function setModeloSensor($modeloSensor)
    {
        $this->modeloSensor = $modeloSensor;
        return $this;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }

    public function getAbbr()
    {
        return $this->abbr;
    }

    public function setAbbr($abbr)
    {
        $this->abbr = $abbr;
        return $this;
    }

    public function getIcono()
    {
        return $this->icono;
    }

    public function setIcono($icono)
    {
        $this->icono = $icono;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->nombre;
    }

    public function data2array()
    {
        $d['id'] = $this->id;
        $d['nombre'] = $this->nombre;
        if (!is_null($this->abbr))
            $d['abbr'] = $this->abbr;
        if (!is_null($this->modeloSensor))
            $d['tipo'] = $this->modeloSensor->getNombre();
        return $d;
    }
}
