<?php
//Clase que especifica si una referencia es privada o publica
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\TipoReferencia
 *
 * @ORM\Table(name="tiporeferencia")
 * @ORM\Entity(repositoryClass="App\Repository\TipoReferenciaRepository")
 */
class TipoReferencia
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var boolean $isPublic
     *
     * @ORM\Column(name="isPublic", type="boolean")
     */
    private $isPublic;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referencia", mappedBy="tipoReferencia")
     */
    protected $referencias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReferenciaGpm", mappedBy="tipoReferencia")
     */
    protected $referenciasGpm;

    public function __construct()
    {
        $this->referencias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * Get isPublic
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Add referencias
     *
     * @param App\Entity\Referencia $referencias
     */
    public function addReferencia(Referencia $referencias)
    {
        $this->referencias[] = $referencias;
    }

    /**
     * Get referencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getReferenciasGpm()
    {
        return $this->referenciasGpm;
    }

    function setReferenciasGpm($referenciasGpm)
    {
        $this->referenciasGpm = $referenciasGpm;
    }
}
