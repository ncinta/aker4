<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="modelosensor")
 * @ORM\Entity
 */
class ModeloSensor
{

    const tipoSensorAnalogico = 1; //rango de valores.
    const tipoSensorDigital = 2;  //solo 0 / 1
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo", inversedBy="sensores")     
     */
    private $modelo;

    /**     
     * @ORM\OneToMany(targetEntity="App\Entity\Sensor", mappedBy="modeloSensor")     
     */
    private $sensores;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="campo_trama", type="string", length=255, nullable=true)
     */
    private $campoTrama;

    /**
     * @ORM\Column(name="seriado", type="boolean", nullable=true)
     */
    private $seriado;

    /**
     * @ORM\Column(name="tipo_sensor", type="integer", nullable=true)
     */
    private $tipoSensor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioInputs", mappedBy="modeloSensor")
     */
    protected $servicios;

    public function getId()
    {
        return $this->id;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCampoTrama()
    {
        return $this->campoTrama;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function setCampoTrama($campoTrama)
    {
        $this->campoTrama = $campoTrama;
        return $this;
    }

    function getSensores()
    {
        return $this->sensores;
    }

    function setSensores($sensores)
    {
        $this->sensores = $sensores;
    }

    public function isSeriado()
    {
        return $this->seriado;
    }

    public function setSeriado($seriado)
    {
        $this->seriado = $seriado;
        return $this;
    }

    public function getTipoSensor()
    {
        return $this->tipoSensor;
    }

    public function setTipoSensor($tipoSensor)
    {
        $this->tipoSensor = $tipoSensor;
        return $this;
    }


    public function __toString()
    {
        return (string) $this->nombre;
    }
}
