<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Producto
 *
 * @author nicolas
 */

/**
 * App\Entity\Producto
 *
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="App\Repository\ProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Producto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="marca", type="string", length=255, nullable=true)
     */
    private $marca;

    /**
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @var string $codigo_barra
     *
     * @ORM\Column(name="codigo_barra", type="string", length=255, nullable=true)
     */
    private $codigoBarra;

    /**
     * @var float $iva
     *
     * @ORM\Column(name="tasa_iva", type="float", nullable=true)
     */
    private $tasaIva;


    /**
     * @var string $modelo
     *
     * @ORM\Column(name="modelo", type="string", length=255, nullable=true)
     */
    private $modelo;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="producto", cascade={"persist"})    
     */
    protected $stocks;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="productos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadProducto", mappedBy="producto")
     */
    private $actividades;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadHistorico", mappedBy="producto",cascade ={"persist"})
     */
    protected $historicos;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductoServicioMantHist", mappedBy="producto")
     */
    private $productoHistoricos;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoProducto", mappedBy="producto", cascade ={"persist"})
     */
    private $ordenesTrabajo;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtProducto", mappedBy="producto", cascade ={"persist"})
     */
    private $tareas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rubro", inversedBy="productos")
     * @ORM\JoinColumn(name="rubro_id", referencedColumnName="id")
     */
    protected $rubro;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoFichaTecnica", mappedBy="producto", cascade ={"persist"})
     */
    protected $fichasTecnicas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoExterno", mappedBy="producto", cascade={"persist"})
     */
    protected $ordenesTrabajoExterno;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getMarca()
    {
        return $this->marca;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getStocks()
    {
        return $this->stocks;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setMarca($marca)
    {
        $this->marca = $marca;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setStocks($stocks)
    {
        $this->stocks = $stocks;
    }

    function getModelo()
    {
        return $this->modelo;
    }

    function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getCodigoBarra()
    {
        return $this->codigoBarra;
    }

    function setCodigoBarra($codigoBarra)
    {
        $this->codigoBarra = $codigoBarra;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }

    public function addActividades($actividad)
    {
        //die('////<pre>' . nl2br(var_export(count($this->servicios), true)) . '</pre>////');        
        if (!$this->actividades->contains($actividad)) {
            $this->actividades[] = $actividad;
        }
    }

    public function removeActividad($actividad)
    {
        $this->actividades->removeElement($actividad);
    }

    public function __toString()
    {
        $str = $this->getNombre() . ' (' . $this->getMarca() . ') ' . $this->getModelo();
        return $str;
    }

    function getHistoricos()
    {
        return $this->historicos;
    }

    function setHistoricos($historicos)
    {
        $this->historicos = $historicos;
    }

    function getMantenimientos()
    {
        return $this->mantenimientos;
    }

    function setMantenimientos($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    function getProductoHistoricos()
    {
        return $this->productoHistoricos;
    }

    function setProductoHistoricos($productoHistoricos)
    {
        $this->productoHistoricos = $productoHistoricos;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    public function addOrdenTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo[] = $ordenesTrabajo;
    }

    public function addTareaOt($tarea)
    {
        $this->tareas[] = $tarea;
    }

    public function removeProducto($ordenesTrabajo)
    {
        $this->ordenesTrabajo->removeElement($ordenesTrabajo);
    }

    function getRubro()
    {
        return $this->rubro;
    }

    function setRubro($rubro)
    {
        $this->rubro = $rubro;
    }

    public function addFichaTecnica($fichaTecnica)
    {
        //die('////<pre>' . nl2br(var_export(count($this->servicios), true)) . '</pre>////');        
        if (!$this->fichasTecnicas->contains($fichaTecnica)) {
            $this->fichasTecnicas[] = $fichaTecnica;
        }
    }

    public function removeFichaTecnica($fichaTecnica)
    {
        $this->fichasTecnicas->removeElement($fichaTecnica);
    }

    function setOrdenesTrabajoExterno($ordenesTrabajoExterno)
    {
        $this->ordenesTrabajoExterno = $ordenesTrabajoExterno;
    }

    public function addOrdenTrabajoExterno($ordenesTrabajo)
    {
        $this->ordenesTrabajoExterno[] = $ordenesTrabajo;
    }

    function getTasaIva()
    {
        return $this->tasaIva;
    }

    function setTasaIva($tasaIva)
    {
        $this->tasaIva = $tasaIva;
    }

    function getTareas()
    {
        return $this->tareas;
    }

    function setTareas($tareas)
    {
        $this->tareas = $tareas;
    }

     /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get the value of created_at
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of updated_at
     */ 
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */ 
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
