<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Actividad
 *
 * @author nicolas
 */

/**
 * App\Entity\Actividad
 *
 * @ORM\Table(name="actividad")
 * @ORM\Entity(repositoryClass="App\Repository\ActividadRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class Actividad
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $fecha
     *
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @var string $fecha
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadProducto", mappedBy="actividad",cascade ={"persist"})
     */
    protected $productos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadHistorico", mappedBy="actividad",cascade ={"persist"})
     */
    protected $historicos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoFichaTecnica", mappedBy="actividad",cascade ={"persist"})
     */
    protected $fichasTecnicas;

    /**
     * @var Mantenimiento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Mantenimiento", inversedBy="actividades")     
     * @ORM\JoinColumn(name="mantenimiento_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $mantenimiento;

    function getId()
    {
        return $this->id;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function getProductos()
    {
        return $this->productos;
    }

    function setProductos($productos)
    {
        $this->productos = $productos;
    }

    public function addProductos($producto)
    {
        $this->productos[] = $producto;
    }

    public function removeProducto($producto)
    {
        $this->productos->removeElement($producto);
    }

    function getMantenimiento()
    {
        return $this->mantenimiento;
    }

    function setMantenimiento(Mantenimiento $mantenimiento)
    {
        $this->mantenimiento = $mantenimiento;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    function getHistoricos()
    {
        return $this->historicos;
    }

    function setHistoricos($historicos)
    {
        $this->historicos = $historicos;
    }

    function getFichasTecnicas()
    {
        return $this->fichasTecnicas;
    }

    function setFichasTecnicas($fichasTecnicas)
    {
        $this->fichasTecnicas = $fichasTecnicas;
    }


    public function __toString()
    {
        return $this->nombre;
    }
}
