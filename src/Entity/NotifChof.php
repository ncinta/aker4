<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ChoferContacto
 *
 * @author nicolas
 */

/**
 * App\Entity\ChoferContacto
 *
 * @ORM\Table(name="notif_choferes")
 * @ORM\Entity(repositoryClass="App\Repository\NotifChofRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class NotifChof
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var Chofer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="notificaciones")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $chofer;

    /**
     * @var notificacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\NotificacionChofer", inversedBy="notifChof")
     * @ORM\JoinColumn(name="notificacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $notificacion;



    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }
    function getId()
    {
        return $this->id;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getChofer(): Chofer
    {
        return $this->chofer;
    }

    function getContacto(): Chofer
    {
        return $this->contacto;
    }

    function setChofer(Chofer $chofer)
    {
        $this->chofer = $chofer;
    }

    function setContacto(Chofer $contacto)
    {
        $this->contacto = $contacto;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getNotificacion()
    {
        return $this->notificacion;
    }

    function setNotificacion($notificacion)
    {
        $this->notificacion = $notificacion;
    }
}
