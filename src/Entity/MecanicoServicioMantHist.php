<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Mecanico
 *
 * @ORM\Table(name="mecanico_serviciomanthist")
 * @ORM\Entity(repositoryClass="App\Repository\MecanicoServicioMantHistRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MecanicoServicioMantHist {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="horas", type="float", nullable=true)
     */
    private $horas;
    
    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="costo", type="float", nullable=true)
     */
    private $costo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServicioMantenimientoHistorico", inversedBy="mecanicos", cascade={"persist"})
     * @ORM\JoinColumn(name="serviciomanthist_id", referencedColumnName="id", nullable=true)
     */
    protected $mantenimiento;



    function getId() {
        return $this->id;
    }

    function getHoras() {
        return $this->horas;
    }

    function getMantenimiento() {
        return $this->mantenimiento;
    }

    function getMecanico() {
        return $this->mecanico;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setHoras($horas) {
        $this->horas = $horas;
    }

    function setMantenimiento($mantenimiento) {
        $this->mantenimiento = $mantenimiento;
    }

    function setMecanico($mecanico) {
        $this->mecanico = $mecanico;
    }

    function getCosto() {
        return $this->costo;
    }

    function setCosto($costo) {
        $this->costo = $costo;
    }



}
