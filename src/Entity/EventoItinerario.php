<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Servicio as Servicio;
use App\Entity\Usuario as Usuario;
use App\Entity\Evento as Evento;
use App\Entity\EventoTemporal as EventoTemporal;
use App\Entity\Itinerario as Itinerario;

/**
 *
 * @ORM\Table(name="evento_itinerario")
 * @ORM\Entity(repositoryClass="App\Repository\EventoItinerarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EventoItinerario
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var Evento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Evento", inversedBy="eventoItinerario")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=true, onDelete="SET NULL"))
     */
    private $evento;

    /**
     * @var Itinerario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="eventoItinerario")     
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $itinerario;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoEvento", inversedBy="eventosItinerario")
     * @ORM\JoinColumn(name="tipoevento_id", referencedColumnName="id")
     */
    protected $tipoEvento;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\HistoricoItinerario", mappedBy="eventoItinerario", cascade={"persist"})
     */
    protected $historicoItinerario;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {        
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        //        $this->updated_at = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getEvento()
    {
        return $this->evento;
    }

    public function setEvento($evento)
    {
        $this->evento = $evento;
    }

    public function removeEvento() {
        $this->evento = null;
    }

    /**
     * Get the value of itinerario
     *
     * @return  Itinerario
     */
    public function getItinerario()
    {
        return $this->itinerario;
    }

    /**
     * Set the value of itinerario
     *
     * @param  Itinerario  $itinerario
     *
     * @return  self
     */
    public function setItinerario(Itinerario $itinerario)
    {
        $this->itinerario = $itinerario;

        return $this;
    }

    /**
     * Get the value of tipoEvento
     */
    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    /**
     * Set the value of tipoEvento
     *
     * @return  self
     */
    public function setTipoEvento($tipoEvento)
    {
        $this->tipoEvento = $tipoEvento;

        return $this;
    }

    /**
     * Get the value of historicoItinerario
     */ 
    public function getHistoricoItinerario()
    {
        return $this->historicoItinerario;
    }

    /**
     * Set the value of historicoItinerario
     *
     * @return  self
     */ 
    public function setHistoricoItinerario($historicoItinerario)
    {
        $this->historicoItinerario = $historicoItinerario;

        return $this;
    }
}
