<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\OrdenTrabajoProducto
 *
 * @ORM\Table(name="ordentrabajo_producto")
 * @ORM\Entity(repositoryClass="App\Repository\OrdenTrabajoProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrdenTrabajoProducto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\Column(name="costo_unitario", type="float", nullable=true)
     */
    private $costoUnitario;

    /**
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @ORM\Column(name="costo_neto", type="float", nullable=true)
     */
    private $costoNeto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="productos")
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Deposito", inversedBy="productosUsados")
     * @ORM\JoinColumn(name="deposito_id", referencedColumnName="id",nullable=true)
     */
    protected $deposito;

    function getId()
    {
        return $this->id;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
    function getCantidad()
    {
        return $this->cantidad;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function getCostoUnitario()
    {
        return $this->costoUnitario;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function setCostoUnitario($costoUnitario)
    {
        $this->costoUnitario = $costoUnitario;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function getCostoNeto()
    {
        return $this->costoNeto;
    }

    function setCostoNeto($costoNeto)
    {
        $this->costoNeto = $costoNeto;
    }
}
