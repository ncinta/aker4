<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Transporte
 *
 * @ORM\Table(name="transporte")
 * @ORM\Entity(repositoryClass="App\Repository\TransporteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Transporte
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $domicilio
     *
     * @ORM\Column(name="domicilio", type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @var string $localidad
     *
     * @ORM\Column(name="localidad", type="string", length=255, nullable=true)
     */
    private $localidad;

    /**
     * @var string $contacto
     *
     * @ORM\Column(name="contacto", type="string", length=255, nullable=true)
     */
    private $contacto;

    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(name="nota", type="text", nullable=true)
     */
    private $nota;


    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Servicio", mappedBy="transporte", cascade={"persist"})
     */
    protected $servicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacto", mappedBy="transporte", cascade={"persist"})
     */
    protected $contactos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Usuario", mappedBy="transporte", cascade={"persist"})
     */
    protected $usuarios;    
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chofer", mappedBy="transporte", cascade={"persist"})
     */
    protected $choferes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="transportes")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Add servicios
     *
     * @param App\Entity\Servicio $servicios
     */
    public function addServicio(Servicio $servicios)
    {
        $this->servicios[] = $servicios;
    }

    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
        return $this->servicios;
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getItinerarios()
    {
        return $this->itinerarios;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }


    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;
    }

    function getServicios()
    {
        return $this->servicios;
    }

    function setServicios($servicios)
    {
        $this->servicios = $servicios;
    }

    /**
     * Get the value of usuarios
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Set the value of usuarios
     *
     * @return  self
     */
    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get the value of contactos
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set the value of contactos
     *
     * @return  self
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;

        return $this;
    }


    public function __construct()
    {

        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contactos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get $domicilio
     *
     * @return  string
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set $domicilio
     *
     * @param  string  $domicilio  $domicilio
     *
     * @return  self
     */
    public function setDomicilio(string $domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get $localidad
     *
     * @return  string
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set $localidad
     *
     * @param  string  $localidad  $localidad
     *
     * @return  self
     */
    public function setLocalidad(string $localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get $contacto
     *
     * @return  string
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set $contacto
     *
     * @param  string  $contacto  $contacto
     *
     * @return  self
     */
    public function setContacto(string $contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get $telefono
     *
     * @return  string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set $telefono
     *
     * @param  string  $telefono  $telefono
     *
     * @return  self
     */
    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get $email
     *
     * @return  string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set $email
     *
     * @param  string  $email  $email
     *
     * @return  self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of nota
     */ 
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Set the value of nota
     *
     * @return  self
     */ 
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get the value of choferes
     */ 
    public function getChoferes()
    {
        return $this->choferes;
    }

    /**
     * Set the value of choferes
     *
     * @return  self
     */ 
    public function setChoferes($choferes)
    {
        $this->choferes = $choferes;

        return $this;
    }
}
