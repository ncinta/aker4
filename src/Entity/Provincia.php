<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * App\Entity\Provincia
 *
 * @ORM\Table(name="provincia", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="code_idx", columns={"code"})
 * })
 * @Unique\UniqueEntity(fields={"code"}, message="Ya existe una provincia con este código")
 * @ORM\Entity(repositoryClass="App\Repository\ProvinciaRepository")
 */
class Provincia
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /** (STATE DE CHROBINSON)
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    protected $code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referencia", mappedBy="provincia")
     */
    protected $referencias;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }



    /**
     * Get the value of referencias
     */ 
    public function getReferencias()
    {
        return $this->referencias;
    }

    /**
     * Set the value of referencias
     *
     * @return  self
     */ 
    public function setReferencias($referencias)
    {
        $this->referencias = $referencias;

        return $this;
    }

    /**
     * Get the value of code
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
}
