<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * Description of PuntoCarga
 *
 * @author nicolas
 */

/**
 * App\Entity\PuntoCarga
 *
 * @ORM\Table(name="puntocarga")
 * @ORM\Entity(repositoryClass="App\Repository\PuntoCargaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PuntoCarga
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @ORM\OrderBy({"nombre" = "ASC"})
     * @Assert\Regex(
     *     pattern="/^[\w\s\(\)\@\:\-\_\*\+\.á-úÁ-Ú\$\&\?\¿°º\/]+$/",
     *     message="Contiene caracteres inválidos. No se permite coma y punto y coma u otros similares."
     * )    
     */
    private $nombre;

    /**
     * @var string $cuit
     *
     * @ORM\Column(name="cuit", type="string", length=255, nullable=true)
     */
    private $cuit;

    /**
     * @var string $codigo
     *
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @ORM\OneToOne(targetEntity="Servicio", inversedBy="puntoCarga")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="SET NULL", nullable=true))
     */
    private $servicio;

    /**
     * @ORM\ManyToOne(targetEntity="Referencia", inversedBy="puntoCarga")
     * @ORM\JoinColumn(name="referencia_id", referencedColumnName="id", onDelete="SET NULL", nullable=true))
     */
    private $referencia;

    /**
     * @ORM\OneToMany(targetEntity="PrecioTipoCombustible", mappedBy="puntoCarga",cascade ={"persist"})
     */
    protected $precioTipoCombustibles;

    /**
     * @ORM\ManyToOne(targetEntity="Organizacion", inversedBy="puntocargas")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CargaCombustible", mappedBy="puntoCarga")
     */
    protected $cargasCombustible;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    function getPrecioTipoCombustibles()
    {
        return $this->precioTipoCombustibles;
    }

    function getIdsTipoCombustibles()
    {
        $ids = array();
        foreach ($this->precioTipoCombustibles as $combustible) {
            $ids[] = $combustible->getTipoCombustible()->getId();
        }
        return $ids;
    }

    function setPrecioTipoCombustibles($precioTipoCombustibles)
    {
        $this->precioTipoCombustibles = $precioTipoCombustibles;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    function setCargasCombustible($cargasCombustible)
    {
        $this->cargasCombustible = $cargasCombustible;
    }

 
    public function getCuit()
    {
        return $this->cuit;
    }

 
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

 
    public function getCodigo()
    {
        return $this->codigo;
    }

    

    public function setCodigo($codigo)
    {
        if($codigo != null){
           $this->codigo = trim($codigo);
        }

        return $this;
    }
    public function __toString()
    {
        return $this->nombre;
    }
}
