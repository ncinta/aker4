<?php

namespace App\Entity;

class FiltroProyecto
{

    protected $desde;
    protected $hasta;
    protected $fechatodos;
    protected $responsable;
    protected $estado;
    protected $cliente;
    protected $contratista;
    protected $organizacion;

    function getDesde()
    {
        return $this->desde;
    }

    function getHasta()
    {
        return $this->hasta;
    }

    function getFechatodos()
    {
        return $this->fechatodos;
    }

    function getResponsable()
    {
        return $this->responsable;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function setDesde($desde)
    {
        $this->desde = $desde;
    }

    function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    function setFechatodos($fechatodos)
    {
        $this->fechatodos = $fechatodos;
    }

    function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }


    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }
    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }
    function getContratista()
    {
        return $this->contratista;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }
}
