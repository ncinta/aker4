<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of TareaOtComentario
 *
 * @author nicolas
 *
 * @ORM\Table(name="tareaot_comentario")
 * @ORM\Entity(repositoryClass="App\Repository\TareaOtComentarioRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class TareaOtComentario
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var string $fecha
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TareaOt", inversedBy="comentarios", cascade={"persist"})
     * @ORM\JoinColumn(name="tareaot_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $tarea;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="tareaotComentario")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    function getId()
    {
        return $this->id;
    }

    function getComentario()
    {
        return $this->comentario;
    }

    function getTarea()
    {
        return $this->tarea;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }

    function setTarea($tarea)
    {
        $this->tarea = $tarea;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function getEjecutor()
    {
        return $this->ejecutor;
    }

    function setEjecutor(Usuario $ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }
}
