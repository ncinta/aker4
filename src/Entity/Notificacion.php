<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Notificacion
 *
 * @ORM\Table(name="notificacion")
 * @ORM\Entity(repositoryClass="App\Repository\NotificacionRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Notificacion
{

    //put your code here
    const DESTINO_SMS = 1;
    const DESTINO_PUSH = 2;
    const DESTINO_EMAIL = 3;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string mensaje
     * 
     * @ORM\Column(name="mensaje", type="text", nullable=false)
     */
    private $mensaje;

    /**
     * @var string destino
     * 
     * @ORM\Column(name="destino", type="string", nullable=false)
     */
    private $destino;
    
    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

     /**
     * @ORM\Column(name="tipo", type="integer")
     */
    private $tipo;

    /**
     * indica por que medio debe notificarse al contacto. 1 = SMS 2 = Email 3 = Push
     * @ORM\Column(name="medio", type="integer", nullable=true)
     */
    private $medio;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Itinerario", mappedBy="notificaciones")
     */
    private $itinerarios;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Poi", mappedBy="notificaciones")
     */
    private $pois;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Contacto", mappedBy="notificacionesContacto")
     */
    private $contactos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bitacora", mappedBy="notificacion");
     */
    protected $bitacora;



    public function getCodeSMS()
    {
        return self::DESTINO_SMS;
    }

    public function getCodePUSH()
    {
        return self::DESTINO_PUSH;
    }

    public function getCodeEMAIL()
    {
        return self::DESTINO_EMAIL;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->created_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }


    /**
     * Get inverse Side
     */
    public function getItinerarios()
    {
        return $this->itinerarios;
    }

    /**
     * Set inverse Side
     *
     * @return  self
     */
    public function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;

        return $this;
    }

    /**
     * Get inverse Side
     */
    public function getPois()
    {
        return $this->pois;
    }

    /**
     * Set inverse Side
     *
     * @return  self
     */
    public function setPois($pois)
    {
        $this->pois = $pois;

        return $this;
    }

    /**
     * Get the value of bitacora
     */
    public function getBitacora()
    {
        return $this->bitacora;
    }

    /**
     * Set the value of bitacora
     *
     * @return  self
     */
    public function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;

        return $this;
    }

    /**
     * Get inverse Side
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set inverse Side
     *
     * @return  self
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;

        return $this;
    }

    /**
     * Get indica por que medio debe notificarse al contacto. 1 = SMS 2 = Email 3 = Push
     */ 
    public function getMedio()
    {
        return $this->medio;
    }

    /**
     * Set indica por que medio debe notificarse al contacto. 1 = SMS 2 = Email 3 = Push
     *
     * @return  self
     */ 
    public function setMedio($medio)
    {
        $this->medio = $medio;

        return $this;
    }

    function addItinerarios($itinerario)
    {
        $this->itinerarios[] = $itinerario;
    }
    
    function addContactos($contacto)
    {
        $this->contactos[] = $contacto;
    }


    /**
     * Get mensaje
     *
     * @return  string
     */ 
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set mensaje
     *
     * @param  string  $mensaje  mensaje
     *
     * @return  self
     */ 
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get destino
     *
     * @return  string
     */ 
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set destino
     *
     * @param  string  $destino  destino
     *
     * @return  self
     */ 
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get the value of tipo
     */ 
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @return  self
     */ 
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }
}
