<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\BitacoraEquipo
 *
 * @ORM\Table(name="bitacoraequipo")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraEquipoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BitacoraEquipo
{

    const EVENTO_INDEFINIDO = 0;
    const EVENTO_EQUIPO_SERVICIO_ASIGNAR = 1;
    const EVENTO_EQUIPO_SERVICIO_RETIRAR = 2;
    const EVENTO_EQUIPO_TRANSFERIR = 3;
    const EVENTO_EQUIPO_NEW = 4;
    const EVENTO_EQUIPO_UPDATE = 5;
    const EVENTO_EQUIPO_CHIP_ASIGNAR = 6;
    const EVENTO_EQUIPO_CHIP_RETIRAR = 7;

    private $TipoEventoDescipcion = array(
        self::EVENTO_INDEFINIDO => array('desc' => 'Indefinido', 'nivel' => 0),
        self::EVENTO_EQUIPO_SERVICIO_ASIGNAR => array('desc' => 'Asignación a servicio', 'nivel' => 0),
        self::EVENTO_EQUIPO_SERVICIO_RETIRAR => array('desc' => 'Retiro de servicio', 'nivel' => 0),
        self::EVENTO_EQUIPO_TRANSFERIR => array('desc' => 'Transferencia', 'nivel' => 0),
        self::EVENTO_EQUIPO_NEW => array('desc' => 'Nuevo', 'nivel' => 0),
        self::EVENTO_EQUIPO_UPDATE => array('desc' => 'Actualización', 'nivel' => 0),
        self::EVENTO_EQUIPO_CHIP_ASIGNAR => array('desc' => 'Asignación de chip', 'nivel' => 0),
        self::EVENTO_EQUIPO_CHIP_RETIRAR => array('desc' => 'Retiro de chip', 'nivel' => 0),
    );

    public function getArrayTipoEvento()
    {
        return $this->TipoEventoDescipcion;
    }

    public function getStrTipoEvento()
    {
        if (isset($this->tipo_evento)) {
            return $this->TipoEventoDescipcion[$this->tipo_evento]['desc'];
        } else {
            return '---';
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="tipo_evento", type="integer")
     */
    private $tipo_evento;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipo", inversedBy="bitacoraEquipo")     
     * @ORM\JoinColumn(name="equipo_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $equipo;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        if ($this->estado != null) {
            return $this->TipoEventoDescipcion[$this->tipo_evento];
        } else {
            return $this->TipoEventoDescipcion[0];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getTipoEvento()
    {
        return $this->tipo_evento;
    }

    public function setTipoEvento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function getEquipo()
    {
        return $this->equipo;
    }

    public function setEquipo(Equipo $equipo)
    {
        $this->equipo = $equipo;
        return $this;
    }
}
