<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="itinerario")
 * @ORM\Entity(repositoryClass="App\Repository\ItinerarioRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Itinerario
{

    const ESTADO_NUEVO = 0;      //estado = cuando se carga el it. es el inicio de todo. default
    const ESTADO_ENCURSO = 1;    //cuando esta iniciado el itinerario
    const ESTADO_AUDITORIA = 2;    //cuando se realizan las pruebas. no lo puedo poner con valor 1 porque va van a joder los itis ya creados
    const ESTADO_IMPLANTADO = 3;   
    const ESTADO_PENDIENTE = 4;   
    const ESTADO_CANCELADO = 5;   
    const ESTADO_PERNOCTADO = 6;   
    const ESTADO_DEMORADO = 7;   
    const ESTADO_DESCONOCE = 8;   
    const ESTADO_CERRADO = 9;    //estado = cuando todo esta terminado
    const ESTADO_ELIMINADO = 10;
    const ESTADO_REFERENCIA_OK = 0;
    const ESTADO_REFERENCIA_OUTRANK = 1; //fuera del rango del itineraro fuera de la fecha inicio y fin
    const ESTADO_REFERENCIA_GREATER = 2; // fecha mayor a la posterior
    const ESTADO_REFERENCIA_LESS = 3; // fecha menor a la anterior

    const MODO_MANUAL = 0;
    const MODO_EXCEL = 1;
    const MODO_TEMPLATE = 2;

    private $strEstado = array(
        'Nuevo' => self::ESTADO_NUEVO,
        'En curso' => self::ESTADO_ENCURSO,        
        'Auditoría' => self::ESTADO_AUDITORIA,
        'Implantado' => self::ESTADO_IMPLANTADO,
        'Pendiente' => self::ESTADO_PENDIENTE,
        'Cancelado' => self::ESTADO_CANCELADO,
        'Pernoctado' => self::ESTADO_PERNOCTADO,        
        'Demorado' => self::ESTADO_DEMORADO,
        'Desconoce' => self::ESTADO_DESCONOCE,
        'Cerrado' => self::ESTADO_CERRADO,
        'Eliminado' => self::ESTADO_ELIMINADO,
    );

    private $badge = array(
        self::ESTADO_NUEVO => 'default',
        self::ESTADO_ENCURSO => 'success',
        self::ESTADO_AUDITORIA => 'warning',
        self::ESTADO_IMPLANTADO => 'success',
        self::ESTADO_PENDIENTE => 'warning',
        self::ESTADO_CANCELADO => 'dark',
        self::ESTADO_PERNOCTADO => 'danger',
        self::ESTADO_DEMORADO => 'warning',
        self::ESTADO_DESCONOCE => 'warning',
        self::ESTADO_CERRADO => 'default',
        self::ESTADO_ELIMINADO => 'dark',
    );
    private $strOrder = array(
        'Ok' => self::ESTADO_REFERENCIA_OK,
        'Fuera de rango de itinerario' => self::ESTADO_REFERENCIA_OUTRANK,
        'Mayor a la referencia posterior' => self::ESTADO_REFERENCIA_GREATER,
        'Menor a la referencia anterior' => self::ESTADO_REFERENCIA_LESS,
    );

    private $colorOrder = array(
        '#FFFFFF' => self::ESTADO_REFERENCIA_OK,
        '#EC7063' => self::ESTADO_REFERENCIA_OUTRANK,
        '#F5B7B1' => self::ESTADO_REFERENCIA_GREATER,
        '#DC143C' => self::ESTADO_REFERENCIA_LESS,
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }


    private $strModoIngreso = array(
        self::MODO_MANUAL => 'Manual',
        self::MODO_EXCEL => 'Excel',
        self::MODO_TEMPLATE => 'Template',
    );

    public function getArrayStrModoIngreso()
    {
        return $this->strModoIngreso;
    }

    public function getStrModoIngreso()
    {
        return isset($this->strModoIngreso[$this->modo_ingreso]) ? $this->strModoIngreso[$this->modo_ingreso] : '---';
    }

    /**
     * pueso pasarle un estado para consultar el str seteado, si no paso nada devuelve el estado del entity
     */
    public function getStrEstado($estado = null)
    {
        if (is_null($estado)) {
            return array_search($this->estado, $this->strEstado);
        } else {
            return array_search($estado, $this->strEstado);
        }
    }

    public function getStrBadge()
    {
        return $this->badge[$this->estado];
    }

    public function getColorOrder($order)
    {
        return array_search($order, $this->colorOrder);
    }

    public function getArrayStrOrder()
    {
        return $this->strOrder;
    }

    public function getStrOrder($orden)
    {
        return array_search($orden, $this->strOrder);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="codigo", type="string", length=255)
     */
    private $codigo;

    /**
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable = true) 
     */
    private $fechaInicio;

    /**
     * @ORM\Column(name="fecha_fin", type="datetime", nullable = true) 
     */
    private $fechaFin;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="estado", type="integer", nullable = true)
     * 
     */
    private $estado;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="nota", type="text", nullable=true)
     * 
     */
    private $nota;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="referencia_externa", type="string", nullable=true)
     * 
     */
    private $referencia_externa;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="cliente_externo", type="string", nullable=true)
     * 
     */
    private $cliente_externo;

    /**
     * 0=manual 1=excel, 2=template
     * @ORM\Column(name="modo_ingreso", type="integer", nullable = true)
     */
    private $modo_ingreso;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="itinerarios")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    private $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Empresa", inversedBy="itinerarios")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     */
    private $empresa;

    /**
     * Owning Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ItinerarioServicio", mappedBy="itinerario", cascade={"persist"})
     */
    private $servicios;

    /**
     * Pois
     * @ORM\OneToMany(targetEntity="App\Entity\Poi", mappedBy="itinerario", cascade={"persist"})
     */
    protected $pois;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", inversedBy="itinerarios")
     */
    private $usuarios;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Contacto", inversedBy="itinerarios")
     */
    private $contactos;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Notificacion", inversedBy="itinerarios")
     * @ORM\JoinTable(name="itinerarioNotificacion")
     */
    private $notificaciones;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Logistica", inversedBy="itinerarios")
     * @ORM\JoinColumn(name="logistica_id", referencedColumnName="id")
     */
    private $logistica;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bitacora", mappedBy="itinerario");
     */
    protected $bitacora;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoItinerario", mappedBy="itinerario");
     */
    protected $eventoItinerario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="itinerario");
     */
    protected $eventoTemporal;

    /**
     * @ORM\OneToOne(targetEntity="GrupoReferencia", inversedBy="itinerario")
     * @ORM\JoinColumn(name="gruporeferencia_id", referencedColumnName="id", onDelete="SET NULL", nullable=true))
     */
    private $grupoReferencia;


    //==========================================================================    
    //==========================================================================    
    //==========================================================================


    public function __construct()
    {
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notificaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contactos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pois = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->eventos  = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function __toString()
    {
        return $this->codigo;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getServicios()
    {
        return $this->servicios;
    }

    function setServicios($servicios)
    {
        $this->servicios = $servicios;
    }

    function getUsuarios()
    {
        return $this->usuarios;
    }

    function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;
    }

    function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    function getFechaFin()
    {
        return $this->fechaFin;
    }

    function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    }

    function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    function getEmpresa()
    {
        return $this->empresa;
    }

    function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    function addServicio($servicio)
    {
        if (!$this->servicios || !$this->servicios->contains($servicio)) {
            $this->servicios[] = $servicio;
        }
    }

    function addNotificacion($notificacion)
    {
        if (!$this->notificaciones  || !$this->notificaciones->contains($notificacion)) {
            $this->notificaciones[] = $notificacion;
        }
    }

    function addUsuario($usuario)
    {
        if (!$this->usuarios || !$this->usuarios->contains($usuario)) {
            $this->usuarios[] = $usuario;
        }
    }

    function addContacto($contacto)
    {
        if (!$this->contactos || !$this->contactos->contains($contacto)) {
            $this->contactos[] = $contacto;
        }
    }

    function addEvento($evento)
    {
        if (!$this->eventos || !$this->eventos->contains($evento)) {
            $this->eventos[] = $evento;
        }
    }

    function addPoi($poi)
    {
        if (!$this->pois || !$this->pois->contains($poi)) {
            $this->pois[] = $poi;
        }
    }

    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
    }

    public function removeUsuario($usuario)
    {
        $this->usuarios->removeElement($usuario);
    }

    public function removeContacto($contacto)
    {
        $this->contactos->removeElement($contacto);
    }

    public function removeEvento($evento)
    {
        $this->eventos->removeElement($evento);
    }

    public function removeNotificacion($notificacion)
    {
        $this->notificaciones->removeElement($notificacion);
    }

    function getEstado()
    {
        if ($this->estado == null) {
            return 0;
        }
        return $this->estado;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getNota()
    {
        return $this->nota;
    }

    function setNota($nota)
    {
        $this->nota = $nota;
    }


    function setBadge($badge)
    {
        $this->badge = $badge;
    }
    function getBadge()
    {
        return $this->badge;
    }

    /**
     * Get pois
     */
    public function getPois()
    {
        return $this->pois;
    }

   
    public function setPois($pois)
    {
        $this->pois = $pois;

    }

    /**
     * Get the value of logistica
     */
    public function getLogistica()
    {
        return $this->logistica;
    }

 
    public function setLogistica($logistica)
    {
        $this->logistica = $logistica;

    }

    /**
     * Get the value of bitacora
     */
    public function getBitacora()
    {
        return $this->bitacora;
    }

    /**
     * Set the value of bitacora
     *
     * @return  self
     */
    public function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;

        return $this;
    }

    /**
     * Get owning Side
     */
    public function getNotificaciones()
    {
        return $this->notificaciones;
    }

    /**
     * Set owning Side
     *
     * @return  self
     */
    public function setNotificaciones($notificaciones)
    {
        $this->notificaciones = $notificaciones;

        return $this;
    }

    /**
     * Get owning Side
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set owning Side
     *
     * @return  self
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;

        return $this;
    }


    /**
     * Get the value of eventoItinerario
     */
    public function getEventoItinerario()
    {
        return $this->eventoItinerario;
    }


    public function setEventoItinerario($eventoItinerario)
    {
        $this->eventoItinerario = $eventoItinerario;

        return $this;
    }

    /**
     * Get the value of grupoReferencia
     */
    public function getGrupoReferencia()
    {
        return $this->grupoReferencia;
    }

    /**
     * Set the value of grupoReferencia
     *
     * @return  self
     */
    public function setGrupoReferencia($grupoReferencia)
    {
        $this->grupoReferencia = $grupoReferencia;

        return $this;
    }

    public function removeGrupoReferencia()
    {
        $this->grupoReferencia = null;
    }


    /**
     * Get the value of eventoTemporal
     */
    public function getEventoTemporal()
    {
        return $this->eventoTemporal;
    }

    /**
     * Set the value of eventoTemporal
     *
     * @return  self
     */
    public function setEventoTemporal($eventoTemporal)
    {
        $this->eventoTemporal = $eventoTemporal;

        return $this;
    }

    /**
     * Get 0=manual 1=excel, 2=template
     */ 
    public function getModoIngreso()
    {
        return $this->modo_ingreso;
    }

 
    public function setModoIngreso($modo_ingreso)
    {
        $this->modo_ingreso = $modo_ingreso;

    }

    /**
     * Get $estado
     *
     * @return  integer
     */ 
    public function getReferencia_externa()
    {
        return $this->referencia_externa;
    }

    /**
     * Set $estado
     *
     * @param  integer  $referencia_externa  $estado
     *
     * @return  self
     */ 
    public function setReferencia_externa($referencia_externa)
    {
        $this->referencia_externa = $referencia_externa;

        return $this;
    }

    /**
     * Get $estado
     *
     * @return  integer
     */ 
    public function getCliente_externo()
    {
        return $this->cliente_externo;
    }

    /**
     * Set $estado
     *
     * @param  integer  $cliente_externo  $estado
     *
     * @return  self
     */ 
    public function setCliente_externo($cliente_externo)
    {
        $this->cliente_externo = $cliente_externo;

        return $this;
    }
}
