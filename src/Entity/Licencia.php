<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Licencia
 *
 * @author nicolas
 */

/**
 * App\Entity\Licencia
 *
 * @ORM\Table(name="licencia")
 * @ORM\Entity(repositoryClass="App\Repository\LicenciaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Licencia
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var date expedicion
     *
     * @ORM\Column(name="expedicion", type="datetime", nullable=true)
     */
    private $expedicion;

    /**
     * @var date vencimiento
     *
     * @ORM\Column(name="vencimiento", type="datetime", nullable=true)
     */
    private $vencimiento;

    /**
     * @var string numero
     *
     * @ORM\Column(name="numero", type="string", nullable=true)
     */
    private $numero;


    /**
     * @var string categoria
     *
     * @ORM\Column(name="categoria", type="string", nullable=true)
     */
    private $categoria;

    /**
     * dias de anticipacion para avisar 
     * @var integer aviso
     *
     * @ORM\Column(name="aviso", type="integer", nullable=true)
     */
    private $aviso;

    /**
     * @ORM\ManyToOne(targetEntity="Chofer", inversedBy="licencias")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $chofer;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getExpedicion()
    {
        return $this->expedicion;
    }

    function getVencimiento()
    {
        return $this->vencimiento;
    }

    function getNumero()
    {
        return $this->numero;
    }


    function getCategoria()
    {
        return $this->categoria;
    }

    function getChofer()
    {
        return $this->chofer;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setExpedicion($expedicion)
    {
        $this->expedicion = $expedicion;
    }

    function setVencimiento($vencimiento)
    {
        $this->vencimiento = $vencimiento;
    }

    function setNumero($numero)
    {
        $this->numero = $numero;
    }

    function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    function setChofer($chofer)
    {
        $this->chofer = $chofer;
    }

    function getAviso()
    {
        return $this->aviso;
    }

    function setAviso($aviso)
    {
        $this->aviso = $aviso;
    }
}
