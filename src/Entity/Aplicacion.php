<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad que modela las aplicaciones que hay en el sistema calypso.
 * App\Entity\Aplicacion
 *
 * @ORM\Table(name="aplicacion")
 * @ORM\Entity
 */
class Aplicacion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var boolean $habilitada
     *
     * @ORM\Column(name="habilitada", type="boolean", nullable=true)
     */
    private $habilitada;

    /**
     * @var string $route
     *
     * @ORM\Column(name="route", type="string", nullable=true)
     */
    private $route;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string $pathIcono
     *
     * @ORM\Column(name="pathIcono", type="string", nullable=true)
     */
    private $pathIcono;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $icono;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Modulo", mappedBy="aplicacion")
     */
    protected $modulos;

    public function __construct()
    {
        $this->modulos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set habilitada
     *
     * @param boolean $habilitada
     */
    public function setHabilitada($habilitada)
    {
        $this->habilitada = $habilitada;
    }

    /**
     * Get habilitada
     *
     * @return boolean 
     */
    public function isHabilitada()
    {
        return $this->habilitada;
    }

    /**
     * Add modulos
     *
     * @param App\Entity\Modulo $modulos
     */
    public function addModulo(\App\Entity\Modulo $modulos)
    {
        $this->modulos[] = $modulos;
    }

    /**
     * Get modulos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getModulos()
    {
        return $this->modulos;
    }

    /**
     * Get habilitada
     *
     * @return boolean 
     */
    public function getHabilitada()
    {
        return $this->habilitada;
    }

    /**
     * Set icono
     *
     * @param string $icono
     */
    public function setPathIcono($pathIcono)
    {
        $this->pathIcono = $pathIcono;
    }

    /**
     * Get icono
     *
     * @return string 
     */
    public function getPathIcono()
    {
        return $this->pathIcono;
    }

    /**
     * Set route
     *
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set descripcion
     *
     * @param text $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * Get descripcion
     *
     * @return text 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getAbsolutePath()
    {
        return null === $this->pathIcono ? null : $this->getUploadRootDir() . '/' . $this->pathIcono;
    }

    public function getWebPath()
    {
        return null === $this->pathIcono ? null : $this->getUploadDir() . '/' . $this->pathIcono;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->icono) {
            return;
        }

        $extension = $this->icono->guessExtension();
        if (!$extension) {
            // extension cannot be guessed
            $extension = 'bin';
        }

        $randomName = rand(1, 99999) . '.' . $extension;

        $this->icono->move($this->getUploadRootDir(), $randomName);
        // move takes the target directory and then the target filename to move to
        //$this->icono->move($this->getUploadRootDir(), $this->icono->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->pathIcono = $randomName;

        // clean up the file property as you won't need it anymore
        $this->icono = null;
    }
}
