<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * App\Entity\Vehiculo
 *
 * @ORM\Table(name="registroshell")
 * @ORM\Entity(repositoryClass="App\Repository\RegistroShellRepository")
 */
class RegistroShell
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $patente
     *
     * @ORM\Column(name="patente", type="string", length=255, nullable=true, unique=true)
     */
    private $patente;

    /**
     * @var datetime $fecha_alta
     *
     * @ORM\Column(name="fecha_alta", type="datetime", nullable=false)
     */
    private $fecha_alta;

    /**
     * @var datetime $fecha_baja
     *
     * @ORM\Column(name="fecha_baja", type="datetime", nullable=true)
     */
    private $fecha_baja;

    /**
     * @var datetime $procesado_alta
     *
     * @ORM\Column(name="procesado_alta", type="datetime", nullable=true)
     */
    private $procesado_alta;

    /**
     * @var datetime $procesado_baja
     *
     * @ORM\Column(name="procesado_baja", type="datetime", nullable=true)
     */
    private $procesado_baja;

    /**
     * Esto corresponde al RUT que tenga el servicio. Lo han pedido los de chile para 
     * shell. Se da en los casos en que un cliente tenga varios camiones y cada uno de 
     * ellos tenga un rut diferente con el que se deba informar a transtel.
     * @var string $ultValido
     *
     * @ORM\Column(name="dato_fiscal", type="string", nullable=true)
     */
    private $dato_fiscal;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Servicio", mappedBy="registro_shell")
     */
    protected $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set patente
     *
     * @param string $patente
     */
    public function setPatente($patente)
    {
        $this->patente = $patente;
    }

    /**
     * Get patente
     *
     * @return string 
     */
    public function getPatente()
    {
        return $this->patente;
    }

    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    public function setFechaAlta($fecha_alta)
    {
        $this->fecha_alta = $fecha_alta;
    }

    public function getFechaBaja()
    {
        return $this->fecha_baja;
    }

    public function setFechaBaja($fecha_baja)
    {
        $this->fecha_baja = $fecha_baja;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    public function getDatoFiscal()
    {
        return $this->dato_fiscal;
    }

    public function setDatoFiscal($dato_fiscal)
    {
        $this->dato_fiscal = $dato_fiscal;
    }
    public function getProcesadoAlta()
    {
        return $this->procesado_alta;
    }

    public function setProcesadoAlta($procesado_alta)
    {
        $this->procesado_alta = $procesado_alta;
        return $this;
    }

    public function getProcesadoBaja()
    {
        return $this->procesado_baja;
    }

    public function setProcesadoBaja($procesado_baja)
    {
        $this->procesado_baja = $procesado_baja;
        return $this;
    }
}
