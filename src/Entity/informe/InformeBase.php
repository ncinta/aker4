<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class InformeBase
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private $uid;

    /**
     *
     * @ORM\Column(name="fecha_inicio", type="string", nullable=true)
     */
    private $fecha_inicio;

    /**
     *
     * @ORM\Column(name="fecha_fin", type="string", nullable=true)
     */
    private $fecha_fin;


    /**
     *
     * @ORM\Column(name="sid", type="string", nullable=true)
     */
    private $sid;

    /**
     *
     * @ORM\Column(name="entidad_solicitante", type="string", nullable=true)
     */
    private $entidad_solicitante;

    /**
     *
     * @ORM\Column(name="id_solicitante", type="integer", nullable=true)
     */
    private $id_solicitante;

    /**
     *
     * @ORM\Column(name="uid_solicitante", type="string", nullable=true)
     */
    private $uid_solicitante;

    /**
     *
     * @ORM\Column(name="id_organizacion", type="integer", nullable=true)
     */
    private $id_organizacion;

    /**
     *
     * @ORM\Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     *
     * @ORM\Column(name="owner", type="string", nullable=true)
     */
    private $owner;

    /**
     *
     * @ORM\Column(name="estado", type="string", nullable=true)
     */
    private $estado;


    /**
     * Get the value of uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     *
     * @return  self
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

}
