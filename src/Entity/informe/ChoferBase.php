<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\MappedSuperclass
 */
abstract class ChoferBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre",type="string", length=63)
     */
    private $nombre;

    /**
     * @ORM\Column(name="fecha_asignacion",type="string", length=63)
     */
    private $fechaAsignacion;

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of fechaAsignacion
     */
    public function getFechaAsignacion()
    {
        return $this->fechaAsignacion;
    }
}
