<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\MappedSuperclass
 */
abstract class ReferenciaBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre",type="string", length=45)
     */
    private $nombre;

    /**
     * @ORM\Column(name="clase",type="boolean")
     */
    private $clase;

    /**
     * @ORM\Column(name="velocidad_maxima",type="smallint")
     */
    private $velocidadMaxima;

    /**
     * @ORM\Column(name="latitud",type="float")
     */
    private $latitud;

    /**
     * @ORM\Column(name="longitud",type="float")
     */
    private $longitud;

    /**
     * @ORM\Column(name="radio",type="smallint")
     */
    private $radio;
   

    // Getters y setters...

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of clase
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Get the value of velocidadMaxima
     */
    public function getVelocidadMaxima()
    {
        return $this->velocidadMaxima;
    }

    /**
     * Get the value of latitud
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Get the value of longitud
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Get the value of radio
     */
    public function getRadio()
    {
        return $this->radio;
    }

}
