<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\MappedSuperclass
 */
abstract class PosicionBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   
    /**
     * @ORM\Column(name="id_reporte",type="string", length=12)
     */
    private $idReporte;

    /**
     * @ORM\Column(name="latitud",type="float")
     */
    private $latitud;

    /**
     * @ORM\Column(name="longitud",type="float")
     */
    private $longitud;

    /**
     * @ORM\Column(name="velocidad",type="smallint")
     */
    private $velocidad;

    /**
     * @ORM\Column(name="fecha",type="datetime")
     */
    private $fecha;


    /**
     * @ORM\Column(name="tipo_exceso",type="string", length=31)
     */
    private $tipoExceso;

    /**
     * @ORM\Column(name="riesgo",type="string", length=31)
     */
    private $riesgo;

    /**
     * @ORM\Column(name="osm_display_name",type="string", length=31)
     */
    private $osmDireccion;

    /**
     * @ORM\Column(name="nombre_parametro",type="string", length=31)
     */
    private $nombreParametro;

    /**
     * @ORM\Column(name="maxima_permitida",type="smallint")
     */
    private $maximaPermitida;



    // Getters y setters...

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of idReporte
     */
    public function getIdReporte()
    {
        return $this->idReporte;
    }

    /**
     * Get the value of latitud
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Get the value of longitud
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Get the value of velocidad
     */
    public function getVelocidad()
    {
        return $this->velocidad;
    }


    /**
     * Get the value of fecha
     */
    public function getFecha()
    {
        return $this->fecha;
    }


    /**
     * Get the value of tipoExceso
     */
    public function getTipoExceso()
    {
        return $this->tipoExceso;
    }

    /**
     * Get the value of nombreParametro
     */
    public function getNombreParametro()
    {
        return $this->nombreParametro;
    }

    /**
     * Get the value of maximaPermitida
     */
    public function getMaximaPermitida()
    {
        return $this->maximaPermitida;
    }





    /**
     * Get the value of osmDireccion
     */ 
    public function getOsmDireccion()
    {
        return $this->osmDireccion;
    }

    /**
     * Set the value of osmDireccion
     *
     * @return  self
     */ 
    public function setOsmDireccion($osmDireccion)
    {
        $this->osmDireccion = $osmDireccion;

        return $this;
    }


    /**
     * Get the value of riesgo
     */ 
    public function getRiesgo()
    {
        return $this->riesgo;
    }

    /**
     * Set the value of riesgo
     *
     * @return  self
     */ 
    public function setRiesgo($riesgo)
    {
        $this->riesgo = $riesgo;

        return $this;
    }
}
