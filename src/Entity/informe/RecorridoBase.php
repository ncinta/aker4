<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class RecorridoBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="tiempo_total_segundos", type="integer")
     */
    private $tiempoTotal;

    /**
     * @ORM\Column(name="tiempo_ralenti_segundos",type="integer")
     */
    private $tiempoRalenti;

    /**
     * @ORM\Column(name="tiempo_detenido_segundos", type="integer")
     */
    private $tiempoDetenido;

    /**
     * @ORM\Column(name="metros_recorridos", type="integer")
     */
    private $metrosRecorridos;


    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }
  

    /**
     * Get the value of metrosRecorridos
     */ 
    public function getMetrosRecorridos()
    {
        return $this->metrosRecorridos;
    }

    /**
     * Set the value of metrosRecorridos
     *
     * @return  self
     */ 
    public function setMetrosRecorridos($metrosRecorridos)
    {
        $this->metrosRecorridos = $metrosRecorridos;

        return $this;
    }

    /**
     * Get the value of tiempoDetenido
     */ 
    public function getTiempoDetenido()
    {
        return $this->tiempoDetenido;
    }

    /**
     * Set the value of tiempoDetenido
     *
     * @return  self
     */ 
    public function setTiempoDetenido($tiempoDetenido)
    {
        $this->tiempoDetenido = $tiempoDetenido;

        return $this;
    }

    /**
     * Get the value of tiempoRalenti
     */ 
    public function getTiempoRalenti()
    {
        return $this->tiempoRalenti;
    }

    /**
     * Set the value of tiempoRalenti
     *
     * @return  self
     */ 
    public function setTiempoRalenti($tiempoRalenti)
    {
        $this->tiempoRalenti = $tiempoRalenti;

        return $this;
    }

    /**
     * Get the value of tiempoTotal
     */ 
    public function getTiempoTotal()
    {
        return $this->tiempoTotal;
    }

    /**
     * Set the value of tiempoTotal
     *
     * @return  self
     */ 
    public function setTiempoTotal($tiempoTotal)
    {
        $this->tiempoTotal = $tiempoTotal;

        return $this;
    }
}
