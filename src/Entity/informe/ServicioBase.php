<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\MappedSuperclass
 */
abstract class ServicioBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre",type="string", length=63)
     */
    private $nombre;

    /**
     * @ORM\Column(name="patente",type="string", length=15)
     */
    private $patente;

    /**
     * @ORM\Column(name="tipo_servicio",type="string", length=31)
     */
    private $tipo_servicio;




    /**
     * Get the value of tipo_servicio
     */
    public function getTipo_servicio()
    {
        return $this->tipo_servicio;
    }

    /**
     * Get the value of patente
     */
    public function getPatente()
    {
        return $this->patente;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

}
