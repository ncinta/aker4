<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class ExcesoBase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(name="maxima_alcanzada", type="float")
     */
    private $maximaAlcanzada;

    /**
     * @ORM\Column(name="maxima_promedio",type="float")
     */
    private $maximaPromedio;



    /**
     * Get the value of maximaAlcanzada
     */
    public function getMaximaAlcanzada()
    {
        return $this->maximaAlcanzada;
    }

    /**
     * Get the value of maximaPromedio
     */
    public function getMaximaPromedio()
    {
        return $this->maximaPromedio;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }
       
}
