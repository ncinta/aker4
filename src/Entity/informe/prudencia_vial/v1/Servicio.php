<?php

namespace App\Entity\informe\prudencia_vial\v1;

use App\Entity\informe\ServicioBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="prudencia_vial_v1.servicios")
 * @ORM\Entity(repositoryClass="App\Repository\informe\prudencia_vial\v1\ServicioRepository")
 */
class Servicio extends ServicioBase
{


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\prudencia_vial\v1\Exceso::class, mappedBy="servicio")
     */
    private $excesos;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\prudencia_vial\v1\Posicion::class, mappedBy="servicio")
     */
    private $posiciones;

    public function __construct()
    {
        $this->excesos = new ArrayCollection();
        $this->posiciones = new ArrayCollection();
    }

    /**
     * Get the value of excesos
     */
    public function getExcesos()
    {
        return $this->excesos;
    }
    

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }
}
