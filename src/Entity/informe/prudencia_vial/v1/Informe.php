<?php

namespace App\Entity\informe\prudencia_vial\v1;

use App\Entity\informe\InformeBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\prudencia_vial\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\prudencia_vial\v1\InformeRepository")
 * @ORM\Table(name="prudencia_vial_v1.informes")
 */
class Informe extends InformeBase
{


    /**
     *
     * @ORM\Column(name="vel_max_en_ruta", type="integer", nullable=true)
     */
    private $velMaxRuta;

    /**
     *
     * @ORM\Column(name="vel_max_en_avenida", type="integer", nullable=true)
     */
    private $velMaxAvenida;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\prudencia_vial\v1\Exceso::class, mappedBy="informe")
     */
    private $excesos;

    public function __construct()
    {
        $this->excesos = new ArrayCollection();
    }


    /**
     * Get the value of excesos
     */
    public function getExcesos()
    {
        return $this->excesos;
    }

    /**
     * Set the value of excesos
     *
     * @return  self
     */
    public function setExcesos($excesos)
    {
        $this->excesos = $excesos;

        return $this;
    }


    /**
     * Get the value of velMaxAvenida
     */
    public function getVelMaxAvenida()
    {
        return $this->velMaxAvenida;
    }

    /**
     * Set the value of velMaxAvenida
     *
     * @return  self
     */
    public function setVelMaxAvenida($velMaxAvenida)
    {
        $this->velMaxAvenida = $velMaxAvenida;

        return $this;
    }

    /**
     * Get the value of velMaxRuta
     */
    public function getVelMaxRuta()
    {
        return $this->velMaxRuta;
    }

    /**
     * Set the value of velMaxRuta
     *
     * @return  self
     */
    public function setVelMaxRuta($velMaxRuta)
    {
        $this->velMaxRuta = $velMaxRuta;

        return $this;
    }

}
