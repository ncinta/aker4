<?php

namespace App\Entity\informe\prudencia_vial\v1;

use App\Entity\informe\ExcesoBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\prudencia_vial\v1\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\prudencia_vial\v1\ExcesoRepository")
 * @ORM\Table(name="prudencia_vial_v1.excesos")
 */
class Exceso extends ExcesoBase
{
    /**
     * @ORM\Column(name="tiempo_exceso", type="integer")
     */
    private $tiempoExceso;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\prudencia_vial\v1\Servicio::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_servicio", referencedColumnName="id", nullable=false)
     */
    private $servicio;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\prudencia_vial\v1\Informe::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="uid_informe",referencedColumnName="uid",nullable=false)
     */
    private $informe;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\prudencia_vial\v1\Posicion::class, inversedBy="excesoInicial")
     * @ORM\JoinColumn(name="id_posicion_inicial",referencedColumnName="id",nullable=false)
     */
    private $posicionInicial;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\prudencia_vial\v1\Posicion::class, inversedBy="excesoFinal")
     * @ORM\JoinColumn(name="id_posicion_final",referencedColumnName="id",nullable=false)
     */
    private $posicionFinal;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\prudencia_vial\v1\Posicion::class, mappedBy="exceso")
     */
    private $posiciones;

    public function __construct()
    {
        $this->posiciones = new ArrayCollection();
    }

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }

    /**
     * Get the value of posicionInicial
     */
    public function getPosicionInicial()
    {
        return $this->posicionInicial;
    }

    /**
     * Set the value of posicionInicial
     *
     * @return  self
     */
    public function setPosicionInicial($posicionInicial)
    {
        $this->posicionInicial = $posicionInicial;

        return $this;
    }

    /**
     * Get the value of posicionFinal
     */
    public function getPosicionFinal()
    {
        return $this->posicionFinal;
    }

    /**
     * Set the value of posicionFinal
     *
     * @return  self
     */
    public function setPosicionFinal($posicionFinal)
    {
        $this->posicionFinal = $posicionFinal;

        return $this;
    }

    /**
     * Get the value of informe
     */
    public function getInforme()
    {
        return $this->informe;
    }

    /**
     * Set the value of informe
     *
     * @return  self
     */
    public function setInforme($informe)
    {
        $this->informe = $informe;

        return $this;
    }

    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Get the value of tiempoExceso
     */ 
    public function getTiempoExceso()
    {
        return $this->tiempoExceso;
    }

    /**
     * Set the value of tiempoExceso
     *
     * @return  self
     */ 
    public function setTiempoExceso($tiempoExceso)
    {
        $this->tiempoExceso = $tiempoExceso;

        return $this;
    }
}
