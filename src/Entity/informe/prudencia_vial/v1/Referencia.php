<?php

namespace App\Entity\informe\prudencia_vial\v1;

use App\Entity\informe\ReferenciaBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\prudencia_vial\v1\ReferenciaRepository")
 * @ORM\Table(name="prudencia_vial_v1.referencias")
 */
class Referencia extends ReferenciaBase
{

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\prudencia_vial\v1\Posicion::class, mappedBy="referencia")
     */
    private $posiciones;

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }
}
