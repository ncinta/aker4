<?php

namespace App\Entity\informe;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="informe_inbox")
 * @ORM\Entity(repositoryClass="App\Repository\informe\InboxRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Inbox implements \Stringable
{
    const TIPO_DEFAULT = -1;
    const TIPO_PRUDENCIA_VIAL = 1;
    const TIPO_INDICE_RESPONSABILIDAD_VIAL = 2;
    const TIPO_EXCESO_VELOCIDAD = 3;
    const TIPO_INDICE_RESPONSABILIDAD_VIAL_V2 = 4;
    const TIPO_DISTANCIAS_RECORRIDAS = null;
    const TIPO_VELOCIDADES = null;
    const TIPO_DETENCIONES = null;
    const TIPO_DETENCIONES_CONSOLIDADO = null;
    const TIPO_DETENCIONES_RALENTI = null;
    const TIPO_HISTORIAL = null;
    const TIPO_PASO_REFERENCIA = null;
    const TIPO_RECORRIDO_REFERENCIA = null;
    const TIPO_PORTICO = null;
    const TIPO_INFRACCIONES = null;
    const TIPO_CHOFERES = null;
    const TIPO_TRABAJO_REFERENCIA = null; //agro
    const TIPO_DISTANCIA_TIEMPO = null; //agro

    private $schemas  = array(
        '---' => self::TIPO_DEFAULT,
        'prudencia_vial_v1' =>  self::TIPO_PRUDENCIA_VIAL,
        'responsabilidad_vial_v1' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL,
        'exceso_velocidad_v1' => self::TIPO_EXCESO_VELOCIDAD,
        'responsabilidad_vial_v2' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2, //por chofer
        '' => self::TIPO_DISTANCIAS_RECORRIDAS,
        '' => self::TIPO_VELOCIDADES,
        '' => self::TIPO_DETENCIONES,
        '' =>  self::TIPO_DETENCIONES_CONSOLIDADO,
        '' => self::TIPO_DETENCIONES_RALENTI,
        '' => self::TIPO_HISTORIAL,
        '' => self::TIPO_PASO_REFERENCIA,
        '' => self::TIPO_RECORRIDO_REFERENCIA,
        '' => self::TIPO_PORTICO,
        '' => self::TIPO_INFRACCIONES,
        '' => self::TIPO_CHOFERES,
        '' => self::TIPO_TRABAJO_REFERENCIA,
        '' => self::TIPO_DISTANCIA_TIEMPO,
    );

    private $tipos  = array(
        '---' => self::TIPO_DEFAULT,
        'Prudencia Vial' =>  self::TIPO_PRUDENCIA_VIAL,
        'Indice de responsabilidad vial' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL,
        'Indice de responsabilidad vial por Chofer' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2, //por chofer
        'Exceso de Velocidad' => self::TIPO_EXCESO_VELOCIDAD,
        'Distancias recorridas' => self::TIPO_DISTANCIAS_RECORRIDAS,
        'Velocidades' => self::TIPO_VELOCIDADES,
        'Detenciones' => self::TIPO_DETENCIONES,
        'Consolidado de Detenciones' =>  self::TIPO_DETENCIONES_CONSOLIDADO,
        'Detenciones en Ralenti' => self::TIPO_DETENCIONES_RALENTI,
        'Historial de posiciones' => self::TIPO_HISTORIAL,
        'Paso por Referencia' => self::TIPO_PASO_REFERENCIA,
        'Recorrido en Referencia' => self::TIPO_RECORRIDO_REFERENCIA,
        'Pórticos' => self::TIPO_PORTICO,
        'Infracciones viales' => self::TIPO_INFRACCIONES,
        'Choferes' => self::TIPO_CHOFERES,
        'Trabajo en Referencia' => self::TIPO_TRABAJO_REFERENCIA,
        'Distancia y Tiempo' => self::TIPO_DISTANCIA_TIEMPO,
    );

    //estos los utilizamos para las funciones
    private $informes  = array(
        '---' => self::TIPO_DEFAULT,
        'prudencia_vial' =>  self::TIPO_PRUDENCIA_VIAL,
        'indice_responsabilidad_vial' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL,
        'indice_responsabilidad_vial_v2' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2, //por chofer
        'exceso_velocidad' => self::TIPO_EXCESO_VELOCIDAD,
        'distancia_recorrida' => self::TIPO_DISTANCIAS_RECORRIDAS,
        'velocidades' => self::TIPO_VELOCIDADES,
        'detenciones' => self::TIPO_DETENCIONES,
        'consolidado_detenciones' =>  self::TIPO_DETENCIONES_CONSOLIDADO,
        'detenciones_ralenti' => self::TIPO_DETENCIONES_RALENTI,
        'historial_posiciones' => self::TIPO_HISTORIAL,
        'paso_referencia' => self::TIPO_PASO_REFERENCIA,
        'recorrido_referencia' => self::TIPO_RECORRIDO_REFERENCIA,
        'porticos' => self::TIPO_PORTICO,
        'infracciones_viales' => self::TIPO_INFRACCIONES,
        'choferes' => self::TIPO_CHOFERES,
        'trabajo_referencia' => self::TIPO_TRABAJO_REFERENCIA,
        'distancia_tiempo' => self::TIPO_DISTANCIA_TIEMPO,
    );


    //estos los utilizamos para los templates de informes
    private $templates  = array(
        '---' => self::TIPO_DEFAULT,
        'prudencia_vial/v1/index.html.twig' =>  self::TIPO_PRUDENCIA_VIAL,
        'responsabilidad_vial/v1/index.html.twig' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL,
        'responsabilidad_vial/v2/index.html.twig' => self::TIPO_INDICE_RESPONSABILIDAD_VIAL_V2,
        'exceso_velocidad/v1/index.html.twig' => self::TIPO_EXCESO_VELOCIDAD,
        'distancia_recorrida/index.html.twig' => self::TIPO_DISTANCIAS_RECORRIDAS,
        'velocidades/index.html.twig' => self::TIPO_VELOCIDADES,
        'detenciones/index.html.twig' => self::TIPO_DETENCIONES,
        'consolidado_detenciones/index.html.twig' =>  self::TIPO_DETENCIONES_CONSOLIDADO,
        'detenciones_ralenti/index.html.twig' => self::TIPO_DETENCIONES_RALENTI,
        'historial_posiciones/index.html.twig' => self::TIPO_HISTORIAL,
        'paso_referencia/index.html.twig' => self::TIPO_PASO_REFERENCIA,
        'recorrido_referencia/index.html.twig' => self::TIPO_RECORRIDO_REFERENCIA,
        'porticos/index.html.twig' => self::TIPO_PORTICO,
        'infracciones_viales/index.html.twig' => self::TIPO_INFRACCIONES,
        'choferes/index.html.twig' => self::TIPO_CHOFERES,
        'trabajo_referencia/index.html.twig' => self::TIPO_TRABAJO_REFERENCIA,
        'distancia_tiempo/index.html.twig' => self::TIPO_DISTANCIA_TIEMPO,
    );


    const ESTADO_EN_COLA = 0;
    const ESTADO_GENERANDO = 1;
    const ESTADO_EN_PROGRESO = 2;
    const ESTADO_FINALIZADO = 3;
    const ESTADO_ERROR = 4;

    private $estados = array(
        'En cola' => self::ESTADO_EN_COLA,
        'Generando...' => self::ESTADO_GENERANDO,
        'En Progreso' => self::ESTADO_EN_PROGRESO,
        'Finalizado' => self::ESTADO_FINALIZADO,
        'Error' => self::ESTADO_ERROR
    );

    private $colorEstado = array(
        '#cccccc' => self::ESTADO_EN_COLA,
        '#99FFCC' => self::ESTADO_FINALIZADO,
        '#FFFF99' => self::ESTADO_GENERANDO,
        '#FDB2B2' => self::ESTADO_ERROR,
    );

    private $strColorEstado = array(
        'default' => self::ESTADO_EN_COLA,
        'success' => self::ESTADO_FINALIZADO,
        'warning' => self::ESTADO_GENERANDO,
        'danger' => self::ESTADO_ERROR,
    );

    public function getColorStatus($status)
    {
        return array_search($status, $this->colorEstado);
    }

    public function getStrColorStatus($status)
    {
        return array_search($status, $this->strColorEstado);
    }

    public function getSchema($tipo)
    {
        return array_search($tipo, $this->schemas);
    }

    public function getStrTipo($tipo)
    {
        return array_search($tipo, $this->tipos);
    }
    public function getStrEstado($status)
    {
        return array_search($status, $this->estados);
    }

    public function getInforme($tipo)
    {
        return array_search($tipo, $this->informes);
    }
    public function getTemplate($tipo)
    {
        return array_search($tipo, $this->templates);
    }
    public function getAllInformes()
    {
        return $this->tipos;
    }


    function __toString(): string
    {
        return (string) $this->uid;
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private $uid;


    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="nota", type="text", nullable=true)
     */
    private $nota;

    /**
     * @ORM\Column(name="asunto", type="string", nullable=true)
     */
    private $asunto;

    /**
     *
     * @ORM\Column(name="fecha_desde", type="string", nullable=true)
     */
    private $fecha_desde;

    /**
     *
     * @ORM\Column(name="fecha_hasta", type="string", nullable=true)
     */
    private $fecha_hasta;


    /**
     * @ORM\Column(name="estado", type="integer", nullable=true)
     */
    private $estado;


    /**
     *
     * @ORM\Column(name="error", type="string", nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(name="data_informe",type="json", nullable=true)
     */
    private $dataInforme;


    /** 
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @ORM\Column(name="visto", type="boolean", nullable=true)
     */
    private $visto;

    /**
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="informesInbox")
     */
    private $organizacion;

    /**
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id", onDelete="SET NULL")
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="informesInbox")
     */
    private $autor;

    public function __construct() {}

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Get the value of autor
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set the value of autor
     *
     * @return  self
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get the value of organizacion
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set the value of organizacion
     *
     * @return  self
     */
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }

    /**
     * Get the value of tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @return  self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get the value of fecha_hasta
     */
    public function getFechaHasta()
    {
        return $this->fecha_hasta;
    }

    /**
     * Set the value of fecha_hasta
     *
     * @return  self
     */
    public function setFechaHasta($fecha_hasta)
    {
        $this->fecha_hasta = $fecha_hasta;

        return $this;
    }

    /**
     * Get the value of fecha_desde
     */
    public function getFechaDesde()
    {
        return $this->fecha_desde;
    }

    /**
     * Set the value of fecha_desde
     *
     * @return  self
     */
    public function setFechaDesde($fecha_desde)
    {
        $this->fecha_desde = $fecha_desde;

        return $this;
    }


    /**
     * Get the value of estado
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     *
     * @return  self
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get the value of error
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of error
     *
     * @return  self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get the value of dataInforme
     */
    public function getDataInforme()
    {
        return $this->dataInforme;
    }

    /**
     * Set the value of dataInforme
     *
     * @return  self
     */
    public function setDataInforme($dataInforme)
    {
        $this->dataInforme = $dataInforme;
       
        return $this;
    }

    /**
     * Get the value of asunto
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set the value of asunto
     *
     * @return  self
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    /**
     * Get the value of nota
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Set the value of nota
     *
     * @return  self
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get the value of visto
     */
    public function getVisto()
    {
        return $this->visto;
    }

    /**
     * Set the value of visto
     *
     * @return  self
     */
    public function setVisto($visto)
    {
        $this->visto = $visto;

        return $this;
    }
}
