<?php

namespace App\Entity\informe\exceso_velocidad\v1;

use App\Entity\informe\ExcesoBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\responsabilidad_vial\v1\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\exceso_velocidad\v1\ExcesoRepository")
 * @ORM\Table(name="exceso_velocidad_v1.excesos")
 */
class Exceso extends ExcesoBase
{

    /**
     * @ORM\Column(name="tiempo_exceso_segundos", type="integer")
     */
    private $tiempoExceso;
    
    /**
     * @ORM\Column(name="distancia_recorrida_metros", type="integer")
     */
    private $distanciaRecorrida;

    /**
     * @ORM\Column(name="total_posiciones",type="smallint")
     */
    private $totalPosiciones;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\exceso_velocidad\v1\Servicio::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_servicio", referencedColumnName="id", nullable=false)
     */
    private $servicio;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\exceso_velocidad\v1\Recorrido::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_recorrido", referencedColumnName="id", nullable=false)
     */
    private $recorrido;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\exceso_velocidad\v1\Referencia::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_referencia", referencedColumnName="id", nullable=false)
     */
    private $referencia;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\exceso_velocidad\v1\Posicion::class, inversedBy="excesoInicial")
     * @ORM\JoinColumn(name="id_posicion_inicial",referencedColumnName="id",nullable=false)
     */
    private $posicionInicial;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\exceso_velocidad\v1\Posicion::class, inversedBy="excesoFinal")
     * @ORM\JoinColumn(name="id_posicion_final",referencedColumnName="id",nullable=false)
     */
    private $posicionFinal;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\exceso_velocidad\v1\Posicion::class, mappedBy="exceso")
     */
    private $posiciones;

    public function __construct()
    {
        $this->posiciones = new ArrayCollection();
    }

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }

    /**
     * Get the value of posicionInicial
     */
    public function getPosicionInicial()
    {
        return $this->posicionInicial;
    }

    /**
     * Set the value of posicionInicial
     *
     * @return  self
     */
    public function setPosicionInicial($posicionInicial)
    {
        $this->posicionInicial = $posicionInicial;

        return $this;
    }

    /**
     * Get the value of posicionFinal
     */
    public function getPosicionFinal()
    {
        return $this->posicionFinal;
    }

    /**
     * Set the value of posicionFinal
     *
     * @return  self
     */
    public function setPosicionFinal($posicionFinal)
    {
        $this->posicionFinal = $posicionFinal;

        return $this;
    }


    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Get the value of distanciaRecorrida
     */
    public function getDistanciaRecorrida()
    {
        return $this->distanciaRecorrida;
    }

    /**
     * Set the value of distanciaRecorrida
     *
     * @return  self
     */
    public function setDistanciaRecorrida($distanciaRecorrida)
    {
        $this->distanciaRecorrida = $distanciaRecorrida;

        return $this;
    }

    /**
     * Get the value of totalPosiciones
     */
    public function getTotalPosiciones()
    {
        return $this->totalPosiciones;
    }

    /**
     * Set the value of totalPosiciones
     *
     * @return  self
     */
    public function setTotalPosiciones($totalPosiciones)
    {
        $this->totalPosiciones = $totalPosiciones;

        return $this;
    }



    /**
     * Get the value of recorrido
     */
    public function getRecorrido()
    {
        return $this->recorrido;
    }

    /**
     * Set the value of recorrido
     *
     * @return  self
     */
    public function setRecorrido($recorrido)
    {
        $this->recorrido = $recorrido;

        return $this;
    }

    /**
     * Get the value of referencia
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set the value of referencia
     *
     * @return  self
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get the value of tiempoExceso
     */ 
    public function getTiempoExceso()
    {
        return $this->tiempoExceso;
    }

    /**
     * Set the value of tiempoExceso
     *
     * @return  self
     */ 
    public function setTiempoExceso($tiempoExceso)
    {
        $this->tiempoExceso = $tiempoExceso;

        return $this;
    }
}
