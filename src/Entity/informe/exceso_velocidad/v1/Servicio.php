<?php

namespace App\Entity\informe\exceso_velocidad\v1;

use App\Entity\informe\ServicioBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\exceso_velocidad\v1\ServicioRepository")
 * @ORM\Table(name="exceso_velocidad_v1.servicios")
 */
class Servicio extends ServicioBase
{


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\exceso_velocidad\v1\Recorrido::class, mappedBy="servicio")
     */
    private $recorridos;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\exceso_velocidad\v1\Posicion::class, mappedBy="servicio")
     */
    private $posiciones;

    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
        $this->posiciones = new ArrayCollection();
    }



    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }

    /**
     * Get the value of recorridos
     */ 
    public function getRecorridos()
    {
        return $this->recorridos;
    }

    /**
     * Set the value of recorridos
     *
     * @return  self
     */ 
    public function setRecorridos($recorridos)
    {
        $this->recorridos = $recorridos;

        return $this;
    }
}
