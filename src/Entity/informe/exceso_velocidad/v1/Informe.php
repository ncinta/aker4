<?php

namespace App\Entity\informe\exceso_velocidad\v1;

use App\Entity\informe\InformeBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\exceso_velocidad\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\exceso_velocidad\v1\InformeRepository")
 * @ORM\Table(name="exceso_velocidad_v1.informes")
 */
class Informe extends InformeBase
{

    /**
     *
     * @ORM\Column(name="velocidad_maxima", type="integer", nullable=true)
     */
    private $velocidadMaxima;


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\exceso_velocidad\v1\Recorrido::class, mappedBy="informe")
     */
    private $recorridos;

    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
    }



    /**
     * Get the value of recorridos
     */
    public function getRecorridos()
    {
        return $this->recorridos;
    }

    /**
     * Set the value of recorridos
     *
     * @return  self
     */
    public function setRecorridos($recorridos)
    {
        $this->recorridos = $recorridos;

        return $this;
    }


    /**
     * Get the value of velocidadMaxima
     */ 
    public function getVelocidadMaxima()
    {
        return $this->velocidadMaxima;
    }

    /**
     * Set the value of velocidadMaxima
     *
     * @return  self
     */ 
    public function setVelocidadMaxima($velocidadMaxima)
    {
        $this->velocidadMaxima = $velocidadMaxima;

        return $this;
    }
}
