<?php

namespace App\Entity\informe\responsabilidad_vial\v1;

use App\Entity\informe\RecorridoBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\responsabilidad_vial\v1\ExcesoRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v1\RecorridoRepository")
 * @ORM\Table(name="responsabilidad_vial_v1.recorridos")
 */
class Recorrido extends RecorridoBase
{

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v1\Servicio::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="id_servicio", referencedColumnName="id", nullable=false)
     */
    private $servicio;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v1\Informe::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="uid_informe",referencedColumnName="uid",nullable=false)
     */
    private $informe;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v1\Exceso::class, mappedBy="recorrido")
     */
    private $excesos;


    public function __construct() {
        $this->excesos = new ArrayCollection();
    }



    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set the value of servicio
     *
     * @return  self
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get the value of informe
     */
    public function getInforme()
    {
        return $this->informe;
    }

    /**
     * Set the value of informe
     *
     * @return  self
     */
    public function setInforme($informe)
    {
        $this->informe = $informe;

        return $this;
    }

    /**
     * Get the value of excesos
     */ 
    public function getExcesos()
    {
        return $this->excesos;
    }

    /**
     * Set the value of excesos
     *
     * @return  self
     */ 
    public function setExcesos($excesos)
    {
        $this->excesos = $excesos;

        return $this;
    }
}
