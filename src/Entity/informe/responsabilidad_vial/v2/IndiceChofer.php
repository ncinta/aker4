<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\IndiceChoferRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.indices_choferes")
 */
class IndiceChofer
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="indice_responsabilidad",type="float")
     */
    private $indiceResponsabilidad;

      /**
     * @ORM\Column(name="riesgo",type="string")
     */
    private $riesgo;




    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Chofer::class, inversedBy="indicesChoferes")
     * @ORM\JoinColumn(name="id_chofer", referencedColumnName="id", nullable=true)
     */
    private $chofer;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Informe::class, inversedBy="indicesChoferes")
     * @ORM\JoinColumn(name="uid_informe",referencedColumnName="uid",nullable=false)
     */
    private $informe;



    public function __construct() {}

    /**
     * Get the value of informe
     */
    public function getInforme()
    {
        return $this->informe;
    }



    /**
     * Get the value of chofer
     */
    public function getChofer()
    {
        return $this->chofer;
    }

    /**
     * Get $id
     *
     * @return  integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of indiceResponsabilidad
     */
    public function getIndiceResponsabilidad()
    {
        return $this->indiceResponsabilidad;
    }

    /**
     * Get the value of riesgo
     */ 
    public function getRiesgo()
    {
        return $this->riesgo;
    }
}
