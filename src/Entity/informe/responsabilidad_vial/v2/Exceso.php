<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\ExcesoBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\responsabilidad_vial\v2\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\ExcesoRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.excesos")
 */
class Exceso extends ExcesoBase
{


    /**
     * @ORM\Column(name="tiempo_exceso_segundos", type="integer")
     */
    private $tiempoExceso;

    /**
     * @ORM\Column(name="riesgo",type="string")
     */
    private $riesgo;

    /**
     * @ORM\Column(name="distancia_recorrida_metros", type="integer")
     */
    private $distanciaRecorrida;

    /**
     * @ORM\Column(name="indice_responsabilidad",type="float")
     */
    private $indiceResponsabilidad;


    /**
     * @ORM\Column(name="total_posiciones",type="smallint")
     */
    private $totalPosiciones;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Recorrido::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_recorrido", referencedColumnName="id", nullable=true)
     */
    private $recorrido;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Referencia::class, inversedBy="excesos")
     * @ORM\JoinColumn(name="id_referencia", referencedColumnName="id", nullable=false)
     */
    private $referencia;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Posicion::class, inversedBy="excesoInicial")
     * @ORM\JoinColumn(name="id_posicion_inicial",referencedColumnName="id",nullable=false)
     */
    private $posicionInicial;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Posicion::class, inversedBy="excesoFinal")
     * @ORM\JoinColumn(name="id_posicion_final",referencedColumnName="id",nullable=false)
     */
    private $posicionFinal;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Posicion::class, mappedBy="exceso")
     */
    private $posiciones;

    public function __construct()
    {
        $this->posiciones = new ArrayCollection();
    }

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Get the value of posicionInicial
     */
    public function getPosicionInicial()
    {
        return $this->posicionInicial;
    }

    /**
     * Get the value of posicionFinal
     */
    public function getPosicionFinal()
    {
        return $this->posicionFinal;
    }


    /**
     * Get the value of distanciaRecorrida
     */
    public function getDistanciaRecorrida()
    {
        return $this->distanciaRecorrida;
    }


    /**
     * Get the value of totalPosiciones
     */
    public function getTotalPosiciones()
    {
        return $this->totalPosiciones;
    }




    /**
     * Get the value of recorrido
     */
    public function getRecorrido()
    {
        return $this->recorrido;
    }


    /**
     * Get the value of riesgo
     */
    public function getRiesgo()
    {
        return $this->riesgo;
    }



    /**
     * Get the value of referencia
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Get the value of tiempoExceso
     */
    public function getTiempoExceso()
    {
        return $this->tiempoExceso;
    }

    /**
     * Set the value of tiempoExceso
     *
     * @return  self
     */
    public function setTiempoExceso($tiempoExceso)
    {
        $this->tiempoExceso = $tiempoExceso;

        return $this;
    }

    /**
     * Get the value of indiceResponsabilidad
     */ 
    public function getIndiceResponsabilidad()
    {
        return $this->indiceResponsabilidad;
    }

    /**
     * Set the value of indiceResponsabilidad
     *
     * @return  self
     */ 
    public function setIndiceResponsabilidad($indiceResponsabilidad)
    {
        $this->indiceResponsabilidad = $indiceResponsabilidad;

        return $this;
    }
}
