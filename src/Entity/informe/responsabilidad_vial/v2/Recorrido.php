<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\RecorridoBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\responsabilidad_vial\v2\ExcesoRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\RecorridoRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.recorridos")
 */
class Recorrido extends RecorridoBase
{

    /**
     *
     * @ORM\Column(name="fecha_inicio", type="string", nullable=true)
     */
    private $fecha_inicio;

    /**
     *
     * @ORM\Column(name="fecha_fin", type="string", nullable=true)
     */
    private $fecha_fin;

 


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Chofer::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="id_chofer", referencedColumnName="id", nullable=true)
     */
    private $chofer;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Servicio::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="id_servicio", referencedColumnName="id", nullable=true)
     */
    private $servicio;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\ChoferServicioHistorico::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="id_historico_servicio_chofer", referencedColumnName="id", nullable=true)
     */
    private $choferServicioHistorico;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Informe::class, inversedBy="recorridos")
     * @ORM\JoinColumn(name="uid_informe",referencedColumnName="uid",nullable=false)
     */
    private $informe;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Exceso::class, mappedBy="recorrido")
     */
    private $excesos;


    public function __construct()
    {
        $this->excesos = new ArrayCollection();
    }


    /**
     * Get the value of informe
     */
    public function getInforme()
    {
        return $this->informe;
    }

    /**
     * Get the value of excesos
     */
    public function getExcesos()
    {
        return $this->excesos;
    }

    /**
     * Get the value of chofer
     */
    public function getChofer()
    {
        return $this->chofer;
    }

    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }


    /**
     * Get the value of choferServicioHistorico
     */
    public function getChoferServicioHistorico()
    {
        return $this->choferServicioHistorico;
    }


    /**
     * Get the value of fecha_inicio
     */ 
    public function getFecha_inicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Get the value of fecha_fin
     */ 
    public function getFecha_fin()
    {
        return $this->fecha_fin;
    }
}
