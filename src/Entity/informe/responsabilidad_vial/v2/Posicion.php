<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\PosicionBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\PosicionRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.posiciones")
 */
class Posicion extends PosicionBase
{


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Exceso::class, inversedBy="posiciones")
     * @ORM\JoinColumn(name="id_exceso",referencedColumnName="id",nullable=false)
     */
    private $exceso;


    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Referencia::class)
     * @ORM\JoinColumn(name="id_referencia",referencedColumnName="id",nullable=false)
     */
    private $referencia;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Exceso::class, mappedBy="posicionInicial")
     */
    private $excesoInicial;


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Exceso::class, mappedBy="posicionFinal")
     */
    private $excesoFinal;


    /**
     * Get the value of exceso
     */
    public function getExceso()
    {
        return $this->exceso;
    }

    /**
     * Get the value of referencia
     */
    public function getReferencia()
    {
        return $this->referencia;
    }


    /**
     * Get the value of excesoInicial
     */
    public function getExcesoInicial()
    {
        return $this->excesoInicial;
    }

    /**
     * Set the value of excesoInicial
     *
     * @return  self
     */
    public function setExcesoInicial($excesoInicial)
    {
        $this->excesoInicial = $excesoInicial;

        return $this;
    }

    /**
     * Get the value of excesoFinal
     */
    public function getExcesoFinal()
    {
        return $this->excesoFinal;
    }

    /**
     * Set the value of excesoFinal
     *
     * @return  self
     */
    public function setExcesoFinal($excesoFinal)
    {
        $this->excesoFinal = $excesoFinal;

        return $this;
    }


}
