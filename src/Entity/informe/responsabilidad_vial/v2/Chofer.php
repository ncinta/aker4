<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\ChoferBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\ChoferRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.choferes")
 */
class Chofer extends ChoferBase
{
    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Recorrido::class, mappedBy="chofer")
     */
    private $recorridos;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\IndiceChofer::class, mappedBy="chofer")
     */
    private $indicesChoferes;

    /**  
     * @ORM\OneToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Servicio::class, inversedBy="chofer")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true)
     */
    private $servicio;

    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
        $this->indicesChoferes = new ArrayCollection();
    }

    /**
     * Get the value of recorridos
     */
    public function getRecorridos()
    {
        return $this->recorridos;
    }


    /**
     * Get the value of indicesChoferes
     */
    public function getIndicesChoferes()
    {
        return $this->indicesChoferes;
    }

    /**
     * Get the value of servicio
     */ 
    public function getServicio()
    {
        return $this->servicio;
    }
}
