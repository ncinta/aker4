<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\ReferenciaBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\ReferenciaRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.referencias")
 */
class Referencia extends ReferenciaBase
{

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Posicion::class, mappedBy="referencia")
     */
    private $posiciones;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Exceso::class, mappedBy="referencia")
     */
    private $excesos;

    /**
     * Get the value of posiciones
     */
    public function getPosiciones()
    {
        return $this->posiciones;
    }

    /**
     * Set the value of posiciones
     *
     * @return  self
     */
    public function setPosiciones($posiciones)
    {
        $this->posiciones = $posiciones;

        return $this;
    }

    /**
     * Get the value of excesos
     */
    public function getExcesos()
    {
        return $this->excesos;
    }

    /**
     * Set the value of excesos
     *
     * @return  self
     */
    public function setExcesos($excesos)
    {
        $this->excesos = $excesos;

        return $this;
    }
}
