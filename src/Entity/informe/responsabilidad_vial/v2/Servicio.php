<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\ServicioBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\ServicioRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.servicios")
 */
class Servicio extends ServicioBase
{


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Recorrido::class, mappedBy="servicio")
     */
    private $recorridos;



    /**
     *
     * @ORM\OneToOne(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Chofer::class, mappedBy="servicio")
     */
    private $chofer;


    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
   
    }

    /**
     * Get the value of recorridos
     */
    public function getRecorridos()
    {
        return $this->recorridos;
    }

    /**
     * Set the value of recorridos
     *
     * @return  self
     */
    public function setRecorridos($recorridos)
    {
        $this->recorridos = $recorridos;

        return $this;
    }

}
