<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\ServicioBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\ChoferServicioHistoricoRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.chofer_servicio_historico")
 */
class ChoferServicioHistorico
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

        /**
     *
     * @ORM\Column(name="fecha_inicio", type="string", nullable=true)
     */
    private $fecha_inicio;

    /**
     *
     * @ORM\Column(name="fecha_fin", type="string", nullable=true)
     */
    private $fecha_fin;



    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Recorrido::class, mappedBy="chofer_servicio_historico")
     */
    private $recorridos;



    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
    }

    /**
     * Get $id
     *
     * @return  integer
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of fecha_inicio
     */ 
    public function getFecha_inicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Get the value of fecha_fin
     */ 
    public function getFecha_fin()
    {
        return $this->fecha_fin;
    }

    /**
     * Get the value of recorridos
     */ 
    public function getRecorridos()
    {
        return $this->recorridos;
    }
}
