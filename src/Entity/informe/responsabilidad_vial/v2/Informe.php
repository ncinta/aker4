<?php

namespace App\Entity\informe\responsabilidad_vial\v2;

use App\Entity\informe\InformeBase;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\informe\responsabilidad_vial\ExcesoRepository;

/**
 * @ORM\Entity(repositoryClass="App\Repository\informe\responsabilidad_vial\v2\InformeRepository")
 * @ORM\Table(name="responsabilidad_vial_v2.informes")
 */
class Informe extends InformeBase
{

    /**
     *
     * @ORM\Column(name="vel_max_en_referencia", type="integer", nullable=true)
     */
    private $velMaxReferencia;

    /**
     *
     * @ORM\Column(name="vel_max_fuera_referencia", type="integer", nullable=true)
     */
    private $velMaxFueraReferencia;


    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\Recorrido::class, mappedBy="informe")
     */
    private $recorridos;

    /**
     * @ORM\OneToMany(targetEntity=App\Entity\informe\responsabilidad_vial\v2\IndiceChofer::class, mappedBy="informe")
     */
    private $indicesChoferes;

    public function __construct()
    {
        $this->recorridos = new ArrayCollection();
    }



    /**
     * Get the value of recorridos
     */
    public function getRecorridos()
    {
        return $this->recorridos;
    }

    /**
     * Set the value of recorridos
     *
     * @return  self
     */
    public function setRecorridos($recorridos)
    {
        $this->recorridos = $recorridos;

        return $this;
    }

    /**
     * Get the value of velMaxReferencia
     */
    public function getVelMaxReferencia()
    {
        return $this->velMaxReferencia;
    }

    /**
     * Set the value of velMaxReferencia
     *
     * @return  self
     */
    public function setVelMaxReferencia($velMaxReferencia)
    {
        $this->velMaxReferencia = $velMaxReferencia;

        return $this;
    }

    /**
     * Get the value of velMaxFueraReferencia
     */
    public function getVelMaxFueraReferencia()
    {
        return $this->velMaxFueraReferencia;
    }

    /**
     * Set the value of velMaxFueraReferencia
     *
     * @return  self
     */
    public function setVelMaxFueraReferencia($velMaxFueraReferencia)
    {
        $this->velMaxFueraReferencia = $velMaxFueraReferencia;

        return $this;
    }
}
