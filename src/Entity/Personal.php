<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Personal
 *
 * @ORM\Table(name="personal")
 * @ORM\Entity(repositoryClass="App\Repository\PersonalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Personal
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Persona", inversedBy="personal", cascade={"persist"})
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $persona;

    /**
     * Inverse Side
     * @ORM\ManyToOne(targetEntity="App\Entity\ActividadGpm", inversedBy="personal", cascade={"persist"})
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $actividad;


    function getId()
    {
        return $this->id;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }

    function getPersona()
    {
        return $this->persona;
    }

    function setPersona($persona)
    {
        $this->persona = $persona;
    }


    function getHorasTrabajoTemp()
    {
        return $this->horasTrabajoTemp;
    }

    function setHorasTrabajoTemp($horasTrabajoTemp)
    {
        $this->horasTrabajoTemp = $horasTrabajoTemp;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }

    function getContratista()
    {
        return $this->contratista;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }
}
