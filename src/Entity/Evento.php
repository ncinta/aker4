<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="App\Repository\EventoRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Evento
{

    const EVENTO_NOTIFICACION_NADA = 0;   //no se notifica
    const EVENTO_NOTIFICACION_ICONO = 1;     //solo icono
    const EVENTO_NOTIFICACION_PANTALLA = 2;     //en pantalla e icono

    private $strNotifWeb = array(
        self::EVENTO_NOTIFICACION_NADA => 'Sin notificación',
        self::EVENTO_NOTIFICACION_ICONO => 'Icono',
        self::EVENTO_NOTIFICACION_PANTALLA => 'Pantalla',
    );
    private $strPrioridad = array(
        0 => 'Sin prioridad',
        null => 'Sin prioridad',
        1 => 'Baja',
        5 => 'Media',
        10 => 'Alta',
    );
    private $colorPrioridad = array(
        null => '#FFFFFF',
        0 => '#FFFFFF',
        1 => '#F2F5A9',
        5 => '#FE9A2E',
        10 => '#FE2E2E',
    );

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="evento_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @ORM\Column(name="lunes", type="boolean", nullable=true)
     */
    private $lunes;

    /**
     * @ORM\Column(name="martes", type="boolean", nullable=true)
     */
    private $martes;

    /**
     * @ORM\Column(name="miercoles", type="boolean", nullable=true)
     */
    private $miercoles;

    /**
     * @ORM\Column(name="jueves", type="boolean", nullable=true)
     */
    private $jueves;

    /**
     * @ORM\Column(name="viernes", type="boolean", nullable=true)
     */
    private $viernes;

    /**
     * @ORM\Column(name="sabado", type="boolean", nullable=true)
     */
    private $sabado;

    /**
     * @ORM\Column(name="domingo", type="boolean", nullable=true)
     */
    private $domingo;

    /**
     * @ORM\Column(name="hora_desde", type="time", nullable=true)
     */
    private $horaDesde;

    /**
     * @ORM\Column(name="hora_hasta", type="time", nullable=true)
     */
    private $horaHasta;

    /**
     * @ORM\Column(name="notificacion_web", type="integer", nullable=true)
     */
    private $notificacionWeb;

    /**
     * @ORM\Column(name="sonido", type="boolean", nullable=true)
     */
    private $sonido;

    /**
     * @ORM\Column(name="registrar", type="boolean", nullable=true)
     */
    private $registrar;


    /**
     * @ORM\Column(name="respuesta", type="text", nullable=true)
     */
    private $respuesta;

    /**
     * @ORM\Column(name="protocolo", type="text", nullable=true)
     */
    private $protocolo;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="prioridad", type="integer", nullable=true)
     */
    private $prioridad;        // 0= sin monitoreo,1 = baja, 5=media, 10 = alta

    /**
     * @ORM\Column(name="clase_evento", type="integer", nullable=true)
     */
    private $clase; // 1= evento clasico, 2=reply, 3=itinerario,

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="eventos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventoParametro", mappedBy="evento", cascade={"persist"})
     */
    protected $parametros;

    /** Notificaciones
     * @ORM\OneToMany(targetEntity="App\Entity\EventoNotificacion", mappedBy="evento", cascade={"persist"})
     */
    protected $notificaciones;

    /** Servicios
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Servicio", inversedBy="eventos", cascade={"persist"})
     * @ORM\JoinTable(name="eventoServicio",
     *      joinColumns={@ORM\JoinColumn(name="evento_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $servicios;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoEvento", inversedBy="eventos")
     * @ORM\JoinColumn(name="tipoevento_id", referencedColumnName="id")
     */
    protected $tipoEvento;

    /** Notificaciones
     * @ORM\OneToMany(targetEntity="App\Entity\EventoHistorial", mappedBy="evento", cascade={"all"})
     */
    protected $historial;
    
    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\HistoricoItinerario", mappedBy="evento", cascade={"all"})
     */
    protected $historicoItinerario;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="evento", cascade={"all"})
     */
    protected $eventoTemporal;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\EventoItinerario", mappedBy="evento", cascade={"all"})
     */
    protected $eventoItinerario;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Bitacora", mappedBy="eventos")
     */
    private $bitacora;


    public function __construct()
    {
        $this->servicios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getActivo()
    {
        return $this->activo;
    }

    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    public function getLunes()
    {
        return $this->lunes;
    }

    public function setLunes($lunes)
    {
        $this->lunes = $lunes;
    }

    public function getMartes()
    {
        return $this->martes;
    }

    public function setMartes($martes)
    {
        $this->martes = $martes;
    }

    public function getMiercoles()
    {
        return $this->miercoles;
    }

    public function setMiercoles($miercoles)
    {
        $this->miercoles = $miercoles;
    }

    public function getJueves()
    {
        return $this->jueves;
    }

    public function setJueves($jueves)
    {
        $this->jueves = $jueves;
    }

    public function getViernes()
    {
        return $this->viernes;
    }

    public function setViernes($viernes)
    {
        $this->viernes = $viernes;
    }

    public function getSabado()
    {
        return $this->sabado;
    }

    public function setSabado($sabado)
    {
        $this->sabado = $sabado;
    }

    public function getDomingo()
    {
        return $this->domingo;
    }

    public function setDomingo($domingo)
    {
        $this->domingo = $domingo;
    }

    public function getHoraDesde()
    {
        return $this->horaDesde;
    }

    public function setHoraDesde($horaDesde)
    {
        $this->horaDesde = $horaDesde;
    }

    public function getHoraHasta()
    {
        return $this->horaHasta;
    }

    public function setHoraHasta($horaHasta)
    {
        $this->horaHasta = $horaHasta;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function getParametros()
    {
        return $this->parametros;
    }

    public function setParametros($parametros)
    {
        $this->parametros = $parametros;
    }

    public function addParametro($parametro)
    {
        $this->parametros[] = $parametro;
    }

    public function getServicios()
    {
        return $this->servicios;
    }

    public function setServicios($servicios)
    {
        $this->servicios = $servicios;
    }

    public function addServicio($servicio)
    {
        if (!$this->servicios->contains($servicio)) {
            $this->servicios[] = $servicio;
        }
    }

    public function removeServicio($servicio)
    {
        $this->servicios->removeElement($servicio);
    }

    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    public function setTipoEvento($tipoEvento)
    {
        $this->tipoEvento = $tipoEvento;
    }

    public function getHoraInicio()
    {
        return date_format($this->horaDesde, 'H:i');
    }

    public function getHoraFin()
    {
        return date_format($this->horaHasta, 'H:i');
    }

    public function setHoraInicio($inicio)
    {
        $this->horaDesde = new \DateTime($inicio);
    }

    public function setHoraFin($fin)
    {
        $this->horaHasta = new \DateTime($fin);
    }

    public function getNotificaciones()
    {
        return $this->notificaciones;
    }

    public function setNotificaciones($notificaciones)
    {
        $this->notificaciones = $notificaciones;
    }

    public function addNotificacion($notificacion)
    {
        $this->notificaciones[] = $notificacion;
    }

    public function removeAllNotificaciones()
    {
        $this->notificaciones = null;
    }

    public function removeNotificacion($notificacion)
    {
        $this->notificaciones->removeElement($notificacion);
    }

    public function getHistorial()
    {
        return $this->historial;
    }

    public function setHistorial($historial)
    {
        $this->historial = $historial;
    }

    public function getNotificacionWeb()
    {
        return $this->notificacionWeb;
    }

    public function setNotificacionWeb($notificacionWeb)
    {
        $this->notificacionWeb = $notificacionWeb;
        return $this;
    }

    public function getArrayStrNotificacionWeb()
    {
        return $this->strNotifWeb;
    }

    public function getStrNotificacionWeb()
    {
        if ($this->notificacionWeb != null && $this->notificacionWeb <= count($this->strNotifWeb) - 1) {
            return $this->strNotifWeb[$this->notificacionWeb];
        } else {
            return $this->strNotifWeb[0];
        }
    }

    function getColorPrioridad()
    {
        if ($this->notificacionWeb != null && $this->notificacionWeb <= count($this->strNotifWeb) - 1) {
            return $this->colorPrioridad[$this->notificacionWeb];
        } else {
            return $this->colorPrioridad[0];
        }
    }

    public function getSonido()
    {
        return $this->sonido;
    }

    public function setSonido($sonido)
    {
        $this->sonido = $sonido;
        return $this;
    }

    public function getRegistrar()
    {
        return $this->registrar;
    }

    public function setRegistrar($registrar)
    {
        $this->registrar = $registrar;
        return $this;
    }

    public function getRespuesta()
    {
        return $this->respuesta;
    }

    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
        return $this;
    }

    function getPrioridad()
    {
        return $this->strPrioridad[$this->prioridad];
    }

    function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;
    }

    function getColor()
    {
        return $this->colorPrioridad[$this->prioridad];
    }

    /**
     * Get the value of eventoTemporal
     */
    public function getEventoTemporal()
    {
        return $this->eventoTemporal;
    }

    /**
     * Set the value of eventoTemporal
     *
     * @return  self
     */
    public function setEventoTemporal($eventoTemporal)
    {
        $this->eventoTemporal = $eventoTemporal;

        return $this;
    }

    /**
     * Get the value of protocolo
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set the value of protocolo
     *
     * @return  self
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get inverse Side
     */
    public function getBitacora()
    {
        return $this->bitacora;
    }

    /**
     * Set inverse Side
     *
     * @return  self
     */
    public function setBitacora($bitacora)
    {
        $this->bitacora = $bitacora;

        return $this;
    }

    /**
     * Get the value of clase
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Set the value of clase
     *
     * @return  self
     */
    public function setClase($clase)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get the value of eventoItinerario
     */
    public function getEventoItinerario()
    {
        return $this->eventoItinerario;
    }

    /**
     * Set the value of eventoItinerario
     *
     * @return  self
     */
    public function setEventoItinerario($eventoItinerario)
    {
        $this->eventoItinerario = $eventoItinerario;

        return $this;
    }

   

    /**
     * Get the value of historicoItinerario
     */ 
    public function getHistoricoItinerario()
    {
        return $this->historicoItinerario;
    }

    /**
     * Set the value of historicoItinerario
     *
     * @return  self
     */ 
    public function setHistoricoItinerario($historicoItinerario)
    {
        $this->historicoItinerario = $historicoItinerario;

        return $this;
    }
}
