<?php

namespace App\Entity;

use BeSimple\SoapBundle\ServiceDefinition\Annotation as Soap;

class Response
{

    /**
     * @Soap\ComplexType("int")
     */
    public $code;

    /**
     * @Soap\ComplexType("string")
     */
    public $message;
}
