<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Description of Deposito
 *
 * @author nicolas
 */

/**
 * App\Entity\Deposito
 *
 * @ORM\Table(name="deposito")
 * @ORM\Entity(repositoryClass="App\Repository\DepositoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Deposito
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="direccion", type="string", length=255,nullable=true)
     */
    private $direccion;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="descripcion", type="string", length=255,nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="depositos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimientoHistorico", mappedBy="deposito", cascade={"persist","remove"})     
     */
    protected $servicioMantenimientos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="deposito", cascade={"persist"})     
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoProducto", mappedBy="deposito", cascade={"persist"})     
     */
    protected $productosUsados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOtProducto", mappedBy="deposito", cascade={"persist"})     
     */
    protected $productosTareaUsados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="deposito", cascade={"persist","remove"})     
     */
    protected $stocks;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getStocks()
    {
        return $this->stocks;
    }

    function setStocks($stocks)
    {
        $this->stocks = $stocks;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getDireccion()
    {
        return $this->direccion;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function getHistoricos()
    {
        return $this->historicos;
    }

    function setHistoricos($historicos)
    {
        $this->historicos = $historicos;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function getProductosUsados()
    {
        return $this->productosUsados;
    }

    function setProductosUsados($productosUsados)
    {
        $this->productosUsados = $productosUsados;
    }

    function getProductosTareaUsados()
    {
        return $this->productosTareaUsados;
    }

    function setProductosTareaUsados($productosTareaUsados)
    {
        $this->productosTareaUsados = $productosTareaUsados;
    }
}
