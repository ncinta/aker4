<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ActividadGpm
 *
 * @author nicolas
 */

/**
 *
 * @ORM\Table(name="actividad_manual")
 * @ORM\Entity(repositoryClass="App\Repository\ActividadManualRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActividadManual
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="fecha", type="datetime", nullable=true) 
     */
    private $fecha;

    /**
     * @ORM\Column(name="tiempo", type="integer", nullable=true)  (tiempo en minutos)
     */
    private $tiempo;

    /**
     * @ORM\Column(name="hectareas", type="float", nullable=true) 
     */
    private $hectareas;

    /**
     * @ORM\Column(name="distancia", type="bigint", nullable=true) (distancia en metros)
     */
    private $distancia;

    /**
     * @ORM\Column(name="chofer", type="string", length=255, nullable=true)
     */
    private $chofer;

    /**
     * @ORM\Column(name="operario", type="string", length=255, nullable=true)
     */
    private $operario;

    /**
     * @ORM\ManyToOne(targetEntity="ActividadGpm", inversedBy="ingresos")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $actividad;

    function getId()
    {
        return $this->id;
    }

    function getHectareas()
    {
        return $this->hectareas;
    }



    function getChofer()
    {
        return $this->chofer;
    }

    function getOperario()
    {
        return $this->operario;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }


    function setHectareas($hectareas)
    {
        $this->hectareas = $hectareas;
    }


    function setChofer($chofer)
    {
        $this->chofer = $chofer;
    }

    function setOperario($operario)
    {
        $this->operario = $operario;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }
    function getTiempo()
    {
        return $this->tiempo;
    }

    function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;
    }

    function getDistancia()
    {
        return $this->distancia;
    }

    function setDistancia($distancia)
    {
        $this->distancia = $distancia;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }
}
