<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Mantenimiento
 *
 * @ORM\Table(name="ordentrabajo_mantenimiento")
 * @ORM\Entity(repositoryClass="App\Repository\OrdenTrabajoMantenimientoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrdenTrabajoMantenimiento
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="mantenimientos", cascade={"persist"})
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServicioMantenimiento", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="serviciomantenimiento_id", referencedColumnName="id",nullable=true)
     */
    protected $servicioMantenimiento;

    function getId()
    {
        return $this->id;
    }

    function getOrdenTrabajo()
    {
        return $this->ordenTrabajo;
    }

    function getServicioMantenimiento()
    {
        return $this->servicioMantenimiento;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setOrdenTrabajo($ordenTrabajo)
    {
        $this->ordenTrabajo = $ordenTrabajo;
    }

    function setServicioMantenimiento($servicioMantenimiento)
    {
        $this->servicioMantenimiento = $servicioMantenimiento;
    }
}
