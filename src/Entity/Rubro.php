<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Mantiene ordenado por rubro los productos de la organizacin
 *
 * @author claudio
 */

/**
 * App\Entity\Rubro
 *
 * @ORM\Table(name="rubro")
 * @ORM\Entity(repositoryClass="App\Repository\RubroRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Rubro
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="codigo", type="string")
     */
    private $codigo;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="rubros")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="rubro")
     */
    protected $productos;

    public function __toString()
    {
        return $this->nombre;
    }

    function getId()
    {
        return $this->id;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getProductos()
    {
        return $this->productos;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setProductos($productos)
    {
        $this->productos = $productos;
    }
}
