<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\ActividadgpmServicio
 *
 * @ORM\Table(name="actgpm_servicio")
 * @ORM\Entity(repositoryClass="App\Repository\ActgpmServicioRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class ActgpmServicio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Servicio", inversedBy="actgpmServicios")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $servicio;

    /**
     * @ORM\ManyToOne(targetEntity="ActividadGpm", inversedBy="actgpmServicios")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $actividad;

    function getId()
    {
        return $this->id;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }
}
