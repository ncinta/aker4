<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="logistica")
 * @ORM\Entity(repositoryClass="App\Repository\LogisticaRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Logistica
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Itinerario", mappedBy="logistica", cascade={"remove"})
     */
    private $itinerarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacto", mappedBy="logistica", cascade={"remove"})
     */
    private $contactos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Usuario", mappedBy="logistica", cascade={"remove"})
     */
    private $usuarios;

     /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Servicio", mappedBy="logisticas")
     */
    private $servicios;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="logisticas")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;




    /**
     * Get the value of itinerarios
     */
    public function getItinerarios()
    {
        return $this->itinerarios;
    }

    /**
     * Set the value of itinerarios
     *
     * @return  self
     */
    public function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;

        return $this;
    }

    /**
     * Get the value of updated_at
     */
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Get the value of contactos
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Set the value of contactos
     *
     * @return  self
     */
    public function setContactos($contactos)
    {
        $this->contactos = $contactos;

        return $this;
    }

    /**
     * Get the value of usuarios
     */ 
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Set the value of usuarios
     *
     * @return  self
     */ 
    public function setUsuarios($usuarios)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get the value of servicios
     */ 
    public function getServicios()
    {
        return $this->servicios;
    }

    /**
     * Set the value of servicios
     *
     * @return  self
     */ 
    public function setServicios($servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of organizacion
     */ 
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set the value of organizacion
     *
     * @return  self
     */ 
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }

    /**
     * Get $id
     *
     * @return  integer
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set $id
     *
     * @param  integer  $id  $id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    function __toString()
    {
        return $this->nombre;
    }

}
