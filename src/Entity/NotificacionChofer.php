<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of NotificacionChofer
 *
 * @author nicolas
 */


/**
 * App\Entity\NotificacionChofer
 *
 * @ORM\Table(name="notificacion_chofer")
 * @ORM\Entity(repositoryClass="App\Repository\NotificacionChoferRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class NotificacionChofer
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string mensaje
     * 
     * @ORM\Column(name="nombre", type="text", nullable=false)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotifChof", mappedBy="notificacion")
     */
    protected $notifChof;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotifContact", mappedBy="notificacion")
     */
    protected $notifContact;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="notifChofer")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getNotifChof()
    {
        return $this->notifChof;
    }

    function getNotifContact()
    {
        return $this->notifContact;
    }

    function setNotifChof($notifChof)
    {
        $this->notifChof = $notifChof;
    }

    function setNotifContact($notifContact)
    {
        $this->notifContact = $notifContact;
    }
}
