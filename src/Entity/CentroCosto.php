<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\CentroCosto
 *
 * @ORM\Table(name="centrocosto")
 * @ORM\Entity(repositoryClass="App\Repository\CentroCostoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CentroCosto
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="centrosCosto")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestacion", mappedBy="centroCosto", cascade={"persist"})
     */
    protected $prestaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="centroCosto", cascade={"persist"})
     */
    protected $ordenesTrabajo;

    /**
     * Inverse Side
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Servicio", mappedBy="centrocosto")
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    private $servicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraCentroCosto", mappedBy="centrocosto")
     */
    protected $bitacoraCentroCosto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IndiceCc", mappedBy="centroCosto",cascade={"persist","remove"})
     */
    protected $indiceCc;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getPrestaciones()
    {
        return $this->prestaciones;
    }

    function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getServicios()
    {
        return $this->servicios;
    }

    function setServicios($servicios)
    {
        $this->servicios = $servicios;
    }

    public function addServicio(Servicio $servicio)
    {
        $this->servicios[] = $servicio;
    }

    public function getIndiceCc()
    {
        return $this->indiceCc;
    }

    public function setIndiceCc($indiceCc): void
    {
        $this->indiceCc = $indiceCc;
    }
}
