<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="serviciomantenimiento")
 * @ORM\Entity(repositoryClass="App\Repository\ServicioMantenimientoRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class ServicioMantenimiento {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $intervalo_dias
     *
     * @ORM\Column(name="intervalo_dias", type="integer", nullable=true)
     */
    private $intervaloDias;

    /**
     * @var integer $intervalo_kilometros
     *
     * @ORM\Column(name="intervalo_kilometros", type="integer", nullable=true)
     */
    private $intervaloKilometros;

    /**
     * @var integer $intervalo_horas
     *
     * @ORM\Column(name="intervalo_horas", type="integer", nullable=true)
     */
    private $intervaloHoras;

    /**
     * @var integer $aviso_dias
     *
     * @ORM\Column(name="aviso_dias", type="integer", nullable=true)
     */
    private $avisoDias;

    /**
     * @var integer $aviso_kilometros
     *
     * @ORM\Column(name="aviso_kilometros", type="integer", nullable=true)
     */
    private $avisoKilometros;

    /**
     * @var integer $aviso_horas
     *
     * @ORM\Column(name="aviso_horas", type="integer", nullable=true)
     */
    private $avisoHoras;

    /**
     * @var integer $proximo_dias
     *
     * @ORM\Column(name="proximo_dias", type="datetime", nullable=true)
     */
    private $proximoDias;

    /**
     * @var integer $proximo_kilometros
     *
     * @ORM\Column(name="proximo_kilometros", type="integer", nullable=true)
     */
    private $proximoKilometros;

    /**
     * @var integer $proximo_horas
     *
     * @ORM\Column(name="proximo_horas", type="integer", nullable=true)
     */
    private $proximoHoras;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="mantenimientos")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $servicio;
       
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mantenimiento", inversedBy="servicioMantenimientos")
     * @ORM\JoinColumn(name="mantenimiento_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $mantenimiento;
   
    /**
     * @todo Esto deberia cambiarse por la relacion de orden de trabajo.
     * Historicos
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimientoHistorico", mappedBy="servicioMantenimiento", cascade={"persist"})
     */
    protected $historicos;
    
    /**     
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoMantenimiento", mappedBy="servicioMantenimiento", cascade={"persist","remove"})
     */
    protected $ordenesTrabajo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt() {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt() {
        $this->updated_at = new \DateTime();
    }

    public function getIntervaloDias() {
        return $this->intervaloDias;
    }

    public function setIntervaloDias($intervaloDias) {
        $this->intervaloDias = $intervaloDias;
    }

    public function getIntervaloKilometros() {
        return $this->intervaloKilometros;
    }

    public function setIntervaloKilometros($intervaloKilometros) {
        $this->intervaloKilometros = $intervaloKilometros;
    }

    public function getIntervaloHoras() {
        return $this->intervaloHoras;
    }

    public function setIntervaloHoras($intervaloHoras) {
        $this->intervaloHoras = $intervaloHoras;
    }

    public function getAvisoDias() {
        return $this->avisoDias;
    }

    public function setAvisoDias($avisoDias) {
        $this->avisoDias = $avisoDias;
    }

    public function getAvisoKilometros() {
        return $this->avisoKilometros;
    }

    public function setAvisoKilometros($avisoKilometros) {
        $this->avisoKilometros = $avisoKilometros;
    }

    public function getAvisoHoras() {
        return $this->avisoHoras;
    }

    public function setAvisoHoras($avisoHoras) {
        $this->avisoHoras = $avisoHoras;
    }

    public function getProximoDias() {
       // return date_format($this->proximoDias, 'd/m/Y');
        return $this->proximoDias;
    }

    public function setProximoDias($proximoDias) {
        $this->proximoDias = $proximoDias;
    }

    public function getProximoKilometros() {
        return $this->proximoKilometros;
    }

    public function setProximoKilometros($proximoKilometros) {
        $this->proximoKilometros = $proximoKilometros;
    }

    public function getProximoHoras() {
        return $this->proximoHoras;
    }

    public function setProximoHoras($proximoHoras) {
        $this->proximoHoras = $proximoHoras;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
    }

    public function getServicio() {
        return $this->servicio;
    }

    public function setServicio($servicio) {
        $this->servicio = $servicio;
    }

    public function getMantenimiento() {
        return $this->mantenimiento;
    }

    public function setMantenimiento($mantenimiento) {
        $this->mantenimiento = $mantenimiento;
    }

    public function getHistoricos() {
        return $this->historicos;
    }

    public function setHistoricos($historicos) {
        $this->historicos = $historicos;
    }

    function getVars() {
        return get_object_vars($this);
    }

    function getOrdenesTrabajo() {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo) {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    public function addOrdenTrabajo($mant) {
        $this->ordenesTrabajo[] = $mant;
    }

}