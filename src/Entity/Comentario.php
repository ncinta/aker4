<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="comentario")
 * @ORM\Entity(repositoryClass="App\Repository\ComentarioRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Comentario
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="texto", type="text")
     */
    private $texto;

    /**
     * @ORM\Column(name="cambio", type="text", nullable=true)
     */
    private $cambio;

    /**
     * @ORM\Column(name="resultado", type="integer", nullable=true) 
     */
    private $resultado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="comentarios")
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Peticion", inversedBy="comentarios")
     * @ORM\JoinColumn(name="peticion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $peticion;

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    function getTexto()
    {
        return $this->texto;
    }

    function getUsuario()
    {
        return $this->usuario;
    }

    function getPeticion()
    {
        return $this->peticion;
    }

    function setTexto($texto)
    {
        $this->texto = $texto;
    }

    function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    function setPeticion($peticion)
    {
        $this->peticion = $peticion;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function getCambio()
    {
        return $this->cambio;
    }

    function setCambio($cambio)
    {
        $this->cambio = $cambio;
    }

    function getResultado()
    {
        return $this->resultado;
    }

    function setResultado($resultado)
    {
        $this->resultado = $resultado;
    }
}
