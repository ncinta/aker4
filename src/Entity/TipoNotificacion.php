<?php
//Clase que especifica si un contacto es telefonico o mail
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="tipo_notificacion")
 * @ORM\Entity
 */
class TipoNotificacion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;    
    
    
    /**
     * @var string $codename
     *
     * @ORM\Column(name="codename", type="string", length=255, nullable = true)
     */
    private $codename;  //NOTIF_APP para aplcicacion , NOTIF_EMAIL para email

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contacto", mappedBy="tipoNotificaciones")
     */
    protected $contactos;

    public function __construct()
    {
        $this->contactos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }


  
    public function addContactos(\App\Entity\Contacto $contacto)
    {
        $this->contactos[] = $contacto;
    }

    /**
     * Get contactos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Get $codename
     *
     * @return  string
     */ 
    public function getCodename()
    {
        return $this->codename;
    }

    /**
     * Set $codename
     *
     * @param  string  $codename  $codename
     *
     * @return  self
     */ 
    public function setCodename(string $codename)
    {
        $this->codename = $codename;

        return $this;
    }
}
