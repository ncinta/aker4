<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\ChoferServicioHistorico
 *
 * @ORM\Table(name="chofer_servicio_historico")
 * @ORM\Entity(repositoryClass="App\Repository\ChoferServicioHistoricoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ChoferServicioHistorico
{
    const MOTIVO_RECEPCION = 0;
    const MOTIVO_ENTREGA = 1;

    const ESTADO_OK = 1;
    const ESTADO_FALLA_LEVE = 2;
    const ESTADO_FALLA_MEDIA = 3;
    const ESTADO_FALLA_GRAVE = 4;

    private $strMotivo = array(
        self::MOTIVO_RECEPCION => 'Recepción',
        self::MOTIVO_ENTREGA   => 'Entrega'
    );

    // para el formulario que usa la clave el revés
    private $strEstado = array(
        'Ok' => self::ESTADO_OK,
        'Falla Leve' => self::ESTADO_FALLA_LEVE,
        'Falla Media' => self::ESTADO_FALLA_MEDIA,
        'Falla Grave' => self::ESTADO_FALLA_GRAVE
    );
    
    //para los templates
    private $strEstadoTemplate = array(
         self::ESTADO_OK => 'Ok',
         self::ESTADO_FALLA_LEVE =>'Falla Leve',
         self::ESTADO_FALLA_MEDIA => 'Falla Media' ,
         self::ESTADO_FALLA_GRAVE => 'Falla Grave'
    );

    private $badge = array(
        self::ESTADO_OK => 'success',
        self::ESTADO_FALLA_LEVE => 'warning',
        self::ESTADO_FALLA_MEDIA => 'warning',
        self::ESTADO_FALLA_GRAVE => 'alert',
    );
    

    public function getArrayStrMotivo()
    {
        return $this->strMotivo;
    }

    public function getStrMotivo()
    {
        if ($this->motivo != null) {
            return $this->strMotivo[$this->motivo];
        } else {
            return $this->strMotivo[0];
        }
    }

    public function getStrBadge()
    {
        if (isset($this->badge[$this->estado])) {
            return $this->badge[$this->estado];
        } else {
            return 'default';
        }
        
    }

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        if ($this->estado != null && isset($this->strEstado[$this->estado])) {
            return $this->strEstado[$this->estado];
        } else {
            return 'indefinido';
        }
    }
   public function getStrEstadoTemplate()
    {
        if ($this->estado != null && isset($this->strEstadoTemplate[$this->estado])) {
            return $this->strEstadoTemplate[$this->estado];
        } else {
            return 'indefinido';
        }
    }



    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;


    /** 0 = recepcion , 1 = entrega
     * @ORM\Column(name="motivo", type="integer", nullable=true)
     */
    private $motivo;


    /** 0 = oka , 1 = falla (estado que se encuentra el vehículo)
     * @ORM\Column(name="estado", type="integer", nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(name="id_formulario", type="string", nullable=true)
     */
    private $idFormulario;

    /**
     * @ORM\Column(name="nombre", type="string", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=true) 
     */
    private $fechaInicio;

    /**
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true) 
     */
    private $fechaFin;

    /**
     * @var float $latitud
     *
     * @ORM\Column(name="latitud", type="float", nullable=true)
     */
    private $latitud;

    /**
     * @var float $longitud
     *
     * @ORM\Column(name="longitud", type="float", nullable=true)
     */
    private $longitud;

    /*
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="choferServicioHistorico")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="choferServicioHistorico")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id")
     */
    private $servicio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="choferServicioHistorico")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id")
     */
    private $chofer;


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function getFecha()
    {
        /**if (is_null($this->fecha)) {
            return $this->fecha;
        } elseif ($this->fecha instanceof \DateTime) {
            //die('////<pre>' . nl2br(var_export(get_class($this->inicio_prestacion), true)) . '</pre>////');
            return date_format($this->fecha, 'd/m/Y H:i');
        }*/
        return $this->fecha;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Set $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }


    /**
     * Get the value of motivo
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set the value of motivo
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    /**
     * Get the value of idFormulario
     */
    public function getIdFormulario()
    {
        return $this->idFormulario;
    }

    /**
     * Set the value of idFormulario
     */
    public function setIdFormulario($idFormulario)
    {
        $this->idFormulario = $idFormulario;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get the value of fechaInicio
     */
    public function getFechaInicio()
    {
        if (is_null($this->fechaInicio)) {
            return $this->fechaInicio;
        } elseif ($this->fechaInicio instanceof \DateTime) {
            return date_format($this->fechaInicio, 'd/m/Y H:i');
        }
        return $this->fechaInicio;
    }

    /**
     * Set the value of fechaInicio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    }

    /**
     * Get the value of fechaFin
     */
    public function getFechaFin()
    {
        if (is_null($this->fechaFin)) {
            return $this->fechaFin;
        } elseif ($this->fechaFin instanceof \DateTime) {
            return date_format($this->fechaFin, 'd/m/Y H:i');
        }
        return $this->fechaFin;
    }

    /**
     * Set the value of fechaFin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    /**
     * Get $latitud
     *
     * @return  float
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set $latitud
     *
     * @param  float  $latitud 
     */
    public function setLatitud(float $latitud)
    {
        $this->latitud = $latitud;
    }

    /**
     * Get $longitud
     *
     * @return  float
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set $longitud
     *
     * @param  float  $longitud
     */
    public function setLongitud(float $longitud)
    {
        $this->longitud = $longitud;
    }

    /**
     * Get /*
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }


    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get the value of servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }


    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * Get the value of chofer
     */
    public function getChofer()
    {
        return $this->chofer;
    }


    public function setChofer($chofer)
    {
        $this->chofer = $chofer;
    }

    /**
     * Get the value of estado
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}
