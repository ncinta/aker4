<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Servicio
 *
 * @ORM\Table(name="servicio")
 * @ORM\Entity(repositoryClass="App\Repository\ServicioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Servicio
{

    const TIPOOBJETO_PERSONA = 0;
    const TIPOOBJETO_VEHICULO = 1;
    const TIPOOBJETO_HERRAMIENTA = 2;
    const TIPOOBJETO_MAQUINARIA = 3;

    const TIPOOBJETOS = array(
        self::TIPOOBJETO_PERSONA => 'Persona',
        self::TIPOOBJETO_VEHICULO => 'Vehiculo',
        self::TIPOOBJETO_HERRAMIENTA => 'Herramienta',
        self::TIPOOBJETO_MAQUINARIA => 'Maquinaria'
    );
    protected $strTipoObjetos = self::TIPOOBJETOS;

    const ESTADO_DESHABILITADO = 0;
    const ESTADO_HABILITADO = 1;
    const ESTADO_BLOQUEADO = 2;
    const ESTADO_SUSPENDIDO = 3;
    const ESTADO_BAJA = 4;
    const ESTADO_ELIMINADO = 9;
    const ESTADO_SERVICIO_OK = 10;
    const ESTADO_SERVICIO_RETRASADO = 11;
    const ESTADO_SERVICIO_ERROR = 12;
    const ESTADO_SERVICIO_INDEFINIDO = 13;
    const TIPO_MEDIDOR_COMBUSTIBLE_ND = 0;
    const TIPO_MEDIDOR_COMBUSTIBLE_PORCENTAJE = 1;
    const TIPO_MEDIDOR_COMBUSTIBLE_LITROS = 2;
    const TIPO_MEDIDOR_COMBUSTIBLE_AMBOS = 3;

    protected $strEstado = array(
        self::ESTADO_DESHABILITADO => 'deshab.', //no recibe tramas ni nada
        self::ESTADO_HABILITADO => 'hab.', //es el estado normal.
        self::ESTADO_BLOQUEADO => 'bloq.', //puede ser por reparacion o algo. sigue recibiendo tramas
        self::ESTADO_SUSPENDIDO => 'susp.', //suspendido.... establecer.
        self::ESTADO_BAJA => 'baja', //cuando el equipo esta dado de baja.
        self::ESTADO_ELIMINADO => 'elim.', //cuando el equipo esta dado de baja.
    );
    private $strStatus = array(
        'Ok' => self::ESTADO_SERVICIO_OK,
        'Retrasado' => self::ESTADO_SERVICIO_RETRASADO,
        'Error' => self::ESTADO_SERVICIO_ERROR,
        'Indefinido' => self::ESTADO_SERVICIO_INDEFINIDO,
    );
    
    const ESTADO = array(
        'Deshabilitado' => self::ESTADO_DESHABILITADO,
        'Habilitado' => self::ESTADO_HABILITADO,
        'Baja' => self::ESTADO_BAJA,

    );
    private $colorStatus = array(
        '#99FFCC' => self::ESTADO_SERVICIO_OK,
        '#FFFF99' => self::ESTADO_SERVICIO_RETRASADO,
        '#FDB2B2' => self::ESTADO_SERVICIO_ERROR,
        '#cccccc' => self::ESTADO_SERVICIO_INDEFINIDO,
    );
    private $strTipoMedidorCombustible = array(
        'No disponible' => self::TIPO_MEDIDOR_COMBUSTIBLE_ND,
        'Porcentaje' => self::TIPO_MEDIDOR_COMBUSTIBLE_PORCENTAJE,
        'Litros' => self::TIPO_MEDIDOR_COMBUSTIBLE_LITROS,
        //    'Litros y Porcentaje' => self::TIPO_MEDIDOR_COMBUSTIBLE_AMBOS,
    );
    private $strColorStatus = array(
        'success' => self::ESTADO_SERVICIO_OK,
        'warning' => self::ESTADO_SERVICIO_RETRASADO,
        'danger' => self::ESTADO_SERVICIO_ERROR,
        'default' => self::ESTADO_SERVICIO_INDEFINIDO,
    );

    public function getColorStatus($status)
    {
        return array_search($status, $this->colorStatus);
    }

    public function getStrColorStatus($status)
    {
        return array_search($status, $this->strColorStatus);
    }

    public function getArrayStrOrder()
    {
        return $this->strStatus;
    }
    
    public function getArrayStrMedidorCombustible()
    {
        return $this->strTipoMedidorCombustible;
    }

    public function getStrStatus($status)
    {
        return array_search($status, $this->strStatus);
    }

    public function getStrTipoMedidorCombustible($tipo)
    {
        return array_search($tipo, $this->strTipoMedidorCombustible);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @ORM\OrderBy({"nombre" = "ASC"})
     * @Assert\Regex(
     *     pattern="/^[\w\s\(\)\@\:\-\_\*\+\.á-úÁ-Ú\$\&\?\¿°º\/]+$/",
     *     message="Contiene caracteres inválidos. No se permite coma y punto y coma u otros similares."
     * )    
     */
    private $nombre;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var time $fechaAlta
     *
     * @ORM\Column(name="fecha_alta", type="datetime", nullable=true)
     */
    private $fecha_alta;

    /**
     * @var date $fechaBaja
     *
     * @ORM\Column(name="fecha_baja", type="datetime", nullable=true)
     */
    private $fecha_baja;

    /**
     * @var integer $estado
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var date $fechaUltConfig
     *
     * @ORM\Column(name="fecha_ult_config", type="datetime", nullable=true)
     */
    private $fechaUltConfig;

    /**
     * Indica fecha del ultimo cambio de odometro u horometro.
     * @var date $fechaUltCambio
     *
     * @ORM\Column(name="fecha_ult_cambio", type="datetime", nullable=true)
     */
    private $fechaUltCambio;

    /**
     * Especifica el color de fondo de las letras del equipo que se va a mostrar 
     * en mapas. Esto lo han solicitado como forma de distingir entre equipos
     * en la pantalla.     
     * @var string $color
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string $ultEvento
     *
     * @ORM\Column(name="ult_evento", type="integer", nullable=true)
     */
    private $ultEvento;

    /**
     * @var float $ultLatitud
     *
     * @ORM\Column(name="ult_latitud", type="float", nullable=true)
     */
    private $ultLatitud;

    /**
     * @var float $ultLongitud
     *
     * @ORM\Column(name="ult_longitud", type="float", nullable=true)
     */
    private $ultLongitud;

    /**
     * @var datetime $ultFechaHora
     *
     * @ORM\Column(name="ult_fechahora", type="datetime", nullable=true)
     */
    private $ultFechaHora;

    /**
     * @var datetime $ultFechaRecepcion
     *
     * @ORM\Column(name="ult_fecharecepcion", type="datetime", nullable=true)
     */
    private $ultFechaRecepcion;

    /**
     * @var string $ultModo
     *
     * @ORM\Column(name="ult_modo", type="integer", nullable=true)
     */
    private $ultModo;

    /**
     * @var string $ultDireccion
     *
     * @ORM\Column(name="ult_direccion", type="integer", nullable=true)
     */
    private $ultDireccion;

    /**
     * @var float $ultVelocidad
     *
     * @ORM\Column(name="ult_velocidad", type="float", nullable=true)
     */
    private $ultVelocidad;

    /**
     * @var string $ultIP
     *
     * @ORM\Column(name="ult_ibutton", type="string", length=255, nullable=true)
     */
    private $ultIbutton;

    /**
     * @var string $ultIP
     *
     * @ORM\Column(name="ult_ip", type="string", length=255, nullable=true)
     */
    private $ultIP;

    /**
     * @var string $ultIP
     *
     * @ORM\Column(name="ult_ip_capturador", type="string", length=255, nullable=true)
     */
    private $ultIPCapturador;

    /**
     * @var integer $ultBateria
     *
     * @ORM\Column(name="ult_bateria", type="integer", nullable=true)
     */
    private $ultBateria;

    /**
     * @var string $ultIdHistorial
     *
     * @ORM\Column(name="ult_idhistorial", type="integer", nullable=true)
     */
    private $ultIdHistorial;

    /**
     * @var string $ultPuerto
     *
     * @ORM\Column(name="ult_puerto", type="integer", nullable=true)
     */
    private $ultPuerto;

    /**
     * @var integer $ultOdometro
     *
     * @ORM\Column(name="ult_odometro", type="bigint", nullable=true)
     */
    private $ultOdometro;

    /**
     * Variable de ajuste de odometro expresado en metros +/-
     * @var integer $ajusteOdometro
     *
     * @ORM\Column(name="ajuste_odometro", type="integer", nullable=true)
     */
    private $ajusteOdometro;

    /**
     * Variable de ajuste de horometro expresado en horas +/-
     * @var integer $ajusteHorometro
     *
     * @ORM\Column(name="ajuste_horometro", type="integer", nullable=true)
     */
    private $ajusteHorometro;

    /**
     * @var integer $ultHorometro
     *
     * @ORM\Column(name="ult_horometro", type="integer", nullable=true)
     */
    private $ultHorometro;

    /**
     * @var float $ultTemperatura
     *
     * @ORM\Column(name="ult_temperatura", type="float", nullable=true)
     */
    private $ultTemperatura;

    /**
     * @var float $ultHumedad
     *
     * @ORM\Column(name="ult_humedad", type="float", nullable=true)
     */
    private $ultHumedad;

    /**
     * @var string $ultValido
     *
     * @ORM\Column(name="ult_valido", type="boolean", nullable=true)
     */
    private $ultValido;

    /**
     * @var string $ultValido
     *
     * @ORM\Column(name="ult_contacto", type="boolean", nullable=true)
     */
    private $ultContacto;

    /**
     * Es un string byte (000, todos los input off, 010, input2 = on, 110, input 1 y 2 en on
     * @var string $ultValido
     *
     * @ORM\Column(name="ult_inputs", type="string", nullable=true)
     */
    private $ultInputs;

    /**
     * Esto corresponde al RUT que tenga el servicio. Lo han pedido los de chile para 
     * shell. Se da en los casos en que un cliente tenga varios camiones y cada uno de 
     * ellos tenga un rut diferente con el que se deba informar a transtel.
     * @var string $ultValido
     *
     * @ORM\Column(name="dato_fiscal", type="string", nullable=true)
     */
    private $dato_fiscal;

    /**
     * Es un string byte (000, todos los input off, 010, input2 = on, 110, input 1 y 2 en on
     * @var string $ultValido
     *
     * @ORM\Column(name="ult_trama", type="text", nullable=true)
     */
    private $ultTrama;

    /**
     * @ORM\Column(name="accesorios", type="array", nullable=true)
     */
    private $accesorios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="servicio")
     */
    protected $peticiones;

    /**
     * @ORM\Column(name="tipo_objeto", type="integer", nullable=true)
     */
    private $tipoObjeto = self::TIPOOBJETO_PERSONA;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vehiculo", inversedBy="servicio", cascade={"persist"})
     * @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id",  onDelete="CASCADE", nullable=true)
     */
    private $vehiculo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Equipo", inversedBy="servicio")
     * @ORM\JoinColumn(name="equipo_id", referencedColumnName="id", onDelete="SET NULL", nullable=true))
     */
    private $equipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoServicio", inversedBy="servicios")
     * @ORM\JoinColumn(name="tipoServicio_id", referencedColumnName="id")
     */
    protected $tipoServicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistorialServicio", mappedBy="servicio")
     */
    protected $historialServicios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraServicio", mappedBy="servicio",cascade={"persist","remove"})
     */
    protected $bitacoraServicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CargaCombustible", mappedBy="servicio",cascade={"persist","remove"})
     */
    protected $cargasCombustible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Panico", mappedBy="servicio")
     */
    protected $panicos;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\GrupoServicio", inversedBy="servicios")
     * @ORM\JoinTable(name="servicioGrupoServ",
     *      joinColumns={@ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="grupoServicio_id", referencedColumnName="id")}
     *      )
     */
    private $grupos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sensor", mappedBy="servicio")
     */
    protected $sensores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioInputs", mappedBy="servicio")
     */
    protected $inputs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Programacion", mappedBy="servicio",cascade={"persist","remove"})
     */
    protected $programaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Parametro", mappedBy="servicio",cascade={"persist","remove"})
     */
    protected $parametros;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Referencia", mappedBy="servicios")
     */
    private $referencias;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\ReferenciaGpm", mappedBy="servicios")
     */
    private $referenciasGpm;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", mappedBy="servicios")
     */
    private $usuarios;

    /**
     * Owning Side
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CentroCosto", inversedBy="servicios")
     * @ORM\JoinColumn(name="centrocosto_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $centrocosto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="servicios")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Evento", mappedBy="servicios")
     */
    private $eventos;

    /** EventoHistorial
     * @ORM\OneToMany(targetEntity="App\Entity\EventoHistorial", mappedBy="servicio", cascade={"persist"})
     */
    protected $eventohistorial;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="servicio", cascade={"persist"})
     */
    protected $eventosTemporal;


    /** Mantenimientos
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimiento", mappedBy="servicio", cascade={"persist"})
     */
    protected $mantenimientos;

    /**
     * Ordenes Trabajo
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="servicio", cascade={"persist"})
     */
    protected $ordenesTrabajo;

    /** ServicioTanque
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioTanque", mappedBy="servicio", cascade={"persist"})
     */
    protected $medicionestanque;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RegistroShell", inversedBy="servicio", cascade={"persist"})
     * @ORM\JoinColumn(name="registroshell_id", referencedColumnName="id",  onDelete="SET NULL", nullable=true)
     */
    private $registro_shell;

    /** ServicioCanbus
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioCanbus", mappedBy="servicio", cascade={"persist"})
     */
    protected $registrocanbus;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestacion", mappedBy="servicio", cascade={"persist"})
     */
    private $prestaciones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServicioMantenimientoHistorico", mappedBy="servicio")
     */
    protected $servicioMantenimientos;

     /**
      * Inverse Side
      * @ORM\OneToMany(targetEntity="App\Entity\ItinerarioServicio", mappedBy="servicio", cascade={"persist"})
      */
     private $itinerarios;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Satelital", inversedBy="servicios")
     * @ORM\JoinColumn(name="satelital_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $satelital;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transporte", inversedBy="servicios")
     * @ORM\JoinColumn(name="transporte_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $transporte;

    /**
     * @ORM\OneToOne(targetEntity="PuntoCarga", mappedBy="servicio")
     */
    private $puntoCarga;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActgpmServicio", mappedBy="servicio",cascade={"remove"})
     */
    private $actgpmServicios;

    /**
     * @var chofer
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Chofer", mappedBy="servicio")
     */
    private $chofer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IndiceServicio", mappedBy="servicio",cascade={"persist","remove"})
     */
    protected $indiceServicio;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Logistica", inversedBy="servicios")
     * @ORM\JoinTable(name="servicio_logistica",
     *      joinColumns={@ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="logistica_id", referencedColumnName="id")}
     *      )
     */
    private $logisticas;

    /** 
     * @ORM\OneToMany(targetEntity="App\Entity\HistoricoItinerario", mappedBy="servicio", cascade={"persist"})
     */
    protected $historicoItinerario;

      /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChoferServicioHistorico", mappedBy="servicio")
     */
    protected $choferServicioHistorico;


    //==============================================================================    
    //==============================================================================    
    //==============================================================================    
    //==============================================================================    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ultEvento
     *
     * @param string $ultEvento
     */
    public function setUltEvento($ultEvento)
    {
        $this->ultEvento = $ultEvento;
    }

    /**
     * Get ultEvento
     *
     * @return string 
     */
    public function getUltEvento()
    {
        return $this->ultEvento;
    }

    /**
     * Set ultLatitud
     *
     * @param float $ultLatitud
     */
    public function setUltLatitud($ultLatitud)
    {
        $this->ultLatitud = $ultLatitud;
    }

    /**
     * Get ultLatitud
     *
     * @return float 
     */
    public function getUltLatitud()
    {
        return $this->ultLatitud;
    }

    /**
     * Set ultLongitud
     *
     * @param float $ultLongitud
     */
    public function setUltLongitud($ultLongitud)
    {
        $this->ultLongitud = $ultLongitud;
    }

    /**
     * Get ultLongitud
     *
     * @return float 
     */
    public function getUltLongitud()
    {
        return $this->ultLongitud;
    }

    /**
     * Set ultFechaHora
     *
     * @param datetime $ultFechaHora
     */
    public function setUltFechaHora($ultFechaHora)
    {
        $this->ultFechaHora = $ultFechaHora;
    }

    /**
     * Get ultFechaHora
     *
     * @return datetime 
     */
    public function getUltFechaHora()
    {
        return $this->ultFechaHora;
    }

    /**
     * Set ultModo
     *
     * @param string $ultModo
     */
    public function setUltModo($ultModo)
    {
        $this->ultModo = $ultModo;
    }

    /**
     * Get ultModo
     *
     * @return string 
     */
    public function getUltModo()
    {
        return $this->ultModo;
    }

    /**
     * Set ultDireccion
     *
     * @param string $ultDireccion
     */
    public function setUltDireccion($ultDireccion)
    {
        $this->ultDireccion = $ultDireccion;
    }

    /**
     * Get ultDireccion
     *
     * @return string 
     */
    public function getUltDireccion()
    {
        return $this->ultDireccion;
    }

    /**
     * Set ultVelocidad
     *
     * @param float $ultVelocidad
     */
    public function setUltVelocidad($ultVelocidad)
    {
        $this->ultVelocidad = $ultVelocidad;
    }

    /**
     * Get ultVelocidad
     *
     * @return float 
     */
    public function getUltVelocidad()
    {
        return $this->ultVelocidad;
    }

    /**
     * Set ultIP
     *
     * @param string $ultIP
     */
    public function setUltIP($ultIP)
    {
        $this->ultIP = $ultIP;
    }

    /**
     * Get ultIP
     *
     * @return string 
     */
    public function getUltIP()
    {
        return $this->ultIP;
    }

    /**
     * Set ultBateria
     *
     * @param integer $ultBateria
     */
    public function setUltBateria($ultBateria)
    {
        $this->ultBateria = $ultBateria;
    }

    /**
     * Get ultBateria
     *
     * @return integer 
     */
    public function getUltBateria()
    {
        return $this->ultBateria;
    }

    /**
     * Set ultIdHistorial
     *
     * @param string $ultIdHistorial
     */
    public function setUltIdHistorial($ultIdHistorial)
    {
        $this->ultIdHistorial = $ultIdHistorial;
    }

    /**
     * Get ultIdHistorial
     *
     * @return string 
     */
    public function getUltIdHistorial()
    {
        return $this->ultIdHistorial;
    }

    /**
     * Set ultPuerto
     *
     * @param string $ultPuerto
     */
    public function setUltPuerto($ultPuerto)
    {
        $this->ultPuerto = $ultPuerto;
    }

    /**
     * Get ultPuerto
     *
     * @return string 
     */
    public function getUltPuerto()
    {
        return $this->ultPuerto;
    }

    /**
     * Set ultOdometro
     *
     * @param integer $ultOdometro
     */
    public function setUltOdometro($ultOdometro)
    {
        $this->ultOdometro = $ultOdometro;
    }

    /**
     * Get ultOdometro
     *
     * @return integer 
     */
    public function getUltOdometro()
    {
        return $this->ultOdometro;
    }

    /**
     * Set ultHorometro
     *
     * @param integer $ultHorometro
     */
    public function setUltHorometro($ultHorometro)
    {
        $this->ultHorometro = $ultHorometro;
    }

    /**
     * Get ultHorometro
     *
     * @return integer 
     */
    public function getUltHorometro()
    {
        return $this->ultHorometro;
    }

    /**
     * Set ultTemperatura
     *
     * @param float $ultTemperatura
     */
    public function setUltTemperatura($ultTemperatura)
    {
        $this->ultTemperatura = $ultTemperatura;
    }

    /**
     * Get ultTemperatura
     *
     * @return float 
     */
    public function getUltTemperatura()
    {
        return $this->ultTemperatura;
    }

    /**
     * Set ultHumedad
     *
     * @param float $ultHumedad
     */
    public function setUltHumedad($ultHumedad)
    {
        $this->ultHumedad = $ultHumedad;
    }

    /**
     * Get ultHumedad
     *
     * @return float 
     */
    public function getUltHumedad()
    {
        return $this->ultHumedad;
    }

    /**
     * Set ultValido
     *
     * @param string $ultValido
     */
    public function setUltValido($ultValido)
    {
        $this->ultValido = $ultValido;
    }

    /**
     * Get ultValido
     *
     * @return string 
     */
    public function getUltValido()
    {
        return $this->ultValido;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @param date $fechaAlta
     */
    public function setFechaAlta($fecha_alta)
    {
        $this->fecha_alta = $fecha_alta;
    }

    /**
     * @return date 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fechaBaja
     *
     * @param date $fechaBaja
     */
    public function setFechaBaja($fecha_baja)
    {
        $this->fecha_baja = $fecha_baja;;
    }

    /**
     * Get fechaBaja
     *
     * @return date 
     */
    public function getFechaBaja()
    {
        return $this->fecha_baja;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    public function getStrEstado()
    {
        if ($this->estado != null) {
            return $this->strEstado[$this->estado];
        } else {
            return $this->strEstado[0];
        }
    }

    /**
     * Set fechaUltConfig
     *
     * @param date $fechaUltConfig
     */
    public function setFechaUltConfig($fechaUltConfig)
    {
        $this->fechaUltConfig = $fechaUltConfig;
    }

    /**
     * Get fechaUltConfig
     *
     * @return date 
     */
    public function getFechaUltConfig()
    {
        return $this->fechaUltConfig;
    }

    /**
     * Set color
     *
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    public function __construct()
    {
        $this->historialServicios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->programaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parametros = new \Doctrine\Common\Collections\ArrayCollection();
        $this->servicioMantenimientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referencias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referenciasGpm = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cargasCombustible = new \Doctrine\Common\Collections\ArrayCollection();
        $this->choferServicioHistorico = new \Doctrine\Common\Collections\ArrayCollection();


        if ($this->fecha_alta == null) {
            //            $this->fecha_alta = new \DateTime('d/m/Y H:i:s');
        }
    }

    /**
     * Set vehiculo
     *
     * @param App\Entity\Vehiculo $vehiculo
     */
    public function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;
    }

    /**
     * Get vehiculo
     *
     * @return App\Entity\Vehiculo 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set equipo
     *
     * @param App\Entity\Equipo $equipo
     */
    public function setEquipo($equipo)
    {
        $this->equipo = $equipo;
    }

    /**
     * Get equipo
     *
     * @return App\Entity\Equipo 
     */
    public function getEquipo()
    {
        return $this->equipo;
    }

    /**
     * Set tipoServicio
     *
     * @param App\Entity\TipoServicio $tipoServicio
     */
    public function setTipoServicio(\App\Entity\TipoServicio $tipoServicio)
    {
        $this->tipoServicio = $tipoServicio;
    }

    /**
     * Get tipoServicio
     *
     * @return App\Entity\TipoServicio 
     */
    public function getTipoServicio()
    {
        return $this->tipoServicio;
    }

    /**
     * Add historialServicios
     *
     * @param App\Entity\HistorialServicio $historialServicios
     */
    public function addHistorialServicio(\App\Entity\HistorialServicio $historialServicios)
    {
        $this->historialServicios[] = $historialServicios;
    }

    /**
     * Get historialServicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHistorialServicios()
    {
        return $this->historialServicios;
    }

    public function addCargaCombustible($carga)
    {
        $this->cargasCombustible[] = $carga;
    }

    public function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    /**
     * Add grupos
     *
     * @param App\Entity\GrupoServicio $grupos
     */
    public function addGrupoServicio(\App\Entity\GrupoServicio $grupos)
    {
        $this->grupos->add($grupos);
    }

    public function removeGrupoServicios($grupo)
    {
        $this->grupos->removeElement($grupo);
        return $this->grupos;
    }

    /**
     * Get grupos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    /**
     * Add programaciones
     *
     * @param App\Entity\Programacion $programaciones
     */
    public function addProgramacion($programaciones)
    {
        $this->programaciones[] = $programaciones;
    }

    /**
     * Get programaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProgramaciones()
    {
        return $this->programaciones;
    }

    /**
     * Add servicioMantenimientos
     *
     * @param App\Entity\ServicioMantenimiento $servicioMantenimientos
     */
    public function addServicioMantenimiento(\App\Entity\ServicioMantenimiento $servicioMantenimientos)
    {
        $this->servicioMantenimientos[] = $servicioMantenimientos;
    }

    /**
     * Get servicioMantenimiento
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServicioMantenimiento()
    {
        return $this->servicioMantenimientos;
    }

    /**
     * Add referencias
     *
     * @param App\Entity\Referencia $referencias
     */
    public function addReferencia(Referencia $referencias)
    {
        $this->referencias[] = $referencias;
    }

    public function addReferenciaGpm(ReferenciaGpm $referencias)
    {
        $this->referenciasGpm[] = $referencias;
    }

    /**
     * Get referencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }

    /**
     * Set organizacion
     *
     * @param App\Entity\Organizacion $organizacion
     */
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get organizacion
     *
     * @return App\Entity\Organizacion 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setTipoObjeto($tipoObjeto)
    {
        $this->tipoObjeto = $tipoObjeto;
    }

    public function getTipoObjeto()
    {
        return $this->tipoObjeto;
    }

    public function getStrTipoObjetos()
    {
        if ($this->tipoObjeto != null) {
            return $this->strTipoObjetos[$this->tipoObjeto];
        } else {
            return $this->strTipoObjetos[0];
        }
    }

    public function getArrayStrTipoObjetos()
    {
        return $this->strTipoObjetos;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getUltIPCapturador()
    {
        return $this->ultIPCapturador;
    }

    public function setUltIPCapturador($ipCapturador)
    {
        $this->ultIPCapturador = $ipCapturador;
    }

    /**
     * Add usuarios
     *
     * @param App\Entity\Usuario $usuarios
     */
    public function addUsuario(\App\Entity\Usuario $usuario)
    {
        $this->usuarios[] = $usuario;
    }

    /**
     * Get usuarios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function getUltInputs()
    {
        return $this->ultInputs;
    }

    public function setUltInputs($ultInputs)
    {
        $this->ultInputs = $ultInputs;
    }

    public function getUltContacto()
    {
        return $this->ultContacto;
    }

    public function setUltContacto($ultcontacto)
    {
        $this->ultContacto = $ultcontacto;
    }

    public function getEventos()
    {
        return $this->eventos;
    }

    public function setEventos($eventos)
    {
        $this->eventos = $eventos;
    }

    public function getStrUltDireccion()
    {
        $dir = $this->getUltDireccion();
        if ($dir == 0) {
            return 'N';
        } elseif ($dir == 90) {
            return 'E';
        } elseif ($dir == 180) {
            return 'S';
        } elseif ($dir == 270) {
            return 'O';
        } else {
            if (in_array($dir, range(1, 89))) {
                return 'NE';
            } elseif (in_array($dir, range(91, 179))) {
                return 'SE';
            } elseif (in_array($dir, range(181, 269))) {
                return 'SO';
            } elseif (in_array($dir, range(271, 359))) {
                return 'NO';
            } else {
                return '';
            }
        }
    }

    public function getEventohistorial()
    {
        return $this->eventohistorial;
    }

    public function setEventohistorial($eventohistorial)
    {
        $this->eventohistorial = $eventohistorial;
    }

    public function getUltTrama()
    {
        return $this->ultTrama;
    }

    public function setUltTrama($ultTrama)
    {
        $this->ultTrama = $ultTrama;
    }

    public function getMantenimientos()
    {
        return $this->mantenimientos;
    }

    public function setMantenimientos($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    public function getDatoFiscal()
    {
        return $this->dato_fiscal;
    }

    public function setDatoFiscal($dato_fiscal)
    {
        $this->dato_fiscal = $dato_fiscal;
    }

    public function getRegistroShell()
    {
        return $this->registro_shell;
    }

    public function setRegistroShell($registro_shell)
    {
        $this->registro_shell = $registro_shell;
    }

    public function getPanicos()
    {
        return $this->panicos;
    }

    public function setPanicos($panicos)
    {
        $this->panicos = $panicos;
    }

    public function getParametros()
    {
        return $this->parametros;
    }

    public function setParametros($parametros)
    {
        $this->parametros = $parametros;
        return $this;
    }

    /**
     * Add Parametro
     *
     * @param App\Entity\Parametro $parametro
     */
    public function addParametro($parametro)
    {
        $this->parametros[] = $parametro;
    }

    public function getSensores()
    {
        return $this->sensores;
    }

    public function setSensores($sensores)
    {
        $this->sensores = $sensores;
        return $this;
    }

    public function getMedicionestanque()
    {
        return $this->medicionestanque;
    }

    public function setMedicionestanque($medicionestanque)
    {
        $this->medicionestanque = $medicionestanque;
        return $this;
    }

    public function getInputs()
    {
        return $this->inputs;
    }

    public function setInputs($inputs)
    {
        $this->inputs = $inputs;
        return $this;
    }

    public function getBitacoraServicio()
    {
        return $this->bitacoraServicio;
    }

    public function setBitacoraServicio($bitacoraServicio)
    {
        $this->bitacoraServicio = $bitacoraServicio;
        return $this;
    }

    function getAjusteOdometro()
    {
        return $this->ajusteOdometro;
    }

    function setAjusteOdometro($ajusteOdometro)
    {
        $this->ajusteOdometro = $ajusteOdometro;
        return $this;
    }

    function getUltFechaRecepcion()
    {
        return $this->ultFechaRecepcion;
    }

    function setUltFechaRecepcion($ultFechaRecepcion)
    {
        $this->ultFechaRecepcion = $ultFechaRecepcion;
        return $this;
    }

    function getPrestaciones()
    {
        return $this->prestaciones;
    }

    function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;
    }

    public function addPrestacion($prestacion)
    {
        if (!$this->prestaciones->contains($prestacion)) {
            $this->prestaciones[] = $prestacion;
        }
    }

    public function removePrestacion($prestacion)
    {
        $this->prestaciones->removeElement($prestacion);
    }

    public function addEvento($evento)
    {
        if (!$this->eventos->contains($evento)) {
            $this->eventos[] = $evento;
        }
    }

    public function removeEvento($evento)
    {
        $this->eventos->removeElement($evento);
    }

    function getVars()
    {
        $vars = get_object_vars($this);
        unset($vars['strTipoObjetos']);
        unset($vars['strEstado']);
        return $vars;
    }

    public function data2array()
    {
        $d['id'] = $this->id;
        $d['nombre'] = $this->nombre;
        if (!is_null($this->dato_fiscal))
            $d['dato_fiscal'] = $this->dato_fiscal;
        if (!is_null($this->getCorteMotor()))
            $d['corte_motor'] = $this->getCorteMotor();
        if (!is_null($this->getBotonPanico()))
            $d['boton_panico'] = $this->getBotonPanico();
        if (!is_null($this->getMedidorCombustible()))
            $d['medidor_combustible'] = $this->getMedidorCombustible();
        if (!is_null($this->getEntradasDigitales()))
            $d['entradas_digitales'] = $this->getEntradasDigitales();
        if (!is_null($this->getIsCustodio()))
            $d['custodio'] = $this->getIsCustodio();

        if (!is_null($this->vehiculo)) {
            $d['vehiculo'] = $this->vehiculo->data2array();
        }
        return $d;
    }

    function getRegistrocanbus()
    {
        return $this->registrocanbus;
    }

    function setRegistrocanbus($registrocanbus)
    {
        $this->registrocanbus = $registrocanbus;
    }

    function getAjusteHorometro()
    {
        return $this->ajusteHorometro;
    }

    function setAjusteHorometro($ajusteHorometro)
    {
        $this->ajusteHorometro = $ajusteHorometro;
    }

    function getUltIbutton()
    {
        return $this->ultIbutton;
    }

    function setUltIbutton($ultIbutton)
    {
        $this->ultIbutton = $ultIbutton;
    }

    /**
     * Get formatHorometro
     *
     * @return string 
     */
    public function formatHorometro()
    {
        $horas = floor($this->ultHorometro / 60);
        $minutos2 = $this->ultHorometro % 60;
        //aca solo formateo el 0 antes del nro.
        if ($horas < 10)
            $horas = '0' . $horas;
        if ($minutos2 < 10)
            $minutos2 = '0' . $minutos2;

        return sprintf('%s:%s', $horas, $minutos2);
    }

    function getServicioMantenimientos()
    {
        return $this->servicioMantenimientos;
    }

    function setServicioMantenimientos($servicioMantenimientos)
    {
        $this->servicioMantenimientos = $servicioMantenimientos;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    function getCentrocosto()
    {
        return $this->centrocosto;
    }

    function setCentrocosto($centrocosto)
    {
        $this->centrocosto = $centrocosto;
    }

    function getFechaUltCambio()
    {
        return $this->fechaUltCambio;
    }

    function setFechaUltCambio($fechaUltCambio)
    {
        $this->fechaUltCambio = $fechaUltCambio;
    }

    public function getHorometro()
    {
        return isset($this->accesorios['horometro']) ? $this->accesorios['horometro'] : false;
    }

    public function setHorometro($horometro)
    {
        $this->accesorios['horometro'] = $horometro;
    }

    public function getCorteMotor()
    {
        return isset($this->accesorios['cortemotor']) ? $this->accesorios['cortemotor'] : false;
    }

    public function setCorteMotor($corte)
    {
        $this->accesorios['cortemotor'] = $corte;
    }

    public function getCorteCerrojo()
    {
        return isset($this->accesorios['cortecerrojo']) ? $this->accesorios['cortecerrojo'] : false;
    }

    public function setCorteCerrojo($corte)
    {
        $this->accesorios['cortecerrojo'] = $corte;
    }

    public function getBotonPanico()
    {
        return isset($this->accesorios['botonpanico']) ? $this->accesorios['botonpanico'] : false;
    }

    public function setBotonPanico($panico)
    {
        $this->accesorios['botonpanico'] = $panico;
    }

    public function getMedidorCombustible()
    {
        return isset($this->accesorios['combustible']) ? $this->accesorios['combustible'] : false;
    }

    public function setMedidorCombustible($medidor)
    {
        $this->accesorios['combustible'] = $medidor;
    }

    //tipo de medidor de combustible
    public function getTipoMedidorCombustible()
    {
        return isset($this->accesorios['tipoMedidorCombustible']) ? $this->accesorios['tipoMedidorCombustible'] : 0;
    }

    public function setTipoMedidorCombustible($tipo)
    { //tipo 1 = porcentaje, 2 = litros, 3 = ambos
        $this->accesorios['tipoMedidorCombustible'] = $tipo;
    }

    public function getEntradasDigitales()
    {
        return isset($this->accesorios['inputs']) ? $this->accesorios['inputs'] : false;
    }

    public function setEntradasDigitales($inputs)
    {
        $this->accesorios['inputs'] = $inputs;
    }

    public function getCanbus()
    {
        return isset($this->accesorios['canbus']) ? $this->accesorios['canbus'] : false;
    }

    public function setCanbus($canbus)
    {
        $this->accesorios['canbus'] = $canbus;
    }

    public function getMedicionCombustible(){
        return $this->getCanbus() || $this->getMedidorCombustible();
    }
    //custodio
    public function getIsCustodio()
    {
        return isset($this->accesorios['custodio']) ? $this->accesorios['custodio'] : false;
    }

    public function setIsCustodio($tipo)
    {
        $this->accesorios['custodio'] = $tipo;
    }

    function getItinerarios()
    {
        return $this->itinerarios;
    }

    function setItinerarios($itinerarios)
    {
        $this->itinerarios = $itinerarios;
    }

    function getSatelital()
    {
        return $this->satelital;
    }

    function setSatelital($satelital)
    {
        $this->satelital = $satelital;
    }

    function getTransporte()
    {
        return $this->transporte;
    }

    function setTransporte($transporte)
    {
        $this->transporte = $transporte;
    }

    function getOdometroGps()
    {
        $trama = json_decode($this->getUltTrama(), true);
        if (isset($trama['odometro_gps'])) {
            return abs(intval($trama['odometro_gps']));
        } else {
            return 0;
        }
    }

    function getPuntoCarga()
    {
        return $this->puntoCarga;
    }

    function setPuntoCarga($puntoCarga)
    {
        $this->puntoCarga = $puntoCarga;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }

    function getActgpmServicios()
    {
        return $this->actgpmServicios;
    }

    function setActgpmServicios($actgpmServicios)
    {
        $this->actgpmServicios = $actgpmServicios;
    }

    public function getChofer()
    {
        return $this->chofer;
    }

    public function setChofer($chofer): void
    {
        $this->chofer = $chofer;
    }

    public function getIndiceServicio()
    {
        return $this->indiceServicio;
    }

    public function setIndiceServicio($indiceServicio): void
    {
        $this->indiceServicio = $indiceServicio;
    }

    /**
     * Get the value of eventosTemporal
     */
    public function getEventosTemporal()
    {
        return $this->eventosTemporal;
    }

    /**
     * Set the value of eventosTemporal
     *
     * @return  self
     */
    public function setEventosTemporal($eventosTemporal)
    {
        $this->eventosTemporal = $eventosTemporal;

        return $this;
    }

    /**
     * Get owning Side
     */
    public function getLogisticas()
    {
        return $this->logisticas;
    }

    /**
     * Set owning Side
     *
     * @return  self
     */
    public function setLogisticas($logisticas)
    {
        $this->logisticas = $logisticas;

        return $this;
    }

    public function addLogistica($logistica)
    {
        $this->logisticas[] = $logistica;
    }

    public function removeLogistica($logistica)
    {
        $this->logisticas->removeElement($logistica);
        return $this->logisticas;
    }

    public function addItinerarios($itinerario)
    {
        $this->itinerarios[] = $itinerario;
    }

    public function removeItinerarios($itinerario)
    {
        $this->itinerarios->removeElement($itinerario);
        return $this->itinerarios;
    }

    /**
     * Get the value of historicoItinerario
     */ 
    public function getHistoricoItinerario()
    {
        return $this->historicoItinerario;
    }

    /**
     * Set the value of historicoItinerario
     *
     * @return  self
     */ 
    public function setHistoricoItinerario($historicoItinerario)
    {
        $this->historicoItinerario = $historicoItinerario;

        return $this;
    }

    public function addChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico[] = $choferServicioHistorico;
    }

    public function removeChoferServicioHistorico(ChoferServicioHistorico $choferServicioHistorico)
    {
        $this->choferServicioHistorico->removeElement($choferServicioHistorico);
        return $this;
    }

    /**
     * Get the value of choferServicioHistorico
     */ 
    public function getChoferServicioHistorico()
    {
        return $this->choferServicioHistorico;
    }
}
