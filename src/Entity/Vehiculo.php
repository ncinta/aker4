<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * App\Entity\Vehiculo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\VehiculoRepository")
 */
class Vehiculo
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $patente
     *
     * @ORM\Column(name="patente", type="string", length=255, nullable=true, unique=true)
     */
    private $patente;

    /**
     * @var string $modelo
     *
     * @ORM\Column(name="modelo", type="string", length=255, nullable=true)
     */
    private $modelo;

    /**
     * @var string $color
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string $numeroCedularVerde
     *
     * @ORM\Column(name="cedulaVerde", type="string", length=255, nullable=true)
     */
    private $cedulaVerde;

    /**
     * @var integer $odometro
     *
     * @ORM\Column(name="odometro", type="integer", nullable=true)
     */
    private $odometro;

    /**
     * @var integer $horometro
     *
     * @ORM\Column(name="horometro", type="integer", nullable=true)
     */
    private $horometro;

    /**
     * @var string $marca
     *
     * @ORM\Column(name="marca", type="string", length=255, nullable=true)
     */
    private $marca;

    /**
     * @var integer $anioFabricacion
     *
     * @ORM\Column(name="anioFabricacion", type="integer", nullable=true)
     */
    private $anioFabricacion;

    /**
     * @var integer $litros_tanque
     *
     * @ORM\Column(name="litros_tanque", type="integer", nullable=true)
     */
    private $litrosTanque;

    /**
     * Rendimiento medido en kilometros por litro (lts/kms = rendimientos)
     * @var integer $rendimiento
     *
     * @ORM\Column(name="rendimiento", type="float", nullable=true)
     */
    private $rendimiento;

    /**
     * Rendimiento medido en horas por litro (lts/horas = rendimientos)
     * @var integer $rendimiento
     *
     * @ORM\Column(name="rendimiento_hora", type="float", nullable=true)
     */
    private $rendimientoHora;

    /**
     * @var string $numeroMotor
     *
     * @ORM\Column(name="numeroMotor", type="string", length=255, nullable=true)
     */
    private $numeroMotor;

    /**
     * @var string $numeroChasis
     *
     * @ORM\Column(name="numeroChasis", type="string", length=255, nullable=true)
     */
    private $numeroChasis;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoVehiculo", inversedBy="vehiculos")
     * @ORM\JoinColumn(name="tipovehiculo_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoVehiculo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VehiculoModelo", inversedBy="vehiculos")
     * @ORM\JoinColumn(name="carmodelo_id", referencedColumnName="id", nullable=true)
     */
    protected $carModelo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Servicio", mappedBy="vehiculo")
     */
    protected $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehiculoExtra", mappedBy="vehiculo", cascade={"persist"})
     */
    private $datosExtra;

    public function __construct()
    {
        $this->datosExtra = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDatosExtra($datosExtra)
    {
        $this->datosExtra = $datosExtra;
    }

    public function addDatosExtra(\App\Entity\VehiculoExtra $datosExtra)
    {
        $this->datosExtra[] = $datosExtra;
        return $this->datosExtra;
    }

    /**
     * Set patente
     *
     * @param string $patente
     */
    public function setPatente($patente)
    {
        $this->patente = $patente;
    }

    /**
     * Get patente
     *
     * @return string 
     */
    public function getPatente()
    {
        return $this->patente;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set color
     *
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set odometro
     *
     * @param integer $odometro
     */
    public function setOdometro($odometro)
    {
        $this->odometro = $odometro;
    }

    /**
     * Get odometro
     *
     * @return integer 
     */
    public function getOdometro()
    {
        return $this->odometro;
    }

    /**
     * Set marca
     *
     * @param string $marca
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set anioFabricacion
     *
     * @param integer $anioFabricacion
     */
    public function setAnioFabricacion($anioFabricacion)
    {
        $this->anioFabricacion = $anioFabricacion;
    }

    /**
     * Get anioFabricacion
     *
     * @return integer 
     */
    public function getAnioFabricacion()
    {
        return $this->anioFabricacion;
    }

    /**
     * Set numeroMotor
     *
     * @param string $numeroMotor
     */
    public function setNumeroMotor($numeroMotor)
    {
        $this->numeroMotor = $numeroMotor;
    }

    /**
     * Get numeroMotor
     *
     * @return string 
     */
    public function getNumeroMotor()
    {
        return $this->numeroMotor;
    }

    /**
     * Set numeroChasis
     *
     * @param string $numeroChasis
     */
    public function setNumeroChasis($numeroChasis)
    {
        $this->numeroChasis = $numeroChasis;
    }

    /**
     * Get numeroChasis
     *
     * @return string 
     */
    public function getNumeroChasis()
    {
        return $this->numeroChasis;
    }

    /**
     * Set tipoVehiculo
     *
     * @param App\Entity\TipoVehiculo $tipoVehiculo
     */
    public function setTipoVehiculo(\App\Entity\TipoVehiculo $tipoVehiculo)
    {
        $this->tipoVehiculo = $tipoVehiculo;
    }

    /**
     * Get tipoVehiculo
     *
     * @return App\Entity\TipoVehiculo 
     */
    public function getTipoVehiculo()
    {
        return $this->tipoVehiculo;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function getLitrosTanque()
    {
        return $this->litrosTanque;
    }

    public function setLitrosTanque($litrosTanque)
    {
        $this->litrosTanque = $litrosTanque;
    }

    public function getRendimiento()
    {
        return $this->rendimiento;
    }

    public function setRendimiento($rendimiento)
    {
        $this->rendimiento = $rendimiento;
    }

    public function getCarModelo()
    {
        return $this->carModelo;
    }

    public function setCarModelo($carModelo)
    {
        $this->carModelo = $carModelo;
    }

    public function data2array()
    {
        if (!is_null($this->id))
            $data['id'] = $this->id;
        if (!is_null($this->patente))
            $data['patente'] = $this->patente;
        if (!is_null($this->odometro))
            $data['odometro'] = $this->odometro;
        if (!is_null($this->horometro))
            $data['horometro'] = $this->horometro;
        if (!is_null($this->marca))
            $data['marca'] = $this->marca;
        if (!is_null($this->modelo))
            $data['modelo'] = $this->modelo;
        if (!is_null($this->anioFabricacion))
            $data['anioFabricacion'] = $this->anioFabricacion;
        if (!is_null($this->color))
            $data['color'] = $this->color;
        if (!is_null($this->litrosTanque))
            $data['litrosTanque'] = $this->litrosTanque;
        if (!is_null($this->numeroChasis))
            $data['numeroChasis'] = $this->numeroChasis;
        if (!is_null($this->numeroMotor))
            $data['numeroMotor'] = $this->numeroMotor;
        if (!is_null($this->rendimiento))
            $data['rendimiento'] = $this->rendimiento;
        return isset($data) ? $data : null;
    }

    function getHorometro()
    {
        return $this->horometro;
    }

    function setHorometro($horometro)
    {
        $this->horometro = $horometro;
    }
    function getDatosExtra()
    {
        return $this->datosExtra;
    }

    function getCedulaVerde()
    {
        return $this->cedulaVerde;
    }

    function setCedulaVerde($cedulaVerde)
    {
        $this->cedulaVerde = $cedulaVerde;
    }

    function getRendimientoHora()
    {
        return $this->rendimientoHora;
    }

    function setRendimientoHora($rendimientoHora)
    {
        $this->rendimientoHora = $rendimientoHora;
    }
}
