<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Description of Stock
 *
 * @author nicolas
 */

/**
 * App\Entity\Stock
 *
 * @ORM\Table(name="stock")
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Stock
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="stock", type="float", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(name="ult_proveedor", type="string", nullable=true)
     */
    private $ultProveedor;

    /**
     * @ORM\Column(name="costo", type="float", nullable=true)
     */
    private $costo;

    /**
     * @ORM\Column(name="ult_compra", type="date", nullable=true)
     */
    private $ultCompra;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Deposito", inversedBy="stocks")
     * @ORM\JoinColumn(name="deposito_id", referencedColumnName="id")     
     * @ORM\OrderBy({"nombre" = "ASC"})
     */
    protected $deposito;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="stocks")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    protected $producto;

    function getId()
    {
        return $this->id;
    }

    function getStock()
    {
        return $this->stock;
    }

    function getUltProveedor()
    {
        return $this->ultProveedor;
    }

    function getCosto()
    {
        return $this->costo;
    }

    function getUltCompra()
    {
        return $this->ultCompra;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setStock($stock)
    {
        $this->stock = $stock;
    }

    function setUltProveedor($ultProveedor)
    {
        $this->ultProveedor = $ultProveedor;
    }

    function setCosto($costo)
    {
        $this->costo = $costo;
    }

    function setUltCompra($ult_compra)
    {
        $this->ultCompra = $ult_compra;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
}
