<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable as JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Dominio
 *
 * @ORM\Table(name="dominio")
 * @ORM\Entity(repositoryClass="App\Repository\DominioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Dominio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="organizacion_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * Dominio. Indica el dominio al que se debe ver cuando antes del login para 
     * determinar el logo del distribuidor.
     * 
     * @var string $dominio
     *
     * @ORM\Column(name="dominio", type="string", unique=true)
     * @ORM\OrderBy({"dominio" = "ASC"})
     */
    private $dominio;

    /**
     * @var string $mensaje
     *
     * @ORM\Column(name="mensaje", type="text", nullable=true)
     */
    private $mensaje;

    /**
     * @var string $pathLogo
     *
     * @ORM\Column(name="pathLogo", type="string", nullable=true)
     */
    private $pathLogo;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $logo;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="dominios", cascade={"persist"})
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organizacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pathLogo
     *
     * @param string $pathLogo
     */
    public function setPathLogo($pathLogo)
    {
        $this->pathLogo = basename($pathLogo);
    }

    /**
     * Get pathLogo
     *
     * @return string 
     */
    public function getPathLogo()
    {
        return $this->pathLogo;
    }

    public function getLogo()
    {
        if ($this->pathLogo != null) {
            return $this->getWebPath();
        } else {
            return null;
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getAbsolutePath()
    {
        return null === $this->pathLogo ? null : $this->getUploadRootDir() . '/' . $this->pathLogo;
    }

    public function getWebPath()
    {
        return null === $this->pathLogo ? null : $this->getUploadDir() . '/' . $this->pathLogo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return '/images/logos';
    }

    public function getFilePath()
    {
        return null === $this->pathLogo ? null : $this->getUploadUrl() . '/' . $this->pathLogo;
    }

    protected function getUploadUrl()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'images/logos';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->logo) {
            return;
        }

        // move takes the target directory and then the target filename to move to
        $this->logo->move($this->getUploadRootDir(), $this->logo->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->pathLogo = $this->logo->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->logo = null;
    }

    public function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    public function getDominio()
    {
        return $this->dominio;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    public function __toString()
    {
        return (string) $this->getDominio();
    }
}
