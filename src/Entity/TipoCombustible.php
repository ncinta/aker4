<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as Unique;

/**
 * Description of TipoCombustible
 *
 * @author nicolas
 */

/**
 * App\Entity\TipoCombustible
 *
 * @ORM\Table(name="tipocombustible", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="tipocomb_idx", columns={"nombre"})
 * })
 * @Unique\UniqueEntity(fields={"nombre"}, message="Ya existe un combustible con este nombre")
 * @ORM\Entity(repositoryClass="App\Repository\TipoCombustibleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TipoCombustible
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @ORM\OrderBy({"nombre" = "ASC"})
     * @Assert\Regex(
     *     pattern="/^[\w\s\(\)\@\:\-\_\*\+\.á-úÁ-Ú\$\&\?\¿°º\/]+$/",
     *     message="Contiene caracteres inválidos. No se permite coma y punto y coma u otros similares."
     * )    
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="PrecioTipoCombustible", mappedBy="tipoCombustible",cascade ={"persist"})
     */
    protected $precioTipoCombustibles;

    /**
     * @ORM\OneToMany(targetEntity="CargaCombustible", mappedBy="tipoCombustible",cascade ={"persist"})
     */
    protected $cargasCombustible;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function getPrecioTipoCombustibles()
    {
        return $this->precioTipoCombustibles;
    }

    function getCargasCombustible()
    {
        return $this->cargasCombustible;
    }

    function setPrecioTipoCombustibles($precioTipoCombustibles): void
    {
        $this->precioTipoCombustibles = $precioTipoCombustibles;
    }

    function setCargasCombustible($cargasCombustible): void
    {
        $this->cargasCombustible = $cargasCombustible;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
