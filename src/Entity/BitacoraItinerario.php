<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Description of BitacoraItinerario
 *
 * @author nicolas
 */

/**
 * App\Entity\BitacoraItinerario
 *
 * @ORM\Table(name="bitacora_itinerario")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraItinerarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BitacoraItinerario
{
    const EVENTO_INDEFINIDO = 0;
    const EVENTO_ITINERARIO_NEW = 1;
    const EVENTO_ITINERARIO_UPDATE = 2;
    const EVENTO_ITINERARIO_CERRAR = 3;
    const EVENTO_ITINERARIO_ESTADO_UPDATE = 4;
    const EVENTO_ITINERARIO_DELETE = 5;
    const EVENTO_ITINERARIO_SERVICIO_UPDATE = 6;
    const EVENTO_ITINERARIO_CUSTODIO_UPDATE = 7;
    const EVENTO_ITINERARIO_REFERENCIA_UPDATE = 8;
    const EVENTO_ITINERARIO_USUARIO_UPDATE = 9;
    const EVENTO_ITINERARIO_CONTACTO_UPDATE = 10;
    const EVENTO_ITINERARIO_COMENTARIO = 11;
    const EVENTO_ITINERARIO_EVENTO_UPDATE = 12;
    const EVENTO_ITINERARIO_SERVICIO_ESTADO_UPDATE = 13;
    
  
    private $TipoEventoDescipcion = array(
        self::EVENTO_INDEFINIDO => array('desc' => 'Indefinido', 'nivel' => 0),
        self::EVENTO_ITINERARIO_NEW => array('desc' => 'Alta', 'nivel' => 0),
        self::EVENTO_ITINERARIO_UPDATE => array('desc' => 'Edición', 'nivel' => 0),
        self::EVENTO_ITINERARIO_CERRAR => array('desc' => 'Cerrar Itinerario', 'nivel' => 0),
        self::EVENTO_ITINERARIO_ESTADO_UPDATE => array('desc' => 'Edición de estado', 'nivel' => 0),
        self::EVENTO_ITINERARIO_DELETE => array('desc' => 'Baja de itinerario', 'nivel' => 0),
        self::EVENTO_ITINERARIO_SERVICIO_UPDATE => array('desc' => 'Edición de Servicios', 'nivel' => 0),
        self::EVENTO_ITINERARIO_CUSTODIO_UPDATE => array('desc' => 'Edición de Custodios', 'nivel' => 0),
        self::EVENTO_ITINERARIO_REFERENCIA_UPDATE => array('desc' => 'Edición de Puntos de Referencia', 'nivel' => 0),
        self::EVENTO_ITINERARIO_USUARIO_UPDATE => array('desc' => 'Edición de usuarios', 'nivel' => 0),
        self::EVENTO_ITINERARIO_CONTACTO_UPDATE => array('desc' => 'Edición de contacto', 'nivel' => 0),
        self::EVENTO_ITINERARIO_COMENTARIO => array('desc' => 'Se agrega comentario', 'nivel' => 0),
        self::EVENTO_ITINERARIO_EVENTO_UPDATE => array('desc' => 'Edición de evento', 'nivel' => 0),
        self::EVENTO_ITINERARIO_SERVICIO_ESTADO_UPDATE => array('desc' => 'Edición de estado de un servicio', 'nivel' => 0),
    );

    public function getArrayTipoEvento()
    {
        return $this->TipoEventoDescipcion;
    }

    public function getStrTipoEvento()
    {
        if (isset($this->tipo_evento)) {
            return $this->TipoEventoDescipcion[$this->tipo_evento]['desc'];
        } else {
            return '---';
        }
    }


    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="tipo_evento", type="integer")
     */
    private $tipo_evento;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var Itinerario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="bitacoraItinerario")     
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $itinerario;

    /**
     * @var HistoricoItinerario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\HistoricoItinerario", inversedBy="bitacoraItinerario")     
     * @ORM\JoinColumn(name="historicoitinerario_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $historicoItinerario;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
    }

    public function __toString()
    {
        if ($this->estado != null) {
            return $this->TipoEventoDescipcion[$this->tipo_evento];
        } else {
            return $this->TipoEventoDescipcion[0];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getTipoEvento()
    {
        return $this->tipo_evento;
    }

    public function setTipoEvento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getTipoEventoDescipcion()
    {
        return $this->TipoEventoDescipcion;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getTipo_evento()
    {
        return $this->tipo_evento;
    }

    function setTipoEventoDescipcion($TipoEventoDescipcion)
    {
        $this->TipoEventoDescipcion = $TipoEventoDescipcion;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setTipo_evento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }


    function getItinerario()
    {
        return $this->itinerario;
    }

    function setItinerario($itinerario)
    {
        $this->itinerario = $itinerario;
    }



}
