<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\UnidadDeMedida;

/**
 * App\Entity\Variable
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity(repositoryClass="App\Repository\VariableRepository")
 */
class Variable
{

    const TVAR_INTEGER = 1;
    const TVAR_STRING = 2;
    const TVAR_REFERENCIA = 3;
    const TVAR_TELECOMANDO = 4;
    const TVAR_COMPUESTA = 5;

    private $arrayTipo = array(
        self::TVAR_INTEGER => 'Entero',
        self::TVAR_STRING => 'String',
        self::TVAR_REFERENCIA => 'Referencia',
        self::TVAR_TELECOMANDO => 'Telecomando',
        self::TVAR_COMPUESTA => 'Compuesta',
    );

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombreKey
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string $nombreKey
     *
     * @ORM\Column(name="codename", type="string", length=255, nullable=true)
     */
    private $codename;

    /**
     * @var integer $tipoVariable
     *
     * @ORM\Column(name="tipo_variable", type="integer", nullable = true)
     */
    private $tipoVariable;

    /**
     * @var integer $unidadMedida
     *
     * @ORM\Column(name="unidad_medida", type="string", nullable = true)
     */
    private $unidadMedida;

    /**
     * @var string $cotaMinima
     *
     * @ORM\Column(name="cota_minima", type="string", length=255, nullable = true)
     */
    private $cotaMinima;

    /**
     * @var string $cotaMaxima
     *
     * @ORM\Column(name="cota_maxima", type="string", length=255, nullable = true)
     */
    private $cotaMaxima;

    /**
     * @var string $cotaMaxima, nullable = true)
     *
     * @ORM\Column(name="valor_default", type="string", length=255, nullable = true)
     */
    private $valorDefault;

    /**
     *
     * @ORM\Column(name="role", type="string", nullable = true)
     */
    private $role;

    /**
     * @ORM\Column(name="usable", type="boolean", nullable=true)
     */
    private $usable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo", inversedBy="variables")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $modelo;

    /**
     * Esta es la relacion que me relaciona los parametros y variables.
     * @ORM\OneToMany(targetEntity="App\Entity\Parametro", mappedBy="variable", cascade={"persist"})
     */
    protected $parametros;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Variable", mappedBy="variable_padre")
     */
    protected $variables;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Variable", inversedBy="variables")
     * @ORM\JoinColumn(name="variable_padre_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $variable_padre;

    public function getCodename()
    {
        return $this->codename;
    }

    public function setCodename($codename)
    {
        $this->codename = $codename;
        return $this;
    }

    public function getUnidadMedida()
    {
        return $this->unidadMedida;
    }

    public function setUnidadMedida($unidadMedida)
    {
        $this->unidadMedida = $unidadMedida;
        return $this;
    }

    public function getCotaMinima()
    {
        return $this->cotaMinima;
    }

    public function setCotaMinima($cotaMinima)
    {
        $this->cotaMinima = $cotaMinima;
        return $this;
    }

    public function getCotaMaxima()
    {
        return $this->cotaMaxima;
    }

    public function setCotaMaxima($cotaMaxima)
    {
        $this->cotaMaxima = $cotaMaxima;
        return $this;
    }

    public function getValorDefault()
    {
        return $this->valorDefault;
    }

    public function setValorDefault($valorDefault)
    {
        $this->valorDefault = $valorDefault;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
        return $this;
    }

    public function getParametros()
    {
        return $this->parametros;
    }

    public function setParametros($parametros)
    {
        $this->parametros = $parametros;
        return $this;
    }

    public function getArrayTipo()
    {
        return $this->arrayTipo;
    }

    public function getStrTtipo()
    {
        if ($this->tipoVariable != null) {
            return $this->arrayTipo[$this->tipoVariable];
        } else {
            return null;
        }
    }

    public function getTipoVariable()
    {
        return $this->tipoVariable;
    }

    public function setTipoVariable($tipoVariable)
    {
        $this->tipoVariable = $tipoVariable;
        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    function getVariables()
    {
        return $this->variables;
    }

    function getVariablePadre()
    {
        return $this->variable_padre;
    }

    function setVariables($variables)
    {
        $this->variables = $variables;
        return $this;
    }

    function setVariablePadre($variable_padre)
    {
        $this->variable_padre = $variable_padre;
        return $this;
    }

    public function setUsable($usable)
    {
        $this->usable = $usable;
    }

    public function isUsable()
    {
        return (bool) $this->usable;
    }
}
