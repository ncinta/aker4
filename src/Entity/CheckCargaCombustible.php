<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\CheckCargaCombustible
 *
 * @ORM\Table(name="check_carga_combustible")
 * @ORM\Entity(repositoryClass="App\Repository\CheckCargaCombustibleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CheckCargaCombustible
{
    const CARGA_NUEVA = 0;
    const CARGA_IGNORADA = 8;
    const CARGA_CHECKEADA = 9;

    const ESTADO_CARGA= array(
        self::CARGA_NUEVA => 'Nuevas',
        self::CARGA_IGNORADA => 'Ignoradas',
        self::CARGA_CHECKEADA => 'Checkeadas'
    );

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Esta esperando hora local donde se produjo la carga
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(name="litros_carga", type="float", nullable=false)
     */
    private $litrosCarga;

    /**
     * @ORM\Column(name="guia_despacho", type="string", length=255, nullable=true)
     */
    private $guia_despacho;

    /**
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(name="codigo_autorizacion", type="string", length=255, nullable=true)
     */
    private $codigo_autorizacion;

    /**
     * @ORM\Column(name="monto_total", type="float", nullable=true)
     */
    private $monto_total;

    /**
     * 0=manual 1=ws 2=app 3=importacion
     * @ORM\Column(name="modo_ingreso", type="integer", nullable = true)
     */
    private $modo_ingreso;

    /**
     * 0=nueva 9=chequeada
     * @ORM\Column(name="estado", type="integer", nullable = true)
     */
    private $estado;

    /**
     * @ORM\Column(name="odometro", type="float", nullable=true)
     */
    private $odometro;

    /**
     * @ORM\Column(name="odometro_tablero", type="float", nullable=true)
     */
    private $odometroTablero;

    /**
     * @ORM\Column(name="horometro_tablero", type="float", nullable=true)
     */
    private $horometroTablero;

    /**
     * @ORM\Column(name="codigo_estacion", type="string", length=255, nullable=true)
     */
    private $codigo_estacion;

      /**
     * @ORM\Column(name="contrato", type="string", length=255, nullable=true)
     */
    private $contrato;

      /**
     * @ORM\Column(name="localidad", type="string", length=255, nullable=true)
     */
    private $localidad;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(name="session", type="string", length=255, nullable=true)
     */
    private $session;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="cargasCombustible")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Organizacion", inversedBy="checkCargasCombustible")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="Servicio", inversedBy="cargasCombustible")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $servicio;

    /**
     * @ORM\ManyToOne(targetEntity="PuntoCarga", inversedBy="cargasCombustible")
     * @ORM\JoinColumn(name="puntocarga_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $puntoCarga;

    /**
     * @ORM\ManyToOne(targetEntity="Chofer", inversedBy="checkCargasCombustible")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $chofer;

    /**
     * @ORM\ManyToOne(targetEntity="TipoCombustible", inversedBy="cargasCombustible")
     * @ORM\JoinColumn(name="tipocombustible_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $tipoCombustible;

    /**
     * @ORM\ManyToOne(targetEntity="CargaCombustible", inversedBy="checkCombustible")
     * @ORM\JoinColumn(name="cargacombustible_id", referencedColumnName="id", nullable=true)
     */    
    protected $cargaCombustible;

    /**
     * 
     * @ORM\Column(name="fecha_trama", type="datetime", nullable=true)
     */
    private $fechaTrama;

    /**
     * @var string $data
     *
     * @ORM\Column(name="data_archivo", type="array", nullable=true)
     */
    private $dataArchivo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getGuiaDespacho()
    {
        return $this->guia_despacho;
    }

    public function setGuiaDespacho($guia_despacho)
    {
        $this->guia_despacho = $guia_despacho;
    }

    public function getLitrosCarga()
    {
        return $this->litrosCarga;
    }

    public function setLitrosCarga($litros_carga)
    {
        $this->litrosCarga = $litros_carga;
    }

    public function getMontoTotal()
    {
        return $this->monto_total;
    }

    public function setMontoTotal($monto_total)
    {
        $this->monto_total = $monto_total;
    }

    public function getOdometro()
    {
        return $this->odometro;
    }

    public function setOdometro($odometro)
    {
        $this->odometro = $odometro;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function getCodigoEstacion()
    {
        return $this->codigo_estacion;
    }

    public function setCodigoEstacion($codigo_estacion)
    {
        $this->codigo_estacion = $codigo_estacion;
    }

    public function getModoIngreso()
    {
        return $this->modo_ingreso;
    }

    public function setModoIngreso($modo_ingreso)
    {
        $this->modo_ingreso = $modo_ingreso;
    }

    public function getCodigoAutorizacion()
    {
        return $this->codigo_autorizacion;
    }

    public function setCodigoAutorizacion($codigo_autorizacion)
    {
        $this->codigo_autorizacion = $codigo_autorizacion;
    }

    function getSession()
    {
        return $this->session;
    }

    function setSession($session)
    {
        $this->session = $session;
        return $this;
    }

    function getVars()
    {
        return get_object_vars($this);
    }

    public function data2array()
    {
        return array(
            'id' => $this->id,
            'fecha' => $this->fecha->format('d/m/Y H:i:s'),
            'litros_carga' => $this->litros_carga,
            'guia_despacho' => $this->guia_despacho,
            'codigo_autorizacion' => $this->codigo_autorizacion,
            'codigo_estacion' => $this->codigo_estacion,
            'contrato' => $this->contrato,
            'localidad' => $this->localidad,
            'odometro' => $this->odometro,
            'monto_total' => $this->monto_total,
            'modo_ingreso' => $this->modo_ingreso,
            'fecha_trama' => $this->fechaTrama,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud,
            'id_trama' => $this->id_trama,
        );
    }

    function getLitros_carga()
    {
        return $this->litros_carga;
    }

    function getGuia_despacho()
    {
        return $this->guia_despacho;
    }

    function getCodigo_autorizacion()
    {
        return $this->codigo_autorizacion;
    }

    function getMonto_total()
    {
        return $this->monto_total;
    }

    function getModo_ingreso()
    {
        return $this->modo_ingreso;
    }

    function getOdometroTablero()
    {
        return $this->odometroTablero;
    }

    function getCodigo_estacion()
    {
        return $this->codigo_estacion;
    }

    function getLatitud()
    {
        return $this->latitud;
    }

    function setOdometroTablero($odometroTablero)
    {
        $this->odometroTablero = $odometroTablero;
    }

    function setCodigo_estacion($codigo_estacion)
    {
        $this->codigo_estacion = $codigo_estacion;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setId_trama($id_trama)
    {
        $this->id_trama = $id_trama;
    }

    function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    function setFechaTrama($fecha_trama)
    {
        $this->fechaTrama = $fecha_trama;
    }

    function getLongitud()
    {
        return $this->longitud;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function getId_trama()
    {
        return $this->id_trama;
    }

    function getFechaTrama()
    {
        return $this->fechaTrama;
    }

    function getLatitudCarga()
    {
        return $this->latitudCarga;
    }

    function getLongitudCarga()
    {
        return $this->longitudCarga;
    }

    function getUsuario()
    {
        return $this->usuario;
    }

    function setLatitudCarga($latitud_carga)
    {
        $this->latitudCarga = $latitud_carga;
    }

    function setLongitudCarga($longitud_carga)
    {
        $this->longitudCarga = $longitud_carga;
    }

    function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    function getHorometroTablero()
    {
        return $this->horometroTablero;
    }

    function setHorometroTablero($horometroTablero)
    {
        $this->horometroTablero = $horometroTablero;
    }

    function getPuntoCarga()
    {
        return $this->puntoCarga;
    }

    function setPuntoCarga($puntoCarga)
    {
        $this->puntoCarga = $puntoCarga;
    }

    function getChofer()
    {
        return $this->chofer;
    }

    function setChofer($chofer)
    {
        $this->chofer = $chofer;
    }

    function getTipoCombustible()
    {
        return $this->tipoCombustible;
    }

    function setTipoCombustible($tipoCombustible)
    {
        $this->tipoCombustible = $tipoCombustible;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function getCargaCompleta()
    {
        return $this->cargaCompleta;
    }

    public function setCargaCompleta($cargaCompleta): void
    {
        $this->cargaCompleta = $cargaCompleta;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * Get $data
     *
     * @return  string
     */ 
    public function getDataArchivo()
    {
        return $this->dataArchivo;
    }

    /**
     * Set $data
     *
     * @param  string  $dataArchivo  $data
     *
     * @return  self
     */ 
    public function setDataArchivo($dataArchivo)
    {
        $this->dataArchivo = $dataArchivo;

        return $this;
    }

    /**
     * Get the value of cargaCombustible
     */ 
    public function getCargaCombustible()
    {
        return $this->cargaCombustible;
    }

    /**
     * Set the value of cargaCombustible
     *
     * @return  self
     */ 
    public function setCargaCombustible($cargaCombustible)
    {
        $this->cargaCombustible = $cargaCombustible;

        return $this;
    }

    /**
     * Get the value of organizacion
     */ 
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set the value of organizacion
     *
     * @return  self
     */ 
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }
       

    /**
     * Get the value of localidad
     */ 
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set the value of localidad
     *
     * @return  self
     */ 
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get the value of contrato
     */ 
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * Set the value of contrato
     *
     * @return  self
     */ 
    public function setContrato($contrato)
    {
        $this->contrato = $contrato;

        return $this;
    }

}
