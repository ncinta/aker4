<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * Description of IndiceServicio
 *
 * @author nicolas
 */

/**
 * App\Entity\IndiceServicio
 *
 * @ORM\Table(name="indice_servicio", indexes={
 *     @Index(name="fecha_idServicio", columns={"servicio_id","fecha"}),
 *     @Index(name="ind_serv_fecha", columns={"fecha"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\IndiceServicioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IndiceServicio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var date Fecha
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="indiceServicio")     
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $servicio;

    /**
     * cantidad de preventivos pendientes
     * @ORM\Column(name="mantenimiento_pendiente", type="integer", nullable=true)
     */
    private $mantenimiento_pendiente;

    /**
     * cantidad de preventivos realizado
     * @ORM\Column(name="mantenimiento_realizado", type="integer", nullable=true)
     */
    private $mantenimiento_realizado;

    /**
     * cantidad de preventivos nuevos
     * @ORM\Column(name="mantenimiento_nuevo", type="integer", nullable=true)
     */
    private $mantenimiento_nuevo;

    /**
     * cantidad de mantenimiento asignados
     * @ORM\Column(name="mantenimiento_asignado", type="integer", nullable=true)
     */
    private $mantenimiento_asignado;

    /**
     * cantidad de pedidos de peticiones nueva
     * @ORM\Column(name="peticion_nueva", type="integer", nullable=true)
     */
    private $peticion_nueva;

    /**
     * cantidad de pedidos de peticiones pendientes
     * @ORM\Column(name="peticion_pendiente", type="integer", nullable=true)
     */
    private $peticion_pendiente;

    /**
     * cantidad de pedidos de peticiones encurso
     * @ORM\Column(name="peticion_encurso", type="integer", nullable=true)
     */
    private $peticion_encurso;

    /**
     * cantidad de pedidos de peticiones detenida
     * @ORM\Column(name="peticion_detenida", type="integer", nullable=true)
     */
    private $peticion_detenida;

    /**
     * cantidad de pedidos de peticiones realizadas en ese dia
     * @ORM\Column(name="peticiones_realizadas", type="integer", nullable=true)
     */
    private $peticion_realizada;

    /**
     * cantidad de ordenes de trabajo nuevas en ese dia
     * @ORM\Column(name="ordentrabajo_nueva", type="integer", nullable=true)
     */
    private $ordentrabajo_nueva;

    /**
     * cantidad de ordenes de trabajo en curso en ese dia
     * @ORM\Column(name="ordentrabajo_encurso", type="integer", nullable=true)
     */
    private $ordentrabajo_encurso;

    /**
     * cantidad de ordenes de trabajo realizadas en ese dia
     * @ORM\Column(name="ordentrabajo_realizadas", type="integer", nullable=true)
     */
    private $ordentrabajo_realizada;

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function getMantenimientoPendiente()
    {
        return $this->mantenimiento_pendiente;
    }

    public function getMantenimientoAsignado()
    {
        return $this->mantenimiento_asignado;
    }

    public function getPeticionPendiente()
    {
        return $this->peticion_pendiente;
    }

    public function getPeticionRealizada()
    {
        return $this->peticion_realizada;
    }

    public function getOrdentrabajoRealizada()
    {
        return $this->ordentrabajo_realizada;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;
    }

    public function setMantenimientoPendiente($mantenimiento_pendiente)
    {
        $this->mantenimiento_pendiente = $mantenimiento_pendiente;
    }

    public function setMantenimientoAsignado($mantenimiento_asignado)
    {
        $this->mantenimiento_asignado = $mantenimiento_asignado;
    }

    public function setPeticionPendiente($peticion_pendiente)
    {
        $this->peticion_pendiente = $peticion_pendiente;
    }

    public function setPeticionRealizada($peticion_realizada)
    {
        $this->peticion_realizada = $peticion_realizada;
    }

    public function setOrdentrabajoRealizada($ordentrabajo_realizada)
    {
        $this->ordentrabajo_realizada = $ordentrabajo_realizada;
    }

    public function getPeticionEncurso()
    {
        return $this->peticion_encurso;
    }

    public function getPeticionDetenida()
    {
        return $this->peticion_detenida;
    }

    public function setPeticionEncurso($peticion_encurso)
    {
        $this->peticion_encurso = $peticion_encurso;
    }

    public function setPeticionDetenida($peticion_detenida)
    {
        $this->peticion_detenida = $peticion_detenida;
    }

    public function getOrdentrabajoNueva()
    {
        return $this->ordentrabajo_nueva;
    }

    public function getOrdentrabajoEncurso()
    {
        return $this->ordentrabajo_encurso;
    }

    public function setOrdentrabajoNueva($ordentrabajo_nueva)
    {
        $this->ordentrabajo_nueva = $ordentrabajo_nueva;
    }

    public function setOrdentrabajoEncurso($ordentrabajo_encurso)
    {
        $this->ordentrabajo_encurso = $ordentrabajo_encurso;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    public function getMantenimientoRealizado()
    {
        return $this->mantenimiento_realizado;
    }

    public function getMantenimientoNuevo()
    {
        return $this->mantenimiento_nuevo;
    }

    public function setMantenimientoRealizado($mantenimiento_realizado): void
    {
        $this->mantenimiento_realizado = $mantenimiento_realizado;
    }

    public function setMantenimientoNuevo($mantenimiento_nuevo): void
    {
        $this->mantenimiento_nuevo = $mantenimiento_nuevo;
    }
    public function getPeticionNueva()
    {
        return $this->peticion_nueva;
    }

    public function setPeticionNueva($peticion_nueva): void
    {
        $this->peticion_nueva = $peticion_nueva;
    }
}
