<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Servicio as Servicio;
use App\Entity\Usuario as Usuario;
use App\Entity\Evento as Evento;
use App\Entity\EventoTemporal as EventoTemporal;

/**
 *
 * @ORM\Table(name="eventohistorial", 
 *    indexes={
 *       @Index(name="fecha_idx", columns={"fecha"}),
 *       @Index(name="idx_eventohistorial_compuesto", columns={"uid", "evento_id"})
 *    })
 * @ORM\Entity(repositoryClass="App\Repository\EventoHistorialRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EventoHistorial
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titulo
     *
     * @ORM\Column(name="titulo", type="string")
     */
    private $titulo;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var time $fechaVista
     *
     * @ORM\Column(name="fecha_vista", type="datetime", nullable=true)
     */
    private $fecha_vista;

    /**
     * @ORM\Column(name="respuesta", type="text", nullable=true)
     */
    private $respuesta;

    /**
     * @var Ejecutor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="eventos")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * @var Evento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Evento", inversedBy="historial")     
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $evento;

    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="eventohistorial")     
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $servicio;

     /**
     * @var EventoTemporal
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventoTemporal", mappedBy="eventoHistorial"))
     */
    private $eventoTemporal;

    /**
     * @var string $uid
     *
     * @ORM\Column(name="uid", type="string", nullable=true)
     */
    private $uid;
    
     /**
     * @var EventoHistorialNotificacion
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventoHistorialNotificacion", mappedBy="eventoHistorial"))
     */
    private $notificaciones;

    /**
     * @var string
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        //        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->titulo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getEvento()
    {
        return $this->evento;
    }

    public function setEvento(Evento $evento)
    {
        $this->evento = $evento;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getFechaVista()
    {
        return $this->fecha_vista;
    }

    public function getRespuesta()
    {
        return $this->respuesta;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setFechaVista($fecha_vista)
    {
        $this->fecha_vista = $fecha_vista;
        return $this;
    }

    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
        return $this;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
        return $this;
    }

    /**
     * Get the value of eventoTemporal
     *
     * @return  EventoTemporal
     */ 
    public function getEventoTemporal()
    {
        return $this->eventoTemporal;
    }

    /**
     * Set the value of eventoTemporal
     *
     * @param  EventoTemporal  $eventoTemporal
     *
     * @return  self
     */ 
    public function setEventoTemporal(EventoTemporal $eventoTemporal)
    {
        $this->eventoTemporal = $eventoTemporal;

        return $this;
    }

    public function addNotificacion($notificacion)
    {
        $this->notificaciones[] = $notificacion;
    }
    /**
     * Get the value of notificaciones
     *
     * @return  EventoHistorialNotificacion
     */ 
    public function getNotificaciones()
    {
        return $this->notificaciones;
    }

    /**
     * Set the value of notificaciones
     *
     * @param  EventoHistorialNotificacion  $notificaciones
     *
     * @return  self
     */ 
    public function setNotificaciones(EventoHistorialNotificacion $notificaciones)
    {
        $this->notificaciones = $notificaciones;

        return $this;
    }

    /**
     * Get $uid
     *
     * @return  string
     */ 
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set $uid
     *
     * @param  string  $uid  $uid
     *
     * @return  self
     */ 
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }
}
