<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of MotivoActividadGpm
 *
 * @author nicolas
 */

/**
 * App\Entity\MotivoActividadGpm
 *
 * @ORM\Table(name="motivo_actividadgpm")
 * @ORM\Entity(repositoryClass="App\Repository\MotivoActividadGpmRepository")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class MotivoActividadGpm
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="motivosgpm")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ActividadGpm", mappedBy="motivo", cascade={"persist"})
     */
    protected $actividades;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }
}
