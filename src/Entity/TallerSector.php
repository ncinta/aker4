<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Taller
 *
 * @ORM\Table(name="tallersector")
 * @ORM\Entity(repositoryClass="App\Repository\TallerSectorRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class TallerSector
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;
    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string $rubro
     *
     * @ORM\Column(name="rubro", type="string", length=255, nullable=true)
     */
    private $rubro;

    /**
     * @var string $horario
     *
     * @ORM\Column(name="horario", type="string", length=255, nullable=true)
     */
    private $horario;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajo", mappedBy="tallerSector", cascade={"persist"})
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="sectoresTaller")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Taller", inversedBy="sectores")
     * @ORM\JoinColumn(name="taller_id", referencedColumnName="id")
     */
    protected $taller;

    public function __toString()
    {
        return $this->nombre;
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getTelefono()
    {
        return $this->telefono;
    }

    function getRubro()
    {
        return $this->rubro;
    }

    function getHorario()
    {
        return $this->horario;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function setRubro($rubro)
    {
        $this->rubro = $rubro;
    }

    function setHorario($horario)
    {
        $this->horario = $horario;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
    function getTaller()
    {
        return $this->taller;
    }

    function setTaller($taller)
    {
        $this->taller = $taller;
    }
}
