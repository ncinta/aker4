<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Chip
 *
 * @ORM\Table(name="bitacora")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Bitacora
{

    const EVENTO_INDEFINIDO = 0;
    const EVENTO_USUARIO_LOGIN = 1;
    const EVENTO_USUARIO_CHANGE_PASSWORD = 2;
    const EVENTO_USUARIO_UPDATE = 3;
    const EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA = 5;
    const EVENTO_USUARIO_BAJA = 8;
    const EVENTO_USUARIO_UPDATE_PERMISOS = 9;
    const EVENTO_ORGANIZACION_UPDATE_MODULO = 10;
    const EVENTO_ORGANIZACION_ALTA = 11;
    const EVENTO_ORGANIZACION_UPDATE = 12;
    const EVENTO_ORGANIZACION_BAJA = 13;
    const EVENTO_REFERENCIA_ALTA = 14;
    const EVENTO_REFERENCIA_UPDATE = 15;
    const EVENTO_REFERENCIA_BAJA = 16;
    const EVENTO_CHOFER_ASIGNAR_SERVICIO = 17;
    const EVENTO_CHOFER_RETIRAR_SERVICIO = 18;
    const EVENTO_EV_ALTA= 19;
    const EVENTO_EV_UPDATE = 20;
    const EVENTO_EV_BAJA = 21;
    const EVENTO_CHOFER_TOMAR_SERVICIO = 22;
    const EVENTO_CHOFER_ENTREGAR_SERVICIO = 23;

    private $TipoEventoDescipcion = array(
        self::EVENTO_INDEFINIDO => array('desc' => 'Indefinido', 'nivel' => 0),
        self::EVENTO_USUARIO_LOGIN => array('desc' => 'Logueo de ejecutor', 'nivel' => 0),
        self::EVENTO_USUARIO_CHANGE_PASSWORD => array('desc' => 'Cambio de Clave Usuario', 'nivel' => 0),
        self::EVENTO_USUARIO_UPDATE => array('desc' => 'Cambio de Datos del Usuario', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA => array('desc' => 'Carga de Combustible Automatico', 'nivel' => 0),
        self::EVENTO_USUARIO_BAJA => array('desc' => 'Eliminación de Usuario', 'nivel' => 0),
        self::EVENTO_USUARIO_UPDATE_PERMISOS => array('desc' => 'Edición de Permisos', 'nivel' => 0),
        self::EVENTO_ORGANIZACION_UPDATE_MODULO => array('desc' => 'Edición de Módulo', 'nivel' => 0),
        self::EVENTO_ORGANIZACION_ALTA => array('desc' => 'Alta de Organización', 'nivel' => 0),
        self::EVENTO_ORGANIZACION_UPDATE => array('desc' => 'Edición de Organización', 'nivel' => 0),
        self::EVENTO_ORGANIZACION_BAJA => array('desc' => 'Baja de Organización', 'nivel' => 0),
        self::EVENTO_REFERENCIA_ALTA => array('desc' => 'Alta de Referencia', 'nivel' => 0),
        self::EVENTO_REFERENCIA_UPDATE => array('desc' => 'Edición de Referencia', 'nivel' => 0),
        self::EVENTO_REFERENCIA_BAJA => array('desc' => 'Baja de Referencia', 'nivel' => 0),
        self::EVENTO_CHOFER_ASIGNAR_SERVICIO => array('desc' => 'Asignación de servicio', 'nivel' => 0),
        self::EVENTO_CHOFER_RETIRAR_SERVICIO => array('desc' => 'Retiro de servicio', 'nivel' => 0),
        self::EVENTO_EV_ALTA => array('desc' => 'Alta de evento', 'nivel' => 0),
        self::EVENTO_EV_UPDATE => array('desc' => 'Edición de evento', 'nivel' => 0),
        self::EVENTO_EV_BAJA => array('desc' => 'Baja de evento', 'nivel' => 0),
        self::EVENTO_CHOFER_TOMAR_SERVICIO => array('desc' => 'Toma de Servicio', 'nivel' => 0),
        self::EVENTO_CHOFER_ENTREGAR_SERVICIO => array('desc' => 'Entrega de Servicio', 'nivel' => 0),
    );

    public function getArrayTipoEvento()
    {
        return $this->TipoEventoDescipcion;
    }

    public function getStrTipoEvento()
    {
        if (isset($this->tipo_evento)) {
            return $this->TipoEventoDescipcion[$this->tipo_evento]['desc'];
        } else {
            return '---';
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(name="tipo_evento", type="integer")
     */
    private $tipo_evento;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

     /**
     * @ORM\Column(name="asunto", type="string", length=255, nullable=true)
     */
    private $asunto;


    /**
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;


    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $itinerario;

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * Es el chofer sobre la cual se ejecuta el evento. Claudio me pidio que no genere una tabla nueva. que coloque el chofer aca 
     * @var Chofer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $chofer;

    /**
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Evento", inversedBy="bitacora")
     * @ORM\JoinTable(name="bitacoraEvento")
     */
    private $eventos;


    /**
     * @var Notificacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Notificacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="notificacion_id", referencedColumnName="id",  nullable=true, onDelete="CASCADE"))
     */
    private $notificacion;


    function addEvento($evento)
    {
        $this->eventos[] = $evento;
    }


    public function removeEvento($evento)
    {
        $this->eventos->removeElement($evento);
    }


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        if ($this->estado != null) {
            return $this->TipoEventoDescipcion[$this->tipo_evento];
        } else {
            return $this->TipoEventoDescipcion[0];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getTipoEvento()
    {
        return $this->tipo_evento;
    }

    public function setTipoEvento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function getChofer()
    {
        return $this->chofer;
    }

    public function setChofer($chofer): void
    {
        $this->chofer = $chofer;
    }

    /**
     * Get the value of itinerario
     *
     * @return  Usuario
     */
    public function getItinerario()
    {
        return $this->itinerario;
    }

    /**
     * Set the value of itinerario
     *
     * @param  Usuario  $itinerario
     *
     * @return  self
     */
    public function setItinerario(Usuario $itinerario)
    {
        $this->itinerario = $itinerario;

        return $this;
    }

    /**
     * Get the value of notificacion
     *
     * @return  Notificacion
     */
    public function getNotificacion()
    {
        return $this->notificacion;
    }

    /**
     * Set the value of notificacion
     *
     * @param  Notificacion  $notificacion
     *
     * @return  self
     */
    public function setNotificacion(Notificacion $notificacion)
    {
        $this->notificacion = $notificacion;

        return $this;
    }

    /**
     * Get the value of asunto
     */ 
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set the value of asunto
     *
     * @return  self
     */ 
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
}
