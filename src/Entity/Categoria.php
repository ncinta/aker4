<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Categoria
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Categoria
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="categorias")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referencia", mappedBy="categoria")
     */
    protected $referencias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReferenciaGpm", mappedBy="categoria")
     */
    protected $referenciasGpm;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set organizacion
     *
     * @param App\Entity\Organizacion $propietario
     */
    public function setOrganizacion(\App\Entity\Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get organizacion
     *
     * @return App\Entity\Organizacion 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Add referencias
     *
     * @param App\Entity\Referencia $referencias
     */
    public function addReferencia(Referencia $referencias)
    {
        $this->referencias[] = $referencias;
    }

    /**
     * Get referencias
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getReferencias()
    {
        return $this->referencias;
    }

    public function __toString()
    {

        return $this->nombre;
    }
    function getReferenciasGpm()
    {
        return $this->referenciasGpm;
    }

    function setReferenciasGpm($referenciasGpm)
    {
        $this->referenciasGpm = $referenciasGpm;
    }
}
