<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="hora_taller")
 * @ORM\Entity(repositoryClass="App\Repository\HoraTallerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class HoraTaller
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="horasTaller")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoHoraTaller", mappedBy="horaTaller", cascade ={"persist"})
     */
    private $ordenesTrabajo;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }


    function setId($id)
    {
        $this->id = $id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    public function addOrdenTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo[] = $ordenesTrabajo;
    }

    public function removeOrdenTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo->removeElement($ordenesTrabajo);
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }
}
