<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Description of BitacoraCentroCosto
 *
 * @author nicolas
 */

/**
 * App\Entity\BitacoraCentroCosto
 *
 * @ORM\Table(name="bitacoracentrocosto")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraCentroCostoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BitacoraCentroCosto
{
    const EVENTO_INDEFINIDO = 0;
    const EVENTO_CENTROCOSTO_ASIGNAR_SERVICIO = 1;
    const EVENTO_CENTROCOSTO_RETIRAR_SERVICIO = 2;

    private $TipoEventoDescipcion = array(
        self::EVENTO_INDEFINIDO => array('desc' => 'Indefinido', 'nivel' => 0),
        self::EVENTO_CENTROCOSTO_ASIGNAR_SERVICIO => array('desc' => 'Asignación de servicio', 'nivel' => 0),
        self::EVENTO_CENTROCOSTO_RETIRAR_SERVICIO => array('desc' => 'Retiro de servicio', 'nivel' => 0),

    );

    public function getArrayTipoEvento()
    {
        return $this->TipoEventoDescipcion;
    }

    public function getStrTipoEvento()
    {
        if (isset($this->tipo_evento)) {
            return $this->TipoEventoDescipcion[$this->tipo_evento]['desc'];
        } else {
            return '---';
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="tipo_evento", type="integer")
     */
    private $tipo_evento;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var CentroCosto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CentroCosto", inversedBy="bitacoraCentroCosto")     
     * @ORM\JoinColumn(name="centrocosto_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $centrocosto;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
    }

    public function __toString()
    {
        if ($this->estado != null) {
            return $this->TipoEventoDescipcion[$this->tipo_evento];
        } else {
            return $this->TipoEventoDescipcion[0];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getTipoEvento()
    {
        return $this->tipo_evento;
    }

    public function setTipoEvento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getTipoEventoDescipcion()
    {
        return $this->TipoEventoDescipcion;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getTipo_evento()
    {
        return $this->tipo_evento;
    }

    function getCentrocosto()
    {
        return $this->centrocosto;
    }

    function setTipoEventoDescipcion($TipoEventoDescipcion)
    {
        $this->TipoEventoDescipcion = $TipoEventoDescipcion;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setTipo_evento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    function setCentrocosto($centrocosto)
    {
        $this->centrocosto = $centrocosto;
    }
}
