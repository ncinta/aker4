<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\OrdenTrabajoProducto
 *
 * @ORM\Table(name="ordentrabajo_externo")
 * @ORM\Entity(repositoryClass="App\Repository\OrdenTrabajoExternoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrdenTrabajoExterno
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrdenTrabajo", inversedBy="externos")
     * @ORM\JoinColumn(name="ordentrabajo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $ordenesTrabajo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TallerExterno", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="tallerexterno_id", referencedColumnName="id",nullable=true)
     */
    protected $tallerExterno;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="ordenesTrabajoExterno")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;

    function getId()
    {
        return $this->id;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getCantidad()
    {
        return $this->cantidad;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function getOrdenesTrabajo()
    {
        return $this->ordenesTrabajo;
    }

    function getTallerExterno()
    {
        return $this->tallerExterno;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function setOrdenesTrabajo($ordenesTrabajo)
    {
        $this->ordenesTrabajo = $ordenesTrabajo;
    }

    function setTallerExterno($tallerExterno)
    {
        $this->tallerExterno = $tallerExterno;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }
}
