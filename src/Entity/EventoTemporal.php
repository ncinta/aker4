<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Index;
use App\Entity\Servicio as Servicio;
use App\Entity\EventoHistorial as EventoHistorial;
use App\Entity\Usuario as Usuario;
use App\Entity\Organizacion as Organizacion;

/**
 *
 * @ORM\Table(name="eventotemporal", indexes={@Index(name="fecha_idy", columns={"fecha"})})
 * @ORM\Entity(repositoryClass="App\Repository\EventoTemporalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EventoTemporal
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $titulo
     *
     * @ORM\Column(name="titulo", type="string")
     */
    private $titulo;


    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;


    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;


    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="eventosTemporal")     
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $servicio;


    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="eventosTemporal")     
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $usuario;

    /**
     * @var EventoHistorico
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\EventoHistorial", inversedBy="eventoTemporal")
     * @ORM\JoinColumn(name="eventohistorico_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $eventoHistorial;

    /**
     * @var HistoricoItinerario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\HistoricoItinerario", inversedBy="eventoTemporal")
     * @ORM\JoinColumn(name="historicoitinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $historicoItinerario;

    /**
     * @var Evento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Evento", inversedBy="eventoTemporal")     
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $evento;

    /**
     * @var Itineario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Itinerario", inversedBy="eventoTemporal")     
     * @ORM\JoinColumn(name="itinerario_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $itinerario;

    /**
     * @ORM\Column(name="clase_evento", type="integer", nullable=true)
     */
    private $clase; // 1= evento clasico, 2=reply, 3=itinerario,

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="eventosTemporal")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var string
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    public function __toString()
    {
        return $this->titulo;
    }


    /**
     * Get es la organizacion sobre la cual se ejecuta el evento.
     *
     * @return  Organizacion
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }


    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }

    /**
     * Get the value of EventoHistorial
     *
     * @return  EventoHistorial
     */
    public function getEventoHistorial()
    {
        return $this->eventoHistorial;
    }

    /**
     * Set the value of EventoHistorico
     *
     * @param  EventoHistorial  $eventoHistorial
     *
     * @return  self
     */
    public function setEventoHistorial($eventoHistorial)
    {
        $this->eventoHistorial = $eventoHistorial;

        return $this;
    }


    /**
     * Get the value of servicio
     *
     * @return  Servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set the value of servicio
     *
     * @param  Servicio  $servicio
     *
     * @return  self
     */
    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get the value of registrar
     */
    public function getRegistrar()
    {
        return $this->registrar;
    }

    /**
     * Set $id
     *
     * @param  integer  $id  $id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }


    /**
     * Get the value of fecha
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get $titulo
     *
     * @return  string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set $titulo
     *
     * @param  string  $titulo  $titulo
     *
     * @return  self
     */
    public function setTitulo(string $titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of usuarios
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set the value of usuarios
     *
     * @return  self
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }



    /**
     * Get the value of evento
     *
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set the value of evento
     *
     *
     * @return  self
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get $id
     *
     * @return  integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of data
     *
     * @return  string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @param  string  $data
     *
     * @return  self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }


    /**
     * Get the value of historicoItinerario
     *
     * @return  HistoricoItinerario
     */
    public function getHistoricoItinerario()
    {
        return $this->historicoItinerario;
    }

    /**
     * Set the value of historicoItinerario
     *
     * @param  HistoricoItinerario  $historicoItinerario
     *
     * @return  self
     */
    public function setHistoricoItinerario($historicoItinerario)
    {
        $this->historicoItinerario = $historicoItinerario;

        return $this;
    }

    /**
     * Get the value of clase
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Set the value of clase
     *
     * @return  self
     */
    public function setClase($clase)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get the value of itinerario
     *
     * @return  Itineario
     */ 
    public function getItinerario()
    {
        return $this->itinerario;
    }

    /**
     * Set the value of itinerario
     *
     * @param  Itineario  $itinerario
     *
     * @return  self
     */ 
    public function setItinerario($itinerario)
    {
        $this->itinerario = $itinerario;

        return $this;
    }
}
