<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Chip
 *
 * @ORM\Table(name="chip")
 * @ORM\Entity(repositoryClass="App\Repository\ChipRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Chip
{

    const ESTADO_INDEFINIDO = 0;
    const ESTADO_DISPONIBLE = 1;
    const ESTADO_INSTALADO = 2;
    const ESTADO_PERDIDO = 3;
    const ESTADO_BAJA = 4;

    private $strEstado = array(
        'Indefinido' => self::ESTADO_INDEFINIDO,
        'Disponible' => self::ESTADO_DISPONIBLE,
        'Instalado' => self::ESTADO_INSTALADO,
        'Perdido' => self::ESTADO_PERDIDO,
        'Baja' => self::ESTADO_BAJA
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        return array_search($this->estado, $this->strEstado);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $imei
     *
     * @ORM\Column(name="imei", type="string", length=255, nullable=true )
     * @Assert\Length(min = 6, max = 25)
     * @Assert\Regex(
     *     pattern="/^[F0-9]+$/",
     *     message="Contiene caracteres inválidos. Deben ser todos números."
     * )
     */
    private $imei;

    /**
     * @var string $numeroTelefono
     *
     * @ORM\Column(name="numeroTelefono", type="string", length=255)
     */
    private $numeroTelefono;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="estado", type="integer")
     * 
     */
    private $estado;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestadora", inversedBy="chips")
     * @ORM\JoinColumn(name="prestadora_id", referencedColumnName="id")
     */
    protected $prestadora;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipo", inversedBy="chips")
     * @ORM\JoinColumn(name="equipo_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $equipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="chips_propietario")
     * @ORM\JoinColumn(name="propietario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $propietario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="chips")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imei
     *
     * @param string $imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    }

    /**
     * Get imei
     *
     * @return string 
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set numeroTelefono
     *
     * @param string $numeroTelefono
     */
    public function setNumeroTelefono($numeroTelefono)
    {
        $this->numeroTelefono = $numeroTelefono;
    }

    /**
     * Get numeroTelefono
     *
     * @return string 
     */
    public function getNumeroTelefono()
    {
        return $this->numeroTelefono;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Set prestadora
     *
     * @param App\Entity\Prestadora $prestadora
     */
    public function setPrestadora(\App\Entity\Prestadora $prestadora)
    {
        $this->prestadora = $prestadora;
    }

    /**
     * Get prestadora
     *
     * @return App\Entity\Prestadora 
     */
    public function getPrestadora()
    {
        return $this->prestadora;
    }

    /**
     * Set equipo
     *
     * @param App\Entity\Equipo $equipo
     */
    public function setEquipo($equipo)
    {
        $this->equipo = $equipo;
    }

    /**
     * Get equipo
     *
     * @return App\Entity\Equipo 
     */
    public function getEquipo()
    {
        return $this->equipo;
    }

    /**
     * Set propietario
     *
     * @param App\Entity\Organizacion $propietar
     */
    public function setPropietario(\App\Entity\Organizacion $propietario)
    {
        $this->propietario = $propietario;
    }

    /**
     * Get propietario
     *
     * @return App\Entity\Organizacion 
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * Set organizacion
     *
     * @param App\Entity\Organizacion $propietario
     */
    public function setOrganizacion(\App\Entity\Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get organizacion
     *
     * @return App\Entity\Organizacion 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function __toString()
    {
        return (string) $this->imei;
    }
}
