<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\Prestacion
 *
 * @ORM\Table(name="paradaprestacion")
 * @ORM\Entity(repositoryClass="App\Repository\ParadaPrestacionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ParadaPrestacion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var datetime $inicio_detencion
     *
     * @ORM\Column(name="inicio_detencion", type="datetime", nullable=true)
     */
    private $inicioDetencion;

    /**
     * @var datetime $fin_detencion
     *
     * @ORM\Column(name="fin_detencion", type="datetime", nullable=true)
     */
    private $finDetencion;

    /**
     * @ORM\Column(name="horas_parada", type="float", nullable=true)
     */
    private $horasParada;

    /**
     * @ORM\Column(name="motivo", type="string", nullable=true)
     */
    private $motivo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestacion", inversedBy="paradas")
     * @ORM\JoinColumn(name="prestacion_id", referencedColumnName="id")
     */
    protected $prestacion;

    public function __construct()
    {
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getInicioDetencion()
    {
        return $this->inicioDetencion;
    }

    function getFinDetencion()
    {
        return $this->finDetencion;
    }

    function getHorasParada()
    {
        return $this->horasParada;
    }

    function getMotivo()
    {
        return $this->motivo;
    }

    function getPrestacion()
    {
        return $this->prestacion;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setInicioDetencion($inicioDetencion)
    {
        $this->inicioDetencion = $inicioDetencion;
    }

    function setFinDetencion($finDetencion)
    {
        $this->finDetencion = $finDetencion;
    }

    function setHorasParada($horasParada)
    {
        $this->horasParada = $horasParada;
    }

    function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    }

    function setPrestacion($prestacion)
    {
        $this->prestacion = $prestacion;
    }
}
