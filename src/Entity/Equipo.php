<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Equipo
 *
 * @ORM\Table(name="equipo")
 * @ORM\Entity(repositoryClass="App\Repository\EquipoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Equipo
{

    const ESTADO_INDEFINIDO = 0;
    const ESTADO_DISPONIBLE = 1;
    const ESTADO_ENDEPOSITO = 2;
    const ESTADO_INSTALADO = 3;

    private $strEstado = array(
        self::ESTADO_INDEFINIDO => 'Indefinido',
        self::ESTADO_DISPONIBLE => 'Disponible',
        self::ESTADO_ENDEPOSITO => 'En deposito',
        self::ESTADO_INSTALADO => 'Instalado'
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        if ($this->estado != null) {
            return $this->strEstado[$this->estado];
        } else {
            return $this->strEstado[0];
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $numeroSerie
     *
     * @ORM\Column(name="numeroSerie", type="string", length=255, nullable=true)
     */
    private $numeroSerie;

    /**
     * @var string $mdmid
     *
     * @ORM\Column(name="mdmid", type="string", length=255, unique=true)
     */
    private $mdmid;

    /**
     * @var string $imei
     *
     * @ORM\Column(name="imei", type="string", length=255, nullable=true)
     * @Assert\Length(min = 6, max = 19)
     * @Assert\Regex(
     *     pattern="/^[F0-9]+$/",
     *     message="Contiene caracteres inválidos. Deben ser todos números."
     * )
     */
    private $imei;

    /**
     * @var string $versionFirmware
     *
     * @ORM\Column(name="versionFirmware", type="string", length=255, nullable=true)
     */
    private $versionFirmware;

    /**
     * @var integer $estado
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="equiposServicio")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    private $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="equiposPropios")
     * @ORM\JoinColumn(name="propietario_id", referencedColumnName="id")
     */
    private $propietario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proveedor", inversedBy="equipos")
     * @ORM\JoinColumn(name="proveedor_id", referencedColumnName="id", nullable=true)
     */
    private $proveedor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo", inversedBy="equipos")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id")
     */
    private $modelo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chip", mappedBy="equipo", cascade={"persist"})
     */
    private $chips;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistorialServicio", mappedBy="equipo")
     */
    private $historialServicios;

    /**
     * @OneToOne(targetEntity="App\Entity\Servicio", mappedBy="equipo")
     */
    private $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BitacoraEquipo", mappedBy="equipo")
     */
    protected $bitacoraEquipo;

    public function __construct()
    {
        $this->chips = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historialServicios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    private $chip;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set numeroSerie
     *
     * @param string $numeroSerie
     */
    public function setNumeroSerie($numeroSerie)
    {
        $this->numeroSerie = $numeroSerie;
    }

    /**
     * Get numeroSerie
     *
     * @return string 
     */
    public function getNumeroSerie()
    {
        return $this->numeroSerie;
    }

    /**
     * Set mdmid
     *
     * @param string $mdmid
     */
    public function setMdmid($mdmid)
    {
        $this->mdmid = $mdmid;
    }

    /**
     * Get mdmid
     *
     * @return string 
     */
    public function getMdmid()
    {
        return $this->mdmid;
    }

    /**
     * Set imei
     *
     * @param string $imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    }

    /**
     * Get imei
     *
     * @return string 
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set versionFirmware
     *
     * @param string $versionFirmware
     */
    public function setVersionFirmware($versionFirmware)
    {
        $this->versionFirmware = $versionFirmware;
    }

    /**
     * Get versionFirmware
     *
     * @return string 
     */
    public function getVersionFirmware()
    {
        return $this->versionFirmware;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Set organizacion
     *
     * @param App\Entity\Organizacion $organizacion
     */
    public function setOrganizacion(\App\Entity\Organizacion $organizacion)
    {
        $this->organizacion = $organizacion;
    }

    /**
     * Get organizacion
     *
     * @return \App\Entity\Organizacion 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set propietario
     *
     * @param App\Entity\Organizacion $propietario
     */
    public function setPropietario(\App\Entity\Organizacion $propietario)
    {
        $this->propietario = $propietario;
    }

    /**
     * Get propietario
     *
     * @return App\Entity\Organizacion 
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * Set proveedor
     *
     * @param App\Entity\Proveedor $proveedor
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;
    }

    /**
     * Get proveedor
     *
     * @return App\Entity\Proveedor 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set modelo
     *
     * @param App\Entity\Modelo $modelo
     */
    public function setModelo(\App\Entity\Modelo $modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * Get modelo
     *
     * @return App\Entity\Modelo 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Add chips
     *
     * @param App\Entity\Chip $chips
     */
    public function addChip(\App\Entity\Chip $chips)
    {
        $this->chips[] = $chips;
    }

    /**
     * Get chips
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChips()
    {
        return $this->chips;
    }

    public function setChips(ArrayCollection $chips)
    {
        $this->chips = $chips;
    }

    /**
     * Add historialServicios
     *
     * @param App\Entity\HistorialServicio $historialServicios
     */
    public function addHistorialServicio(\App\Entity\HistorialServicio $historialServicios)
    {
        $this->historialServicios[] = $historialServicios;
    }

    /**
     * Get historialServicios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHistorialServicios()
    {
        return $this->historialServicios;
    }

    /**
     * Set servicio
     *
     * @param App\Entity\Servicio $servicio
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * Get servicio
     *
     * @return App\Entity\Servicio 
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    public function __toString()
    {
        return $this->mdmid;
    }

    public function setChip($chip)
    {
        $this->addChip($chip);
    }

    public function getchip()
    {
        return $this->getChips();
    }

    function getVars()
    {
        $vars = get_object_vars($this);
        unset($vars['strEstado']);
        return $vars;
    }

    public function data2array()
    {
        return array(
            'id' => $this->id,
            'mdmid' => $this->mdmid,
            'serie' => $this->numeroSerie,
            'imei' => $this->imei,
            'modelo' => $this->getModelo()->getNombre()
        );
    }

    public function getBitacoraEquipo()
    {
        return $this->bitacoraEquipo;
    }

    public function setBitacoraEquipo($bitacoraEquipo)
    {
        $this->bitacoraEquipo = $bitacoraEquipo;
        return $this;
    }
}
