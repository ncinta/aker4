<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entidad de capturadores. La idea es modelar los datos de los capturadores del 
 * sistema que permitiran saber por donde esta entrando las tramas.
 * App\Entity\Aplicacion
 *
 * @ORM\Table(name="capturador")
 * @ORM\Entity(repositoryClass="App\Repository\CapturadorRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Capturador
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Nombre del capturador, ser lo mas descriptivo posible.
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * IP del capturador donde se encuentra corriendo. Puede ser ip publica o 
     * de la VPN
     * @var string $ip
     * @Assert\Ip(
     *    version = 4
     * )
     * @ORM\Column(name="ip", type="string", nullable=false)
     */
    private $ip;

    /**
     * Puerto donde esta recibiendo las tramas de los equipos
     * @var string $puerto_trama
     *
     * @ORM\Column(name="puerto_trama", type="integer", nullable=false)
     */
    private $puerto_trama;

    /**
     * IP:Puerto donde esta configurado el programador remoto que se debe utilizar
     * para este capturador.
     * @var string $programador
     *
     * @ORM\Column(name="programador", type="string", nullable=true)
     */
    private $programador;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    public function __toString()
    {
        return (string) $this->nombre;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getPuertoTrama()
    {
        return $this->puerto_trama;
    }

    public function getProgramador()
    {
        return $this->programador;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function setPuertoTrama($puerto_trama)
    {
        $this->puerto_trama = $puerto_trama;
        return $this;
    }

    public function setProgramador($programador)
    {
        $this->programador = $programador;
        return $this;
    }
}
