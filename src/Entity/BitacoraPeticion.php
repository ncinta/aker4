<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="peticion_bitacora")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraPeticionesRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class BitacoraPeticion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime") 
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime") 
     */
    private $updated_at;

    /**
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Peticion", inversedBy="bitacora")
     * @ORM\JoinColumn(name="peticion_id", referencedColumnName="id")
     */
    protected $peticion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacoraPeticiones")
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id")
     */
    protected $ejecutor;

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getData()
    {
        return $this->data;
    }

    function getEjecutor()
    {
        return $this->ejecutor;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    function getPeticion()
    {
        return $this->peticion;
    }

    function setPeticion($peticion)
    {
        $this->peticion = $peticion;
    }
}
