<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Mecanico
 *
 * @ORM\Table(name="tareaot_mecanico")
 * @ORM\Entity(repositoryClass="App\Repository\TareaOtMecanicoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TareaOtMecanico
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="horas", type="float", nullable=true)
     */
    private $horas;

    /**
     * @var integer cantidad
     *
     * @ORM\Column(name="costo", type="float", nullable=true)
     */
    private $costo;

    /**
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TareaOt", inversedBy="mecanicos", cascade={"persist"})
     * @ORM\JoinColumn(name="tareaot_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $tarea;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mecanico", inversedBy="tareasMecanico")
     * @ORM\JoinColumn(name="mecanico_id", referencedColumnName="id",nullable=true)
     */
    protected $mecanico;

    function getId()
    {
        return $this->id;
    }

    function getHoras()
    {
        return $this->horas;
    }

    function getMantenimiento()
    {
        return $this->mantenimiento;
    }

    function getMecanico()
    {
        return $this->mecanico;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setHoras($horas)
    {
        $this->horas = $horas;
    }

    function setMecanico($mecanico)
    {
        $this->mecanico = $mecanico;
    }

    function getCosto()
    {
        return $this->costo;
    }

    function setCosto($costo)
    {
        $this->costo = $costo;
    }

    public function addTareaOt($tareaot)
    {
        $this->tarea[] = $tareaot;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }
    function getTarea()
    {
        return $this->tarea;
    }

    function setTarea($tarea)
    {
        $this->tarea = $tarea;
    }
}
