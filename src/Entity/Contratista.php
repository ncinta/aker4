<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Transporte
 *
 * @ORM\Table(name="contratista")
 * @ORM\Entity(repositoryClass="App\Repository\ContratistaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Contratista
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="contratistas")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestacion", mappedBy="contratista", cascade={"persist"})
     */
    protected $prestaciones;

    /**
     * @ORM\OneToMany(targetEntity="Proyecto", mappedBy="contratista", cascade={"persist"})
     */
    protected $proyectos;

    /**
     * @ORM\OneToMany(targetEntity="ActividadGpm", mappedBy="contratista", cascade={"persist"})
     */
    protected $actividades;

    /**
     * @ORM\OneToMany(targetEntity="Persona", mappedBy="contratista")
     */
    protected $personas;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Add Personal
     *
     * @param App\Entity\Personal $personal
     */
    public function addPersonal(Personal $personal)
    {
        $this->personal[] = $personal;
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function getPrestaciones()
    {
        return $this->prestaciones;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getProyectos()
    {
        return $this->proyectos;
    }

    function setProyectos($proyectos)
    {
        $this->proyectos = $proyectos;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }

    function getPersonas()
    {
        return $this->personas;
    }

    function setPersonas($personas)
    {
        $this->personas = $personas;
    }
}
