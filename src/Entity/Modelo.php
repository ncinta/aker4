<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Modelo
 *
 * @ORM\Table(name="modelo")
 * @ORM\Entity
 */
class Modelo
{

    const TPROG_CARFINDER = 1;
    const TPROG_ENFORA = 2;
    const TPROG_TR600 = 3;
    const TPROG_TR203 = 4;
    const TPROG_MAXTRACK = 5;
    const TPROG_CARBOLT = 6;
    const TPROG_RINHO = 7;
    const TPROG_MS = 8;
    const TPROG_QUECKLINK = 9; //los puse porque en la bd estan y aca no. Entonces en el list tira error
    const TPROG_SUNTECH = 10;
    const TPROG_LANDHER = 11;
    const TPROG_TELTONIKA = 12;
    const TPROG_CONCOX = 13;

    private $arrayTipoProg = array(
        self::TPROG_CARFINDER => 'CarFinder',
        self::TPROG_ENFORA => 'Enfora',
        self::TPROG_TR600 => 'GsTrack TR-600',
        self::TPROG_TR203 => 'GsTrack TR-203',
        self::TPROG_MAXTRACK => 'MaxTrack',
        self::TPROG_CARBOLT => 'CarBolt',
        self::TPROG_RINHO => 'Rinho',
        self::TPROG_MS => 'MS',
        self::TPROG_QUECKLINK => 'Quecklink',
        self::TPROG_SUNTECH => 'Suntech',
        self::TPROG_LANDHER => 'Landher',
        self::TPROG_TELTONIKA => 'Teltonica',
        self::TPROG_CONCOX => 'Concox'
    );

    public function getArrayTipoProgramacion()
    {
        return $this->arrayTipoProg;
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $fabricante
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;

    /**
     * @var integer $cantidadGeocercas
     *
     * @ORM\Column(name="cantidadGeocercas", type="integer", nullable=true)
     */
    private $cantidadGeocercas;

    /**
     * @ORM\Column(name="tipo_programacion", type="integer", nullable=true)
     */
    private $tipoProgramacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equipo", mappedBy="modelo")
     */
    protected $equipos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Variable", mappedBy="modelo", cascade={"persist"})
     */
    protected $variables;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModeloMotivoReporte", mappedBy="modelo", cascade={"persist"})
     */
    protected $motivosReporte;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ModeloSensor", mappedBy="modelo", cascade={"persist"})
     */
    protected $sensores;

    /**
     * @ORM\Column(name="accesorios", type="array", nullable=true)
     */
    private $accesorios;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;
    }

    /**
     * Get fabricante
     *
     * @return string 
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set cantidadGeocercas
     *
     * @param integer $cantidadGeocercas
     */
    public function setCantidadGeocercas($cantidadGeocercas)
    {
        $this->cantidadGeocercas = $cantidadGeocercas;
    }

    /**
     * Get cantidadGeocercas
     *
     * @return integer 
     */
    public function getCantidadGeocercas()
    {
        return $this->cantidadGeocercas;
    }

    public function __construct()
    {
        $this->equipos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->motivosReporte = new \Doctrine\Common\Collections\ArrayCollection();
        $this->variables = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add equipos
     *
     * @param App\Entity\Equipo $equipos
     */
    public function addEquipo(\App\Entity\Equipo $equipos)
    {
        $this->equipos[] = $equipos;
    }

    /**
     * Get equipos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEquipos()
    {
        return $this->equipos;
    }

    /**
     * Add variables
     *
     * @param App\Entity\Variable $variables
     */
    public function addVariable(\App\Entity\Variable $variables)
    {
        $this->variables[] = $variables;
    }

    /**
     * Get variables
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /* public function addVariable(\App\Entity\Variable $variable) {
      $this->variables->add($variable);
      return $this;
      } */

    public function removeVariable(\App\Entity\Variable $variable)
    {
        $this->variables->removeElement($variable);
        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getMotivosReporte()
    {
        return $this->motivosReporte;
    }

    public function setMotivosReporte($motivosReporte)
    {
        $this->motivosReporte = $motivosReporte;
    }

    public function getTipoProgramacion()
    {
        return $this->tipoProgramacion;
    }

    public function setTipoProgramacion($tipoProgramacion)
    {
        $this->tipoProgramacion = $tipoProgramacion;
        return $this;
    }

    public function getSensores()
    {
        return $this->sensores;
    }

    public function setSensores($sensores)
    {
        $this->sensores = $sensores;
        return $this;
    }

    public function getStrTtipoProgramacion()
    {
        if ($this->tipoProgramacion != null) {
            return $this->arrayTipoProg[$this->tipoProgramacion];
        } else {
            return null;
        }
    }

    public function getCorteMotor()
    {
        return isset($this->accesorios['cortemotor']) ? $this->accesorios['cortemotor'] : false;
    }

    public function setCorteMotor($corte)
    {
        $this->accesorios['cortemotor'] = $corte;
    }

    public function getHorometro()
    {
        return isset($this->accesorios['horometro']) ? $this->accesorios['horometro'] : false;
    }

    public function setHorometro($horometro)
    {
        $this->accesorios['horometro'] = $horometro;
    }

    public function getBotonPanico()
    {
        return isset($this->accesorios['botonpanico']) ? $this->accesorios['botonpanico'] : false;
    }

    public function setBotonPanico($panico)
    {
        $this->accesorios['botonpanico'] = $panico;
    }

    public function getMedidorCombustible()
    {
        return isset($this->accesorios['combustible']) ? $this->accesorios['combustible'] : false;
    }

    public function setMedidorCombustible($medidor)
    {
        $this->accesorios['combustible'] = $medidor;
    }

    public function getEntradasDigitales()
    {
        return isset($this->accesorios['inputs']) ? $this->accesorios['inputs'] : false;
    }

    public function setEntradasDigitales($inputs)
    {
        $this->accesorios['inputs'] = $inputs;
    }

    public function getCorteCerrojo()
    {
        return isset($this->accesorios['cortecerrojo']) ? $this->accesorios['cortecerrojo'] : false;
    }

    public function setCorteCerrojo($corte)
    {
        $this->accesorios['cortecerrojo'] = $corte;
    }

    public function getCanbus()
    {
        return isset($this->accesorios['canbus']) ? $this->accesorios['canbus'] : false;
    }

    public function setCanbus($canbus)
    {
        $this->accesorios['canbus'] = $canbus;
    }

    public function getAccesorios()
    {
        return $this->accesorios;
    }
}
