<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Chip
 *
 * @ORM\Table(name="bitacoraservicio")
 * @ORM\Entity(repositoryClass="App\Repository\BitacoraServicioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BitacoraServicio
{

    const EVENTO_INDEFINIDO = 0;
    const EVENTO_COMBUSTIBLE_CREATE_MANUAL = 1;
    const EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA = 2;
    const EVENTO_COMBUSTIBLE_UPDATE = 3;
    const EVENTO_COMBUSTIBLE_DELETE = 4;
    const EVENTO_SERVICIO_NEW = 5;
    const EVENTO_SERVICIO_BAJA = 6;
    const EVENTO_SERVICIO_ELIMINACION = 7;
    const EVENTO_SERVICIO_ASIGNAR_EQUIPO = 8;
    const EVENTO_SERVICIO_RETIRAR_EQUIPO = 9;
    const EVENTO_SERVICIO_UPDATE = 10;
    const EVENTO_SERVICIO_PRG = 11;
    const EVENTO_SERVICIO_CORTEMOTOR = 12;
    const EVENTO_SERVICIO_CERROJO = 13;
    const EVENTO_SERVICIO_SENSOR_NEW = 14;
    const EVENTO_SERVICIO_SENSOR_UPDATE = 15;
    const EVENTO_SERVICIO_SENSOR_DELETE = 16;
    const EVENTO_SERVICIO_INPUT_NEW = 17;
    const EVENTO_SERVICIO_INPUT_UPDATE = 18;
    const EVENTO_SERVICIO_INPUT_DELETE = 19;
    const EVENTO_MANTENIMIENTO_REGISTRO = 20;
    const EVENTO_CAMBIO_ODOMETRO = 21;
    const EVENTO_SERVICIO_ASIGNAR_USUARIO = 22;
    const EVENTO_SERVICIO_RETIRAR_USUARIO = 23;
    const EVENTO_CAMBIO_HOROMETRO = 24;
    const EVENTO_SERVICIO_QUERYVERSION = 25;
    const EVENTO_COMBUSTIBLE_CREATE_APP = 26;
    const EVENTO_SERVICIO_CREATE_TAREAMANT = 27;
    const EVENTO_SERVICIO_DELETE_TAREAMANT = 28;
    const EVENTO_SERVICIO_COMPLETE_TAREAMANT = 29;
    const EVENTO_EVENTO_ENABLED = 30;
    const EVENTO_EVENTO_DISABLED = 31;
    const EVENTO_ORDENTRABAJO_NEW = 32;
    const EVENTO_ORDENTRABAJO_EDIT = 33;
    const EVENTO_ORDENTRABAJO_CLOSE = 34;
    const EVENTO_ORDENTRABAJO_DELETE = 35;

    private $TipoEventoDescipcion = array(
        self::EVENTO_INDEFINIDO => array('desc' => 'Indefinido', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_CREATE_MANUAL => array('desc' => 'Carga de Combustible Manual', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_CREATE_AUTOMATICA => array('desc' => 'Carga de Combustible Automatico', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_UPDATE => array('desc' => 'Edicion carga de combustible', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_DELETE => array('desc' => 'Eliminación carga de combustible', 'nivel' => 0),
        self::EVENTO_SERVICIO_NEW => array('desc' => 'Alta', 'nivel' => 0),
        self::EVENTO_SERVICIO_UPDATE => array('desc' => 'Edición', 'nivel' => 0),
        self::EVENTO_SERVICIO_BAJA => array('desc' => 'Baja', 'nivel' => 0),
        self::EVENTO_SERVICIO_ELIMINACION => array('desc' => 'Eliminación', 'nivel' => 0),
        self::EVENTO_SERVICIO_ASIGNAR_EQUIPO => array('desc' => 'Asignación de equipo', 'nivel' => 0),
        self::EVENTO_SERVICIO_RETIRAR_EQUIPO => array('desc' => 'Retiro de equipo', 'nivel' => 0),
        self::EVENTO_SERVICIO_PRG => array('desc' => 'Programación', 'nivel' => 0),
        self::EVENTO_SERVICIO_CORTEMOTOR => array('desc' => 'Corte Motor', 'nivel' => 0),
        self::EVENTO_SERVICIO_CERROJO => array('desc' => 'Cerrojo', 'nivel' => 0),
        self::EVENTO_SERVICIO_SENSOR_NEW => array('desc' => 'Alta de Sensor', 'nivel' => 0),
        self::EVENTO_SERVICIO_SENSOR_UPDATE => array('desc' => 'Actualizacion de Sensor', 'nivel' => 0),
        self::EVENTO_SERVICIO_SENSOR_DELETE => array('desc' => 'Eliminación de Sensor', 'nivel' => 0),
        self::EVENTO_SERVICIO_INPUT_NEW => array('desc' => 'Alta de Input', 'nivel' => 0),
        self::EVENTO_SERVICIO_INPUT_UPDATE => array('desc' => 'Actualizacion de Input', 'nivel' => 0),
        self::EVENTO_SERVICIO_INPUT_DELETE => array('desc' => 'Eliminación de Input', 'nivel' => 0),
        self::EVENTO_MANTENIMIENTO_REGISTRO => array('desc' => 'Registro de Tarea Mantenimiento', 'nivel' => 0),
        self::EVENTO_CAMBIO_ODOMETRO => array('desc' => 'Cambio Odómetro', 'nivel' => 0),
        self::EVENTO_SERVICIO_ASIGNAR_USUARIO => array('desc' => 'Asignación de usuario', 'nivel' => 0),
        self::EVENTO_SERVICIO_RETIRAR_USUARIO => array('desc' => 'Retiro de usuario', 'nivel' => 0),
        self::EVENTO_CAMBIO_HOROMETRO => array('desc' => 'Cambio Horómetro', 'nivel' => 0),
        self::EVENTO_SERVICIO_QUERYVERSION => array('desc' => 'Solicitud Versión', 'nivel' => 0),
        self::EVENTO_COMBUSTIBLE_CREATE_APP => array('desc' => 'Carga de Combustible por App', 'nivel' => 0),
        self::EVENTO_SERVICIO_CREATE_TAREAMANT => array('desc' => 'Alta de tarea de mantenimiento', 'nivel' => 0),
        self::EVENTO_SERVICIO_DELETE_TAREAMANT => array('desc' => 'Eliminación de tarea de mantenimiento', 'nivel' => 0),
        self::EVENTO_SERVICIO_COMPLETE_TAREAMANT => array('desc' => 'Realización de tarea de mantenimiento', 'nivel' => 0),
        self::EVENTO_EVENTO_ENABLED => array('desc' => 'Evento Activado', 'nivel' => 0),
        self::EVENTO_EVENTO_DISABLED => array('desc' => 'Evento Desactivado', 'nivel' => 0),
        self::EVENTO_ORDENTRABAJO_NEW => array('desc' => 'Nueva orden de trabajo', 'nivel' => 0),
        self::EVENTO_ORDENTRABAJO_EDIT => array('desc' => 'Cambio en orden de trabajo', 'nivel' => 0),
        self::EVENTO_ORDENTRABAJO_CLOSE => array('desc' => 'Orden de trabajo cerrada', 'nivel' => 0),
        self::EVENTO_ORDENTRABAJO_DELETE => array('desc' => 'Orden de trabajo eliminada', 'nivel' => 0),
    );

    public function getArrayTipoEvento()
    {
        return $this->TipoEventoDescipcion;
    }

    public function getStrTipoEvento()
    {
        if (isset($this->tipo_evento)) {
            return $this->TipoEventoDescipcion[$this->tipo_evento]['desc'];
        } else {
            return '---';
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="tipo_evento", type="integer")
     */
    private $tipo_evento;

    /**
     * @var json
     * @ORM\Column(name="data", type="array", nullable=true)
     */
    protected $data;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $ejecutor;
    
    /**
     * @var Chofer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Chofer", inversedBy="bitacoraServicio")     
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", nullable=true, onDelete="CASCADE"))
     */
    private $chofer;

    /**
     * Es la organizacion sobre la cual se ejecuta el evento. 
     * @var Organizacion
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="bitacora")     
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $organizacion;

    /**
     * @var Servicio
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="bitacoraServicio")     
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $servicio;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        if ($this->estado != null) {
            return $this->TipoEventoDescipcion[$this->tipo_evento];
        } else {
            return $this->TipoEventoDescipcion[0];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getTipoEvento()
    {
        return $this->tipo_evento;
    }

    public function setTipoEvento($tipo_evento)
    {
        $this->tipo_evento = $tipo_evento;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getEjecutor()
    {
        return $this->ejecutor;
    }

    public function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }    
    
    public function getChofer()
    {
        return $this->chofer;
    }

    public function setChofer(Chofer $chofer)
    {
        $this->chofer = $chofer;
        return $this;
    }
}
