<?php

namespace App\Entity;

/**
 * Description of Proyecto
 *
 * @author nicolas
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="proyecto")
 * @ORM\Entity(repositoryClass="App\Repository\ProyectoRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Proyecto
{

    const ESTADO_PROGRAMADO = 0;
    const ESTADO_ENCURSO = 1;
    const ESTADO_FINALIZADO = 2;

    private $strEstado = array(
        self::ESTADO_PROGRAMADO => 'Programado',
        self::ESTADO_ENCURSO => 'En Curso',
        self::ESTADO_FINALIZADO => 'Finalizado',
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        if ($this->estado != null) {
            return $this->strEstado[$this->estado];
        } else {
            return $this->strEstado[0];
        }
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=true) 
     */
    private $fechaInicio;

    /**
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true) 
     */
    private $fechaFin;

    /**
     * @ORM\Column(name="estado", type="integer", nullable=true) 
     */
    private $estado;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="ActividadGpm", mappedBy="proyecto",cascade ={"remove"})
     */
    protected $actividades;

    /**
     * @ORM\ManyToOne(targetEntity="Contratista", inversedBy="proyectos")
     * @ORM\JoinColumn(name="contratista_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $contratista;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="proyectos")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="proyectos")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="ResponsableGpm", inversedBy="proyectos")
     * @ORM\JoinColumn(name="responsable_id", referencedColumnName="id")
     */
    protected $responsable;

    private $progreso;

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getColor()
    {
        return $this->color;
    }

    function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getActividades()
    {
        return $this->actividades;
    }

    function getContratista()
    {
        return $this->contratista;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setColor($color)
    {
        $this->color = $color;
    }

    function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setActividades($actividades)
    {
        $this->actividades = $actividades;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function getOrganizacion()
    {
        return $this->organizacion;
    }

    function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;
    }

    function getFechaFin()
    {
        return $this->fechaFin;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getResponsable()
    {
        return $this->responsable;
    }

    function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }

    function getProgreso()
    {
        return $this->progreso;
    }

    function setProgreso($progreso)
    {
        $this->progreso = $progreso;
    }
}
