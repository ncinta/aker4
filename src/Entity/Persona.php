<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Persona
 *
 * @ORM\Table(name="persona")
 * @ORM\Entity(repositoryClass="App\Repository\PersonaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Persona
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string cuit
     *
     * @ORM\Column(name="cuit", type="string", nullable=true)
     */
    private $cuit;

    /**
     * @var string documento
     *
     * @ORM\Column(name="documento", type="string", nullable=true)
     */
    private $documento;

    /**
     * @var string telefono
     *
     * @ORM\Column(name="telefono", type="string", nullable=true)
     */
    private $telefono;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;


    /**
     * @ORM\OneToMany(targetEntity="Personal", mappedBy="persona")
     */
    protected $personal;

    /**
     * @ORM\OneToOne(targetEntity="ResponsableGpm", mappedBy="persona")
     */
    protected $responsable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contratista", inversedBy="personas")
     * @ORM\JoinColumn(name="contratista_id", referencedColumnName="id")
     */
    protected $contratista;


    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getId()
    {
        return $this->id;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getCreatedAt()
    {
        return $this->created_at;
    }

    function getUpdatedAt()
    {
        return $this->updated_at;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    function getCuit()
    {
        return $this->cuit;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getTelefono()
    {
        return $this->telefono;
    }

    function getPersonal()
    {
        return $this->personal;
    }

    function setCuit($cuit)
    {
        $this->cuit = $cuit;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    function setPersonal($personal)
    {
        $this->personal = $personal;
    }

    function getResponsable()
    {
        return $this->responsable;
    }

    function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }

    function getContratista()
    {
        return $this->contratista;
    }

    function setContratista($contratista)
    {
        $this->contratista = $contratista;
    }
}
