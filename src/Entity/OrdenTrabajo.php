<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\OrdenTrabajo
 *
 * @ORM\Table(name="ordentrabajo")
 * @ORM\Entity(repositoryClass="App\Repository\OrdenTrabajoRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class OrdenTrabajo
{

    const ESTADO_NUEVO = 0;
    const ESTADO_ENCURSO = 1;
    const ESTADO_CERRADO = 2;

    private $strEstado = array(
        'Nueva' => self::ESTADO_NUEVO,
        'En Curso' => self::ESTADO_ENCURSO,
        'Cerrada' => self::ESTADO_CERRADO,
    );

    public function getArrayStrEstado()
    {
        return $this->strEstado;
    }

    public function getStrEstado()
    {
        return array_search($this->estado, $this->strEstado);
    }

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime Fecha en la que se realizo el manteniemiento. La carga el operador segun su criterio
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var datetime Fecha en la que se realizo el cierre de la OT.
     * @ORM\Column(name="fecha_cierre", type="date", nullable=true)
     */
    private $fechaCierre;

    /**
     * @var datetime esta fecha la vamos a usar para el indice. Se setea con la actual cuando se cierra la ot.
     * @ORM\Column(name="close_at", type="datetime", nullable=true)
     */
    private $close_at;

    /**
     * @var datetime fecha en la cual ingreso el vehicul.
     * @ORM\Column(name="fecha_ingreso", type="date", nullable=true)
     */
    private $fechaIngreso;

    /**
     * @var datetime Fecha en la que salio el vehiculo
     * @ORM\Column(name="fecha_egreso", type="datetime", nullable=true)
     */
    private $fechaEgreso;

    /**
     * Indica cuanto es lo que se registro el odometro.
     * @ORM\Column(name="odometro", type="integer", nullable=true)
     */
    private $odometro;

    /**
     * Indica cuanto es lo que se registro el horometro.
     * @ORM\Column(name="horometro", type="integer", nullable=true)
     */
    private $horometro;

    /**
     * Indica cuanto es lo que se registro el odometro pero en el gps en el momento de carga.
     * @ORM\Column(name="odometro_gps", type="integer", nullable=true)
     */
    private $odometroGps;

    /**
     * Indica cuanto es lo que se registro el horometro pero en el gps en el momento de carga.
     * la fecha/hora de carga es la que figura en created_at
     * @ORM\Column(name="horometro_gps", type="integer", nullable=true)
     */
    private $horometroGps;

    /**
     * Tipo de orden de trabajo 0=Correctivo 1=Preventivo
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @var text $observacion es el texto descriptivo de lo que se ha realizado
     *
     * @ORM\Column(name="descripcion", type="string")
     */
    private $descripcion;

    /**
     * @var text $observacion es el texto descriptivo de lo que se ha realizado
     *
     * @ORM\Column(name="nota", type="text", nullable=true)
     */
    private $nota;

    /**
     * @ORM\Column(name="numero_externo", type="string", nullable=true)
     */
    private $numeroExterno;

    /**
     * @ORM\Column(name="numero_interno", type="string", nullable=true)
     */
    private $numeroInterno;

    /**
     * @var float $costo
     *
     * @ORM\Column(name="costo_total", type="float", nullable=true)
     */
    private $costoTotal;

    /**
     * @var float $costo
     *
     * @ORM\Column(name="costo_neto", type="float", nullable=true)
     */
    private $costoNeto;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(name="identificador", type="integer", nullable=true)
     */
    private $identificador;

    /**
     * @var integer $estado
     * 
     * @ORM\Column(name="estado", type="integer",  nullable=true)
     * 
     */
    private $estado;


    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="ordenesTrabajo")     
     * @ORM\JoinColumn(name="ejecutor_id", referencedColumnName="id", nullable=true, onDelete="SET NULL"))
     */
    private $ejecutor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Taller", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="taller_id", referencedColumnName="id", nullable=true)
     */
    protected $taller;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TallerSector", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="tallersector_id", referencedColumnName="id", nullable=true)
     */
    protected $tallerSector;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Deposito", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="deposito_id", referencedColumnName="id", nullable=true)
     */
    protected $deposito;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organizacion", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="organizacion_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organizacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoProducto", mappedBy="ordenesTrabajo")
     */
    private $productos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoMecanico", mappedBy="ordenTrabajo")
     */
    private $mecanicos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoExterno", mappedBy="ordenesTrabajo")
     */
    private $externos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoMantenimiento", mappedBy="ordenTrabajo")
     */
    private $mantenimientos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CentroCosto", inversedBy="ordenesTrabajo")
     * @ORM\JoinColumn(name="centrocosto_id", referencedColumnName="id",onDelete="SET NULL", nullable=true)
     */
    protected $centroCosto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="ordenTrabajo")
     */
    protected $peticiones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdenTrabajoHoraTaller", mappedBy="ordenesTrabajo")
     */
    private $horasTaller;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TareaOt", mappedBy="ordenTrabajo")
     */
    private $tareas;

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    function getVars()
    {
        return get_object_vars($this);
    }

    function getId()
    {
        return $this->id;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getCostoTotal()
    {
        return $this->costoTotal;
    }

    function getCreated_at()
    {
        return $this->created_at;
    }

    function getUpdated_at()
    {
        return $this->updated_at;
    }

    function getEjecutor()
    {
        return $this->ejecutor;
    }

    function getTaller()
    {
        return $this->taller;
    }

    function getDeposito()
    {
        return $this->deposito;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function getProductos()
    {
        return $this->productos;
    }

    function getMecanicos()
    {
        return $this->mecanicos;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;
    }

    function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    function setEjecutor($ejecutor)
    {
        $this->ejecutor = $ejecutor;
    }

    function setTaller($taller)
    {
        $this->taller = $taller;
    }

    function setDeposito($deposito)
    {
        $this->deposito = $deposito;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function setProductos($productos)
    {
        $this->productos = $productos;
    }

    public function addHorasTaller($horaTaller)
    {
        $this->horasTaller[] = $horaTaller;
    }

    public function removeHoraTaller($horaTaller)
    {
        $this->horasTaller->removeElement($horaTaller);
    }

    public function addProductos($producto)
    {
        $this->productos[] = $producto;
    }

    public function removeProducto($producto)
    {
        $this->productos->removeElement($producto);
    }

    function setMecanicos($mecanicos)
    {
        $this->mecanicos = $mecanicos;
    }

    public function addMecanicos($mecanico)
    {
        $this->mecanicos[] = $mecanico;
    }

    function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }

    function getOdometro()
    {
        return $this->odometro;
    }

    function getHorometro()
    {
        return $this->horometro;
    }

    function getOdometroGps()
    {
        return $this->odometroGps;
    }

    function getHorometroGps()
    {
        return $this->horometroGps;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function getNumeroExterno()
    {
        return $this->numeroExterno;
    }

    function getNumeroInterno()
    {
        return $this->numeroInterno;
    }

    function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;
    }

    function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;
    }

    function setOdometro($odometro)
    {
        $this->odometro = $odometro;
    }

    function setHorometro($horometro)
    {
        $this->horometro = $horometro;
    }

    function setOdometroGps($odometroGps)
    {
        $this->odometroGps = $odometroGps;
    }

    function setHorometroGps($horometroGps)
    {
        $this->horometroGps = $horometroGps;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    function setNumeroExterno($numeroExterno)
    {
        $this->numeroExterno = $numeroExterno;
    }

    function setNumeroInterno($numeroInterno)
    {
        $this->numeroInterno = $numeroInterno;
    }

    function getStrTipo()
    {
        if ($this->tipo == 0) {
            return 'Correctivo';
        } elseif ($this->tipo == 1) {
            return 'Preventivo';
        } elseif ($this->tipo == 2) {
            return 'Retrabajo';
        } else {
            return '---';
        }
    }

    function getExternos()
    {
        return $this->externos;
    }

    function setExternos($externos)
    {
        $this->externos = $externos;
    }

    function getTallerSector()
    {
        return $this->tallerSector;
    }

    function setTallerSector($tallerSector)
    {
        $this->tallerSector = $tallerSector;
    }

    function getMantenimientos()
    {
        return $this->mantenimientos;
    }

    function setMantenimientos($mantenimientos)
    {
        $this->mantenimientos = $mantenimientos;
    }

    function addMantenimiento($mant)
    {
        $this->mantenimientos[] = $mant;
    }

    function getIdentificador()
    {
        return $this->identificador;
    }

    function getFormatIdentificador()
    {
        return str_pad($this->identificador, 6, '0', STR_PAD_LEFT);
    }

    function setIdentificador($identificador)
    {
        $this->identificador = $identificador;
    }

    public function addExterno($externo)
    {
        $this->externos[] = $externo;
    }

    public function removeExterno($externo)
    {
        $this->externos->removeElement($externo);
    }

    function getCentroCosto()
    {
        return $this->centroCosto;
    }

    function setCentroCosto($centroCosto)
    {
        $this->centroCosto = $centroCosto;
    }

    function getCostoNeto()
    {
        return $this->costoNeto;
    }

    function setCostoNeto($costoNeto)
    {
        $this->costoNeto = $costoNeto;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    public function addPeticion($peticion)
    {
        $this->peticiones[] = $peticion;
    }

    public function removePeticion($peticion)
    {
        $this->peticiones->removeElement($peticion);
    }

    function getHorasTaller()
    {
        return $this->horasTaller;
    }

    function setHorasTaller($horasTaller)
    {
        $this->horasTaller = $horasTaller;
    }

    function getIndice_matenimientos()
    {
        return $this->indice_matenimientos;
    }

    function getNota()
    {
        return $this->nota;
    }

    function setNota($nota)
    {
        $this->nota = $nota;
    }

    function getTareas()
    {
        return $this->tareas;
    }

    function setTareas($tareas)
    {
        $this->tareas = $tareas;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }
    public function getCloseAt()
    {
        return $this->close_at;
    }

    public function setCloseAt($close_at): void
    {
        $this->close_at = $close_at;
    }

    /**
     * Get fecha en la que se realizo el cierre de la OT.
     *
     * @return  datetime
     */ 
    public function getFechaCierre()
    {
        return $this->fechaCierre;
    }

    /**
     * Set fecha en la que se realizo el cierre de la OT.
     *
     * @param  datetime  $fechaCierre  Fecha en la que se realizo el cierre de la OT.
     *
     * @return  self
     */ 
    public function setFechaCierre($fechaCierre)
    {
        $this->fechaCierre = $fechaCierre;

        return $this;
    }

    /**
     * Get the value of organizacion
     */ 
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set the value of organizacion
     *
     * @return  self
     */ 
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }
}
