<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\VehiculoFichaTecnica
 *
 * @ORM\Table(name="vehiculofichatecnica")
 * @ORM\Entity
 */
class VehiculoFichaTecnica
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="cantidad", type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\Column(name="frecuenciaCambio", type="string", nullable=true)
     */
    private $frecuenciaCambio;

    /**
     * @ORM\Column(name="alternativo", type="string", nullable=true)
     */
    private $alternativo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="fichasTecnicas", cascade={"persist"})
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Actividad", inversedBy="fichasTecnicas", cascade={"persist"})
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    protected $actividad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VehiculoModelo", inversedBy="fichaTecnica", cascade={"persist"})
     * @ORM\JoinColumn(name="vehiculomodelo_id", referencedColumnName="id")
     */
    protected $modelo;

    function getId()
    {
        return $this->id;
    }

    function getCantidad()
    {
        return $this->cantidad;
    }

    function getFrecuenciaCambio()
    {
        return $this->frecuenciaCambio;
    }

    function getAlternativo()
    {
        return $this->alternativo;
    }

    function getProducto()
    {
        return $this->producto;
    }

    function getModelo()
    {
        return $this->modelo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    function setFrecuenciaCambio($frecuenciaCambio)
    {
        $this->frecuenciaCambio = $frecuenciaCambio;
    }

    function setAlternativo($alternativo)
    {
        $this->alternativo = $alternativo;
    }

    function setProducto($producto)
    {
        $this->producto = $producto;
    }

    function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    function getActividad()
    {
        return $this->actividad;
    }

    function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }
}
