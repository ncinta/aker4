<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\ManyToMany as ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="estado")
 * @ORM\Entity(repositoryClass="App\Repository\EstadoRepository")
 */
class Estado
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="is_cerrado", type="boolean")
     */
    private $isCerrado;

    /**
     * @ORM\Column(name="is_default", type="boolean")
     */
    private $isDefault;

    /**
     * @ORM\Column(name="is_curso", type="boolean", nullable=true)
     */
    private $isCurso;

    /**
     * @ORM\Column(name="posicion", type="integer")
     * @ORM\OrderBy({"posicion" = "ASC"})
     */
    private $posicion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Peticion", mappedBy="estado")
     */
    private $peticiones;

    function getId()
    {
        return $this->id;
    }

    function __toString()
    {
        return $this->nombre;
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function getIsCerrado()
    {
        return $this->isCerrado;
    }

    function getIsDefault()
    {
        return $this->isDefault;
    }

    function getPosicion()
    {
        return $this->posicion;
    }

    function getPeticiones()
    {
        return $this->peticiones;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setIsCerrado($isCerrado)
    {
        $this->isCerrado = $isCerrado;
    }

    function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    function setPosicion($posicion)
    {
        $this->posicion = $posicion;
    }

    function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    }

    function getIsCurso()
    {
        return $this->isCurso;
    }

    function setIsCurso($isCurso)
    {
        $this->isCurso = $isCurso;
    }
}
