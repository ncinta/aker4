<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Programacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\Repository\ProgramacionRepository")
 * @ORM\HasLifecycleCallbacks() 
 */
class Programacion
{

    const RECHAZADA = -1;
    const INDEFINIDO = 0;
    const DESPACHADA = 1;
    const TERMINADAOK = 5;
    const TERMINADAPARCIAL = 6;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $mdmid
     *
     * @ORM\Column(name="mdmid", type="string", length=255, nullable=true)
     */
    private $mdmid;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var integer $estado
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * Mantiene la cantidad de intentos de programacion que se han realizado.
     * @var integer $intentos
     *
     * @ORM\Column(name="intentos", type="integer", nullable=true)
     */
    private $intentos;

    /**
     * @var datetime $ejecucion_at
     *
     * @ORM\Column(name="ejecucion_at", type="datetime", nullable=true)
     */
    private $ejecucion_at;

    /**
     * @var text $programacion
     *
     * @ORM\Column(name="programacion", type="text")
     */
    private $programacion;

    /**
     * @var text $resultado
     *
     * @ORM\Column(name="resultado", type="array", nullable=true)
     */
    private $resultado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="programaciones")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id")
     */
    protected $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * Set estado
     *
     * @param integer $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set ejecucion_at
     *
     * @param datetime $ejecucionAt
     */
    public function setEjecucionAt($ejecucionAt)
    {
        $this->ejecucion_at = $ejecucionAt;
    }

    /**
     * Get ejecucion_at
     *
     * @return datetime 
     */
    public function getEjecucionAt()
    {
        return $this->ejecucion_at;
    }

    /**
     * Set programacion
     *
     * @param text $programacion
     */
    public function setProgramacion($programacion)
    {
        $this->programacion = $programacion;
    }

    /**
     * Get programacion
     *
     * @return text 
     */
    public function getProgramacion()
    {
        return $this->programacion;
    }

    /**
     * Set resultado
     *
     * @param text $resultado
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;
    }

    /**
     * Get resultado
     *
     * @return text 
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set servicio
     *
     * @param App\Entity\Servicio $servicio
     */
    public function setServicio(\App\Entity\Servicio $servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * Get servicio
     *
     * @return App\Entity\Servicio 
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    public function getIntentos()
    {
        return $this->intentos;
    }

    public function setIntentos($intentos)
    {
        $this->intentos = $intentos;
        return $this;
    }

    public function getMdmid()
    {
        return $this->mdmid;
    }

    public function setMdmid($mdmid)
    {
        $this->mdmid = $mdmid;
        return $this;
    }
}
