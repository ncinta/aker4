<?php

namespace App\Entity;

use BeSimple\SoapBundle\ServiceDefinition\Annotation as Soap;

class ResponseReportes
{

    /**
     * @Soap\ComplexType("int")
     */
    public $code;

    /**
     * @Soap\ComplexType("string")
     */
    public $message;

    /**
     * @Soap\ComplexType("int")
     */
    public $servicio_id;

    /**
     * @Soap\ComplexType("string")
     */
    public $patente;

    /**
     * @Soap\ComplexType("dateTime", nillable = true)
     */
    public $fecha_inicio;

    /**
     * @Soap\ComplexType("dateTime", nillable = true)
     */
    public $fecha_fin;

    /**
     * @Soap\ComplexType("int", nillable = true)
     */
    public $cantidad_dias_reportados;

    /**
     * @Soap\ComplexType("int", nillable = true)
     */
    public $cantidad_dias_totales;

    /**
     * @Soap\ComplexType("int", nillable = true)
     */
    public $efectividad;
}
