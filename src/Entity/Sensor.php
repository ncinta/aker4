<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="sensor")
 * @ORM\Entity(repositoryClass="App\Repository\SensorRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Sensor
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false )
     */
    private $nombre;

    /**
     * @ORM\Column(name="abbr", type="string", length=5, nullable=false )
     * @Assert\Length(min = 2, max = 5, maxMessage = "El máximo debe ser de 5 caracteres.")
     */
    private $abbr;

    /**
     * @ORM\Column(name="medida", type="string", length=20, nullable=true )
     */
    private $medida;

    /**
     * @ORM\Column(name="serie", type="string", nullable=true )
     */
    private $serie;

    /**
     * @ORM\Column(name="valor_minimo", type="integer", nullable=true )
     */
    private $valorMinimo;

    /**
     * @ORM\Column(name="valor_maximo", type="integer", nullable=true )
     */
    private $valorMaximo;

    /**
     * @ORM\Column(name="ult_fechahora", type="datetime", nullable=true )
     */
    private $ultFechahora;

    /**
     * @ORM\Column(name="ult_valor", type="string", nullable=true )
     */
    private $ultValor;

    /**
     * @ORM\Column(name="ult_bateria", type="integer", nullable=true )
     */
    private $ultBateria;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ModeloSensor", inversedBy="sensores"))
     * @ORM\JoinColumn(name="modelosensor_id", referencedColumnName="id")
     */
    protected $modeloSensor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="sensores")
     * @ORM\JoinColumn(name="servicio_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    private $servicio;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function incrementCreatedAt()
    {
        if (null === $this->created_at) {
            $this->created_at = new \DateTime();
        }
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function incrementUpdatedAt()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function getModeloSensor()
    {
        return $this->modeloSensor;
    }

    public function setModeloSensor($modeloSensor)
    {
        $this->modeloSensor = $modeloSensor;
        return $this;
    }

    public function getServicio()
    {
        return $this->servicio;
    }

    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }

    public function getAbbr()
    {
        return $this->abbr;
    }

    public function setAbbr($abbr)
    {
        $this->abbr = $abbr;
        return $this;
    }

    public function getMedida()
    {
        return $this->medida;
    }

    public function getValorMinimo()
    {
        return $this->valorMinimo;
    }

    public function getValorMaximo()
    {
        return $this->valorMaximo;
    }

    public function setMedida($medida)
    {
        $this->medida = $medida;
        return $this;
    }

    public function setValorMinimo($valorMinimo)
    {
        $this->valorMinimo = $valorMinimo;
        return $this;
    }

    public function setValorMaximo($valorMaximo)
    {
        $this->valorMaximo = $valorMaximo;
        return $this;
    }

    public function getSerie()
    {
        return $this->serie;
    }

    public function setSerie($serie)
    {
        $this->serie = $serie;
        return $this;
    }

    public function getUltFechahora()
    {
        return $this->ultFechahora;
    }

    public function getUltValor()
    {
        return $this->ultValor;
    }

    public function getUltBateria()
    {
        return $this->ultBateria;
    }

    public function setUltFechahora($ultFechahora)
    {
        $this->ultFechahora = $ultFechahora;
        return $this;
    }

    public function setUltValor($ultValor)
    {
        $this->ultValor = $ultValor;
        return $this;
    }

    public function setUltBateria($ultBateria)
    {
        $this->ultBateria = $ultBateria;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->nombre;
    }

    public function data2array()
    {
        $d['id'] = $this->id;
        $d['nombre'] = $this->nombre;
        if (!is_null($this->abbr))
            $d['abbr'] = $this->abbr;
        if (!is_null($this->valorMinimo))
            $d['valorMinimo'] = $this->valorMinimo;
        if (!is_null($this->valorMaximo))
            $d['valorMaximo'] = $this->valorMaximo;
        if (!is_null($this->serie))
            $d['serie'] = $this->serie;
        if (!is_null($this->modeloSensor))
            $d['tipo'] = $this->modeloSensor->getNombre();
        return $d;
    }
}
