<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ServicioEvent extends Event
{
    public const EVENTO_DISABLED = 'evento.disabled';
    public const EVENTO_ENABLED = 'evento.enabled';

    public const OT_NEW = 'ot.new';
    public const OT_EDIT = 'ot.edit';
    public const OT_DELETE = 'ot.delete';
    public const OT_CLOSE = 'ot.close';


    protected $servicio;
    protected $evento;


    public function __construct($servicio, $evento = null)
    {
        $this->servicio = $servicio;
        $this->evento = $evento;
    }

    public function getServicio()
    {
        return $this->servicio;
    }
    public function getEvento()
    {
        return $this->evento;
    }
}
