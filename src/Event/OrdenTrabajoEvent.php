<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class OrdenTrabajoEvent extends Event
{

    public const OT_NEW = 'ot.new';
    public const OT_EDIT = 'ot.edit';
    public const OT_DELETE = 'ot.delete';
    public const OT_CLOSE = 'ot.close';


    protected $servicio;
    protected $ordenTrabajo;


    public function __construct($servicio, $ordenTrabajo = null)
    {
        $this->servicio = $servicio;
        $this->ordenTrabajo = $ordenTrabajo;
    }

    public function getServicio()
    {
        return $this->servicio;
    }
    public function getOrdenTrabajo()
    {
        return $this->ordenTrabajo;
    }
}
