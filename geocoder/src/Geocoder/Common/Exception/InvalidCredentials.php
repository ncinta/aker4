<?php
namespace Geocoder\Common\Exception;


final class InvalidCredentials extends \RuntimeException implements Exception
{
}
