<?php

namespace Geocoder\Common\Exception;


final class QuotaExceeded extends \RuntimeException implements Exception
{
}
