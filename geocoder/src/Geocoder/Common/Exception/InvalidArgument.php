<?php
namespace Geocoder\Common\Exception;

class InvalidArgument extends \InvalidArgumentException implements Exception
{
}
