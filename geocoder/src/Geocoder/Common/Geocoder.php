<?php



namespace Geocoder\Common;

use Geocoder\Common\Provider\Provider;

interface Geocoder extends Provider
{

    /**
     * The default result limit.
    */
    const DEFAULT_RESULT_LIMIT = 5;

    /**
     * Reverses geocode given latitude and longitude values.
     *
     * @param float $latitude
     * @param float $longitude
     *
     *
     * @throws \Geocoder\Exception\Exception
     */
    public function reverse(float $latitude, float $longitude);
}
