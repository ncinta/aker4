<?php

namespace Geocoder\Common\Provider;

use Geocoder\Common\Exception\QuotaExceeded;
use Geocoder\Common\Exception\InvalidCredentials;
use Geocoder\Common\Exception\InvalidServerResponse;

abstract class AbstractProvider implements Provider
{


    private function curlGet($service_url)
    {
        //echo "Url GET ".$service_url;
        //inicializa
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 90); //timeout in seconds
        //ejecuta
        $curl_response = curl_exec($curl);

        // Comprobar el código de estado HTTP
        if (!curl_errno($curl)) {
            switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                case 200:  # OK
                    break;
                case 400:
                    throw InvalidServerResponse::create($service_url, $http_code);
                    break;
                case 401:
                case 403:
                    throw new InvalidCredentials();
                    break;
                case 429:
                    throw new QuotaExceeded();
                    break;
                default:
                    throw InvalidServerResponse::create($service_url, $http_code);
            }
        }

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            return  array('status' => $info['http_code'], 'info' => $info);
        }

        // Cerrar el manejador
        curl_close($curl);

        $decoded = json_decode($curl_response, true);

        if (isset($decoded)) {
            $decoded['exito'] = true;
            $decoded['url']   = $service_url;
            return $decoded;
        }
    }

    /**
     * Get URL and return contents. If content is empty, an exception will be thrown.
     *
     * @param string $url
     *
     * @throws InvalidServerResponse
     */
    protected function getUrlContents($url)
    {
        try {
            $result = $this->curlGet($url);            
            if (isset($result['exito']) && $result['exito']) {
                return $result;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }
}
