<?php

namespace Geocoder\Common\Provider;

use Geocoder\Common\Query\ReverseQuery;


interface Provider
{
    

    /**
     * @param ReverseQuery $query
     *
     * @return Collection
     *
     * @throws \Geocoder\Exception\Exception
     */
    public function reverseQuery(ReverseQuery $query);

    /**
     * Returns the provider's name.
     *
     * @return string
     */
    public function getName();
}
