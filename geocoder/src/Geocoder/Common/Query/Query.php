<?php

namespace Geocoder\Common\Query;


interface Query
{
    /**
     * @param string $locale
     *
     * @return Query
     */
    public function withLocale( $locale);

    /**
     * @param int $limit
     *
     * @return Query
     */
    public function withLimit(int $limit);

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return Query
     */
    public function withData( $name, $value);

    /**
     * @return string|null
     */
    public function getLocale();

    /**
     * @return int
     */
    public function getLimit();

    /**
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function getData( $name, $default = null);

    /**
     * @return array
     */
    public function getAllData();

    /**
     * @return string
     */
    public function __toString();
}
