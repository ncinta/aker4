<?php
namespace Geocoder\Common;

use Geocoder\Model\Bounds;
use Geocoder\Common\Query\GeocodeQuery;
use Geocoder\Common\Query\ReverseQuery;
use Geocoder\Common\Provider\Provider;


final class StatefulGeocoder implements Geocoder
{
    /**
     * @var string
     */
    private $locale;

    /**
     * @var Bounds
     */
    private $bounds;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @param Provider $provider
     * @param string   $locale
     */
    public function __construct(Provider $provider, $locale = null)
    {
        $this->provider = $provider;
        $this->locale = $locale;
        $this->limit = Geocoder::DEFAULT_RESULT_LIMIT;
    }

    /**
     * {@inheritdoc}
     */
    public function reverse(float $latitude, float $longitude)
    {
        $query = ReverseQuery::fromCoordinates($latitude, $longitude)
            ->withLimit($this->limit);

        if (!empty($this->locale)) {
            $query = $query->withLocale($this->locale);
        }

        return $this->provider->reverseQuery($query);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseQuery(ReverseQuery $query)
    {
        $locale = $query->getLocale();
        if (empty($locale) && null !== $this->locale) {
            $query->withLocale($this->locale);
        }

        return $this->provider->reverseQuery($query);
    }

    /**
     * @param string $locale
     *
     * @return StatefulGeocoder
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @param Bounds $bounds
     *
     * @return StatefulGeocoder
     */
    public function setBounds(Bounds $bounds)
    {
        $this->bounds = $bounds;

        return $this;
    }

    /**
     * @param int $limit
     *
     * @return StatefulGeocoder
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'stateful_geocoder';
    }
}
