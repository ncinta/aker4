<?php

namespace Geocoder\Provider\Cache;

use Geocoder\Collection;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\Provider\Provider;
use Psr\SimpleCache\CacheInterface;

final class ProviderCache implements Provider
{
    /**
     * @var Provider
     */
    private $realProvider;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * How log a result is going to be cached.
     *
     * @var int|null
     */
    private $lifetime;

    /**
     * @param Provider       $realProvider
     * @param CacheInterface $cache
     * @param int            $lifetime
     */
    public function __construct(Provider $realProvider, CacheInterface $cache, int $lifetime = null)
    {
        $this->realProvider = $realProvider;
        $this->cache = $cache;
        $this->lifetime = $lifetime;
    }


    /**
     * {@inheritdoc}
     */
    public function reverseQuery(ReverseQuery $query)
    {
        $cacheKey = $this->getCacheKey($query);
        if (null !== $result = $this->cache->get($cacheKey)) {
            return $result;
        }

        $result = $this->realProvider->reverseQuery($query);
        $this->cache->set($cacheKey, $result, $this->lifetime);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return sprintf('%s (cache)', $this->realProvider->getName());
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->realProvider, $method], $args);
    }

    /**
     * @param GeocodeQuery|ReverseQuery $query
     *
     * @return string
     */
    private function getCacheKey($query)
    {
        // Include the major version number of the geocoder to avoid issues unserializing.
        return 'v4'.sha1((string) $query);
    }
}
