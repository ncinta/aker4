<?php

namespace Geocoder\Provider\Nominatim;


use Geocoder\Common\Query\GeocodeQuery;
use Geocoder\Common\Query\ReverseQuery;
use Geocoder\Common\Provider\AbstractProvider;



final class Nominatim extends AbstractProvider
{
    const ROOT_URL = 'http://nominatim.akercontrol.com';
    //const ROOT_URL = 'http://192.168.28.80'; 
    //const ROOT_URL = 'http://192.168.26.54/nominatim'; 
    //const ROOT_URL = 'https://open.mapquestapi.com/nominatim/v1'; 
    /**
     * @var string
     */
    private $rootUrl;

    private $coordinates;

    private $key;
    /**
     * @param string     $rootUrl Root URL of the nominatim server
     */
    public function __construct($rootUrl, $key)
    {
        $this->rootUrl = rtrim($rootUrl, '/');
        $this->key = $key;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseQuery(ReverseQuery $query)
    {
        $this->coordinates = $query->getCoordinates();
        $longitude =  $this->coordinates->getLongitude();
        $latitude  =  $this->coordinates->getLatitude();
        $url = sprintf($this->getReverseEndpointUrl(), $latitude, $longitude, $this->key);
        $content = $this->executeQuery($url, $query->getLocale());
        return $content;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nominatim';
    }

    /**
     * @param string      $url
     * @param string|null $locale
     *
     * @return string
     */
    private function executeQuery($url,  $locale = 'es')
    {       
        if (null !== $locale) {
            $url = sprintf('%s&accept-language=%s', $url, $locale);
        }

        $content = parent::getUrlContents($url);        
        if (isset($content)) {
            $longitude  = $this->coordinates->getLongitude();
            $latitude   = $this->coordinates->getLatitude();
            $result = array(
                'provided_by' => $this->getName(),
                'consulta' => null,
                'exito' => $content['exito'],
                'url'   => $content['url'],
                'direccion' => null,
                'latitud'   => ['O' => null],
                'longitud'  => ['O' => null],
            );

            if (isset($content['address'])) {
                $result['address'] = $content['address'];
            }

            $result['consulta']    = $latitude . ', ' . $longitude;
            $result['direccion']    = isset($content['display_name']) ? $content['display_name'] : 'N/A';
            $result['latitud']['O']  = isset($content['lat']) ? $content['lat'] : 'N/A';
            $result['longitud']['O']  = isset($content['lon']) ? $content['lon'] : 'N/A';
            return $result;
        }

        return null;
    }


    private function getReverseEndpointUrl()
    {
        //return $this->rootUrl.'/reverse.php?format=jsonv2&lat=%F&lon=%F&osm_type=N&key=%s&addressdetails=1';
        return $this->rootUrl . '/reverse?format=jsonv2&lat=%F&lon=%F&osm_type=N&key=%s&addressdetails=1';
    }
}
