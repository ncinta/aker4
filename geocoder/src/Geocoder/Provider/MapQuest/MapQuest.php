<?php

namespace Geocoder\Provider\MapQuest;


use Geocoder\Common\Exception\InvalidCredentials;
use Geocoder\Common\Provider\AbstractProvider;
use Geocoder\Common\Query\ReverseQuery;



final class MapQuest extends AbstractProvider
{
   
    //const MPQ_API_KEY = 'gGUGaPbYWeaUa8r3BM3MhbspeHANvbFU';
    const MPQ_API_KEY = 'Fp1r7oAaGqjGbI5NXmK8sASLrZYDIuy2';
    /**
     * @var string
     */
    const OPEN_REVERSE_ENDPOINT_URL = 'https://open.mapquestapi.com/geocoding/v1/reverse?key=%s&lat=%F&lng=%F';


    /**
     * @var string
     */
    const LICENSED_REVERSE_ENDPOINT_URL = 'https://www.mapquestapi.com/geocoding/v1/reverse?key=%s&lat=%F&lng=%F';

    /**
     * MapQuest offers two geocoding endpoints one commercial (true) and one open (false)
     * More information: http://developer.mapquest.com/web/tools/getting-started/platform/licensed-vs-open.
     *
     * @var bool
     */
    private $licensed;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param HttpClient $client   an HTTP adapter
     * @param string     $apiKey   an API key
     * @param bool       $licensed true to use MapQuest's licensed endpoints, default is false to use the open endpoints (optional)
     */
    public function __construct( $apiKey,  $licensed = false)
    {
        if (empty($apiKey)) {
            throw new InvalidCredentials('No API key provided.');
        }

        $this->apiKey = $apiKey;
        $this->licensed = $licensed;
    }

    

    /**
     * {@inheritdoc}
     */
    public function reverseQuery(ReverseQuery $query)
    {
        $coordinates = $query->getCoordinates();
        $longitude = $coordinates->getLongitude();
        $latitude = $coordinates->getLatitude();

        if ($this->licensed) {
            $url = sprintf(self::LICENSED_REVERSE_ENDPOINT_URL, $this->apiKey, $latitude, $longitude);
        } else {
            $url = sprintf(self::OPEN_REVERSE_ENDPOINT_URL, $this->apiKey, $latitude, $longitude);
        }

        return $this->executeQuery($url);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'map_quest';
    }

    /**
     * @param string $url
     *
     * @return AddressCollection
     */
    private function executeQuery($url)
    {
        $json = parent::getUrlContents($url);

        if (!isset($json['results']) || empty($json['results'])) {
            return null;
        }

        $locations = $json['results'][0]['locations'][0];

        if (empty($locations)) {
            return null;
        }

        //var_dump($json);
        //Process response
        $result = array(
            'provided_by'=>'n/a',
            'consulta' => null,
            'exito' => $json['exito'],
            'url'   => $json['url'],
            'direccion' => null,
            'latitud'   => ['O' =>null],
            'longitud'  => ['O' =>null],
            'geocode_quality_code' =>null,
            'geocode_quality' => null,
            'street_name' => null,
            'locality' => null,
            'state'    => null,
            'postal_code' => null,
            'sub_locality' => null,
            'country' => null,
            'side_of_street' => null,
            'map_url' => null
        );  

        $result['consulta']             = $this->getProvidedLocation($json);
        $result['provided_by']          = $this->getName();
        $result['latitud']['O']         = $locations['displayLatLng']['lat'];
        $result['longitud']['O']        = $locations['displayLatLng']['lng'];
        $result['geocode_quality_code'] = $locations['geocodeQualityCode'];
        $result['geocode_quality']      = $locations['geocodeQuality'];
        $result['street_name']          = $locations['street'];
        $result['side_of_street']       = $locations['sideOfStreet'];
        $result['locality']             = $locations['adminArea5'];
        $result['state']                = $locations['adminArea3'];
        $result['postal_code']          = $locations['postalCode'];
        $result['sub_locality']         = $locations['adminArea6'];
        $result['country']              = $this->getCountry($locations['adminArea1']);
        $result['map_url']              = $locations['mapUrl'];
        $result['direccion']           = $this->getDisplayName($result);
        return $result;
    }

    private function getProvidedLocation($json){
        if(!isset($json)){
            return null;
        }
        return  $json['results'][0]['providedLocation']['latLng']['lat'].', '.$json['results'][0]['providedLocation']['latLng']['lng'];
    }

    private function getDisplayName($location){

        if(!isset($location)){
            return null;
        }

        $dispName = $location['street_name'].', '.$location['side_of_street'].', '.$location['locality'].', '.$location['state'].', '.$location['country'].', CP'.$location['postal_code'];

        return $dispName;
    }

    private function getCountry($code){
        if(!isset($code)){
            return null;
        }
        
        if($code == 'AR'){
            return 'Argentina';
        }

        if($code == 'CH'){
            return 'Chile';
        }
    }

}
