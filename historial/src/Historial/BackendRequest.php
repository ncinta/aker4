<?php

namespace Historial;

use Symfony\Component\HttpKernel\Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BackendRequest
{

    private $result;
    private $client;

    public function __construct(HttpClientInterface $client, $backend, $request, $context)
    {
        try {
            $this->client = $client;
            set_time_limit(60);
            $result = $this->send($backend, $request, $context);
            $this->result = isset($result) ? $result : null;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function send($backend, $request, $data)
    {
        $uri = $backend . '/' . $request . '/index.php';      //armo la uri completa
        $d = json_encode($data, true);          //convierto el array a string json

        $response = $this->client->request(
            'GET',
            $uri,
            [
                'headers' => [
                    'Content-Type' => 'text/plain',
                    'Content-Length' => strlen($d),
                ],
                'body' => $d,
            ]
        );
        
        if ($response->getStatusCode() == 200) {
            $this->result = $response->getContent();
            return $this->result;
        } else {
            return null;
        }
    }

    public function getResult()
    {
        return $this->result;
    }
}
