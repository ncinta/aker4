<?php

namespace Historial;

class ContextHelper {

    function castReferencias($referencias) {
        $context_referencias = array();
        foreach ($referencias as $referencia) {
            if ($referencia instanceof \App\Entity\Referencia) {
                $context_referencia = array(
                    'referencia_id' => $referencia->getId(),
                    'angulo_deteccion' => $referencia->getAnguloDeteccion(),
                    'entrada' => $referencia->getEntrada(),
                    'salida' => $referencia->getSalida(),
                    'velocidad_maxima' => $referencia->getVelocidadMaxima()
                );
            }else{ //es una instancia de referenciagpm
                $context_referencia = array(
                    'referencia_id' => $referencia->getId(),
                    'velocidad_maxima' => $referencia->getVelocidadMaxima()
                );
                
            }
            if ($referencia->getClase() === $referencia::CLASE_POLIGONO) {
                if ($referencia->getPoligono() == '') {
                    continue;   //es un poligono pero no tiene poligo.... chaaaan
                }
                if ($referencia->isOldPoligon()) {
                    $context_referencia = array_merge($context_referencia, array(
                        'poligono' => $referencia->getPoligono()
                    ));
                } else {
                    //es para mantener el formato viejo.
                    $context_referencia = array_merge($context_referencia, array(
                        'poligono' => '3,#ffff0000,#19ffffff,' . $referencia->getPoligono())
                    );
                }
            } else {
                if ($referencia->getRadio() == '') {
                    continue;   //es un radio pero no tiene radio.... chaaaan
                }
                $context_referencia = array_merge($context_referencia, array(
                    'latitud' => $referencia->getLatitud(),
                    'longitud' => $referencia->getLongitud(),
                    'radio' => $referencia->getRadio()
                ));
            }
            $context_referencias[] = $context_referencia;
        }
        return $context_referencias;
    }

    function getReferencias($em, $user) {
        $organizacion = $user->getOrganizacion();
        $referencias = $em->getRepository('SecurMainBundle:Referencia')->getReferenciasAsociadas($organizacion);
        return $this->castReferencias($referencias);
    }

}

?>